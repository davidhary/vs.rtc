﻿Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    ''' <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 6/20/2019 </para></remarks>
    Public NotInheritable Class MyLibrary

        ''' <summary> Constructor that prevents a default instance of this class from being created. </summary>
        Private Sub New()
            MyBase.New()
        End Sub

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = isr.Core.ProjectTraceEventId.Visuals + &H3

        Public Const AssemblyTitle As String = "Real Time Chart Library"
        Public Const AssemblyDescription As String = "Real Time Chart Library"
        Public Const AssemblyProduct As String = "Visuals.Real.Time.Chart.Library"

    End Class

End Namespace

