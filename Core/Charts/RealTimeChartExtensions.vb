Partial Public Class RealTimeChartControl

    ''' <summary> Removes a channel (trace). </summary>
    Public Sub DeleteChannel(ByVal channel As Integer)
        ' Select channel (trace) to delete
        Me.LogicalChannel = -channel
    End Sub

    ''' <summary> Gets or sets the chart enabled. </summary>
    ''' <value> The chart enabled. </value>
    Public Property ChartingEnabled As Boolean

    ''' <summary> Disable charting. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub DisableCharting()
        ' CHeck if chart is enabled
        If ChartingEnabled Then
            Try
                ' Prevent each channel (trace) from being erased individually
                Me.ChartAction = RealTimeChart.ChartAction.DisablePaint

                ' Set the charting global to false.
                ChartingEnabled = False
            Catch

            End Try
        End If
    End Sub

    ''' <summary> Enable charting. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub EnableCharting()
        ' CHeck if chart is disabled
        If Not ChartingEnabled Then
            Try
                ' Enable paint
                Me.ChartAction = RealTimeChart.ChartAction.EnablePaint

                ' Set the charting global.
                ChartingEnabled = True
            Catch

            End Try

        End If
    End Sub

    ''' <summary> Erase channel (trace). </summary>
    ''' <param name="channel"> The channel (trace). </param>
    Public Sub EraseChannel(ByVal channel As Integer)
        ' select channel (trace) to erase
        Me.LogicalChannel = channel

        ' use channel erase display action
        Me.ChnDspAction = RealTimeChart.ChannelDisplayAction.Erase
        Me.ChnDspAction = RealTimeChart.ChannelDisplayAction.Flush
    End Sub

    ''' <summary> Erase channels (traces). </summary>
    ''' <param name="firstChannel"> The first channel (trace). </param>
    ''' <param name="lastChannel">  The last channel (trace). </param>
    Public Sub EraseChannels(ByVal firstChannel As Integer, ByVal lastChannel As Integer)

        ' disable chart to prevent each channel (trace) from being erased
        DisableCharting()

        ' erase specified channels (traces)
        For channel As Integer = firstChannel To lastChannel

            ' Erase channel (trace)
            Me.EraseChannel(channel)

        Next channel

        ' paint entire window to erase all channels (traces)
        EnableCharting()

    End Sub

    ''' <summary> Erase horizontal scale. </summary>
    ''' <param name="logicalWindow"> The logical window. </param>
    Public Sub EraseHorizontalScale(ByVal logicalWindow As Integer)

        ' select Scale to erase
        Me.LogicalScale = 2 * logicalWindow - 1

        ' Select the vertical scale
        Me.ScaleDimension = RealTimeChart.LogicalDimension.Horizontal

        ' Erase the scale
        Me.ScaleAction = RealTimeChart.ScaleAction.Erase

    End Sub

    ''' <summary> Erase vertical scale. </summary>
    ''' <param name="logicalWindow"> The logical window. </param>
    Public Sub EraseVerticalScale(ByVal logicalWindow As Integer)

        ' Description: Erase a vertical scale and label.
        With Me

            ' select Scale to erase
            .LogicalScale = 2 * logicalWindow

            ' Select the vertical scale
            .ScaleDimension = RealTimeChart.LogicalDimension.Vertical

            ' scale visibility: none, scale, label, or both
            .ScaleVisibility = RealTimeChart.ScaleVisibility.Hidden

            ' Erase the scale
            .ScaleAction = RealTimeChart.ScaleAction.Initialize
            .ScaleAction = RealTimeChart.ScaleAction.Erase

        End With
    End Sub

    ''' <summary> Initializes the channels (traces). </summary>
    ''' <param name="channelsCount">  Number of channels (traces). </param>
    ''' <param name="samplesCount">   Number of samples. </param>
    ''' <param name="viewportsCount"> Number of viewports. </param>
    ''' <param name="displayMode">    The display mode. </param>
    Public Sub InitializeChannels(ByVal channelsCount As Integer, ByVal samplesCount As Integer, ByVal viewportsCount As Integer, ByVal displayMode As ChannelDisplayMode)

        '   This method initializes traces for scrolling or CRT mode.
        With Me

            ' Describe all Channels (traces)
            For channel As Integer = 1 To channelsCount

                ' Select channel
                .LogicalChannel = channel

                ' Assign viewport to channel
                If viewportsCount >= channel Then
                    .ChnDspViewport = channel
                Else
                    .ChnDspViewport = channel Mod viewportsCount + 1
                End If

                ' Assign window to channel
                .ChnDspWindow = channel

                ' Specify pen, same value as channel name
                .ChnDspPen = channel

                ' Set total input buffer length
                .ChnDspBufLen = samplesCount

                ' Set display mode
                .ChnDspMode = displayMode

                ' use line style for plotting
                .ChnDspStyle = RealTimeChart.ChannelDisplayStyle.Lines

                ' Set offsets to zero
                .ChnDspOffsetX = 0
                .ChnDspOffsetY = 0

                ' Describe horizontal dimension
                .ChannelDataDimension = RealTimeChart.LogicalDimension.Horizontal
                .ChnDataShape = RealTimeChart.ChannelDataShape.AutoIncr

                ' offset into buffer (in points)
                .ChnDataOffset = 0

                ' logical increment to next value in array
                .ChnDataIncr = 1

                ' Describe vertical dimension
                .ChannelDataDimension = RealTimeChart.LogicalDimension.Vertical
                .ChnDataShape = RealTimeChart.ChannelDataShape.Scalar
                .ChnDataType = RealTimeChart.ChannelDataType.Single

                ' offset into buffer (in points)
                .ChnDataOffset = 0

                ' logical increment to next value for 2D array
                .ChnDataIncr = 1

                ' Initialize the channel.
                .ChnDspAction = RealTimeChart.ChannelDisplayAction.Initialize

            Next channel

        End With
    End Sub

    ''' <summary> Initializes the channels single dimension. </summary>
    ''' <param name="channelsCount">  Number of channels. </param>
    ''' <param name="samplesCount">   Number of samples. </param>
    ''' <param name="viewportsCount"> Number of viewports. </param>
    ''' <param name="displayMode">    The display mode. </param>
    Public Sub InitializeChannelsSingleDimension(ByVal channelsCount As Integer, ByVal samplesCount As Integer, ByVal viewportsCount As Integer, ByVal displayMode As ChannelDisplayMode)

        '   initializes the strip chart for display traces of 1d data from and 1d data array.
        With Me

            ' Describe all Channels
            For channel As Integer = 1 To channelsCount

                ' Select channel
                .LogicalChannel = channel

                ' Assign viewport to channel
                If viewportsCount >= channel Then
                    .ChnDspViewport = channel
                Else
                    .ChnDspViewport = channel Mod viewportsCount + 1
                End If

                ' Assign window to channel
                .ChnDspWindow = channel

                ' Specify pen, same value as channel name
                .ChnDspPen = channel

                ' Set total input buffer length
                .ChnDspBufLen = samplesCount

                ' Set display mode
                .ChnDspMode = displayMode

                ' use line style for plotting
                .ChnDspStyle = RealTimeChart.ChannelDisplayStyle.Lines

                ' Set offsets to zero
                .ChnDspOffsetX = 0
                .ChnDspOffsetY = 0

                ' Describe horizontal dimension
                .ChannelDataDimension = RealTimeChart.LogicalDimension.Horizontal
                .ChnDataShape = RealTimeChart.ChannelDataShape.AutoIncr

                ' offset into buffer (in points)
                .ChnDataOffset = 0

                ' logical increment to next value in array
                .ChnDataIncr = 1

                ' Describe vertical dimension
                .ChannelDataDimension = RealTimeChart.LogicalDimension.Vertical
                .ChnDataShape = RealTimeChart.ChannelDataShape.Scalar
                .ChnDataType = RealTimeChart.ChannelDataType.Single

                ' offset into buffer (in points)
                .ChnDataOffset = channel - 1

                ' logical increment to next value for 1D array
                .ChnDataIncr = channelsCount

                ' Initialize the channel.
                .ChnDspAction = RealTimeChart.ChannelDisplayAction.Initialize

            Next channel

        End With
    End Sub

    ''' <summary> Initializes the horizontal scale. </summary>
    ''' <param name="logicalWindow"> The logical window. </param>
    Public Sub InitializeHorizontalScale(ByVal logicalWindow As Integer)

        '   This method initialize the vertical scale and label.
        '   scale is 2 * (logical window) for vertical and  2 * (logical window) -1 for horizontal        
        With Me

            ' select scale
            .LogicalScale = 2 * logicalWindow - 1

            ' Select the horizontal scale
            .ScaleDimension = RealTimeChart.LogicalDimension.Horizontal

            ' Initialize the scale
            .ScaleAction = RealTimeChart.ScaleAction.Initialize

        End With

    End Sub

    ''' <summary> Initializes the vertical scale. </summary>
    ''' <param name="logicalWindow"> The logical window. </param>
    Public Sub InitializeVerticalScale(ByVal logicalWindow As Integer)

        '   This method initializes the vertical scale and label.
        With Me
            ' select Scale
            .LogicalScale = 2 * logicalWindow

            ' Select the vertical scale
            .ScaleDimension = RealTimeChart.LogicalDimension.Vertical

            ' Initialize the scale
            .ScaleAction = RealTimeChart.ScaleAction.Initialize

        End With

    End Sub

    ''' <summary> Initializes the viewports. </summary>
    ''' <param name="viewportsCount"> Number of viewports. </param>
    Public Sub InitializeViewports(ByVal viewportsCount As Integer)

        '   This method initializes the chart viewports.
        With Me

            ' Viewport Description:

            ' number of viewports to be initialized
            .Viewports = viewportsCount

            ' storage mode plot color, same for all viewports
            .ViewportStorageColor = Drawing.Color.Green

            ' Initialize storage mode for each viewport:
            For iViewPort As Integer = 1 To viewportsCount

                ' select viewport
                .LogicalViewport = iViewPort

                ' disable storage mode
                .ViewportStorageOn = False

            Next iViewPort

            ' Set the number of viewports
            .Viewports = viewportsCount

        End With

    End Sub

    ''' <summary> Initializes the windows. All windows are initialized to the same ordinate and abscissa. </summary>
    ''' <param name="windowsCount">             Number of windows. </param>
    ''' <param name="ordinateUnitsPerDivision"> The ordinate units per division. </param>
    ''' <param name="abscissaUnitsPerDivision"> The abscissa units per division. </param>
    Public Sub InitializeWindows(ByVal windowsCount As Integer, ByVal ordinateUnitsPerDivision As Single, ByVal abscissaUnitsPerDivision As Single)

        Dim height, width, ordinateMin As Single

        With Me

            ' Set width in Seconds (X/div * N divisions)
            width = abscissaUnitsPerDivision * .MajorDivHorz

            ' Set height in Volts (Y/div * N divisions)
            height = ordinateUnitsPerDivision * .MajorDivVert

            ' Set bottom minimum ordinate to negative half height
            ordinateMin = height / -2

            For iWindow As Integer = 1 To windowsCount

                ' Window Description:

                ' select window
                .LogicalWindow = iWindow

                ' left minimum abscissa
                .WndXmin = 0

                ' Width
                .WndWidth = width

                ' Bottom
                .WndYmin = ordinateMin

                ' height
                .WndHeight = height

            Next iWindow

        End With

    End Sub

    ''' <summary> Measure text. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="font"> The font. </param>
    ''' <param name="text"> The text. </param>
    ''' <returns> A Drawing.SizeF. </returns>
    Public Shared Function MeasureText(ByVal font As Drawing.Font, ByVal text As String) As Drawing.SizeF
        If font Is Nothing Then Throw New ArgumentNullException(NameOf(font))
        Using graphics As System.Drawing.Graphics = New Windows.Forms.Control().CreateGraphics()
            Return graphics.MeasureString(text, font)
        End Using
    End Function

    ''' <summary> Maximizes. </summary>
    ''' <remarks>
    ''' RtChart uses isotropic scaling, where each division is exactly a square.  The isotropic
    ''' scaling is achieved by setting equal number if pixels for the vertical and horizontal size of
    ''' each divisions.  Thus, in order to set a desired margin with given divisions, we must find
    ''' the smallest of the two division dimensions in pixels and use this value to set the size of
    ''' both dimensions.<para>
    ''' To fit the chart to the space better, you can adjust the number of divisions.  For
    ''' example:</para><para>
    ''' </para><para>
    '''       divisions = 2 * Samples / Rate
    ''' </para><para>
    ''' The following assumptions were made:</para><para>
    ''' 1.  We assume in this routine that the chart divisions
    '''     were set earlier and that they are given in the
    '''     .MajorDivHorz and .MajorDivVert properties.</para><para>
    ''' 2.  We assume that space is needed for both left, right,
    '''     and bottom scales and labels.</para><para>
    ''' </para>
    ''' Also, because the TextWidth and TextHeight methods do not apply to the chart, set the same
    ''' fonts for the RtChart and its parent control.
    ''' </remarks>
    ''' <param name="left">    The left. </param>
    ''' <param name="top">     The top. </param>
    ''' <param name="width">   The width. </param>
    ''' <param name="height">  The height. </param>
    ''' <param name="caption"> The largest vertical scale label, e.g., 99.9. It seems that RtChart
    '''                        takes the negative sign into account separately. </param>
    Public Sub Maximize(ByVal left As Integer, ByVal top As Integer, ByVal width As Integer, ByVal height As Integer, ByVal caption As String)
        Me.Maximize(left, top, width, height, MeasureText(Me.Font, caption))
    End Sub

    ''' <summary> Maximizes. </summary>
    ''' <remarks>
    ''' RtChart uses isotropic scaling, where each division is exactly a square.  The isotropic
    ''' scaling is achieved by setting equal number if pixels for the vertical and horizontal size of
    ''' each divisions.  Thus, in order to set a desired margin with given divisions, we must find
    ''' the smallest of the two division dimensions in pixels and use this value to set the size of
    ''' both dimensions.<para>
    ''' To fit the chart to the space better, you can adjust the number of divisions.  For
    ''' example:</para><para>
    ''' </para><para>
    '''       divisions = 2 * Samples / Rate
    ''' </para><para>
    ''' The following assumptions were made:</para><para>
    ''' 1.  We assume in this routine that the chart divisions
    '''     were set earlier and that they are given in the
    '''     .MajorDivHorz and .MajorDivVert properties.</para><para>
    ''' 2.  We assume that space is needed for both left, right,
    '''     and bottom scales and labels.</para><para>
    ''' </para>
    ''' Also, because the TextWidth and TextHeight methods do not apply to the chart, set the same
    ''' fonts for the RtChart and its parent control.
    ''' </remarks>
    ''' <param name="left">        The left. </param>
    ''' <param name="top">         The top. </param>
    ''' <param name="width">       The width. </param>
    ''' <param name="height">      The height. </param>
    ''' <param name="captionSize"> Size of the largest vertical scale label, e.g., 99.9. It seems
    '''                            that RtChart takes the negative sign into account separately. </param>
    Public Sub Maximize(ByVal left As Integer, ByVal top As Integer, ByVal width As Integer, ByVal height As Integer, ByVal captionSize As Drawing.SizeF)

        Dim lRTCtop, lRTCleft, lRTCheight, lRTCwidth As Integer
        Dim iBorderWidth As Integer

        Dim iDesiredRightMargin, iDesiredLeftMargin, iDesiredTopMargin, iDesiredBotMargin As Integer

        Dim iPixelsPerDivVert, iPixelsPerDivHorz, iPixelsPerDiv As Integer

        With Me

            ' Set parent control scale mode to Twips.
            ' not supported.

            ' Calculate Border Width:
            iBorderWidth = .BorderWidth

            ' Add inner and outer bevels as necessary.
            If .BevelInner <> RealTimeChart.BevelStyle.None Then
                iBorderWidth += .BevelWidthInner
            End If
            If .BevelOuter <> RealTimeChart.BevelStyle.None Then
                iBorderWidth += .BevelWidthOuter
            End If

            ' Add outline pixel
            If .Outline Then
                iBorderWidth += 1
            End If

            ' Compute the desired left and right margins = height of label + width of scale marks (5 characters)
            iDesiredLeftMargin = CInt(1.5 * captionSize.Height * 15 + captionSize.Width)

            ' Correct for border and bevel.
            iDesiredLeftMargin += iBorderWidth * 15

            ' set desired right margin to same as left one.
            iDesiredRightMargin = iDesiredLeftMargin

            ' Compute the desired top margin
            iDesiredTopMargin = (2 + iBorderWidth) * 15

            ' Compute the minimum bottom margin = height of label and scale
            iDesiredBotMargin = CInt(2.5 * captionSize.Height * 15)

            ' Correct for border and bevel.
            iDesiredBotMargin += iBorderWidth * 15

            ' Compute horizontal div size in pixels.
            If True Then
                iPixelsPerDivHorz = CInt((width - iDesiredLeftMargin - iDesiredRightMargin) / (.MajorDivHorz * 15))


                ' Compute vertical div size in pixels, w/o top label.
                iPixelsPerDivVert = CInt((height - iDesiredBotMargin - iDesiredTopMargin) / (.MajorDivVert * 15))

            Else

                ' !@# change / to \ : This causes larger margins!
                iPixelsPerDivHorz = (width - iDesiredLeftMargin - iDesiredRightMargin) \ (.MajorDivHorz * 15)


                ' Compute vertical div size in pixels, w/o top label.
                iPixelsPerDivVert = (height - iDesiredBotMargin - iDesiredTopMargin) \ (.MajorDivVert * 15)

            End If

            ' Select the smaller of the two values as the number of
            ' pixels per division.
            If iPixelsPerDivVert < iPixelsPerDivHorz Then
                iPixelsPerDiv = iPixelsPerDivVert
            Else
                iPixelsPerDiv = iPixelsPerDivHorz
            End If

            ' Set chart width to max for Horizontal divisions.
            lRTCwidth = .MajorDivHorz * 15 * iPixelsPerDiv + iDesiredLeftMargin + iDesiredRightMargin

            ' Set chart height to max for vertical divisions
            ' w/o top label.
            lRTCheight = .MajorDivVert * 15 * iPixelsPerDiv + iDesiredBotMargin + iDesiredTopMargin

            ' Compute RtChart position (left, top):
            lRTCleft = CInt(left + 0.5 * (width - lRTCwidth))
            lRTCtop = top '+ 0.5 * (vlHeight - lRTCheight)

            ' Set the RtChart margins.
            .MarginTop = iDesiredTopMargin \ 15 - iBorderWidth
            .MarginBottom = iDesiredBotMargin \ 15 - iBorderWidth
            .MarginLeft = iDesiredLeftMargin \ 15 - iBorderWidth
            .MarginRight = iDesiredRightMargin \ 15 - iBorderWidth

            ' Center the chart on its parent control
            .SetBounds(CInt(lRTCleft / 15), CInt(lRTCtop / 15), CInt(lRTCwidth / 15), CInt(lRTCheight / 15))

        End With

    End Sub

    ''' <summary> Maximize old. </summary>
    ''' <remarks>
    ''' RtChart uses isotropic scaling, where each division is exactly a square.  The isotropic
    ''' scaling is achieved by setting equal number if pixels for the vertical and horizontal size of
    ''' each divisions.  Thus, in order to set a desired margin with given divisions, we must find
    ''' the smallest of the two division dimensions in pixels and use this value to set the size of
    ''' both dimensions.<para>
    ''' To fit the chart to the space better, you can adjust the number of divisions.  For
    ''' example:</para><para>
    ''' </para><para>
    '''       divisions = 2 * Samples / Rate
    ''' </para><para>
    ''' The following assumptions were made:</para><para>
    ''' 1.  We assume in this routine that the chart divisions
    '''     were set earlier and that they are given in the
    '''     .MajorDivHorz and .MajorDivVert properties.</para><para>
    ''' 2.  We assume that space is needed for both left, right,
    '''     and bottom scales and labels.</para><para>
    ''' </para>
    ''' Also, because the TextWidth and TextHeight methods do not apply to the chart, set the same
    ''' fonts for the RtChart and its parent control.
    ''' </remarks>
    ''' <param name="left">    The left. </param>
    ''' <param name="top">     The top. </param>
    ''' <param name="width">   The width. </param>
    ''' <param name="height">  The height. </param>
    ''' <param name="caption"> The largest vertical scale label, e.g., 99.9. It seems that RtChart
    '''                        takes the negative sign into account separately. </param>
    Public Sub MaximizeOld(ByVal left As Integer, ByVal top As Integer, ByVal width As Integer, ByVal height As Integer, ByVal caption As String)
        '             is a String expression that specifies the
        '             largest vertical scale label, e.g., 99.9.
        '             It seems that RtChart takes the negative sign
        '             into account separately.
        Me.MaximizeOld(left, top, width, height, MeasureText(Me.Font, caption))
    End Sub

    ''' <summary> Maximize old. </summary>
    ''' <remarks>
    ''' RtChart uses isotropic scaling, where each division is exactly a square.  The isotropic
    ''' scaling is achieved by setting equal number if pixels for the vertical and horizontal size of
    ''' each divisions.  Thus, in order to set a desired margin with given divisions, we must find
    ''' the smallest of the two division dimensions in pixels and use this value to set the size of
    ''' both dimensions.<para>
    ''' To fit the chart to the space better, you can adjust the number of divisions.  For
    ''' example:</para><para>
    ''' </para><para>
    '''       divisions = 2 * Samples / Rate
    ''' </para><para>
    ''' The following assumptions were made:</para><para>
    ''' 1.  We assume in this routine that the chart divisions
    '''     were set earlier and that they are given in the
    '''     .MajorDivHorz and .MajorDivVert properties.</para><para>
    ''' 2.  We assume that space is needed for both left, right,
    '''     and bottom scales and labels.</para><para>
    ''' </para>
    ''' Also, because the TextWidth and TextHeight methods do not apply to the chart, set the same
    ''' fonts for the RtChart and its parent control.
    ''' </remarks>
    ''' <param name="left">        The left. </param>
    ''' <param name="top">         The top. </param>
    ''' <param name="width">       The width. </param>
    ''' <param name="height">      The height. </param>
    ''' <param name="captionSize"> Size of the caption. </param>
    Public Sub MaximizeOld(ByVal left As Integer, ByVal top As Integer, ByVal width As Integer, ByVal height As Integer, ByVal captionSize As Drawing.SizeF)

        Dim iRTCtop, iRTCleft, iRTCheight, iRTCwidth, iBorderWidth As Integer

        Dim iDesiredRightMargin, iDesiredLeftMargin, iDesiredTopMargin, iDesiredBotMargin As Integer

        Dim iPixelsPerDivVert, iPixelsPerDivHorz, iPixelsPerDiv As Integer

        With Me

            ' Set parent control scale mode to Twips.
            ' not supported

            ' Calculate Border Width:
            iBorderWidth = .BorderWidth

            ' Add inner and outer bevels as necessary.
            If .BevelInner <> RealTimeChart.BevelStyle.None Then
                iBorderWidth += .BevelWidthInner
            End If
            If .BevelOuter <> RealTimeChart.BevelStyle.None Then
                iBorderWidth += .BevelWidthOuter
            End If

            ' Add outline pixel
            If .Outline Then
                iBorderWidth += 1
            End If

            ' Compute the desired left and right margins = height of label + width of scale marks (5 characters)
            iDesiredLeftMargin = CInt(1.5 * captionSize.Height * 15 + captionSize.Width)

            ' Correct for border and bevel.
            iDesiredLeftMargin += iBorderWidth * 15

            ' set desired right margin to same as left one.
            iDesiredRightMargin = iDesiredLeftMargin

            ' Computer the desired top margin
            iDesiredTopMargin = (2 + iBorderWidth) * 15

            ' Compute the minimum bottom margin = height of label and scale
            iDesiredBotMargin = CInt(2.5 * captionSize.Height * 15)

            ' Correct for border and bevel.
            iDesiredBotMargin += iBorderWidth * 15

            ' Compute horizontal div size in pixels.
            iPixelsPerDivHorz = CInt((width - iDesiredLeftMargin - iDesiredRightMargin) / (.MajorDivHorz * 15))

            ' Compute vertical div size in pixels, w/o top label.
            iPixelsPerDivVert = CInt((height - iDesiredBotMargin - iDesiredTopMargin) / (.MajorDivVert * 15))

            ' Select the smaller of the two values as the number of
            ' pixels per division.
            If iPixelsPerDivVert < iPixelsPerDivHorz Then
                iPixelsPerDiv = iPixelsPerDivVert
            Else
                iPixelsPerDiv = iPixelsPerDivHorz
            End If

            ' Set chart width to max width for Horizontal divisions.
            iRTCwidth = .MajorDivHorz * 15 * iPixelsPerDiv + iDesiredLeftMargin + iDesiredLeftMargin

            ' Set chart width to max width for vertical divisions w/o top label.
            iRTCheight = .MajorDivVert * 15 * iPixelsPerDiv + iDesiredBotMargin + iDesiredTopMargin

            ' Compute RtChart position (left, top):
            iRTCleft = CInt(left + 0.5 * (width - iRTCwidth))
            iRTCtop = top '+ 0.5 * (viHeight - iRTCheight)

            ' Set the RtChart margins.
            .MarginTop = iDesiredTopMargin \ 15 - iBorderWidth
            .MarginBottom = iDesiredBotMargin \ 15 - iBorderWidth
            .MarginLeft = iDesiredLeftMargin \ 15 - iBorderWidth
            .MarginRight = iDesiredLeftMargin \ 15 - iBorderWidth

            ' Center the chart on its parent control
            .SetBounds(CInt(iRTCleft / 15), CInt(iRTCtop / 15), CInt(iRTCwidth / 15), CInt(iRTCheight / 15))

        End With

    End Sub

    ''' <summary> Define border. </summary>
    Public Sub DefineBorder()

        With Me

            ' Border description properties

            ' Set border RGB color
            .BorderColor = Drawing.Color.LightGray

            ' Set light RGB color
            .LightColor = Drawing.Color.White ' LIGHTGRAY ' DARKGRAY

            ' Set shadow RGB color
            .ShadowColor = Drawing.Color.DarkGray ' BLACK

            ' Enable inner raised bevel (Disable=RtChart_None)
            .BevelWidthInner = 2
            .BevelInner = RealTimeChart.BevelStyle.Inset
            '   .BevelInner = RtChart_None

            ' Enable outer raised bevel (Disable=RtChart_None)
            .BevelWidthOuter = 2
            .BevelOuter = RealTimeChart.BevelStyle.Raised
            '   .BevelOuter = RtChart_None

            .BorderWidth = 2

            .Outline = True

            .OutlineColor = Drawing.Color.Black

            .RoundedCorners = True ' False

        End With

    End Sub

    ''' <summary> Applies the channel (trace) offsets. </summary>
    ''' <param name="channel">        The channel. </param>
    ''' <param name="abscissaOffset"> The abscissa offset. </param>
    ''' <param name="ordinateOffset"> The ordinate offset. </param>
    Public Sub ApplyChannelOffsets(ByVal channel As Integer, ByVal abscissaOffset As Single, ByVal ordinateOffset As Single)

        With Me

            ' select channel
            .LogicalChannel = channel

            ' Set Y offset
            .ChnDspOffsetY = ordinateOffset

            ' Set X offset
            .ChnDspOffsetX = abscissaOffset

            ' update channel position
            .ChnDspAction = RealTimeChart.ChannelDisplayAction.Translate

        End With

    End Sub

    ''' <summary> Applies the global operations; image file, hit test as false and frames per second as zero. </summary>
    Public Sub ApplyGlobalOperations(Optional imageFile As String = "", Optional hitTest As Boolean = False, Optional framesPerSec As Integer = 0)

        '   This method sets global chart operations
        ' Set Global operation properties:
        With Me

            ' Set file name for Write action
            .ImageFile = imageFile

            ' Disable mouse pointer "hit" detection
            .HitTest = hitTest

            ' Set asynchronous (0) operation (-1 for synchronous)
            .FramesPerSec = framesPerSec

        End With

    End Sub

    ''' <summary> Define graticule. </summary>
    ''' <param name="majorAbscissaDivisions"> The major abscissa divisions. </param>
    ''' <param name="majorOrdinateDivisions"> The major ordinate divisions. </param>
    ''' <param name="minorDivisions">         The minor divisions. </param>
    Public Sub DefineGraticule(ByVal majorAbscissaDivisions As Integer, ByVal majorOrdinateDivisions As Integer, ByVal minorDivisions As Integer)

        With Me

            ' Set Ariel Font for label and scales.
            .FontName = "Arial"
            .FontBold = False
            .FontSize = 8.5

            ' Set parent font identical for sizing.
            ' not supported for now...

            ' Set background RGB color
            .BackColor = Drawing.Color.DarkCyan

            ' Allow non-square border.  Grid is always square
            .AutoSize = False

            ' Turn frame on.
            .FrameOn = True
            .FrameTics = RealTimeChart.FrameTicsStyle.MajorMinorFrameTics
            .FrameColor = Drawing.Color.White

            ' Turn both axis on.
            .AxesType = RealTimeChart.AxesType.HorizontalVertical
            .AxesTics = RealTimeChart.AxesTicsStyle.MajorMinorAxesTics
            .AxesColor = Drawing.Color.White

            ' Turn both grids on.
            .GridOn = True
            .GridType = RealTimeChart.GridType.HorizontalVertical
            .GridColor = Drawing.Color.LightGray

            ' Set Chart Graticule divisions.
            ' major horizontal divisions
            .MajorDivHorz = majorAbscissaDivisions

            ' Major vertical divisions
            .MajorDivVert = majorOrdinateDivisions

            ' Major minor divisions
            .MinorDiv = minorDivisions

            ' Set minimum Margins
            .MarginTop = 1
            .MarginBottom = 1
            .MarginLeft = 1
            .MarginRight = 1

            ' 256 PALLETTE TYPE ADAPTER ONLY wINDOWS_g
            .ColorDepth = RealTimeChart.ColorDepth.Color16

        End With

    End Sub

    ''' <summary> Define horizontal scale. </summary>
    ''' <param name="viewport">    The viewport. </param>
    ''' <param name="window">      The window. </param>
    ''' <param name="decimals">    The number of digits to the right of the decimal point for the scale labels. </param>
    ''' <param name="location">    The location. </param>
    ''' <param name="position">    The position. </param>
    ''' <param name="scaleFactor"> The scale factor. </param>
    ''' <param name="caption">     The caption. </param>
    Public Sub DefineHorizontalScale(ByVal viewport As Integer, ByVal window As Integer, ByVal decimals As Integer,
                             ByVal location As ScaleLocation, ByVal position As Position, ByVal scaleFactor As Single, ByVal caption As String)

        ' Set RtChart Scale properties:
        With Me

            ' select scale name (same for both dimensions NOT TRUE!)
            .LogicalScale = 2 * window - 1

            ' Exit if De-Select (iWnd < 0)
            If window <= 0 Then Exit Sub

            ' select horizontal scale/label
            .ScaleDimension = RealTimeChart.LogicalDimension.Horizontal

            ' specify viewport
            .ScaleViewport = viewport

            ' specify window to be scaled
            .ScaleWindow = window

            ' specify pen color for scale
            .ScalePen = RealTimeChart.PenName.White

            ' Scale Location: axis or frame
            .ScaleLocation = location

            ' Scale Position: above or below ScaleLocation
            .ScalePosition = position

            ' Scale Orientation: Parallel or Perpendicular to ScaleDimension
            .ScaleOrientation = RealTimeChart.Orientation.Parallel

            ' Scale Path: direction which characters follow: left. right, up, down
            .ScalePath = RealTimeChart.Path.Right

            ' Scale Gap: space between ScaleLocation and scale/label in pixels
            ' Default = 1 pixel.
            '.ScaleGap = 4

            ' multiplier used to convert window units to scale values
            ' Default is 1.
            .ScaleFactor = scaleFactor

            ' number of significant digits to the right of decimal point
            ' Default = 0
            .ScalePlaces = decimals

            ' scale visibility: none, scale, label, or both
            .ScaleVisibility = RealTimeChart.ScaleVisibility.ScaleAndLabel

            '  Set RtChart Label properties

            ' specify Label Caption text: "volts", "secs" , etc.
            .LabelCaption = caption

            ' specify pen color for Label
            .LabelPen = RealTimeChart.PenName.White

            ' label location relative to scale: left, right, center
            .LabelLocation = RealTimeChart.LabelLocation.Center

            ' label Position: above, below, or on LabelLocation
            .LabelPosition = position ' RtChart_OnScale

            ' label character orientation relative to ScaleDimension
            .LabelOrientation = RealTimeChart.Orientation.Parallel

            ' Scale Path: direction which characters follow
            .LabelPath = RealTimeChart.Path.Right

            ' Select vertical scale/label
            '   .ScaleDimension = RtChart_Vert

            ' Set visibility to none!
            '   .ScaleVisibility = RtChart_NONE

            ' Erase current and draw new scale and label
            .ScaleAction = RealTimeChart.ScaleAction.Initialize

        End With


    End Sub

    ''' <summary> Define vertical scale and label format. </summary>
    ''' <param name="viewport">    The viewport. </param>
    ''' <param name="window">      The window. </param>
    ''' <param name="decimals">    The number of digits to the right of the decimal point for the scale labels. </param>
    ''' <param name="location">    The location. </param>
    ''' <param name="position">    The position. </param>
    ''' <param name="scaleFactor"> The scale factor. </param>
    ''' <param name="caption">     The caption. </param>
    Public Sub DefineVertScale(ByVal viewport As Integer, ByVal window As Integer, ByVal decimals As Integer,
                        ByVal location As ScaleLocation, ByVal position As Position, ByVal scaleFactor As Single, ByVal caption As String)

        ' Set RtChart Scale properties:
        With Me

            ' select scale name.
            ' BUG: Should be the same for both dimensions but
            ' if set the same we cannot set horizontal and
            ' vertical labels in different orientations!
            .LogicalScale = 2 * window

            ' select vertical scale/label
            .ScaleDimension = RealTimeChart.LogicalDimension.Vertical

            ' specify viewport
            .ScaleViewport = viewport

            ' specify window to be scaled
            .ScaleWindow = window

            ' specify pen color for scale
            .ScalePen = RealTimeChart.PenName.White

            ' Scale Location: axis or frame
            .ScaleLocation = location

            ' Scale Position: above or below ScaleLocation
            .ScalePosition = position

            ' Scale Orientation: Parallel or Perpendicular to ScaleDimension
            .ScaleOrientation = RealTimeChart.Orientation.Perpendicular

            ' Scale Path: direction which characters follow: left. right, up, down
            .ScalePath = RealTimeChart.Path.Right

            ' Scale Gap: space between ScaleLocation and scale/label in pixels
            ' Default = 1 pixel.
            .ScaleGap = 0

            ' multiplier used to convert window units to scale values
            ' Default is 1.
            .ScaleFactor = scaleFactor

            ' number of significant digits to the right of decimal point
            ' Default = 0
            .ScalePlaces = decimals

            ' scale visibility: none, scale, label, or both
            .ScaleVisibility = RealTimeChart.ScaleVisibility.ScaleAndLabel

            ' Set RtChart Label properties

            ' select vertical scale/label
            .ScaleDimension = RealTimeChart.LogicalDimension.Vertical

            ' specify Label Caption text: "volts", "secs" , etc.
            .LabelCaption = caption

            ' specify pen color for Label
            .LabelPen = window '  Match color to graph

            ' label location relative to scale: left, right, center
            .LabelLocation = RealTimeChart.LabelLocation.Center

            ' label character orientation relative to ScaleDimension
            .LabelOrientation = RealTimeChart.Orientation.Parallel

            ' label Position: above, below, or on LabelLocation
            .LabelPosition = position ' RtChart_OnScale

            ' Scale Path: direction which characters follow
            .LabelPath = RealTimeChart.Path.Right

            ' Select horizontal scale/label
            '   .ScaleDimension = RtChart_HORZ

            ' Set visibility to none!
            '   .ScaleVisibility = RtChart_NONE

            ' Draw the scale and label
            .ScaleAction = RealTimeChart.ScaleAction.Initialize

        End With

    End Sub

    ''' <summary> Define window. </summary>
    ''' <param name="window">         The window. </param>
    ''' <param name="abscissaOrigin"> The abscissa origin. </param>
    ''' <param name="abscissaWidth">  Width of the abscissa. </param>
    ''' <param name="ordinateOrigin"> The ordinate origin. </param>
    ''' <param name="ordinateHeight"> Height of the ordinate. </param>
    Public Sub DefineWindow(ByVal window As Integer, ByVal abscissaOrigin As Single, ByVal abscissaWidth As Single, ByVal ordinateOrigin As Single, ByVal ordinateHeight As Single)

        ' Window Description
        With Me
            .LogicalWindow = window ' select window
            .WndXmin = abscissaOrigin ' left minimum abscissa
            .WndWidth = abscissaWidth ' Width
            .WndYmin = ordinateOrigin ' Bottom
            .WndHeight = ordinateHeight ' height
        End With

    End Sub

    ''' <summary> Define window height. </summary>
    ''' <param name="window">           The window. </param>
    ''' <param name="unitsPerDivision"> The units per division. </param>
    Public Sub DefineWindowHeight(ByVal window As Integer, ByVal unitsPerDivision As Single)

        Dim height As Single
        With Me

            ' Set height in Amp.Units=Units/div * NumDivisions
            height = unitsPerDivision * .MajorDivVert

            ' select window
            .LogicalWindow = window

            ' Set height
            .WndHeight = height

            ' bottom minimum ordinate
            .WndYmin = CSng(-0.5 * height)

            .ChartAction = RealTimeChart.ChartAction.Rescale

        End With

    End Sub

    ''' <summary> Define window width. </summary>
    ''' <param name="window">           The window. </param>
    ''' <param name="unitsPerDivision"> The units per division. </param>
    Public Sub DefineWindowWidth(ByVal window As Integer, ByVal unitsPerDivision As Single)

        With Me

            ' select window
            .LogicalWindow = window

            ' window width in number of samples
            .WndWidth = unitsPerDivision * .MajorDivHorz

            ' left minimum abscissa
            .WndXmin = 0

            ' Rescale the window
            .ChartAction = RealTimeChart.ChartAction.Rescale
        End With

    End Sub

    ''' <summary> Zero channels (traces) offsets. </summary>
    ''' <param name="firstChannel"> The first channel (trace). </param>
    ''' <param name="lastChannel"> The last channel (trace). </param>
    Public Sub ZeroChannelsOffsets(ByVal firstChannel As Integer, ByVal lastChannel As Integer)

        ' prevent each channel from being re-painted individually
        ' on Translate Action
        Me.DisableCharting()

        For channel As Integer = firstChannel To lastChannel

            Me.ApplyChannelOffsets(channel, 0.0!, 0.0!)

        Next channel

        ' paint entire window with new channel positions for all channels
        Me.EnableCharting()

    End Sub

    ''' <summary> Returns the Chart error message. </summary>
    ''' <param name="errorCode"> The error code. </param>
    ''' <returns> A String. </returns>
    Public Shared Function ChartErrorMessage(ByVal errorCode As Integer) As String
        Return RealTimeChart.ChartPen.ChartErrorMessage(errorCode)
    End Function

End Class

<CodeAnalysis.SuppressMessage("Performance", "CA1815:Override equals and operator equals on value types", Justification:="<Pending>")>
Public Structure ChannelScaleInfo
    Public Property OrdinateUnitsPerDivision As Single
    Public Property AbscissaUnitsPerDivision As Single
    Public Property OrdinateOffset As Single
    Public Property AbscissaOffset As Single
End Structure

