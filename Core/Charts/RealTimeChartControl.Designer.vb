﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class RealTimeChartControl

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(RealTimeChartControl))
        Me._RealTimeChart = New AxLortchtxLib.AxSST_RtChart()
        CType(Me._RealTimeChart, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_RealTimeChart
        '
        Me._RealTimeChart.Dock = System.Windows.Forms.DockStyle.Fill
        Me._RealTimeChart.Location = New System.Drawing.Point(0, 0)
        Me._RealTimeChart.Name = "_RealTimeChart"
        Me._RealTimeChart.OcxState = CType(resources.GetObject("_RealTimeChart.OcxState"), System.Windows.Forms.AxHost.State)
        Me._RealTimeChart.Size = New System.Drawing.Size(764, 347)
        Me._RealTimeChart.TabIndex = 0
        '
        'RealTimeChartParentControl
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._RealTimeChart)
        Me.Name = "RealTimeChartParentControl"
        Me.Size = New System.Drawing.Size(764, 347)
        CType(Me._RealTimeChart, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Private WithEvents _RealTimeChart As AxLortchtxLib.AxSST_RtChart
End Class
