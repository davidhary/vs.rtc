' file:		PenColor.vb
' LabOBJX Real-Time Chart Example Code for Pens
' (C) Copyright 1995, Scientific Software Tools, Inc.
' All Rights Reserved.
Option Strict Off
Option Explicit On
Imports System.ComponentModel
Imports System.Drawing
Imports isr.Core.EnumExtensions
Namespace ChartPen

    Public Module Methods

        ''' <summary>
        ''' Initializes the channels pens. Describes pen characteristics. uses channel values for pen
        ''' names. each pen name has the same value as its assigned channel.
        ''' </summary>
        ''' <param name="chart"> The chart. </param>
        Public Sub InitChannelsPens(ByVal chart As RealTimeChartControl)
            If chart Is Nothing Then Throw New ArgumentNullException(NameOf(chart))

            ' Pen for CHANNEL1
            chart.LogicalPen = ChannelName.Channel1
            chart.PenColor = Color.Red ' Drawing.Color.FromArgb(RED)
            chart.PenIntenseColor = Color.Yellow '  Drawing.Color.FromArgb(YELLOW)
            chart.PenWidth = 1
            chart.PenStyle = (PenStyle.SolidPen)

            ' Pen for CHANNEL2
            chart.LogicalPen = ChannelName.Channel2
            chart.PenColor = Color.White ' Drawing.Color.FromArgb(WHITE)
            chart.PenIntenseColor = Color.Yellow '  Drawing.Color.FromArgb(YELLOW)
            chart.PenWidth = 1
            chart.PenStyle = ((PenStyle.SolidPen))

            ' Pen for CHANNEL3
            chart.LogicalPen = ChannelName.Channel3
            chart.PenColor = Color.Blue ' Drawing.Color.FromArgb(BLUE)
            chart.PenIntenseColor = Color.Yellow '  Drawing.Color.FromArgb(YELLOW)
            chart.PenWidth = 1
            chart.PenStyle = ((PenStyle.SolidPen))

            ' Pen for CHANNEL4
            chart.LogicalPen = ChannelName.Channel4
            chart.PenColor = Color.Black ' Drawing.Color.FromArgb(BLACK)
            chart.PenIntenseColor = Color.Yellow '  Drawing.Color.FromArgb(YELLOW)
            chart.PenWidth = 1
            chart.PenStyle = ((PenStyle.SolidPen))

            ' Pen for XY CHANNEL
            chart.LogicalPen = ChannelName.XYChannel
            chart.PenColor = Color.Green ' Drawing.Color.FromArgb(GREEN)
            chart.PenIntenseColor = Color.Yellow '  Drawing.Color.FromArgb(YELLOW)
            chart.PenWidth = 1
            chart.PenStyle = ((PenStyle.SolidPen))

            ' Pen for cursor channels
            chart.LogicalPen = ChannelName.Cursor1
            chart.PenColor = Color.Yellow ' Drawing.Color.FromArgb(YELLOW)
            chart.PenIntenseColor = Color.White ' Drawing.Color.FromArgb(WHITE)
            chart.PenWidth = 1
            chart.PenStyle = ((PenStyle.SolidPen))

        End Sub

        ''' <summary>
        ''' Initializes the pens. Describes pen characteristics for 20 pre-defined and custom pens. get
        ''' RGB values from PenColorArray for each pen.
        ''' </summary>
        ''' <param name="chart"> The chart. </param>
        Public Sub InitializePens(ByVal chart As RealTimeChartControl)
            If chart Is Nothing Then Throw New ArgumentNullException(NameOf(chart))
            For Pen As Integer = PenName.First To PenName.Last
                chart.LogicalPen = (Pen)
                chart.PenColor = Drawing.Color.FromArgb(PenColorArray(Pen).RGBValue) ' RGB color value
                chart.PenWidth = 1
                chart.PenStyle = ((PenStyle.SolidPen))
            Next Pen
        End Sub

        Private Class PenItemNameValueCollection
            Inherits ObjectModel.KeyedCollection(Of Integer, ItemNameValue)
            Protected Overrides Function GetKeyForItem(item As ItemNameValue) As Integer
                Return item.ListIndex
            End Function
        End Class

        ''' <summary> Array of pen colors. </summary>
        Private _PenColorArray As PenItemNameValueCollection

        ''' <summary> Pen color array. </summary>
        ''' <param name="pen"> The pen. </param>
        ''' <returns> An ItemNameValue. </returns>
        ''' <remarks>  
        ''' <list type="bullet">this array provides the means to relate the text pen name with the RGB value for each pen, it contains:<item>
        ''' ItemName - the text name of the pen to be listed in the combo box</item><item>
        ''' Color    - the pen color.</item><item>
        ''' RGBValue - the RGB value used to specify the color of the pen</item><item>
        ''' ListIndex - controls the list ordering in the combo box list</item></list>
        ''' </remarks>
        Public Function PenColorArray(ByVal pen As Integer) As ItemNameValue
            If Methods._PenColorArray Is Nothing Then
                Dim darkYellow As Color = Color.FromArgb(0, &HC0, &HC0)
                Methods._PenColorArray = New PenItemNameValueCollection From {
                    New ItemNameValue(Color.Black, PenName.Black.Description, PenName.Black),
                    New ItemNameValue(Color.White, PenName.White.Description, PenName.White),
                    New ItemNameValue(Color.DarkGray, PenName.DarkGray.Description, PenName.DarkGray),
                    New ItemNameValue(Color.LightGray, PenName.LightGray.Description, PenName.LightGray),
                    New ItemNameValue(Color.Red, PenName.Red.Description, PenName.Red),
                    New ItemNameValue(Color.DarkRed, PenName.DarkRed.Description, PenName.DarkRed),
                    New ItemNameValue(Color.LightCoral, PenName.LightRed.Description, PenName.LightRed),
                    New ItemNameValue(Color.Green, PenName.Green.Description, PenName.Green),
                    New ItemNameValue(Color.DarkGreen, PenName.DarkGreen.Description, PenName.DarkGreen),
                    New ItemNameValue(Color.LightGreen, PenName.LightGreen.Description, PenName.LightGreen),
                    New ItemNameValue(Color.Blue, PenName.Blue.Description, PenName.Blue),
                    New ItemNameValue(Color.DarkBlue, PenName.DarkBlue.Description, PenName.DarkBlue),
                    New ItemNameValue(Color.LightBlue, PenName.LightBlue.Description, PenName.LightBlue),
                    New ItemNameValue(Color.Yellow, PenName.Yellow.Description, PenName.Yellow),
                    New ItemNameValue(darkYellow, PenName.DarkYellow.Description, PenName.DarkYellow),
                    New ItemNameValue(Color.LightYellow, PenName.LightYellow.Description, PenName.LightYellow),
                    New ItemNameValue(Color.Cyan, PenName.Cyan.Description, PenName.Cyan),
                    New ItemNameValue(Color.DarkCyan, PenName.DarkCyan.Description, PenName.DarkCyan),
                    New ItemNameValue(Color.Magenta, PenName.Magenta.Description, PenName.Magenta),
                    New ItemNameValue(Color.DarkMagenta, PenName.DarkMagenta.Description, PenName.DarkMagenta)
                }
            End If
            Return _PenColorArray(pen)
        End Function

        Private _ErrorMessageDictionary As Dictionary(Of Integer, String) ' declare ChartData error array here

        ''' <summary> Returns the chart error message associated with the error code. </summary>
        ''' <param name="errorCode"> The error code. </param>
        ''' <returns> A String. </returns>
        Public Function ChartErrorMessage(ByVal errorCode As Integer) As String
            If Methods._ErrorMessageDictionary Is Nothing Then
                Methods._ErrorMessageDictionary = New Dictionary(Of Integer, String)
                For Each ec As isr.Visuals.RealTimeChart.ErrorCode In [Enum].GetValues(GetType(isr.Visuals.RealTimeChart.ErrorCode))
                    Methods._ErrorMessageDictionary.Add(-CInt(ec), ec.Description)
                Next
            End If
            Return Methods._ErrorMessageDictionary(Math.Abs(errorCode))
        End Function

    End Module

    ''' <summary> Defines a data type to associate pen nume (number) with the pen name and color. </summary>
    <CodeAnalysis.SuppressMessage("Performance", "CA1815:Override equals and operator equals on value types", Justification:="<Pending>")>
    Public Structure ItemNameValue
        Public Sub New(ByVal color As Color, ByVal itemName As String, ByVal itemIndex As Integer)
            Me.Color = color
            Me.ItemName = itemName
            Me.ListIndex = itemIndex
        End Sub
        Public Property ItemName As String ' combo box list item string
        Public Property ListIndex As Short ' combo box index for list ordering
        Public Property Color As Color
        ' RGB color value used by RtChart control
        Public ReadOnly Property RGBValue As Integer
            Get
                Return Color.ToArgb
            End Get
        End Property
        Public Shared Function CreateInstance() As ItemNameValue
            Dim result As New ItemNameValue With {
                    .ItemName = String.Empty
                }
            Return result
        End Function
    End Structure

End Namespace

''' <summary> Values that represent pen names. </summary>
<CodeAnalysis.SuppressMessage("Design", "CA1027:Mark enums with FlagsAttribute", Justification:="<Pending>")>
Public Enum PenName
    <Description("Not specified")> None = 0
    <Description("Black")> Black = 1
    <Description("White ")> White = 2
    <Description("Dark gray")> DarkGray = 3
    <Description("Light gray")> LightGray = 4
    <Description("Red")> Red = 5
    <Description("Dark red")> DarkRed = 6
    <Description("Light red")> LightRed = 7
    <Description("Green")> Green = 8
    <Description("Dark green")> DarkGreen = 9
    <Description("Light green")> LightGreen = 10
    <Description("Blue")> Blue = 11
    <Description("Dark blue")> DarkBlue = 12
    <Description("Light blue")> LightBlue = 13
    <Description("Yellow")> Yellow = 14
    <Description("Dark Yellow")> DarkYellow = 15
    <Description("Light Yellow")> LightYellow = 16
    <Description("Cyan")> Cyan = 17
    <Description("Dark cyan")> DarkCyan = 18
    <Description("Magenta")> Magenta = 19
    <Description("Dark Magenta")> DarkMagenta = 20
    <Description("First pen")> First = Black
    <Description("Last pen")> Last = DarkMagenta
End Enum

''' <summary> Values that represent channel and cursor names. </summary>
<CodeAnalysis.SuppressMessage("Design", "CA1027:Mark enums with FlagsAttribute", Justification:="<Pending>")>
Public Enum ChannelName
    <Description("Not specified")> None = 0
    <Description("Channel 1")> Channel1 = 1
    <Description("Channel 2")> Channel2 = 2
    <Description("Channel 3")> Channel3 = 3
    <Description("Channel 4")> Channel4 = 4

    <Description("Cursor 1")> Cursor1 = 5
    <Description("Cursor 2")> Cursor2 = 6
    <Description("Cursor 3")> Cursor3 = 7
    <Description("Cursor 4")> Cursor4 = 8

    ' channel name constant for x vs y channel
    <Description("XY Channel")> XYChannel = 9
    <Description("Last channel")> LastChannel = ChannelName.Channel4
    <Description("Last Cursor")> LastCursor = Cursor4
End Enum

