Imports System.ComponentModel

''' <summary> Values that represent axes tics styles. </summary>
Public Enum AxesTicsStyle
    <Description("Major and minor axes tics")> MajorMinorAxesTics = LortchtxLib.AxesTics.RtChart_MajorMinorAxesTics
    <Description("Major axes tics")> MajorAxesTics = LortchtxLib.AxesTics.RtChart_MajorAxesTics
    <Description("No axes tics")> NoAxesTics = LortchtxLib.AxesTics.RtChart_NoAxesTics
End Enum

''' <summary> Values that represent axes types. </summary>
Public Enum AxesType
    <Description("Horizontal and vertical axes")> HorizontalVertical = LortchtxLib.AxesTypes.RtChart_HorzVertAxes
    <Description("Horizontal axis")> Horizontal = LortchtxLib.AxesTypes.RtChart_HorzAxis
    <Description("Vertical axis")> Vertical = LortchtxLib.AxesTypes.RtChart_VertAxis
    <Description("No axes")> NoAxes = LortchtxLib.AxesTypes.RtChart_NoAxes
End Enum

''' <summary> Values that represent bevel styles. </summary>
Public Enum BevelStyle
    <Description("None")> None = LortchtxLib.BevelStyles.RtChart_None
    <Description("Inset")> Inset = LortchtxLib.BevelStyles.RtChart_Inset
    <Description("Raised")> Raised = LortchtxLib.BevelStyles.RtChart_Raised
End Enum

''' <summary> Values that represent the logical dimension. </summary>
Public Enum LogicalDimension
    <Description("Horizontal")> Horizontal = LortchtxLib.Dims.RtChart_Horz
    <Description("Vertical")> Vertical = LortchtxLib.Dims.RtChart_Vert
End Enum

''' <summary> Values that represent color depths. </summary>
Public Enum ColorDepth
    <Description("Color Default")> ColorDefault = LortchtxLib.ColorDepth.RtChart_ColorDefault
    <Description("Color 16")> Color16 = LortchtxLib.ColorDepth.RtChart_Color16
    <Description("Color 256")> Color256 = LortchtxLib.ColorDepth.RtChart_Color256
End Enum

''' <summary> Values that represent frame tics styles. </summary>
Public Enum FrameTicsStyle
    <Description("Major and minor Frame tics")> MajorMinorFrameTics = LortchtxLib.FrameTics.RtChart_MajorMinorFrameTics
    <Description("Major Frame tics")> MajorFrameTics = LortchtxLib.FrameTics.RtChart_MajorFrameTics
    <Description("No Frame tics")> NoFrameTics = LortchtxLib.FrameTics.RtChart_NoFrameTics
End Enum

''' <summary> Values that represent grid symmetries. </summary>
Public Enum GridSymmetry
    <Description("Isotropic Grid")> IsotropicGrid = LortchtxLib.GridSymmetry.RtChart_IsotropicGrid
    <Description("Anisotropic Grid")> AnisotropicGrid = LortchtxLib.GridSymmetry.RtChart_AnisotropicGrid
End Enum

''' <summary> Values that represent grid types. </summary>
Public Enum GridType
    <Description("Horizontal and vertical Grid")> HorizontalVertical = LortchtxLib.GridTypes.RtChart_HorzVertGrid
    <Description("Horizontal axis")> Horizontal = LortchtxLib.GridTypes.RtChart_HorzGrid
    <Description("Vertical axis")> Vertical = LortchtxLib.GridTypes.RtChart_VertGrid
    <Description("No Grid")> NoGrid = LortchtxLib.GridTypes.RtChart_NoGrid
End Enum

''' <summary> Values that represent pen styles. </summary>
Public Enum PenStyle
    <Description("Solid Pen")> SolidPen = LortchtxLib.PenStyles.RtChart_SolidPen
    <Description("Dash Pen")> DashPen = LortchtxLib.PenStyles.RtChart_DashPen
    <Description("Dot Pen")> DotPen = LortchtxLib.PenStyles.RtChart_DotPen
    <Description("Dash Dot Pen")> DashDotPen = LortchtxLib.PenStyles.RtChart_DashDotPen
    <Description("Dash Dot+Dot Pen")> DashDotDotPen = LortchtxLib.PenStyles.RtChart_DashDotDotPen
End Enum

''' <summary> Values that represent chart actions. </summary>
Public Enum ChartAction
    <Description("Rescale")> Rescale = LortchtxLib.ChartActions.RtChart_Rescale
    <Description("Copy To Clipboard")> CopyToClipboard = LortchtxLib.ChartActions.RtChart_CopyToClipboard
    <Description("Append To Clipboard")> AppendToClipboard = LortchtxLib.ChartActions.RtChart_AppendToClipboard
    <Description("Print")> Print = LortchtxLib.ChartActions.RtChart_Print
    <Description("Write To Disk")> WriteToDisk = LortchtxLib.ChartActions.RtChart_WriteToDisk
    ''' <summary> prevent each channel from being erased individually. </summary>
    <Description("Disable Paint")> DisablePaint = LortchtxLib.ChartActions.RtChart_DisablePaint
    ' paint entire window to erase all channels
    <Description("Enable Paint")> EnablePaint = LortchtxLib.ChartActions.RtChart_EnablePaint
End Enum

''' <summary> Values that represent channel data shapes. </summary>
Public Enum ChannelDataShape
    <Description("Scalar")> Scalar = LortchtxLib.ChnDataShapes.RtChart_Scalar
    <Description("Pair")> Pair = LortchtxLib.ChnDataShapes.RtChart_Pair
    <Description("AutoIncr")> AutoIncr = LortchtxLib.ChnDataShapes.RtChart_AutoIncr
End Enum

''' <summary> Values that represent channel data types. </summary>
<CodeAnalysis.SuppressMessage("Design", "CA1027:Mark enums with FlagsAttribute", Justification:="<Pending>")>
Public Enum ChannelDataType
    <Description("Unsigned Integer 8")> UnsignedInteger8 = LortchtxLib.ChnDataTypes.RtChart_Unsigned_8_int ' 0
    <Description("Signed integer 8")> SignedInteger8 = LortchtxLib.ChnDataTypes.RtChart_Signed_8_int ' 1
    <Description("Unsigned Integer 16")> UnsignedInteger16 = LortchtxLib.ChnDataTypes.RtChart_Unsigned_16_int ' 2
    <Description("Signed integer 16")> SignedInt16 = LortchtxLib.ChnDataTypes.RtChart_Signed_16_int ' 3
    <Description("Integer")> [Integer] = LortchtxLib.ChnDataTypes.RtChart_Integer ' 3
    <Description("Unsigned Integer 32")> UnsignedInteger32 = LortchtxLib.ChnDataTypes.RtChart_Unsigned_32_int ' 4
    <Description("Signed Integer 32")> SignedInteger32 = LortchtxLib.ChnDataTypes.RtChart_Signed_32_int ' 5
    <Description("Float 32")> Float32 = LortchtxLib.ChnDataTypes.RtChart_Float_32 ' 6
    <Description("Single")> [Single] = LortchtxLib.ChnDataTypes.RtChart_Single ' 6
    <Description("Double")> [Double] = LortchtxLib.ChnDataTypes.RtChart_Double ' 7
    <Description("Float 64")> Float64 = LortchtxLib.ChnDataTypes.RtChart_Float_64 ' 7
    <Description("Long")> [Long] = LortchtxLib.ChnDataTypes.RtChart_Long ' 5
End Enum

''' <summary> Values that represent channel display actions. </summary>
Public Enum ChannelDisplayAction
    <Description("No Action")> NoAction = LortchtxLib.ChnDspActions.RtChart_ChnNoAction
    <Description("Initialize")> Initialize = LortchtxLib.ChnDspActions.RtChart_ChnInitialize
    ''' <summary>  use channel erase. </summary>
    <Description("Erase")> [Erase] = LortchtxLib.ChnDspActions.RtChart_ChnErase
    <Description("Intense Color")> IntenseColor = LortchtxLib.ChnDspActions.RtChart_ChnIntenseColor
    <Description("Normal Color")> NormalColor = LortchtxLib.ChnDspActions.RtChart_ChnNormalColor
    <Description("Change Pen")> ChangePen = LortchtxLib.ChnDspActions.RtChart_ChnChangePen
    <Description("Change Viewport")> ChangeViewport = LortchtxLib.ChnDspActions.RtChart_ChnChangeViewport
    <Description("Change Window")> ChangeWindow = LortchtxLib.ChnDspActions.RtChart_ChnChangeWindow
    <Description("Flush")> Flush = LortchtxLib.ChnDspActions.RtChart_ChnFlush

    ''' <summary> update channel position </summary>
    <Description("Translate")> Translate = LortchtxLib.ChnDspActions.RtChart_ChnTranslate
End Enum

Public Enum ChannelDisplayMode
    <Description("CRT")> CRT = LortchtxLib.ChnDspModes.RtChart_CRT
    <Description("Scroll Right")> ScrollRight = LortchtxLib.ChnDspModes.RtChart_ScrollRight
End Enum

''' <summary> Values that represent channel display styles. </summary>
Public Enum ChannelDisplayStyle
    <Description("Points")> Points = LortchtxLib.ChnDspStyles.RtChart_Points
    <Description("Lines")> Lines = LortchtxLib.ChnDspStyles.RtChart_Lines
    <Description("Area")> Area = LortchtxLib.ChnDspStyles.RtChart_Area
End Enum

''' <summary> Values that represent Label Locations. </summary>
Public Enum LabelLocation
    <Description("Left Bottom")> LeftBottom = LortchtxLib.LabelLocations.RtChart_LeftBottom ' 0
    <Description("Center")> Center = LortchtxLib.LabelLocations.RtChart_Center ' 1
    <Description("Right Top")> RightTop = LortchtxLib.LabelLocations.RtChart_RightTop ' 2
End Enum


''' <summary> Values that represent Scale actions. </summary>
Public Enum ScaleAction
    <Description("No Action")> NoAction = LortchtxLib.ScaleActions.RtChart_ScaleNoAction ' 0

    ''' <summary> execute selected Scale Action. </summary>
    <Description("Initialize")> Initialize = LortchtxLib.ScaleActions.RtChart_ScaleInitialize ' 1
    <Description("Erase")> [Erase] = LortchtxLib.ScaleActions.RtChart_ScaleErase ' 2
    <Description("Intense Color")> IntenseColor = LortchtxLib.ScaleActions.RtChart_ScaleIntenseColor ' 3
    <Description("Normal Color")> NormalColor = LortchtxLib.ScaleActions.RtChart_ScaleNormalColor ' 4
    <Description("Change Pen")> ChangePane = LortchtxLib.ScaleActions.RtChart_ScaleChangePen ' 5
    <Description("Change Viewport")> ChangeViewport = LortchtxLib.ScaleActions.RtChart_ScaleChangeViewport ' 6
    <Description("Change Window")> ChangeWindow = LortchtxLib.ScaleActions.RtChart_ScaleChangeWindow ' 7
End Enum

''' <summary> Values that represent Scale Locations. </summary>
Public Enum ScaleLocation
    <Description("Axis")> Axis = LortchtxLib.ScaleLocations.RtChart_Axis ' 0
    <Description("Left Top")> LeftTop = LortchtxLib.ScaleLocations.RtChart_FrameLeftTop ' 1
    <Description("Right Bottom")> RightBottom = LortchtxLib.ScaleLocations.RtChart_FrameRightBottom ' 2
End Enum

''' <summary> Values that represent orientations. </summary>
Public Enum Orientation
    <Description("Parallel")> Parallel = LortchtxLib.Orientations.RtChart_Parallel ' 0
    <Description("Perpendicular")> Perpendicular = LortchtxLib.Orientations.RtChart_Perpendicular ' 1
End Enum

''' <summary> Values that represent Paths. </summary>
Public Enum Path
    <Description("Right")> Right = LortchtxLib.Paths.RtChart_RightPath ' 0
    <Description("Up")> Up = LortchtxLib.Paths.RtChart_UpPath ' 1
    <Description("Left")> Left = LortchtxLib.Paths.RtChart_LeftPath ' 2
    <Description("Down")> Down = LortchtxLib.Paths.RtChart_DownPath ' 3
End Enum

''' <summary> Values that represent Positions. </summary>
Public Enum Position
    <Description("Below Right")> BelowRight = LortchtxLib.Positions.RtChart_BelowRight ' 0
    <Description("Above Left")> AboveLeft = LortchtxLib.Positions.RtChart_AboveLeft ' 1
    <Description("On Scale")> OnScale = LortchtxLib.Positions.RtChart_OnScale ' 2
End Enum

''' <summary> Values that represent Scale Visibilities. </summary>
Public Enum ScaleVisibility
    <Description("Hidden")> Hidden = LortchtxLib.ScaleVisibilities.RtChart_Hidden ' 0
    <Description("Scale Only")> ScaleOnly = LortchtxLib.ScaleVisibilities.RtChart_ScaleOnly ' 1
    <Description("Label Only")> LabelOnly = LortchtxLib.ScaleVisibilities.RtChart_LabelOnly ' 2
    <Description("Scale And Label")> ScaleAndLabel = LortchtxLib.ScaleVisibilities.RtChart_ScaleAndLabel ' 3
End Enum

''' <summary> Values that represent error codes. </summary>
Public Enum ErrorCode
    <Description("No Error")> None = 0
    <Description("Invalid control argument specified")> InvalidControlArgument = -LortchtxLib.ErrCode.RtChartData_InvCtl ' -1
    <Description("Unexpected internal control error")> UnexpectedInternalControl = -LortchtxLib.ErrCode.RtChartData_IntCtl ' -2
    <Description("Invalid/undefined channel specified")> InvalidChannel = -LortchtxLib.ErrCode.RtChartData_InvChn ' -3
    <Description("Channel not initialized")> ChannelNotInitialized = -LortchtxLib.ErrCode.RtChartData_NotInit ' -4
    <Description("Vertical (Y) transform not defined")> VerticalTransformNotDefined = -LortchtxLib.ErrCode.RtChartData_NoYtran ' -5
    <Description("Vertical (Y) data is null")> NullVerticalData = -LortchtxLib.ErrCode.RtchartData_NullY ' -6
    <Description("Invalid channel data shape specified")> InvalidChannelDataShape = -LortchtxLib.ErrCode.RtChartData_Shape ' -7
    <Description("Horizontal (X) transform not defined")> HorizontalTransformNotDefined = -LortchtxLib.ErrCode.RtChartData_NoXtran ' -8
    <Description("Horizontal (X) data is null")> NullHorizontalData = -LortchtxLib.ErrCode.RtchartData_NullX ' -9
    <Description("Array bounds error detected or nPts <= 0")> ArrayBoundsError = -LortchtxLib.ErrCode.RtChartData_Bounds ' -10
End Enum

''' <summary> Values that represent mouse pointers. </summary>
Public Enum MousePointer
    <Description("Default")> [Default] = LortchtxLib.MousePointers.MousePointer_Default ' 0
    <Description("Arrow")> Arrow = LortchtxLib.MousePointers.MousePointer_Arrow '1
    <Description("Cross")> Cross = LortchtxLib.MousePointers.MousePointer_Cross '2
    <Description("IBeam")> IBeam = LortchtxLib.MousePointers.MousePointer_IBeam ' 3
    <Description("Icon")> Icon = LortchtxLib.MousePointers.MousePointer_Icon ' 4
    <Description("Size")> Size = LortchtxLib.MousePointers.MousePointer_Size ' 5
    <Description("SizeNESW")> SizeNESW = LortchtxLib.MousePointers.MousePointer_SizeNESW ' 6
    <Description("SizeNS")> SizeNS = LortchtxLib.MousePointers.MousePointer_SizeNS ' 7
    <Description("SizeNWSE")> SizeNWSE = LortchtxLib.MousePointers.MousePointer_SizeNWSE ' 8
    <Description("SizeWE")> SizeWE = LortchtxLib.MousePointers.MousePointer_SizeWE ' 9
    <Description("UpArrow")> UpArrow = LortchtxLib.MousePointers.MousePointer_UpArrow ' 10
    <Description("Hourglass")> Hourglass = LortchtxLib.MousePointers.MousePointer_Hourglass ' 11
    <Description("No Drop")> NoDrop = LortchtxLib.MousePointers.MousePointer_NoDrop ' 12
    <Description("AppStarting")> AppStarting = LortchtxLib.MousePointers.MousePointer_AppStarting ' 13
    <Description("Question")> Question = LortchtxLib.MousePointers.MousePointer_Question '14
    <Description("SizeAll")> SizeAll = LortchtxLib.MousePointers.MousePointer_SizeAll ' 15
    <Description("Hand")> Hand = LortchtxLib.MousePointers.MousePointer_Hand '16
    <Description("Bar")> Bar = LortchtxLib.MousePointers.MousePointer_Bar ' 17
    <Description("PushLt")> PushLt = LortchtxLib.MousePointers.MousePointer_PushLt ' 18
    <Description("PushRt")> PushRt = LortchtxLib.MousePointers.MousePointer_PushRt ' 19
    <Description("CrossHair")> CrossHair = LortchtxLib.MousePointers.MousePointer_CrossHair ' 20
    <Description("Target")> Target = LortchtxLib.MousePointers.MousePointer_Target ' 21
    <Description("Disc")> Disc = LortchtxLib.MousePointers.MousePointer_Disc ' 22
    <Description("Custom")> Custom = LortchtxLib.MousePointers.MousePointer_Custom ' 99
End Enum

