Imports System.ComponentModel
Imports System.Drawing
Imports AxLortchtxLib

''' <summary> A real time chart control. </summary>
''' <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 6/28/2019 </para><para>
''' David, 6/20/2019 </para></remarks>
Public Class RealTimeChartControl
    Inherits System.Windows.Forms.UserControl

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Gets or sets the sentinel indicating if the control is in initializing its components.
    ''' </summary>
    ''' <value> <see langword="True" /> if initializing components. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Protected Property InitializingComponents As Boolean

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New
        Me.InitializingComponents = True

        ' This call is required by the designer.
        Me.InitializeComponent()

        Me.InitializingComponents = False

    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> <see langword="True" /> to release both managed and unmanaged
    '''                                                   resources; <see langword="False" /> to
    '''                                                   release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " ENCAPSULATED CHART: WINDOW "

    ''' <summary> Gets or sets the logical window; Window "name". </summary>
    ''' <value> The logical window. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property LogicalWindow As Integer
        Get
            Return Me._RealTimeChart.Wnd_Select
        End Get
        Set(value As Integer)
            Me._RealTimeChart.Wnd_Select = CShort(value)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the size of the automatic; false allows non-square border, grid is always square;
    ''' true forces square border, frame is always square.
    ''' </summary>
    ''' <value> The size of the automatic. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Overloads Property AutoSize As Boolean
        Get
            Return Me._RealTimeChart.AutoSize
        End Get
        Set(value As Boolean)
            Me._RealTimeChart.AutoSize = value
            MyBase.AutoSize = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the height of the window, e.g., in Volts (Volts/div * 8 divisions)
    ''' </summary>
    ''' <value> The height of the window. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property WndHeight As Single
        Get
            Return Me._RealTimeChart.WndHeight
        End Get
        Set(value As Single)
            Me._RealTimeChart.WndHeight = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the width of the window, e.g., in Seconds (Time/div * 10 divisions)
    ''' </summary>
    ''' <value> The width of the window. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property WndWidth As Single
        Get
            Return Me._RealTimeChart.WndWidth
        End Get
        Set(value As Single)
            Me._RealTimeChart.WndWidth = value
        End Set
    End Property

    ''' <summary> Gets or sets the window x coordinate; takes twips and returns Chart Window units </summary>
    ''' <value> The window x coordinate. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property WndX As Single
        Get
            Return Me._RealTimeChart.WndX
        End Get
        Set(value As Single)
            Me._RealTimeChart.WndX = value
        End Set
    End Property

    ''' <summary> Gets or sets the window X min; left minimum abscissa. </summary>
    ''' <value> The window X min, x:[-1,1]. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property WndXmin As Single
        Get
            Return Me._RealTimeChart.WndXmin
        End Get
        Set(value As Single)
            Me._RealTimeChart.WndXmin = value
        End Set
    End Property

    ''' <summary> Gets or sets the window y coordinate; takes twips and returns Chart Window units </summary>
    ''' <value> The window y coordinate. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property WndY As Single
        Get
            Return Me._RealTimeChart.WndY
        End Get
        Set(value As Single)
            Me._RealTimeChart.WndY = value
        End Set
    End Property

    ''' <summary> Gets or sets the window y min; bottom minimum ordinate. </summary>
    ''' <value> The window y min ,y:[-1,1]. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property WndYmin As Single
        Get
            Return Me._RealTimeChart.WndYmin
        End Get
        Set(value As Single)
            Me._RealTimeChart.WndYmin = value
        End Set
    End Property

    ''' <summary> Gets or sets the color of the axes. </summary>
    ''' <value> The color of the axes. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property AxesColor As Drawing.Color
        Get
            Return Me._RealTimeChart.AxesColor
        End Get
        Set(value As Drawing.Color)
            Me._RealTimeChart.AxesColor = value
        End Set
    End Property

    ''' <summary> Gets or sets the axes tics. </summary>
    ''' <value> The axes tics. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property AxesTics As AxesTicsStyle
        Get
            Return CType(Me._RealTimeChart.AxesTics, AxesTicsStyle)
        End Get
        Set(value As AxesTicsStyle)
            Me._RealTimeChart.AxesTics = CType(value, LortchtxLib.AxesTics)
        End Set
    End Property

    ''' <summary> Gets or sets the type of the axes. </summary>
    ''' <value> The type of the axes. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property AxesType As AxesType
        Get
            Return CType(Me._RealTimeChart.AxesType, AxesType)
        End Get
        Set(value As AxesType)
            Me._RealTimeChart.AxesType = CType(value, LortchtxLib.AxesTypes)
        End Set
    End Property

    ''' <summary> Gets or sets the color of the back. </summary>
    ''' <value> The color of the back. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Overloads Property BackColor As Drawing.Color
        Get
            Return Me._RealTimeChart.BackColor
        End Get
        Set(value As Drawing.Color)
            Me._RealTimeChart.BackColor = value
            MyBase.BackColor = value
        End Set
    End Property

#End Region

#Region " ENCAPSULATED CHART: BEVEL "

    ''' <summary> Gets or sets the bevel inner. </summary>
    ''' <value> The bevel inner. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property BevelInner As BevelStyle
        Get
            Return CType(Me._RealTimeChart.BevelInner, BevelStyle)
        End Get
        Set(value As BevelStyle)
            Me._RealTimeChart.BevelInner = CType(value, LortchtxLib.BevelStyles)
        End Set
    End Property

    ''' <summary> Gets or sets the bevel outer. </summary>
    ''' <value> The bevel outer. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property BevelOuter As BevelStyle
        Get
            Return CType(Me._RealTimeChart.BevelOuter, BevelStyle)
        End Get
        Set(value As BevelStyle)
            Me._RealTimeChart.BevelOuter = CType(value, LortchtxLib.BevelStyles)
        End Set
    End Property

    ''' <summary> Gets or sets the bevel width inner. </summary>
    ''' <value> The bevel width inner. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property BevelWidthInner As Integer
        Get
            Return Me._RealTimeChart.BevelWidth_Inner
        End Get
        Set(value As Integer)
            Me._RealTimeChart.BevelWidth_Inner = CShort(value)
        End Set
    End Property

    ''' <summary> Gets or sets the bevel width outer. </summary>
    ''' <value> The bevel width outer. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property BevelWidthOuter As Integer
        Get
            Return Me._RealTimeChart.BevelWidth_Outer
        End Get
        Set(value As Integer)
            Me._RealTimeChart.BevelWidth_Outer = CShort(value)
        End Set
    End Property

#End Region

#Region " ENCAPSULATED CHART: BORDER "

    ''' <summary> Gets or sets the color of the border. </summary>
    ''' <value> The color of the border. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property BorderColor As Drawing.Color
        Get
            Return Me._RealTimeChart.BorderColor
        End Get
        Set(value As Drawing.Color)
            Me._RealTimeChart.BorderColor = value
        End Set
    End Property

    ''' <summary> Gets or sets the width of the border. </summary>
    ''' <value> The width of the border. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property BorderWidth As Integer
        Get
            Return Me._RealTimeChart.BorderWidth
        End Get
        Set(value As Integer)
            Me._RealTimeChart.BorderWidth = CShort(value)
        End Set
    End Property

    ''' <summary> Gets or sets the color of the light. </summary>
    ''' <value> The color of the light. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property LightColor As Drawing.Color
        Get
            Return Me._RealTimeChart.LightColor
        End Get
        Set(value As Drawing.Color)
            Me._RealTimeChart.LightColor = value
        End Set
    End Property

    ''' <summary> Gets or sets the rounded corners. </summary>
    ''' <value> The rounded corners. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property RoundedCorners As Boolean
        Get
            Return Me._RealTimeChart.RoundedCorners
        End Get
        Set(value As Boolean)
            Me._RealTimeChart.RoundedCorners = value
        End Set
    End Property

    ''' <summary> Gets or sets the color of the shadow. </summary>
    ''' <value> The color of the shadow. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ShadowColor As Drawing.Color
        Get
            Return Me._RealTimeChart.ShadowColor
        End Get
        Set(value As Drawing.Color)
            Me._RealTimeChart.ShadowColor = value
        End Set
    End Property

#End Region

#Region " ENCAPSULATED CHART: CHANNEL "

    ''' <summary> Gets or sets the logical channel. use negative channel value to De-initialize channel. </summary>
    ''' <value> The logical channel. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property LogicalChannel As Integer
        Get
            Return Me._RealTimeChart.Chn_Select
        End Get
        Set(value As Integer)
            Me._RealTimeChart.Chn_Select = CShort(value)
        End Set
    End Property

    ''' <summary> Gets or sets the length of the channel display buffer; input buffer length for each channel. </summary>
    ''' <value> The length of the channel display buffer. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ChnDspBufLen As Integer
        Get
            Return Me._RealTimeChart.ChnDspBufLen
        End Get
        Set(value As Integer)
            Me._RealTimeChart.ChnDspBufLen = CShort(value)
        End Set
    End Property

    ''' <summary> Gets or sets the channel display pen; the <see cref="LogicalPen"/> to use; pens often have same value as channel or cursor. </summary>
    ''' <remarks> TO_DO: rename. </remarks>
    ''' <value> The channel display pen. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ChnDspPen As Integer
        Get
            Return Me._RealTimeChart.ChnDspPen
        End Get
        Set(value As Integer)
            Me._RealTimeChart.ChnDspPen = CShort(value)
        End Set
    End Property

    ''' <summary> Gets or sets the logical window associated with the channel; logical coordinates range </summary>
    ''' <remarks> TO_DO: Rename to Channel Display Window. </remarks>
    ''' <value> The channel display DSP window. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ChnDspWindow As Integer
        Get
            Return Me._RealTimeChart.ChnDspWindow
        End Get
        Set(value As Integer)
            Me._RealTimeChart.ChnDspWindow = CShort(value)
        End Set
    End Property

    ''' <summary> Gets or sets the logical Viewport associated with the channel; default display location </summary>
    ''' <remarks> TO_DO: Rename to Channel Display Viewport. </remarks>
    ''' <value> The channel display DSP Viewport. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ChnDspViewport As Integer
        Get
            Return Me._RealTimeChart.ChnDspViewport
        End Get
        Set(value As Integer)
            Me._RealTimeChart.ChnDspViewport = CShort(value)
        End Set
    End Property

    ''' <summary> Gets or sets the channel display action. </summary>
    ''' <remarks> TO_DO: Rename to Channel Display Action. </remarks>
    ''' <value> The channel display action. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ChnDspAction As ChannelDisplayAction
        Get
            Return CType(Me._RealTimeChart.ChnDspAction, ChannelDisplayAction)
        End Get
        Set(value As ChannelDisplayAction)
            Me._RealTimeChart.ChnDspAction = CType(value, LortchtxLib.ChnDspActions)
        End Set
    End Property

    ''' <summary> Gets or sets the channel display mode. </summary>
    ''' <value> The channel display mode. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ChnDspMode As ChannelDisplayMode
        Get
            Return CType(Me._RealTimeChart.ChnDspMode, ChannelDisplayMode)
        End Get
        Set(value As ChannelDisplayMode)
            Me._RealTimeChart.ChnDspMode = CType(value, LortchtxLib.ChnDspModes)
        End Set
    End Property

    ''' <summary> Gets or sets the chn DSP offset x coordinate. </summary>
    ''' <value> The chn DSP offset x coordinate. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ChnDspOffsetX As Single
        Get
            Return Me._RealTimeChart.ChnDspOffsetX
        End Get
        Set(value As Single)
            Me._RealTimeChart.ChnDspOffsetX = value
        End Set
    End Property

    ''' <summary> Gets or sets the chn DSP offset y coordinate. </summary>
    ''' <value> The chn DSP offset y coordinate. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ChnDspOffsetY As Single
        Get
            Return Me._RealTimeChart.ChnDspOffsetY
        End Get
        Set(value As Single)
            Me._RealTimeChart.ChnDspOffsetY = value
        End Set
    End Property

    ''' <summary> Gets or sets the channel display style; line style for plotting arrays. </summary>
    ''' <value> The channel display style. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ChnDspStyle As ChannelDisplayStyle
        Get
            Return CType(Me._RealTimeChart.ChnDspStyle, ChannelDisplayStyle)
        End Get
        Set(value As ChannelDisplayStyle)
            Me._RealTimeChart.ChnDspStyle = CType(value, LortchtxLib.ChnDspStyles)
        End Set
    End Property

    ''' <summary> Gets or sets the channel data dimension. </summary>
    ''' <value> The channel data dimension. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ChannelDataDimension As LogicalDimension
        Get
            Return CType(Me._RealTimeChart.ChnData_Dimension, LogicalDimension)
        End Get
        Set(value As LogicalDimension)
            Me._RealTimeChart.ChnData_Dimension = CType(value, LortchtxLib.Dims)
        End Set
    End Property

    ''' <summary> Gets or sets the channel data offset into buffer (in points). </summary>
    ''' <value> The channel data offset. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ChnDataOffset As Integer
        Get
            Return Me._RealTimeChart.ChnDataOffset
        End Get
        Set(value As Integer)
            Me._RealTimeChart.ChnDataOffset = CShort(value)
        End Set
    End Property

    ''' <summary> Gets or sets the channel data logical increment to next value in array. </summary>
    ''' <value> The channel data increment. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ChnDataIncr As Integer
        Get
            Return Me._RealTimeChart.ChnDataIncr
        End Get
        Set(value As Integer)
            Me._RealTimeChart.ChnDataIncr = CShort(value)
        End Set
    End Property

    ''' <summary> Gets or sets the channel data transform. </summary>
    ''' <value> The channel data transform. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ChnDataXform As Integer
        Get
            Return Me._RealTimeChart.ChnDataXform
        End Get
        Set(value As Integer)
            Me._RealTimeChart.ChnDataXform = CShort(value)
        End Set
    End Property

    ''' <summary> Gets or sets the channel data shape. </summary>
    ''' <value> The channel data shape. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ChnDataShape As ChannelDataShape
        Get
            Return CType(Me._RealTimeChart.ChnDataShape, ChannelDataShape)
        End Get
        Set(value As ChannelDataShape)
            Me._RealTimeChart.ChnDataShape = CType(value, LortchtxLib.ChnDataShapes)
        End Set
    End Property

    ''' <summary> Gets or sets the type of the channel data. </summary>
    ''' <value> The type of the channel data. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ChnDataType As ChannelDataType
        Get
            Return CType(Me._RealTimeChart.ChnDataType, ChannelDataType)
        End Get
        Set(value As ChannelDataType)
            Me._RealTimeChart.ChnDataType = CType(value, LortchtxLib.ChnDataTypes)
        End Set
    End Property

    ''' <summary> Gets or sets information describing the channel display user. </summary>
    ''' <value> Information describing the channel display user. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ChannelDisplayUserData As Integer
        Get
            Return Me._RealTimeChart.ChnDspUserData
        End Get
        Set(value As Integer)
            Me._RealTimeChart.ChnDspUserData = CShort(value)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the depth of the color. Defines the color adapter as 16 or 256 colors.
    ''' </summary>
    ''' <value> The depth of the color. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ColorDepth As ColorDepth
        Get
            Return CType(Me._RealTimeChart.ColorDepth, ColorDepth)
        End Get
        Set(value As ColorDepth)
            Me._RealTimeChart.ColorDepth = CType(value, LortchtxLib.ColorDepth)
        End Set
    End Property

    ''' <summary> Gets or sets the error base. </summary>
    ''' <value> The error base. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ErrorBase As Integer
        Get
            Return Me._RealTimeChart.ErrBase
        End Get
        Set(value As Integer)
            Me._RealTimeChart.ErrBase = CShort(value)
        End Set
    End Property

    ''' <summary> Gets or sets the color of the foreground. </summary>
    ''' <value> The color of the foreground. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Overloads Property ForeColor As Drawing.Color
        Get
            Return Me._RealTimeChart.ForeColor
        End Get
        Set(value As Drawing.Color)
            Me._RealTimeChart.ForeColor = value
            MyBase.ForeColor = value
        End Set
    End Property

#End Region

#Region " ENCAPSULATED CHART: TRANSFORM "

    ''' <summary> Gets or sets the channel transformation. </summary>
    ''' <value> The channel transformation. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ChannelTransformation As Integer
        Get
            Return Me._RealTimeChart.ChnXform_Select
        End Get
        Set(value As Integer)
            Me._RealTimeChart.ChnXform_Select = CShort(value)
        End Set
    End Property

    ''' <summary> Gets or sets the channel transform gain. </summary>
    ''' <value> The channel transform gain. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ChnXformGain As Double
        Get
            Return Me._RealTimeChart.ChnXformGain
        End Get
        Set(value As Double)
            Me._RealTimeChart.ChnXformGain = CSng(value)
        End Set
    End Property

    ''' <summary> Gets or sets the channel transform offset. </summary>
    ''' <value> The channel transform offset. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ChnXformOffset As Double
        Get
            Return Me._RealTimeChart.ChnXformOffset
        End Get
        Set(value As Double)
            Me._RealTimeChart.ChnXformOffset = CSng(value)
        End Set
    End Property

    ''' <summary> Gets or sets the channel transform LSB. </summary>
    ''' <value> The channel transform LSB. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ChnXformLSB As Integer
        Get
            Return Me._RealTimeChart.ChnXformLSB
        End Get
        Set(value As Integer)
            Me._RealTimeChart.ChnXformLSB = CShort(value)
        End Set
    End Property

    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ChnXformMSB As Integer
        Get
            Return Me._RealTimeChart.ChnXformMSB
        End Get
        Set(value As Integer)
            Me._RealTimeChart.ChnXformMSB = CShort(value)
        End Set
    End Property

#End Region

#Region " ENCAPSULATED CHART: FONT "

    ''' <summary> Gets or sets the chart font. </summary>
    ''' <remarks>
    ''' This set the chart font view the font properties. Assigning a font as object fails due to
    ''' failure to load the Standard OLE Types type library.
    ''' </remarks>
    ''' <value> The chart font. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ChartFont As Drawing.Font
        Get
            Return Me._RealTimeChart.Font
        End Get
        Set(value As Drawing.Font)
            If value IsNot Nothing Then
                Me._RealTimeChart.FontBold = value.Bold
                Me._RealTimeChart.FontItalic = value.Italic
                Me._RealTimeChart.FontName = value.Name
                Me._RealTimeChart.FontSize = value.Size
                Me._RealTimeChart.FontStrikethru = value.Strikeout
                Me._RealTimeChart.FontUnderline = value.Underline
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the font bold. </summary>
    ''' <value> The font bold. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property FontBold As Boolean
        Get
            Return Me._RealTimeChart.FontBold
        End Get
        Set(value As Boolean)
            Me._RealTimeChart.FontBold = value
        End Set
    End Property

    ''' <summary> Gets or sets the font italic. </summary>
    ''' <value> The font italic. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property FontItalic As Boolean
        Get
            Return Me._RealTimeChart.FontItalic
        End Get
        Set(value As Boolean)
            Me._RealTimeChart.FontItalic = value
        End Set
    End Property

    ''' <summary> Gets or sets the name of the font. </summary>
    ''' <value> The name of the font. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property FontName As String
        Get
            Return Me._RealTimeChart.FontName
        End Get
        Set(value As String)
            Me._RealTimeChart.FontName = value
        End Set
    End Property

    ''' <summary> Gets or sets the size of the font. </summary>
    ''' <value> The size of the font. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property FontSize As Single
        Get
            Return Me._RealTimeChart.FontSize
        End Get
        Set(value As Single)
            Me._RealTimeChart.FontSize = value
        End Set
    End Property

    ''' <summary> Gets or sets the font strikeout. </summary>
    ''' <value> The font strikeout. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property FontStrikeout As Boolean
        Get
            Return Me._RealTimeChart.FontStrikethru
        End Get
        Set(value As Boolean)
            Me._RealTimeChart.FontStrikethru = value
        End Set
    End Property

    ''' <summary> Gets or sets the font underline. </summary>
    ''' <value> The font underline. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property FontUnderline As Boolean
        Get
            Return Me._RealTimeChart.FontUnderline
        End Get
        Set(value As Boolean)
            Me._RealTimeChart.FontUnderline = value
        End Set
    End Property

#End Region

#Region " ENCAPSULATED CHART: FRAME "

    ''' <summary> Gets or sets the color of the frame. </summary>
    ''' <value> The color of the frame. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property FrameColor As Drawing.Color
        Get
            Return Me._RealTimeChart.FrameColor
        End Get
        Set(value As Drawing.Color)
            Me._RealTimeChart.FrameColor = value
        End Set
    End Property

    ''' <summary> Gets or sets the frame on. </summary>
    ''' <value> The frame on. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property FrameOn As Boolean
        Get
            Return Me._RealTimeChart.FrameOn
        End Get
        Set(value As Boolean)
            Me._RealTimeChart.FrameOn = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the frames per second; 0 for asynchronous, -1 for synchronous.
    ''' </summary>
    ''' <value> The frames per second. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property FramesPerSec As Integer
        Get
            Return Me._RealTimeChart.FramesPerSec
        End Get
        Set(value As Integer)
            Me._RealTimeChart.FramesPerSec = CShort(value)
        End Set
    End Property

    ''' <summary> Gets or sets the frame tics. </summary>
    ''' <value> The frame tics. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property FrameTics As FrameTicsStyle
        Get
            Return CType(Me._RealTimeChart.FrameTics, FrameTicsStyle)
        End Get
        Set(value As FrameTicsStyle)
            Me._RealTimeChart.FrameTics = CType(value, LortchtxLib.FrameTics)
        End Set
    End Property

#End Region

#Region " ENCAPSULATED CHART: GRID "

    ''' <summary> Gets or sets the color of the grid. </summary>
    ''' <value> The color of the grid. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property GridColor As Drawing.Color
        Get
            Return Me._RealTimeChart.GridColor
        End Get
        Set(value As Drawing.Color)
            Me._RealTimeChart.GridColor = value
        End Set
    End Property

    ''' <summary> Gets or sets the grid on. </summary>
    ''' <value> The grid on. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property GridOn As Boolean
        Get
            Return Me._RealTimeChart.GridOn
        End Get
        Set(value As Boolean)
            Me._RealTimeChart.GridOn = value
        End Set
    End Property

    ''' <summary> Gets or sets the grid symmetry. </summary>
    ''' <value> The grid symmetry. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property GridSymmetry As GridSymmetry
        Get
            Return CType(Me._RealTimeChart.GridSymmetry, GridSymmetry)
        End Get
        Set(value As GridSymmetry)
            Me._RealTimeChart.GridSymmetry = CType(value, LortchtxLib.GridSymmetry)
        End Set
    End Property

    ''' <summary> Gets or sets the type of the grid. </summary>
    ''' <value> The type of the grid. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property GridType As GridType
        Get
            Return CType(Me._RealTimeChart.GridType, GridType)
        End Get
        Set(value As GridType)
            Me._RealTimeChart.GridType = CType(value, LortchtxLib.GridTypes)
        End Set
    End Property

#End Region

#Region " ENCAPSULATED CHART: CHART AREA "

    ''' <summary> Gets or sets the hit test; enables mouse pointer "hit" detection. </summary>
    ''' <value> The hit test. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property HitTest As Boolean
        Get
            Return Me._RealTimeChart.HitTest
        End Get
        Set(value As Boolean)
            Me._RealTimeChart.HitTest = value
        End Set
    End Property

    ''' <summary> Gets or sets the image file; specifies file name for Write action. </summary>
    ''' <value> The image file. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ImageFile As String
        Get
            Return Me._RealTimeChart.ImageFile
        End Get
        Set(value As String)
            Me._RealTimeChart.ImageFile = value
        End Set
    End Property

    ''' <summary> Gets or sets the major div horizontal. </summary>
    ''' <value> The major div horizontal. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property MajorDivHorz As Integer
        Get
            Return Me._RealTimeChart.MajorDivHorz
        End Get
        Set(value As Integer)
            Me._RealTimeChart.MajorDivHorz = CShort(value)
        End Set
    End Property

    ''' <summary> Gets or sets the major div vertical. </summary>
    ''' <value> The major div vertical. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property MajorDivVert As Integer
        Get
            Return Me._RealTimeChart.MajorDivVert
        End Get
        Set(value As Integer)
            Me._RealTimeChart.MajorDivVert = CShort(value)
        End Set
    End Property

    ''' <summary> Gets or sets the bottom graticule border. </summary>
    ''' <value> The margin bottom. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property MarginBottom As Integer
        Get
            Return Me._RealTimeChart.MarginBottom
        End Get
        Set(value As Integer)
            Me._RealTimeChart.MarginBottom = CShort(value)
        End Set
    End Property

    ''' <summary> Gets or sets the left graticule border. </summary>
    ''' <value> The margin left. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property MarginLeft As Integer
        Get
            Return Me._RealTimeChart.MarginLeft
        End Get
        Set(value As Integer)
            Me._RealTimeChart.MarginLeft = CShort(value)
        End Set
    End Property

    ''' <summary> Gets or sets the right graticule border. </summary>
    ''' <value> The margin right. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property MarginRight As Integer
        Get
            Return Me._RealTimeChart.MarginRight
        End Get
        Set(value As Integer)
            Me._RealTimeChart.MarginRight = CShort(value)
        End Set
    End Property

    ''' <summary> Gets or sets the top graticule border. </summary>
    ''' <value> The margin top. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property MarginTop As Integer
        Get
            Return Me._RealTimeChart.MarginTop
        End Get
        Set(value As Integer)
            Me._RealTimeChart.MarginTop = CShort(value)
        End Set
    End Property

    ''' <summary> Gets or sets the minor div. </summary>
    ''' <value> The minor div. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property MinorDiv As Integer
        Get
            Return Me._RealTimeChart.MinorDiv
        End Get
        Set(value As Integer)
            Me._RealTimeChart.MinorDiv = CShort(value)
        End Set
    End Property

    ''' <summary> Gets or sets the outline. </summary>
    ''' <value> The outline. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property Outline As Boolean
        Get
            Return Me._RealTimeChart.Outline
        End Get
        Set(value As Boolean)
            Me._RealTimeChart.Outline = value
        End Set
    End Property

#End Region

#Region " ENCAPSULATED CHART: CHART OUTLINE "

    ''' <summary> Gets or sets the color of the outline. </summary>
    ''' <value> The color of the outline. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property OutlineColor As Drawing.Color
        Get
            Return Me._RealTimeChart.OutlineColor
        End Get
        Set(value As Drawing.Color)
            Me._RealTimeChart.OutlineColor = value
        End Set
    End Property

#End Region

#Region " ENCAPSULATED CHART: CHART PEN "

    ''' <summary> Gets or sets the logical pen;  Pen "name". </summary>
    ''' <value> The logical pen. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property LogicalPen As Integer
        Get
            Return Me._RealTimeChart.Pen_Select
        End Get
        Set(value As Integer)
            Me._RealTimeChart.Pen_Select = CShort(value)
        End Set
    End Property

    ''' <summary> Gets or sets the color of the pen. </summary>
    ''' <value> The color of the pen. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property PenColor As Drawing.Color
        Get
            Return Me._RealTimeChart.PenColor
        End Get
        Set(value As Drawing.Color)
            Me._RealTimeChart.PenColor = value
        End Set
    End Property

    ''' <summary> Gets or sets the color of the pen intense. </summary>
    ''' <value> The color of the pen intense. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property PenIntenseColor As Drawing.Color
        Get
            Return Me._RealTimeChart.PenIntenseColor
        End Get
        Set(value As Drawing.Color)
            Me._RealTimeChart.PenIntenseColor = value
        End Set
    End Property

    ''' <summary> Gets or sets the pen style. </summary>
    ''' <value> The pen style. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property PenStyle As PenStyle
        Get
            Return CType(Me._RealTimeChart.PenStyle, PenStyle)
        End Get
        Set(value As PenStyle)
            Me._RealTimeChart.PenStyle = CType(value, LortchtxLib.PenStyles)
        End Set
    End Property

    ''' <summary> Gets or sets the width of the pen. </summary>
    ''' <value> The width of the pen. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property PenWidth As Integer
        Get
            Return Me._RealTimeChart.PenWidth
        End Get
        Set(value As Integer)
            Me._RealTimeChart.PenWidth = CShort(value)
        End Set
    End Property

#End Region

#Region " ENCAPSULATED CHART: CHART LABEL "

    ''' <summary> Gets or sets the label caption; "volts", "secs" , etc. </summary>
    ''' <value> The label caption. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property LabelCaption As String
        Get
            Return Me._RealTimeChart.LabelCaption
        End Get
        Set(value As String)
            Me._RealTimeChart.LabelCaption = value
        End Set
    End Property

    ''' <summary> Gets or sets the label location relative to scale: left, right, center. </summary>
    ''' <value> The label location. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property LabelLocation As LabelLocation
        Get
            Return CType(Me._RealTimeChart.LabelLocation, LabelLocation)
        End Get
        Set(value As LabelLocation)
            Me._RealTimeChart.LabelLocation = CType(value, LortchtxLib.LabelLocations)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the Label orientation relative to Scale Dimension Parallel or Perpendicular to
    ''' LabelDimension.
    ''' </summary>
    ''' <value> The Label orientation. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property LabelOrientation As Orientation
        Get
            Return CType(Me._RealTimeChart.LabelOrientation, Orientation)
        End Get
        Set(value As Orientation)
            Me._RealTimeChart.LabelOrientation = CType(value, LortchtxLib.Orientations)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the full pathname of the Label file; direction which characters follow: left.
    ''' right, up, down.
    ''' </summary>
    ''' <value> The full pathname of the Label file. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property LabelPath As Path
        Get
            Return CType(Me._RealTimeChart.LabelPath, Path)
        End Get
        Set(value As Path)
            Me._RealTimeChart.LabelPath = CType(value, LortchtxLib.Paths)
        End Set
    End Property

    ''' <summary> Gets or sets the Label pen. specifies pen color for Label. </summary>
    ''' <value> The Label pen. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property LabelPen As Integer
        Get
            Return Me._RealTimeChart.LabelPen
        End Get
        Set(value As Integer)
            Me._RealTimeChart.LabelPen = CShort(value)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the Label position; label Position: above, below, or on Label Location.
    ''' </summary>
    ''' <value> The Label position. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property LabelPosition As Position
        Get
            Return CType(Me._RealTimeChart.LabelPosition, Position)
        End Get
        Set(value As Position)
            Me._RealTimeChart.LabelPosition = CType(value, LortchtxLib.Positions)
        End Set
    End Property

#End Region

#Region " ENCAPSULATED CHART: CHART SCALE "

    ''' <summary>
    ''' Gets or sets the logical scale; selects the scale name (same for both dimensions).
    ''' </summary>
    ''' <value> The logical scale. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property LogicalScale As Integer
        Get
            Return Me._RealTimeChart.Scale_Select
        End Get
        Set(value As Integer)
            Me._RealTimeChart.Scale_Select = CShort(value)
        End Set
    End Property

    ''' <summary> Gets or sets the scale action. </summary>
    ''' <value> The scale action. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ScaleAction As ScaleAction
        Get
            Return CType(Me._RealTimeChart.ScaleAction, ScaleAction)
        End Get
        Set(value As ScaleAction)
            Me._RealTimeChart.ScaleAction = CType(value, LortchtxLib.ScaleActions)
        End Set
    End Property

    ''' <summary> Gets or sets the scale dimension. select horizontal or vertical scale/label. </summary>
    ''' <value> The scale dimension. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ScaleDimension As LogicalDimension
        Get
            Return CType(Me._RealTimeChart.ScaleDimension, LogicalDimension)
        End Get
        Set(value As LogicalDimension)
            Me._RealTimeChart.ScaleDimension = CType(value, LortchtxLib.Dims)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the scale factor; multiplier used to convert window units to scale values.
    ''' </summary>
    ''' <value> The scale factor. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ScaleFactor As Single
        Get
            Return Me._RealTimeChart.ScaleFactor
        End Get
        Set(value As Single)
            Me._RealTimeChart.ScaleFactor = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the scale gap; space between ScaleLocation and scale/label in pixels.
    ''' </summary>
    ''' <value> The scale gap. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ScaleGap As Integer
        Get
            Return Me._RealTimeChart.ScaleGap
        End Get
        Set(value As Integer)
            Me._RealTimeChart.ScaleGap = CShort(value)
        End Set
    End Property

    ''' <summary> Gets or sets the scale location: axis or frame. </summary>
    ''' <value> The scale location. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ScaleLocation As ScaleLocation
        Get
            Return CType(Me._RealTimeChart.ScaleLocation, ScaleLocation)
        End Get
        Set(value As ScaleLocation)
            Me._RealTimeChart.ScaleLocation = CType(value, LortchtxLib.ScaleLocations)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the scale orientation; Parallel or Perpendicular to ScaleDimension.
    ''' </summary>
    ''' <value> The scale orientation. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ScaleOrientation As Orientation
        Get
            Return CType(Me._RealTimeChart.ScaleOrientation, Orientation)
        End Get
        Set(value As Orientation)
            Me._RealTimeChart.ScaleOrientation = CType(value, LortchtxLib.Orientations)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the full pathname of the scale file; direction which characters follow: left.
    ''' right, up, down.
    ''' </summary>
    ''' <value> The full pathname of the scale file. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ScalePath As Path
        Get
            Return CType(Me._RealTimeChart.ScalePath, Path)
        End Get
        Set(value As Path)
            Me._RealTimeChart.ScalePath = CType(value, LortchtxLib.Paths)
        End Set
    End Property

    ''' <summary> Gets or sets the scale pen. specifies pen color for scale. </summary>
    ''' <value> The scale pen. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ScalePen As Integer
        Get
            Return Me._RealTimeChart.ScalePen
        End Get
        Set(value As Integer)
            Me._RealTimeChart.ScalePen = CShort(value)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the scale places; number of significant digits to the right of decimal point.
    ''' </summary>
    ''' <value> The scale places. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ScalePlaces As Integer
        Get
            Return Me._RealTimeChart.ScalePlaces
        End Get
        Set(value As Integer)
            Me._RealTimeChart.ScalePlaces = CShort(value)
        End Set
    End Property

    ''' <summary> Gets or sets the scale position; above or below ScaleLocation. </summary>
    ''' <value> The scale position. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ScalePosition As Position
        Get
            Return CType(Me._RealTimeChart.ScalePosition, Position)
        End Get
        Set(value As Position)
            Me._RealTimeChart.ScalePosition = CType(value, LortchtxLib.Positions)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the scale viewport. specify viewport before selecting dimension.
    ''' </summary>
    ''' <value> The scale viewport. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ScaleViewport As Integer
        Get
            Return Me._RealTimeChart.ScaleViewport
        End Get
        Set(value As Integer)
            Me._RealTimeChart.ScaleViewport = CShort(value)
        End Set
    End Property

    ''' <summary> Gets or sets the scale visibility: none, scale, label, or both. </summary>
    ''' <value> The scale visibility. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ScaleVisibility As ScaleVisibility
        Get
            Return CType(Me._RealTimeChart.ScaleVisibility, ScaleVisibility)
        End Get
        Set(value As ScaleVisibility)
            Me._RealTimeChart.ScaleVisibility = CType(value, LortchtxLib.ScaleVisibilities)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the scale window. specify window to be scaled before selecting dimension.
    ''' </summary>
    ''' <value> The scale window. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ScaleWindow As Integer
        Get
            Return Me._RealTimeChart.ScaleWindow
        End Get
        Set(value As Integer)
            Me._RealTimeChart.ScaleWindow = CShort(value)
        End Set
    End Property

#End Region

#Region " ENCAPSULATED CHART: CHART VIEWPORT "

    ''' <summary> Gets or sets the logical viewport. </summary>
    ''' <value> The logical viewport. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property LogicalViewport As Integer
        Get
            Return Me._RealTimeChart.Viewport_Select
        End Get
        Set(value As Integer)
            Me._RealTimeChart.Viewport_Select = CShort(value)
        End Set
    End Property

    ''' <summary> Gets or sets the number of view ports to be initialized. </summary>
    ''' <value> The view ports. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property Viewports As Integer
        Get
            Return Me._RealTimeChart.Viewports
        End Get
        Set(value As Integer)
            Me._RealTimeChart.Viewports = CShort(value)
        End Set
    End Property

    ''' <summary> Gets or sets the storage mode plot color, same for all view ports </summary>
    ''' <value> The color of the viewport storage. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ViewportStorageColor As Drawing.Color
        Get
            Return Me._RealTimeChart.ViewportStorageColor
        End Get
        Set(value As Drawing.Color)
            Me._RealTimeChart.ViewportStorageColor = value
        End Set
    End Property

    ''' <summary> Gets or sets the viewport storage on. </summary>
    ''' <value> The viewport storage on. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ViewportStorageOn As Boolean
        Get
            Return Me._RealTimeChart.ViewportStorageOn
        End Get
        Set(value As Boolean)
            Me._RealTimeChart.ViewportStorageOn = value
        End Set
    End Property

#End Region

#Region " ENCAPSULATED CHART: CHART "

    ''' <summary> Gets or sets the chart action. </summary>
    ''' <value> The chart action. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property ChartAction As ChartAction
        Get
            Return CType(Me._RealTimeChart.Action, ChartAction)
        End Get
        Set(value As ChartAction)
            Me._RealTimeChart.Action = CType(value, LortchtxLib.ChartActions)
        End Set
    End Property

#End Region

#Region " ENCAPSULATED CHART: MOUSE "

    ''' <summary> Gets or sets the mouse icon. </summary>
    ''' <value> The mouse icon. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property MouseIcon As Drawing.Image
        Get
            Return Me._RealTimeChart.MouseIcon
        End Get
        Set(value As Drawing.Image)
            Me._RealTimeChart.MouseIcon = value
        End Set
    End Property

    ''' <summary> Gets or sets the mouse pointer. </summary>
    ''' <value> The mouse pointer. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property MousePointer As MousePointer
        Get
            Return CType(Me._RealTimeChart.MousePointer, MousePointer)
        End Get
        Set(value As MousePointer)
            Me._RealTimeChart.MousePointer = CType(value, LortchtxLib.MousePointers)
        End Set
    End Property

#End Region

#Region " INITIALIZED CHART DEFAULTS "

    ''' <summary> Resets the know state = Initial state. </summary>
    Public Sub ResetKnownState()

        ' define chart
        Me.ColorDepth = ColorDepth.ColorDefault
        Me.ErrorBase = 0
        Me.ForeColor = Color.FromArgb(255, 0, 0, 0) ' Black
        Me.HitTest = False
        Me.ImageFile = String.Empty
        Me.LightColor = Color.FromArgb(&HFF, &HFF, &HFF, &HFF) ' Highlight Text
        Me.Outline = True
        Me.OutlineColor = Color.FromArgb(&HFF, &H64, &H64, &H64) ' Window Frame
        Me.RoundedCorners = False
        Me.ShadowColor = Color.FromArgb(&HFF, &HA0, &HA0, &HA0) ' Color Dark

        ' define chart border
        Me.BevelInner = isr.Visuals.RealTimeChart.BevelStyle.None
        Me.BevelOuter = isr.Visuals.RealTimeChart.BevelStyle.Raised
        Me.BevelWidthInner = 1
        Me.BevelWidthOuter = 1
        Me.BorderColor = Color.FromArgb(&HFF, &HF0, &HF0, &HF0) ' Control
        Me.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.BorderWidth = 3

        ' Define logical drawing viewport
        Me.LogicalViewport = 1
        Me.Viewports = 1
        Me.ViewportStorageColor = Color.FromArgb(&HFF, &HC0, &HC0, &HC0) ' Silver
        Me.ViewportStorageOn = False

        ' Define logical drawing window
        Me.LogicalWindow = 1 ' Window "name"
        Me.WndXmin = 0
        Me.WndWidth = 10
        Me.WndYmin = 0
        Me.WndHeight = 10

        ' define drawing area
        Me.AutoSize = False
        Me.BackColor = Color.FromArgb(&HFF, &HC0, &HC0, &HC0) ' Silver
        Me.MarginBottom = 20
        Me.MarginLeft = 20
        Me.MarginRight = 20
        Me.MarginTop = 20

        ' define drawing frame
        Me.FrameColor = Color.FromArgb(0, 255, 255) ' Aqua
        Me.FrameOn = True
        Me.FramesPerSec = 0 ' synchronous drawing
        Me.FrameTics = isr.Visuals.RealTimeChart.FrameTicsStyle.MajorMinorFrameTics

        ' define the grid
        Me.GridColor = Color.FromArgb(128, 128, 128) ' Grey
        Me.GridOn = True
        Me.GridSymmetry = isr.Visuals.RealTimeChart.GridSymmetry.AnisotropicGrid
        Me.GridType = isr.Visuals.RealTimeChart.GridType.HorizontalVertical
        Me.MajorDivHorz = 10
        Me.MajorDivVert = 8
        Me.MinorDiv = 5

        ' define Axes
        Me.AxesColor = Color.FromArgb(0, 255, 255) ' Aqua
        Me.AxesTics = isr.Visuals.RealTimeChart.AxesTicsStyle.MajorMinorAxesTics
        Me.AxesType = isr.Visuals.RealTimeChart.AxesType.HorizontalVertical

        ' Specify a drawing pen
        Me.LogicalPen = 1 ' Pen "name"
        Me.PenColor = Color.FromArgb(0, 255, 255) ' Aqua
        Me.PenIntenseColor = Color.FromArgb(0, 255, 255) ' Aqua
        Me.PenStyle = isr.Visuals.RealTimeChart.PenStyle.SolidPen
        Me.PenWidth = 0

        ' Setup display channel parameters
        Me.LogicalChannel = 1 ' Channel "name"
        Me.ChannelDisplayUserData = 0
        Me.ChnDspWindow = 1 ' logical window to use
        Me.ChnDspViewport = 1 ' viewport to use
        Me.ChnDspPen = 1 ' pen to use
        Me.ChnDspBufLen = 500 ' input buffer length
        Me.ChnDspStyle = isr.Visuals.RealTimeChart.ChannelDisplayStyle.Points
        Me.LogicalScale = 1

        ' Describe horizontal dimension
        Me.ChannelDataDimension = LogicalDimension.Horizontal
        Me.ChnDataShape = ChannelDataShape.Scalar
        Me.ChnDataType = ChannelDataType.Single
        Me.ChnDataOffset = 0
        Me.ChnDataIncr = 1 ' logical increment to next value
        Me.ChannelTransformation = 1

        ' Describe vertical dimension
        Me.ChannelDataDimension = LogicalDimension.Vertical
        Me.ChnDataShape = ChannelDataShape.Scalar
        Me.ChnDataType = ChannelDataType.Single
        Me.ChnDataOffset = 0
        Me.ChnDataIncr = 1 ' logical increment to next value
        Me.ChannelTransformation = 1

    End Sub

    ''' <summary> Initializes the chart XY plot. </summary>
    Public Sub InitializeChartXYPlot()

        ' define chart
        Me.ColorDepth = ColorDepth.ColorDefault
        Me.ErrorBase = 30200
        Me.ForeColor = Color.FromArgb(-842150451) ' B2 32 32 33
        Me.HitTest = False
        Me.ImageFile = String.Empty
        Me.LightColor = Color.FromArgb(&HFF, &HFF, &HFF, &HF2) ' (-2147483634)
        Me.Outline = True
        Me.OutlineColor = Color.FromArgb(&HFF, &HFF, &HFF, &HFA) '(-2147483642)
        Me.RoundedCorners = False
        Me.ShadowColor = Color.FromArgb(&HFF, &HFF, &HFF, &HF0) '(-2147483632)

        ' define chart border
        Me.BevelInner = isr.Visuals.RealTimeChart.BevelStyle.Inset
        Me.BevelOuter = isr.Visuals.RealTimeChart.BevelStyle.Raised
        Me.BevelWidthInner = 1
        Me.BevelWidthOuter = 1
        Me.BorderColor = Color.FromArgb(&HFF, &HFF, &HFF, &HF1)
        Me.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.BorderWidth = 3

        ' Define logical drawing viewport
        Me.LogicalViewport = 1
        Me.Viewports = 1
        Me.ViewportStorageColor = Color.FromArgb(0, &HC0, &HC0) ' 12632256 ' C0 C0 C0
        Me.ViewportStorageOn = False

        ' Define logical drawing window
        Me.LogicalWindow = 1 ' Window "name"
        Me.WndXmin = -1.1 ' -1 <= x <= 1
        Me.WndWidth = 2.2
        Me.WndYmin = -1.1 ' -1 <= y <= 1
        Me.WndHeight = 2.2

        ' define drawing area
        Me.AutoSize = False
        Me.BackColor = Color.FromArgb(0, 128, 128)
        Me.MarginBottom = 10
        Me.MarginLeft = 10
        Me.MarginRight = 10
        Me.MarginTop = 10

        ' define drawing frame
        Me.FrameColor = Color.FromArgb(0, 255, 255)
        Me.FrameOn = True
        Me.FramesPerSec = 0 ' synchronous drawing
        Me.FrameTics = isr.Visuals.RealTimeChart.FrameTicsStyle.MajorMinorFrameTics

        ' define the grid
        Me.GridColor = Color.FromArgb(128, 128, 128)
        Me.GridOn = True
        Me.GridSymmetry = isr.Visuals.RealTimeChart.GridSymmetry.IsotropicGrid
        Me.GridType = isr.Visuals.RealTimeChart.GridType.HorizontalVertical
        Me.MajorDivHorz = 10
        Me.MajorDivVert = 8
        Me.MinorDiv = 5

        ' define Axes
        Me.AxesColor = Color.FromArgb(0, 255, 255)
        Me.AxesTics = isr.Visuals.RealTimeChart.AxesTicsStyle.MajorMinorAxesTics
        Me.AxesType = isr.Visuals.RealTimeChart.AxesType.HorizontalVertical

        ' Specify a drawing pen
        Me.LogicalPen = 1 ' Pen "name"
        Me.PenColor = Color.FromArgb(255, 255, 0)
        Me.PenIntenseColor = Color.FromArgb(255, 255, 255) ' ColorTranslator.ToOle(Color.FromArgb(255, 255, 255))
        Me.PenStyle = isr.Visuals.RealTimeChart.PenStyle.SolidPen
        Me.PenWidth = 1

        ' Setup display channel parameters
        Me.LogicalChannel = 1 ' Channel "name"
        Me.ChannelDataDimension = isr.Visuals.RealTimeChart.LogicalDimension.Vertical
        Me.ChannelDisplayUserData = 0
        Me.ChannelTransformation = 1
        Me.ChnDspWindow = 1 ' logical window to use
        Me.ChnDspViewport = 1 ' viewport to use
        Me.ChnDspPen = 1 ' pen to use
        Me.ChnDspBufLen = 500 ' input buffer length
        Me.ChnDspStyle = isr.Visuals.RealTimeChart.ChannelDisplayStyle.Points
        Me.LogicalScale = 1

        ' Describe horizontal dimension
        Me.ChannelDataDimension = LogicalDimension.Horizontal
        Me.ChnDataShape = ChannelDataShape.Scalar
        Me.ChnDataType = ChannelDataType.Single
        Me.ChnDataOffset = 0
        Me.ChnDataIncr = 1 ' logical increment to next value
        Me.ChannelTransformation = 1

        ' Describe vertical dimension
        Me.ChannelDataDimension = LogicalDimension.Vertical
        Me.ChnDataShape = ChannelDataShape.Scalar
        Me.ChnDataType = ChannelDataType.Single
        Me.ChnDataOffset = 0
        Me.ChnDataIncr = 1 ' logical increment to next value

    End Sub

#End Region

#Region " CHARTING ARRAY OF ARRAYS "

    ''' <summary> Chart data integer. </summary>
    ''' <param name="channel">        The channel. </param>
    ''' <param name="pointCount">     Number of points. </param>
    ''' <param name="ordinate">       The ordinate. </param>
    ''' <param name="ordinateOffset"> The ordinate offset. </param>
    ''' <returns> An Integer. </returns>
    Public Function ChartDataInteger(ByVal channel As Integer, ByVal pointCount As Integer, ByVal ordinate()() As Integer, ByVal ordinateOffset As Integer) As Integer
        Return Me._RealTimeChart.ChartData(CShort(channel), pointCount, ordinate(channel - 1), ordinateOffset, 0, ordinateOffset)
    End Function

    ''' <summary> Chart data single. </summary>
    ''' <param name="channel">        The channel. </param>
    ''' <param name="pointCount">     Number of points. </param>
    ''' <param name="ordinate">       The ordinate. </param>
    ''' <param name="ordinateOffset"> The ordinate offset. </param>
    ''' <returns> An Integer. </returns>
    Public Function ChartDataSingle(ByVal channel As Integer, ByVal pointCount As Integer, ByVal ordinate()() As Single, ByVal ordinateOffset As Integer) As Integer
        Return Me._RealTimeChart.ChartData(CShort(channel), pointCount, ordinate(channel - 1), ordinateOffset, 0, ordinateOffset)
    End Function

#End Region

#Region " CHARTING FUNCTIONS "

    ''' <summary> Chart data. </summary>
    ''' <param name="channel">        The channel. </param>
    ''' <param name="pointCount">     Number of points. </param>
    ''' <param name="ordinate">       The ordinate. </param>
    ''' <param name="ordinateOffset"> The ordinate offset. </param>
    ''' <param name="abscissa">       The abscissa. </param>
    ''' <param name="abscissaOffset"> The abscissa offset. </param>
    ''' <returns> An Integer. </returns>
    Public Function ChartData(ByVal channel As Integer, ByVal pointCount As Integer, ByVal ordinate As Integer(),
                              ByVal ordinateOffset As Integer, ByVal abscissa As Integer(), ByVal abscissaOffset As Integer) As Integer
        Return Me._RealTimeChart.ChartData(CShort(channel), pointCount, ordinate, ordinateOffset, abscissa, abscissaOffset)
    End Function

    ''' <summary> Chart data. </summary>
    ''' <param name="channel">        The channel. </param>
    ''' <param name="pointCount">     Number of points. </param>
    ''' <param name="ordinate">       The ordinate. </param>
    ''' <param name="ordinateOffset"> The ordinate offset. </param>
    ''' <param name="abscissa">       The abscissa. </param>
    ''' <param name="abscissaOffset"> The abscissa offset. </param>
    ''' <returns> An Integer. </returns>
    Public Function ChartData(ByVal channel As Integer, ByVal pointCount As Integer, ByVal ordinate As Single(),
                              ByVal ordinateOffset As Integer, ByVal abscissa As Single(), ByVal abscissaOffset As Integer) As Integer
        Return Me._RealTimeChart.ChartData(CShort(channel), pointCount, ordinate, ordinateOffset, abscissa, abscissaOffset)
    End Function

    Public Function ChartData(ByVal channel As Integer, ByVal pointCount As Integer, ByVal cord As Single()) As Integer
        Return Me._RealTimeChart.ChartData(CShort(channel), pointCount, cord, 0, 0, 0)
    End Function

    ''' <summary> Chart a data point. </summary>
    ''' <param name="channel">        The channel. </param>
    ''' <param name="pointCount">     Number of points. </param>
    ''' <param name="dataPoint">      A data point to plot. </param>
    ''' <param name="ordinateOffset"> The ordinate offset. </param>
    ''' <param name="abscissaOffset"> The abscissa offset. </param>
    ''' <returns> An Integer. </returns>
    Public Function ChartData(ByVal channel As Integer, ByVal pointCount As Integer, ByVal dataPoint As PointF,
                              ByVal ordinateOffset As Integer, ByVal abscissaOffset As Integer) As Integer
        Return Me._RealTimeChart.ChartData(CShort(channel), pointCount, New Single() {dataPoint.X, dataPoint.Y}, ordinateOffset, 0, abscissaOffset)
    End Function

    ''' <summary> Chart data. </summary>
    ''' <param name="channel">        The channel. </param>
    ''' <param name="pointCount">     Number of points. </param>
    ''' <param name="ordinate">       The ordinate. </param>
    ''' <param name="ordinateOffset"> The ordinate offset. </param>
    ''' <param name="abscissa">       The abscissa. </param>
    ''' <param name="abscissaOffset"> The abscissa offset. </param>
    ''' <returns> An Integer. </returns>
    Public Function ChartData(ByVal channel As Integer, ByVal pointCount As Integer, ByVal ordinate As Double(),
                              ByVal ordinateOffset As Integer, ByVal abscissa As Double(), ByVal abscissaOffset As Integer) As Integer
        Return Me._RealTimeChart.ChartData(CShort(channel), pointCount, ordinate, ordinateOffset, abscissa, abscissaOffset)
    End Function

    ''' <summary> Chart a data point. </summary>
    ''' <param name="channel">        The channel. </param>
    ''' <param name="pointCount">     Number of points. </param>
    ''' <param name="dataPoint">      A data point to plot. </param>
    ''' <param name="ordinateOffset"> The ordinate offset. </param>
    ''' <param name="abscissaOffset"> The abscissa offset. </param>
    ''' <returns> An Integer. </returns>
    Public Function ChartData(ByVal channel As Integer, ByVal pointCount As Integer, ByVal dataPoint As Windows.Point,
                              ByVal ordinateOffset As Integer, ByVal abscissaOffset As Integer) As Integer
        Return Me._RealTimeChart.ChartData(CShort(channel), pointCount, New Double() {dataPoint.X, dataPoint.Y}, ordinateOffset, 0, abscissaOffset)
    End Function

    ''' <summary> Chart data. </summary>
    ''' <param name="channel">        The channel. </param>
    ''' <param name="pointCount">     Number of points. </param>
    ''' <param name="values">         The values. </param>
    ''' <param name="ordinateOffset"> The ordinate offset. </param>
    ''' <param name="placeHolder">    A place holder. </param>
    ''' <param name="abscissaOffset"> The place holder. </param>
    ''' <returns> An Integer. </returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId:="2#")>
    Public Function ChartData(ByVal channel As Integer, ByVal pointCount As Integer, ByVal values As Integer(,),
                              ByVal ordinateOffset As Integer, ByVal placeHolder As Object, ByVal abscissaOffset As Integer) As Integer
        Return Me._RealTimeChart.ChartData(CShort(channel), pointCount, values, ordinateOffset, placeHolder, abscissaOffset)
    End Function

    ''' <summary> Chart data. </summary>
    ''' <param name="channel">        The channel. </param>
    ''' <param name="pointCount">     Number of points. </param>
    ''' <param name="values">         The values. </param>
    ''' <param name="ordinateOffset"> The ordinate offset. </param>
    ''' <param name="placeHolder">    A place holder. </param>
    ''' <param name="abscissaOffset"> The abscissa offset. </param>
    ''' <returns> An Integer. </returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId:="2#")>
    Public Function ChartData(ByVal channel As Integer, ByVal pointCount As Integer, ByVal values As Single(,),
                              ByVal ordinateOffset As Integer, ByVal placeHolder As Object, ByVal abscissaOffset As Integer) As Integer
        Return Me._RealTimeChart.ChartData(CShort(channel), pointCount, values, ordinateOffset, placeHolder, abscissaOffset)
    End Function

    ''' <summary> Chart data. </summary>
    ''' <param name="channel">        The channel. </param>
    ''' <param name="pointCount">     Number of points. </param>
    ''' <param name="values">         The values. </param>
    ''' <param name="ordinateOffset"> The ordinate offset. </param>
    ''' <param name="placeHolder">    A place holder. </param>
    ''' <param name="abscissaOffset"> The abscissa offset. </param>
    ''' <returns> An Integer. </returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId:="2#")>
    Public Function ChartData(ByVal channel As Integer, ByVal pointCount As Integer, ByVal values As Double(,),
                              ByVal ordinateOffset As Integer, ByVal placeHolder As Object, ByVal abscissaOffset As Integer) As Integer
        Return Me._RealTimeChart.ChartData(CShort(channel), pointCount, values, ordinateOffset, placeHolder, abscissaOffset)
    End Function

#End Region

#Region " CHARTING EVENTS  "

    Public Event RefreshEvent As EventHandler(Of RefreshEventArgs)

    ''' <summary> Real time chart refresh event. </summary>
    ''' <remarks>
    ''' <see cref="RealTimeChart.ChartAction"/>=<see cref="RealTimeChart.ChartAction.Rescale"/>causes
    ''' Refresh events for each displayed channel. Refresh occurs only if entire chart needs to be
    ''' redrawn (new plot outside current plot area). Plots should be re-drawn after ReScale Action
    ''' to maintain display resolution if changes in Window height and width are made.
    ''' </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      A _DLortchtxEvents_RefreshEvent to process. </param>
    Private Sub RealTimeChart_RefreshEvent(sender As Object, e As _DLortchtxEvents_RefreshEvent) Handles _RealTimeChart.RefreshEvent
        Dim evt As EventHandler(Of RefreshEventArgs) = Me.RefreshEventEvent
        evt?.Invoke(Me, New RefreshEventArgs(e))
    End Sub

#End Region

End Class

''' <summary> Additional information for refresh events. </summary>
''' <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 7/13/2019 </para></remarks>
Public Class RefreshEventArgs
    Inherits System.EventArgs
    Public Property Channel As Integer
    Public Property Samples As Integer
    Friend Sub New(ByVal legacyEventArgs As _DLortchtxEvents_RefreshEvent)
        MyBase.New()
        Me.Channel = legacyEventArgs.chn
        Me.Samples = legacyEventArgs.samples
    End Sub
End Class
