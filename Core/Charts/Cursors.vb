' file:		Cursors.vb
' LabOBJX Real-Time Chart - Mouse Cursor Support Declarations
' (C) Copyright 1993-95, Scientific Software Tools, Inc.
' All Rights Reserved.
'     Rev 1.0   27 Aug 1993 15:43:00   RFURMAN
'  Initial revision.
'
'     Rev 1.1   18 Apr 1995 10:35:00   RGABRIEL
'   added system cursor constants
'   added comments throughout
Option Strict Off
Option Explicit On
Public Module Cursors

    ' Declarations for Windows API to select custom cursors exported by RtChart VBX
    Private CursorDLLhInst As IntPtr ' handle to DLL (VBX) instance

    Public HandCurHandle As Integer ' handle used for "Hand" pointer
    Public BarCurHandle As Integer ' handle used for "Bar" pointer
    Public PushLtCurHandle As Integer ' handle used for "Push Left" pointer
    Public PushRtCurHandle As Integer ' handle used for "Push Right" pointer
    Public CrosshairCurHandle As Integer ' handle used for "Cross Hair" pointer
    Public DiscCurHandle As Integer ' handle used for "Disc" pointer
    Public TargetCurHandle As Integer ' handle used for "Target" pointer

    Public Sub LoadCustomCursors()

        ' RtChart control exports useful custom mouse pointers

        ' get handle to chart custom control DLL (VBX)
        CursorDLLhInst = SafeNativeMethods.LoadLibrary("lortchtx.dll")

        ' get handle to each cursor
        HandCurHandle = SafeNativeMethods.LoadCursor(CursorDLLhInst, "handCur")
        BarCurHandle = SafeNativeMethods.LoadCursor(CursorDLLhInst, "barCur")
        PushLtCurHandle = SafeNativeMethods.LoadCursor(CursorDLLhInst, "pushLtCur")
        PushRtCurHandle = SafeNativeMethods.LoadCursor(CursorDLLhInst, "pushRtCur")
        CrosshairCurHandle = SafeNativeMethods.LoadCursor(CursorDLLhInst, "crosshairCur")
        DiscCurHandle = SafeNativeMethods.LoadCursor(CursorDLLhInst, "diskCur")
        TargetCurHandle = SafeNativeMethods.LoadCursor(CursorDLLhInst, "targetCur")

    End Sub

    ''' <summary> Unload custom cursors. </summary>
    ''' <returns> An Integer. </returns>
    Public Function UnloadCustomCursors() As Integer
        Dim result As Integer = 0
        If CursorDLLhInst = 0 Then Return result

        ' free Windows resources used by custom cursors
        If HandCurHandle <> 0 Then result = SafeNativeMethods.DestroyCursor(HandCurHandle)
        If BarCurHandle <> 0 Then result = SafeNativeMethods.DestroyCursor(BarCurHandle)
        If PushLtCurHandle <> 0 Then result = SafeNativeMethods.DestroyCursor(PushLtCurHandle)
        If PushRtCurHandle <> 0 Then result = SafeNativeMethods.DestroyCursor(PushRtCurHandle)
        If CrosshairCurHandle <> 0 Then result = SafeNativeMethods.DestroyCursor(CrosshairCurHandle)
        If DiscCurHandle <> 0 Then result = SafeNativeMethods.DestroyCursor(DiscCurHandle)
        If TargetCurHandle <> 0 Then result = SafeNativeMethods.DestroyCursor(TargetCurHandle)

        ' decrement load count of RtChart DLL (VBX)
        SafeNativeMethods.FreeLibrary(CursorDLLhInst)

        CursorDLLhInst = 0
        Return result
    End Function

End Module

