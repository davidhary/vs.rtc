# Real Time Chart

Encapsulates Scientific Software tools (www.sstnet.com) Real Timer Chart part of Lab Objects.

<a name="Pre-Requisites"></a>
### Pre-Requisites

### .Net Framework 4.7.2
[Microsoft /.NET Framework](https://dotnet.microsoft.com/download)
.NET Framework must installed before proceeding with this installation.

### Real Time Chart
[Real Time Chart](http://www.sstnet.com) 3.21 and above is required 

<a name="Source-Code"></a>
## Source Code
Clone the repository along with its requisite repositories to their respective relative path.

### Repositories
The repositories listed in [external repositories](ExternalReposCommits.csv) are required:
* [RTC](https://www.bitbucket.org/davidhary/vs.rtc) - Core Libraries

```
git clone git@bitbucket.org:davidhary/vs.rtc.git
```

Clone the repositories into the following folders (parents of the .git folder):
```
.\Libraries\VS\Visuals\rtc
```


Requires installation and registration of the Lab Objects Real Time Chart

## Built, Tested and Facilitated By

* [Visual Studio](https://www.visualstudIO.com/) - Visual Studio 2015
* [Jarte](https://www.jarte.com/) - RTF Editor
* [Wix Installer](https://www.wixtoolset.org/) - Wix Toolset
* [Atomineer Code Documentation](https://www.atomineerutils.com/) - Code Documentation
* [EW Software](https://github.com/EWSoftware/VSSpellChecker/wiki/) - Spell Checker

## Authors

* **Roy Furman**(https://www.sstnet.com) - creator
* **David Hary** - *Initial Workarounds* - [ATE Coder](https://www.IntegratedScientificResources.com)

#### Global Configuration Files
ISR libraries use a global editor configuration file and a global test run settings file. 
These files can be found in the [IDE Repository]((https://www.bitbucket.org/davidhary/vs.ide)).

Restoring Editor Configuration assuming c:\My is the root folder of the .NET solutions):
```
xcopy /Y c:\My\.editorconfig c:\My\.editorconfig.bak
xcopy /Y c:\My\Libraries\VS\Core\IDE\code\.editorconfig c:\My\.editorconfig
```

Restoring Run Settings assuming c:\user\<me> is the root user folder:
```
xcopy /Y c:\user\<me>\.runsettings c:\user\<me>\.runsettings.bak
xcopy /Y c:\My\Libraries\VS\Core\IDE\code\.runsettings c:\user\<me>\.runsettings
```

<a name="FacilitatedBy"></a>
## Facilitated By
* [Visual Studio](https://www.visualstudio.com/) - Visual Studio
* [Jarte](https://www.jarte.com/) - RTF Editor
* [Wix Installer](https://www.wixtoolset.org/) - WiX Toolset
* [Atomineer Code Documentation](https://www.atomineerutils.com/) - Code Documentation
* [EW Software](https://github.com/EWSoftware/VSSpellChecker/wiki/) - Spell Checker
* [Code Converter](https://github.com/icsharpcode/CodeConverter) - Code Converter
* [Search and Replace](http://www.funduc.com/search_replace.htm) - Funduc Search and Replace for Windows
* [Scientific Software Tools](http://www.sstool.com)

<a name="Authors"></a>
## Authors
* [ATE Coder](https://www.IntegratedScientificResources.com)

<a name="Acknowledgments"></a>
## Acknowledgments
* [Its all a remix](https://www.everythingisaremix.info) -- we are but a spec on the shoulders of giants
* [John Simmons](https://www.codeproject.com/script/Membership/View.aspx?mid=7741) - outlaw programmer
* [Stack overflow](https://www.stackoveflow.com) - Joel Spolsky

<a name="Open-Source"></a>
### Open source
Open source used by this software is described and licensed at the
following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)

<a name="Closed-software"></a>
### Closed software
Closed software used by this software are described and licensed on
the following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[Scientific Software Tools](http://www.sstnet.com)
