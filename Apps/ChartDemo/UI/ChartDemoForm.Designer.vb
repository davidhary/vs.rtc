<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ChartDemoForm

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Private _ToolTip As System.Windows.Forms.ToolTip
    Private WithEvents _YCursorPositionTextBox As System.Windows.Forms.TextBox
    Private WithEvents _XCursorPositionTextBox As System.Windows.Forms.TextBox
    Private WithEvents _YCursorPositionTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _XCursorPositionTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _MouseGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _SamplesCountTextBox As System.Windows.Forms.TextBox
    Private WithEvents _ChannelCountLabel As System.Windows.Forms.Label
    Private WithEvents _SamplesCountTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _ChannelsCountTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _DataArrayGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _SamplesPerDivisionTextBox As System.Windows.Forms.TextBox
    Private WithEvents _VoltsPerDivisionTextBox As System.Windows.Forms.TextBox
    Private WithEvents _SamplesPerDivisionTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _VoltsPerDivisionTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _WindowGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _ZeroChannelsButton As System.Windows.Forms.Button
    Private WithEvents _CurrentChannelTextBox As System.Windows.Forms.TextBox
    Private WithEvents _XOffsetTextBox As System.Windows.Forms.TextBox
    Private WithEvents _YOffsetTextBox As System.Windows.Forms.TextBox
    Private WithEvents _XTraceLockToggle As System.Windows.Forms.CheckBox
    Private WithEvents _YTraceLockToggle As System.Windows.Forms.CheckBox
    Private WithEvents _ChannelTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _YOffsetTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _XOffsetTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _OffsetGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _X3PositionTextBox As System.Windows.Forms.TextBox
    Private WithEvents _X4PositionTextBox As System.Windows.Forms.TextBox
    Private WithEvents _Y2PositionTextBox As System.Windows.Forms.TextBox
    Private WithEvents _Y1PositionTextBox As System.Windows.Forms.TextBox
    Private WithEvents _DeltaTimeTextBox As System.Windows.Forms.TextBox
    Private WithEvents _DeltaVoltsTextBox As System.Windows.Forms.TextBox
    Private WithEvents _AutoTackToggle As System.Windows.Forms.CheckBox
    Private WithEvents _CursorsToggle As System.Windows.Forms.CheckBox
    Private WithEvents _X3PositionTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _X4PositionTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _Y2PositionTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _Y1PositionTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _DeltaTimeTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _DeltaVoltsTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _CursorFrame As System.Windows.Forms.GroupBox
    Private WithEvents _AreaDisplayStyleRadioButton As System.Windows.Forms.RadioButton
    Private WithEvents _LineDisplayStyleRadioButton As System.Windows.Forms.RadioButton
    Private WithEvents _PointsDisplayStyleRadioButton As System.Windows.Forms.RadioButton
    Private WithEvents _DisplayStyleFrame As System.Windows.Forms.GroupBox
    Private WithEvents _FrameToggle As System.Windows.Forms.CheckBox
    Private WithEvents _AxesToggle As System.Windows.Forms.CheckBox
    Private WithEvents _GridToggle As System.Windows.Forms.CheckBox
    Private WithEvents _GraticuleFrame As System.Windows.Forms.GroupBox
    Private WithEvents _ViewportsTextBox As System.Windows.Forms.TextBox
    Private WithEvents _StorageToggle As System.Windows.Forms.CheckBox
    Private WithEvents _ViewportsGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _EraseAllChannelsButton As System.Windows.Forms.Button
    Private WithEvents _PlotXYButton As System.Windows.Forms.Button
    Private WithEvents _SingleButton As System.Windows.Forms.Button
    Private WithEvents _StartStopButton As System.Windows.Forms.Button
    Private WithEvents _PointsButton As System.Windows.Forms.Button
    Private WithEvents _PenColorButton As System.Windows.Forms.Button
    Private WithEvents _InitializeButton As System.Windows.Forms.Button
    Private WithEvents _ScrollButton As System.Windows.Forms.Button
    Private WithEvents _ExitButton As System.Windows.Forms.Button
    Private WithEvents _HorizontalScrollBar As System.Windows.Forms.HScrollBar
    Private WithEvents _VerticalScrollBar As System.Windows.Forms.VScrollBar
    Private WithEvents _Chart As isr.Visuals.RealTimeChart.RealTimeChartControl
    Private WithEvents _Panel As System.Windows.Forms.Panel
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ChartDemoForm))
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._Panel = New System.Windows.Forms.Panel()
        Me._MouseGroupBox = New System.Windows.Forms.GroupBox()
        Me._YCursorPositionTextBox = New System.Windows.Forms.TextBox()
        Me._XCursorPositionTextBox = New System.Windows.Forms.TextBox()
        Me._YCursorPositionTextBoxLabel = New System.Windows.Forms.Label()
        Me._XCursorPositionTextBoxLabel = New System.Windows.Forms.Label()
        Me._DataArrayGroupBox = New System.Windows.Forms.GroupBox()
        Me._SamplesCountTextBox = New System.Windows.Forms.TextBox()
        Me._ChannelCountLabel = New System.Windows.Forms.Label()
        Me._SamplesCountTextBoxLabel = New System.Windows.Forms.Label()
        Me._ChannelsCountTextBoxLabel = New System.Windows.Forms.Label()
        Me._WindowGroupBox = New System.Windows.Forms.GroupBox()
        Me._SamplesPerDivisionTextBox = New System.Windows.Forms.TextBox()
        Me._VoltsPerDivisionTextBox = New System.Windows.Forms.TextBox()
        Me._SamplesPerDivisionTextBoxLabel = New System.Windows.Forms.Label()
        Me._VoltsPerDivisionTextBoxLabel = New System.Windows.Forms.Label()
        Me._OffsetGroupBox = New System.Windows.Forms.GroupBox()
        Me._ZeroChannelsButton = New System.Windows.Forms.Button()
        Me._CurrentChannelTextBox = New System.Windows.Forms.TextBox()
        Me._XOffsetTextBox = New System.Windows.Forms.TextBox()
        Me._YOffsetTextBox = New System.Windows.Forms.TextBox()
        Me._XTraceLockToggle = New System.Windows.Forms.CheckBox()
        Me._YTraceLockToggle = New System.Windows.Forms.CheckBox()
        Me._ChannelTextBoxLabel = New System.Windows.Forms.Label()
        Me._YOffsetTextBoxLabel = New System.Windows.Forms.Label()
        Me._XOffsetTextBoxLabel = New System.Windows.Forms.Label()
        Me._CursorFrame = New System.Windows.Forms.GroupBox()
        Me._X3PositionTextBox = New System.Windows.Forms.TextBox()
        Me._X4PositionTextBox = New System.Windows.Forms.TextBox()
        Me._Y2PositionTextBox = New System.Windows.Forms.TextBox()
        Me._Y1PositionTextBox = New System.Windows.Forms.TextBox()
        Me._DeltaTimeTextBox = New System.Windows.Forms.TextBox()
        Me._DeltaVoltsTextBox = New System.Windows.Forms.TextBox()
        Me._AutoTackToggle = New System.Windows.Forms.CheckBox()
        Me._CursorsToggle = New System.Windows.Forms.CheckBox()
        Me._X3PositionTextBoxLabel = New System.Windows.Forms.Label()
        Me._X4PositionTextBoxLabel = New System.Windows.Forms.Label()
        Me._Y2PositionTextBoxLabel = New System.Windows.Forms.Label()
        Me._Y1PositionTextBoxLabel = New System.Windows.Forms.Label()
        Me._DeltaTimeTextBoxLabel = New System.Windows.Forms.Label()
        Me._DeltaVoltsTextBoxLabel = New System.Windows.Forms.Label()
        Me._DisplayStyleFrame = New System.Windows.Forms.GroupBox()
        Me._AreaDisplayStyleRadioButton = New System.Windows.Forms.RadioButton()
        Me._LineDisplayStyleRadioButton = New System.Windows.Forms.RadioButton()
        Me._PointsDisplayStyleRadioButton = New System.Windows.Forms.RadioButton()
        Me._GraticuleFrame = New System.Windows.Forms.GroupBox()
        Me._FrameToggle = New System.Windows.Forms.CheckBox()
        Me._AxesToggle = New System.Windows.Forms.CheckBox()
        Me._GridToggle = New System.Windows.Forms.CheckBox()
        Me._ViewportsGroupBox = New System.Windows.Forms.GroupBox()
        Me._ViewportsTextBox = New System.Windows.Forms.TextBox()
        Me._StorageToggle = New System.Windows.Forms.CheckBox()
        Me._EraseAllChannelsButton = New System.Windows.Forms.Button()
        Me._PlotXYButton = New System.Windows.Forms.Button()
        Me._SingleButton = New System.Windows.Forms.Button()
        Me._StartStopButton = New System.Windows.Forms.Button()
        Me._PointsButton = New System.Windows.Forms.Button()
        Me._PenColorButton = New System.Windows.Forms.Button()
        Me._InitializeButton = New System.Windows.Forms.Button()
        Me._ScrollButton = New System.Windows.Forms.Button()
        Me._ExitButton = New System.Windows.Forms.Button()
        Me._HorizontalScrollBar = New System.Windows.Forms.HScrollBar()
        Me._VerticalScrollBar = New System.Windows.Forms.VScrollBar()
        Me._Chart = New isr.Visuals.RealTimeChart.RealTimeChartControl()
        Me._Panel.SuspendLayout()
        Me._MouseGroupBox.SuspendLayout()
        Me._DataArrayGroupBox.SuspendLayout()
        Me._WindowGroupBox.SuspendLayout()
        Me._OffsetGroupBox.SuspendLayout()
        Me._CursorFrame.SuspendLayout()
        Me._DisplayStyleFrame.SuspendLayout()
        Me._GraticuleFrame.SuspendLayout()
        Me._ViewportsGroupBox.SuspendLayout()
        Me.SuspendLayout()
        '
        '_Panel
        '
        Me._Panel.AllowDrop = True
        Me._Panel.BackColor = System.Drawing.SystemColors.Control
        Me._Panel.Controls.Add(Me._MouseGroupBox)
        Me._Panel.Controls.Add(Me._DataArrayGroupBox)
        Me._Panel.Controls.Add(Me._WindowGroupBox)
        Me._Panel.Controls.Add(Me._OffsetGroupBox)
        Me._Panel.Controls.Add(Me._CursorFrame)
        Me._Panel.Controls.Add(Me._DisplayStyleFrame)
        Me._Panel.Controls.Add(Me._GraticuleFrame)
        Me._Panel.Controls.Add(Me._ViewportsGroupBox)
        Me._Panel.Controls.Add(Me._EraseAllChannelsButton)
        Me._Panel.Controls.Add(Me._PlotXYButton)
        Me._Panel.Controls.Add(Me._SingleButton)
        Me._Panel.Controls.Add(Me._StartStopButton)
        Me._Panel.Controls.Add(Me._PointsButton)
        Me._Panel.Controls.Add(Me._PenColorButton)
        Me._Panel.Controls.Add(Me._InitializeButton)
        Me._Panel.Controls.Add(Me._ScrollButton)
        Me._Panel.Controls.Add(Me._ExitButton)
        Me._Panel.Controls.Add(Me._HorizontalScrollBar)
        Me._Panel.Controls.Add(Me._VerticalScrollBar)
        Me._Panel.Controls.Add(Me._Chart)
        Me._Panel.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._Panel.Location = New System.Drawing.Point(0, 0)
        Me._Panel.Name = "_Panel"
        Me._Panel.Size = New System.Drawing.Size(474, 451)
        Me._Panel.TabIndex = 11
        '
        '_MouseGroupBox
        '
        Me._MouseGroupBox.AllowDrop = True
        Me._MouseGroupBox.BackColor = System.Drawing.Color.Transparent
        Me._MouseGroupBox.Controls.Add(Me._YCursorPositionTextBox)
        Me._MouseGroupBox.Controls.Add(Me._XCursorPositionTextBox)
        Me._MouseGroupBox.Controls.Add(Me._YCursorPositionTextBoxLabel)
        Me._MouseGroupBox.Controls.Add(Me._XCursorPositionTextBoxLabel)
        Me._MouseGroupBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me._MouseGroupBox.Location = New System.Drawing.Point(395, 8)
        Me._MouseGroupBox.Name = "_MouseGroupBox"
        Me._MouseGroupBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._MouseGroupBox.Size = New System.Drawing.Size(73, 107)
        Me._MouseGroupBox.TabIndex = 58
        Me._MouseGroupBox.TabStop = False
        Me._MouseGroupBox.Text = "Mouse"
        '
        '_YCursorPositionTextBox
        '
        Me._YCursorPositionTextBox.AcceptsReturn = True
        Me._YCursorPositionTextBox.AllowDrop = True
        Me._YCursorPositionTextBox.BackColor = System.Drawing.SystemColors.Control
        Me._YCursorPositionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._YCursorPositionTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._YCursorPositionTextBox.ForeColor = System.Drawing.Color.Black
        Me._YCursorPositionTextBox.Location = New System.Drawing.Point(8, 36)
        Me._YCursorPositionTextBox.MaxLength = 0
        Me._YCursorPositionTextBox.Name = "_YCursorPositionTextBox"
        Me._YCursorPositionTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._YCursorPositionTextBox.Size = New System.Drawing.Size(53, 19)
        Me._YCursorPositionTextBox.TabIndex = 60
        Me._YCursorPositionTextBox.TabStop = False
        Me._YCursorPositionTextBox.Text = "1"
        '
        '_XCursorPositionTextBox
        '
        Me._XCursorPositionTextBox.AcceptsReturn = True
        Me._XCursorPositionTextBox.AllowDrop = True
        Me._XCursorPositionTextBox.BackColor = System.Drawing.SystemColors.Control
        Me._XCursorPositionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._XCursorPositionTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._XCursorPositionTextBox.ForeColor = System.Drawing.Color.Black
        Me._XCursorPositionTextBox.Location = New System.Drawing.Point(8, 80)
        Me._XCursorPositionTextBox.MaxLength = 0
        Me._XCursorPositionTextBox.Name = "_XCursorPositionTextBox"
        Me._XCursorPositionTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._XCursorPositionTextBox.Size = New System.Drawing.Size(53, 19)
        Me._XCursorPositionTextBox.TabIndex = 59
        Me._XCursorPositionTextBox.TabStop = False
        Me._XCursorPositionTextBox.Text = "1"
        '
        '_YCursorPositionTextBoxLabel
        '
        Me._YCursorPositionTextBoxLabel.AllowDrop = True
        Me._YCursorPositionTextBoxLabel.AutoSize = True
        Me._YCursorPositionTextBoxLabel.BackColor = System.Drawing.SystemColors.Control
        Me._YCursorPositionTextBoxLabel.ForeColor = System.Drawing.Color.Black
        Me._YCursorPositionTextBoxLabel.Location = New System.Drawing.Point(8, 20)
        Me._YCursorPositionTextBoxLabel.Name = "_YCursorPositionTextBoxLabel"
        Me._YCursorPositionTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._YCursorPositionTextBoxLabel.Size = New System.Drawing.Size(52, 13)
        Me._YCursorPositionTextBoxLabel.TabIndex = 62
        Me._YCursorPositionTextBoxLabel.Text = "Y Twips"
        '
        '_XCursorPositionTextBoxLabel
        '
        Me._XCursorPositionTextBoxLabel.AllowDrop = True
        Me._XCursorPositionTextBoxLabel.AutoSize = True
        Me._XCursorPositionTextBoxLabel.BackColor = System.Drawing.SystemColors.Control
        Me._XCursorPositionTextBoxLabel.ForeColor = System.Drawing.Color.Black
        Me._XCursorPositionTextBoxLabel.Location = New System.Drawing.Point(8, 64)
        Me._XCursorPositionTextBoxLabel.Name = "_XCursorPositionTextBoxLabel"
        Me._XCursorPositionTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._XCursorPositionTextBoxLabel.Size = New System.Drawing.Size(52, 13)
        Me._XCursorPositionTextBoxLabel.TabIndex = 61
        Me._XCursorPositionTextBoxLabel.Text = "X Twips"
        '
        '_DataArrayGroupBox
        '
        Me._DataArrayGroupBox.AllowDrop = True
        Me._DataArrayGroupBox.BackColor = System.Drawing.Color.Transparent
        Me._DataArrayGroupBox.Controls.Add(Me._SamplesCountTextBox)
        Me._DataArrayGroupBox.Controls.Add(Me._ChannelCountLabel)
        Me._DataArrayGroupBox.Controls.Add(Me._SamplesCountTextBoxLabel)
        Me._DataArrayGroupBox.Controls.Add(Me._ChannelsCountTextBoxLabel)
        Me._DataArrayGroupBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me._DataArrayGroupBox.Location = New System.Drawing.Point(228, 381)
        Me._DataArrayGroupBox.Name = "_DataArrayGroupBox"
        Me._DataArrayGroupBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._DataArrayGroupBox.Size = New System.Drawing.Size(156, 63)
        Me._DataArrayGroupBox.TabIndex = 53
        Me._DataArrayGroupBox.TabStop = False
        Me._DataArrayGroupBox.Text = "Data Array"
        '
        '_SamplesCountTextBox
        '
        Me._SamplesCountTextBox.AcceptsReturn = True
        Me._SamplesCountTextBox.AllowDrop = True
        Me._SamplesCountTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._SamplesCountTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._SamplesCountTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._SamplesCountTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.6!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SamplesCountTextBox.ForeColor = System.Drawing.Color.Black
        Me._SamplesCountTextBox.Location = New System.Drawing.Point(84, 30)
        Me._SamplesCountTextBox.MaxLength = 6
        Me._SamplesCountTextBox.Name = "_SamplesCountTextBox"
        Me._SamplesCountTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._SamplesCountTextBox.Size = New System.Drawing.Size(61, 22)
        Me._SamplesCountTextBox.TabIndex = 54
        Me._SamplesCountTextBox.Text = "1000"
        Me._SamplesCountTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        '_ChannelCountLabel
        '
        Me._ChannelCountLabel.AllowDrop = True
        Me._ChannelCountLabel.BackColor = System.Drawing.SystemColors.Control
        Me._ChannelCountLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._ChannelCountLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.6!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ChannelCountLabel.ForeColor = System.Drawing.Color.Black
        Me._ChannelCountLabel.Location = New System.Drawing.Point(16, 31)
        Me._ChannelCountLabel.Name = "_ChannelCountLabel"
        Me._ChannelCountLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ChannelCountLabel.Size = New System.Drawing.Size(49, 20)
        Me._ChannelCountLabel.TabIndex = 57
        Me._ChannelCountLabel.Text = "1"
        Me._ChannelCountLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        '_SamplesCountTextBoxLabel
        '
        Me._SamplesCountTextBoxLabel.AllowDrop = True
        Me._SamplesCountTextBoxLabel.AutoSize = True
        Me._SamplesCountTextBoxLabel.BackColor = System.Drawing.SystemColors.Control
        Me._SamplesCountTextBoxLabel.ForeColor = System.Drawing.Color.Black
        Me._SamplesCountTextBoxLabel.Location = New System.Drawing.Point(84, 16)
        Me._SamplesCountTextBoxLabel.Name = "_SamplesCountTextBoxLabel"
        Me._SamplesCountTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._SamplesCountTextBoxLabel.Size = New System.Drawing.Size(54, 13)
        Me._SamplesCountTextBoxLabel.TabIndex = 56
        Me._SamplesCountTextBoxLabel.Text = "Samples"
        '
        '_ChannelsCountTextBoxLabel
        '
        Me._ChannelsCountTextBoxLabel.AllowDrop = True
        Me._ChannelsCountTextBoxLabel.AutoSize = True
        Me._ChannelsCountTextBoxLabel.BackColor = System.Drawing.SystemColors.Control
        Me._ChannelsCountTextBoxLabel.ForeColor = System.Drawing.Color.Black
        Me._ChannelsCountTextBoxLabel.Location = New System.Drawing.Point(14, 16)
        Me._ChannelsCountTextBoxLabel.Name = "_ChannelsCountTextBoxLabel"
        Me._ChannelsCountTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ChannelsCountTextBoxLabel.Size = New System.Drawing.Size(59, 13)
        Me._ChannelsCountTextBoxLabel.TabIndex = 55
        Me._ChannelsCountTextBoxLabel.Text = "Channels"
        '
        '_WindowGroupBox
        '
        Me._WindowGroupBox.AllowDrop = True
        Me._WindowGroupBox.BackColor = System.Drawing.Color.Transparent
        Me._WindowGroupBox.Controls.Add(Me._SamplesPerDivisionTextBox)
        Me._WindowGroupBox.Controls.Add(Me._VoltsPerDivisionTextBox)
        Me._WindowGroupBox.Controls.Add(Me._SamplesPerDivisionTextBoxLabel)
        Me._WindowGroupBox.Controls.Add(Me._VoltsPerDivisionTextBoxLabel)
        Me._WindowGroupBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me._WindowGroupBox.Location = New System.Drawing.Point(8, 381)
        Me._WindowGroupBox.Name = "_WindowGroupBox"
        Me._WindowGroupBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._WindowGroupBox.Size = New System.Drawing.Size(171, 63)
        Me._WindowGroupBox.TabIndex = 48
        Me._WindowGroupBox.TabStop = False
        Me._WindowGroupBox.Text = "Window"
        '
        '_SamplesPerDivisionTextBox
        '
        Me._SamplesPerDivisionTextBox.AcceptsReturn = True
        Me._SamplesPerDivisionTextBox.AllowDrop = True
        Me._SamplesPerDivisionTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._SamplesPerDivisionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._SamplesPerDivisionTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._SamplesPerDivisionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.6!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SamplesPerDivisionTextBox.ForeColor = System.Drawing.Color.Black
        Me._SamplesPerDivisionTextBox.Location = New System.Drawing.Point(89, 30)
        Me._SamplesPerDivisionTextBox.MaxLength = 7
        Me._SamplesPerDivisionTextBox.Name = "_SamplesPerDivisionTextBox"
        Me._SamplesPerDivisionTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._SamplesPerDivisionTextBox.Size = New System.Drawing.Size(73, 22)
        Me._SamplesPerDivisionTextBox.TabIndex = 50
        Me._SamplesPerDivisionTextBox.Text = "100"
        Me._SamplesPerDivisionTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        '_VoltsPerDivisionTextBox
        '
        Me._VoltsPerDivisionTextBox.AcceptsReturn = True
        Me._VoltsPerDivisionTextBox.AllowDrop = True
        Me._VoltsPerDivisionTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._VoltsPerDivisionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._VoltsPerDivisionTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._VoltsPerDivisionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.6!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._VoltsPerDivisionTextBox.ForeColor = System.Drawing.Color.Black
        Me._VoltsPerDivisionTextBox.Location = New System.Drawing.Point(16, 30)
        Me._VoltsPerDivisionTextBox.MaxLength = 0
        Me._VoltsPerDivisionTextBox.Name = "_VoltsPerDivisionTextBox"
        Me._VoltsPerDivisionTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._VoltsPerDivisionTextBox.Size = New System.Drawing.Size(49, 22)
        Me._VoltsPerDivisionTextBox.TabIndex = 49
        Me._VoltsPerDivisionTextBox.Text = "5"
        Me._VoltsPerDivisionTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        '_SamplesPerDivisionTextBoxLabel
        '
        Me._SamplesPerDivisionTextBoxLabel.AllowDrop = True
        Me._SamplesPerDivisionTextBoxLabel.AutoSize = True
        Me._SamplesPerDivisionTextBoxLabel.BackColor = System.Drawing.SystemColors.Control
        Me._SamplesPerDivisionTextBoxLabel.ForeColor = System.Drawing.Color.Black
        Me._SamplesPerDivisionTextBoxLabel.Location = New System.Drawing.Point(88, 16)
        Me._SamplesPerDivisionTextBoxLabel.Name = "_SamplesPerDivisionTextBoxLabel"
        Me._SamplesPerDivisionTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._SamplesPerDivisionTextBoxLabel.Size = New System.Drawing.Size(79, 13)
        Me._SamplesPerDivisionTextBoxLabel.TabIndex = 52
        Me._SamplesPerDivisionTextBoxLabel.Text = "Samples/Div"
        '
        '_VoltsPerDivisionTextBoxLabel
        '
        Me._VoltsPerDivisionTextBoxLabel.AllowDrop = True
        Me._VoltsPerDivisionTextBoxLabel.AutoSize = True
        Me._VoltsPerDivisionTextBoxLabel.BackColor = System.Drawing.SystemColors.Control
        Me._VoltsPerDivisionTextBoxLabel.ForeColor = System.Drawing.Color.Black
        Me._VoltsPerDivisionTextBoxLabel.Location = New System.Drawing.Point(16, 16)
        Me._VoltsPerDivisionTextBoxLabel.Name = "_VoltsPerDivisionTextBoxLabel"
        Me._VoltsPerDivisionTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._VoltsPerDivisionTextBoxLabel.Size = New System.Drawing.Size(60, 13)
        Me._VoltsPerDivisionTextBoxLabel.TabIndex = 51
        Me._VoltsPerDivisionTextBoxLabel.Text = "Volts/Div"
        '
        '_OffsetGroupBox
        '
        Me._OffsetGroupBox.AllowDrop = True
        Me._OffsetGroupBox.BackColor = System.Drawing.Color.Transparent
        Me._OffsetGroupBox.Controls.Add(Me._ZeroChannelsButton)
        Me._OffsetGroupBox.Controls.Add(Me._CurrentChannelTextBox)
        Me._OffsetGroupBox.Controls.Add(Me._XOffsetTextBox)
        Me._OffsetGroupBox.Controls.Add(Me._YOffsetTextBox)
        Me._OffsetGroupBox.Controls.Add(Me._XTraceLockToggle)
        Me._OffsetGroupBox.Controls.Add(Me._YTraceLockToggle)
        Me._OffsetGroupBox.Controls.Add(Me._ChannelTextBoxLabel)
        Me._OffsetGroupBox.Controls.Add(Me._YOffsetTextBoxLabel)
        Me._OffsetGroupBox.Controls.Add(Me._XOffsetTextBoxLabel)
        Me._OffsetGroupBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me._OffsetGroupBox.Location = New System.Drawing.Point(8, 318)
        Me._OffsetGroupBox.Name = "_OffsetGroupBox"
        Me._OffsetGroupBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._OffsetGroupBox.Size = New System.Drawing.Size(379, 63)
        Me._OffsetGroupBox.TabIndex = 38
        Me._OffsetGroupBox.TabStop = False
        Me._OffsetGroupBox.Text = "Offset"
        '
        '_ZeroChannelsButton
        '
        Me._ZeroChannelsButton.AllowDrop = True
        Me._ZeroChannelsButton.BackColor = System.Drawing.SystemColors.Control
        Me._ZeroChannelsButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._ZeroChannelsButton.Location = New System.Drawing.Point(299, 19)
        Me._ZeroChannelsButton.Name = "_ZeroChannelsButton"
        Me._ZeroChannelsButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ZeroChannelsButton.Size = New System.Drawing.Size(69, 29)
        Me._ZeroChannelsButton.TabIndex = 47
        Me._ZeroChannelsButton.Text = "Zero"
        Me._ZeroChannelsButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me._ZeroChannelsButton.UseVisualStyleBackColor = False
        '
        '_CurrentChannelTextBox
        '
        Me._CurrentChannelTextBox.AcceptsReturn = True
        Me._CurrentChannelTextBox.AllowDrop = True
        Me._CurrentChannelTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._CurrentChannelTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._CurrentChannelTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._CurrentChannelTextBox.ForeColor = System.Drawing.Color.Black
        Me._CurrentChannelTextBox.Location = New System.Drawing.Point(121, 32)
        Me._CurrentChannelTextBox.MaxLength = 0
        Me._CurrentChannelTextBox.Name = "_CurrentChannelTextBox"
        Me._CurrentChannelTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._CurrentChannelTextBox.Size = New System.Drawing.Size(45, 19)
        Me._CurrentChannelTextBox.TabIndex = 43
        Me._CurrentChannelTextBox.TabStop = False
        Me._CurrentChannelTextBox.Text = " 1"
        '
        '_XOffsetTextBox
        '
        Me._XOffsetTextBox.AcceptsReturn = True
        Me._XOffsetTextBox.AllowDrop = True
        Me._XOffsetTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._XOffsetTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._XOffsetTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._XOffsetTextBox.ForeColor = System.Drawing.Color.Black
        Me._XOffsetTextBox.Location = New System.Drawing.Point(237, 32)
        Me._XOffsetTextBox.MaxLength = 0
        Me._XOffsetTextBox.Name = "_XOffsetTextBox"
        Me._XOffsetTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._XOffsetTextBox.Size = New System.Drawing.Size(45, 19)
        Me._XOffsetTextBox.TabIndex = 42
        Me._XOffsetTextBox.TabStop = False
        Me._XOffsetTextBox.Text = "0"
        '
        '_YOffsetTextBox
        '
        Me._YOffsetTextBox.AcceptsReturn = True
        Me._YOffsetTextBox.AllowDrop = True
        Me._YOffsetTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._YOffsetTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._YOffsetTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._YOffsetTextBox.ForeColor = System.Drawing.Color.Black
        Me._YOffsetTextBox.Location = New System.Drawing.Point(179, 32)
        Me._YOffsetTextBox.MaxLength = 0
        Me._YOffsetTextBox.Name = "_YOffsetTextBox"
        Me._YOffsetTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._YOffsetTextBox.Size = New System.Drawing.Size(45, 19)
        Me._YOffsetTextBox.TabIndex = 41
        Me._YOffsetTextBox.TabStop = False
        Me._YOffsetTextBox.Text = "0"
        '
        '_XTraceLockToggle
        '
        Me._XTraceLockToggle.AllowDrop = True
        Me._XTraceLockToggle.AutoSize = True
        Me._XTraceLockToggle.BackColor = System.Drawing.SystemColors.Control
        Me._XTraceLockToggle.ForeColor = System.Drawing.SystemColors.ControlText
        Me._XTraceLockToggle.Location = New System.Drawing.Point(8, 38)
        Me._XTraceLockToggle.Name = "_XTraceLockToggle"
        Me._XTraceLockToggle.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._XTraceLockToggle.Size = New System.Drawing.Size(97, 17)
        Me._XTraceLockToggle.TabIndex = 40
        Me._XTraceLockToggle.Text = "X Drag Lock"
        Me._XTraceLockToggle.UseVisualStyleBackColor = False
        '
        '_YTraceLockToggle
        '
        Me._YTraceLockToggle.AllowDrop = True
        Me._YTraceLockToggle.AutoSize = True
        Me._YTraceLockToggle.BackColor = System.Drawing.SystemColors.Control
        Me._YTraceLockToggle.ForeColor = System.Drawing.SystemColors.ControlText
        Me._YTraceLockToggle.Location = New System.Drawing.Point(8, 16)
        Me._YTraceLockToggle.Name = "_YTraceLockToggle"
        Me._YTraceLockToggle.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._YTraceLockToggle.Size = New System.Drawing.Size(97, 17)
        Me._YTraceLockToggle.TabIndex = 39
        Me._YTraceLockToggle.Text = "Y Drag Lock"
        Me._YTraceLockToggle.UseVisualStyleBackColor = False
        '
        '_ChannelTextBoxLabel
        '
        Me._ChannelTextBoxLabel.AllowDrop = True
        Me._ChannelTextBoxLabel.AutoSize = True
        Me._ChannelTextBoxLabel.BackColor = System.Drawing.SystemColors.Control
        Me._ChannelTextBoxLabel.ForeColor = System.Drawing.Color.Black
        Me._ChannelTextBoxLabel.Location = New System.Drawing.Point(117, 17)
        Me._ChannelTextBoxLabel.Name = "_ChannelTextBoxLabel"
        Me._ChannelTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ChannelTextBoxLabel.Size = New System.Drawing.Size(53, 13)
        Me._ChannelTextBoxLabel.TabIndex = 46
        Me._ChannelTextBoxLabel.Text = "Channel"
        '
        '_YOffsetTextBoxLabel
        '
        Me._YOffsetTextBoxLabel.AllowDrop = True
        Me._YOffsetTextBoxLabel.AutoSize = True
        Me._YOffsetTextBoxLabel.BackColor = System.Drawing.SystemColors.Control
        Me._YOffsetTextBoxLabel.ForeColor = System.Drawing.Color.Black
        Me._YOffsetTextBoxLabel.Location = New System.Drawing.Point(175, 16)
        Me._YOffsetTextBoxLabel.Name = "_YOffsetTextBoxLabel"
        Me._YOffsetTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._YOffsetTextBoxLabel.Size = New System.Drawing.Size(53, 13)
        Me._YOffsetTextBoxLabel.TabIndex = 45
        Me._YOffsetTextBoxLabel.Text = "Y Offset"
        '
        '_XOffsetTextBoxLabel
        '
        Me._XOffsetTextBoxLabel.AllowDrop = True
        Me._XOffsetTextBoxLabel.AutoSize = True
        Me._XOffsetTextBoxLabel.BackColor = System.Drawing.SystemColors.Control
        Me._XOffsetTextBoxLabel.ForeColor = System.Drawing.Color.Black
        Me._XOffsetTextBoxLabel.Location = New System.Drawing.Point(233, 16)
        Me._XOffsetTextBoxLabel.Name = "_XOffsetTextBoxLabel"
        Me._XOffsetTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._XOffsetTextBoxLabel.Size = New System.Drawing.Size(53, 13)
        Me._XOffsetTextBoxLabel.TabIndex = 44
        Me._XOffsetTextBoxLabel.Text = "X Offset"
        '
        '_CursorFrame
        '
        Me._CursorFrame.AllowDrop = True
        Me._CursorFrame.BackColor = System.Drawing.Color.Transparent
        Me._CursorFrame.Controls.Add(Me._X3PositionTextBox)
        Me._CursorFrame.Controls.Add(Me._X4PositionTextBox)
        Me._CursorFrame.Controls.Add(Me._Y2PositionTextBox)
        Me._CursorFrame.Controls.Add(Me._Y1PositionTextBox)
        Me._CursorFrame.Controls.Add(Me._DeltaTimeTextBox)
        Me._CursorFrame.Controls.Add(Me._DeltaVoltsTextBox)
        Me._CursorFrame.Controls.Add(Me._AutoTackToggle)
        Me._CursorFrame.Controls.Add(Me._CursorsToggle)
        Me._CursorFrame.Controls.Add(Me._X3PositionTextBoxLabel)
        Me._CursorFrame.Controls.Add(Me._X4PositionTextBoxLabel)
        Me._CursorFrame.Controls.Add(Me._Y2PositionTextBoxLabel)
        Me._CursorFrame.Controls.Add(Me._Y1PositionTextBoxLabel)
        Me._CursorFrame.Controls.Add(Me._DeltaTimeTextBoxLabel)
        Me._CursorFrame.Controls.Add(Me._DeltaVoltsTextBoxLabel)
        Me._CursorFrame.ForeColor = System.Drawing.SystemColors.ControlText
        Me._CursorFrame.Location = New System.Drawing.Point(8, 254)
        Me._CursorFrame.Name = "_CursorFrame"
        Me._CursorFrame.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._CursorFrame.Size = New System.Drawing.Size(379, 63)
        Me._CursorFrame.TabIndex = 23
        Me._CursorFrame.TabStop = False
        Me._CursorFrame.Text = "Cursor"
        '
        '_X3PositionTextBox
        '
        Me._X3PositionTextBox.AcceptsReturn = True
        Me._X3PositionTextBox.AllowDrop = True
        Me._X3PositionTextBox.BackColor = System.Drawing.SystemColors.Control
        Me._X3PositionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._X3PositionTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._X3PositionTextBox.ForeColor = System.Drawing.Color.Black
        Me._X3PositionTextBox.Location = New System.Drawing.Point(240, 32)
        Me._X3PositionTextBox.MaxLength = 0
        Me._X3PositionTextBox.Name = "_X3PositionTextBox"
        Me._X3PositionTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._X3PositionTextBox.Size = New System.Drawing.Size(41, 19)
        Me._X3PositionTextBox.TabIndex = 31
        Me._X3PositionTextBox.TabStop = False
        Me._X3PositionTextBox.Text = "0"
        '
        '_X4PositionTextBox
        '
        Me._X4PositionTextBox.AcceptsReturn = True
        Me._X4PositionTextBox.AllowDrop = True
        Me._X4PositionTextBox.BackColor = System.Drawing.SystemColors.Control
        Me._X4PositionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._X4PositionTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._X4PositionTextBox.ForeColor = System.Drawing.Color.Black
        Me._X4PositionTextBox.Location = New System.Drawing.Point(284, 32)
        Me._X4PositionTextBox.MaxLength = 0
        Me._X4PositionTextBox.Name = "_X4PositionTextBox"
        Me._X4PositionTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._X4PositionTextBox.Size = New System.Drawing.Size(41, 19)
        Me._X4PositionTextBox.TabIndex = 30
        Me._X4PositionTextBox.TabStop = False
        Me._X4PositionTextBox.Text = "0"
        '
        '_Y2PositionTextBox
        '
        Me._Y2PositionTextBox.AcceptsReturn = True
        Me._Y2PositionTextBox.AllowDrop = True
        Me._Y2PositionTextBox.BackColor = System.Drawing.SystemColors.Control
        Me._Y2PositionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._Y2PositionTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._Y2PositionTextBox.ForeColor = System.Drawing.Color.Black
        Me._Y2PositionTextBox.Location = New System.Drawing.Point(141, 32)
        Me._Y2PositionTextBox.MaxLength = 0
        Me._Y2PositionTextBox.Name = "_Y2PositionTextBox"
        Me._Y2PositionTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._Y2PositionTextBox.Size = New System.Drawing.Size(41, 19)
        Me._Y2PositionTextBox.TabIndex = 29
        Me._Y2PositionTextBox.TabStop = False
        Me._Y2PositionTextBox.Text = "0"
        '
        '_Y1PositionTextBox
        '
        Me._Y1PositionTextBox.AcceptsReturn = True
        Me._Y1PositionTextBox.AllowDrop = True
        Me._Y1PositionTextBox.BackColor = System.Drawing.SystemColors.Control
        Me._Y1PositionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._Y1PositionTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._Y1PositionTextBox.ForeColor = System.Drawing.Color.Black
        Me._Y1PositionTextBox.Location = New System.Drawing.Point(97, 32)
        Me._Y1PositionTextBox.MaxLength = 0
        Me._Y1PositionTextBox.Name = "_Y1PositionTextBox"
        Me._Y1PositionTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._Y1PositionTextBox.Size = New System.Drawing.Size(41, 19)
        Me._Y1PositionTextBox.TabIndex = 28
        Me._Y1PositionTextBox.TabStop = False
        Me._Y1PositionTextBox.Text = "0"
        '
        '_DeltaTimeTextBox
        '
        Me._DeltaTimeTextBox.AcceptsReturn = True
        Me._DeltaTimeTextBox.AllowDrop = True
        Me._DeltaTimeTextBox.BackColor = System.Drawing.SystemColors.Control
        Me._DeltaTimeTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._DeltaTimeTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._DeltaTimeTextBox.ForeColor = System.Drawing.Color.Black
        Me._DeltaTimeTextBox.Location = New System.Drawing.Point(328, 32)
        Me._DeltaTimeTextBox.MaxLength = 0
        Me._DeltaTimeTextBox.Name = "_DeltaTimeTextBox"
        Me._DeltaTimeTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._DeltaTimeTextBox.Size = New System.Drawing.Size(41, 19)
        Me._DeltaTimeTextBox.TabIndex = 27
        Me._DeltaTimeTextBox.TabStop = False
        Me._DeltaTimeTextBox.Text = "0"
        '
        '_DeltaVoltsTextBox
        '
        Me._DeltaVoltsTextBox.AcceptsReturn = True
        Me._DeltaVoltsTextBox.AllowDrop = True
        Me._DeltaVoltsTextBox.BackColor = System.Drawing.SystemColors.Control
        Me._DeltaVoltsTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._DeltaVoltsTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._DeltaVoltsTextBox.ForeColor = System.Drawing.Color.Black
        Me._DeltaVoltsTextBox.Location = New System.Drawing.Point(185, 32)
        Me._DeltaVoltsTextBox.MaxLength = 0
        Me._DeltaVoltsTextBox.Name = "_DeltaVoltsTextBox"
        Me._DeltaVoltsTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._DeltaVoltsTextBox.Size = New System.Drawing.Size(41, 19)
        Me._DeltaVoltsTextBox.TabIndex = 26
        Me._DeltaVoltsTextBox.TabStop = False
        Me._DeltaVoltsTextBox.Text = "0"
        '
        '_AutoTackToggle
        '
        Me._AutoTackToggle.AllowDrop = True
        Me._AutoTackToggle.AutoSize = True
        Me._AutoTackToggle.BackColor = System.Drawing.SystemColors.Control
        Me._AutoTackToggle.ForeColor = System.Drawing.SystemColors.ControlText
        Me._AutoTackToggle.Location = New System.Drawing.Point(8, 38)
        Me._AutoTackToggle.Name = "_AutoTackToggle"
        Me._AutoTackToggle.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._AutoTackToggle.Size = New System.Drawing.Size(89, 17)
        Me._AutoTackToggle.TabIndex = 25
        Me._AutoTackToggle.Text = "Auto Track"
        Me._AutoTackToggle.UseVisualStyleBackColor = False
        '
        '_CursorsToggle
        '
        Me._CursorsToggle.AllowDrop = True
        Me._CursorsToggle.AutoSize = True
        Me._CursorsToggle.BackColor = System.Drawing.SystemColors.Control
        Me._CursorsToggle.ForeColor = System.Drawing.SystemColors.ControlText
        Me._CursorsToggle.Location = New System.Drawing.Point(8, 16)
        Me._CursorsToggle.Name = "_CursorsToggle"
        Me._CursorsToggle.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._CursorsToggle.Size = New System.Drawing.Size(65, 17)
        Me._CursorsToggle.TabIndex = 24
        Me._CursorsToggle.Text = "Enable"
        Me._CursorsToggle.UseVisualStyleBackColor = False
        '
        '_X3PositionTextBoxLabel
        '
        Me._X3PositionTextBoxLabel.AllowDrop = True
        Me._X3PositionTextBoxLabel.AutoSize = True
        Me._X3PositionTextBoxLabel.BackColor = System.Drawing.SystemColors.Control
        Me._X3PositionTextBoxLabel.ForeColor = System.Drawing.Color.Black
        Me._X3PositionTextBoxLabel.Location = New System.Drawing.Point(240, 16)
        Me._X3PositionTextBoxLabel.Name = "_X3PositionTextBoxLabel"
        Me._X3PositionTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._X3PositionTextBoxLabel.Size = New System.Drawing.Size(22, 13)
        Me._X3PositionTextBoxLabel.TabIndex = 37
        Me._X3PositionTextBoxLabel.Text = "X3"
        '
        '_X4PositionTextBoxLabel
        '
        Me._X4PositionTextBoxLabel.AllowDrop = True
        Me._X4PositionTextBoxLabel.AutoSize = True
        Me._X4PositionTextBoxLabel.BackColor = System.Drawing.SystemColors.Control
        Me._X4PositionTextBoxLabel.ForeColor = System.Drawing.Color.Black
        Me._X4PositionTextBoxLabel.Location = New System.Drawing.Point(284, 16)
        Me._X4PositionTextBoxLabel.Name = "_X4PositionTextBoxLabel"
        Me._X4PositionTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._X4PositionTextBoxLabel.Size = New System.Drawing.Size(22, 13)
        Me._X4PositionTextBoxLabel.TabIndex = 36
        Me._X4PositionTextBoxLabel.Text = "X4"
        '
        '_Y2PositionTextBoxLabel
        '
        Me._Y2PositionTextBoxLabel.AllowDrop = True
        Me._Y2PositionTextBoxLabel.AutoSize = True
        Me._Y2PositionTextBoxLabel.BackColor = System.Drawing.SystemColors.Control
        Me._Y2PositionTextBoxLabel.ForeColor = System.Drawing.Color.Black
        Me._Y2PositionTextBoxLabel.Location = New System.Drawing.Point(142, 16)
        Me._Y2PositionTextBoxLabel.Name = "_Y2PositionTextBoxLabel"
        Me._Y2PositionTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._Y2PositionTextBoxLabel.Size = New System.Drawing.Size(22, 13)
        Me._Y2PositionTextBoxLabel.TabIndex = 35
        Me._Y2PositionTextBoxLabel.Text = "Y2"
        '
        '_Y1PositionTextBoxLabel
        '
        Me._Y1PositionTextBoxLabel.AllowDrop = True
        Me._Y1PositionTextBoxLabel.AutoSize = True
        Me._Y1PositionTextBoxLabel.BackColor = System.Drawing.SystemColors.Control
        Me._Y1PositionTextBoxLabel.ForeColor = System.Drawing.Color.Black
        Me._Y1PositionTextBoxLabel.Location = New System.Drawing.Point(97, 16)
        Me._Y1PositionTextBoxLabel.Name = "_Y1PositionTextBoxLabel"
        Me._Y1PositionTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._Y1PositionTextBoxLabel.Size = New System.Drawing.Size(22, 13)
        Me._Y1PositionTextBoxLabel.TabIndex = 34
        Me._Y1PositionTextBoxLabel.Text = "Y1"
        '
        '_DeltaTimeTextBoxLabel
        '
        Me._DeltaTimeTextBoxLabel.AllowDrop = True
        Me._DeltaTimeTextBoxLabel.AutoSize = True
        Me._DeltaTimeTextBoxLabel.BackColor = System.Drawing.SystemColors.Control
        Me._DeltaTimeTextBoxLabel.ForeColor = System.Drawing.Color.Black
        Me._DeltaTimeTextBoxLabel.Location = New System.Drawing.Point(328, 16)
        Me._DeltaTimeTextBoxLabel.Name = "_DeltaTimeTextBoxLabel"
        Me._DeltaTimeTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._DeltaTimeTextBoxLabel.Size = New System.Drawing.Size(22, 13)
        Me._DeltaTimeTextBoxLabel.TabIndex = 33
        Me._DeltaTimeTextBoxLabel.Text = "dX"
        '
        '_DeltaVoltsTextBoxLabel
        '
        Me._DeltaVoltsTextBoxLabel.AllowDrop = True
        Me._DeltaVoltsTextBoxLabel.AutoSize = True
        Me._DeltaVoltsTextBoxLabel.BackColor = System.Drawing.SystemColors.Control
        Me._DeltaVoltsTextBoxLabel.ForeColor = System.Drawing.Color.Black
        Me._DeltaVoltsTextBoxLabel.Location = New System.Drawing.Point(185, 16)
        Me._DeltaVoltsTextBoxLabel.Name = "_DeltaVoltsTextBoxLabel"
        Me._DeltaVoltsTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._DeltaVoltsTextBoxLabel.Size = New System.Drawing.Size(22, 13)
        Me._DeltaVoltsTextBoxLabel.TabIndex = 32
        Me._DeltaVoltsTextBoxLabel.Text = "dY"
        '
        '_DisplayStyleFrame
        '
        Me._DisplayStyleFrame.AllowDrop = True
        Me._DisplayStyleFrame.BackColor = System.Drawing.Color.Transparent
        Me._DisplayStyleFrame.Controls.Add(Me._AreaDisplayStyleRadioButton)
        Me._DisplayStyleFrame.Controls.Add(Me._LineDisplayStyleRadioButton)
        Me._DisplayStyleFrame.Controls.Add(Me._PointsDisplayStyleRadioButton)
        Me._DisplayStyleFrame.ForeColor = System.Drawing.SystemColors.ControlText
        Me._DisplayStyleFrame.Location = New System.Drawing.Point(8, 163)
        Me._DisplayStyleFrame.Name = "_DisplayStyleFrame"
        Me._DisplayStyleFrame.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._DisplayStyleFrame.Size = New System.Drawing.Size(85, 84)
        Me._DisplayStyleFrame.TabIndex = 19
        Me._DisplayStyleFrame.TabStop = False
        Me._DisplayStyleFrame.Text = "Style"
        '
        '_AreaDisplayStyleRadioButton
        '
        Me._AreaDisplayStyleRadioButton.AllowDrop = True
        Me._AreaDisplayStyleRadioButton.BackColor = System.Drawing.Color.Transparent
        Me._AreaDisplayStyleRadioButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._AreaDisplayStyleRadioButton.Location = New System.Drawing.Point(8, 60)
        Me._AreaDisplayStyleRadioButton.Name = "_AreaDisplayStyleRadioButton"
        Me._AreaDisplayStyleRadioButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._AreaDisplayStyleRadioButton.Size = New System.Drawing.Size(65, 17)
        Me._AreaDisplayStyleRadioButton.TabIndex = 22
        Me._AreaDisplayStyleRadioButton.TabStop = True
        Me._AreaDisplayStyleRadioButton.Text = "Area"
        Me._AreaDisplayStyleRadioButton.UseVisualStyleBackColor = False
        '
        '_LineDisplayStyleRadioButton
        '
        Me._LineDisplayStyleRadioButton.AllowDrop = True
        Me._LineDisplayStyleRadioButton.BackColor = System.Drawing.SystemColors.Control
        Me._LineDisplayStyleRadioButton.Checked = True
        Me._LineDisplayStyleRadioButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._LineDisplayStyleRadioButton.Location = New System.Drawing.Point(8, 38)
        Me._LineDisplayStyleRadioButton.Name = "_LineDisplayStyleRadioButton"
        Me._LineDisplayStyleRadioButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._LineDisplayStyleRadioButton.Size = New System.Drawing.Size(65, 17)
        Me._LineDisplayStyleRadioButton.TabIndex = 21
        Me._LineDisplayStyleRadioButton.TabStop = True
        Me._LineDisplayStyleRadioButton.Text = "Line"
        Me._LineDisplayStyleRadioButton.UseVisualStyleBackColor = False
        '
        '_PointsDisplayStyleRadioButton
        '
        Me._PointsDisplayStyleRadioButton.AllowDrop = True
        Me._PointsDisplayStyleRadioButton.BackColor = System.Drawing.SystemColors.Control
        Me._PointsDisplayStyleRadioButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._PointsDisplayStyleRadioButton.Location = New System.Drawing.Point(8, 16)
        Me._PointsDisplayStyleRadioButton.Name = "_PointsDisplayStyleRadioButton"
        Me._PointsDisplayStyleRadioButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._PointsDisplayStyleRadioButton.Size = New System.Drawing.Size(57, 17)
        Me._PointsDisplayStyleRadioButton.TabIndex = 20
        Me._PointsDisplayStyleRadioButton.TabStop = True
        Me._PointsDisplayStyleRadioButton.Text = "Point"
        Me._PointsDisplayStyleRadioButton.UseVisualStyleBackColor = False
        '
        '_GraticuleFrame
        '
        Me._GraticuleFrame.AllowDrop = True
        Me._GraticuleFrame.BackColor = System.Drawing.Color.Transparent
        Me._GraticuleFrame.Controls.Add(Me._FrameToggle)
        Me._GraticuleFrame.Controls.Add(Me._AxesToggle)
        Me._GraticuleFrame.Controls.Add(Me._GridToggle)
        Me._GraticuleFrame.ForeColor = System.Drawing.SystemColors.ControlText
        Me._GraticuleFrame.Location = New System.Drawing.Point(8, 8)
        Me._GraticuleFrame.Name = "_GraticuleFrame"
        Me._GraticuleFrame.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._GraticuleFrame.Size = New System.Drawing.Size(85, 85)
        Me._GraticuleFrame.TabIndex = 15
        Me._GraticuleFrame.TabStop = False
        Me._GraticuleFrame.Text = "Graticule"
        '
        '_FrameToggle
        '
        Me._FrameToggle.AllowDrop = True
        Me._FrameToggle.AutoSize = True
        Me._FrameToggle.BackColor = System.Drawing.SystemColors.Control
        Me._FrameToggle.Checked = True
        Me._FrameToggle.CheckState = System.Windows.Forms.CheckState.Checked
        Me._FrameToggle.ForeColor = System.Drawing.SystemColors.ControlText
        Me._FrameToggle.Location = New System.Drawing.Point(8, 16)
        Me._FrameToggle.Name = "_FrameToggle"
        Me._FrameToggle.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._FrameToggle.Size = New System.Drawing.Size(60, 17)
        Me._FrameToggle.TabIndex = 18
        Me._FrameToggle.Text = "Frame"
        Me._FrameToggle.UseVisualStyleBackColor = False
        '
        '_AxesToggle
        '
        Me._AxesToggle.AllowDrop = True
        Me._AxesToggle.AutoSize = True
        Me._AxesToggle.BackColor = System.Drawing.SystemColors.Control
        Me._AxesToggle.Checked = True
        Me._AxesToggle.CheckState = System.Windows.Forms.CheckState.Checked
        Me._AxesToggle.ForeColor = System.Drawing.SystemColors.ControlText
        Me._AxesToggle.Location = New System.Drawing.Point(8, 38)
        Me._AxesToggle.Name = "_AxesToggle"
        Me._AxesToggle.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._AxesToggle.Size = New System.Drawing.Size(53, 17)
        Me._AxesToggle.TabIndex = 17
        Me._AxesToggle.Text = "Axes"
        Me._AxesToggle.UseVisualStyleBackColor = False
        '
        '_GridToggle
        '
        Me._GridToggle.AllowDrop = True
        Me._GridToggle.AutoSize = True
        Me._GridToggle.BackColor = System.Drawing.SystemColors.Control
        Me._GridToggle.Checked = True
        Me._GridToggle.CheckState = System.Windows.Forms.CheckState.Checked
        Me._GridToggle.ForeColor = System.Drawing.SystemColors.ControlText
        Me._GridToggle.Location = New System.Drawing.Point(8, 60)
        Me._GridToggle.Name = "_GridToggle"
        Me._GridToggle.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._GridToggle.Size = New System.Drawing.Size(49, 17)
        Me._GridToggle.TabIndex = 16
        Me._GridToggle.Text = "Grid"
        Me._GridToggle.UseVisualStyleBackColor = False
        '
        '_ViewportsGroupBox
        '
        Me._ViewportsGroupBox.AllowDrop = True
        Me._ViewportsGroupBox.BackColor = System.Drawing.Color.Transparent
        Me._ViewportsGroupBox.Controls.Add(Me._ViewportsTextBox)
        Me._ViewportsGroupBox.Controls.Add(Me._StorageToggle)
        Me._ViewportsGroupBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me._ViewportsGroupBox.Location = New System.Drawing.Point(8, 95)
        Me._ViewportsGroupBox.Name = "_ViewportsGroupBox"
        Me._ViewportsGroupBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ViewportsGroupBox.Size = New System.Drawing.Size(85, 66)
        Me._ViewportsGroupBox.TabIndex = 12
        Me._ViewportsGroupBox.TabStop = False
        Me._ViewportsGroupBox.Text = "Viewport"
        '
        '_ViewportsTextBox
        '
        Me._ViewportsTextBox.AcceptsReturn = True
        Me._ViewportsTextBox.AllowDrop = True
        Me._ViewportsTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._ViewportsTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._ViewportsTextBox.Cursor = System.Windows.Forms.Cursors.Default
        Me._ViewportsTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.6!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ViewportsTextBox.ForeColor = System.Drawing.Color.Black
        Me._ViewportsTextBox.Location = New System.Drawing.Point(8, 16)
        Me._ViewportsTextBox.MaxLength = 1
        Me._ViewportsTextBox.Name = "_ViewportsTextBox"
        Me._ViewportsTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ViewportsTextBox.Size = New System.Drawing.Size(29, 22)
        Me._ViewportsTextBox.TabIndex = 13
        Me._ViewportsTextBox.Text = "1"
        Me._ViewportsTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        '_StorageToggle
        '
        Me._StorageToggle.AllowDrop = True
        Me._StorageToggle.AutoSize = True
        Me._StorageToggle.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.81!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._StorageToggle.Location = New System.Drawing.Point(8, 43)
        Me._StorageToggle.Name = "_StorageToggle"
        Me._StorageToggle.Size = New System.Drawing.Size(70, 17)
        Me._StorageToggle.TabIndex = 14
        Me._StorageToggle.Text = "Storage"
        '
        '_EraseAllChannelsButton
        '
        Me._EraseAllChannelsButton.AllowDrop = True
        Me._EraseAllChannelsButton.BackColor = System.Drawing.SystemColors.Control
        Me._EraseAllChannelsButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._EraseAllChannelsButton.Location = New System.Drawing.Point(395, 164)
        Me._EraseAllChannelsButton.Name = "_EraseAllChannelsButton"
        Me._EraseAllChannelsButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._EraseAllChannelsButton.Size = New System.Drawing.Size(73, 29)
        Me._EraseAllChannelsButton.TabIndex = 6
        Me._EraseAllChannelsButton.Text = "Erase"
        Me._EraseAllChannelsButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me._EraseAllChannelsButton.UseVisualStyleBackColor = False
        '
        '_PlotXYButton
        '
        Me._PlotXYButton.AllowDrop = True
        Me._PlotXYButton.BackColor = System.Drawing.SystemColors.Control
        Me._PlotXYButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._PlotXYButton.Location = New System.Drawing.Point(395, 308)
        Me._PlotXYButton.Name = "_PlotXYButton"
        Me._PlotXYButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._PlotXYButton.Size = New System.Drawing.Size(73, 29)
        Me._PlotXYButton.TabIndex = 2
        Me._PlotXYButton.Text = "Plot XY"
        Me._PlotXYButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me._PlotXYButton.UseVisualStyleBackColor = False
        '
        '_SingleButton
        '
        Me._SingleButton.AllowDrop = True
        Me._SingleButton.BackColor = System.Drawing.SystemColors.Control
        Me._SingleButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._SingleButton.Location = New System.Drawing.Point(395, 344)
        Me._SingleButton.Name = "_SingleButton"
        Me._SingleButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._SingleButton.Size = New System.Drawing.Size(73, 29)
        Me._SingleButton.TabIndex = 0
        Me._SingleButton.Text = "&Single"
        Me._SingleButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me._SingleButton.UseVisualStyleBackColor = False
        '
        '_StartStopButton
        '
        Me._StartStopButton.AllowDrop = True
        Me._StartStopButton.BackColor = System.Drawing.SystemColors.Control
        Me._StartStopButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._StartStopButton.Location = New System.Drawing.Point(395, 380)
        Me._StartStopButton.Name = "_StartStopButton"
        Me._StartStopButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._StartStopButton.Size = New System.Drawing.Size(73, 29)
        Me._StartStopButton.TabIndex = 1
        Me._StartStopButton.Text = "Start"
        Me._StartStopButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me._StartStopButton.UseVisualStyleBackColor = False
        '
        '_PointsButton
        '
        Me._PointsButton.AllowDrop = True
        Me._PointsButton.BackColor = System.Drawing.SystemColors.Control
        Me._PointsButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._PointsButton.Location = New System.Drawing.Point(395, 272)
        Me._PointsButton.Name = "_PointsButton"
        Me._PointsButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._PointsButton.Size = New System.Drawing.Size(73, 29)
        Me._PointsButton.TabIndex = 3
        Me._PointsButton.Text = "Points"
        Me._PointsButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me._PointsButton.UseVisualStyleBackColor = False
        '
        '_PenColorButton
        '
        Me._PenColorButton.AllowDrop = True
        Me._PenColorButton.BackColor = System.Drawing.SystemColors.Control
        Me._PenColorButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._PenColorButton.Location = New System.Drawing.Point(395, 200)
        Me._PenColorButton.Name = "_PenColorButton"
        Me._PenColorButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._PenColorButton.Size = New System.Drawing.Size(73, 29)
        Me._PenColorButton.TabIndex = 5
        Me._PenColorButton.Text = "Pen Color"
        Me._PenColorButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me._PenColorButton.UseVisualStyleBackColor = False
        '
        '_InitializeButton
        '
        Me._InitializeButton.AllowDrop = True
        Me._InitializeButton.BackColor = System.Drawing.SystemColors.Control
        Me._InitializeButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._InitializeButton.Location = New System.Drawing.Point(395, 128)
        Me._InitializeButton.Name = "_InitializeButton"
        Me._InitializeButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._InitializeButton.Size = New System.Drawing.Size(73, 29)
        Me._InitializeButton.TabIndex = 7
        Me._InitializeButton.Text = "Initialize"
        Me._InitializeButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me._InitializeButton.UseVisualStyleBackColor = False
        '
        '_ScrollButton
        '
        Me._ScrollButton.AllowDrop = True
        Me._ScrollButton.BackColor = System.Drawing.SystemColors.Control
        Me._ScrollButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._ScrollButton.Location = New System.Drawing.Point(395, 236)
        Me._ScrollButton.Name = "_ScrollButton"
        Me._ScrollButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ScrollButton.Size = New System.Drawing.Size(73, 29)
        Me._ScrollButton.TabIndex = 4
        Me._ScrollButton.Text = "Scroll"
        Me._ScrollButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me._ScrollButton.UseVisualStyleBackColor = False
        '
        '_ExitButton
        '
        Me._ExitButton.AllowDrop = True
        Me._ExitButton.BackColor = System.Drawing.SystemColors.Control
        Me._ExitButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._ExitButton.Location = New System.Drawing.Point(395, 416)
        Me._ExitButton.Name = "_ExitButton"
        Me._ExitButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ExitButton.Size = New System.Drawing.Size(73, 29)
        Me._ExitButton.TabIndex = 10
        Me._ExitButton.Text = "E&xit"
        Me._ExitButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me._ExitButton.UseVisualStyleBackColor = False
        '
        '_HorizontalScrollBar
        '
        Me._HorizontalScrollBar.AllowDrop = True
        Me._HorizontalScrollBar.LargeChange = 1
        Me._HorizontalScrollBar.Location = New System.Drawing.Point(99, 232)
        Me._HorizontalScrollBar.Maximum = 1000
        Me._HorizontalScrollBar.Minimum = -1000
        Me._HorizontalScrollBar.Name = "_HorizontalScrollBar"
        Me._HorizontalScrollBar.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._HorizontalScrollBar.Size = New System.Drawing.Size(273, 17)
        Me._HorizontalScrollBar.TabIndex = 8
        Me._HorizontalScrollBar.TabStop = True
        '
        '_VerticalScrollBar
        '
        Me._VerticalScrollBar.AllowDrop = True
        Me._VerticalScrollBar.LargeChange = 1
        Me._VerticalScrollBar.Location = New System.Drawing.Point(372, 8)
        Me._VerticalScrollBar.Maximum = 200
        Me._VerticalScrollBar.Minimum = -200
        Me._VerticalScrollBar.Name = "_VerticalScrollBar"
        Me._VerticalScrollBar.Size = New System.Drawing.Size(17, 225)
        Me._VerticalScrollBar.TabIndex = 9
        Me._VerticalScrollBar.TabStop = True
        '
        '_Chart
        '
        Me._Chart.ChartingEnabled = False
        Me._Chart.Location = New System.Drawing.Point(99, 8)
        Me._Chart.Name = "_Chart"
        Me._Chart.Size = New System.Drawing.Size(273, 225)
        Me._Chart.TabIndex = 59
        '
        'ChartDemoForm
        '
        Me.AllowDrop = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Silver
        Me.ClientSize = New System.Drawing.Size(476, 453)
        Me.Controls.Add(Me._Panel)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.Color.Black
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(160, 105)
        Me.MaximizeBox = False
        Me.Name = "ChartDemoForm"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "LabOBJX Real-Time Chart Example Application"
        Me._Panel.ResumeLayout(False)
        Me._MouseGroupBox.ResumeLayout(False)
        Me._MouseGroupBox.PerformLayout()
        Me._DataArrayGroupBox.ResumeLayout(False)
        Me._DataArrayGroupBox.PerformLayout()
        Me._WindowGroupBox.ResumeLayout(False)
        Me._WindowGroupBox.PerformLayout()
        Me._OffsetGroupBox.ResumeLayout(False)
        Me._OffsetGroupBox.PerformLayout()
        Me._CursorFrame.ResumeLayout(False)
        Me._CursorFrame.PerformLayout()
        Me._DisplayStyleFrame.ResumeLayout(False)
        Me._GraticuleFrame.ResumeLayout(False)
        Me._GraticuleFrame.PerformLayout()
        Me._ViewportsGroupBox.ResumeLayout(False)
        Me._ViewportsGroupBox.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

End Class