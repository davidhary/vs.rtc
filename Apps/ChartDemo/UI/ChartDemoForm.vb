' LabOBJX Real-Time Chart - Example Code
' (C) Copyright 1995, Scientific Software Tools, Inc.
' All Rights Reserved.


Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic
Imports System
Imports System.ComponentModel
Imports System.Windows.Forms
Imports isr.Visuals.RealTimeChart

''' <summary> Form for viewing the chart demo. </summary>
''' <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 7/1/2019 </para></remarks>
Partial Public Class ChartDemoForm
    Inherits System.Windows.Forms.Form

#Region " CONSTRUCTION and CLEANUP "

    Private ReadOnly _IsInitializingComponent As Boolean
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub New()
        MyBase.New()
        If _Instance Is Nothing Then
            If _InitializingInstance Then
                _Instance = Me
            Else
                Try
                    'For the start-up form, the first instance created is the default instance.
                    If Not (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint Is Nothing) AndAlso
                        System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType Is Me.GetType() Then
                        _Instance = Me
                    End If
                Catch

                End Try
            End If
        End If
        'This call is required by the Windows Form Designer.
        _IsInitializingComponent = True
        InitializeComponent()
        _IsInitializingComponent = False
        Me.ResetKnownState()
    End Sub

#End Region

#Region " SINGLETON INSTANCE "

    Private Shared _Instance As ChartDemoForm
    Private Shared _InitializingInstance As Boolean
    Public Shared Property DefInstance() As ChartDemoForm
        Get
            If _Instance Is Nothing OrElse _Instance.IsDisposed Then
                _InitializingInstance = True
                _Instance = CreateInstance()
                _InitializingInstance = False
            End If
            Return _Instance
        End Get
        Set(ByVal value As ChartDemoForm)
            _Instance = value
        End Set
    End Property

    Public Shared Function CreateInstance() As ChartDemoForm
        Dim theInstance As New ChartDemoForm()
        Return theInstance
    End Function

#End Region

#Region " FORM EVENTS "

    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ResetKnownState()
        Try
            Me._Chart.ResetKnownState()

            ' initialize program's global variables
            InitializeGlobalVariables()

            ' load cursors from RtChart control if not previously loaded
            If Me._CursorsLoaded = 0 Then
                LoadCustomCursors()
                Me._CursorsLoaded = 1
            End If

            ' initialize RtChart global properties
            InitializeRtChart(_Chart)

            ' initialize scroll bars - causes scroll event procs to execute
            InitializeScrollBars()

            ' initialize RtChart ViewPort
            InitializeViewPorts(_Chart)

            ' initialize RtChart Pens
            ChartPen.InitChannelsPens(_Chart)
            InitChannelPenArray()

            ' initialize RtChart for Volts vs Time plots
            InitializeChannel(_Chart, ChannelName.Channel1, _NumSamples)

            ' initialize check boxes & buttons according to RtChart properties
            InitializeFrontPanel()

            ' change mouse pointer to "Cross-hair" when over RtChart for measurement
            _CurrentPointer = Cursors.CrosshairCurHandle
            _Chart.MousePointer = Me._CurrentPointer

            ' initialize text boxes with default values
            ' NOTE: these cause change event procedures to execute
            _ChannelCountLabel.Text = _NumChannels.ToString()
            _SamplesCountTextBox.Text = _NumSamples.ToString()
            _SamplesPerDivisionTextBox.Text = SamplesPerDiv.ToString()
            _VoltsPerDivisionTextBox.Text = VoltsPerDiv.ToString()

            ' center window on screen
            Me.SetBounds((Screen.PrimaryScreen.Bounds.Width - Me.ClientRectangle.Width) / 2,
                         (Screen.PrimaryScreen.Bounds.Height - Me.ClientRectangle.Height) / 2, 0, 0, BoundsSpecified.X Or BoundsSpecified.Y)

            ' initialize font properties
            InitFonts(_Chart)

            ' initialize Horizontal Scale
            InitHorizontalScale(_Chart, 10, 1, X_TIME_WINDOW)

            ' initialize VerticalScale
            InitVerticalScale(_Chart, 1, 1, X_TIME_WINDOW)

            ' plot single buffer
            Single_Click(_SingleButton, New EventArgs())

        Catch exc As Exception
            System.Windows.Forms.MessageBox.Show(Me, exc.ToString, "Form initialization error", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
        End Try
    End Sub

    Private Sub Form_Closed(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MyBase.Closed

        If Program.Running Then Running = False

        UnloadCustomCursors()
        _CursorsLoaded = 0

        Environment.Exit(0)

    End Sub

#End Region

#Region " EXPOSED VALUES "

    Public Property CurrentY1AbsolutePositionCaption
        Get
            Return Me._Y1PositionTextBox.Text
        End Get
        Set(value)
            Me._Y1PositionTextBox.Text = value
        End Set
    End Property

    Public Property CurrentY2AbsolutePositionCaption
        Get
            Return Me._Y2PositionTextBox.Text
        End Get
        Set(value)
            Me._Y2PositionTextBox.Text = value
        End Set
    End Property

    Public Property CurrentX3AbsolutePositionCaption
        Get
            Return Me._X3PositionTextBox.Text
        End Get
        Set(value)
            Me._X3PositionTextBox.Text = value
        End Set
    End Property

    Public Property CurrentX4AbsolutePositionCaption
        Get
            Return Me._X4PositionTextBox.Text
        End Get
        Set(value)
            Me._X4PositionTextBox.Text = value
        End Set
    End Property

    Public Property DeltaVoltageCaption
        Get
            Return Me._DeltaVoltsTextBox.Text
        End Get
        Set(value)
            Me._DeltaVoltsTextBox.Text = value
        End Set
    End Property

    Public Property DeltaTimeCaption
        Get
            Return Me._DeltaTimeTextBox.Text
        End Get
        Set(value)
            Me._DeltaTimeTextBox.Text = value
        End Set
    End Property

#End Region

#Region " CONTROL EVENTS  "

    Private _I As Integer ' index in for/next loops
    Private _NumChannels As Integer ' number of channels
    Private _NumSamples As Integer ' number of samples
    Private _CursorsLoaded As Boolean ' flag to indicate if custom cursors were loaded
    Private _CurrentPointer As Integer ' handle to current mouse pointer
    Private _TempValue As Single ' temporary storage variable
    Private ReadOnly _KeyCode As Integer ' for key down procedure
    Private _NumViewports As Integer ' number of viewports
    Private _YTrace_Lock As Boolean ' boolean to control scroll and drag functions
    Private _XTrace_Lock As Boolean ' boolean to control scroll and drag functions
    Private _AutoCursor As Boolean ' boolean to control auto cursor mode
    Private _ReInitChannnels As Boolean ' boolean to indicate if channel must be initialized

    Private _MaxVolts As Single ' signal waveform limits (positive peak)
    Private _MinVolts As Single ' signal waveform limits (negative peak)

    ' Scroll Bar Min, Max, & Value are multiplied by ScrollBarScaler to increase resolution because
    ' scroll bar properties are integers and RtChart offset properties are single precision.
    Private _VScrollBarScaler As Integer
    Private _HScrollBarScaler As Integer

    Private _VBDataArrayY(,) As Single ' VB array to store multi-channel voltage data values
    Private _VBDataArrayX() As Single ' VB array to store X-axis voltage values for X vs Y plot

    Private _SelectedChannel As Integer ' currently selected data channel

    Private ReadOnly _ChannelPenArray(20) As Integer

    ' used in RtChart event procedures for re-positioning data channels and cursors
    Private _LastObject As Integer ' selected channel on MouseDown event
    Private ReadOnly _MouseX As Single ' previous mouse X-axis position
    Private ReadOnly _MouseY As Single ' previous mouse Y-axis position
    Private _CursorVolts As Single
    Private _Cursor1Volts As Single ' voltage position value of cursor1
    Private _Cursor2Volts As Single ' voltage position value of cursor2
    Private ReadOnly _Cursor3Time As Single ' time position value of cursor3
    Private ReadOnly _Cursor4Time As Single ' time position value of cursor4

    Private Sub AutoCursorsCheckBox_CheckStateChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _AutoTackToggle.CheckStateChanged
        Me._AutoCursor = _AutoTackToggle.Checked
    End Sub

    Private Sub AutoMoveCursor(ByRef yCursorName As Integer, ByRef xCursorTime As Single)

        ' moves horizontal (volts) cursor in response to vertical (time) cursor
        ' to intersecting point on channel trace
        ' takes name of Y cursor to auto position and time position of X cursor
        ' globals - SelectedChannel, NumSamples, CursorVolts, Cursor1Volts,
        ' Cursor2Volts, LastObject, and FirstPoint

        ' auto track cursors are not implemented for XY plots by this demo app
        If _SelectedChannel = ChannelName.XYChannel Then Exit Sub

        _Chart.LogicalChannel = (_SelectedChannel) ' select current data channel to get its offset

        ' index = FirstPoint + "time" (in samples) - data channel X-axis offset
        ' index i must be within data buffer index range
        _I = firstpoint + xCursorTime - _Chart.ChnDspOffsetX
        If _I < 0 Then _I = 0
        If _I >= _NumSamples Then _I = _NumSamples - 1

        ' get voltage point which intersects with time YCursorName, add data channel Y-axis offset
        ' compensate for any offset in data plot
        _CursorVolts = _Chart.ChnDspOffsetY + _VBDataArrayY(_I, _SelectedChannel - 1)

        ' use absolute cursor voltage value for readout and delta values
        Select Case yCursorName ' select which cursor voltage to update for text box
            Case ChannelName.Cursor1
                _Cursor1Volts = _CursorVolts
                _Y1PositionTextBox.Text = _Cursor1Volts.ToString() ' update Cursor1 position text box
            Case ChannelName.Cursor2
                _Cursor2Volts = _CursorVolts
                _Y2PositionTextBox.Text = _Cursor2Volts.ToString() ' update Cursor2 position text box
        End Select

        ' select volts cursor and get offset to compensate for existing offset in cursor
        ' to correctly place cursor over data plot
        _Chart.LogicalChannel = (yCursorName)
        _CursorVolts -= _Chart.ChnDspOffsetY

        ' move volts cursor to new position
        DrawCursor(_Chart, yCursorName, _Chart.WndXmin, _CursorVolts, _Chart.WndWidth, _CursorVolts)

        ' re-select current object (YCursorName) for mouse up event
        _Chart.LogicalChannel = (_LastObject)

        ' calculate and update voltage cursors' delta
        _DeltaVoltsTextBox.Text = (_Cursor1Volts - _Cursor2Volts).ToString()

    End Sub

    Private Sub AxesTypeCheckBox_CheckStateChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _AxesToggle.CheckStateChanged
        If _AxesToggle.Checked Then
            _Chart.AxesType = AxesType.HorizontalVertical
        Else
            _Chart.AxesType = AxesType.NoAxes
        End If
    End Sub

    Private Sub ExitButton_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _ExitButton.Click
        Form_Closed(Me, New CancelEventArgs())
    End Sub

    Private Sub CurrentChannelTextBox_TextChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _CurrentChannelTextBox.TextChanged
        If _IsInitializingComponent Then Return

        ' current channel can be selected by mouse click or keyboard
        Me._SelectedChannel = Conversion.Val(_CurrentChannelTextBox.Text)

        ' move cursors to newly selected data channel
        If _CursorsToggle.Checked Then DrawCursors(_Chart, _SelectedChannel)

    End Sub

    Private Sub CursorsCheckBox_CheckStateChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _CursorsToggle.CheckStateChanged
        If _IsInitializingComponent Then Return
        If _CursorsToggle.Checked Then
            _AutoTackToggle.Enabled = True
            ' initialize cursor channels
            InitializeCursors(_Chart, X_TIME_WINDOW)
            DrawCursors(_Chart, _SelectedChannel) ' move cursors to newly selected data channel
        Else
            _AutoTackToggle.Enabled = False
            ' De-initialize cursor channels
            DeInitChannels(_Chart, ChannelName.Cursor1, ChannelName.Cursor4)
        End If

    End Sub

    Private Sub DisplayStyleArea_CheckedChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _AreaDisplayStyleRadioButton.CheckedChanged
        If _IsInitializingComponent Then Return
        If eventSender.Checked Then

            ' prevent each channel from being re-painted individually
            _Chart.ChartAction = ChartAction.DisablePaint

            For channel As Integer = ChannelName.Channel1 To _NumChannels
                ' change display style for selected channel
                _Chart.LogicalChannel = channel
                _Chart.ChnDspStyle = ((ChannelDisplayStyle.Area))
            Next channel

            ' paint entire window with all channels
            _Chart.ChartAction = ChartAction.EnablePaint

        End If
    End Sub

    Private Sub DisplayStyleLine_CheckedChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _LineDisplayStyleRadioButton.CheckedChanged
        If _IsInitializingComponent Then Return
        If eventSender.Checked Then
            ' prevent each channel from being re-painted individually
            _Chart.ChartAction = ChartAction.DisablePaint

            For channel As Integer = ChannelName.Channel1 To _NumChannels
                ' change display style for selected channel
                _Chart.LogicalChannel = channel
                _Chart.ChnDspStyle = ChannelDisplayStyle.Lines
            Next channel

            ' paint entire window with all channels
            _Chart.ChartAction = ChartAction.EnablePaint

        End If
    End Sub

    Private Sub DisplayStylePoints_CheckedChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _PointsDisplayStyleRadioButton.CheckedChanged
        If _IsInitializingComponent Then Return
        If eventSender.Checked Then

            ' prevent each channel from being re-painted individually
            _Chart.ChartAction = ChartAction.DisablePaint

            For channel As Integer = ChannelName.Channel1 To _NumChannels
                ' change display style for selected channel
                _Chart.LogicalChannel = channel
                _Chart.ChnDspStyle = ChannelDisplayStyle.Points
            Next channel

            ' paint entire window with all channels
            _Chart.ChartAction = ChartAction.EnablePaint

        End If
    End Sub

    Private Sub EraseAllChannels_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _EraseAllChannelsButton.Click

        ' save current storage mode
        _TempValue = _Chart.ViewportStorageOn

        ' clear screen of stored waveforms, if any
        _Chart.ViewportStorageOn = False

        ' turn off auto track cursors
        AutoCursorsCheckBox_CheckStateChanged(_AutoTackToggle, New EventArgs())

        ' erase all time channels
        EraseChannels(_Chart, ChannelName.Channel1, _NumChannels)

        ' de-initializing XY channel will also erase if it exists
        DeInitChannels(_Chart, ChannelName.XYChannel, ChannelName.XYChannel)

        ' restore previous storage mode
        _Chart.ViewportStorageOn = (_TempValue)

    End Sub

    Private Sub FrameOn_CheckStateChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _FrameToggle.CheckStateChanged
        If _IsInitializingComponent Then Return

        ' set RtChart property
        _Chart.FrameOn = _FrameToggle.Checked

    End Sub

    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub Generate2D_SineWaveArray(ByVal numChannels As Integer, ByVal numSamples As Integer, ByRef voltsPeak As Single)

        Dim NumCycles, CycleTime As Single
        Try
            'build array 2% larger than NumSamples to allow random starting points for demonstration
            Dim nPoints As Integer = (0.02 * numSamples) + numSamples

            ' NumSamples is 0-based, NumChannels is 1-based
            ReDim _VBDataArrayY(nPoints, numChannels - 1) ' size the array for 2D data

            Dim RadiansPerSample As Single = TWO_PIE / numSamples

            ' NumCycles determines # of cycles in buffer
            For channel As Integer = 1 To numChannels
                NumCycles = ((channel - 1) * RadiansPerSample)

                For sample As Integer = 0 To nPoints - 1
                    CycleTime = RadiansPerSample + NumCycles
                    _VBDataArrayY(sample, channel - 1) = voltsPeak * Math.Sin(sample * CycleTime)

                Next sample
            Next channel

        Catch exc As Exception
            System.Windows.Forms.MessageBox.Show(Me, exc.ToString, "Sine wage generation error", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
        End Try

    End Sub

    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    Private Sub GenerateCosineWaveArray(ByVal numChannels As Integer, ByVal numSamples As Integer, ByRef voltsPeak As Single)


        ReDim _VBDataArrayX(numSamples)

        Dim RadiansPerSample As Single = TWO_PIE / numSamples

        For sample As Integer = 0 To numSamples - 1
            _VBDataArrayX(sample) = voltsPeak * Math.Cos(sample * RadiansPerSample)
        Next sample

    End Sub

    Private Sub GridOn_CheckStateChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _GridToggle.CheckStateChanged
        If _IsInitializingComponent Then Return

        ' set RtChart property
        _Chart.GridOn = _GridToggle.Checked

    End Sub

    Private Sub HScroll1_ValueChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _HorizontalScrollBar.ValueChanged


        ' scroll bar Min, Max, & Value are multiplied by HScrollBarScaler to increase resolution
        ' as these properties are integers and RtChart offsets are single precision
        ' scroll bar Value must be divided by HScrollBarScaler to return it to a ChnDspOffset value


        If _XTrace_Lock Then ' keep all channels' offset equal on X-axis

            ' prevent each channel from being re-painted individually on Translate Action
            _Chart.ChartAction = ChartAction.DisablePaint

            For channel As Integer = ChannelName.Channel1 To _NumChannels
                _Chart.LogicalChannel = channel ' select channel

                ' use same offset for all channels
                _Chart.ChnDspOffsetX = _HorizontalScrollBar.Value / _HScrollBarScaler

                _Chart.ChnDspAction = ChannelDisplayAction.Translate ' update channel position

            Next channel

            ' paint entire window with new channel positions for all channels
            _Chart.ChartAction = ChartAction.EnablePaint

        Else

            ' change offset for selected channel only
            _Chart.LogicalChannel = _SelectedChannel
            _Chart.ChnDspOffsetX = _HorizontalScrollBar.Value / _HScrollBarScaler
            _Chart.ChnDspAction = ChannelDisplayAction.Translate ' update channel position

        End If

        ' update X-axis offset text box
        _XOffsetTextBox.Text = CStr(_Chart.ChnDspOffsetX)

    End Sub

    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    Private Sub HScroll1_Scroll_Renamed(ByVal newScrollValue As Integer)

        ' call change event procedure so dragging will cause RtChart to move offset
        HScroll1_ValueChanged(_HorizontalScrollBar, New EventArgs())

    End Sub

    Private Sub InitChannelPenArray()

        ' this array is used to relate channel number to RGB value
        _ChannelPenArray(ChannelName.Channel1) = PenName.Red
        _ChannelPenArray(ChannelName.Channel2) = PenName.White
        _ChannelPenArray(ChannelName.Channel3) = PenName.Blue
        _ChannelPenArray(ChannelName.Channel4) = PenName.Black

    End Sub

    Private Sub InitializeFrontPanel()

        _AxesToggle.Checked = True '   H & V axes = 0, NoAxes = 2 (true)

        _FrameToggle.Checked = _Chart.FrameOn

        _GridToggle.Checked = _Chart.GridOn

        _StorageToggle.Checked = _Chart.ViewportStorageOn

        _XTraceLockToggle.Checked = False
        _YTraceLockToggle.Checked = False

        _LineDisplayStyleRadioButton.Checked = True

        _ViewportsTextBox.Text = "1"

        _CursorsToggle.Checked = False

        _AutoTackToggle.Checked = True

    End Sub

    Private Sub InitializeGlobalVariables()

        'initialize variables
        Running = False
        _NumChannels = 1
        _SelectedChannel = _NumChannels

        _NumSamples = 100

        SamplesPerDiv = _NumSamples / MAJOR_HORZ_DIV ' in samples

        _MaxVolts = 10.0#
        _MinVolts = -10.0#

        VoltsPerDiv = _MaxVolts / 2

        _CursorsLoaded = 0

        _LastObject = -1

        _VScrollBarScaler = 10
        _HScrollBarScaler = 1

        _NumViewports = 1

    End Sub

    Private Sub InitializeScrollBars()

        _VerticalScrollBar.Top = _Chart.Top
        _VerticalScrollBar.Left = _Chart.Left + _Chart.Width
        _VerticalScrollBar.Height = _Chart.Height

        _HorizontalScrollBar.Top = _Chart.Top + _Chart.Height
        _HorizontalScrollBar.Width = _Chart.Width
        _HorizontalScrollBar.Left = _Chart.Left

    End Sub

    Private Sub PenColor_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _PenColorButton.Click

        ' swap pen colors
        ReDefinePenColor(_Chart, _NumChannels)

    End Sub

    Private Sub PlotXY_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _PlotXYButton.Click


        ' stop continuous plotting if running is true
        If Running Then StartStop_Click(_StartStopButton, New EventArgs())

        ' stop continuous plotting if scrolling is true
        If Scrolling Then Scroll_Click(_ScrollButton, New EventArgs())

        ' prevent each channel from being re-painted individually
        _Chart.ChartAction = ChartAction.DisablePaint

        ' erase all existing time channels
        EraseChannels(_Chart, ChannelName.Channel1, _NumChannels)

        ' select view port for display
        _ViewportsTextBox.Text = "1"

        ' auto tracking cursors are not implemented for XY plot by this app
        AutoCursorsCheckBox_CheckStateChanged(_AutoTackToggle, New EventArgs())

        ' XY plot uses line style
        _LineDisplayStyleRadioButton.Checked = True

        ' generate two arrays to plot against each other
        Generate2D_SineWaveArray(ChannelName.LastChannel, _NumSamples, _MaxVolts)
        GenerateCosineWaveArray(1, _NumSamples, _MaxVolts)

        ' setup RtChart control properties for XY plot
        InitializeXYChannel(_Chart, ChannelName.XYChannel, _NumSamples)

        ' select XY channel
        _CurrentChannelTextBox.Text = CInt(ChannelName.XYChannel).ToString() ' if change event occurs, cursors will be redrawn

        ' update scroll bar range & reset position
        UpdateScrollBars(_Chart, _SelectedChannel, 0, 0)

        ' paint entire window with all channels at once
        _Chart.ChartAction = ChartAction.EnablePaint

        ' calls ChartData to plot two arrays
        PlotXYData(_Chart, _NumChannels, _NumSamples, _VBDataArrayY, _VBDataArrayX)

    End Sub

    Private Sub Points_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _PointsButton.Click


        ' stop continuous plotting if running is true
        If Running Then StartStop_Click(_StartStopButton, New EventArgs())

        ' stop continuous plotting if scrolling is true
        If Scrolling Then Scroll_Click(_ScrollButton, New EventArgs())

        ' prevent each channel from being re-painted individually
        _Chart.ChartAction = ChartAction.DisablePaint

        ' De-initializing XY channel will also erase if it exists
        DeInitChannels(_Chart, ChannelName.XYChannel, ChannelName.XYChannel)

        _PointsDisplayStyleRadioButton.Checked = True

        ' generate 2-dimensional array to plot
        Generate2D_SineWaveArray(_NumChannels, _NumSamples, _MaxVolts)

        ' setup RtChart control properties for single point plotting
        InitializePointChannels(_Chart, _NumChannels, _NumSamples)

        ' forces reinit for other line plots
        _ReInitChannnels = True

        ' assign each channel to a different Viewport if Viewports => than channels
        SelectViewPorts(_Chart, _NumChannels)

        ' if CurrentChannel_Change event occurs, cursors will be redrawn
        _CurrentChannelTextBox.Text = CInt(ChannelName.Channel1).ToString()

        ' update scroll bar range & reset position
        UpdateScrollBars(_Chart, _SelectedChannel, 0, 0)

        ' paint entire window with all channels at once
        _Chart.ChartAction = ChartAction.EnablePaint

        ' call point plotting routine
        PlotDataPoints(_Chart, _NumChannels, _NumSamples, _VBDataArrayY)

    End Sub

    Private Sub ReDefinePenColor(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal numChannels As Double)

        ' swap channel pen color with the other channels' pens


        ' swap pens
        Dim TempColor As Integer = _ChannelPenArray(ChannelName.Channel1)
        _ChannelPenArray(ChannelName.Channel1) = _ChannelPenArray(ChannelName.Channel2)
        _ChannelPenArray(ChannelName.Channel2) = _ChannelPenArray(ChannelName.Channel3)
        _ChannelPenArray(ChannelName.Channel3) = _ChannelPenArray(ChannelName.Channel4)
        _ChannelPenArray(ChannelName.Channel4) = TempColor
        '
        rtChart.ChartAction = ChartAction.DisablePaint

        For channel As Integer = ChannelName.Channel1 To numChannels

            ' describe new pen characteristics
            rtChart.LogicalPen = channel
            rtChart.PenColor = Drawing.Color.FromArgb(_ChannelPenArray(channel))
            rtChart.PenIntenseColor = Color.Yellow
            rtChart.PenWidth = 1
            rtChart.PenStyle = PenStyle.SolidPen

            ' select Channel's pen
            rtChart.LogicalChannel = channel
            rtChart.ChnDspPen = channel

            ' use change pen display action
            rtChart.ChnDspAction = ChannelDisplayAction.ChangePen

        Next channel

        rtChart.ChartAction = ChartAction.EnablePaint

    End Sub

    Private Sub ReInitialize_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _InitializeButton.Click

        Me.EraseAllChannels_Click(Me._EraseAllChannelsButton, New EventArgs())

        ' re-initialize everything as if restarting program
        Me.ResetKnownState()

    End Sub

    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    Private Sub RtChart1_MouseDown(ByRef button As Integer, ByRef shift As Integer, ByRef x As Single, ByRef y As Single,
                                   ByRef obj As Integer, ByRef position As Integer, ByRef objType As Integer)

        ' Event was not handled in the original VB4 code

        'Dim RtChart_ChnIntenseColor As Object
        '
        ' RtChart Mouse (button) Down event procedure, executes anytime mouse pointer is over Rtchart
        ' and mouse button is pressed down. Used to select channels (and cursors) for dragging.
        ' Changes plot color from normal to intense
        ' Mouse pointer is changed to "size" cursor and restored to "hand" in Mouse Up event procedure.
        '
        'RtChart1.SetFocus         ' move focus to RtChart control for copy and other functions
        '
        'If Obj > 0 And Obj <> LastObject Then ' if object selected, but not previously selected
        '
        'RtChart1.LogicalChannel = (Obj) ' select object that was hit
        ''UPGRADE_WARNING: (1068) RtChart_ChnIntenseColor of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
        'RtChart1.ChnDspAction = ((ChannelDisplayAction.IntenseColor)) ' use intense pen color to highlight
        '
        'ScreenToWindow(RtChart1, Obj, X, Y) ' convert screen positions to window coordinates
        '
        ' used to calculate offset from current position
        'MouseX = X ' store current mouse location X
        'MouseY = Y ' store current mouse location Y
        '
        'LastObject = Obj ' save selected object to be moved
        '
        'Select Case Obj ' if obj is data channel, change mouse pointer
        'Case ChannelName.Channel1 To ChannelName.Channel4 ' "hit" detection is only provided on time plots
        '
        ' system cursors must be selected with MousePointer (VB standard) property of RtChart
        'ReflectionHelper.LetMember(RtChart1, "MousePointer", SIZE_POINTER) ' select "size" pointer
        'CurrentPointer = SIZE_POINTER ' update current pointer value
        '
        ' update selected channel box
        'CurrentChannel.Text = Obj.ToString() ' if change event occurs, cursors will be redrawn
        'End Select
        '
        'End If
        '
    End Sub

    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    Private Sub RtChart1_MouseMove(ByRef button As Integer, ByRef shift As Integer,
                                   ByRef x As Single, ByRef y As Single, ByRef obj As Integer, ByRef position As Integer, ByRef objType As Integer)
        ' Event was not handled in the original VB4 code

        ' RtChart Mouse (pointer) Move event procedure, for every mouse pointer movement over Rtchart
        ' Used to drag channels (and cursors). Mouse pointer is changed to "crosshair" cursor if not over
        ' channel plot and to "hand" pointer if it is over plot and mouse is up.
        '
        ' Button = 0 for both buttons up, 1 for left button down, 2 for right button down
        '
        ' "hit" detection is only provided on time plots
        '
        ' if dragging trace (Button = 1), don't test for other possible mouse pointer changes
        'If Button = 0 Then
        ' if pointer needs to be changed AND mouse pointer is not over RtChart object
        'If CurrentPointer <> CrosshairCurHandle And Obj = 0 Then
        ' change mouse pointer to "Crosshair" when over RtChart for measurement
        'CurrentPointer = CrosshairCurHandle
        ' if pointer needs to be changed AND mouse pointer is over RtChart object
        'ElseIf CurrentPointer <> HandCurHandle And Obj > 0 Then 
        ' change mouse pointer to "Hand" when over channel or cursor
        'CurrentPointer = HandCurHandle
        'End If
        'End If
        '
        ' convert mouse pointer position to world coordinates (engineering units)
        'ScreenToWindow(RtChart1, SelectedChannel, X, Y) ' use currently selected channel's window
        ' update mouse pointer position text boxes with new location
        'CursorPosY.Text = CStr(Y)
        'CursorPosX.Text = CStr(X)
        '
        'If LastObject <= 0 Then 'Exit Sub ' no objects were selected by mouse down event, exit sub
        '
        'RtChart1.LogicalChannel = (LastObject) ' select object to change offset (move)
        '
        ' drag channels (move) by changing channel offsets
        ' use displacement relative to original position
        'Select Case LastObject
        'Case ChannelName.Cursor1 ' top voltage cursor (horizontal)
        ' must convert absolute mouse position values to a relative offset value
        ' new offset = old offset + new mouse position - previous mouse position
        'RtChart1.ChnDspOffsetY = (RtChart1.ChnDspOffsetY + Y - MouseY)
        ' store current Y-axis mouse position in RtChart Window coordinates
        'MouseY = Y
        ' store current Y-axis Cursor1 position for delta measurements
        'Cursor1Volts = MouseY
        ' update Cursor1 position text box
        'CurY1AbsPos.Text = Cursor1Volts.ToString()
        ' calculate and update voltage cursors' delta
        'DeltaVolts.Text = (Cursor1Volts - Cursor2Volts).ToString()
        '
        'Case ChannelName.Cursor2 ' bottom voltage cursor (horizontal)
        'RtChart1.ChnDspOffsetY = (RtChart1.ChnDspOffsetY + Y - MouseY)
        'MouseY = Y
        'Cursor2Volts = MouseY
        'CurY2AbsPos.Text = Cursor2Volts.ToString()
        'DeltaVolts.Text = (Cursor1Volts - Cursor2Volts).ToString()
        '
        'Case ChannelName.Cursor3 ' left time cursor (vertical)
        'RtChart1.ChnDspOffsetX = (RtChart1.ChnDspOffsetX + X - MouseX)
        'MouseX = X
        'Cursor3Time = MouseX
        'CurX3AbsPos.Text = Cursor3Time.ToString()
        'DeltaTime.Text = (Cursor4Time - Cursor3Time).ToString()
        ' move voltage cursor to data point intersecting with time cursor
        'If AutoCursor Then 'AutoMoveCursor(ChannelName.Cursor1, Cursor3Time)
        '
        'Case ChannelName.Cursor4 ' right time cursor (vertical)
        'RtChart1.ChnDspOffsetX = (RtChart1.ChnDspOffsetX + X - MouseX)
        'MouseX = X
        'Cursor4Time = MouseX
        'CurX4AbsPos.Text = Cursor4Time.ToString()
        'DeltaTime.Text = (Cursor4Time - Cursor3Time).ToString()
        ' move voltage cursor to data point intersecting with time cursor
        'If AutoCursor Then 'AutoMoveCursor(ChannelName.Cursor2, Cursor4Time)
        '
        'Case ChannelName.Channel1 To ChannelName.Channel4 ' for data channels 1, 2, 3, or 4
        'RtChart1.ChnDspOffsetX = (RtChart1.ChnDspOffsetX + X - MouseX)
        'RtChart1.ChnDspOffsetY = (RtChart1.ChnDspOffsetY + Y - MouseY)
        'MouseX = X
        'MouseY = Y
        '
        'Case Else
        '
        'End Select
        '
        ''UPGRADE_WARNING: (1068) RtChart_ChnTranslate of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
        'RtChart1.ChnDspAction = ChannelDisplayAction.Translate ' update channel position
        '
    End Sub

    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    Private Sub RtChart1_MouseUp(ByRef button As Integer, ByRef shift As Integer, ByRef x As Single, ByRef y As Single,
                                 ByRef obj As Integer, ByRef position As Integer, ByRef objType As Integer)
        ' Event was not handled in the original VB4 code

        ' RtChart Mouse (button) Up event procedure, executes anytime mouse pointer is over Rtchart
        ' and mouse button is let up. Changes plot color back to normal, Mouse pointer back to "hand" cursor
        ' Scroll bars are updated for changes in channel offset
        '
        'If LastObject > 0 Then ' if object was selected by mouse down event
        '
        ' change mouse pointer back to "Hand" on mouse button up when over channel or cursor
        'CurrentPointer = HandCurHandle ' update current pointer value
        '
        'ReflectionHelper.LetMember(RtChart1, "MousePointer", CurrentPointer) ' must select default mousepointer to get custom cursors
        '
        ' select channel (object)
        'RtChart1.LogicalChannel = (LastObject)
        ' "unselect" channel by changing pen color back to normal
        ''UPGRADE_WARNING: (1068) RtChart_ChnNormalColor of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
        'RtChart1.ChnDspAction = ((ChannelDisplayAction.NormalColor))
        '
        'Select Case LastObject
        'Case ChannelName.Channel1 To ChannelName.Channel4
        ' prevent each channel from being re-painted individually on Translate Action
        ''UPGRADE_WARNING: (1068) RtChart_DisablePaint of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
        'RtChart1.ChartAction =  ChartAction.DisablePaint
        '
        ' update scroll bar positions with new channel offsets
        'UpdateScrollBars(RtChart1, LastObject, RtChart1.ChnDspOffsetX, RtChart1.ChnDspOffsetY)
        '
        ' paint entire window with new channel positions for all channels
        ''UPGRADE_WARNING: (1068) RtChart_EnablePaint of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
        'RtChart1.ChartAction =  ChartAction.EnablePaint
        '
        'End Select
        '
        'LastObject = -1 ' object move is complete, reset value for mouse move event
        '
        'End If
        '
    End Sub



    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    Private Sub RtChart1_Refresh(ByRef chn As Integer, ByRef samples As Integer)
        ' Event was not handled in the original VB4 code

        ' RTChart.Action = RtChart_ReScale causes Refresh events for each displayed channel.
        ' Refresh occurs only if entire chart needs to be redrawn (new plot outside current plot area).
        ' Plots should be re-drawn after ReScale Action to maintain display resolution if changes in
        ' Window height and width are made.
        '
        ' re-draw data channels only
        'Select Case Chn
        'Case ChannelName.Channel1 To ChannelName.Channel4
        'result = RtChart1.ChartData(Chn, Samples, VBDataArrayY, firstpoint + (Chn - 1) * Samples, 0, 0)
        ' display returned status code converted to string
        'If result < 0 Then 'Interaction.MsgBox(RtChartError(result), MsgBoxStyle.Exclamation, "RtChart ChartData Error")
        '
        'Case ChannelName.XYChannel
        ' data arrays are passed for both horizontal and vertical axis
        'result = RtChart1.ChartData(ChannelName.XYChannel, Samples, VBDataArrayY, (NumChannels - 1) * Samples, VBDataArrayX, 0)
        ' display returned status code converted to string
        'If result < 0 Then 'Interaction.MsgBox(RtChartError(result), MsgBoxStyle.Exclamation, "RtChart ChartData Error")
        '
        'End Select
        '
    End Sub

    Private Sub SamplesBox_Enter(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _SamplesCountTextBox.Enter

        _SamplesCountTextBox.SelectionStart = 0
        _SamplesCountTextBox.SelectionLength = Strings.Len(_SamplesCountTextBox.Text)

    End Sub

    Private Sub SamplesBox_KeyDown(ByVal eventSender As Object, ByVal eventArgs As KeyEventArgs) Handles _SamplesCountTextBox.KeyDown
        Dim KeyCode As Keys = eventArgs.KeyCode
        Dim Shift As Integer = eventArgs.KeyData / 65536
        Try

            If KeyCode = Keys.Return Then
                ' prevent NULL or disallowed values from being used
                If String.IsNullOrEmpty(_SamplesCountTextBox.Text) Then
                    _SamplesCountTextBox.Text = _NumSamples.ToString()
                    Exit Sub
                Else
                    _TempValue = Conversion.Val(_SamplesCountTextBox.Text)
                    If _TempValue < 100 Or _TempValue > MAX_SAMPLES Then
                        _SamplesCountTextBox.Text = _NumSamples.ToString()
                        Exit Sub
                    End If
                End If

                ' VB imposes a 64K byte limit for arrays because the huge array format is undocumented.
                ' This is one reason why RtChart is limited to 16K points for single precision values.
                ' This example uses a 2D array for channel data, the maximum number of samples for 4 channels
                ' is therefore 4K per channel.
                If _TempValue > (MAX_SAMPLES / _NumChannels) Then
                    _SamplesCountTextBox.Text = (MAX_SAMPLES / _NumChannels).ToString()
                End If

                _NumSamples = CInt(_SamplesCountTextBox.Text)

                _ReInitChannnels = True

            End If
        Finally
            eventArgs.Handled = KeyCode = 0
        End Try

    End Sub

    Private Sub SamplesBox_Leave(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _SamplesCountTextBox.Leave

        SamplesBox_KeyDown(_SamplesCountTextBox, New KeyEventArgs(Keys.Return))

    End Sub

    Private Sub SamplesDiv_TextChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _SamplesPerDivisionTextBox.TextChanged
        If _IsInitializingComponent Then Return
        ' SamplesDiv_Change procedure moved to SamplesDiv_KeyDown
    End Sub

    Private Sub SamplesDiv_Enter(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _SamplesPerDivisionTextBox.Enter
        _SamplesPerDivisionTextBox.SelectionStart = 0
        _SamplesPerDivisionTextBox.SelectionLength = Strings.Len(_SamplesPerDivisionTextBox.Text)
    End Sub

    Private Sub SamplesDiv_KeyDown(ByVal eventSender As Object, ByVal eventArgs As KeyEventArgs) Handles _SamplesPerDivisionTextBox.KeyDown
        Dim KeyCode As Keys = eventArgs.KeyCode
        Dim Shift As Integer = eventArgs.KeyData / 65536
        Try

            ' change time per division scaling of RtChart window, redraw cursors,
            ' zero offsets, and update scroll bars

            If KeyCode = Keys.Return Then
                ' prevent NULL or disallowed values from being used
                If String.IsNullOrEmpty(_SamplesPerDivisionTextBox.Text) Then
                    _SamplesPerDivisionTextBox.Text = VoltsPerDiv.ToString()
                    Exit Sub
                Else
                    _TempValue = Conversion.Val(_SamplesPerDivisionTextBox.Text)
                    If _TempValue < 1 Or _TempValue > MAX_SAMPLESDIV Then
                        _SamplesPerDivisionTextBox.Text = VoltsPerDiv.ToString()
                        Exit Sub
                    End If
                End If

                ' get value if entered by keyboard
                SamplesPerDiv = CInt(_SamplesPerDivisionTextBox.Text)

                ' prevent each channel from being re-painted individually
                _Chart.ChartAction = ChartAction.DisablePaint

                ' update width properties for time window
                UpdateWindowWidth(_Chart, X_TIME_WINDOW, SamplesPerDiv)

                ' re-draw cursors at same grid-relative position based on new window width
                If _CursorsToggle.Checked Then DrawCursors(_Chart, _SelectedChannel)

                ' zero channel offsets after change in window height
                ZeroChannelOffsets(_Chart, ChannelName.Channel1, _NumChannels)

                ' update scroll bar range & reset position
                UpdateScrollBars(_Chart, _SelectedChannel, 0, 0)

                ' paint entire window with all channels at once
                _Chart.ChartAction = ChartAction.EnablePaint

            End If
        Finally
            eventArgs.Handled = KeyCode = 0
        End Try

    End Sub

    Private Sub SamplesDiv_Leave(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _SamplesPerDivisionTextBox.Leave

        SamplesDiv_KeyDown(_SamplesPerDivisionTextBox, New KeyEventArgs(Keys.Return))

    End Sub

    Private Sub Scroll_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _ScrollButton.Click

        ' stop continuous plotting if running is true
        If Running Then StartStop_Click(_StartStopButton, New EventArgs())

        If Not Scrolling Then
            Scrolling = True
            _ScrollButton.Text = "Stop"

            ' prevent each channel from being re-painted individually
            _Chart.ChartAction = ChartAction.DisablePaint

            ' de-initializing XY channel will also erase if it exists
            DeInitChannels(_Chart, ChannelName.XYChannel, ChannelName.XYChannel)

            _InitializeButton.Enabled = False
            _LineDisplayStyleRadioButton.Checked = True

            ' generate 2-dimensional array to plot
            Generate2D_SineWaveArray(_NumChannels, _NumSamples, _MaxVolts)

            ' setup RtChart control properties for scroll plotting
            InitializeScrollChannels(_Chart, _NumChannels, _NumSamples)

            ' forces reinit for other line plots
            _ReInitChannnels = True

            ' if CurrentChannel_Change event occurs, cursors will be redrawn
            _CurrentChannelTextBox.Text = CInt(ChannelName.Channel1).ToString()

            ' update scroll bar range & reset position
            UpdateScrollBars(_Chart, _SelectedChannel, 0, 0)

            ' paint entire window with all channels at once
            _Chart.ChartAction = ChartAction.EnablePaint

            ' call scroll plotting routine
            PlotDataScroll(_Chart, _NumChannels, _NumSamples, 1, _VBDataArrayY)

        Else : Scrolling = False
            _ScrollButton.Text = "Scroll"
            _InitializeButton.Enabled = True

        End If

    End Sub

    Private Sub Single_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _SingleButton.Click


        ' stop continuous plotting if running is true
        If Running Then StartStop_Click(_StartStopButton, New EventArgs())

        ' stop continuous plotting if scrolling is true
        If Scrolling Then Scroll_Click(_ScrollButton, New EventArgs())

        ' prevent each channel from being re-painted individually
        _Chart.ChartAction = ChartAction.DisablePaint

        ' de-initializing XY channel will also erase if it exists
        DeInitChannels(_Chart, ChannelName.XYChannel, ChannelName.XYChannel)

        ' generate 2-dimensional array to plot
        Generate2D_SineWaveArray(_NumChannels, _NumSamples, _MaxVolts)

        ' update RtChart channel and window properties
        ' NOTE: initialize display action causes current plots to be erased
        ' Initialize channels only if change to # of channels or samples occurred
        If _ReInitChannnels Then
            If UpdateChannels(_Chart, _NumChannels, _NumSamples) Then
                _ReInitChannnels = False
            Else
                ' UpdateChannels failed, exit sub
                ' enable paint before exiting sub
                _Chart.ChartAction = ChartAction.EnablePaint
                Exit Sub
            End If
        End If

        _CurrentChannelTextBox.Text = CInt(ChannelName.Channel1).ToString() ' if change event occurs, cursors will be redrawn

        ' update scroll bar range & reset position
        UpdateScrollBars(_Chart, _SelectedChannel, 0, 0)

        ' paint entire window with all channels at once
        _Chart.ChartAction = ChartAction.EnablePaint

        ' get random starting point based on 2% of numSamples for demonstration
        VBMath.Randomize()
        firstpoint = (VBMath.Rnd() * _NumSamples * 0.02)

        ' calls ChartData function once
        PlotData(_Chart, _NumChannels, _NumSamples, firstpoint, _VBDataArrayY)

    End Sub

    Private Sub SpinChannel_SpinDown()
        ' Event was not handled in the original VB4 code

        'NumChannels -= 1
        'If NumChannels < 1 Then 'NumChannels = 1
        '
        'ChannelBox.Text = NumChannels.ToString()
        '
        'SamplesBox_KeyDown(SamplesBox, New KeyEventArgs(Keys.Return))
        '
        ' De-initialize unused channel
        'DeInitChannels(RtChart1, NumChannels + 1, NumChannels + 1)
        '
    End Sub

    Private Sub SpinChannel_SpinUp()
        ' Event was not handled in the original VB4 code

        'NumChannels += 1
        'If NumChannels > ChannelName.LastChannel Then 'NumChannels = ChannelName.LastChannel
        '
        'ChannelBox.Text = NumChannels.ToString()
        '
        'SamplesBox_KeyDown(SamplesBox, New KeyEventArgs(Keys.Return))
        '
        'InitializeChannel(RtChart1, NumChannels, NumSamples)
        '
    End Sub

    Private Sub SpinSamples_SpinDown()
        ' Event was not handled in the original VB4 code

        'NumSamples -= 100
        'If NumSamples < 100 Then 'NumSamples = 100
        '
        'SamplesBox.Text = NumSamples.ToString()
        '
        'SamplesBox_KeyDown(SamplesBox, New KeyEventArgs(Keys.Return))
        '
    End Sub

    Private Sub SpinSamples_SpinUp()
        ' Event was not handled in the original VB4 code

        'NumSamples += 100
        'If NumSamples > MAX_SAMPLES Then 'NumSamples = MAX_SAMPLES
        '
        'SamplesBox.Text = NumSamples.ToString()
        '
        'SamplesBox_KeyDown(SamplesBox, New KeyEventArgs(Keys.Return))
        '
    End Sub

    Private Sub SpinSamplesDiv_SpinDown()
        ' Event was not handled in the original VB4 code

        'SamplesPerDiv -= 10
        'If SamplesPerDiv < 10 Then 'SamplesPerDiv = 10
        '
        'SamplesDiv.Text = SamplesPerDiv.ToString()
        '
        'SamplesDiv_KeyDown(SamplesDiv, New KeyEventArgs(Keys.Return))
        '
    End Sub

    Private Sub SpinSamplesDiv_SpinUp()
        ' Event was not handled in the original VB4 code

        'SamplesPerDiv += 10
        'If SamplesPerDiv > MAX_SAMPLESDIV Then 'SamplesPerDiv = MAX_SAMPLESDIV
        '
        'SamplesDiv.Text = SamplesPerDiv.ToString()
        '
        'SamplesDiv_KeyDown(SamplesDiv, New KeyEventArgs(Keys.Return))
        '
    End Sub

    Private Sub SpinViewports_SpinDown()
        ' Event was not handled in the original VB4 code

        'NumViewports -= 1
        'If NumViewports < 1 Then 'NumViewports = 1
        '
        'ViewportsBox.Text = NumViewports.ToString()
        '
        'ViewportsBox_KeyDown(ViewportsBox, New KeyEventArgs(Keys.Return))
        '
    End Sub

    Private Sub SpinViewports_SpinUp()
        ' Event was not handled in the original VB4 code

        'NumViewports += 1
        'If NumViewports > ChannelName.LastChannel Then 'NumViewports = ChannelName.LastChannel
        '
        'ViewportsBox.Text = NumViewports.ToString()
        '
        'ViewportsBox_KeyDown(ViewportsBox, New KeyEventArgs(Keys.Return))
        '
    End Sub

    Private Sub SpinVoltsDiv_SpinDown()
        ' Event was not handled in the original VB4 code

        'VoltsPerDiv -= 1
        'If VoltsPerDiv < 1 Then 'VoltsPerDiv = 1
        '
        'VoltsDiv.Text = VoltsPerDiv.ToString()
        '
        'VoltsDiv_KeyDown(VoltsDiv, New KeyEventArgs(Keys.Return))
        '
    End Sub

    Private Sub SpinVoltsDiv_SpinUp()
        ' Event was not handled in the original VB4 code

        'VoltsPerDiv += 1
        'If VoltsPerDiv > MAX_VOLTSDIV Then 'VoltsPerDiv = MAX_VOLTSDIV
        '
        'VoltsDiv.Text = VoltsPerDiv.ToString()
        '
        'VoltsDiv_KeyDown(VoltsDiv, New KeyEventArgs(Keys.Return))
        '
    End Sub

    Private Sub StartStop_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _StartStopButton.Click

        ' stop continuous plotting if scrolling is true
        If Program.Scrolling Then Scroll_Click(_ScrollButton, New EventArgs())

        If Not Program.Running Then
            Program.Running = True
            _StartStopButton.Text = "Stop"

            _InitializeButton.Enabled = False

            _LineDisplayStyleRadioButton.Checked = True

            ' prevent each channel from being re-painted individually
            _Chart.ChartAction = ChartAction.DisablePaint

            ' De-initializing XY channel will also erase if it exists
            DeInitChannels(_Chart, ChannelName.XYChannel, ChannelName.XYChannel)

            ' generate 2-dimensional arrays to plot "variable" wave
            Generate2D_SineWaveArray(_NumChannels, _NumSamples, _MaxVolts)

            ' update RtChart channel and window properties
            ' NOTE: initialize display action causes current plots to be erased
            ' Initialize channels only if change to # of channels or samples occurred
            If _ReInitChannnels Then
                If UpdateChannels(_Chart, _NumChannels, _NumSamples) Then
                    _ReInitChannnels = False
                Else
                    ' UpdateChannels failed, exit sub
                    ' enable paint before exiting sub
                    _Chart.ChartAction = ChartAction.EnablePaint
                    Exit Sub
                End If
            End If

            _CurrentChannelTextBox.Text = CInt(ChannelName.Channel1).ToString() ' if change event occurs, cursors will be redrawn

            ' update scroll bar range & reset position
            UpdateScrollBars(_Chart, _SelectedChannel, _Chart.ChnDspOffsetX, _Chart.ChnDspOffsetY)

            ' paint entire window with all channels at once
            _Chart.ChartAction = ChartAction.EnablePaint

            ' calls ChartData function continuously
            Program.PlotDataContinuous(_Chart, _NumChannels, _NumSamples, _VBDataArrayY)

        Else
            Program.Running = False
            _StartStopButton.Text = "Start"
            _InitializeButton.Enabled = True

        End If

    End Sub

    Private Sub StorageOn_CheckedChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _StorageToggle.CheckedChanged
        Dim Value As Integer = IIf(CType(eventSender, CheckBox).Checked, -1, 0)

        ' keep storage mode the same for all view ports (although not required)
        For _I = 1 To _Chart.Viewports ' Viewports is number of view ports currently displayed
            _Chart.LogicalViewport = _I ' select view port
            _Chart.ViewportStorageOn = Value ' enables storage mode when true
        Next _I

    End Sub

    Private Sub UpdateScrollBars(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal channel As Integer,
                                 ByVal xOffset As Single, ByVal yOffset As Single)
        Dim newLargeChange3, newLargeChange2, newLargeChange As Integer

        ' Called by RtChart MouseUp procedure to synchronize scroll bar Value with new channel offset.
        ' Called by VoltsDiv_Change & SamplesDiv_Change to rescale scroll bars according to new window size.
        ' Called to change horizontal scroll bar according to number of samples in plot.

        ' select channel
        rtChart.LogicalChannel = channel

        Select Case rtChart.ChnDspWindow ' scale horizontal scroll bar according to channel's window
            Case X_TIME_WINDOW

                _HScrollBarScaler = 1

                ' Updates horizontal scroll bar Min & Max for change in number of samples for
                ' time scales (0 - tmax), use ChnDspBufLen to allow full range of offset.

                _HorizontalScrollBar.Minimum = (-rtChart.ChnDspBufLen) * _HScrollBarScaler
                _HorizontalScrollBar.Maximum = (rtChart.ChnDspBufLen * _HScrollBarScaler + _HorizontalScrollBar.LargeChange - 1)

                _HorizontalScrollBar.SmallChange = rtChart.ChnDspBufLen * 0.01 * _HScrollBarScaler
                newLargeChange = rtChart.ChnDspBufLen * 0.1 * _HScrollBarScaler
                _HorizontalScrollBar.Maximum = _HorizontalScrollBar.Maximum + newLargeChange - _HorizontalScrollBar.LargeChange
                _HorizontalScrollBar.LargeChange = newLargeChange

            Case X_VOLTS_WINDOW

                _HScrollBarScaler = _VScrollBarScaler

                ' Min is left (negative volts), Max is right (positive volts)
                ' use VScrollBarScaler because Y-axis is scaled the same
                _HorizontalScrollBar.Minimum = (-rtChart.WndWidth) * _HScrollBarScaler
                _HorizontalScrollBar.Maximum = (rtChart.WndWidth * _HScrollBarScaler + _HorizontalScrollBar.LargeChange - 1)

                _HorizontalScrollBar.SmallChange = rtChart.WndWidth * 0.01 * _HScrollBarScaler
                newLargeChange2 = rtChart.WndWidth * 0.1 * _HScrollBarScaler
                _HorizontalScrollBar.Maximum = _HorizontalScrollBar.Maximum + newLargeChange2 - _HorizontalScrollBar.LargeChange
                _HorizontalScrollBar.LargeChange = newLargeChange2

        End Select

        ' prevent Value from exceeding Min or Max
        xOffset *= _HScrollBarScaler
        If xOffset < _HorizontalScrollBar.Minimum Then xOffset = _HorizontalScrollBar.Minimum
        If xOffset > (_HorizontalScrollBar.Maximum - (_HorizontalScrollBar.LargeChange + 1)) Then xOffset = (_HorizontalScrollBar.Maximum - (_HorizontalScrollBar.LargeChange + 1))
        _HorizontalScrollBar.Value = xOffset

        ' Updates vertical scroll bar Min & Max for change in volts/div.
        ' Use WndHeight to allow full range of offset scroll bar.
        ' Min, Max, & Value are multiplied by ScrollBarScaler to increase resolution because
        ' scroll bar properties are integers and RtChart offset properties are single precision.

        ' Min is top (positive volts), Max is bottom (negative volts)
        _VerticalScrollBar.Maximum = rtChart.WndHeight * _VScrollBarScaler
        _VerticalScrollBar.Minimum = (-rtChart.WndHeight) * _VScrollBarScaler + _VerticalScrollBar.LargeChange - 1

        ' prevent Value from exceeding Min or Max
        yOffset *= _VScrollBarScaler
        If yOffset > _VerticalScrollBar.Maximum Then yOffset = _VerticalScrollBar.Maximum
        If yOffset < _VerticalScrollBar.Minimum + (_VerticalScrollBar.LargeChange + 1) Then yOffset = _VerticalScrollBar.Minimum + (_VerticalScrollBar.LargeChange + 1)
        _VerticalScrollBar.Value = yOffset

        _VerticalScrollBar.SmallChange = rtChart.WndHeight * 0.01 * _VScrollBarScaler
        newLargeChange3 = rtChart.WndHeight * 0.1 * _VScrollBarScaler
        _VerticalScrollBar.Maximum = _VerticalScrollBar.Maximum - newLargeChange3 + _VerticalScrollBar.LargeChange
        _VerticalScrollBar.LargeChange = newLargeChange3

    End Sub

    Private Sub UpdateViewPorts(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByRef viewports As Integer)

        ' update RtChart Channel Viewport properties
        rtChart.Viewports = (viewports) ' number of viewports

        ' keep same storage mode for all view ports
        For _I = 1 To viewports
            rtChart.LogicalViewport = (_I) ' select view port
            rtChart.ViewportStorageOn = (_StorageToggle.Checked) ' enables storage mode when true

            ' change # of Y scales as # of view ports changes
            InitVerticalScale(_Chart, _I, _I, X_TIME_WINDOW)
        Next _I

        ' keep X scale on bottom of chart as # of view ports changes
        InitHorizontalScale(_Chart, 10, viewports, X_TIME_WINDOW)

        ' paint entire window with new channel positions for all channels
        _Chart.ChartAction = ChartAction.DisablePaint

        ' assign each channel to a different Viewport if Viewports => than channels
        SelectViewPorts(_Chart, _NumChannels)

        ' update cursor Window and View Port assignments based on selected channel
        If _CursorsToggle.Checked Then
            ' select channel to get currently selected channel's window & view port
            _Chart.LogicalChannel = (_SelectedChannel)
            UpdateCursors(_Chart, ChannelName.Cursor1, _Chart.ChnDspWindow, _Chart.ChnDspViewport)
        End If

        ' paint entire window with new channel positions for all channels
        _Chart.ChartAction = ChartAction.EnablePaint

    End Sub

    Private Sub ViewportsBox_TextChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _ViewportsTextBox.TextChanged
        If _IsInitializingComponent Then Return

        ViewportsBox_KeyDown(_ViewportsTextBox, New KeyEventArgs(Keys.Return))

    End Sub

    Private Sub ViewportsBox_Enter(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _ViewportsTextBox.Enter

        _ViewportsTextBox.SelectionStart = 0
        _ViewportsTextBox.SelectionLength = Strings.Len(_ViewportsTextBox.Text)

    End Sub

    Private Sub ViewportsBox_KeyDown(ByVal eventSender As Object, ByVal eventArgs As KeyEventArgs) Handles _ViewportsTextBox.KeyDown
        Dim KeyCode As Keys = eventArgs.KeyCode
        Dim Shift As Integer = eventArgs.KeyData / 65536
        Try

            If KeyCode = Keys.Return Then

                ' prevent NULL or disallowed values from being used
                If String.IsNullOrEmpty(_ViewportsTextBox.Text) Then
                    _ViewportsTextBox.Text = _NumViewports.ToString()
                    Exit Sub
                Else
                    _TempValue = Conversion.Val(_ViewportsTextBox.Text)
                    If _TempValue < ChannelName.Channel1 Or _TempValue > ChannelName.LastChannel Then
                        _ViewportsTextBox.Text = _NumViewports.ToString()
                        Exit Sub
                    End If
                End If

                _NumViewports = CInt(_ViewportsTextBox.Text)

                UpdateViewPorts(_Chart, _NumViewports)

            End If
        Finally
            eventArgs.Handled = KeyCode = 0
        End Try

    End Sub

    Private Sub ViewportsBox_Leave(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _ViewportsTextBox.Leave

        ViewportsBox_KeyDown(_ViewportsTextBox, New KeyEventArgs(Keys.Return))

    End Sub

    Private Sub VoltsDiv_TextChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _VoltsPerDivisionTextBox.TextChanged
        If _IsInitializingComponent Then Return

        ' VoltsDiv_Change proc moved to VoltsDiv_KeyDown

    End Sub

    Private Sub VoltsDiv_Enter(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _VoltsPerDivisionTextBox.Enter

        _VoltsPerDivisionTextBox.SelectionStart = 0
        _VoltsPerDivisionTextBox.SelectionLength = Strings.Len(_VoltsPerDivisionTextBox.Text)

    End Sub

    Private Sub VoltsDiv_KeyDown(ByVal eventSender As Object, ByVal eventArgs As KeyEventArgs) Handles _VoltsPerDivisionTextBox.KeyDown
        Dim KeyCode As Keys = eventArgs.KeyCode
        Dim Shift As Integer = eventArgs.KeyData / 65536
        Try

            ' change volts per division scaling of RtChart window, redraw cursors,
            ' zero offsets, and update scroll bars

            If KeyCode = Keys.Return Then
                ' prevent NULL or disallowed values from being used
                If String.IsNullOrEmpty(_VoltsPerDivisionTextBox.Text) Then
                    _VoltsPerDivisionTextBox.Text = VoltsPerDiv.ToString()
                    Exit Sub
                Else
                    _TempValue = Conversion.Val(_VoltsPerDivisionTextBox.Text)
                    If _TempValue < 1 Or _TempValue > MAX_VOLTSDIV Then
                        _VoltsPerDivisionTextBox.Text = VoltsPerDiv.ToString()
                        Exit Sub
                    End If
                End If

                ' get value if entered by keyboard
                VoltsPerDiv = CSng(_VoltsPerDivisionTextBox.Text)

                ' prevent each channel from being re-painted individually
                _Chart.ChartAction = ChartAction.DisablePaint

                ' update height properties for time window
                UpdateWindowHeight(_Chart, X_TIME_WINDOW, VoltsPerDiv)

                ' update height and width properties for XY plot window
                UpdateWindow_XY(_Chart, X_VOLTS_WINDOW, VoltsPerDiv)

                ' re-draw cursors at same grid-relative position based on new window height
                If _CursorsToggle.Checked Then DrawCursors(_Chart, _SelectedChannel)

                ' zero channel offsets after change in window height
                ZeroChannelOffsets(_Chart, ChannelName.Channel1, _NumChannels)

                ' update scroll bar range & reset position
                UpdateScrollBars(_Chart, _SelectedChannel, 0, 0)

                ' paint entire window with all channels at once
                _Chart.ChartAction = ChartAction.EnablePaint

            End If
        Finally
            eventArgs.Handled = KeyCode = 0
        End Try

    End Sub

    Private Sub VoltsDiv_Leave(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _VoltsPerDivisionTextBox.Leave

        VoltsDiv_KeyDown(_VoltsPerDivisionTextBox, New KeyEventArgs(Keys.Return))

    End Sub

    Private Sub VerticalScrollBar_ValueChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _VerticalScrollBar.ValueChanged


        ' scroll bar Min, Max, & Value are multiplied by VScrollBarScaler to increase resolution
        ' as these properties are integers and RtChart offsets are single precision
        ' scroll bar Value must be divided by VScrollBarScaler to return it to a ChnDspOffset value


        If _YTrace_Lock Then ' keep all channels' offset equal on Y-axis

            ' prevent each channel from being re-painted individually on Translate Action
            _Chart.ChartAction = ChartAction.DisablePaint

            For channel As Integer = ChannelName.Channel1 To _NumChannels
                _Chart.LogicalChannel = channel ' select channel

                _Chart.ChnDspOffsetY = -(_VerticalScrollBar.Value / _VScrollBarScaler) ' use same offset for all channels

                ' use same offset for all channels
                _Chart.ChnDspAction = ChannelDisplayAction.Translate ' update channel position

            Next channel

            ' paint entire window with new channel positions for all channels
            _Chart.ChartAction = ChartAction.EnablePaint

        Else

            ' change offset for selected channel only
            _Chart.LogicalChannel = Me._SelectedChannel
            _Chart.ChnDspOffsetY = -_VerticalScrollBar.Value / _VScrollBarScaler
            _Chart.ChnDspAction = ChannelDisplayAction.Translate ' update channel position

        End If

        ' update X-axis offset text box
        _YOffsetTextBox.Text = CStr(_Chart.ChnDspOffsetY)

    End Sub

    'UPGRADE_NOTE: (2010) VScroll1.Scroll was changed from an event to a procedure. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2010
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    Private Sub VScroll1_Scroll_Renamed(ByVal newScrollValue As Integer)
        If _IsInitializingComponent Then Return
        ' call change event procedure so dragging will cause RtChart to move offset
        VerticalScrollBar_ValueChanged(_VerticalScrollBar, New EventArgs())
    End Sub

    Private Sub XOffsetBox_TextChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _XOffsetTextBox.TextChanged
        If _IsInitializingComponent Then Return
        _Chart.LogicalChannel = (_SelectedChannel) ' select channel
        _Chart.ChnDspOffsetX = (Conversion.Val(_XOffsetTextBox.Text)) ' get offset
        _Chart.ChnDspAction = ChannelDisplayAction.Translate ' update channel position
    End Sub

    Private Sub XTraceLock_CheckStateChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _XTraceLockToggle.CheckStateChanged
        If _IsInitializingComponent Then Return

        _XTrace_Lock = _XTraceLockToggle.Checked
        HScroll1_ValueChanged(_HorizontalScrollBar, New EventArgs())

    End Sub

    Private Sub YOffsetBox_TextChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _YOffsetTextBox.TextChanged
        If _IsInitializingComponent Then Return
        _Chart.LogicalChannel = (_SelectedChannel) ' select channel
        _Chart.ChnDspOffsetY = (Conversion.Val(_YOffsetTextBox.Text)) ' get offset
        _Chart.ChnDspAction = ChannelDisplayAction.Translate ' update channel position
    End Sub

    Private Sub YTraceLock_CheckStateChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _YTraceLockToggle.CheckStateChanged
        If _IsInitializingComponent Then Return

        _YTrace_Lock = _YTraceLockToggle.Checked
        VerticalScrollBar_ValueChanged(_VerticalScrollBar, New EventArgs())

    End Sub

    Private Sub ZeroChannels_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _ZeroChannelsButton.Click


        ' prevent each channel from being re-painted individually
        _Chart.ChartAction = ChartAction.DisablePaint

        ' zero data channel offsets
        ZeroChannelOffsets(_Chart, ChannelName.Channel1, _NumChannels)

        ' update scroll bar range & reset position
        UpdateScrollBars(_Chart, _SelectedChannel, 0, 0)

        ' paint entire window with new positions for all channels
        _Chart.ChartAction = ChartAction.EnablePaint

    End Sub
    Private Sub HScroll1_Scroll(ByVal eventSender As Object, ByVal eventArgs As ScrollEventArgs) Handles _HorizontalScrollBar.Scroll
        Select Case eventArgs.Type
            Case ScrollEventType.ThumbTrack
                HScroll1_Scroll_Renamed(eventArgs.NewValue)
        End Select
    End Sub
    Private Sub VScroll1_Scroll(ByVal eventSender As Object, ByVal eventArgs As ScrollEventArgs) Handles _VerticalScrollBar.Scroll
        Select Case eventArgs.Type
            Case ScrollEventType.ThumbTrack
                VScroll1_Scroll_Renamed(eventArgs.NewValue)
        End Select
    End Sub

#End Region

End Class

