' LabOBJX Real-Time Chart Source Code Example Library
' (C) Copyright 1995, Scientific Software Tools, Inc.
' All Rights Reserved.


Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic
Imports System
Imports System.Windows.Forms
Imports isr.Visuals.RealTimeChart

Friend Module Program
#If False Then
    <STAThread>
    Sub Main()
        Application.EnableVisualStyles()
        Application.SetCompatibleTextRenderingDefault(False)
        Try
            Dim tempLoadForm As RTCDemoForm = RTCDemoForm.DefInstance
            Application.Run(RTCDemoForm.DefInstance)
        Catch
            System.Windows.Forms.MessageBox.Show($"Sub Main Error {Information.Err().Number} {Conversion.ErrorToString(Information.Err().Number)}", Application.ProductName)
            Application.Exit()
        End Try
    End Sub
#End If

    Public Const AssemblyTitle As String = "Real Time Chart Demo"
    Public Const AssemblyDescription As String = "Real Time Demo"
    Public Const AssemblyProduct As String = "Visuals.Real.Time.Chart.Demo"


    <CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>")>
    Public Result As Integer ' return value from function call

    <CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>")>
    Public Running As Integer ' boolean indicates when plotting in CRT mode

    <CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>")>
    Public Scrolling As Integer ' boolean indicates when plotting in Scroll mode


    <CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>")>
    Public VoltsPerDiv As Single ' vertical scaling is based on volts per division

    <CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>")>
    Public SamplesPerDiv As Single ' horizontal scaling is based on seconds per division


    <CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>")>
    Public firstpoint As Integer ' random starting point in data array for demonstration

    Public Const TWO_PIE As Double = 2 * 3.1415926535898

    Public Const MAX_VOLTSDIV As Integer = 50
    Public Const MAX_SAMPLESDIV As Integer = 2000
    Public Const MAX_SAMPLES As Integer = 16000

    ' window constants
    Public Const X_TIME_WINDOW As Integer = 1
    Public Const X_VOLTS_WINDOW As Integer = 2

    ' grid constants
    Public Const MAJOR_HORZ_DIV As Integer = 10
    Public Const MAJOR_VERT_DIV As Integer = 8
    Public Const MINOR_DIVISIONS As Integer = 5

    ' unique value for scale pen name
    Public Const SCALE_PEN As Integer = 10

    Public Sub DeInitChannels(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal firstChannel As Integer, ByVal lastChannel As Integer)

        ' prevent each channel from being erased individually
        rtChart.ChartAction = ChartAction.DisablePaint

        ' De-initialize specified channels
        For channel As Integer = firstChannel To lastChannel
            ' use negative channel value to De-initialize channel
            rtChart.LogicalChannel = (-(channel))
        Next channel

        ' paint entire window to erase all channels
        rtChart.ChartAction = ChartAction.EnablePaint

    End Sub

    Public Sub DrawCursor(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal channel As Integer,
                          ByVal x1 As Single, ByVal y1 As Single, ByVal x2 As Single, ByVal y2 As Single)
        ' draws a cursor (2-point line) at the coordinates given

        ' Draw a single cursor
        Dim LineCoord(4) As Single

        ' assign coordinates of cursor line to array
        LineCoord(0) = x1
        LineCoord(1) = y1
        LineCoord(2) = x2
        LineCoord(3) = y2

        ' pass 2 points (4 coordinates) for single line in LineCoord array
        Result = rtChart.ChartData(channel, 2, LineCoord) ', 0, 0, 0)

        ' display returned status code converted to string
        If Result < 0 Then Windows.Forms.MessageBox.Show(ChartPen.ChartErrorMessage(Result), "RtChart ChartData Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

    End Sub

    Public Sub DrawCursors(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByRef selectedChannel As Integer)

        ' determines the coordinates to draw four cursors at fixed positions between two grid lines
        ' based on the current channel's window size and calls DrawCursor to plot

        ' cursor position variables

        ' prevent each cursor from being re-painted individually on Initialize Action
        rtChart.ChartAction = ChartAction.DisablePaint

        ' zero cursor channel offsets
        ZeroChannelOffsets(rtChart, ChannelName.Cursor1, ChannelName.Cursor4)

        ' select channel to get currently selected channel's window & view port
        rtChart.LogicalChannel = (selectedChannel)

        ' update cursor Window and View Port assignments based on selected channel
        UpdateCursors(rtChart, ChannelName.Cursor1, rtChart.ChnDspWindow, rtChart.ChnDspViewport)

        ' select appropriate window to reference specified Window properties
        rtChart.LogicalWindow = rtChart.ChnDspWindow ' select "time" window

        ' calculate absolute full-scale (maximum) window position
        Dim PosFS_WndY As Single = rtChart.WndYmin + rtChart.WndHeight
        Dim PosFS_WndX As Single = rtChart.WndXmin + rtChart.WndWidth

        ' assign values to cursor position variables, locate cursors 1.5 divs inside frame
        Dim PosY1 As Single = PosFS_WndY - 1.5 * rtChart.WndHeight / MAJOR_VERT_DIV ' -1.5 * (WndHeight / 8 divs)
        Dim PosY2 As Single = rtChart.WndYmin + 1.5 * rtChart.WndHeight / MAJOR_VERT_DIV ' +1.5 * (WndHeight / 8 divs)
        Dim PosX3 As Single = rtChart.WndXmin + 1.5 * rtChart.WndWidth / MAJOR_HORZ_DIV ' +1.5 * (WndWidth / 10 divs)
        Dim PosX4 As Single = PosFS_WndX - 1.5 * rtChart.WndWidth / MAJOR_HORZ_DIV ' -1.5 * (WndWidth / 10 divs)

        ' plot four cursor lines
        DrawCursor(rtChart, ChannelName.Cursor1, rtChart.WndXmin, PosY1, PosFS_WndX, PosY1)
        DrawCursor(rtChart, ChannelName.Cursor2, rtChart.WndXmin, PosY2, PosFS_WndX, PosY2)
        DrawCursor(rtChart, ChannelName.Cursor3, PosX3, rtChart.WndYmin, PosX3, PosFS_WndY)
        DrawCursor(rtChart, ChannelName.Cursor4, PosX4, rtChart.WndYmin, PosX4, PosFS_WndY)

        ' paint entire window with new positions for all cursors
        rtChart.ChartAction = ChartAction.EnablePaint

        ' update text readout of absolute cursor position values
        ' update text readout of absolute cursor position values
        ChartDemoForm.DefInstance.CurrentY1AbsolutePositionCaption = PosY1.ToString()
        ChartDemoForm.DefInstance.CurrentY2AbsolutePositionCaption = PosY2.ToString()
        ChartDemoForm.DefInstance.CurrentX3AbsolutePositionCaption = PosX3.ToString()
        ChartDemoForm.DefInstance.CurrentX4AbsolutePositionCaption = PosX4.ToString()

        ' update text readout of relative (delta) cursor position values
        ChartDemoForm.DefInstance.DeltaVoltageCaption = (PosY1 - PosY2).ToString()
        ChartDemoForm.DefInstance.DeltaTimeCaption = (PosX4 - PosX3).ToString()

    End Sub

    Public Sub EraseChannels(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal firstChannel As Integer, ByVal lastChannel As Integer)

        ' prevent each channel from being erased individually
        rtChart.ChartAction = ChartAction.DisablePaint

        ' erase specified channels
        For channel As Integer = firstChannel To lastChannel
            rtChart.LogicalChannel = channel ' select channel to erase
            rtChart.ChnDspAction = ChannelDisplayAction.Erase ' use channel erase display action
        Next channel

        ' paint entire window to erase all channels
        rtChart.ChartAction = ChartAction.EnablePaint

    End Sub

    Public Sub InitFonts(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl)

        ' initialize scale pen
        rtChart.LogicalPen = Program.SCALE_PEN ' define pen for scale
        rtChart.PenColor = Color.White
        rtChart.PenIntenseColor = Color.Yellow
        rtChart.PenWidth = 1
        rtChart.PenStyle = PenStyle.SolidPen

        ' The font properties are global for all RtChart Scales and Labels
        rtChart.FontBold = False
        rtChart.FontItalic = False
        rtChart.FontName = "Arial"
        rtChart.FontSize = 6
        rtChart.FontStrikeout = False
        rtChart.FontUnderline = False

    End Sub

    Public Sub InitHorizontalScale(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal scaleSelect As Integer, ByVal scaleViewport As Integer, ByVal scaleWindow As Integer)

        ' initialize horizontal scale
        rtChart.LogicalScale = scaleSelect ' select scale name (same for both dimensions)
        rtChart.ScaleViewport = scaleViewport ' specify view port before selecting dimension
        rtChart.ScaleWindow = scaleWindow ' specify window to be scaled before selecting dimension

        rtChart.ScaleDimension = LogicalDimension.Horizontal ' select horizontal or vertical scale/label

        rtChart.ScalePen = SCALE_PEN ' specify pen color for scale

        rtChart.ScaleLocation = ScaleLocation.RightBottom ' Scale Location: axis or frame
        rtChart.ScalePosition = Position.BelowRight ' Scale Position: above or below ScaleLocation

        rtChart.ScaleOrientation = Orientation.Parallel ' Scale Orientation: Parallel or Perpendicular to ScaleDimension
        rtChart.ScalePath = Path.Right ' Scale Path: direction which characters follow: left. right, up, down
        rtChart.ScaleGap = 3 ' Scale Gap: space between ScaleLocation and scale/label in pixels

        rtChart.ScaleFactor = 1 ' multiplier used to convert window units to scale values
        rtChart.ScalePlaces = 0 ' number of significant digits to the right of decimal point

        ' update RtChart Label properties
        rtChart.LabelCaption = "Seconds" ' specify Label Caption text: "volts", "secs" , etc.
        rtChart.LabelPen = SCALE_PEN ' specify pen color for Label

        rtChart.LabelLocation = LabelLocation.Center ' label location relative to scale: left, right, center
        rtChart.LabelPosition = Position.BelowRight ' label Position: above, below, or on LabelLocation
        rtChart.LabelOrientation = Orientation.Parallel ' label character orientation relative to ScaleDimension
        rtChart.LabelPath = Path.Right ' Scale Path: direction which characters follow

        rtChart.ScaleVisibility = ScaleVisibility.ScaleAndLabel ' scale visibility: none, scale, label, or both
        rtChart.ScaleAction = ScaleAction.Initialize ' execute selected Scale Action

    End Sub

    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub InitializeChannel(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal channel As Integer, ByVal samplesPerChannel As Integer)

        ' Initialize Window and Channels for CRT mode, line style, buffer length,
        ' single precision auto inc scalars
        Try
            ' Window Description
            rtChart.LogicalWindow = X_TIME_WINDOW ' select X-axis time window
            rtChart.WndXmin = 0 ' left minimum abscissa
            rtChart.WndWidth = SamplesPerDiv * MAJOR_HORZ_DIV ' in Seconds (samples/div * 10 divisions) 100 * 10 = 1000
            rtChart.WndHeight = VoltsPerDiv * MAJOR_VERT_DIV ' in Volts (Volts/div * 8 divisions) 5 * 8 = 40
            rtChart.WndYmin = rtChart.WndHeight / -2 ' bottom minimum ordinate (negative full scale) 40 / -2 = -20

            ' Describe Channel
            rtChart.LogicalChannel = channel ' select channel
            rtChart.ChnDspWindow = X_TIME_WINDOW ' assign X-axis (time) window to channel
            rtChart.ChnDspViewport = 1 ' specify view port
            rtChart.ChnDspPen = channel ' specify pen, pens have same value as channel
            rtChart.ChnDspBufLen = samplesPerChannel ' input buffer length for each channel
            rtChart.ChnDspMode = ChannelDisplayMode.CRT ' per channel display mode, use RunMode constants
            rtChart.ChnDspStyle = ChannelDisplayStyle.Lines ' use line style for plotting arrays
            rtChart.ChnDspOffsetX = 0 ' set offset to 0
            rtChart.ChnDspOffsetY = 0 ' set offset to 0

            ' Describe horizontal dimension
            rtChart.ChannelDataDimension = LogicalDimension.Horizontal
            rtChart.ChnDataShape = ChannelDataShape.AutoIncr
            rtChart.ChnDataOffset = 0 ' offset into buffer (in points)
            rtChart.ChnDataIncr = 1 ' logical increment to next value in array

            ' Describe vertical dimension
            rtChart.ChannelDataDimension = LogicalDimension.Vertical
            rtChart.ChnDataShape = ChannelDataShape.Scalar
            rtChart.ChnDataType = ChannelDataType.Single
            rtChart.ChnDataOffset = 0 ' offset into buffer (in points)
            rtChart.ChnDataIncr = 1 ' logical increment to next value for 2D array
            rtChart.ChnDataXform = 0

            ' initialize channel
            rtChart.ChnDspAction = ChannelDisplayAction.Initialize
        Catch
            System.Windows.Forms.MessageBox.Show($"Channel Initialization Error #{Information.Err().Number} {Conversion.ErrorToString()}", Application.ProductName)
            Exit Sub
        End Try

    End Sub

    Public Sub InitializeCursors(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal window As Integer)

        ' Setup cursor channel parameters - 2 point line mode, pass location values as pairs
        For channel As Integer = ChannelName.Cursor1 To ChannelName.Cursor4

            rtChart.LogicalChannel = channel
            rtChart.ChnDspWindow = window ' logical coordinates range
            rtChart.ChnDspViewport = 1 ' default display location
            rtChart.ChnDspPen = ChannelName.Cursor1 ' drawing pen, uses same name as cursor1
            rtChart.ChnDspBufLen = 2 ' input buffer length is 2 point for line
            rtChart.ChnDspMode = ChannelDisplayMode.CRT ' per channel display mode, use RunMode constants
            rtChart.ChnDspStyle = ChannelDisplayStyle.Lines ' choose line mode
            rtChart.ChnDspOffsetX = 0 ' zero X offset
            rtChart.ChnDspOffsetY = 0 ' zero Y offset

            ' Describe input channel as 2 point line for cursor
            rtChart.ChannelDataDimension = LogicalDimension.Vertical ' use only vertical dimension for a cursor
            rtChart.ChnDataShape = ChannelDataShape.Pair ' supply 2 points as pairs (4 values)
            rtChart.ChnDataType = ChannelDataType.Single ' use single precision, same as data channel
            rtChart.ChnDataOffset = 0 ' offset into buffer (in points)
            rtChart.ChnDataIncr = 1 ' logical increment to next value

            rtChart.ChnDspAction = ChannelDisplayAction.Initialize

        Next channel

    End Sub

    Public Sub InitializePointChannels(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal numChannels As Integer, ByVal samplesPerChannel As Integer)

        ' Initialize Window and Channels for CRT mode, point style, buffer length,
        ' single precision auto increment scalars

        ' Window Description
        rtChart.LogicalWindow = X_TIME_WINDOW ' select window
        rtChart.WndXmin = 0 ' left minimum abscissa
        rtChart.WndWidth = (SamplesPerDiv * MAJOR_HORZ_DIV) ' in Seconds (Time/div * 10 divisions)
        rtChart.WndHeight = (VoltsPerDiv * MAJOR_VERT_DIV) ' in Volts (Volts/div * 8 divisions)
        rtChart.WndYmin = (rtChart.WndHeight / -2) ' bottom minimum ordinate

        For channel As Integer = ChannelName.Channel1 To numChannels

            ' Describe Channel
            rtChart.LogicalChannel = channel ' select channel
            rtChart.ChnDspWindow = X_TIME_WINDOW ' assign X-axis (time) window to channel

            ' assign each channel to a different View port if View ports => than channels
            If rtChart.Viewports >= channel Then
                rtChart.ChnDspViewport = channel ' each channel in its own separate view port
            ElseIf channel = ChannelName.Channel4 And rtChart.Viewports = 2 Then
                rtChart.ChnDspViewport = 2 ' assign channel 4 to view port 2, if available
            Else
                rtChart.ChnDspViewport = 1 ' all channels assigned to view port 1
            End If

            rtChart.ChnDspPen = channel ' specify pen
            rtChart.ChnDspBufLen = samplesPerChannel ' total input buffer length
            rtChart.ChnDspMode = ChannelDisplayMode.CRT ' per channel display mode
            rtChart.ChnDspStyle = ChannelDisplayStyle.Points ' use point style for plotting single points
            rtChart.ChnDspOffsetX = 0
            rtChart.ChnDspOffsetY = 0

            ' Describe horizontal dimension
            rtChart.ChannelDataDimension = LogicalDimension.Horizontal
            rtChart.ChnDataShape = ChannelDataShape.AutoIncr
            rtChart.ChnDataOffset = 0 ' offset into buffer (in points)
            rtChart.ChnDataIncr = 1 ' logical increment to next value in array

            ' Describe vertical dimension
            rtChart.ChannelDataDimension = LogicalDimension.Vertical
            rtChart.ChnDataShape = ChannelDataShape.Scalar
            rtChart.ChnDataType = ChannelDataType.Single
            rtChart.ChnDataOffset = 0 ' offset into buffer (in points)
            rtChart.ChnDataIncr = 1 ' logical increment to next value in array

            rtChart.ChnDspAction = ChannelDisplayAction.Initialize

        Next channel

    End Sub

    Public Sub InitializeRtChart(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl)

        ' set global operation properties, color, border, bevel, grid, frame, axis, etc.

        ' Global operation properties
        rtChart.ImageFile = String.Empty ' specifies file name for Write action
        rtChart.HitTest = True ' enables mouse pointer "hit" detection
        rtChart.FramesPerSec = 0 ' 0 for asynchronous, -1 for synchronous

        ' Border description properties
        rtChart.BorderColor = Color.LightGray
        rtChart.LightColor = Color.DarkGray
        rtChart.ShadowColor = Color.Black
        rtChart.BevelInner = BevelStyle.Inset
        rtChart.BevelOuter = BevelStyle.Raised
        rtChart.BevelWidthInner = 4
        rtChart.BevelWidthOuter = 4
        rtChart.BorderWidth = 3
        rtChart.Outline = True
        rtChart.OutlineColor = Color.Black
        rtChart.RoundedCorners = False

        ' Graticule description properties
        rtChart.BackColor = Color.DarkCyan
        rtChart.AutoSize = False ' true forces square border, frame is always square

        rtChart.FrameOn = True
        rtChart.FrameTics = FrameTicsStyle.MajorMinorFrameTics
        rtChart.FrameColor = Color.White

        rtChart.AxesType = AxesType.HorizontalVertical
        rtChart.AxesTics = AxesTicsStyle.MajorMinorAxesTics
        rtChart.AxesColor = Color.White

        rtChart.GridOn = True
        rtChart.GridType = GridType.HorizontalVertical
        rtChart.GridColor = Color.LightGray

        rtChart.MajorDivHorz = MAJOR_HORZ_DIV ' major horizontal divisions
        rtChart.MajorDivVert = MAJOR_VERT_DIV ' major vertical divisions
        rtChart.MinorDiv = MINOR_DIVISIONS ' minor divisions
        rtChart.MarginLeft = 20 ' left Graticule border
        rtChart.MarginTop = 3 ' top Graticule border
        rtChart.MarginRight = 3 ' right Graticule border
        rtChart.MarginBottom = 25 ' bottom Graticule border

        rtChart.ColorDepth = ColorDepth.Color16 '  256 supported only by certain display cards

    End Sub

    Public Sub InitializeScrollChannels(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal numChannels As Integer, ByVal samplesPerChannel As Integer)

        ' Initialize Window and Channels for Scroll mode, line style, buffer length,
        ' single precision auto inc scalars

        ' Window Description
        rtChart.LogicalWindow = X_TIME_WINDOW ' select window
        rtChart.WndXmin = 0 ' left minimum abscissa
        rtChart.WndWidth = (SamplesPerDiv * MAJOR_HORZ_DIV) ' in Seconds (Time/div * 10 divisions)
        rtChart.WndHeight = (VoltsPerDiv * MAJOR_VERT_DIV) ' in Volts (Volts/div * 8 divisions)
        rtChart.WndYmin = (rtChart.WndHeight / -2) ' bottom minimum ordinate

        For channel As Integer = ChannelName.Channel1 To numChannels

            ' Describe Channel
            rtChart.LogicalChannel = channel ' select channel

            ' assign each channel to a different View port if View ports => than channels
            If rtChart.Viewports >= channel Then
                rtChart.ChnDspViewport = channel ' each channel in its own separate view port
            ElseIf channel = ChannelName.Channel4 And rtChart.Viewports = 2 Then
                rtChart.ChnDspViewport = 2 ' assign channel 4 to view port 2, if available
            Else
                rtChart.ChnDspViewport = 1 ' all channels assigned to view port 1
            End If

            rtChart.ChnDspWindow = X_TIME_WINDOW ' assign X-axis (time) window to channel
            rtChart.ChnDspPen = channel ' specify pen
            rtChart.ChnDspBufLen = samplesPerChannel ' total input buffer length
            rtChart.ChnDspMode = ChannelDisplayMode.ScrollRight ' per channel display mode, use RunMode constants
            rtChart.ChnDspStyle = ChannelDisplayStyle.Lines ' use point style for scroll plotting
            rtChart.ChnDspOffsetX = 0
            rtChart.ChnDspOffsetY = 0

            ' Describe horizontal dimension
            rtChart.ChannelDataDimension = LogicalDimension.Horizontal
            rtChart.ChnDataShape = ChannelDataShape.AutoIncr
            rtChart.ChnDataOffset = 0 ' offset into buffer (in points)
            rtChart.ChnDataIncr = 1 ' logical increment to next value in array

            ' Describe vertical dimension
            rtChart.ChannelDataDimension = LogicalDimension.Vertical
            rtChart.ChnDataShape = ChannelDataShape.Scalar
            rtChart.ChnDataType = ChannelDataType.Single
            rtChart.ChnDataOffset = 0 ' offset into buffer (in points)
            rtChart.ChnDataIncr = 1 ' logical increment to next value for 2D array

            rtChart.ChnDspAction = ChannelDisplayAction.Initialize

        Next channel

    End Sub

    Public Sub InitializeViewPorts(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl)

        ' initialize view ports for number, storage color and mode
        ' View port Description
        rtChart.Viewports = 4 ' number of view ports to be initialized
        rtChart.ViewportStorageColor = Color.Green ' storage mode plot color, same for all view ports

        ' initialize storage mode for each view port
        For channel As Integer = 1 To rtChart.Viewports
            rtChart.LogicalViewport = channel ' select view port
            rtChart.ViewportStorageOn = False ' disable storage mode
        Next channel
        rtChart.Viewports = 1 ' number of view ports displayed initially

    End Sub

    Public Sub InitializeXYChannel(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal channel As Integer, ByVal numSamples As Integer)

        ' Initialize Window and Channels for XY CRT mode, line style, buffer length,
        ' single precision scalars for vertical and horizontal dimensions

        ' Window Description
        rtChart.LogicalWindow = X_VOLTS_WINDOW ' select XY (volts) window
        rtChart.WndWidth = VoltsPerDiv * MAJOR_HORZ_DIV ' in Volts (5/div * 10 divisions) 50 volts
        rtChart.WndXmin = rtChart.WndWidth / -2 ' left minimum abscissa -25v
        rtChart.WndHeight = VoltsPerDiv * MAJOR_VERT_DIV ' in Volts (5/div * 8 divisions) 40 volts
        rtChart.WndYmin = rtChart.WndHeight / -2 ' bottom minimum ordinate -20v

        ' Describe Channel
        rtChart.LogicalChannel = channel
        rtChart.ChnDspWindow = X_VOLTS_WINDOW ' assign XY (volts) window to XY channel
        rtChart.ChnDspViewport = 1 ' use same View port as time plots
        rtChart.ChnDspPen = channel ' specify pen, uses same name as channel
        rtChart.ChnDspBufLen = numSamples ' input buffer length
        rtChart.ChnDspMode = ChannelDisplayMode.CRT ' per channel display mode, use RunMode constants
        rtChart.ChnDspStyle = ChannelDisplayStyle.Lines ' specify plotting style
        rtChart.ChnDspOffsetX = 0 ' set offset to 0
        rtChart.ChnDspOffsetY = 0 ' set offset to 0

        ' Describe horizontal dimension
        rtChart.ChannelDataDimension = LogicalDimension.Horizontal ' specify spatial dimension
        rtChart.ChnDataShape = ChannelDataShape.Scalar ' specify shape of data elements
        rtChart.ChnDataType = ChannelDataType.Single
        rtChart.ChnDataOffset = 0 ' offset into buffer (in points)
        rtChart.ChnDataIncr = 1 ' logical increment to next value

        ' Describe vertical dimension
        rtChart.ChannelDataDimension = LogicalDimension.Vertical ' specify spatial dimension
        rtChart.ChnDataShape = ChannelDataShape.Scalar ' specify shape of data elements
        rtChart.ChnDataType = ChannelDataType.Single
        rtChart.ChnDataOffset = 0 ' offset into buffer (in points)
        rtChart.ChnDataIncr = 1 ' logical increment to next value

        ' Initialize the channel
        rtChart.ChnDspAction = ChannelDisplayAction.Initialize

    End Sub

    Public Sub InitVerticalScale(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal scaleSelect As Integer, ByVal scaleViewport As Integer, ByVal scaleWindow As Integer)

        ' initialize vertical scale
        rtChart.LogicalScale = scaleSelect ' select scale name (same for both dimensions)
        rtChart.ScaleViewport = scaleViewport ' specify view port before selecting dimension
        rtChart.ScaleWindow = scaleWindow ' specify window to be scaled before selecting dimension
        rtChart.ScaleDimension = LogicalDimension.Vertical ' select horizontal or vertical scale/label

        rtChart.ScalePen = SCALE_PEN ' specify pen color for scale
        rtChart.ScaleLocation = ScaleLocation.LeftTop ' Scale Location: axis or frame
        rtChart.ScalePosition = Position.AboveLeft ' Scale Position: above or below ScaleLocation
        rtChart.ScaleOrientation = Orientation.Perpendicular ' Scale Orientation: Parallel or Perpendicular to ScaleDimension
        rtChart.ScalePath = Path.Right ' Scale Path: direction which characters follow: left. right, up, down
        rtChart.ScaleGap = 3 ' Scale Gap: space between ScaleLocation and scale/label in pixels
        rtChart.ScaleFactor = 1 ' multiplier used to convert window units to scale values
        rtChart.ScalePlaces = 0 ' number of significant digits to the right of decimal point

        ' update RtChart Label properties
        rtChart.LabelCaption = "Volts" ' specify Label Caption text: "volts", "secs" , etc.
        rtChart.LabelPen = SCALE_PEN ' specify pen color for Label

        rtChart.LabelLocation = LabelLocation.Center ' label location relative to scale: left, right, center
        rtChart.LabelPosition = Position.AboveLeft ' label Position: above, below, or on LabelLocation
        rtChart.LabelOrientation = Orientation.Perpendicular ' label character orientation relative to ScaleDimension
        rtChart.LabelPath = Path.Down ' Scale Path: direction which characters follow

        rtChart.ScaleVisibility = ScaleVisibility.ScaleAndLabel ' scale visibility: none, scale, label, or both
        rtChart.ScaleAction = ScaleAction.Initialize ' execute selected Scale Action

    End Sub

    Public Sub PlotData(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal lastChannel As Integer, ByVal nPoints As Integer, ByRef firstpoint As Integer, ByVal vbArray(,) As Single)

        ' RtChart exports ChartData function to plot data of selected channel
        rtChart.FramesPerSec = -1 ' synchronous mode forces paint event to occur for each channel

        For channel As Integer = ChannelName.Channel1 To lastChannel
            Result = rtChart.ChartData(channel, nPoints, vbArray, firstpoint + nPoints * (channel - 1), 0, 0)
            ' program control returns after data is plotted in synchronous mode
        Next channel

        rtChart.FramesPerSec = 0 ' asynchronous mode

        ' display returned status code converted to string
        If Result < 0 Then Windows.Forms.MessageBox.Show(ChartPen.ChartErrorMessage(Result), "Real Time Chart Data Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

    End Sub

    Public Sub PlotDataContinuous(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal lastChannel As Integer, ByVal nPoints As Integer, ByVal vbArray(,) As Single)

        ' this procedure simulates continuous CRT mode plotting
        ' the loop calls ChartData after new buffer is "ready"

        Dim firstpoint As Double
        rtChart.FramesPerSec = -1 ' synchronous mode forces paint event to occur for each channel

        Do While Program.Running

            ' get random starting point based on 2% of numSamples for demonstration
            firstpoint = VBMath.Rnd() * nPoints * 0.02
            For channel As Integer = ChannelName.Channel1 To lastChannel
                Result = rtChart.ChartData(channel, nPoints, vbArray, firstpoint + (channel - 1) * nPoints, 0, 0)
                ' program control returns after data is plotted in synchronous mode
            Next channel

            ' display returned status code converted to string
            If Result < 0 Then Windows.Forms.MessageBox.Show(ChartPen.ChartErrorMessage(Result), "Real Time Chart Data Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

            ' give control to Windows to process events (mouse, keyboard, etc.)
            Application.DoEvents()

        Loop

        rtChart.FramesPerSec = 0 ' asynchronous mode

    End Sub

    Public Sub PlotDataPoints(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal lastChannel As Integer, ByVal samples As Integer, ByVal vbArray(,) As Single)

        ' this procedure simulates continuous point mode plotting, passes a single point per ChartData call

        Dim nPoints As Integer = 1 ' plot nPoints on each ChartData call

        rtChart.FramesPerSec = -1 ' synchronous mode forces paint event to occur on each point

        For channel As Integer = ChannelName.Channel1 To lastChannel

            For i As Integer = 0 To samples - 1
                Result = rtChart.ChartData(channel, nPoints, vbArray, i + samples * (channel - 1), 0, 0)

                ' display returned status code converted to string
                If Result < 0 Then Windows.Forms.MessageBox.Show(ChartPen.ChartErrorMessage(Result), "Real Time Chart Data Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Next i

        Next channel

        rtChart.FramesPerSec = 0 ' 0 for asynchronous

    End Sub

    Public Sub PlotDataScroll(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal lastChannel As Integer, ByVal samples As Integer, ByRef nPoints As Integer, ByVal vbArray(,) As Single)

        ' this procedure simulates scroll mode plotting
        ' the for/next loop calls ChartData when new point is "ready"


        rtChart.FramesPerSec = 0 ' in asynchronous mode, the paint event is handled during DoEvents

        Do While Scrolling

            For i As Integer = 0 To samples - 1 Step nPoints

                For channel As Integer = ChannelName.Channel1 To lastChannel
                    ' plot nPoints on each ChartData call
                    Result = rtChart.ChartData(channel, nPoints, vbArray, i + samples * (channel - 1), 0, 0)
                    ' display returned status code converted to string
                    If Result < 0 Then Windows.Forms.MessageBox.Show(ChartPen.ChartErrorMessage(Result), "Real Time Chart Data Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Next channel

                Application.DoEvents() ' windows must have control to process paint, mouse, and keyboard events
                If Not Scrolling Then Exit For ' user clicked "STOP", break loop

            Next i

        Loop

    End Sub

    Public Sub PlotXYData(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal channel As Integer,
                          ByVal numPoints As Integer, ByRef vBArrayY(,) As Single, ByRef vbArrayX() As Single)

        ' data arrays are passed for both horizontal and vertical axis
        Result = rtChart.ChartData(ChannelName.XYChannel, numPoints, vBArrayY, numPoints * (channel - 1), vbArrayX, 0)

        ' display returned status code converted to string
        If Result < 0 Then Windows.Forms.MessageBox.Show(ChartPen.ChartErrorMessage(Result), "Real Time Chart Data Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)


    End Sub

    Public Sub ScreenToWindow(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal obj As Integer, ByRef x As Single, ByRef y As Single)

        ' Convert screen coordinates in twips to world coordinates (engineering units) for object
        rtChart.LogicalChannel = (obj) ' select channel object
        rtChart.LogicalWindow = rtChart.ChnDspWindow ' select channel's window
        rtChart.LogicalViewport = rtChart.ChnDspViewport ' select channel's view port

        ' WndX & WndY properties take twips and return RtChart Window units
        rtChart.WndX = x ' convert twips to window coordinates
        x = rtChart.WndX ' return world coordinates

        rtChart.WndY = y ' convert twips to window coordinates
        y = rtChart.WndY ' return world coordinates

    End Sub

    Public Sub SelectViewPorts(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal numChannels As Integer)

        ' Re-assigns View ports to data channels based on number of available view ports

        ' prevent each channel from being re-painted individually
        rtChart.ChartAction = ChartAction.DisablePaint

        For channel As Integer = ChannelName.Channel1 To numChannels

            ' select channel
            rtChart.LogicalChannel = channel

            ' assign each channel to a different View port if View ports => than channels
            If rtChart.Viewports >= channel Then
                rtChart.ChnDspViewport = channel ' each channel in its own separate view port
            ElseIf channel = ChannelName.Channel4 And rtChart.Viewports = 2 Then
                rtChart.ChnDspViewport = 2 ' assign channel 4 to view port 2, if available
            Else
                rtChart.ChnDspViewport = 1 ' all channels assigned to view port 1
            End If

            rtChart.ChnDspAction = ChannelDisplayAction.ChangeViewport

        Next channel

        ' paint all channels simultaneously for each view port
        rtChart.ChartAction = ChartAction.EnablePaint

    End Sub

    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function UpdateChannels(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal numChannels As Integer, ByVal samplesPerChannel As Integer) As Boolean

        Dim result2 As Boolean
        ' Update for change in size of buffer and number of channels
        ' must initialize for changes in buffer length or data transforms
        result2 = True ' "assume" Channel initialization was successful

        Try
            For channel As Integer = ChannelName.Channel1 To numChannels

                ' Window Description
                rtChart.LogicalWindow = X_TIME_WINDOW ' select X-axis time window
                rtChart.WndXmin = 0 ' left minimum abscissa
                rtChart.WndWidth = (SamplesPerDiv * MAJOR_HORZ_DIV) ' in Seconds (samples/div * 10 divisions) 100 * 10 = 1000
                rtChart.WndHeight = (VoltsPerDiv * MAJOR_VERT_DIV) ' in Volts (Volts/div * 8 divisions) 5 * 8 = 40
                rtChart.WndYmin = (rtChart.WndHeight / -2) ' bottom minimum ordinate (negative full scale) 40 / -2 = -20

                ' Describe Channel
                rtChart.LogicalChannel = channel ' select channel
                rtChart.ChnDspWindow = X_TIME_WINDOW ' assign X-axis (time) window to channel

                ' assign each channel to a different View port if View ports => than channels
                If rtChart.Viewports >= channel Then
                    rtChart.ChnDspViewport = channel ' each channel in its own separate view port
                ElseIf channel = ChannelName.Channel4 And rtChart.Viewports = 2 Then
                    rtChart.ChnDspViewport = 2 ' assign channel 4 to view port 2, if available
                Else
                    rtChart.ChnDspViewport = 1 ' all channels assigned to view port 1
                End If

                rtChart.ChnDspPen = channel ' specify pen, pens have same value as channel
                rtChart.ChnDspBufLen = samplesPerChannel ' input buffer length for each channel
                rtChart.ChnDspMode = ChannelDisplayMode.CRT ' per channel display mode, use RunMode constants
                rtChart.ChnDspStyle = ChannelDisplayStyle.Lines ' use line style for plotting arrays
                rtChart.ChnDspOffsetX = 0 ' set offset to 0
                rtChart.ChnDspOffsetY = 0 ' set offset to 0

                ' Describe horizontal dimension
                rtChart.ChannelDataDimension = LogicalDimension.Horizontal
                rtChart.ChnDataShape = ChannelDataShape.AutoIncr
                rtChart.ChnDataOffset = 0 ' offset into buffer (in points)
                rtChart.ChnDataIncr = 1 ' logical increment to next value in array

                ' Describe vertical dimension
                rtChart.ChannelDataDimension = LogicalDimension.Vertical
                rtChart.ChnDataShape = ChannelDataShape.Scalar
                rtChart.ChnDataType = ChannelDataType.Single
                rtChart.ChnDataOffset = 0 ' offset into buffer (in points)
                rtChart.ChnDataIncr = 1 ' logical increment to next value for 2D array
                rtChart.ChnDataXform = 0

                rtChart.ChnDspAction = ChannelDisplayAction.Initialize

            Next channel

        Catch
            System.Windows.Forms.MessageBox.Show("Channel Initialization Error #" & Conversion.Str(Information.Err().Number) & " " & Conversion.ErrorToString(), Application.ProductName) ' initialization not successful, return false
            Return False
        End Try

        Return result2
    End Function

    Public Sub UpdateCursors(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal color As Integer, ByVal window As Integer, ByVal viewport As Integer)

        ' Update for change in cursor channel window, view port assignments, and for pen color


        ' prevent each cursor from being re-painted individually on Initialize Action
        rtChart.ChartAction = ChartAction.DisablePaint

        For cursor As Integer = ChannelName.Cursor1 To ChannelName.Cursor4

            rtChart.LogicalChannel = (cursor) ' select cursor channel

            rtChart.ChnDspWindow = window ' specify logical window
            rtChart.ChnDspAction = ChannelDisplayAction.ChangeWindow

            rtChart.ChnDspViewport = (viewport) ' assign all cursors to the selected View port
            rtChart.ChnDspAction = ChannelDisplayAction.ChangeViewport

            rtChart.ChnDspPen = color ' specify drawing pen color
            rtChart.ChnDspAction = ChannelDisplayAction.ChangePen

        Next cursor

        ' paint entire window with new positions for all cursors
        rtChart.ChartAction = ChartAction.EnablePaint

    End Sub

    Public Sub UpdateWindow_XY(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal windowSelect As Integer, ByVal unitsPerDivision As Single)

        ' update window with new dimensions, perform ReScale Action which may cause ReFresh

        ' Window Description
        rtChart.LogicalWindow = windowSelect ' select window

        rtChart.WndHeight = unitsPerDivision * rtChart.MajorDivVert ' in Volts (V/div * 8 divisions)
        rtChart.WndYmin = rtChart.WndHeight / -2 ' bottom minimum ordinate

        rtChart.WndWidth = unitsPerDivision * rtChart.MajorDivHorz ' in Volts (V/div * 10 divisions)
        rtChart.WndXmin = rtChart.WndWidth / -2 ' left minimum ordinate

        rtChart.ChartAction = ChartAction.Rescale

    End Sub

    Public Sub UpdateWindowHeight(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal windowSelect As Integer, ByVal unitsPerDivision As Single)

        ' update window with new dimensions, perform ReScale Action which may cause ReFresh

        ' Window Description
        rtChart.LogicalWindow = windowSelect ' select window
        rtChart.WndHeight = unitsPerDivision * rtChart.MajorDivVert ' in Volts (assume V/div * 8 divisions)
        rtChart.WndYmin = rtChart.WndHeight / -2 ' bottom minimum ordinate

        rtChart.ChartAction = ChartAction.Rescale

    End Sub

    Public Sub UpdateWindowWidth(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal windowSelect As Integer, ByVal unitsPerDivision As Single)

        ' update window with new dimensions, perform ReScale Action which may cause ReFresh

        ' Window Description
        rtChart.LogicalWindow = windowSelect ' select window
        rtChart.WndWidth = unitsPerDivision * rtChart.MajorDivHorz ' window width in number of samples
        rtChart.WndXmin = 0 ' left minimum abscissa

        rtChart.ChartAction = ChartAction.Rescale

    End Sub

    Public Sub ZeroChannelOffsets(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal firstChannel As Integer, ByVal lastChannel As Integer)

        ' reset any channel offsets

        ' prevent each channel from being re-painted individually on Translate Action
        rtChart.ChartAction = ChartAction.DisablePaint

        For channel As Integer = firstChannel To lastChannel

            ' Describe Channel
            rtChart.LogicalChannel = channel ' select channel
            rtChart.ChnDspOffsetY = 0 ' zero offset
            rtChart.ChnDspOffsetX = 0 ' zero offset

            rtChart.ChnDspAction = ChannelDisplayAction.Translate ' update channel position

        Next channel

        ' paint entire window with new channel positions for all channels
        rtChart.ChartAction = ChartAction.EnablePaint

    End Sub
End Module
