Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic
Imports System
Imports System.ComponentModel
Imports System.Windows.Forms

Imports isr.Visuals.RealTimeChart
Partial Friend Class ScalesForm
	Inherits System.Windows.Forms.Form


	' LabOBJX Real-Time Chart - Example Code
	' (C) Copyright 1995, Scientific Software Tools, Inc.
	' All Rights Reserved.
	'

	Dim BevelStyleArray() As String

	Dim FrameMargin As Integer
	Dim DefaultSize_Renamed As Integer
	Dim LastRtChartWidth As Integer
	Dim LastRtChartHeight As Integer

	' size value after border, margin, and outlines are added
	'Const DefaultRtChartHeight = 2784  ' margin = 5
	'Const DefaultRtChartWidth = 3384

	Const DefaultRtChartHeight As Integer = 3456 ' margin = 30
	Const DefaultRtChartWidth As Integer = 4056

	' temporary variables for selected values
	Dim SelectedScale As Integer
	Dim SelectedScaleAction As Integer
	Dim SelectedScaleDimension As Integer
	Dim SelectedScaleFactor As Single
	Dim SelectedScaleGap As Integer
	Dim SelectedScaleLocation As Integer
	Dim SelectedScaleOrientation As Integer
	Dim SelectedScalePen As Integer
	Dim SelectedScalePlaces As Integer
	Dim SelectedScalePosition As Integer
	Dim SelectedScaleViewport As Integer
	Dim SelectedScaleVisibility As Integer
	Dim SelectedScaleWindow As Integer
	Dim SelectedScalePath As Integer

	Dim SelectedLabelCaption As String = String.Empty
	Dim SelectedLabelLocation As Integer
	Dim SelectedLabelPen As Integer
	Dim SelectedLabelPath As Integer
	Dim SelectedLabelOrientation As Integer
	Dim SelectedLabelPosition As Integer

	Private isInitializingComponent As Boolean
	Public Sub New()
		MyBase.New()
		If _Instance Is Nothing Then
			If _InitializingInstance Then
				_Instance = Me
			Else
				Try
					'For the start-up form, the first instance created is the default instance.
					If Not (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint Is Nothing) AndAlso System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType Is Me.GetType() Then
						_Instance = Me
					End If

				Catch
				End Try
			End If
		End If
		'This call is required by the Windows Form Designer.
		isInitializingComponent = True
		InitializeComponent()
		isInitializingComponent = False
		ReLoadForm(False)
	End Sub


	Private Sub AutoSizeCheck_CheckStateChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles AutoSizeCheck.CheckStateChanged
       If isInitializingComponent Then Return

		' true forces square border, frame is always square
		ReflectionHelper.LetMember(RtChart1, "AutoSize", AutoSizeCheck.CheckState)

		' update RtChart height and width
		RtChartHeight.Text = ReflectionHelper.GetMember(RtChart1, "Height").ToString()
		RtChartWidth.Text = ReflectionHelper.GetMember(RtChart1, "Width").ToString()

	End Sub

	Private Sub AxesCheck_CheckStateChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles AxesCheck.CheckStateChanged
        If isInitializingComponent Then Return

		If AxesCheck.CheckState = CheckState.Checked Then
			'UPGRADE_WARNING: (1068) RtChart_HorzVertAxes of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
			RtChart1.AxesType = AxesType.HorizontalVertical
		Else : AxesCheck.CheckState = CheckState.Unchecked
			'UPGRADE_WARNING: (1068) RtChart_NoAxes of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
			RtChart1.AxesType = AxesType.NoAxes
		End If

	End Sub

	Private Sub BevelStyleInnerComboBox_SelectedIndexChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles BevelStyleInnerComboBox.SelectedIndexChanged

		ReflectionHelper.LetMember(RtChart1, "BevelInner", BevelStyleInnerComboBox.SelectedIndex)

		' update RtChart height and width
		RtChartHeight.Text = ReflectionHelper.GetMember(RtChart1, "Height").ToString()
		RtChartWidth.Text = ReflectionHelper.GetMember(RtChart1, "Width").ToString()

	End Sub

	Private Sub BevelStyleOuterComboBox_SelectedIndexChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles BevelStyleOuterComboBox.SelectedIndexChanged

		ReflectionHelper.LetMember(RtChart1, "BevelOuter", BevelStyleOuterComboBox.SelectedIndex)

		' update RtChart height and width
		RtChartHeight.Text = ReflectionHelper.GetMember(RtChart1, "Height").ToString()
		RtChartWidth.Text = ReflectionHelper.GetMember(RtChart1, "Width").ToString()

	End Sub

	Private Sub BevelWidthInnerTextBox_Enter(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles BevelWidthInnerTextBox.Enter

		BevelWidthInnerTextBox.SelectionStart = 0
		BevelWidthInnerTextBox.SelectionLength = Strings.Len(BevelWidthInnerTextBox.Text)

	End Sub

	Private Sub BevelWidthInnerTextBox_KeyDown(ByVal eventSender As Object, ByVal eventArgs As KeyEventArgs) Handles BevelWidthInnerTextBox.KeyDown
		Dim KeyCode As Keys = eventArgs.KeyCode
		Dim Shift As Integer = eventArgs.KeyData / 65536
		Try

			If KeyCode = keys.Return Then
				TempVal = Conversion.Val(BevelWidthInnerTextBox.Text)
				If TempVal < 0 Or TempVal > 32767 Then Exit Sub

				ReflectionHelper.LetMember(RtChart1, "BevelWidth_Inner", TempVal)

				' update RtChart height and width
				RtChartHeight.Text = ReflectionHelper.GetMember(RtChart1, "Height").ToString()
				RtChartWidth.Text = ReflectionHelper.GetMember(RtChart1, "Width").ToString()
			End If
		Finally 
			eventArgs.Handled = KeyCode = 0
		End Try

	End Sub

	Private Sub BevelWidthInnerTextBox_Leave(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles BevelWidthInnerTextBox.Leave

		BevelWidthInnerTextBox_KeyDown(BevelWidthInnerTextBox, New KeyEventArgs(Keys.Return))

	End Sub

	Private Sub BevelWidthOuterTextBox_Enter(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles BevelWidthOuterTextBox.Enter

		BevelWidthOuterTextBox.SelectionStart = 0
		BevelWidthOuterTextBox.SelectionLength = Strings.Len(BevelWidthOuterTextBox.Text)

	End Sub

	Private Sub BevelWidthOuterTextBox_KeyDown(ByVal eventSender As Object, ByVal eventArgs As KeyEventArgs) Handles BevelWidthOuterTextBox.KeyDown
		Dim KeyCode As Keys = eventArgs.KeyCode
		Dim Shift As Integer = eventArgs.KeyData / 65536
		Try

			If KeyCode = keys.Return Then
				TempVal = Conversion.Val(BevelWidthOuterTextBox.Text)
				If TempVal < 0 Or TempVal > 32767 Then Exit Sub

				ReflectionHelper.LetMember(RtChart1, "BevelWidth_Outer", TempVal)

				' update RtChart height and width
				RtChartHeight.Text = ReflectionHelper.GetMember(RtChart1, "Height").ToString()
				RtChartWidth.Text = ReflectionHelper.GetMember(RtChart1, "Width").ToString()
			End If
		Finally 
			eventArgs.Handled = KeyCode = 0
		End Try

	End Sub

	Private Sub BevelWidthOuterTextBox_Leave(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles BevelWidthOuterTextBox.Leave

		BevelWidthOuterTextBox_KeyDown(BevelWidthOuterTextBox, New KeyEventArgs(Keys.Return))

	End Sub

	Private Sub BorderWidthTextBox_Enter(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles BorderWidthTextBox.Enter

		BorderWidthTextBox.SelectionStart = 0
		BorderWidthTextBox.SelectionLength = Strings.Len(BorderWidthTextBox.Text)

	End Sub

	Private Sub BorderWidthTextBox_KeyDown(ByVal eventSender As Object, ByVal eventArgs As KeyEventArgs) Handles BorderWidthTextBox.KeyDown
		Dim KeyCode As Keys = eventArgs.KeyCode
		Dim Shift As Integer = eventArgs.KeyData / 65536
		Try

			If KeyCode = keys.Return Then
				TempVal = Conversion.Val(BorderWidthTextBox.Text)
				If TempVal < 0 Or TempVal > 32767 Then Exit Sub

				ReflectionHelper.LetMember(RtChart1, "BorderWidth", TempVal)

				' update RtChart height and width
				RtChartHeight.Text = ReflectionHelper.GetMember(RtChart1, "Height").ToString()
				RtChartWidth.Text = ReflectionHelper.GetMember(RtChart1, "Width").ToString()
			End If
		Finally 
			eventArgs.Handled = KeyCode = 0
		End Try

	End Sub

	Private Sub BorderWidthTextBox_Leave(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles BorderWidthTextBox.Leave

		BorderWidthTextBox_KeyDown(BorderWidthTextBox, New KeyEventArgs(Keys.Return))

	End Sub

	Private Sub DefaultButton_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles DefaultButton.Click

		InitializeRtChart(RtChart1)

		InitFrontPanel(RtChart1)

	End Sub

	Private Sub Form_Load()
		Dim RtChart_Horz As Object

		Try

			InitPenColorArray()
			InitializePens(RtChart1)

			InitColorComboBoxes()

			InitBevelStyleArray()

			InitializeWindows(RtChart1)

			InitializeRtChart(RtChart1)

			InitFrontPanel(RtChart1)

			InitLabelGroup()

			InitScaleGroup()

			InitFontsGroup()

			InitializeScale_Click(InitializeScale, New EventArgs())

			InitVerticalScale(RtChart1)

			' sync variables with front panel
			SelectedScale = 1 ' select scale name (same for both dimensions)
			'UPGRADE_WARNING: (1068) RtChart_Horz of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
			ReflectionHelper.LetMember(RtChart1, "ScaleDimension", ReflectionHelper.GetPrimitiveValue(RtChart_Horz)) ' select horizontal or vertical scale/label

			Me.SetBounds((Screen.PrimaryScreen.Bounds.Width - Me.ClientRectangle.Width) / 2, (Screen.PrimaryScreen.Bounds.Height - Me.ClientRectangle.Height) / 2, 0, 0, BoundsSpecified.X Or BoundsSpecified.Y)

		Catch

			MessageBox.Show("Program Init Error: " & Conversion.ErrorToString(Information.Err().Number), Application.ProductName)
			'UPGRADE_TODO: (1065) Error handling statement (Resume Next) could not be converted. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1065
			UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("Resume Next Statement")
		End Try

	End Sub

	Private Sub Form_Closed(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MyBase.Closed
		Environment.Exit(0)
	End Sub

	Private Sub FrameCheck_CheckStateChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles FrameCheck.CheckStateChanged
        If isInitializingComponent Then Return

		' set RtChart property
		RtChart1.FrameOn = (FrameCheck.CheckState)

	End Sub

	Private Sub GridCheck_CheckStateChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles GridCheck.CheckStateChanged
        If isInitializingComponent Then Return

		' set RtChart property
		RtChart1.GridOn = (GridCheck.CheckState)

	End Sub

	Private Sub InitBevelStyleArray()

		ReDim BevelStyleArray(3)

		BevelStyleArray(0) = "None"
		BevelStyleArray(1) = "Inset"
		BevelStyleArray(2) = "Raised"

		For i = 0 To 2
			BevelStyleInnerComboBox.AddItem(BevelStyleArray(i), i)
			BevelStyleOuterComboBox.AddItem(BevelStyleArray(i), i)
		Next i

		' select current style from list (default)
		BevelStyleInnerComboBox.SelectedIndex = 1
		BevelStyleOuterComboBox.SelectedIndex = 2

	End Sub

	Private Sub InitColorComboBoxes()

		' initialize each color combobox and set default color
		InitComboBox(ColorForm.DefInstance.AxesColorComboBox)
		InitComboBox(ColorForm.DefInstance.BackGroundColorComboBox)
		InitComboBox(ColorForm.DefInstance.BorderColorComboBox)
		InitComboBox(ColorForm.DefInstance.FrameColorComboBox)
		InitComboBox(ColorForm.DefInstance.GridColorComboBox)
		InitComboBox(ColorForm.DefInstance.LightColorComboBox)
		InitComboBox(ColorForm.DefInstance.OutlineColorComboBox)
		InitComboBox(ColorForm.DefInstance.ShadowColorComboBox)

	End Sub

	Private Sub InitComboBox(ByRef RTCComboBox As ComboBox)

		' build list of pen color names and specify list index for ordering

		For i As Integer = 1 To 20
			RTCComboBox.AddItem(PenColorArray(i).ItemName, PenColorArray(i).ListIndex)
			' the ItemData property is used to store RGB color values
			RTCComboBox.SetItemData(PenColorArray(i).ListIndex, PenColorArray(i).RGBValue)
		Next i

	End Sub

	Private Sub InitFontCheckBoxes()

		FontsForm.DefInstance.FontBoldCheck.CheckState = False
		ReflectionHelper.LetMember(RtChart1, "FontBold", False)

		FontsForm.DefInstance.FontItalicCheck.CheckState = False
		ReflectionHelper.LetMember(RtChart1, "FontItalic", False)

		FontsForm.DefInstance.FontStrikethruCheck.CheckState = False
		ReflectionHelper.LetMember(RtChart1, "FontStrikethru", False)

		FontsForm.DefInstance.FontUnderlineCheck.CheckState = False
		ReflectionHelper.LetMember(RtChart1, "FontUnderline", False)

	End Sub

	Private Sub InitFontNameList()

		'UPGRADE_ISSUE: (2070) Constant Screen was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2070
		'UPGRADE_ISSUE: (2064) Screen property Screen.FontCount was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2064
		Dim tempForEndVar As Integer = UpgradeSolution1Support.UpgradeStubs.VB_Global.getScreen().getFontCount() - 1
		For i = 0 To tempForEndVar
			'UPGRADE_ISSUE: (2070) Constant Screen was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2070
			'UPGRADE_ISSUE: (2064) Screen property Screen.Fonts was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2064
			FontsForm.DefInstance.FontNameList.AddItem(UpgradeSolution1Support.UpgradeStubs.VB_Global.getScreen().Fonts(i))
		Next i

		'FontsForm.FontNameList.ListIndex = 1
		FontsForm.DefInstance.FontNameList.Text = "Arial"
		ReflectionHelper.LetMember(RtChart1, "FontName", FontsForm.DefInstance.FontNameList.Text)

	End Sub

	Private Sub InitFontsGroup()

		InitFontNameList()
		InitFontSizeCombo()
		InitFontCheckBoxes()

	End Sub

	Private Sub InitFontSizeCombo()
		Dim RtChart_ScaleInitialize As Object

		FontsForm.DefInstance.FontSizeCombo.AddItem("4")
		FontsForm.DefInstance.FontSizeCombo.AddItem("6")
		FontsForm.DefInstance.FontSizeCombo.AddItem("7.8")
		FontsForm.DefInstance.FontSizeCombo.AddItem("8")
		FontsForm.DefInstance.FontSizeCombo.AddItem("9.6")
		FontsForm.DefInstance.FontSizeCombo.AddItem("10")
		FontsForm.DefInstance.FontSizeCombo.AddItem("12")
		FontsForm.DefInstance.FontSizeCombo.AddItem("13.8")
		FontsForm.DefInstance.FontSizeCombo.AddItem("14")
		FontsForm.DefInstance.FontSizeCombo.AddItem("16")
		FontsForm.DefInstance.FontSizeCombo.AddItem("18")
		FontsForm.DefInstance.FontSizeCombo.AddItem("20")
		FontsForm.DefInstance.FontSizeCombo.AddItem("22")
		FontsForm.DefInstance.FontSizeCombo.AddItem("24")

		FontsForm.DefInstance.FontSizeCombo.Text = "7.8"
		ReflectionHelper.LetMember(RtChart1, "FontSize", Conversion.Val(FontsForm.DefInstance.FontSizeCombo.Text))
		'UPGRADE_WARNING: (1068) RtChart_ScaleInitialize of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart1.ScaleAction = ((ScaleAction.ScaleInitialize))

	End Sub

	Private Sub InitFrontPanel(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl)

		' color group - select default color
		ColorForm.DefInstance.AxesColorComboBox.SelectedIndex = PenColorArray(WHITE_PEN).ListIndex
		ColorForm.DefInstance.BackGroundColorComboBox.SelectedIndex = PenColorArray(DARKCYAN_PEN).ListIndex
		ColorForm.DefInstance.BorderColorComboBox.SelectedIndex = PenColorArray(LIGHTGRAY_PEN).ListIndex
		ColorForm.DefInstance.FrameColorComboBox.SelectedIndex = PenColorArray(WHITE_PEN).ListIndex
		ColorForm.DefInstance.GridColorComboBox.SelectedIndex = PenColorArray(CYAN_PEN).ListIndex
		ColorForm.DefInstance.LightColorComboBox.SelectedIndex = PenColorArray(DARKGRAY_PEN).ListIndex
		ColorForm.DefInstance.OutlineColorComboBox.SelectedIndex = PenColorArray(BLACK_PEN).ListIndex
		ColorForm.DefInstance.ShadowColorComboBox.SelectedIndex = PenColorArray(BLACK_PEN).ListIndex

		' bevel group
		BevelStyleInnerComboBox.SelectedIndex = ReflectionHelper.GetMember(Of Integer)(RtChart, "BevelInner")
		BevelStyleOuterComboBox.SelectedIndex = ReflectionHelper.GetMember(Of Integer)(RtChart, "BevelOuter")
		BevelWidthOuterTextBox.Text = ReflectionHelper.GetMember(RtChart, "BevelWidth_Outer").ToString()
		BevelWidthInnerTextBox.Text = ReflectionHelper.GetMember(RtChart, "BevelWidth_Inner").ToString()

		' border group
		BorderWidthTextBox.Text = ReflectionHelper.GetMember(RtChart, "BorderWidth").ToString()
		AutoSizeCheck.CheckState = Math.Abs(ReflectionHelper.GetMember(Of Double)(RtChart1, "AutoSize"))
		OutlineCheck.CheckState = Math.Abs(ReflectionHelper.GetMember(Of Double)(RtChart, "Outline"))
		RoundCornersCheck.CheckState = ReflectionHelper.GetMember(Of CheckState)(RtChart, "RoundedCorners")

		FrameMargin = 30
		ReflectionHelper.LetMember(RtChart1, "MarginRight", FrameMargin)
		ReflectionHelper.LetMember(RtChart1, "MarginLeft", FrameMargin)
		ReflectionHelper.LetMember(RtChart1, "MarginBottom", FrameMargin)
		ReflectionHelper.LetMember(RtChart1, "MarginTop", FrameMargin)

		' margin group
		MarginRightTextBox.Text = ReflectionHelper.GetMember(RtChart1, "MarginRight").ToString()
		MarginLeftTextBox.Text = ReflectionHelper.GetMember(RtChart1, "MarginLeft").ToString()
		MarginBottomTextBox.Text = ReflectionHelper.GetMember(RtChart1, "MarginBottom").ToString()
		MarginTopTextBox.Text = ReflectionHelper.GetMember(RtChart1, "MarginTop").ToString()

		' divisions group
		MajorDivHorzTextBox.Text = ReflectionHelper.GetMember(RtChart1, "MajorDivHorz").ToString()
		MajorDivVertTextBox.Text = ReflectionHelper.GetMember(RtChart1, "MajorDivVert").ToString()
		MinorDivTextBox.Text = ReflectionHelper.GetMember(RtChart1, "MinorDiv").ToString()

		' RtChart size group
		RtChartHeight.Text = ReflectionHelper.GetMember(RtChart1, "Height").ToString()
		RtChartWidth.Text = ReflectionHelper.GetMember(RtChart1, "Width").ToString()
		LastRtChartHeight = ReflectionHelper.GetMember(Of Integer)(RtChart1, "Height")
		LastRtChartWidth = ReflectionHelper.GetMember(Of Integer)(RtChart1, "Width")
		DefaultSize_Renamed = True

		' Graticule group
		FrameCheck.CheckState = CheckState.Checked
		AxesCheck.CheckState = CheckState.Checked
		GridCheck.CheckState = CheckState.Checked

		' viewport group
		Viewports1.Checked = True

	End Sub

	Private Sub InitializeRtChart(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl)
		Dim RtChart_Color16, RtChart_HorzVertAxes, RtChart_HorzVertGrid, RtChart_Inset, RtChart_MajorMinorAxesTics, RtChart_MajorMinorFrameTics, RtChart_Raised As Object

		' set auto size to false to resize chart
		RtChart.AutoSize = ( False)
		ReflectionHelper.LetMember(RtChart, "Height", DefaultRtChartHeight) ' VB standard property
		ReflectionHelper.LetMember(RtChart, "Width", DefaultRtChartWidth) ' VB standard property

		' Global operation properties
		RtChart.HitTest = False ' enables mouse pointer "hit" detection
		RtChart.FramesPerSec = 0 ' 0 for asynchronous, -1 for synchronous

		' View port Description
		RtChart.Viewports = 1 ' number of view ports displayed initially
		RtChart.ViewportStorageColor  = Color.Green ' storage mode plot color, same for all view ports
		RtChart.LogicalViewport = 1 ' select view port
		RtChart.ViewportStorageOn = False ' disable storage mode

		' Border description properties
		RtChart.BorderColor = Color.LightGray
		RtChart.LightColor = Color.DarkGray
		RtChart.ShadowColor = Color.Black
		RtChart.OutlineColor = Color.Black
		RtChart.Outline = True
		RtChart.RoundedCorners = False

		'UPGRADE_WARNING: (1068) RtChart_Inset of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.BevelInner = BevelStyle.Inset
		'UPGRADE_WARNING: (1068) RtChart_Raised of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.BevelOuter = BevelStyle.Raised
		RtChart.BevelWidthInner = ( 3)
		RtChart.BevelWidthOuter = ( 3)
		RtChart.BorderWidth = 3

		' Graticule description properties
		RtChart.BackColor = Color.DarkCyan ' background color

		RtChart.FrameOn = True
		'UPGRADE_WARNING: (1068) RtChart_MajorMinorFrameTics of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.FrameTics = FrameTicsStyle.MajorMinorFrameTics
		RtChart.FrameColor = Color.White

		'UPGRADE_WARNING: (1068) RtChart_HorzVertAxes of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.AxesType  = AxesType.HorizontalVertical
		'UPGRADE_WARNING: (1068) RtChart_MajorMinorAxesTics of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.AxesTics = AxesTicsStyle.MajorMinorAxesTics
		RtChart.AxesColor = Color.White

		RtChart.GridOn = True
		'UPGRADE_WARNING: (1068) RtChart_HorzVertGrid of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.GridType = GridType.HorizontalVertical
		RtChart.GridColor = Drawing.Color.FromArgb(ChartPen.CYAN)

		ReflectionHelper.LetMember(RtChart, "MajorDivHorz", 10) ' major horizontal divisions
		RtChart.MajorDivVert = (8) ' major vertical divisions
		RtChart.MinorDiv = 5 ' minor divisions

		RtChart.MarginTop = 2 ' top Graticule border
		RtChart.MarginBottom = 2 ' bottom Graticule border
		RtChart.MarginLeft = 2 ' left Graticule border
		RtChart.MarginRight = 2 ' right Graticule border

		'UPGRADE_WARNING: (1068) RtChart_Color16 of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.ColorDepth = ColorDepth.Color16 ' 16 color mode

	End Sub

	Private Sub InitializeScale_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles InitializeScale.Click
		Dim RtChart_ScaleInitialize As Integer

		ScaleActionListBox.SelectedIndex = RtChart_ScaleInitialize
		ScaleActionListBox_SelectedIndexChanged(ScaleActionListBox, New EventArgs())

	End Sub

	Private Sub InitializeWindows(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl)

		' Window Description
		RtChart.LogicalWindow = 1 ' select X-axis time window
		RtChart.WndWidth = (10) ' in Seconds
		RtChart.WndXmin = 0 ' left minimum abscissa
		RtChart.WndHeight = (40) ' in Volts
		RtChart.WndYmin = (RtChart.WndHeight/ -2) ' bottom minimum ordinate (negative full scale) 40 / -2 = -20

		' Window Description
		RtChart.LogicalWindow = 2 ' select X-axis degrees window
		RtChart.WndWidth = (180) ' in Seconds
		RtChart.WndXmin = (-90) ' left minimum abscissa
		RtChart.WndHeight = (10) ' in Volts
		ReflectionHelper.LetMember(RtChart, "WndYmin", 0) ' bottom minimum ordinate

		' Window Description
		RtChart.LogicalWindow = 3 ' select X-axis window
		RtChart.WndWidth = (1000) ' in Seconds
		RtChart.WndXmin = (100) ' left minimum abscissa
		RtChart.WndHeight = (100) ' in Volts
		RtChart.WndYmin = (RtChart.WndHeight/ -2) ' bottom minimum ordinate (negative full scale) 40 / -2 = -20

		' Window Description
		RtChart.LogicalWindow = 4 ' select X-axis  window
		RtChart.WndWidth = 3 ' in minutes
		RtChart.WndXmin = 0 ' left minimum abscissa
		RtChart.WndHeight = 1 ' unitless, ratio
		ReflectionHelper.LetMember(RtChart, "WndYmin", 0) ' bottom minimum ordinate

	End Sub

	Private Sub InitLabelGroup()
		Dim RtChart_AboveLeft As Object
		Dim RtChart_BelowRight, RtChart_Center As Integer
		Dim RtChart_DownPath, RtChart_LeftBottom, RtChart_LeftPath, RtChart_OnScale As Object
		Dim RtChart_Parallel As Integer
		Dim RtChart_Perpendicular As Object
		Dim RtChart_RightPath As Integer
		Dim RtChart_RightTop, RtChart_UpPath As Object

		' All list item selections ("ListIndex =...") cause click events to occur.
		' Combo and list box click events update temporary storage variables
		' used in UpdateScaleProps sub to write selected values to RtChart Scale properties.

		' specify Label Caption items, more may be added at run-time
		LabelCaptionComboBox.AddItem("Volts") ' add items to list
		LabelCaptionComboBox.AddItem("Seconds")
		LabelCaptionComboBox.AddItem("Temperature")
		LabelCaptionComboBox.AddItem("Minutes")
		LabelCaptionComboBox.AddItem("Torque")
		LabelCaptionComboBox.AddItem("Horse Power")
		LabelCaptionComboBox.AddItem("Stress")
		LabelCaptionComboBox.AddItem("Micro Strain")
		LabelCaptionComboBox.AddItem("Amps")
		LabelCaptionComboBox.AddItem("Watts")
		LabelCaptionComboBox.AddItem("Power Factor")
		LabelCaptionComboBox.AddItem("Phase Shift")
		LabelCaptionComboBox.SelectedIndex = 1 ' select second item from list

		' initialize Pen list
		For i = BLACK_PEN To DARKMAGENTA_PEN
			LabelPenListBox.AddItem(PenColorArray(i).ItemName, PenColorArray(i).ListIndex)
			' the ItemData property is used to the pen name value
			LabelPenListBox.SetItemData(PenColorArray(i).ListIndex, i)
		Next i
		LabelPenListBox.SelectedIndex = PenColorArray(WHITE_PEN).ListIndex ' select Pen

		' initialize Location list
		LabelLocationListBox.AddItem("Left/Bottom", ReflectionHelper.GetPrimitiveValue(Of Integer)(RtChart_LeftBottom))
		LabelLocationListBox.AddItem("Center", RtChart_Center)
		LabelLocationListBox.AddItem("Right/Top", ReflectionHelper.GetPrimitiveValue(Of Integer)(RtChart_RightTop))
		LabelLocationListBox.SelectedIndex = RtChart_Center ' select Location

		' initialize Orientation list
		LabelOrientationListBox.AddItem("Parallel", RtChart_Parallel)
		LabelOrientationListBox.AddItem("Perpendicular", ReflectionHelper.GetPrimitiveValue(Of Integer)(RtChart_Perpendicular))
		LabelOrientationListBox.SelectedIndex = RtChart_Parallel ' select Orientation

		' initialize Position list
		LabelPositionListBox.AddItem("Below", RtChart_BelowRight)
		LabelPositionListBox.AddItem("Above", ReflectionHelper.GetPrimitiveValue(Of Integer)(RtChart_AboveLeft))
		LabelPositionListBox.AddItem("On Scale", ReflectionHelper.GetPrimitiveValue(Of Integer)(RtChart_OnScale))
		LabelPositionListBox.SelectedIndex = RtChart_BelowRight ' select Position

		' initialize Path list
		LabelPathListBox.AddItem("Right", RtChart_RightPath)
		LabelPathListBox.AddItem("Up", ReflectionHelper.GetPrimitiveValue(Of Integer)(RtChart_UpPath))
		LabelPathListBox.AddItem("Left", ReflectionHelper.GetPrimitiveValue(Of Integer)(RtChart_LeftPath))
		LabelPathListBox.AddItem("Down", ReflectionHelper.GetPrimitiveValue(Of Integer)(RtChart_DownPath))
		LabelPathListBox.SelectedIndex = RtChart_RightPath ' select Path

	End Sub

	Private Sub InitScaleComboItems()
		Dim RtChart_AboveLeft, RtChart_Axis, RtChart_BelowRight, RtChart_DownPath, RtChart_FrameLeftTop, RtChart_FrameRightBottom, RtChart_Hidden, RtChart_Horz, RtChart_LabelOnly, RtChart_LeftPath, RtChart_Parallel, RtChart_Perpendicular As Object
		Dim RtChart_RightPath As Integer
		Dim RtChart_ScaleAndLabel, RtChart_ScaleOnly, RtChart_UpPath, RtChart_Vert As Object

		' initialize Scale Select array
		InitScaleSelectArray()

		' initialize Action item array
		InitActionArray()

		' initialize Select list
		For i = 0 To 11
			ScaleSelectComboBox.AddItem(ScaleNames(i).ItemName, ScaleNames(i).ListIndex)
			' the ItemData property is used to store the value associated with the name
			ScaleSelectComboBox.SetItemData(ScaleNames(i).ListIndex, ScaleNames(i).ScaleSelectValue)
		Next i

		' initialize Scale Action list
		For i = 0 To 7
			ScaleActionListBox.AddItem(ScaleAction(i), i)
		Next i

		' initialize Scale Viewport combo
		ScaleViewportComboBox.AddItem("0")
		ScaleViewportComboBox.AddItem("1")
		ScaleViewportComboBox.AddItem("2")
		ScaleViewportComboBox.AddItem("3")
		ScaleViewportComboBox.AddItem("4")

		' initialize Scale Window combo
		ScaleWindowComboBox.AddItem("0")
		ScaleWindowComboBox.AddItem("1")
		ScaleWindowComboBox.AddItem("2")
		ScaleWindowComboBox.AddItem("3")
		ScaleWindowComboBox.AddItem("4")

		' initialize Scale Pen list
		For i = BLACK_PEN To DARKMAGENTA_PEN
			ScalePenListBox.AddItem(PenColorArray(i).ItemName, PenColorArray(i).ListIndex)
			' the ItemData property is used to the pen name value
			ScalePenListBox.SetItemData(PenColorArray(i).ListIndex, i)
		Next i

		' initialize Location list
		ScaleLocationListBox.AddItem("Axis", ReflectionHelper.GetPrimitiveValue(Of Integer)(RtChart_Axis))
		ScaleLocationListBox.AddItem("Frame Left/Top", ReflectionHelper.GetPrimitiveValue(Of Integer)(RtChart_FrameLeftTop))
		ScaleLocationListBox.AddItem("Frame Right/Bottom", ReflectionHelper.GetPrimitiveValue(Of Integer)(RtChart_FrameRightBottom))

		' initialize Dimension list
		ScaleDimensionListBox.AddItem("Horizontal", ReflectionHelper.GetPrimitiveValue(Of Integer)(RtChart_Horz))
		ScaleDimensionListBox.AddItem("Vertical", ReflectionHelper.GetPrimitiveValue(Of Integer)(RtChart_Vert))

		' initialize Orientation list
		ScaleOrientationListBox.AddItem("Parallel", ReflectionHelper.GetPrimitiveValue(Of Integer)(RtChart_Parallel))
		ScaleOrientationListBox.AddItem("Perpendicular", ReflectionHelper.GetPrimitiveValue(Of Integer)(RtChart_Perpendicular))

		' initialize Position list
		ScalePositionListBox.AddItem("Below", ReflectionHelper.GetPrimitiveValue(Of Integer)(RtChart_BelowRight))
		ScalePositionListBox.AddItem("Above", ReflectionHelper.GetPrimitiveValue(Of Integer)(RtChart_AboveLeft))

		' initialize Visibility list
		ScaleVisibilityListBox.AddItem("Hidden", ReflectionHelper.GetPrimitiveValue(Of Integer)(RtChart_Hidden))
		ScaleVisibilityListBox.AddItem("ScaleOnly", ReflectionHelper.GetPrimitiveValue(Of Integer)(RtChart_ScaleOnly))
		ScaleVisibilityListBox.AddItem("LabelOnly", ReflectionHelper.GetPrimitiveValue(Of Integer)(RtChart_LabelOnly))
		ScaleVisibilityListBox.AddItem("ScaleAndLabel", ReflectionHelper.GetPrimitiveValue(Of Integer)(RtChart_ScaleAndLabel))

		' initialize Factor combo
		ScaleFactorComboBox.AddItem("1")
		ScaleFactorComboBox.AddItem("2")
		ScaleFactorComboBox.AddItem("5")
		ScaleFactorComboBox.AddItem("10")
		ScaleFactorComboBox.AddItem("100")

		' initialize Gap combo
		ScaleGapComboBox.AddItem("0")
		ScaleGapComboBox.AddItem("1")
		ScaleGapComboBox.AddItem("2")
		ScaleGapComboBox.AddItem("5")
		ScaleGapComboBox.AddItem("10")

		' initialize Places combo
		ScalePlacesComboBox.AddItem("0")
		ScalePlacesComboBox.AddItem("1")
		ScalePlacesComboBox.AddItem("2")
		ScalePlacesComboBox.AddItem("3")

		' initialize Path list
		ScalePathListBox.AddItem("Right", RtChart_RightPath)
		ScalePathListBox.AddItem("Up", ReflectionHelper.GetPrimitiveValue(Of Integer)(RtChart_UpPath))
		ScalePathListBox.AddItem("Left", ReflectionHelper.GetPrimitiveValue(Of Integer)(RtChart_LeftPath))
		ScalePathListBox.AddItem("Down", ReflectionHelper.GetPrimitiveValue(Of Integer)(RtChart_DownPath))
		ScalePathListBox.SelectedIndex = RtChart_RightPath ' select Path

	End Sub

	Private Sub InitScaleGroup()

		' initialize combo item lists
		InitScaleComboItems()

		' select combo items from list
		SelectScaleListItems()

	End Sub

	Private Sub InitScaleSelectArray()

		ReDim ScaleNames(12)

		For i = 0 To 11
			ScaleNames(i).ItemName = "Scale" & (i + 1).ToString()
			ScaleNames(i).ListIndex = i
			ScaleNames(i).ScaleSelectValue = i + 1

		Next i

	End Sub

	Private Sub LabelCaptionComboBox_SelectionChangeCommitted(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles LabelCaptionComboBox.SelectionChangeCommitted

		SelectedLabelCaption = LabelCaptionComboBox.Text

	End Sub

	Private Sub LabelCaptionComboBox_KeyDown(ByVal eventSender As Object, ByVal eventArgs As KeyEventArgs) Handles LabelCaptionComboBox.KeyDown
		Dim KeyCode As Keys = eventArgs.KeyCode
		Dim Shift As Integer = eventArgs.KeyData / 65536
		Try

			If KeyCode = keys.Return Then
				TempString = LabelCaptionComboBox.Text
				If TempString = String.Empty Then Exit Sub

				LabelCaptionComboBox.AddItem(TempString)
				SelectedLabelCaption = TempString
			End If
		Finally 
			eventArgs.Handled = KeyCode = 0
		End Try

	End Sub

	Private Sub LabelCaptionComboBox_Leave(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles LabelCaptionComboBox.Leave

		TempString = LabelCaptionComboBox.Text
		If TempString = String.Empty Then Exit Sub

		SelectedLabelCaption = TempString

	End Sub

	Private Sub LabelLocationListBox_SelectedIndexChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles LabelLocationListBox.SelectedIndexChanged

		SelectedLabelLocation = LabelLocationListBox.SelectedIndex

	End Sub

	Private Sub LabelOrientationListBox_SelectedIndexChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles LabelOrientationListBox.SelectedIndexChanged

		SelectedLabelOrientation = LabelOrientationListBox.SelectedIndex

	End Sub

	Private Sub LabelPathListBox_SelectedIndexChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles LabelPathListBox.SelectedIndexChanged

		SelectedLabelPath = LabelPathListBox.SelectedIndex

	End Sub

	Private Sub LabelPenListBox_SelectedIndexChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles LabelPenListBox.SelectedIndexChanged

		' the ItemData property stores the Pen name value which is indexed by the seleted list item
		SelectedLabelPen = LabelPenListBox.GetItemData(LabelPenListBox.SelectedIndex)

	End Sub

	Private Sub LabelPositionListBox_SelectedIndexChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles LabelPositionListBox.SelectedIndexChanged

		SelectedLabelPosition = LabelPositionListBox.SelectedIndex

	End Sub

	Private Sub MajorDivHorzTextBox_Enter(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MajorDivHorzTextBox.Enter

		MajorDivHorzTextBox.SelectionStart = 0
		MajorDivHorzTextBox.SelectionLength = Strings.Len(MajorDivHorzTextBox.Text)

	End Sub

	Private Sub MajorDivHorzTextBox_KeyDown(ByVal eventSender As Object, ByVal eventArgs As KeyEventArgs) Handles MajorDivHorzTextBox.KeyDown
		Dim KeyCode As Keys = eventArgs.KeyCode
		Dim Shift As Integer = eventArgs.KeyData / 65536
		Try

			If KeyCode = keys.Return Then
				TempVal = Conversion.Val(MajorDivHorzTextBox.Text) ' major horizontal divisions

				If TempVal < 0 Then Exit Sub

				' number of horizontal grid lines
				ReflectionHelper.LetMember(RtChart1, "MajorDivHorz", TempVal)

				' update RtChart height and width
				RtChartHeight.Text = ReflectionHelper.GetMember(RtChart1, "Height").ToString()
				RtChartWidth.Text = ReflectionHelper.GetMember(RtChart1, "Width").ToString()
			End If
		Finally 
			eventArgs.Handled = KeyCode = 0
		End Try

	End Sub

	Private Sub MajorDivHorzTextBox_Leave(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MajorDivHorzTextBox.Leave

		MajorDivHorzTextBox_KeyDown(MajorDivHorzTextBox, New KeyEventArgs(Keys.Return))

	End Sub

	Private Sub MajorDivVertTextBox_Enter(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MajorDivVertTextBox.Enter

		MajorDivVertTextBox.SelectionStart = 0
		MajorDivVertTextBox.SelectionLength = Strings.Len(MajorDivVertTextBox.Text)

	End Sub

	Private Sub MajorDivVertTextBox_KeyDown(ByVal eventSender As Object, ByVal eventArgs As KeyEventArgs) Handles MajorDivVertTextBox.KeyDown
		Dim KeyCode As Keys = eventArgs.KeyCode
		Dim Shift As Integer = eventArgs.KeyData / 65536
		Try

			If KeyCode = keys.Return Then
				TempVal = Conversion.Val(MajorDivVertTextBox.Text) ' major vertical divisions

				If TempVal < 0 Then Exit Sub

				' number of vertical grid lines
				ReflectionHelper.LetMember(RtChart1, "MajorDivVert", TempVal)

				' update RtChart height and width
				RtChartHeight.Text = ReflectionHelper.GetMember(RtChart1, "Height").ToString()
				RtChartWidth.Text = ReflectionHelper.GetMember(RtChart1, "Width").ToString()
			End If
		Finally 
			eventArgs.Handled = KeyCode = 0
		End Try

	End Sub

	Private Sub MajorDivVertTextBox_Leave(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MajorDivVertTextBox.Leave

		MajorDivVertTextBox_KeyDown(MajorDivVertTextBox, New KeyEventArgs(Keys.Return))

	End Sub

	Private Sub MarginBottomTextBox_Enter(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MarginBottomTextBox.Enter

		MarginBottomTextBox.SelectionStart = 0
		MarginBottomTextBox.SelectionLength = Strings.Len(MarginBottomTextBox.Text)

	End Sub

	Private Sub MarginBottomTextBox_KeyDown(ByVal eventSender As Object, ByVal eventArgs As KeyEventArgs) Handles MarginBottomTextBox.KeyDown
		Dim KeyCode As Keys = eventArgs.KeyCode
		Dim Shift As Integer = eventArgs.KeyData / 65536
		Try

			If KeyCode = keys.Return Then
				TempVal = Conversion.Val(MarginBottomTextBox.Text)

				If TempVal < 0 Then Exit Sub

				' bottom Graticule border
				ReflectionHelper.LetMember(RtChart1, "MarginBottom", TempVal)

				' update RtChart height and width
				RtChartHeight.Text = ReflectionHelper.GetMember(RtChart1, "Height").ToString()
				RtChartWidth.Text = ReflectionHelper.GetMember(RtChart1, "Width").ToString()
			End If
		Finally 
			eventArgs.Handled = KeyCode = 0
		End Try

	End Sub

	Private Sub MarginBottomTextBox_Leave(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MarginBottomTextBox.Leave

		MarginBottomTextBox_KeyDown(MarginBottomTextBox, New KeyEventArgs(Keys.Return))

	End Sub

	Private Sub MarginLeftTextBox_Enter(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MarginLeftTextBox.Enter

		MarginLeftTextBox.SelectionStart = 0
		MarginLeftTextBox.SelectionLength = Strings.Len(MarginLeftTextBox.Text)

	End Sub

	Private Sub MarginLeftTextBox_KeyDown(ByVal eventSender As Object, ByVal eventArgs As KeyEventArgs) Handles MarginLeftTextBox.KeyDown
		Dim KeyCode As Keys = eventArgs.KeyCode
		Dim Shift As Integer = eventArgs.KeyData / 65536
		Try

			If KeyCode = keys.Return Then
				TempVal = Conversion.Val(MarginLeftTextBox.Text)

				If TempVal < 0 Then Exit Sub

				' left Graticule border
				ReflectionHelper.LetMember(RtChart1, "MarginLeft", TempVal)

				' update RtChart height and width
				RtChartHeight.Text = ReflectionHelper.GetMember(RtChart1, "Height").ToString()
				RtChartWidth.Text = ReflectionHelper.GetMember(RtChart1, "Width").ToString()
			End If
		Finally 
			eventArgs.Handled = KeyCode = 0
		End Try

	End Sub

	Private Sub MarginLeftTextBox_Leave(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MarginLeftTextBox.Leave

		MarginLeftTextBox_KeyDown(MarginLeftTextBox, New KeyEventArgs(Keys.Return))

	End Sub

	Private Sub MarginRightTextBox_Enter(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MarginRightTextBox.Enter

		MarginRightTextBox.SelectionStart = 0
		MarginRightTextBox.SelectionLength = Strings.Len(MarginRightTextBox.Text)

	End Sub

	Private Sub MarginRightTextBox_KeyDown(ByVal eventSender As Object, ByVal eventArgs As KeyEventArgs) Handles MarginRightTextBox.KeyDown
		Dim KeyCode As Keys = eventArgs.KeyCode
		Dim Shift As Integer = eventArgs.KeyData / 65536
		Try

			If KeyCode = keys.Return Then
				TempVal = Conversion.Val(MarginRightTextBox.Text)

				If TempVal < 0 Then Exit Sub

				' right Graticule border
				ReflectionHelper.LetMember(RtChart1, "MarginRight", TempVal)

				' update RtChart height and width
				RtChartHeight.Text = ReflectionHelper.GetMember(RtChart1, "Height").ToString()
				RtChartWidth.Text = ReflectionHelper.GetMember(RtChart1, "Width").ToString()
			End If
		Finally 
			eventArgs.Handled = KeyCode = 0
		End Try

	End Sub

	Private Sub MarginRightTextBox_Leave(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MarginRightTextBox.Leave

		MarginRightTextBox_KeyDown(MarginRightTextBox, New KeyEventArgs(Keys.Return))

	End Sub

	Private Sub MarginTopTextBox_Enter(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MarginTopTextBox.Enter

		MarginTopTextBox.SelectionStart = 0
		MarginTopTextBox.SelectionLength = Strings.Len(MarginTopTextBox.Text)

	End Sub

	Private Sub MarginTopTextBox_KeyDown(ByVal eventSender As Object, ByVal eventArgs As KeyEventArgs) Handles MarginTopTextBox.KeyDown
		Dim KeyCode As Keys = eventArgs.KeyCode
		Dim Shift As Integer = eventArgs.KeyData / 65536
		Try

			If KeyCode = keys.Return Then
				TempVal = Conversion.Val(MarginTopTextBox.Text)

				If TempVal < 0 Then Exit Sub

				' top Graticule border
				ReflectionHelper.LetMember(RtChart1, "MarginTop", TempVal)

				' update RtChart height and width
				RtChartHeight.Text = ReflectionHelper.GetMember(RtChart1, "Height").ToString()
				RtChartWidth.Text = ReflectionHelper.GetMember(RtChart1, "Width").ToString()
			End If
		Finally 
			eventArgs.Handled = KeyCode = 0
		End Try

	End Sub

	Private Sub MarginTopTextBox_Leave(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MarginTopTextBox.Leave

		MarginTopTextBox_KeyDown(MarginTopTextBox, New KeyEventArgs(Keys.Return))

	End Sub

	Public Sub MenuItem_Exit_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MenuItem_Exit.Click

		Form_Closed(Me, New CancelEventArgs())

	End Sub

	Public Sub MenuItem_SelectColors_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MenuItem_SelectColors.Click

		ColorForm.DefInstance.ShowDialog()

	End Sub

	Public Sub MenuItem_SelectFonts_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MenuItem_SelectFonts.Click

		RtChart1.LogicalScale = ( SelectedScale) ' select scale name (same for both dimensions)
		ReflectionHelper.LetMember(RtChart1, "ScaleDimension", SelectedScaleDimension) ' select horizontal or vertical scale/label

		FontsForm.DefInstance.FontBoldCheck.CheckState = Math.Abs(ReflectionHelper.GetMember(Of Double)(RtChart1, "FontBold"))

		FontsForm.DefInstance.FontItalicCheck.CheckState = Math.Abs(ReflectionHelper.GetMember(Of Double)(RtChart1, "FontItalic"))

		FontsForm.DefInstance.FontStrikethruCheck.CheckState = Math.Abs(ReflectionHelper.GetMember(Of Double)(RtChart1, "FontStrikethru"))

		FontsForm.DefInstance.FontUnderlineCheck.CheckState = Math.Abs(ReflectionHelper.GetMember(Of Double)(RtChart1, "FontUnderline"))

		FontsForm.DefInstance.FontNameList.Text = ReflectionHelper.GetMember(Of String)(RtChart1, "FontName")

		FontsForm.DefInstance.FontSizeCombo.Text = ReflectionHelper.GetMember(RtChart1, "FontSize").ToString()

		FontsForm.DefInstance.ShowDialog()

	End Sub

	Private Sub MinorDivTextBox_Enter(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MinorDivTextBox.Enter

		MinorDivTextBox.SelectionStart = 0
		MinorDivTextBox.SelectionLength = Strings.Len(MinorDivTextBox.Text)

	End Sub

	Private Sub MinorDivTextBox_KeyDown(ByVal eventSender As Object, ByVal eventArgs As KeyEventArgs) Handles MinorDivTextBox.KeyDown
		Dim KeyCode As Keys = eventArgs.KeyCode
		Dim Shift As Integer = eventArgs.KeyData / 65536
		Try

			If KeyCode = keys.Return Then
				TempVal = Conversion.Val(MinorDivTextBox.Text) ' minor divisions

				If TempVal < 0 Then Exit Sub

				ReflectionHelper.LetMember(RtChart1, "MinorDiv", TempVal)
			End If
		Finally 
			eventArgs.Handled = KeyCode = 0
		End Try

	End Sub

	Private Sub MinorDivTextBox_Leave(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MinorDivTextBox.Leave

		MinorDivTextBox_KeyDown(MinorDivTextBox, New KeyEventArgs(Keys.Return))

	End Sub

	Private Sub OutlineCheck_CheckStateChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles OutlineCheck.CheckStateChanged
        If isInitializingComponent Then Return

		ReflectionHelper.LetMember(RtChart1, "Outline", OutlineCheck.CheckState)

	End Sub

	'UPGRADE_NOTE: (7001) The following declaration (OutlineColorComboBox_Click) seems to be dead code More Information: https://www.mobilize.net/vbtonet/ewis/ewi7001
	'Private Sub OutlineColorComboBox_Click()
		'
	'End Sub

	Private Sub RoundCornersCheck_CheckStateChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles RoundCornersCheck.CheckStateChanged
        If isInitializingComponent Then Return

		ReflectionHelper.LetMember(RtChart1, "RoundedCorners", RoundCornersCheck.CheckState)

		' update RtChart height and width
		RtChartHeight.Text = ReflectionHelper.GetMember(RtChart1, "Height").ToString()
		RtChartWidth.Text = ReflectionHelper.GetMember(RtChart1, "Width").ToString()

	End Sub

	'UPGRADE_NOTE: (7001) The following declaration (RtChart1_DblClick) seems to be dead code More Information: https://www.mobilize.net/vbtonet/ewis/ewi7001
	'Private Sub RtChart1_DblClick()
		'Dim RtChart_DisablePaint, RtChart_EnablePaint, RtChart_ScaleInitialize As Object
		'
		''UPGRADE_WARNING: (1068) RtChart_DisablePaint of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		'RtChart1.ChartAction =  ChartAction.DisablePaint
		'
		'If Not DefaultSize_Renamed Then
			'ReflectionHelper.LetMember(RtChart1, "Height", DefaultRtChartHeight)
			'ReflectionHelper.LetMember(RtChart1, "Width", DefaultRtChartWidth)
			'
			'RtChartHeight.Text = ReflectionHelper.GetMember(RtChart1, "Height").ToString()
			'RtChartWidth.Text = ReflectionHelper.GetMember(RtChart1, "Width").ToString()
			'
			'DefaultSize_Renamed = True
		'Else
			'ReflectionHelper.LetMember(RtChart1, "Height", LastRtChartHeight)
			'ReflectionHelper.LetMember(RtChart1, "Width", LastRtChartWidth)
			'
			'RtChartHeight.Text = ReflectionHelper.GetMember(RtChart1, "Height").ToString()
			'RtChartWidth.Text = ReflectionHelper.GetMember(RtChart1, "Width").ToString()
			'
			'DefaultSize_Renamed = False
		'End If
		'
		''UPGRADE_WARNING: (1068) RtChart_ScaleInitialize of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		'RtChart1.ScaleAction = ((ScaleAction.ScaleInitialize))
		''UPGRADE_WARNING: (1068) RtChart_EnablePaint of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		'RtChart1.ChartAction =  ChartAction.EnablePaint
		'
	'End Sub

	Private Sub RtChartHeight_Enter(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles RtChartHeight.Enter

		RtChartHeight.SelectionStart = 0
		RtChartHeight.SelectionLength = Strings.Len(RtChartHeight.Text)

	End Sub

	Private Sub RtChartHeight_KeyDown(ByVal eventSender As Object, ByVal eventArgs As KeyEventArgs) Handles RtChartHeight.KeyDown
		Dim KeyCode As Keys = eventArgs.KeyCode
		Dim Shift As Integer = eventArgs.KeyData / 65536
		Try
			Dim RtChart_DisablePaint, RtChart_EnablePaint, RtChart_ScaleInitialize As Object

			If KeyCode = keys.Return Then
				TempVal = Conversion.Val(RtChartHeight.Text)
				If TempVal < 0 Or TempVal > 32767 Then Exit Sub
				LastRtChartHeight = TempVal
					RtChart1.ChartAction =  ChartAction.DisablePaint
				ReflectionHelper.LetMember(RtChart1, "Height", TempVal)
				If ReflectionHelper.GetMember(Of Double)(RtChart1, "Height") <> DefaultRtChartHeight Then DefaultSize_Renamed = False
				' execute selected Scale Action
				'UPGRADE_WARNING: (1068) RtChart_ScaleInitialize of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
				RtChart1.ScaleAction = ((ScaleAction.ScaleInitialize))
						RtChart1.ChartAction =  ChartAction.EnablePaint
			End If
		Finally 
			eventArgs.Handled = KeyCode = 0
		End Try

	End Sub

	Private Sub RtChartHeight_Leave(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles RtChartHeight.Leave

		RtChartHeight_KeyDown(RtChartHeight, New KeyEventArgs(Keys.Return))

	End Sub

	Private Sub RtChartWidth_Enter(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles RtChartWidth.Enter

		RtChartWidth.SelectionStart = 0
		RtChartWidth.SelectionLength = Strings.Len(RtChartWidth.Text)

	End Sub

	Private Sub RtChartWidth_KeyDown(ByVal eventSender As Object, ByVal eventArgs As KeyEventArgs) Handles RtChartWidth.KeyDown
		Dim KeyCode As Keys = eventArgs.KeyCode
		Dim Shift As Integer = eventArgs.KeyData / 65536
		Try
			Dim RtChart_DisablePaint, RtChart_EnablePaint, RtChart_ScaleInitialize As Object

			If KeyCode = keys.Return Then
				TempVal = Conversion.Val(RtChartWidth.Text)
				If TempVal < 0 Or TempVal > 32767 Then Exit Sub
				LastRtChartWidth = TempVal
					RtChart1.ChartAction =  ChartAction.DisablePaint
				ReflectionHelper.LetMember(RtChart1, "Width", TempVal)
				If ReflectionHelper.GetMember(Of Double)(RtChart1, "Width") <> DefaultRtChartWidth Then DefaultSize_Renamed = False
				' execute selected Scale Action
				'UPGRADE_WARNING: (1068) RtChart_ScaleInitialize of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
				RtChart1.ScaleAction = ((ScaleAction.ScaleInitialize))
						RtChart1.ChartAction =  ChartAction.EnablePaint
			End If
		Finally 
			eventArgs.Handled = KeyCode = 0
		End Try

	End Sub

	Private Sub RtChartWidth_Leave(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles RtChartWidth.Leave

		RtChartWidth_KeyDown(RtChartWidth, New KeyEventArgs(Keys.Return))

	End Sub

	Private Sub ScaleActionListBox_SelectedIndexChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles ScaleActionListBox.SelectedIndexChanged

		Try

			UpdateScaleProps()

			' execute selected Scale Action
			SelectedScaleAction = ScaleActionListBox.SelectedIndex
			ReflectionHelper.LetMember(RtChart1, "ScaleAction", SelectedScaleAction)

		Catch

			MessageBox.Show("ScaleAction Error: " & Conversion.ErrorToString(Information.Err().Number), Application.ProductName)
			'UPGRADE_TODO: (1065) Error handling statement (Resume Next) could not be converted. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1065
			UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("Resume Next Statement")
		End Try

	End Sub

	Private Sub ScaleDimensionListBox_SelectedIndexChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles ScaleDimensionListBox.SelectedIndexChanged

		SelectedScaleDimension = ScaleDimensionListBox.SelectedIndex

		UpdateScaleGroup()

	End Sub

	Private Sub ScaleFactorComboBox_SelectionChangeCommitted(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles ScaleFactorComboBox.SelectionChangeCommitted

		SelectedScaleFactor = Conversion.Val(ScaleFactorComboBox.Text)

	End Sub

	Private Sub ScaleFactorComboBox_KeyDown(ByVal eventSender As Object, ByVal eventArgs As KeyEventArgs) Handles ScaleFactorComboBox.KeyDown
		Dim KeyCode As Keys = eventArgs.KeyCode
		Dim Shift As Integer = eventArgs.KeyData / 65536
		Try

			If KeyCode = keys.Return Then
				TempVal = Conversion.Val(ScaleFactorComboBox.Text)
				If TempVal < 0 Then Exit Sub

				ScaleFactorComboBox.AddItem(TempVal.ToString())

				SelectedScaleFactor = TempVal
			End If
		Finally 
			eventArgs.Handled = KeyCode = 0
		End Try

	End Sub

	Private Sub ScaleFactorComboBox_Leave(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles ScaleFactorComboBox.Leave

		TempVal = Conversion.Val(ScaleFactorComboBox.Text)
		If TempVal < 0 Then Exit Sub

		SelectedScaleFactor = TempVal

	End Sub

	Private Sub ScaleGapComboBox_SelectionChangeCommitted(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles ScaleGapComboBox.SelectionChangeCommitted

		SelectedScaleGap = Conversion.Val(ScaleGapComboBox.Text)

	End Sub

	Private Sub ScaleGapComboBox_KeyDown(ByVal eventSender As Object, ByVal eventArgs As KeyEventArgs) Handles ScaleGapComboBox.KeyDown
		Dim KeyCode As Keys = eventArgs.KeyCode
		Dim Shift As Integer = eventArgs.KeyData / 65536
		Try

			If KeyCode = keys.Return Then
				TempVal = Conversion.Val(ScaleGapComboBox.Text)
				If TempVal < 0 Then Exit Sub

				ScaleGapComboBox.AddItem(TempVal.ToString())

				SelectedScaleGap = TempVal
			End If
		Finally 
			eventArgs.Handled = KeyCode = 0
		End Try

	End Sub

	Private Sub ScaleGapComboBox_Leave(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles ScaleGapComboBox.Leave

		TempVal = Conversion.Val(ScaleGapComboBox.Text)
		If TempVal < 0 Then Exit Sub

		SelectedScaleGap = TempVal

	End Sub

	Private Sub ScaleLocationListBox_SelectedIndexChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles ScaleLocationListBox.SelectedIndexChanged

		SelectedScaleLocation = ScaleLocationListBox.SelectedIndex

	End Sub

	Private Sub ScaleOrientationListBox_SelectedIndexChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles ScaleOrientationListBox.SelectedIndexChanged

		SelectedScaleOrientation = ScaleOrientationListBox.SelectedIndex

	End Sub

	Private Sub ScalePathListBox_SelectedIndexChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles ScalePathListBox.SelectedIndexChanged

		SelectedScalePath = ScalePathListBox.SelectedIndex

	End Sub

	Private Sub ScalePenListBox_SelectedIndexChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles ScalePenListBox.SelectedIndexChanged

		' the ItemData property stores the Pen name value which is indexed by the seleted list item
		SelectedScalePen = ScalePenListBox.GetItemData(ScalePenListBox.SelectedIndex)

	End Sub

	Private Sub ScalePlacesComboBox_SelectionChangeCommitted(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles ScalePlacesComboBox.SelectionChangeCommitted

		SelectedScalePlaces = Conversion.Val(ScalePlacesComboBox.Text)

	End Sub

	Private Sub ScalePlacesComboBox_KeyDown(ByVal eventSender As Object, ByVal eventArgs As KeyEventArgs) Handles ScalePlacesComboBox.KeyDown
		Dim KeyCode As Keys = eventArgs.KeyCode
		Dim Shift As Integer = eventArgs.KeyData / 65536
		Try

			If KeyCode = keys.Return Then
				TempVal = Conversion.Val(ScalePlacesComboBox.Text)
				If TempVal < 0 Then Exit Sub

				ScalePlacesComboBox.AddItem(TempVal.ToString())

				SelectedScalePlaces = TempVal
			End If
		Finally 
			eventArgs.Handled = KeyCode = 0
		End Try

	End Sub

	Private Sub ScalePlacesComboBox_Leave(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles ScalePlacesComboBox.Leave

		TempVal = Conversion.Val(ScalePlacesComboBox.Text)
		If TempVal < 0 Then Exit Sub

		SelectedScalePlaces = TempVal

	End Sub

	Private Sub ScalePositionListBox_SelectedIndexChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles ScalePositionListBox.SelectedIndexChanged

		SelectedScalePosition = ScalePositionListBox.SelectedIndex

	End Sub

	Private Sub ScaleSelectComboBox_SelectionChangeCommitted(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles ScaleSelectComboBox.SelectionChangeCommitted

		TempVal = ScaleSelectComboBox.GetItemData(ScaleSelectComboBox.SelectedIndex)
		If TempVal < 0 Or TempVal > 32768 Then Exit Sub

		SelectedScale = TempVal

		UpdateScaleGroup()

	End Sub

	Private Sub ScaleViewportComboBox_SelectionChangeCommitted(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles ScaleViewportComboBox.SelectionChangeCommitted

		SelectedScaleViewport = Conversion.Val(ScaleViewportComboBox.Text)

	End Sub

	Private Sub ScaleViewportComboBox_KeyDown(ByVal eventSender As Object, ByVal eventArgs As KeyEventArgs) Handles ScaleViewportComboBox.KeyDown
		Dim KeyCode As Keys = eventArgs.KeyCode
		Dim Shift As Integer = eventArgs.KeyData / 65536
		Try

			If KeyCode = keys.Return Then
				TempVal = Conversion.Val(ScaleViewportComboBox.Text)
				If TempVal < 0 Then Exit Sub

				ScaleViewportComboBox.AddItem(TempVal.ToString())

				SelectedScaleViewport = TempVal
			End If
		Finally 
			eventArgs.Handled = KeyCode = 0
		End Try

	End Sub

	Private Sub ScaleViewportComboBox_Leave(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles ScaleViewportComboBox.Leave

		TempVal = Conversion.Val(ScaleViewportComboBox.Text)
		If TempVal < 0 Then Exit Sub

		SelectedScaleViewport = TempVal

	End Sub

	Private Sub ScaleVisibilityListBox_SelectedIndexChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles ScaleVisibilityListBox.SelectedIndexChanged

		SelectedScaleVisibility = ScaleVisibilityListBox.SelectedIndex

	End Sub

	Private Sub ScaleWindowComboBox_SelectionChangeCommitted(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles ScaleWindowComboBox.SelectionChangeCommitted

		SelectedScaleWindow = Conversion.Val(ScaleWindowComboBox.Text)

	End Sub

	Private Sub ScaleWindowComboBox_KeyDown(ByVal eventSender As Object, ByVal eventArgs As KeyEventArgs) Handles ScaleWindowComboBox.KeyDown
		Dim KeyCode As Keys = eventArgs.KeyCode
		Dim Shift As Integer = eventArgs.KeyData / 65536
		Try

			If KeyCode = keys.Return Then
				TempVal = Conversion.Val(ScaleWindowComboBox.Text)
				If TempVal < 0 Then Exit Sub

				ScaleWindowComboBox.AddItem(TempVal.ToString())

				SelectedScaleWindow = TempVal
			End If
		Finally 
			eventArgs.Handled = KeyCode = 0
		End Try

	End Sub

	Private Sub ScaleWindowComboBox_Leave(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles ScaleWindowComboBox.Leave

		TempVal = Conversion.Val(ScaleWindowComboBox.Text)
		If TempVal < 0 Then Exit Sub

		SelectedScaleWindow = TempVal

	End Sub

	Private Sub SelectScaleListItems()
		Dim RtChart_BelowRight, RtChart_FrameRightBottom, RtChart_Horz, RtChart_Parallel, RtChart_ScaleAndLabel As Integer


		SelectedScale = ScaleNames(0).ScaleSelectValue ' just init variable, init list item later
		SelectedScaleDimension = RtChart_Horz ' just init variable, init list item later

		' these statements cause combo box click events where global vars are updated
		ScaleViewportComboBox.SelectedIndex = 1 ' select Window 1
		ScaleWindowComboBox.SelectedIndex = 1 ' select view port 1
		ScalePenListBox.SelectedIndex = PenColorArray(WHITE_PEN).ListIndex

		ScaleLocationListBox.SelectedIndex = RtChart_FrameRightBottom
		ScaleOrientationListBox.SelectedIndex = RtChart_Parallel
		ScalePositionListBox.SelectedIndex = RtChart_BelowRight
		ScaleVisibilityListBox.SelectedIndex = RtChart_ScaleAndLabel
		ScaleFactorComboBox.SelectedIndex = 0
		ScaleGapComboBox.SelectedIndex = 1
		ScalePlacesComboBox.SelectedIndex = 0

		' write globals to RtChart properties
		UpdateScaleProps()

		' select first Scale name item in list, causes click event
		ScaleSelectComboBox.SelectedIndex = ScaleNames(0).ListIndex

	End Sub

	Private Sub UpdateScaleGroup()

		' called by ScaleSelectComboBox_Click when new scale is selected

		Try

			RtChart1.LogicalScale = ( SelectedScale)

			' must select scale dimension here (use current value)
			ReflectionHelper.LetMember(RtChart1, "ScaleDimension", SelectedScaleDimension)

			' must select item for initialization here
			ScaleDimensionListBox.SelectedIndex = ReflectionHelper.GetMember(Of Integer)(RtChart1, "ScaleDimension")

			ScaleViewportComboBox.Text = ReflectionHelper.GetMember(RtChart1, "ScaleViewport").ToString()
			ScaleViewportComboBox_SelectionChangeCommitted(ScaleViewportComboBox, New EventArgs())

			ScaleWindowComboBox.Text = ReflectionHelper.GetMember(RtChart1, "ScaleWindow").ToString()
			ScaleWindowComboBox_SelectionChangeCommitted(ScaleWindowComboBox, New EventArgs())

			ScalePenListBox.SelectedIndex = PenColorArray(ReflectionHelper.GetMember(Of Integer)(RtChart1, "ScalePen")).ListIndex
			ScalePenListBox_SelectedIndexChanged(ScalePenListBox, New EventArgs())

			ScaleLocationListBox.SelectedIndex = ReflectionHelper.GetMember(Of Integer)(RtChart1, "ScaleLocation")
			ScaleLocationListBox_SelectedIndexChanged(ScaleLocationListBox, New EventArgs())

			ScaleOrientationListBox.SelectedIndex = ReflectionHelper.GetMember(Of Integer)(RtChart1, "ScaleOrientation")
			ScaleOrientationListBox_SelectedIndexChanged(ScaleOrientationListBox, New EventArgs())

			ScalePositionListBox.SelectedIndex = ReflectionHelper.GetMember(Of Integer)(RtChart1, "ScalePosition")
			ScalePositionListBox_SelectedIndexChanged(ScalePositionListBox, New EventArgs())

			ScaleVisibilityListBox.SelectedIndex = ReflectionHelper.GetMember(Of Integer)(RtChart1, "ScaleVisibility")
			ScaleVisibilityListBox_SelectedIndexChanged(ScaleVisibilityListBox, New EventArgs())

			ScaleFactorComboBox.Text = ReflectionHelper.GetMember(RtChart1, "ScaleFactor").ToString()
			ScaleFactorComboBox_SelectionChangeCommitted(ScaleFactorComboBox, New EventArgs())

			ScaleGapComboBox.Text = ReflectionHelper.GetMember(RtChart1, "ScaleGap").ToString()
			ScaleGapComboBox_SelectionChangeCommitted(ScaleGapComboBox, New EventArgs())

			ScalePlacesComboBox.Text = ReflectionHelper.GetMember(RtChart1, "ScalePlaces").ToString()
			ScalePlacesComboBox_SelectionChangeCommitted(ScalePlacesComboBox, New EventArgs())

			ScalePathListBox.SelectedIndex = ReflectionHelper.GetMember(Of Integer)(RtChart1, "ScalePath")
			ScalePathListBox_SelectedIndexChanged(ScalePathListBox, New EventArgs())

		Catch

			MessageBox.Show("ComboBoxError: " & Conversion.ErrorToString(Information.Err().Number), Application.ProductName)
			'UPGRADE_TODO: (1065) Error handling statement (Resume Next) could not be converted. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1065
			UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("Resume Next Statement")
		End Try

	End Sub

	Private Sub UpdateScaleProps()

		Try

			' update RtChart Scale properties
			RtChart1.LogicalScale = ( SelectedScale) ' select scale name (same for both dimensions)
			ReflectionHelper.LetMember(RtChart1, "ScaleDimension", SelectedScaleDimension) ' select horizontal or vertical scale/label

			ReflectionHelper.LetMember(RtChart1, "ScaleLocation", SelectedScaleLocation) ' Scale Location: axis or frame
			ReflectionHelper.LetMember(RtChart1, "ScalePosition", SelectedScalePosition) ' Scale Position: above or below ScaleLocation

			ReflectionHelper.LetMember(RtChart1, "ScaleOrientation", SelectedScaleOrientation) ' Scale Orientation: Parallel or Perpendicular to ScaleDimension
			ReflectionHelper.LetMember(RtChart1, "ScalePath", SelectedScalePath) ' Scale Path: direction which characters follow: left. right, up, down
			ReflectionHelper.LetMember(RtChart1, "ScaleGap", SelectedScaleGap) ' Scale Gap: space between ScaleLocation and scale/label in pixels

			ReflectionHelper.LetMember(RtChart1, "ScaleFactor", SelectedScaleFactor) ' multiplier used to convert window units to scale values
			ReflectionHelper.LetMember(RtChart1, "ScalePlaces", SelectedScalePlaces) ' number of significant digits to the right of decimal point

			ReflectionHelper.LetMember(RtChart1, "ScaleViewport", SelectedScaleViewport) ' specify viewport
			ReflectionHelper.LetMember(RtChart1, "ScaleWindow", SelectedScaleWindow) ' specify window to be scaled
			ReflectionHelper.LetMember(RtChart1, "ScalePen", SelectedScalePen) ' specify pen color for scale

			ReflectionHelper.LetMember(RtChart1, "ScaleVisibility", SelectedScaleVisibility) ' scale visibility: none, scale, label, or both

			' update RtChart Label properties
			ReflectionHelper.LetMember(RtChart1, "LabelCaption", SelectedLabelCaption) ' specify Label Caption text: "volts", "secs" , etc.
			ReflectionHelper.LetMember(RtChart1, "LabelPen", SelectedLabelPen) ' specify pen color for Label

			ReflectionHelper.LetMember(RtChart1, "LabelLocation", SelectedLabelLocation) ' label location relative to scale: left, right, center
			ReflectionHelper.LetMember(RtChart1, "LabelPosition", SelectedLabelPosition) ' label Position: above, below, or on LabelLocation
			ReflectionHelper.LetMember(RtChart1, "LabelOrientation", SelectedLabelOrientation) ' label character orientation relative to ScaleDimension
			ReflectionHelper.LetMember(RtChart1, "LabelPath", SelectedLabelPath) ' Scale Path: direction which characters follow

		Catch

			MessageBox.Show("RTC Property Error: " & Conversion.ErrorToString(Information.Err().Number), Application.ProductName)
			'UPGRADE_TODO: (1065) Error handling statement (Resume Next) could not be converted. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1065
			UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("Resume Next Statement")
		End Try

	End Sub

	Private Sub Viewports1_CheckedChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles Viewports1.CheckedChanged
		If eventSender.Checked Then
			If isInitializingComponent Then
				Exit Sub
			End If

			' number of viewports
			ReflectionHelper.LetMember(RtChart1, "Viewports", 1)

		End If
	End Sub

	Private Sub Viewports2_CheckedChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles Viewports2.CheckedChanged
		If eventSender.Checked Then
			If isInitializingComponent Then
				Exit Sub
			End If

			' number of viewports
			ReflectionHelper.LetMember(RtChart1, "Viewports", 2)

		End If
	End Sub

	Private Sub Viewports4_CheckedChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles Viewports4.CheckedChanged
		If eventSender.Checked Then
			If isInitializingComponent Then
				Exit Sub
			End If

			' number of viewports
			ReflectionHelper.LetMember(RtChart1, "Viewports", 4)

		End If
	End Sub
End Class