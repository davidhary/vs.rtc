<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FontsForm
#Region "Upgrade Support "
	Private Shared _Instance As FontsForm
	Private Shared _InitializingInstance As Boolean
	Public Shared Property DefInstance() As FontsForm
		Get
			If _Instance Is Nothing OrElse _Instance.IsDisposed Then
				_InitializingInstance = True
				_Instance = CreateInstance()
				_InitializingInstance = False
			End If
			Return _Instance
		End Get
		Set(ByVal value As FontsForm)
			_Instance = Value
		End Set
	End Property
#End Region
#Region "Windows Form Designer generated code "
	Public Shared Function CreateInstance() As FontsForm
		Dim theInstance As New FontsForm()
		Return theInstance
	End Function
	Private visualControls() As String = New String() {"components", "ToolTipMain", "CloseFontsForm", "FontSizeCombo", "FontNameList", "FontUnderlineCheck", "FontStrikethruCheck", "FontItalicCheck", "FontBoldCheck", "Label2", "Label1", "Frame1", "Label3"}
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTipMain As System.Windows.Forms.ToolTip
	Public WithEvents CloseFontsForm As System.Windows.Forms.Button
	Public WithEvents FontSizeCombo As System.Windows.Forms.ComboBox
	Public WithEvents FontNameList As System.Windows.Forms.ComboBox
	Public WithEvents FontUnderlineCheck As System.Windows.Forms.CheckBox
	Public WithEvents FontStrikethruCheck As System.Windows.Forms.CheckBox
	Public WithEvents FontItalicCheck As System.Windows.Forms.CheckBox
	Public WithEvents FontBoldCheck As System.Windows.Forms.CheckBox
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents Frame1 As System.Windows.Forms.GroupBox
	Public WithEvents Label3 As System.Windows.Forms.Label
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> _
	 Private Sub InitializeComponent()
		Me.components = New System.ComponentModel.Container()
		Dim resources As New System.ComponentModel.ComponentResourceManager(GetType(FontsForm))
		Me.ToolTipMain = New System.Windows.Forms.ToolTip(Me.components)
		Me.Frame1 = New System.Windows.Forms.GroupBox()
		Me.CloseFontsForm = New System.Windows.Forms.Button()
		Me.FontSizeCombo = New System.Windows.Forms.ComboBox()
		Me.FontNameList = New System.Windows.Forms.ComboBox()
		Me.FontUnderlineCheck = New System.Windows.Forms.CheckBox()
		Me.FontStrikethruCheck = New System.Windows.Forms.CheckBox()
		Me.FontItalicCheck = New System.Windows.Forms.CheckBox()
		Me.FontBoldCheck = New System.Windows.Forms.CheckBox()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.Label3 = New System.Windows.Forms.Label()
		Me.Frame1.SuspendLayout()
		Me.SuspendLayout()
		' 
		'Frame1
		' 
		Me.Frame1.AllowDrop = True
		Me.Frame1.BackColor = System.Drawing.SystemColors.Window
		Me.Frame1.Controls.Add(Me.CloseFontsForm)
		Me.Frame1.Controls.Add(Me.FontSizeCombo)
		Me.Frame1.Controls.Add(Me.FontNameList)
		Me.Frame1.Controls.Add(Me.FontUnderlineCheck)
		Me.Frame1.Controls.Add(Me.FontStrikethruCheck)
		Me.Frame1.Controls.Add(Me.FontItalicCheck)
		Me.Frame1.Controls.Add(Me.FontBoldCheck)
		Me.Frame1.Controls.Add(Me.Label2)
		Me.Frame1.Controls.Add(Me.Label1)
		Me.Frame1.Enabled = True
		Me.Frame1.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Frame1.Location = New System.Drawing.Point(4, 44)
		Me.Frame1.Name = "Frame1"
		Me.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame1.Size = New System.Drawing.Size(289, 117)
		Me.Frame1.TabIndex = 0
		Me.Frame1.Text = "Fonts"
		Me.Frame1.Visible = True
		' 
		'CloseFontsForm
		' 
		Me.CloseFontsForm.AllowDrop = True
		Me.CloseFontsForm.BackColor = System.Drawing.SystemColors.Control
		Me.CloseFontsForm.ForeColor = System.Drawing.SystemColors.ControlText
		Me.CloseFontsForm.Location = New System.Drawing.Point(192, 72)
		Me.CloseFontsForm.Name = "CloseFontsForm"
		Me.CloseFontsForm.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.CloseFontsForm.Size = New System.Drawing.Size(85, 29)
		Me.CloseFontsForm.TabIndex = 9
		Me.CloseFontsForm.Text = "Close"
		Me.CloseFontsForm.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
		Me.CloseFontsForm.UseVisualStyleBackColor = False
		' 
		'FontSizeCombo
		' 
		Me.FontSizeCombo.AllowDrop = True
		Me.FontSizeCombo.BackColor = System.Drawing.SystemColors.Window
		Me.FontSizeCombo.CausesValidation = True
		Me.FontSizeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
		Me.FontSizeCombo.Enabled = True
		Me.FontSizeCombo.ForeColor = System.Drawing.SystemColors.WindowText
		Me.FontSizeCombo.IntegralHeight = True
		Me.FontSizeCombo.Location = New System.Drawing.Point(96, 80)
		Me.FontSizeCombo.Name = "FontSizeCombo"
		Me.FontSizeCombo.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.FontSizeCombo.Size = New System.Drawing.Size(81, 20)
		Me.FontSizeCombo.Sorted = False
		Me.FontSizeCombo.TabIndex = 6
		Me.FontSizeCombo.TabStop = True
		Me.FontSizeCombo.Text = "Combo2"
		Me.FontSizeCombo.Visible = True
		' 
		'FontNameList
		' 
		Me.FontNameList.AllowDrop = True
		Me.FontNameList.BackColor = System.Drawing.SystemColors.Window
		Me.FontNameList.CausesValidation = True
		Me.FontNameList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
		Me.FontNameList.Enabled = True
		Me.FontNameList.ForeColor = System.Drawing.SystemColors.WindowText
		Me.FontNameList.IntegralHeight = True
		Me.FontNameList.Location = New System.Drawing.Point(96, 36)
		Me.FontNameList.Name = "FontNameList"
		Me.FontNameList.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.FontNameList.Size = New System.Drawing.Size(181, 20)
		Me.FontNameList.Sorted = True
		Me.FontNameList.TabIndex = 5
		Me.FontNameList.TabStop = True
		Me.FontNameList.Text = "FontNameList"
		Me.FontNameList.Visible = True
		' 
		'FontUnderlineCheck
		' 
		Me.FontUnderlineCheck.AllowDrop = True
		Me.FontUnderlineCheck.Appearance = System.Windows.Forms.Appearance.Normal
		Me.FontUnderlineCheck.BackColor = System.Drawing.SystemColors.Window
		Me.FontUnderlineCheck.CausesValidation = True
		Me.FontUnderlineCheck.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.FontUnderlineCheck.CheckState = System.Windows.Forms.CheckState.Unchecked
		Me.FontUnderlineCheck.Enabled = True
		Me.FontUnderlineCheck.ForeColor = System.Drawing.SystemColors.WindowText
		Me.FontUnderlineCheck.Location = New System.Drawing.Point(8, 84)
		Me.FontUnderlineCheck.Name = "FontUnderlineCheck"
		Me.FontUnderlineCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.FontUnderlineCheck.Size = New System.Drawing.Size(77, 17)
		Me.FontUnderlineCheck.TabIndex = 4
		Me.FontUnderlineCheck.TabStop = True
		Me.FontUnderlineCheck.Text = "Underline"
		Me.FontUnderlineCheck.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.FontUnderlineCheck.Visible = True
		' 
		'FontStrikethruCheck
		' 
		Me.FontStrikethruCheck.AllowDrop = True
		Me.FontStrikethruCheck.Appearance = System.Windows.Forms.Appearance.Normal
		Me.FontStrikethruCheck.BackColor = System.Drawing.SystemColors.Window
		Me.FontStrikethruCheck.CausesValidation = True
		Me.FontStrikethruCheck.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.FontStrikethruCheck.CheckState = System.Windows.Forms.CheckState.Unchecked
		Me.FontStrikethruCheck.Enabled = True
		Me.FontStrikethruCheck.ForeColor = System.Drawing.SystemColors.WindowText
		Me.FontStrikethruCheck.Location = New System.Drawing.Point(8, 64)
		Me.FontStrikethruCheck.Name = "FontStrikethruCheck"
		Me.FontStrikethruCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.FontStrikethruCheck.Size = New System.Drawing.Size(53, 17)
		Me.FontStrikethruCheck.TabIndex = 3
		Me.FontStrikethruCheck.TabStop = True
		Me.FontStrikethruCheck.Text = "Strike"
		Me.FontStrikethruCheck.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.FontStrikethruCheck.Visible = True
		' 
		'FontItalicCheck
		' 
		Me.FontItalicCheck.AllowDrop = True
		Me.FontItalicCheck.Appearance = System.Windows.Forms.Appearance.Normal
		Me.FontItalicCheck.BackColor = System.Drawing.SystemColors.Window
		Me.FontItalicCheck.CausesValidation = True
		Me.FontItalicCheck.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.FontItalicCheck.CheckState = System.Windows.Forms.CheckState.Unchecked
		Me.FontItalicCheck.Enabled = True
		Me.FontItalicCheck.ForeColor = System.Drawing.SystemColors.WindowText
		Me.FontItalicCheck.Location = New System.Drawing.Point(8, 44)
		Me.FontItalicCheck.Name = "FontItalicCheck"
		Me.FontItalicCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.FontItalicCheck.Size = New System.Drawing.Size(49, 17)
		Me.FontItalicCheck.TabIndex = 2
		Me.FontItalicCheck.TabStop = True
		Me.FontItalicCheck.Text = "Italic"
		Me.FontItalicCheck.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.FontItalicCheck.Visible = True
		' 
		'FontBoldCheck
		' 
		Me.FontBoldCheck.AllowDrop = True
		Me.FontBoldCheck.Appearance = System.Windows.Forms.Appearance.Normal
		Me.FontBoldCheck.BackColor = System.Drawing.SystemColors.Window
		Me.FontBoldCheck.CausesValidation = True
		Me.FontBoldCheck.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.FontBoldCheck.CheckState = System.Windows.Forms.CheckState.Checked
		Me.FontBoldCheck.Enabled = True
		Me.FontBoldCheck.ForeColor = System.Drawing.SystemColors.WindowText
		Me.FontBoldCheck.Location = New System.Drawing.Point(8, 24)
		Me.FontBoldCheck.Name = "FontBoldCheck"
		Me.FontBoldCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.FontBoldCheck.Size = New System.Drawing.Size(49, 17)
		Me.FontBoldCheck.TabIndex = 1
		Me.FontBoldCheck.TabStop = True
		Me.FontBoldCheck.Text = "Bold"
		Me.FontBoldCheck.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.FontBoldCheck.Visible = True
		' 
		'Label2
		' 
		Me.Label2.AllowDrop = True
		Me.Label2.BackColor = System.Drawing.SystemColors.Window
		Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label2.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label2.Location = New System.Drawing.Point(96, 20)
		Me.Label2.Name = "Label2"
		Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label2.Size = New System.Drawing.Size(49, 13)
		Me.Label2.TabIndex = 8
		Me.Label2.Text = "Name"
		' 
		'Label1
		' 
		Me.Label1.AllowDrop = True
		Me.Label1.BackColor = System.Drawing.SystemColors.Window
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label1.Location = New System.Drawing.Point(96, 64)
		Me.Label1.Name = "Label1"
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.Size = New System.Drawing.Size(49, 13)
		Me.Label1.TabIndex = 7
		Me.Label1.Text = "Size"
		' 
		'Label3
		' 
		Me.Label3.AllowDrop = True
		Me.Label3.BackColor = System.Drawing.SystemColors.Window
		Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label3.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label3.Location = New System.Drawing.Point(8, 4)
		Me.Label3.Name = "Label3"
		Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label3.Size = New System.Drawing.Size(281, 37)
		Me.Label3.TabIndex = 10
		Me.Label3.Text = "To specify the font of the currently selected Scale and Dimension, choose the desired parameters then click Close to Initialize Scale with new values."
		' 
		'FontsForm
		' 
		Me.AllowDrop = True
		Me.AutoScaleDimensions = New System.Drawing.SizeF(7, 15)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Window
		Me.ClientSize = New System.Drawing.Size(297, 165)
		Me.ControlBox = False
		Me.Controls.Add(Me.Frame1)
		Me.Controls.Add(Me.Label3)
		Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.ForeColor = System.Drawing.SystemColors.WindowText
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
		Me.Location = New System.Drawing.Point(94, 138)
		Me.Location = New System.Drawing.Point(91, 116)
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.Name = "FontsForm"
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Size = New System.Drawing.Size(304, 190)
		Me.Text = "Select Fonts"
		Me.Frame1.ResumeLayout(False)
		Me.ResumeLayout(False)
	End Sub
	Sub ReLoadForm(ByVal addEvents As Boolean)
		If addEvents Then
			AddHandler MyBase.Closed, AddressOf Me.Form_Closed
		End If
	End Sub
#End Region
End Class