Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic
Imports System
Imports System.Windows.Forms

Imports isr.Visuals.RealTimeChart
Partial Friend Class FontsForm
	Inherits System.Windows.Forms.Form
	' LabOBJX Real-Time Chart - Example Code
	' (C) Copyright 1995, Scientific Software Tools, Inc.
	' All Rights Reserved.
	'
	Public Sub New()
		MyBase.New()
		If _Instance Is Nothing Then
			If _InitializingInstance Then
				_Instance = Me
			Else
				Try
					'For the start-up form, the first instance created is the default instance.
					If Not (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint Is Nothing) AndAlso System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType Is Me.GetType() Then
						_Instance = Me
					End If

				Catch
				End Try
			End If
		End If
		'This call is required by the Windows Form Designer.
		isInitializingComponent = True
		InitializeComponent()
		isInitializingComponent = False
		ReLoadForm(False)
	End Sub



	Private Sub CloseFontsForm_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles CloseFontsForm.Click
		Dim RtChart_ScaleInitialize As Object

		'UPGRADE_WARNING: (1068) RtChart_ScaleInitialize of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		ReflectionHelper.LetMember(ScalesForm.DefInstance.RtChart1, "ScaleAction", ReflectionHelper.GetPrimitiveValue(RtChart_ScaleInitialize))
		Me.Hide()

	End Sub

	Private isInitializingComponent As Boolean
	Private Sub FontBoldCheck_CheckStateChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles FontBoldCheck.CheckStateChanged
		ReflectionHelper.LetMember(ScalesForm.DefInstance.RtChart1, "FontBold", FontBoldCheck.CheckState)
		'ScalesForm.RtChart1.ScaleAction = RtChart_ScaleInitialize
	End Sub

	Private Sub FontItalicCheck_CheckStateChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles FontItalicCheck.CheckStateChanged
		ReflectionHelper.LetMember(ScalesForm.DefInstance.RtChart1, "FontItalic", FontItalicCheck.CheckState)
		'ScalesForm.RtChart1.ScaleAction = RtChart_ScaleInitialize
	End Sub

	Private Sub FontNameList_SelectionChangeCommitted(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles FontNameList.SelectionChangeCommitted

		ReflectionHelper.LetMember(ScalesForm.DefInstance.RtChart1, "FontName", Me.FontNameList.Text)
		'ScalesForm.RtChart1.ScaleAction = RtChart_ScaleInitialize

	End Sub

	Private Sub FontSizeCombo_SelectionChangeCommitted(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles FontSizeCombo.SelectionChangeCommitted

		ReflectionHelper.LetMember(ScalesForm.DefInstance.RtChart1, "FontSize", Conversion.Val(Me.FontSizeCombo.Text))
		'ScalesForm.RtChart1.ScaleAction = RtChart_ScaleInitialize

	End Sub

	Private Sub FontSizeCombo_KeyDown(ByVal eventSender As Object, ByVal eventArgs As KeyEventArgs) Handles FontSizeCombo.KeyDown
		Dim KeyCode As Keys = eventArgs.KeyCode
		Dim Shift As Integer = eventArgs.KeyData / 65536
		Try

			If KeyCode = keys.Return Then
				TempVal = Conversion.Val(FontSizeCombo.Text)
				If TempVal < 0 Then Exit Sub

				FontSizeCombo.AddItem(TempVal.ToString())

				ReflectionHelper.LetMember(ScalesForm.DefInstance.RtChart1, "FontSize", TempVal)
				'  ScalesForm.RtChart1.ScaleAction = RtChart_ScaleInitialize
			End If
		Finally 
			eventArgs.Handled = KeyCode = 0
		End Try

	End Sub

	Private Sub FontStrikethruCheck_CheckStateChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles FontStrikethruCheck.CheckStateChanged
        If isInitializingComponent Then Return

		ReflectionHelper.LetMember(ScalesForm.DefInstance.RtChart1, "FontStrikethru", FontStrikethruCheck.CheckState)
		'ScalesForm.RtChart1.ScaleAction = RtChart_ScaleInitialize

	End Sub

	Private Sub FontUnderlineCheck_CheckStateChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles FontUnderlineCheck.CheckStateChanged
		ReflectionHelper.LetMember(ScalesForm.DefInstance.RtChart1, "FontUnderline", FontUnderlineCheck.CheckState)
		'ScalesForm.RtChart1.ScaleAction = RtChart_ScaleInitialize
	End Sub

	Private Sub Form_Closed(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MyBase.Closed
	End Sub
End Class