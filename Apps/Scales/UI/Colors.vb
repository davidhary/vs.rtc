Option Strict Off
Option Explicit On
Imports System
Imports System.Windows.Forms

Imports isr.Visuals.RealTimeChart
Partial Friend Class ColorForm
	Inherits System.Windows.Forms.Form
	' LabOBJX Real-Time Chart - Example Code
	' (C) Copyright 1995, Scientific Software Tools, Inc.
	' All Rights Reserved.
	'
	Public Sub New()
		MyBase.New()
		If _Instance Is Nothing Then
			If _InitializingInstance Then
				_Instance = Me
			Else
				Try
					'For the start-up form, the first instance created is the default instance.
					If Not (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint Is Nothing) AndAlso System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType Is Me.GetType() Then
						_Instance = Me
					End If

				Catch
				End Try
			End If
		End If
		'This call is required by the Windows Form Designer.
		InitializeComponent()
		ReLoadForm(False)
	End Sub



	Private Sub AxesColorComboBox_SelectedIndexChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles AxesColorComboBox.SelectedIndexChanged

		' the ItemData property stores the RGB color value which is indexed by the seleted list item
		ReflectionHelper.LetMember(ScalesForm.DefInstance.RtChart1, "AxesColor", AxesColorComboBox.GetItemData(AxesColorComboBox.SelectedIndex))

	End Sub

	Private Sub BackGroundColorComboBox_SelectedIndexChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles BackGroundColorComboBox.SelectedIndexChanged

		' the ItemData property stores the RGB color value which is indexed by the seleted list item
		ReflectionHelper.LetMember(ScalesForm.DefInstance.RtChart1, "BackColor", BackGroundColorComboBox.GetItemData(BackGroundColorComboBox.SelectedIndex))

	End Sub

	Private Sub BorderColorComboBox_SelectedIndexChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles BorderColorComboBox.SelectedIndexChanged

		' the ItemData property stores the RGB color value which is indexed by the seleted list item
		ReflectionHelper.LetMember(ScalesForm.DefInstance.RtChart1, "BorderColor", BorderColorComboBox.GetItemData(BorderColorComboBox.SelectedIndex))

	End Sub

	Private Sub CloseColorForm_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles CloseColorForm.Click

		Me.Hide()

	End Sub

	Private Sub FrameColorComboBox_SelectedIndexChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles FrameColorComboBox.SelectedIndexChanged

		' the ItemData property stores the RGB color value which is indexed by the seleted list item
		ReflectionHelper.LetMember(ScalesForm.DefInstance.RtChart1, "FrameColor", FrameColorComboBox.GetItemData(FrameColorComboBox.SelectedIndex))

	End Sub

	Private Sub GridColorComboBox_SelectedIndexChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles GridColorComboBox.SelectedIndexChanged

		' the ItemData property stores the RGB color value which is indexed by the seleted list item
		ReflectionHelper.LetMember(ScalesForm.DefInstance.RtChart1, "GridColor", GridColorComboBox.GetItemData(GridColorComboBox.SelectedIndex))

	End Sub

	Private Sub LightColorComboBox_SelectedIndexChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles LightColorComboBox.SelectedIndexChanged

		' the ItemData property stores the RGB color value which is indexed by the seleted list item
		ReflectionHelper.LetMember(ScalesForm.DefInstance.RtChart1, "LightColor", LightColorComboBox.GetItemData(LightColorComboBox.SelectedIndex))

	End Sub

	Private Sub OutlineColorComboBox_SelectedIndexChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles OutlineColorComboBox.SelectedIndexChanged

		' the ItemData property stores the RGB color value which is indexed by the seleted list item
		ReflectionHelper.LetMember(ScalesForm.DefInstance.RtChart1, "OutlineColor", OutlineColorComboBox.GetItemData(OutlineColorComboBox.SelectedIndex))

	End Sub

	Private Sub ShadowColorComboBox_SelectedIndexChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles ShadowColorComboBox.SelectedIndexChanged

		' the ItemData property stores the RGB color value which is indexed by the seleted list item
		ReflectionHelper.LetMember(ScalesForm.DefInstance.RtChart1, "ShadowColor", ShadowColorComboBox.GetItemData(ShadowColorComboBox.SelectedIndex))

	End Sub
	Private Sub Form_Closed(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MyBase.Closed
	End Sub
End Class