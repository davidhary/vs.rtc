Option Strict Off
Option Explicit On
Imports System
Imports isr.Visuals.RealTimeChart
Module Program
    ' LabOBJX Real-Time Chart - Example Code
    ' (C) Copyright 1995, Scientific Software Tools, Inc.
    ' All Rights Reserved.
    '
    Public Const AssemblyTitle As String = "Real Time Strip Chart Scales Demo"
    Public Const AssemblyDescription As String = "Real Time Strip Chart Scales Demo"
    Public Const AssemblyProduct As String = "Visuals.Real.Time.Chart.Scales"


    Public i As Integer
    Public TempVal As Single
    Public TempString As String = String.Empty

    Public Structure ScaleNameArray
        Dim ItemName As String
        Dim ListIndex As Short
        Dim ScaleSelectValue As Short
        Public Shared Function CreateInstance() As ScaleNameArray
            Dim result As New ScaleNameArray()
            result.ItemName = String.Empty
            Return result
        End Function
    End Structure

    Public ScaleNames() As ScaleNameArray = Nothing
    Public ScaleAction() As String


    Sub InitActionArray()
        Dim RtChart_ScaleChangePen, RtChart_ScaleChangeViewport, RtChart_ScaleChangeWindow, RtChart_ScaleErase, RtChart_ScaleInitialize, RtChart_ScaleIntenseColor, RtChart_ScaleNoAction, RtChart_ScaleNormalColor As Byte

        ReDim ScaleAction(8)

        ' Channel display actions - ChnDspAction
        ScaleAction(RtChart_ScaleNoAction) = "No Action"
        ScaleAction(RtChart_ScaleInitialize) = "Initialize"
        ScaleAction(RtChart_ScaleErase) = "Erase"
        ScaleAction(RtChart_ScaleIntenseColor) = "Intense Color"
        ScaleAction(RtChart_ScaleNormalColor) = "Normal Color"
        ScaleAction(RtChart_ScaleChangePen) = "Change Pen"
        ScaleAction(RtChart_ScaleChangeViewport) = "Change Viewport"
        ScaleAction(RtChart_ScaleChangeWindow) = "Change Window"

    End Sub

    Sub InitHorizontalScale(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl)
        Dim RtChart_Below, RtChart_Center, RtChart_FrameRightBottom, RtChart_Horz, RtChart_Parallel, RtChart_RightPath, RtChart_ScaleAndLabel, RtChart_ScaleInitialize As Object

        ' initialize horizontal scale
        rtChart.LogicalScale = 1 ' select scale name (same for both dimensions)
        rtChart.ScaleViewport = 1 ' specify view port before selecting dimension
        rtChart.ScaleWindow = 1 ' specify window to be scaled before selecting dimension

        rtChart.ScaleDimension = LogicalDimension.Horizontal ' select horizontal or vertical scale/label
        rtChart.ScalePen = PenName.White ' specify pen color for scale

        'UPGRADE_WARNING: (1068) RtChart_FrameRightBottom of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
        rtChart.ScaleLocation = ScaleLocation.RightBottom ' Scale Location: axis or frame
        'UPGRADE_WARNING: (1068) RtChart_Below of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
        rtChart.ScalePosition = ((Position.Below)) ' Scale Position: above or below ScaleLocation

        'UPGRADE_WARNING: (1068) RtChart_Parallel of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
        rtChart.ScaleOrientation = Orientation.Parallel ' Scale Orientation: Parallel or Perpendicular to ScaleDimension
        'UPGRADE_WARNING: (1068) RtChart_RightPath of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
        rtChart.ScalePath = Path.Right ' Scale Path: direction which characters follow: left. right, up, down
        rtChart.ScaleGap = 1 ' Scale Gap: space between ScaleLocation and scale/label in pixels

        rtChart.ScaleFactor = 1 ' multiplier used to convert window units to scale values
        rtChart.ScalePlaces = 0 ' number of significant digits to the right of decimal point

        ' update RtChart Label properties
        rtChart.LabelCaption = "Seconds" ' specify Label Caption text: "volts", "secs" , etc.
        rtChart.LabelPen = PenName.White ' specify pen color for Label

        'UPGRADE_WARNING: (1068) RtChart_Center of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
        rtChart.LabelLocation = LabelLocation.Center ' label location relative to scale: left, right, center
        'UPGRADE_WARNING: (1068) RtChart_Below of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
        rtChart.LabelPosition = ((Position.Below)) ' label Position: above, below, or on LabelLocation
        'UPGRADE_WARNING: (1068) RtChart_Parallel of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
        rtChart.LabelOrientation = Orientation.Parallel ' label character orientation relative to ScaleDimension
        'UPGRADE_WARNING: (1068) RtChart_RightPath of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
        rtChart.LabelPath = Path.Right ' Scale Path: direction which characters follow

        rtChart.FontBold = False
        rtChart.FontItalic = False
        rtChart.FontName = "Arial"
        rtChart.FontSize = 6
        rtChart.FontStrikeout = False
        rtChart.FontUnderline = False

        'UPGRADE_WARNING: (1068) RtChart_ScaleAndLabel of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
        rtChart.ScaleVisibility = ScaleVisibility.ScaleAndLabel ' scale visibility: none, scale, label, or both
        'UPGRADE_WARNING: (1068) RtChart_ScaleInitialize of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
        rtChart.ScaleAction = ScaleAction.Initialize ' execute selected Scale Action

    End Sub

    Sub InitPreDefSelectArray()

        ReDim ScaleNames(12)

        ScaleNames(0).ItemName = "InsideTopFrame"
        ScaleNames(0).ListIndex = 0
        ScaleNames(0).ScaleSelectValue = 1

        ScaleNames(1).ItemName = "InsideBottomFrame"
        ScaleNames(1).ListIndex = 1
        ScaleNames(1).ScaleSelectValue = 2

        ScaleNames(2).ItemName = "InsideLeftFrame"
        ScaleNames(2).ListIndex = 2
        ScaleNames(2).ScaleSelectValue = 3

        ScaleNames(3).ItemName = "InsideRightFrame"
        ScaleNames(3).ListIndex = 3
        ScaleNames(3).ScaleSelectValue = 4

        ScaleNames(4).ItemName = "OutsideTopFrame"
        ScaleNames(4).ListIndex = 4
        ScaleNames(4).ScaleSelectValue = 5

        ScaleNames(5).ItemName = "OutsideBottomFrame"
        ScaleNames(5).ListIndex = 5
        ScaleNames(5).ScaleSelectValue = 6

        ScaleNames(6).ItemName = "OutsideLeftFrame"
        ScaleNames(6).ListIndex = 6
        ScaleNames(6).ScaleSelectValue = 7

        ScaleNames(7).ItemName = "OutsideRightFrame"
        ScaleNames(7).ListIndex = 7
        ScaleNames(7).ScaleSelectValue = 8

        ScaleNames(8).ItemName = "Above_X_Axis"
        ScaleNames(8).ListIndex = 8
        ScaleNames(8).ScaleSelectValue = 9

        ScaleNames(9).ItemName = "Below_X_Axis"
        ScaleNames(9).ListIndex = 9
        ScaleNames(9).ScaleSelectValue = 10

        ScaleNames(10).ItemName = "Above_Y_Axis"
        ScaleNames(10).ListIndex = 10
        ScaleNames(10).ScaleSelectValue = 11

        ScaleNames(11).ItemName = "Below_Y_Axis"
        ScaleNames(11).ListIndex = 11
        ScaleNames(11).ScaleSelectValue = 12

    End Sub

    Sub InitVerticalScale(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl)

        ' initialize vertical scale
        rtChart.LogicalScale = 1 ' select scale name (same for both dimensions)
        rtChart.ScaleViewport = 1 ' specify view port before selecting dimension
        rtChart.ScaleWindow = 1 ' specify window to be scaled before selecting dimension

        rtChart.ScaleDimension = LogicalDimension.Vertical ' select horizontal or vertical scale/label
        rtChart.ScalePen = PenName.White ' specify pen color for scale

        'UPGRADE_WARNING: (1068) RtChart_FrameLeftTop of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
        rtChart.ScaleLocation = ScaleLocation.LeftTop ' Scale Location: axis or frame
        'UPGRADE_WARNING: (1068) RtChart_AboveLeft of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
        rtChart.ScalePosition = Position.AboveLeft ' Scale Position: above or below ScaleLocation

        'UPGRADE_WARNING: (1068) RtChart_Perpendicular of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
        rtChart.ScaleOrientation = Orientation.Perpendicular ' Scale Orientation: Parallel or Perpendicular to ScaleDimension
        'UPGRADE_WARNING: (1068) RtChart_RightPath of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
        rtChart.ScalePath = Path.Right ' Scale Path: direction which characters follow: left. right, up, down
        rtChart.ScaleGap = 1 ' Scale Gap: space between ScaleLocation and scale/label in pixels

        rtChart.ScaleFactor = 1 ' multiplier used to convert window units to scale values
        rtChart.ScalePlaces = 0 ' number of significant digits to the right of decimal point

        ' update RtChart Label properties
        rtChart.LabelCaption = "Volts" ' specify Label Caption text: "volts", "secs" , etc.
        rtChart.LabelPen = PenName.White ' specify pen color for Label

        'UPGRADE_WARNING: (1068) RtChart_Center of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
        rtChart.LabelLocation = LabelLocation.Center ' label location relative to scale: left, right, center
        'UPGRADE_WARNING: (1068) RtChart_AboveLeft of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
        rtChart.LabelPosition = Position.AboveLeft ' label Position: above, below, or on LabelLocation
        'UPGRADE_WARNING: (1068) RtChart_Perpendicular of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
        rtChart.LabelOrientation = Orientation.Perpendicular ' label character orientation relative to ScaleDimension
        'UPGRADE_WARNING: (1068) RtChart_DownPath of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
        rtChart.LabelPath = Path.Down ' Scale Path: direction which characters follow

        rtChart.FontBold = False
        rtChart.FontItalic = False
        rtChart.FontName = "Arial"
        rtChart.FontSize = (7.8)
        rtChart.FontStrikeout = False
        rtChart.FontUnderline = False

        'UPGRADE_WARNING: (1068) RtChart_ScaleAndLabel of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
        rtChart.ScaleVisibility = ScaleVisibility.ScaleAndLabel ' scale visibility: none, scale, label, or both
        'UPGRADE_WARNING: (1068) RtChart_ScaleInitialize of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
        rtChart.ScaleAction = ScaleAction.Initialize ' execute selected Scale Action

    End Sub
End Module