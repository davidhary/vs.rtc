<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ScalesForm
#Region "Upgrade Support "
	Private Shared _Instance As ScalesForm
	Private Shared _InitializingInstance As Boolean
	Public Shared Property DefInstance() As ScalesForm
		Get
			If _Instance Is Nothing OrElse _Instance.IsDisposed Then
				_InitializingInstance = True
				_Instance = CreateInstance()
				_InitializingInstance = False
			End If
			Return _Instance
		End Get
		Set(ByVal value As ScalesForm)
			_Instance = Value
		End Set
	End Property
#End Region
#Region "Windows Form Designer generated code "
	Public Shared Function CreateInstance() As ScalesForm
		Dim theInstance As New ScalesForm()
		theInstance.Form_Load()
		Return theInstance
	End Function
	Private visualControls() As String = New String() {"components", "ToolTipMain", "MenuItem_Exit", "MenuName_File", "MenuItem_SelectColors", "MenuItem_SelectFonts", "MenuName_Edit", "MainMenu1", "RoundCornersCheck", "OutlineCheck", "BorderWidthTextBox", "Label13", "Frame4", "BevelWidthOuterTextBox", "BevelWidthInnerTextBox", "Label3", "Label12", "Frame5", "BevelStyleInnerComboBox", "BevelStyleOuterComboBox", "Label2", "Label1", "Frame6", "Frame3", "Label5", "Label4", "Frame7", "Viewports1", "Viewports2", "Viewports4", "Frame1", "AutoSizeCheck", "RtChartHeight", "RtChartWidth", "DefaultButton", "Label21", "Label22", "Frame11", "LabelPathListBox", "LabelPenListBox", "LabelPositionListBox", "LabelOrientationListBox", "LabelLocationListBox", "LabelCaptionComboBox", "Label39", "Label40", "Label41", "Label38", "Label37", "Label36", "Frame13", "ScaleOrientationListBox", "ScaleLocationListBox", "ScalePathListBox", "ScaleActionListBox", "ScaleVisibilityListBox", "InitializeScale", "ScalePlacesComboBox", "ScaleSelectComboBox", "ScalePenListBox", "ScaleDimensionListBox", "ScaleFactorComboBox", "ScaleGapComboBox", "ScalePositionListBox", "ScaleViewportComboBox", "ScaleWindowComboBox", "Label27", "Label28", "Label42", "Label34", "Label35", "Label33", "Label32", "Label31", "Label30", "Label29", "Label26", "Label25", "Label24", "Label23", "Frame12", "MajorDivVertTextBox", "MajorDivHorzTextBox", "MinorDivTextBox", "Label20", "Label19", "Label18", "Frame10", "MarginLeftTextBox", "MarginTopTextBox", "MarginBottomTextBox", "MarginRightTextBox", "Label17", "Label16", "Label15", "Label14", "Frame9", "FrameCheck", "GridCheck", "AxesCheck", "Frame2", "RtChart1"}
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTipMain As System.Windows.Forms.ToolTip
	Public WithEvents MenuItem_Exit As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents MenuName_File As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents MenuItem_SelectColors As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents MenuItem_SelectFonts As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents MenuName_Edit As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents MainMenu1 As System.Windows.Forms.MenuStrip
	Public WithEvents RoundCornersCheck As System.Windows.Forms.CheckBox
	Public WithEvents OutlineCheck As System.Windows.Forms.CheckBox
	Public WithEvents BorderWidthTextBox As System.Windows.Forms.TextBox
	Public WithEvents Label13 As System.Windows.Forms.Label
	Public WithEvents Frame4 As System.Windows.Forms.GroupBox
	Public WithEvents BevelWidthOuterTextBox As System.Windows.Forms.TextBox
	Public WithEvents BevelWidthInnerTextBox As System.Windows.Forms.TextBox
	Public WithEvents Label3 As System.Windows.Forms.Label
	Public WithEvents Label12 As System.Windows.Forms.Label
	Public WithEvents Frame5 As System.Windows.Forms.GroupBox
	Public WithEvents BevelStyleInnerComboBox As System.Windows.Forms.ComboBox
	Public WithEvents BevelStyleOuterComboBox As System.Windows.Forms.ComboBox
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents Frame6 As System.Windows.Forms.GroupBox
	Public WithEvents Frame3 As System.Windows.Forms.GroupBox
	Public WithEvents Label5 As System.Windows.Forms.Label
	Public WithEvents Label4 As System.Windows.Forms.Label
	Public WithEvents Frame7 As System.Windows.Forms.GroupBox
	Public WithEvents Viewports1 As System.Windows.Forms.RadioButton
	Public WithEvents Viewports2 As System.Windows.Forms.RadioButton
	Public WithEvents Viewports4 As System.Windows.Forms.RadioButton
	Public WithEvents Frame1 As System.Windows.Forms.GroupBox
	Public WithEvents AutoSizeCheck As System.Windows.Forms.CheckBox
	Public WithEvents RtChartHeight As System.Windows.Forms.TextBox
	Public WithEvents RtChartWidth As System.Windows.Forms.TextBox
	Public WithEvents DefaultButton As System.Windows.Forms.Button
	Public WithEvents Label21 As System.Windows.Forms.Label
	Public WithEvents Label22 As System.Windows.Forms.Label
	Public WithEvents Frame11 As System.Windows.Forms.GroupBox
	Public WithEvents LabelPathListBox As System.Windows.Forms.ComboBox
	Public WithEvents LabelPenListBox As System.Windows.Forms.ComboBox
	Public WithEvents LabelPositionListBox As System.Windows.Forms.ComboBox
	Public WithEvents LabelOrientationListBox As System.Windows.Forms.ComboBox
	Public WithEvents LabelLocationListBox As System.Windows.Forms.ComboBox
	Public WithEvents LabelCaptionComboBox As System.Windows.Forms.ComboBox
	Public WithEvents Label39 As System.Windows.Forms.Label
	Public WithEvents Label40 As System.Windows.Forms.Label
	Public WithEvents Label41 As System.Windows.Forms.Label
	Public WithEvents Label38 As System.Windows.Forms.Label
	Public WithEvents Label37 As System.Windows.Forms.Label
	Public WithEvents Label36 As System.Windows.Forms.Label
	Public WithEvents Frame13 As System.Windows.Forms.GroupBox
	Public WithEvents ScaleOrientationListBox As System.Windows.Forms.ComboBox
	Public WithEvents ScaleLocationListBox As System.Windows.Forms.ComboBox
	Public WithEvents ScalePathListBox As System.Windows.Forms.ComboBox
	Public WithEvents ScaleActionListBox As System.Windows.Forms.ComboBox
	Public WithEvents ScaleVisibilityListBox As System.Windows.Forms.ComboBox
	Public WithEvents InitializeScale As System.Windows.Forms.Button
	Public WithEvents ScalePlacesComboBox As System.Windows.Forms.ComboBox
	Public WithEvents ScaleSelectComboBox As System.Windows.Forms.ComboBox
	Public WithEvents ScalePenListBox As System.Windows.Forms.ComboBox
	Public WithEvents ScaleDimensionListBox As System.Windows.Forms.ComboBox
	Public WithEvents ScaleFactorComboBox As System.Windows.Forms.ComboBox
	Public WithEvents ScaleGapComboBox As System.Windows.Forms.ComboBox
	Public WithEvents ScalePositionListBox As System.Windows.Forms.ComboBox
	Public WithEvents ScaleViewportComboBox As System.Windows.Forms.ComboBox
	Public WithEvents ScaleWindowComboBox As System.Windows.Forms.ComboBox
	Public WithEvents Label27 As System.Windows.Forms.Label
	Public WithEvents Label28 As System.Windows.Forms.Label
	Public WithEvents Label42 As System.Windows.Forms.Label
	Public WithEvents Label34 As System.Windows.Forms.Label
	Public WithEvents Label35 As System.Windows.Forms.Label
	Public WithEvents Label33 As System.Windows.Forms.Label
	Public WithEvents Label32 As System.Windows.Forms.Label
	Public WithEvents Label31 As System.Windows.Forms.Label
	Public WithEvents Label30 As System.Windows.Forms.Label
	Public WithEvents Label29 As System.Windows.Forms.Label
	Public WithEvents Label26 As System.Windows.Forms.Label
	Public WithEvents Label25 As System.Windows.Forms.Label
	Public WithEvents Label24 As System.Windows.Forms.Label
	Public WithEvents Label23 As System.Windows.Forms.Label
	Public WithEvents Frame12 As System.Windows.Forms.GroupBox
	Public WithEvents MajorDivVertTextBox As System.Windows.Forms.TextBox
	Public WithEvents MajorDivHorzTextBox As System.Windows.Forms.TextBox
	Public WithEvents MinorDivTextBox As System.Windows.Forms.TextBox
	Public WithEvents Label20 As System.Windows.Forms.Label
	Public WithEvents Label19 As System.Windows.Forms.Label
	Public WithEvents Label18 As System.Windows.Forms.Label
	Public WithEvents Frame10 As System.Windows.Forms.GroupBox
	Public WithEvents MarginLeftTextBox As System.Windows.Forms.TextBox
	Public WithEvents MarginTopTextBox As System.Windows.Forms.TextBox
	Public WithEvents MarginBottomTextBox As System.Windows.Forms.TextBox
	Public WithEvents MarginRightTextBox As System.Windows.Forms.TextBox
	Public WithEvents Label17 As System.Windows.Forms.Label
	Public WithEvents Label16 As System.Windows.Forms.Label
	Public WithEvents Label15 As System.Windows.Forms.Label
	Public WithEvents Label14 As System.Windows.Forms.Label
	Public WithEvents Frame9 As System.Windows.Forms.GroupBox
	Public WithEvents FrameCheck As System.Windows.Forms.CheckBox
	Public WithEvents GridCheck As System.Windows.Forms.CheckBox
	Public WithEvents AxesCheck As System.Windows.Forms.CheckBox
	Public WithEvents Frame2 As System.Windows.Forms.GroupBox
	Public WithEvents RtChart1 As isr.Visuals.RealTimeChart.RealTimeChartControl
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> _
	 Private Sub InitializeComponent()
		Me.components = New System.ComponentModel.Container()
		Dim resources As New System.ComponentModel.ComponentResourceManager(GetType(ScalesForm))
		Me.ToolTipMain = New System.Windows.Forms.ToolTip(Me.components)
		Me.MainMenu1 = New System.Windows.Forms.MenuStrip()
		Me.MenuName_File = New System.Windows.Forms.ToolStripMenuItem()
		Me.MenuItem_Exit = New System.Windows.Forms.ToolStripMenuItem()
		Me.MenuName_Edit = New System.Windows.Forms.ToolStripMenuItem()
		Me.MenuItem_SelectColors = New System.Windows.Forms.ToolStripMenuItem()
		Me.MenuItem_SelectFonts = New System.Windows.Forms.ToolStripMenuItem()
		Me.Frame4 = New System.Windows.Forms.GroupBox()
		Me.RoundCornersCheck = New System.Windows.Forms.CheckBox()
		Me.OutlineCheck = New System.Windows.Forms.CheckBox()
		Me.BorderWidthTextBox = New System.Windows.Forms.TextBox()
		Me.Label13 = New System.Windows.Forms.Label()
		Me.Frame3 = New System.Windows.Forms.GroupBox()
		Me.Frame5 = New System.Windows.Forms.GroupBox()
		Me.BevelWidthOuterTextBox = New System.Windows.Forms.TextBox()
		Me.BevelWidthInnerTextBox = New System.Windows.Forms.TextBox()
		Me.Label3 = New System.Windows.Forms.Label()
		Me.Label12 = New System.Windows.Forms.Label()
		Me.Frame6 = New System.Windows.Forms.GroupBox()
		Me.BevelStyleInnerComboBox = New System.Windows.Forms.ComboBox()
		Me.BevelStyleOuterComboBox = New System.Windows.Forms.ComboBox()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.Frame7 = New System.Windows.Forms.GroupBox()
		Me.Label5 = New System.Windows.Forms.Label()
		Me.Label4 = New System.Windows.Forms.Label()
		Me.Frame1 = New System.Windows.Forms.GroupBox()
		Me.Viewports1 = New System.Windows.Forms.RadioButton()
		Me.Viewports2 = New System.Windows.Forms.RadioButton()
		Me.Viewports4 = New System.Windows.Forms.RadioButton()
		Me.Frame11 = New System.Windows.Forms.GroupBox()
		Me.AutoSizeCheck = New System.Windows.Forms.CheckBox()
		Me.RtChartHeight = New System.Windows.Forms.TextBox()
		Me.RtChartWidth = New System.Windows.Forms.TextBox()
		Me.DefaultButton = New System.Windows.Forms.Button()
		Me.Label21 = New System.Windows.Forms.Label()
		Me.Label22 = New System.Windows.Forms.Label()
		Me.Frame13 = New System.Windows.Forms.GroupBox()
		Me.LabelPathListBox = New System.Windows.Forms.ComboBox()
		Me.LabelPenListBox = New System.Windows.Forms.ComboBox()
		Me.LabelPositionListBox = New System.Windows.Forms.ComboBox()
		Me.LabelOrientationListBox = New System.Windows.Forms.ComboBox()
		Me.LabelLocationListBox = New System.Windows.Forms.ComboBox()
		Me.LabelCaptionComboBox = New System.Windows.Forms.ComboBox()
		Me.Label39 = New System.Windows.Forms.Label()
		Me.Label40 = New System.Windows.Forms.Label()
		Me.Label41 = New System.Windows.Forms.Label()
		Me.Label38 = New System.Windows.Forms.Label()
		Me.Label37 = New System.Windows.Forms.Label()
		Me.Label36 = New System.Windows.Forms.Label()
		Me.Frame12 = New System.Windows.Forms.GroupBox()
		Me.ScaleOrientationListBox = New System.Windows.Forms.ComboBox()
		Me.ScaleLocationListBox = New System.Windows.Forms.ComboBox()
		Me.ScalePathListBox = New System.Windows.Forms.ComboBox()
		Me.ScaleActionListBox = New System.Windows.Forms.ComboBox()
		Me.ScaleVisibilityListBox = New System.Windows.Forms.ComboBox()
		Me.InitializeScale = New System.Windows.Forms.Button()
		Me.ScalePlacesComboBox = New System.Windows.Forms.ComboBox()
		Me.ScaleSelectComboBox = New System.Windows.Forms.ComboBox()
		Me.ScalePenListBox = New System.Windows.Forms.ComboBox()
		Me.ScaleDimensionListBox = New System.Windows.Forms.ComboBox()
		Me.ScaleFactorComboBox = New System.Windows.Forms.ComboBox()
		Me.ScaleGapComboBox = New System.Windows.Forms.ComboBox()
		Me.ScalePositionListBox = New System.Windows.Forms.ComboBox()
		Me.ScaleViewportComboBox = New System.Windows.Forms.ComboBox()
		Me.ScaleWindowComboBox = New System.Windows.Forms.ComboBox()
		Me.Label27 = New System.Windows.Forms.Label()
		Me.Label28 = New System.Windows.Forms.Label()
		Me.Label42 = New System.Windows.Forms.Label()
		Me.Label34 = New System.Windows.Forms.Label()
		Me.Label35 = New System.Windows.Forms.Label()
		Me.Label33 = New System.Windows.Forms.Label()
		Me.Label32 = New System.Windows.Forms.Label()
		Me.Label31 = New System.Windows.Forms.Label()
		Me.Label30 = New System.Windows.Forms.Label()
		Me.Label29 = New System.Windows.Forms.Label()
		Me.Label26 = New System.Windows.Forms.Label()
		Me.Label25 = New System.Windows.Forms.Label()
		Me.Label24 = New System.Windows.Forms.Label()
		Me.Label23 = New System.Windows.Forms.Label()
		Me.Frame10 = New System.Windows.Forms.GroupBox()
		Me.MajorDivVertTextBox = New System.Windows.Forms.TextBox()
		Me.MajorDivHorzTextBox = New System.Windows.Forms.TextBox()
		Me.MinorDivTextBox = New System.Windows.Forms.TextBox()
		Me.Label20 = New System.Windows.Forms.Label()
		Me.Label19 = New System.Windows.Forms.Label()
		Me.Label18 = New System.Windows.Forms.Label()
		Me.Frame9 = New System.Windows.Forms.GroupBox()
		Me.MarginLeftTextBox = New System.Windows.Forms.TextBox()
		Me.MarginTopTextBox = New System.Windows.Forms.TextBox()
		Me.MarginBottomTextBox = New System.Windows.Forms.TextBox()
		Me.MarginRightTextBox = New System.Windows.Forms.TextBox()
		Me.Label17 = New System.Windows.Forms.Label()
		Me.Label16 = New System.Windows.Forms.Label()
		Me.Label15 = New System.Windows.Forms.Label()
		Me.Label14 = New System.Windows.Forms.Label()
		Me.Frame2 = New System.Windows.Forms.GroupBox()
		Me.FrameCheck = New System.Windows.Forms.CheckBox()
		Me.GridCheck = New System.Windows.Forms.CheckBox()
		Me.AxesCheck = New System.Windows.Forms.CheckBox()
		Me.RtChart1 = New isr.Visuals.RealTimeChart.RealTimeChartControl()
		Me.Frame4.SuspendLayout()
		Me.Frame3.SuspendLayout()
		Me.Frame5.SuspendLayout()
		Me.Frame6.SuspendLayout()
		Me.Frame7.SuspendLayout()
		Me.Frame1.SuspendLayout()
		Me.Frame11.SuspendLayout()
		Me.Frame13.SuspendLayout()
		Me.Frame12.SuspendLayout()
		Me.Frame10.SuspendLayout()
		Me.Frame9.SuspendLayout()
		Me.Frame2.SuspendLayout()
		Me.SuspendLayout()
		' 
		'MainMenu1
		' 
		Me.MainMenu1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuName_File, Me.MenuName_Edit})
		' 
		'MenuName_File
		' 
		Me.MenuName_File.Available = True
		Me.MenuName_File.Checked = False
		Me.MenuName_File.Enabled = True
		Me.MenuName_File.Name = "MenuName_File"
		Me.MenuName_File.Text = "&File"
		Me.MenuName_File.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuItem_Exit})
		' 
		'MenuItem_Exit
		' 
		Me.MenuItem_Exit.Available = True
		Me.MenuItem_Exit.Checked = False
		Me.MenuItem_Exit.Enabled = True
		Me.MenuItem_Exit.Name = "MenuItem_Exit"
		Me.MenuItem_Exit.Text = "E&xit"
		' 
		'MenuName_Edit
		' 
		Me.MenuName_Edit.Available = True
		Me.MenuName_Edit.Checked = False
		Me.MenuName_Edit.Enabled = True
		Me.MenuName_Edit.Name = "MenuName_Edit"
		Me.MenuName_Edit.Text = "&Edit"
		Me.MenuName_Edit.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuItem_SelectColors, Me.MenuItem_SelectFonts})
		' 
		'MenuItem_SelectColors
		' 
		Me.MenuItem_SelectColors.Available = True
		Me.MenuItem_SelectColors.Checked = False
		Me.MenuItem_SelectColors.Enabled = True
		Me.MenuItem_SelectColors.Name = "MenuItem_SelectColors"
		Me.MenuItem_SelectColors.Text = "&Colors..."
		' 
		'MenuItem_SelectFonts
		' 
		Me.MenuItem_SelectFonts.Available = True
		Me.MenuItem_SelectFonts.Checked = False
		Me.MenuItem_SelectFonts.Enabled = True
		Me.MenuItem_SelectFonts.Name = "MenuItem_SelectFonts"
		Me.MenuItem_SelectFonts.Text = "&Fonts..."
		' 
		'Frame4
		' 
		Me.Frame4.AllowDrop = True
		Me.Frame4.BackColor = System.Drawing.SystemColors.Window
		Me.Frame4.Controls.Add(Me.RoundCornersCheck)
		Me.Frame4.Controls.Add(Me.OutlineCheck)
		Me.Frame4.Controls.Add(Me.BorderWidthTextBox)
		Me.Frame4.Controls.Add(Me.Label13)
		Me.Frame4.Enabled = True
		Me.Frame4.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Frame4.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Frame4.Location = New System.Drawing.Point(180, 264)
		Me.Frame4.Name = "Frame4"
		Me.Frame4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame4.Size = New System.Drawing.Size(69, 113)
		Me.Frame4.TabIndex = 39
		Me.Frame4.Text = "Border"
		Me.Frame4.Visible = True
		' 
		'RoundCornersCheck
		' 
		Me.RoundCornersCheck.AllowDrop = True
		Me.RoundCornersCheck.Appearance = System.Windows.Forms.Appearance.Normal
		Me.RoundCornersCheck.BackColor = System.Drawing.SystemColors.Window
		Me.RoundCornersCheck.CausesValidation = True
		Me.RoundCornersCheck.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.RoundCornersCheck.CheckState = System.Windows.Forms.CheckState.Unchecked
		Me.RoundCornersCheck.Enabled = True
		Me.RoundCornersCheck.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.RoundCornersCheck.ForeColor = System.Drawing.SystemColors.WindowText
		Me.RoundCornersCheck.Location = New System.Drawing.Point(8, 80)
		Me.RoundCornersCheck.Name = "RoundCornersCheck"
		Me.RoundCornersCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.RoundCornersCheck.Size = New System.Drawing.Size(57, 13)
		Me.RoundCornersCheck.TabIndex = 40
		Me.RoundCornersCheck.TabStop = True
		Me.RoundCornersCheck.Text = "Round"
		Me.RoundCornersCheck.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.RoundCornersCheck.Visible = True
		' 
		'OutlineCheck
		' 
		Me.OutlineCheck.AllowDrop = True
		Me.OutlineCheck.Appearance = System.Windows.Forms.Appearance.Normal
		Me.OutlineCheck.BackColor = System.Drawing.SystemColors.Window
		Me.OutlineCheck.CausesValidation = True
		Me.OutlineCheck.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.OutlineCheck.CheckState = System.Windows.Forms.CheckState.Checked
		Me.OutlineCheck.Enabled = True
		Me.OutlineCheck.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.OutlineCheck.ForeColor = System.Drawing.SystemColors.WindowText
		Me.OutlineCheck.Location = New System.Drawing.Point(8, 56)
		Me.OutlineCheck.Name = "OutlineCheck"
		Me.OutlineCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.OutlineCheck.Size = New System.Drawing.Size(57, 13)
		Me.OutlineCheck.TabIndex = 41
		Me.OutlineCheck.TabStop = True
		Me.OutlineCheck.Text = "Outline"
		Me.OutlineCheck.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.OutlineCheck.Visible = True
		' 
		'BorderWidthTextBox
		' 
		Me.BorderWidthTextBox.AcceptsReturn = True
		Me.BorderWidthTextBox.AllowDrop = True
		Me.BorderWidthTextBox.BackColor = System.Drawing.SystemColors.Window
		Me.BorderWidthTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.BorderWidthTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.BorderWidthTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.BorderWidthTextBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.BorderWidthTextBox.Location = New System.Drawing.Point(8, 28)
		Me.BorderWidthTextBox.MaxLength = 5
		Me.BorderWidthTextBox.Name = "BorderWidthTextBox"
		Me.BorderWidthTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.BorderWidthTextBox.Size = New System.Drawing.Size(37, 20)
		Me.BorderWidthTextBox.TabIndex = 42
		' 
		'Label13
		' 
		Me.Label13.AllowDrop = True
		Me.Label13.BackColor = System.Drawing.SystemColors.Window
		Me.Label13.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label13.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label13.Location = New System.Drawing.Point(8, 16)
		Me.Label13.Name = "Label13"
		Me.Label13.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label13.Size = New System.Drawing.Size(45, 13)
		Me.Label13.TabIndex = 43
		Me.Label13.Text = "Width"
		' 
		'Frame3
		' 
		Me.Frame3.AllowDrop = True
		Me.Frame3.BackColor = System.Drawing.SystemColors.Window
		Me.Frame3.Controls.Add(Me.Frame5)
		Me.Frame3.Controls.Add(Me.Frame6)
		Me.Frame3.Enabled = True
		Me.Frame3.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Frame3.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Frame3.Location = New System.Drawing.Point(4, 264)
		Me.Frame3.Name = "Frame3"
		Me.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame3.Size = New System.Drawing.Size(169, 113)
		Me.Frame3.TabIndex = 82
		Me.Frame3.Text = "Bevel"
		Me.Frame3.Visible = True
		' 
		'Frame5
		' 
		Me.Frame5.AllowDrop = True
		Me.Frame5.BackColor = System.Drawing.SystemColors.Window
		Me.Frame5.Controls.Add(Me.BevelWidthOuterTextBox)
		Me.Frame5.Controls.Add(Me.BevelWidthInnerTextBox)
		Me.Frame5.Controls.Add(Me.Label3)
		Me.Frame5.Controls.Add(Me.Label12)
		Me.Frame5.Enabled = True
		Me.Frame5.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Frame5.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Frame5.Location = New System.Drawing.Point(104, 16)
		Me.Frame5.Name = "Frame5"
		Me.Frame5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame5.Size = New System.Drawing.Size(57, 93)
		Me.Frame5.TabIndex = 88
		Me.Frame5.Text = "&Width"
		Me.Frame5.Visible = True
		' 
		'BevelWidthOuterTextBox
		' 
		Me.BevelWidthOuterTextBox.AcceptsReturn = True
		Me.BevelWidthOuterTextBox.AllowDrop = True
		Me.BevelWidthOuterTextBox.BackColor = System.Drawing.SystemColors.Window
		Me.BevelWidthOuterTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.BevelWidthOuterTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.BevelWidthOuterTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.BevelWidthOuterTextBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.BevelWidthOuterTextBox.Location = New System.Drawing.Point(8, 68)
		Me.BevelWidthOuterTextBox.MaxLength = 5
		Me.BevelWidthOuterTextBox.Name = "BevelWidthOuterTextBox"
		Me.BevelWidthOuterTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.BevelWidthOuterTextBox.Size = New System.Drawing.Size(41, 20)
		Me.BevelWidthOuterTextBox.TabIndex = 90
		' 
		'BevelWidthInnerTextBox
		' 
		Me.BevelWidthInnerTextBox.AcceptsReturn = True
		Me.BevelWidthInnerTextBox.AllowDrop = True
		Me.BevelWidthInnerTextBox.BackColor = System.Drawing.SystemColors.Window
		Me.BevelWidthInnerTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.BevelWidthInnerTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.BevelWidthInnerTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.BevelWidthInnerTextBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.BevelWidthInnerTextBox.Location = New System.Drawing.Point(8, 32)
		Me.BevelWidthInnerTextBox.MaxLength = 5
		Me.BevelWidthInnerTextBox.Name = "BevelWidthInnerTextBox"
		Me.BevelWidthInnerTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.BevelWidthInnerTextBox.Size = New System.Drawing.Size(41, 20)
		Me.BevelWidthInnerTextBox.TabIndex = 89
		' 
		'Label3
		' 
		Me.Label3.AllowDrop = True
		Me.Label3.BackColor = System.Drawing.SystemColors.Window
		Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label3.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label3.Location = New System.Drawing.Point(8, 20)
		Me.Label3.Name = "Label3"
		Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label3.Size = New System.Drawing.Size(33, 13)
		Me.Label3.TabIndex = 92
		Me.Label3.Text = "Inner"
		' 
		'Label12
		' 
		Me.Label12.AllowDrop = True
		Me.Label12.BackColor = System.Drawing.SystemColors.Window
		Me.Label12.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label12.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label12.Location = New System.Drawing.Point(8, 56)
		Me.Label12.Name = "Label12"
		Me.Label12.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label12.Size = New System.Drawing.Size(37, 13)
		Me.Label12.TabIndex = 91
		Me.Label12.Text = "Outer"
		' 
		'Frame6
		' 
		Me.Frame6.AllowDrop = True
		Me.Frame6.BackColor = System.Drawing.SystemColors.Window
		Me.Frame6.Controls.Add(Me.BevelStyleInnerComboBox)
		Me.Frame6.Controls.Add(Me.BevelStyleOuterComboBox)
		Me.Frame6.Controls.Add(Me.Label2)
		Me.Frame6.Controls.Add(Me.Label1)
		Me.Frame6.Enabled = True
		Me.Frame6.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Frame6.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Frame6.Location = New System.Drawing.Point(8, 16)
		Me.Frame6.Name = "Frame6"
		Me.Frame6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame6.Size = New System.Drawing.Size(89, 93)
		Me.Frame6.TabIndex = 83
		Me.Frame6.Text = "&Style"
		Me.Frame6.Visible = True
		' 
		'BevelStyleInnerComboBox
		' 
		Me.BevelStyleInnerComboBox.AllowDrop = True
		Me.BevelStyleInnerComboBox.BackColor = System.Drawing.SystemColors.Window
		Me.BevelStyleInnerComboBox.CausesValidation = True
		Me.BevelStyleInnerComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.BevelStyleInnerComboBox.Enabled = True
		Me.BevelStyleInnerComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.BevelStyleInnerComboBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.BevelStyleInnerComboBox.IntegralHeight = True
		Me.BevelStyleInnerComboBox.Location = New System.Drawing.Point(8, 32)
		Me.BevelStyleInnerComboBox.Name = "BevelStyleInnerComboBox"
		Me.BevelStyleInnerComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.BevelStyleInnerComboBox.Size = New System.Drawing.Size(73, 20)
		Me.BevelStyleInnerComboBox.Sorted = False
		Me.BevelStyleInnerComboBox.TabIndex = 85
		Me.BevelStyleInnerComboBox.TabStop = True
		Me.BevelStyleInnerComboBox.Visible = True
		' 
		'BevelStyleOuterComboBox
		' 
		Me.BevelStyleOuterComboBox.AllowDrop = True
		Me.BevelStyleOuterComboBox.BackColor = System.Drawing.SystemColors.Window
		Me.BevelStyleOuterComboBox.CausesValidation = True
		Me.BevelStyleOuterComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.BevelStyleOuterComboBox.Enabled = True
		Me.BevelStyleOuterComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.BevelStyleOuterComboBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.BevelStyleOuterComboBox.IntegralHeight = True
		Me.BevelStyleOuterComboBox.Location = New System.Drawing.Point(8, 68)
		Me.BevelStyleOuterComboBox.Name = "BevelStyleOuterComboBox"
		Me.BevelStyleOuterComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.BevelStyleOuterComboBox.Size = New System.Drawing.Size(73, 20)
		Me.BevelStyleOuterComboBox.Sorted = False
		Me.BevelStyleOuterComboBox.TabIndex = 84
		Me.BevelStyleOuterComboBox.TabStop = True
		Me.BevelStyleOuterComboBox.Visible = True
		' 
		'Label2
		' 
		Me.Label2.AllowDrop = True
		Me.Label2.BackColor = System.Drawing.SystemColors.Window
		Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label2.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label2.Location = New System.Drawing.Point(8, 56)
		Me.Label2.Name = "Label2"
		Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label2.Size = New System.Drawing.Size(45, 13)
		Me.Label2.TabIndex = 87
		Me.Label2.Text = "Outer"
		' 
		'Label1
		' 
		Me.Label1.AllowDrop = True
		Me.Label1.BackColor = System.Drawing.SystemColors.Window
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label1.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label1.Location = New System.Drawing.Point(8, 20)
		Me.Label1.Name = "Label1"
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.Size = New System.Drawing.Size(45, 13)
		Me.Label1.TabIndex = 86
		Me.Label1.Text = "Inner"
		' 
		'Frame7
		' 
		Me.Frame7.AllowDrop = True
		Me.Frame7.BackColor = System.Drawing.SystemColors.Window
		Me.Frame7.Controls.Add(Me.Label5)
		Me.Frame7.Controls.Add(Me.Label4)
		Me.Frame7.Enabled = True
		Me.Frame7.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Frame7.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Frame7.Location = New System.Drawing.Point(280, 24)
		Me.Frame7.Name = "Frame7"
		Me.Frame7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame7.Size = New System.Drawing.Size(69, 241)
		Me.Frame7.TabIndex = 78
		Me.Frame7.Text = "Notes"
		Me.Frame7.Visible = True
		' 
		'Label5
		' 
		Me.Label5.AllowDrop = True
		Me.Label5.BackColor = System.Drawing.SystemColors.Window
		Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label5.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label5.Location = New System.Drawing.Point(4, 108)
		Me.Label5.Name = "Label5"
		Me.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label5.Size = New System.Drawing.Size(61, 129)
		Me.Label5.TabIndex = 80
		Me.Label5.Text = "Press enter or change focus to input new data. Scale changes must use Action."
		' 
		'Label4
		' 
		Me.Label4.AllowDrop = True
		Me.Label4.BackColor = System.Drawing.SystemColors.Window
		Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label4.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label4.Location = New System.Drawing.Point(4, 16)
		Me.Label4.Name = "Label4"
		Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label4.Size = New System.Drawing.Size(61, 89)
		Me.Label4.TabIndex = 79
		Me.Label4.Text = "Double Click on RtChart to toggle default and new size."
		' 
		'Frame1
		' 
		Me.Frame1.AllowDrop = True
		Me.Frame1.BackColor = System.Drawing.SystemColors.Window
		Me.Frame1.Controls.Add(Me.Viewports1)
		Me.Frame1.Controls.Add(Me.Viewports2)
		Me.Frame1.Controls.Add(Me.Viewports4)
		Me.Frame1.Enabled = True
		Me.Frame1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Frame1.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Frame1.Location = New System.Drawing.Point(180, 376)
		Me.Frame1.Name = "Frame1"
		Me.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame1.Size = New System.Drawing.Size(69, 77)
		Me.Frame1.TabIndex = 77
		Me.Frame1.Text = "Viewports"
		Me.Frame1.Visible = True
		' 
		'Viewports1
		' 
		Me.Viewports1.AllowDrop = True
		Me.Viewports1.Appearance = System.Windows.Forms.Appearance.Normal
		Me.Viewports1.BackColor = System.Drawing.SystemColors.Window
		Me.Viewports1.CausesValidation = True
		Me.Viewports1.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.Viewports1.Checked = True
		Me.Viewports1.Enabled = True
		Me.Viewports1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Viewports1.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Viewports1.Location = New System.Drawing.Point(8, 20)
		Me.Viewports1.Name = "Viewports1"
		Me.Viewports1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Viewports1.Size = New System.Drawing.Size(49, 13)
		Me.Viewports1.TabIndex = 10
		Me.Viewports1.TabStop = True
		Me.Viewports1.Text = " 1"
		Me.Viewports1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.Viewports1.Visible = True
		' 
		'Viewports2
		' 
		Me.Viewports2.AllowDrop = True
		Me.Viewports2.Appearance = System.Windows.Forms.Appearance.Normal
		Me.Viewports2.BackColor = System.Drawing.SystemColors.Window
		Me.Viewports2.CausesValidation = True
		Me.Viewports2.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.Viewports2.Checked = False
		Me.Viewports2.Enabled = True
		Me.Viewports2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Viewports2.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Viewports2.Location = New System.Drawing.Point(8, 40)
		Me.Viewports2.Name = "Viewports2"
		Me.Viewports2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Viewports2.Size = New System.Drawing.Size(49, 13)
		Me.Viewports2.TabIndex = 11
		Me.Viewports2.TabStop = True
		Me.Viewports2.Text = " 2"
		Me.Viewports2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.Viewports2.Visible = True
		' 
		'Viewports4
		' 
		Me.Viewports4.AllowDrop = True
		Me.Viewports4.Appearance = System.Windows.Forms.Appearance.Normal
		Me.Viewports4.BackColor = System.Drawing.SystemColors.Window
		Me.Viewports4.CausesValidation = True
		Me.Viewports4.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.Viewports4.Checked = False
		Me.Viewports4.Enabled = True
		Me.Viewports4.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Viewports4.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Viewports4.Location = New System.Drawing.Point(8, 60)
		Me.Viewports4.Name = "Viewports4"
		Me.Viewports4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Viewports4.Size = New System.Drawing.Size(49, 13)
		Me.Viewports4.TabIndex = 12
		Me.Viewports4.TabStop = True
		Me.Viewports4.Text = " 4"
		Me.Viewports4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.Viewports4.Visible = True
		' 
		'Frame11
		' 
		Me.Frame11.AllowDrop = True
		Me.Frame11.BackColor = System.Drawing.SystemColors.Window
		Me.Frame11.Controls.Add(Me.AutoSizeCheck)
		Me.Frame11.Controls.Add(Me.RtChartHeight)
		Me.Frame11.Controls.Add(Me.RtChartWidth)
		Me.Frame11.Controls.Add(Me.DefaultButton)
		Me.Frame11.Controls.Add(Me.Label21)
		Me.Frame11.Controls.Add(Me.Label22)
		Me.Frame11.Enabled = True
		Me.Frame11.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Frame11.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Frame11.Location = New System.Drawing.Point(256, 352)
		Me.Frame11.Name = "Frame11"
		Me.Frame11.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame11.Size = New System.Drawing.Size(93, 101)
		Me.Frame11.TabIndex = 74
		Me.Frame11.Text = "RtChart Size"
		Me.Frame11.Visible = True
		' 
		'AutoSizeCheck
		' 
		Me.AutoSizeCheck.AllowDrop = True
		Me.AutoSizeCheck.Appearance = System.Windows.Forms.Appearance.Normal
		Me.AutoSizeCheck.BackColor = System.Drawing.SystemColors.Window
		Me.AutoSizeCheck.CausesValidation = True
		Me.AutoSizeCheck.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.AutoSizeCheck.CheckState = System.Windows.Forms.CheckState.Checked
		Me.AutoSizeCheck.Enabled = True
		Me.AutoSizeCheck.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.AutoSizeCheck.ForeColor = System.Drawing.SystemColors.WindowText
		Me.AutoSizeCheck.Location = New System.Drawing.Point(12, 48)
		Me.AutoSizeCheck.Name = "AutoSizeCheck"
		Me.AutoSizeCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.AutoSizeCheck.Size = New System.Drawing.Size(69, 13)
		Me.AutoSizeCheck.TabIndex = 81
		Me.AutoSizeCheck.TabStop = True
		Me.AutoSizeCheck.Text = "AutoSize"
		Me.AutoSizeCheck.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.AutoSizeCheck.Visible = True
		' 
		'RtChartHeight
		' 
		Me.RtChartHeight.AcceptsReturn = True
		Me.RtChartHeight.AllowDrop = True
		Me.RtChartHeight.BackColor = System.Drawing.SystemColors.Window
		Me.RtChartHeight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.RtChartHeight.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.RtChartHeight.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.RtChartHeight.ForeColor = System.Drawing.SystemColors.WindowText
		Me.RtChartHeight.Location = New System.Drawing.Point(4, 28)
		Me.RtChartHeight.MaxLength = 5
		Me.RtChartHeight.Name = "RtChartHeight"
		Me.RtChartHeight.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.RtChartHeight.Size = New System.Drawing.Size(37, 19)
		Me.RtChartHeight.TabIndex = 13
		' 
		'RtChartWidth
		' 
		Me.RtChartWidth.AcceptsReturn = True
		Me.RtChartWidth.AllowDrop = True
		Me.RtChartWidth.BackColor = System.Drawing.SystemColors.Window
		Me.RtChartWidth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.RtChartWidth.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.RtChartWidth.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.RtChartWidth.ForeColor = System.Drawing.SystemColors.WindowText
		Me.RtChartWidth.Location = New System.Drawing.Point(52, 28)
		Me.RtChartWidth.MaxLength = 5
		Me.RtChartWidth.Name = "RtChartWidth"
		Me.RtChartWidth.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.RtChartWidth.Size = New System.Drawing.Size(37, 19)
		Me.RtChartWidth.TabIndex = 14
		' 
		'DefaultButton
		' 
		Me.DefaultButton.AllowDrop = True
		Me.DefaultButton.BackColor = System.Drawing.SystemColors.Control
		Me.DefaultButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.DefaultButton.ForeColor = System.Drawing.SystemColors.ControlText
		Me.DefaultButton.Location = New System.Drawing.Point(8, 68)
		Me.DefaultButton.Name = "DefaultButton"
		Me.DefaultButton.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.DefaultButton.Size = New System.Drawing.Size(77, 25)
		Me.DefaultButton.TabIndex = 15
		Me.DefaultButton.Text = "Reset All"
		Me.DefaultButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
		Me.DefaultButton.UseVisualStyleBackColor = False
		' 
		'Label21
		' 
		Me.Label21.AllowDrop = True
		Me.Label21.BackColor = System.Drawing.SystemColors.Window
		Me.Label21.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label21.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label21.Location = New System.Drawing.Point(52, 16)
		Me.Label21.Name = "Label21"
		Me.Label21.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label21.Size = New System.Drawing.Size(37, 13)
		Me.Label21.TabIndex = 76
		Me.Label21.Text = "Width"
		' 
		'Label22
		' 
		Me.Label22.AllowDrop = True
		Me.Label22.BackColor = System.Drawing.SystemColors.Window
		Me.Label22.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label22.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label22.Location = New System.Drawing.Point(4, 16)
		Me.Label22.Name = "Label22"
		Me.Label22.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label22.Size = New System.Drawing.Size(41, 13)
		Me.Label22.TabIndex = 75
		Me.Label22.Text = "Height"
		' 
		'Frame13
		' 
		Me.Frame13.AllowDrop = True
		Me.Frame13.BackColor = System.Drawing.SystemColors.Window
		Me.Frame13.Controls.Add(Me.LabelPathListBox)
		Me.Frame13.Controls.Add(Me.LabelPenListBox)
		Me.Frame13.Controls.Add(Me.LabelPositionListBox)
		Me.Frame13.Controls.Add(Me.LabelOrientationListBox)
		Me.Frame13.Controls.Add(Me.LabelLocationListBox)
		Me.Frame13.Controls.Add(Me.LabelCaptionComboBox)
		Me.Frame13.Controls.Add(Me.Label39)
		Me.Frame13.Controls.Add(Me.Label40)
		Me.Frame13.Controls.Add(Me.Label41)
		Me.Frame13.Controls.Add(Me.Label38)
		Me.Frame13.Controls.Add(Me.Label37)
		Me.Frame13.Controls.Add(Me.Label36)
		Me.Frame13.Enabled = True
		Me.Frame13.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Frame13.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Frame13.Location = New System.Drawing.Point(356, 24)
		Me.Frame13.Name = "Frame13"
		Me.Frame13.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame13.Size = New System.Drawing.Size(241, 125)
		Me.Frame13.TabIndex = 67
		Me.Frame13.Text = "Labels"
		Me.Frame13.Visible = True
		' 
		'LabelPathListBox
		' 
		Me.LabelPathListBox.AllowDrop = True
		Me.LabelPathListBox.BackColor = System.Drawing.SystemColors.Window
		Me.LabelPathListBox.CausesValidation = True
		Me.LabelPathListBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.LabelPathListBox.Enabled = True
		Me.LabelPathListBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.LabelPathListBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.LabelPathListBox.IntegralHeight = True
		Me.LabelPathListBox.Location = New System.Drawing.Point(124, 100)
		Me.LabelPathListBox.Name = "LabelPathListBox"
		Me.LabelPathListBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.LabelPathListBox.Size = New System.Drawing.Size(109, 20)
		Me.LabelPathListBox.Sorted = False
		Me.LabelPathListBox.TabIndex = 21
		Me.LabelPathListBox.TabStop = True
		Me.LabelPathListBox.Visible = True
		' 
		'LabelPenListBox
		' 
		Me.LabelPenListBox.AllowDrop = True
		Me.LabelPenListBox.BackColor = System.Drawing.SystemColors.Window
		Me.LabelPenListBox.CausesValidation = True
		Me.LabelPenListBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.LabelPenListBox.Enabled = True
		Me.LabelPenListBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.LabelPenListBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.LabelPenListBox.IntegralHeight = True
		Me.LabelPenListBox.Location = New System.Drawing.Point(124, 28)
		Me.LabelPenListBox.Name = "LabelPenListBox"
		Me.LabelPenListBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.LabelPenListBox.Size = New System.Drawing.Size(109, 20)
		Me.LabelPenListBox.Sorted = False
		Me.LabelPenListBox.TabIndex = 17
		Me.LabelPenListBox.TabStop = True
		Me.LabelPenListBox.Visible = True
		' 
		'LabelPositionListBox
		' 
		Me.LabelPositionListBox.AllowDrop = True
		Me.LabelPositionListBox.BackColor = System.Drawing.SystemColors.Window
		Me.LabelPositionListBox.CausesValidation = True
		Me.LabelPositionListBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.LabelPositionListBox.Enabled = True
		Me.LabelPositionListBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.LabelPositionListBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.LabelPositionListBox.IntegralHeight = True
		Me.LabelPositionListBox.Location = New System.Drawing.Point(8, 100)
		Me.LabelPositionListBox.Name = "LabelPositionListBox"
		Me.LabelPositionListBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.LabelPositionListBox.Size = New System.Drawing.Size(109, 20)
		Me.LabelPositionListBox.Sorted = False
		Me.LabelPositionListBox.TabIndex = 20
		Me.LabelPositionListBox.TabStop = True
		Me.LabelPositionListBox.Visible = True
		' 
		'LabelOrientationListBox
		' 
		Me.LabelOrientationListBox.AllowDrop = True
		Me.LabelOrientationListBox.BackColor = System.Drawing.SystemColors.Window
		Me.LabelOrientationListBox.CausesValidation = True
		Me.LabelOrientationListBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.LabelOrientationListBox.Enabled = True
		Me.LabelOrientationListBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.LabelOrientationListBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.LabelOrientationListBox.IntegralHeight = True
		Me.LabelOrientationListBox.Location = New System.Drawing.Point(124, 64)
		Me.LabelOrientationListBox.Name = "LabelOrientationListBox"
		Me.LabelOrientationListBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.LabelOrientationListBox.Size = New System.Drawing.Size(109, 20)
		Me.LabelOrientationListBox.Sorted = False
		Me.LabelOrientationListBox.TabIndex = 19
		Me.LabelOrientationListBox.TabStop = True
		Me.LabelOrientationListBox.Visible = True
		' 
		'LabelLocationListBox
		' 
		Me.LabelLocationListBox.AllowDrop = True
		Me.LabelLocationListBox.BackColor = System.Drawing.SystemColors.Window
		Me.LabelLocationListBox.CausesValidation = True
		Me.LabelLocationListBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.LabelLocationListBox.Enabled = True
		Me.LabelLocationListBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.LabelLocationListBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.LabelLocationListBox.IntegralHeight = True
		Me.LabelLocationListBox.Location = New System.Drawing.Point(8, 64)
		Me.LabelLocationListBox.Name = "LabelLocationListBox"
		Me.LabelLocationListBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.LabelLocationListBox.Size = New System.Drawing.Size(109, 20)
		Me.LabelLocationListBox.Sorted = False
		Me.LabelLocationListBox.TabIndex = 18
		Me.LabelLocationListBox.TabStop = True
		Me.LabelLocationListBox.Visible = True
		' 
		'LabelCaptionComboBox
		' 
		Me.LabelCaptionComboBox.AllowDrop = True
		Me.LabelCaptionComboBox.BackColor = System.Drawing.SystemColors.Window
		Me.LabelCaptionComboBox.CausesValidation = True
		Me.LabelCaptionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
		Me.LabelCaptionComboBox.Enabled = True
		Me.LabelCaptionComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.LabelCaptionComboBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.LabelCaptionComboBox.IntegralHeight = True
		Me.LabelCaptionComboBox.Location = New System.Drawing.Point(8, 28)
		Me.LabelCaptionComboBox.Name = "LabelCaptionComboBox"
		Me.LabelCaptionComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.LabelCaptionComboBox.Size = New System.Drawing.Size(109, 20)
		Me.LabelCaptionComboBox.Sorted = False
		Me.LabelCaptionComboBox.TabIndex = 16
		Me.LabelCaptionComboBox.TabStop = True
		Me.LabelCaptionComboBox.Visible = True
		' 
		'Label39
		' 
		Me.Label39.AllowDrop = True
		Me.Label39.BackColor = System.Drawing.SystemColors.Window
		Me.Label39.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label39.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label39.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label39.Location = New System.Drawing.Point(124, 88)
		Me.Label39.Name = "Label39"
		Me.Label39.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label39.Size = New System.Drawing.Size(65, 13)
		Me.Label39.TabIndex = 59
		Me.Label39.Text = "Path"
		' 
		'Label40
		' 
		Me.Label40.AllowDrop = True
		Me.Label40.BackColor = System.Drawing.SystemColors.Window
		Me.Label40.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label40.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label40.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label40.Location = New System.Drawing.Point(124, 16)
		Me.Label40.Name = "Label40"
		Me.Label40.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label40.Size = New System.Drawing.Size(65, 13)
		Me.Label40.TabIndex = 44
		Me.Label40.Text = "Pen"
		' 
		'Label41
		' 
		Me.Label41.AllowDrop = True
		Me.Label41.BackColor = System.Drawing.SystemColors.Window
		Me.Label41.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label41.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label41.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label41.Location = New System.Drawing.Point(8, 88)
		Me.Label41.Name = "Label41"
		Me.Label41.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label41.Size = New System.Drawing.Size(65, 13)
		Me.Label41.TabIndex = 45
		Me.Label41.Text = "Position"
		' 
		'Label38
		' 
		Me.Label38.AllowDrop = True
		Me.Label38.BackColor = System.Drawing.SystemColors.Window
		Me.Label38.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label38.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label38.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label38.Location = New System.Drawing.Point(124, 52)
		Me.Label38.Name = "Label38"
		Me.Label38.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label38.Size = New System.Drawing.Size(65, 13)
		Me.Label38.TabIndex = 73
		Me.Label38.Text = "Orientation"
		' 
		'Label37
		' 
		Me.Label37.AllowDrop = True
		Me.Label37.BackColor = System.Drawing.SystemColors.Window
		Me.Label37.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label37.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label37.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label37.Location = New System.Drawing.Point(8, 52)
		Me.Label37.Name = "Label37"
		Me.Label37.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label37.Size = New System.Drawing.Size(65, 13)
		Me.Label37.TabIndex = 72
		Me.Label37.Text = "Location"
		' 
		'Label36
		' 
		Me.Label36.AllowDrop = True
		Me.Label36.BackColor = System.Drawing.SystemColors.Window
		Me.Label36.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label36.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label36.Location = New System.Drawing.Point(8, 16)
		Me.Label36.Name = "Label36"
		Me.Label36.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label36.Size = New System.Drawing.Size(65, 13)
		Me.Label36.TabIndex = 71
		Me.Label36.Text = "Caption"
		' 
		'Frame12
		' 
		Me.Frame12.AllowDrop = True
		Me.Frame12.BackColor = System.Drawing.SystemColors.Window
		Me.Frame12.Controls.Add(Me.ScaleOrientationListBox)
		Me.Frame12.Controls.Add(Me.ScaleLocationListBox)
		Me.Frame12.Controls.Add(Me.ScalePathListBox)
		Me.Frame12.Controls.Add(Me.ScaleActionListBox)
		Me.Frame12.Controls.Add(Me.ScaleVisibilityListBox)
		Me.Frame12.Controls.Add(Me.InitializeScale)
		Me.Frame12.Controls.Add(Me.ScalePlacesComboBox)
		Me.Frame12.Controls.Add(Me.ScaleSelectComboBox)
		Me.Frame12.Controls.Add(Me.ScalePenListBox)
		Me.Frame12.Controls.Add(Me.ScaleDimensionListBox)
		Me.Frame12.Controls.Add(Me.ScaleFactorComboBox)
		Me.Frame12.Controls.Add(Me.ScaleGapComboBox)
		Me.Frame12.Controls.Add(Me.ScalePositionListBox)
		Me.Frame12.Controls.Add(Me.ScaleViewportComboBox)
		Me.Frame12.Controls.Add(Me.ScaleWindowComboBox)
		Me.Frame12.Controls.Add(Me.Label27)
		Me.Frame12.Controls.Add(Me.Label28)
		Me.Frame12.Controls.Add(Me.Label42)
		Me.Frame12.Controls.Add(Me.Label34)
		Me.Frame12.Controls.Add(Me.Label35)
		Me.Frame12.Controls.Add(Me.Label33)
		Me.Frame12.Controls.Add(Me.Label32)
		Me.Frame12.Controls.Add(Me.Label31)
		Me.Frame12.Controls.Add(Me.Label30)
		Me.Frame12.Controls.Add(Me.Label29)
		Me.Frame12.Controls.Add(Me.Label26)
		Me.Frame12.Controls.Add(Me.Label25)
		Me.Frame12.Controls.Add(Me.Label24)
		Me.Frame12.Controls.Add(Me.Label23)
		Me.Frame12.Enabled = True
		Me.Frame12.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Frame12.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Frame12.Location = New System.Drawing.Point(356, 148)
		Me.Frame12.Name = "Frame12"
		Me.Frame12.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame12.Size = New System.Drawing.Size(241, 305)
		Me.Frame12.TabIndex = 54
		Me.Frame12.Text = "Scale"
		Me.Frame12.Visible = True
		' 
		'ScaleOrientationListBox
		' 
		Me.ScaleOrientationListBox.AllowDrop = True
		Me.ScaleOrientationListBox.BackColor = System.Drawing.SystemColors.Window
		Me.ScaleOrientationListBox.CausesValidation = True
		Me.ScaleOrientationListBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.ScaleOrientationListBox.Enabled = True
		Me.ScaleOrientationListBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.ScaleOrientationListBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.ScaleOrientationListBox.IntegralHeight = True
		Me.ScaleOrientationListBox.Location = New System.Drawing.Point(8, 172)
		Me.ScaleOrientationListBox.Name = "ScaleOrientationListBox"
		Me.ScaleOrientationListBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ScaleOrientationListBox.Size = New System.Drawing.Size(109, 20)
		Me.ScaleOrientationListBox.Sorted = False
		Me.ScaleOrientationListBox.TabIndex = 31
		Me.ScaleOrientationListBox.TabStop = True
		Me.ScaleOrientationListBox.Visible = True
		' 
		'ScaleLocationListBox
		' 
		Me.ScaleLocationListBox.AllowDrop = True
		Me.ScaleLocationListBox.BackColor = System.Drawing.SystemColors.Window
		Me.ScaleLocationListBox.CausesValidation = True
		Me.ScaleLocationListBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.ScaleLocationListBox.Enabled = True
		Me.ScaleLocationListBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.ScaleLocationListBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.ScaleLocationListBox.IntegralHeight = True
		Me.ScaleLocationListBox.Location = New System.Drawing.Point(124, 136)
		Me.ScaleLocationListBox.Name = "ScaleLocationListBox"
		Me.ScaleLocationListBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ScaleLocationListBox.Size = New System.Drawing.Size(109, 20)
		Me.ScaleLocationListBox.Sorted = False
		Me.ScaleLocationListBox.TabIndex = 30
		Me.ScaleLocationListBox.TabStop = True
		Me.ScaleLocationListBox.Visible = True
		' 
		'ScalePathListBox
		' 
		Me.ScalePathListBox.AllowDrop = True
		Me.ScalePathListBox.BackColor = System.Drawing.SystemColors.Window
		Me.ScalePathListBox.CausesValidation = True
		Me.ScalePathListBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.ScalePathListBox.Enabled = True
		Me.ScalePathListBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.ScalePathListBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.ScalePathListBox.IntegralHeight = True
		Me.ScalePathListBox.Location = New System.Drawing.Point(8, 208)
		Me.ScalePathListBox.Name = "ScalePathListBox"
		Me.ScalePathListBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ScalePathListBox.Size = New System.Drawing.Size(109, 20)
		Me.ScalePathListBox.Sorted = False
		Me.ScalePathListBox.TabIndex = 33
		Me.ScalePathListBox.TabStop = True
		Me.ScalePathListBox.Visible = True
		' 
		'ScaleActionListBox
		' 
		Me.ScaleActionListBox.AllowDrop = True
		Me.ScaleActionListBox.BackColor = System.Drawing.SystemColors.Window
		Me.ScaleActionListBox.CausesValidation = True
		Me.ScaleActionListBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.ScaleActionListBox.Enabled = True
		Me.ScaleActionListBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.ScaleActionListBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.ScaleActionListBox.IntegralHeight = True
		Me.ScaleActionListBox.Location = New System.Drawing.Point(8, 28)
		Me.ScaleActionListBox.Name = "ScaleActionListBox"
		Me.ScaleActionListBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ScaleActionListBox.Size = New System.Drawing.Size(109, 20)
		Me.ScaleActionListBox.Sorted = False
		Me.ScaleActionListBox.TabIndex = 22
		Me.ScaleActionListBox.TabStop = True
		Me.ScaleActionListBox.Visible = True
		' 
		'ScaleVisibilityListBox
		' 
		Me.ScaleVisibilityListBox.AllowDrop = True
		Me.ScaleVisibilityListBox.BackColor = System.Drawing.SystemColors.Window
		Me.ScaleVisibilityListBox.CausesValidation = True
		Me.ScaleVisibilityListBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.ScaleVisibilityListBox.Enabled = True
		Me.ScaleVisibilityListBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.ScaleVisibilityListBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.ScaleVisibilityListBox.IntegralHeight = True
		Me.ScaleVisibilityListBox.Location = New System.Drawing.Point(124, 64)
		Me.ScaleVisibilityListBox.Name = "ScaleVisibilityListBox"
		Me.ScaleVisibilityListBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ScaleVisibilityListBox.Size = New System.Drawing.Size(109, 20)
		Me.ScaleVisibilityListBox.Sorted = False
		Me.ScaleVisibilityListBox.TabIndex = 25
		Me.ScaleVisibilityListBox.TabStop = True
		Me.ScaleVisibilityListBox.Visible = True
		' 
		'InitializeScale
		' 
		Me.InitializeScale.AllowDrop = True
		Me.InitializeScale.BackColor = System.Drawing.SystemColors.Control
		Me.InitializeScale.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.InitializeScale.ForeColor = System.Drawing.SystemColors.ControlText
		Me.InitializeScale.Location = New System.Drawing.Point(40, 272)
		Me.InitializeScale.Name = "InitializeScale"
		Me.InitializeScale.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.InitializeScale.Size = New System.Drawing.Size(157, 25)
		Me.InitializeScale.TabIndex = 23
		Me.InitializeScale.Text = "Initialize"
		Me.InitializeScale.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
		Me.InitializeScale.UseVisualStyleBackColor = False
		' 
		'ScalePlacesComboBox
		' 
		Me.ScalePlacesComboBox.AllowDrop = True
		Me.ScalePlacesComboBox.BackColor = System.Drawing.SystemColors.Window
		Me.ScalePlacesComboBox.CausesValidation = True
		Me.ScalePlacesComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
		Me.ScalePlacesComboBox.Enabled = True
		Me.ScalePlacesComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.ScalePlacesComboBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.ScalePlacesComboBox.IntegralHeight = True
		Me.ScalePlacesComboBox.Location = New System.Drawing.Point(124, 248)
		Me.ScalePlacesComboBox.Name = "ScalePlacesComboBox"
		Me.ScalePlacesComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ScalePlacesComboBox.Size = New System.Drawing.Size(109, 20)
		Me.ScalePlacesComboBox.Sorted = False
		Me.ScalePlacesComboBox.TabIndex = 36
		Me.ScalePlacesComboBox.TabStop = True
		Me.ScalePlacesComboBox.Visible = True
		' 
		'ScaleSelectComboBox
		' 
		Me.ScaleSelectComboBox.AllowDrop = True
		Me.ScaleSelectComboBox.BackColor = System.Drawing.SystemColors.Window
		Me.ScaleSelectComboBox.CausesValidation = True
		Me.ScaleSelectComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
		Me.ScaleSelectComboBox.Enabled = True
		Me.ScaleSelectComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.ScaleSelectComboBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.ScaleSelectComboBox.IntegralHeight = True
		Me.ScaleSelectComboBox.Location = New System.Drawing.Point(124, 28)
		Me.ScaleSelectComboBox.Name = "ScaleSelectComboBox"
		Me.ScaleSelectComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ScaleSelectComboBox.Size = New System.Drawing.Size(109, 20)
		Me.ScaleSelectComboBox.Sorted = False
		Me.ScaleSelectComboBox.TabIndex = 24
		Me.ScaleSelectComboBox.TabStop = True
		Me.ScaleSelectComboBox.Text = "ScaleSelectListBox"
		Me.ScaleSelectComboBox.Visible = True
		' 
		'ScalePenListBox
		' 
		Me.ScalePenListBox.AllowDrop = True
		Me.ScalePenListBox.BackColor = System.Drawing.SystemColors.Window
		Me.ScalePenListBox.CausesValidation = True
		Me.ScalePenListBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.ScalePenListBox.Enabled = True
		Me.ScalePenListBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.ScalePenListBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.ScalePenListBox.IntegralHeight = True
		Me.ScalePenListBox.Location = New System.Drawing.Point(8, 136)
		Me.ScalePenListBox.Name = "ScalePenListBox"
		Me.ScalePenListBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ScalePenListBox.Size = New System.Drawing.Size(109, 20)
		Me.ScalePenListBox.Sorted = False
		Me.ScalePenListBox.TabIndex = 28
		Me.ScalePenListBox.TabStop = True
		Me.ScalePenListBox.Visible = True
		' 
		'ScaleDimensionListBox
		' 
		Me.ScaleDimensionListBox.AllowDrop = True
		Me.ScaleDimensionListBox.BackColor = System.Drawing.SystemColors.Window
		Me.ScaleDimensionListBox.CausesValidation = True
		Me.ScaleDimensionListBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.ScaleDimensionListBox.Enabled = True
		Me.ScaleDimensionListBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.ScaleDimensionListBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.ScaleDimensionListBox.IntegralHeight = True
		Me.ScaleDimensionListBox.Location = New System.Drawing.Point(8, 64)
		Me.ScaleDimensionListBox.Name = "ScaleDimensionListBox"
		Me.ScaleDimensionListBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ScaleDimensionListBox.Size = New System.Drawing.Size(109, 20)
		Me.ScaleDimensionListBox.Sorted = False
		Me.ScaleDimensionListBox.TabIndex = 29
		Me.ScaleDimensionListBox.TabStop = True
		Me.ScaleDimensionListBox.Visible = True
		' 
		'ScaleFactorComboBox
		' 
		Me.ScaleFactorComboBox.AllowDrop = True
		Me.ScaleFactorComboBox.BackColor = System.Drawing.SystemColors.Window
		Me.ScaleFactorComboBox.CausesValidation = True
		Me.ScaleFactorComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
		Me.ScaleFactorComboBox.Enabled = True
		Me.ScaleFactorComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.ScaleFactorComboBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.ScaleFactorComboBox.IntegralHeight = True
		Me.ScaleFactorComboBox.Location = New System.Drawing.Point(8, 248)
		Me.ScaleFactorComboBox.Name = "ScaleFactorComboBox"
		Me.ScaleFactorComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ScaleFactorComboBox.Size = New System.Drawing.Size(109, 20)
		Me.ScaleFactorComboBox.Sorted = False
		Me.ScaleFactorComboBox.TabIndex = 35
		Me.ScaleFactorComboBox.TabStop = True
		Me.ScaleFactorComboBox.Visible = True
		' 
		'ScaleGapComboBox
		' 
		Me.ScaleGapComboBox.AllowDrop = True
		Me.ScaleGapComboBox.BackColor = System.Drawing.SystemColors.Window
		Me.ScaleGapComboBox.CausesValidation = True
		Me.ScaleGapComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
		Me.ScaleGapComboBox.Enabled = True
		Me.ScaleGapComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.ScaleGapComboBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.ScaleGapComboBox.IntegralHeight = True
		Me.ScaleGapComboBox.Location = New System.Drawing.Point(124, 208)
		Me.ScaleGapComboBox.Name = "ScaleGapComboBox"
		Me.ScaleGapComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ScaleGapComboBox.Size = New System.Drawing.Size(109, 20)
		Me.ScaleGapComboBox.Sorted = False
		Me.ScaleGapComboBox.TabIndex = 34
		Me.ScaleGapComboBox.TabStop = True
		Me.ScaleGapComboBox.Visible = True
		' 
		'ScalePositionListBox
		' 
		Me.ScalePositionListBox.AllowDrop = True
		Me.ScalePositionListBox.BackColor = System.Drawing.SystemColors.Window
		Me.ScalePositionListBox.CausesValidation = True
		Me.ScalePositionListBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.ScalePositionListBox.Enabled = True
		Me.ScalePositionListBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.ScalePositionListBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.ScalePositionListBox.IntegralHeight = True
		Me.ScalePositionListBox.Location = New System.Drawing.Point(124, 172)
		Me.ScalePositionListBox.Name = "ScalePositionListBox"
		Me.ScalePositionListBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ScalePositionListBox.Size = New System.Drawing.Size(109, 20)
		Me.ScalePositionListBox.Sorted = False
		Me.ScalePositionListBox.TabIndex = 32
		Me.ScalePositionListBox.TabStop = True
		Me.ScalePositionListBox.Visible = True
		' 
		'ScaleViewportComboBox
		' 
		Me.ScaleViewportComboBox.AllowDrop = True
		Me.ScaleViewportComboBox.BackColor = System.Drawing.SystemColors.Window
		Me.ScaleViewportComboBox.CausesValidation = True
		Me.ScaleViewportComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
		Me.ScaleViewportComboBox.Enabled = True
		Me.ScaleViewportComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.ScaleViewportComboBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.ScaleViewportComboBox.IntegralHeight = True
		Me.ScaleViewportComboBox.Location = New System.Drawing.Point(8, 100)
		Me.ScaleViewportComboBox.Name = "ScaleViewportComboBox"
		Me.ScaleViewportComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ScaleViewportComboBox.Size = New System.Drawing.Size(109, 20)
		Me.ScaleViewportComboBox.Sorted = False
		Me.ScaleViewportComboBox.TabIndex = 26
		Me.ScaleViewportComboBox.TabStop = True
		Me.ScaleViewportComboBox.Text = "1"
		Me.ScaleViewportComboBox.Visible = True
		' 
		'ScaleWindowComboBox
		' 
		Me.ScaleWindowComboBox.AllowDrop = True
		Me.ScaleWindowComboBox.BackColor = System.Drawing.SystemColors.Window
		Me.ScaleWindowComboBox.CausesValidation = True
		Me.ScaleWindowComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
		Me.ScaleWindowComboBox.Enabled = True
		Me.ScaleWindowComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.ScaleWindowComboBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.ScaleWindowComboBox.IntegralHeight = True
		Me.ScaleWindowComboBox.Location = New System.Drawing.Point(124, 100)
		Me.ScaleWindowComboBox.Name = "ScaleWindowComboBox"
		Me.ScaleWindowComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ScaleWindowComboBox.Size = New System.Drawing.Size(109, 20)
		Me.ScaleWindowComboBox.Sorted = False
		Me.ScaleWindowComboBox.TabIndex = 27
		Me.ScaleWindowComboBox.TabStop = True
		Me.ScaleWindowComboBox.Text = "1"
		Me.ScaleWindowComboBox.Visible = True
		' 
		'Label27
		' 
		Me.Label27.AllowDrop = True
		Me.Label27.BackColor = System.Drawing.SystemColors.Window
		Me.Label27.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label27.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label27.Location = New System.Drawing.Point(8, 160)
		Me.Label27.Name = "Label27"
		Me.Label27.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label27.Size = New System.Drawing.Size(65, 13)
		Me.Label27.TabIndex = 68
		Me.Label27.Text = "Orientation"
		' 
		'Label28
		' 
		Me.Label28.AllowDrop = True
		Me.Label28.BackColor = System.Drawing.SystemColors.Window
		Me.Label28.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label28.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label28.Location = New System.Drawing.Point(124, 124)
		Me.Label28.Name = "Label28"
		Me.Label28.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label28.Size = New System.Drawing.Size(65, 13)
		Me.Label28.TabIndex = 69
		Me.Label28.Text = "Location"
		' 
		'Label42
		' 
		Me.Label42.AllowDrop = True
		Me.Label42.BackColor = System.Drawing.SystemColors.Window
		Me.Label42.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label42.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label42.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label42.Location = New System.Drawing.Point(8, 196)
		Me.Label42.Name = "Label42"
		Me.Label42.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label42.Size = New System.Drawing.Size(65, 13)
		Me.Label42.TabIndex = 70
		Me.Label42.Text = "Path"
		' 
		'Label34
		' 
		Me.Label34.AllowDrop = True
		Me.Label34.BackColor = System.Drawing.SystemColors.Window
		Me.Label34.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label34.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label34.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label34.Location = New System.Drawing.Point(8, 16)
		Me.Label34.Name = "Label34"
		Me.Label34.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label34.Size = New System.Drawing.Size(65, 13)
		Me.Label34.TabIndex = 66
		Me.Label34.Text = "Action"
		' 
		'Label35
		' 
		Me.Label35.AllowDrop = True
		Me.Label35.BackColor = System.Drawing.SystemColors.Window
		Me.Label35.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label35.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label35.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label35.Location = New System.Drawing.Point(124, 52)
		Me.Label35.Name = "Label35"
		Me.Label35.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label35.Size = New System.Drawing.Size(65, 13)
		Me.Label35.TabIndex = 65
		Me.Label35.Text = "Visibility"
		' 
		'Label33
		' 
		Me.Label33.AllowDrop = True
		Me.Label33.BackColor = System.Drawing.SystemColors.Window
		Me.Label33.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label33.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label33.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label33.Location = New System.Drawing.Point(124, 16)
		Me.Label33.Name = "Label33"
		Me.Label33.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label33.Size = New System.Drawing.Size(89, 13)
		Me.Label33.TabIndex = 64
		Me.Label33.Text = "Select"
		' 
		'Label32
		' 
		Me.Label32.AllowDrop = True
		Me.Label32.BackColor = System.Drawing.SystemColors.Window
		Me.Label32.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label32.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label32.Location = New System.Drawing.Point(8, 124)
		Me.Label32.Name = "Label32"
		Me.Label32.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label32.Size = New System.Drawing.Size(65, 13)
		Me.Label32.TabIndex = 63
		Me.Label32.Text = "Pen"
		' 
		'Label31
		' 
		Me.Label31.AllowDrop = True
		Me.Label31.BackColor = System.Drawing.SystemColors.Window
		Me.Label31.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label31.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label31.Location = New System.Drawing.Point(8, 52)
		Me.Label31.Name = "Label31"
		Me.Label31.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label31.Size = New System.Drawing.Size(65, 13)
		Me.Label31.TabIndex = 62
		Me.Label31.Text = "Dimension"
		' 
		'Label30
		' 
		Me.Label30.AllowDrop = True
		Me.Label30.BackColor = System.Drawing.SystemColors.Window
		Me.Label30.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label30.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label30.Location = New System.Drawing.Point(8, 236)
		Me.Label30.Name = "Label30"
		Me.Label30.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label30.Size = New System.Drawing.Size(65, 13)
		Me.Label30.TabIndex = 61
		Me.Label30.Text = "Factor"
		' 
		'Label29
		' 
		Me.Label29.AllowDrop = True
		Me.Label29.BackColor = System.Drawing.SystemColors.Window
		Me.Label29.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label29.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label29.Location = New System.Drawing.Point(124, 196)
		Me.Label29.Name = "Label29"
		Me.Label29.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label29.Size = New System.Drawing.Size(65, 13)
		Me.Label29.TabIndex = 60
		Me.Label29.Text = "Gap"
		' 
		'Label26
		' 
		Me.Label26.AllowDrop = True
		Me.Label26.BackColor = System.Drawing.SystemColors.Window
		Me.Label26.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label26.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label26.Location = New System.Drawing.Point(124, 236)
		Me.Label26.Name = "Label26"
		Me.Label26.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label26.Size = New System.Drawing.Size(65, 13)
		Me.Label26.TabIndex = 58
		Me.Label26.Text = "Places"
		' 
		'Label25
		' 
		Me.Label25.AllowDrop = True
		Me.Label25.BackColor = System.Drawing.SystemColors.Window
		Me.Label25.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label25.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label25.Location = New System.Drawing.Point(124, 160)
		Me.Label25.Name = "Label25"
		Me.Label25.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label25.Size = New System.Drawing.Size(65, 13)
		Me.Label25.TabIndex = 57
		Me.Label25.Text = "Position"
		' 
		'Label24
		' 
		Me.Label24.AllowDrop = True
		Me.Label24.BackColor = System.Drawing.SystemColors.Window
		Me.Label24.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label24.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label24.Location = New System.Drawing.Point(8, 88)
		Me.Label24.Name = "Label24"
		Me.Label24.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label24.Size = New System.Drawing.Size(65, 13)
		Me.Label24.TabIndex = 56
		Me.Label24.Text = "Viewport"
		' 
		'Label23
		' 
		Me.Label23.AllowDrop = True
		Me.Label23.BackColor = System.Drawing.SystemColors.Window
		Me.Label23.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label23.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label23.Location = New System.Drawing.Point(124, 88)
		Me.Label23.Name = "Label23"
		Me.Label23.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label23.Size = New System.Drawing.Size(65, 13)
		Me.Label23.TabIndex = 55
		Me.Label23.Text = "Window"
		' 
		'Frame10
		' 
		Me.Frame10.AllowDrop = True
		Me.Frame10.BackColor = System.Drawing.SystemColors.Window
		Me.Frame10.Controls.Add(Me.MajorDivVertTextBox)
		Me.Frame10.Controls.Add(Me.MajorDivHorzTextBox)
		Me.Frame10.Controls.Add(Me.MinorDivTextBox)
		Me.Frame10.Controls.Add(Me.Label20)
		Me.Frame10.Controls.Add(Me.Label19)
		Me.Frame10.Controls.Add(Me.Label18)
		Me.Frame10.Enabled = True
		Me.Frame10.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Frame10.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Frame10.Location = New System.Drawing.Point(80, 376)
		Me.Frame10.Name = "Frame10"
		Me.Frame10.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame10.Size = New System.Drawing.Size(93, 77)
		Me.Frame10.TabIndex = 50
		Me.Frame10.Text = "Divisions"
		Me.Frame10.Visible = True
		' 
		'MajorDivVertTextBox
		' 
		Me.MajorDivVertTextBox.AcceptsReturn = True
		Me.MajorDivVertTextBox.AllowDrop = True
		Me.MajorDivVertTextBox.BackColor = System.Drawing.SystemColors.Window
		Me.MajorDivVertTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.MajorDivVertTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.MajorDivVertTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.MajorDivVertTextBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.MajorDivVertTextBox.Location = New System.Drawing.Point(52, 28)
		Me.MajorDivVertTextBox.MaxLength = 0
		Me.MajorDivVertTextBox.Name = "MajorDivVertTextBox"
		Me.MajorDivVertTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.MajorDivVertTextBox.Size = New System.Drawing.Size(33, 19)
		Me.MajorDivVertTextBox.TabIndex = 8
		' 
		'MajorDivHorzTextBox
		' 
		Me.MajorDivHorzTextBox.AcceptsReturn = True
		Me.MajorDivHorzTextBox.AllowDrop = True
		Me.MajorDivHorzTextBox.BackColor = System.Drawing.SystemColors.Window
		Me.MajorDivHorzTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.MajorDivHorzTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.MajorDivHorzTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.MajorDivHorzTextBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.MajorDivHorzTextBox.Location = New System.Drawing.Point(8, 28)
		Me.MajorDivHorzTextBox.MaxLength = 0
		Me.MajorDivHorzTextBox.Name = "MajorDivHorzTextBox"
		Me.MajorDivHorzTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.MajorDivHorzTextBox.Size = New System.Drawing.Size(33, 19)
		Me.MajorDivHorzTextBox.TabIndex = 7
		' 
		'MinorDivTextBox
		' 
		Me.MinorDivTextBox.AcceptsReturn = True
		Me.MinorDivTextBox.AllowDrop = True
		Me.MinorDivTextBox.BackColor = System.Drawing.SystemColors.Window
		Me.MinorDivTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.MinorDivTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.MinorDivTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.MinorDivTextBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.MinorDivTextBox.Location = New System.Drawing.Point(28, 56)
		Me.MinorDivTextBox.MaxLength = 0
		Me.MinorDivTextBox.Name = "MinorDivTextBox"
		Me.MinorDivTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.MinorDivTextBox.Size = New System.Drawing.Size(37, 19)
		Me.MinorDivTextBox.TabIndex = 9
		' 
		'Label20
		' 
		Me.Label20.AllowDrop = True
		Me.Label20.BackColor = System.Drawing.SystemColors.Window
		Me.Label20.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label20.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label20.Location = New System.Drawing.Point(52, 16)
		Me.Label20.Name = "Label20"
		Me.Label20.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label20.Size = New System.Drawing.Size(37, 13)
		Me.Label20.TabIndex = 53
		Me.Label20.Text = "Vert"
		' 
		'Label19
		' 
		Me.Label19.AllowDrop = True
		Me.Label19.BackColor = System.Drawing.SystemColors.Window
		Me.Label19.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label19.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label19.Location = New System.Drawing.Point(8, 16)
		Me.Label19.Name = "Label19"
		Me.Label19.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label19.Size = New System.Drawing.Size(37, 13)
		Me.Label19.TabIndex = 52
		Me.Label19.Text = "Horiz"
		' 
		'Label18
		' 
		Me.Label18.AllowDrop = True
		Me.Label18.BackColor = System.Drawing.SystemColors.Window
		Me.Label18.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label18.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label18.Location = New System.Drawing.Point(28, 44)
		Me.Label18.Name = "Label18"
		Me.Label18.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label18.Size = New System.Drawing.Size(41, 13)
		Me.Label18.TabIndex = 51
		Me.Label18.Text = "Minor"
		' 
		'Frame9
		' 
		Me.Frame9.AllowDrop = True
		Me.Frame9.BackColor = System.Drawing.SystemColors.Window
		Me.Frame9.Controls.Add(Me.MarginLeftTextBox)
		Me.Frame9.Controls.Add(Me.MarginTopTextBox)
		Me.Frame9.Controls.Add(Me.MarginBottomTextBox)
		Me.Frame9.Controls.Add(Me.MarginRightTextBox)
		Me.Frame9.Controls.Add(Me.Label17)
		Me.Frame9.Controls.Add(Me.Label16)
		Me.Frame9.Controls.Add(Me.Label15)
		Me.Frame9.Controls.Add(Me.Label14)
		Me.Frame9.Enabled = True
		Me.Frame9.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Frame9.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Frame9.Location = New System.Drawing.Point(256, 264)
		Me.Frame9.Name = "Frame9"
		Me.Frame9.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame9.Size = New System.Drawing.Size(93, 85)
		Me.Frame9.TabIndex = 38
		Me.Frame9.Text = "Margin"
		Me.Frame9.Visible = True
		' 
		'MarginLeftTextBox
		' 
		Me.MarginLeftTextBox.AcceptsReturn = True
		Me.MarginLeftTextBox.AllowDrop = True
		Me.MarginLeftTextBox.BackColor = System.Drawing.SystemColors.Window
		Me.MarginLeftTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.MarginLeftTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.MarginLeftTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.MarginLeftTextBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.MarginLeftTextBox.Location = New System.Drawing.Point(8, 60)
		Me.MarginLeftTextBox.MaxLength = 0
		Me.MarginLeftTextBox.Name = "MarginLeftTextBox"
		Me.MarginLeftTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.MarginLeftTextBox.Size = New System.Drawing.Size(29, 20)
		Me.MarginLeftTextBox.TabIndex = 2
		' 
		'MarginTopTextBox
		' 
		Me.MarginTopTextBox.AcceptsReturn = True
		Me.MarginTopTextBox.AllowDrop = True
		Me.MarginTopTextBox.BackColor = System.Drawing.SystemColors.Window
		Me.MarginTopTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.MarginTopTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.MarginTopTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.MarginTopTextBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.MarginTopTextBox.Location = New System.Drawing.Point(8, 28)
		Me.MarginTopTextBox.MaxLength = 0
		Me.MarginTopTextBox.Name = "MarginTopTextBox"
		Me.MarginTopTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.MarginTopTextBox.Size = New System.Drawing.Size(29, 20)
		Me.MarginTopTextBox.TabIndex = 0
		' 
		'MarginBottomTextBox
		' 
		Me.MarginBottomTextBox.AcceptsReturn = True
		Me.MarginBottomTextBox.AllowDrop = True
		Me.MarginBottomTextBox.BackColor = System.Drawing.SystemColors.Window
		Me.MarginBottomTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.MarginBottomTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.MarginBottomTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.MarginBottomTextBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.MarginBottomTextBox.Location = New System.Drawing.Point(52, 28)
		Me.MarginBottomTextBox.MaxLength = 0
		Me.MarginBottomTextBox.Name = "MarginBottomTextBox"
		Me.MarginBottomTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.MarginBottomTextBox.Size = New System.Drawing.Size(29, 20)
		Me.MarginBottomTextBox.TabIndex = 1
		' 
		'MarginRightTextBox
		' 
		Me.MarginRightTextBox.AcceptsReturn = True
		Me.MarginRightTextBox.AllowDrop = True
		Me.MarginRightTextBox.BackColor = System.Drawing.SystemColors.Window
		Me.MarginRightTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.MarginRightTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.MarginRightTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.MarginRightTextBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.MarginRightTextBox.Location = New System.Drawing.Point(52, 60)
		Me.MarginRightTextBox.MaxLength = 0
		Me.MarginRightTextBox.Name = "MarginRightTextBox"
		Me.MarginRightTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.MarginRightTextBox.Size = New System.Drawing.Size(29, 20)
		Me.MarginRightTextBox.TabIndex = 3
		' 
		'Label17
		' 
		Me.Label17.AllowDrop = True
		Me.Label17.BackColor = System.Drawing.SystemColors.Window
		Me.Label17.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label17.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label17.Location = New System.Drawing.Point(8, 48)
		Me.Label17.Name = "Label17"
		Me.Label17.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label17.Size = New System.Drawing.Size(33, 13)
		Me.Label17.TabIndex = 49
		Me.Label17.Text = "Left"
		' 
		'Label16
		' 
		Me.Label16.AllowDrop = True
		Me.Label16.BackColor = System.Drawing.SystemColors.Window
		Me.Label16.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label16.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label16.Location = New System.Drawing.Point(52, 48)
		Me.Label16.Name = "Label16"
		Me.Label16.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label16.Size = New System.Drawing.Size(33, 13)
		Me.Label16.TabIndex = 48
		Me.Label16.Text = "Right"
		' 
		'Label15
		' 
		Me.Label15.AllowDrop = True
		Me.Label15.BackColor = System.Drawing.SystemColors.Window
		Me.Label15.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label15.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label15.Location = New System.Drawing.Point(8, 16)
		Me.Label15.Name = "Label15"
		Me.Label15.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label15.Size = New System.Drawing.Size(37, 13)
		Me.Label15.TabIndex = 47
		Me.Label15.Text = "Top"
		' 
		'Label14
		' 
		Me.Label14.AllowDrop = True
		Me.Label14.BackColor = System.Drawing.SystemColors.Window
		Me.Label14.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label14.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label14.Location = New System.Drawing.Point(52, 16)
		Me.Label14.Name = "Label14"
		Me.Label14.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label14.Size = New System.Drawing.Size(33, 13)
		Me.Label14.TabIndex = 46
		Me.Label14.Text = "Botm"
		' 
		'Frame2
		' 
		Me.Frame2.AllowDrop = True
		Me.Frame2.BackColor = System.Drawing.SystemColors.Window
		Me.Frame2.Controls.Add(Me.FrameCheck)
		Me.Frame2.Controls.Add(Me.GridCheck)
		Me.Frame2.Controls.Add(Me.AxesCheck)
		Me.Frame2.Enabled = True
		Me.Frame2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Frame2.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Frame2.Location = New System.Drawing.Point(4, 376)
		Me.Frame2.Name = "Frame2"
		Me.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame2.Size = New System.Drawing.Size(69, 77)
		Me.Frame2.TabIndex = 37
		Me.Frame2.Text = "Graticule"
		Me.Frame2.Visible = True
		' 
		'FrameCheck
		' 
		Me.FrameCheck.AllowDrop = True
		Me.FrameCheck.Appearance = System.Windows.Forms.Appearance.Normal
		Me.FrameCheck.BackColor = System.Drawing.SystemColors.Window
		Me.FrameCheck.CausesValidation = True
		Me.FrameCheck.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.FrameCheck.CheckState = System.Windows.Forms.CheckState.Checked
		Me.FrameCheck.Enabled = True
		Me.FrameCheck.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.FrameCheck.ForeColor = System.Drawing.SystemColors.WindowText
		Me.FrameCheck.Location = New System.Drawing.Point(8, 20)
		Me.FrameCheck.Name = "FrameCheck"
		Me.FrameCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.FrameCheck.Size = New System.Drawing.Size(53, 13)
		Me.FrameCheck.TabIndex = 4
		Me.FrameCheck.TabStop = True
		Me.FrameCheck.Text = "Frame"
		Me.FrameCheck.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.FrameCheck.Visible = True
		' 
		'GridCheck
		' 
		Me.GridCheck.AllowDrop = True
		Me.GridCheck.Appearance = System.Windows.Forms.Appearance.Normal
		Me.GridCheck.BackColor = System.Drawing.SystemColors.Window
		Me.GridCheck.CausesValidation = True
		Me.GridCheck.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.GridCheck.CheckState = System.Windows.Forms.CheckState.Checked
		Me.GridCheck.Enabled = True
		Me.GridCheck.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.GridCheck.ForeColor = System.Drawing.SystemColors.WindowText
		Me.GridCheck.Location = New System.Drawing.Point(8, 52)
		Me.GridCheck.Name = "GridCheck"
		Me.GridCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.GridCheck.Size = New System.Drawing.Size(45, 13)
		Me.GridCheck.TabIndex = 6
		Me.GridCheck.TabStop = True
		Me.GridCheck.Text = "Grid"
		Me.GridCheck.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.GridCheck.Visible = True
		' 
		'AxesCheck
		' 
		Me.AxesCheck.AllowDrop = True
		Me.AxesCheck.Appearance = System.Windows.Forms.Appearance.Normal
		Me.AxesCheck.BackColor = System.Drawing.SystemColors.Window
		Me.AxesCheck.CausesValidation = True
		Me.AxesCheck.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.AxesCheck.CheckState = System.Windows.Forms.CheckState.Checked
		Me.AxesCheck.Enabled = True
		Me.AxesCheck.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.AxesCheck.ForeColor = System.Drawing.SystemColors.WindowText
		Me.AxesCheck.Location = New System.Drawing.Point(8, 36)
		Me.AxesCheck.Name = "AxesCheck"
		Me.AxesCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.AxesCheck.Size = New System.Drawing.Size(49, 13)
		Me.AxesCheck.TabIndex = 5
		Me.AxesCheck.TabStop = True
		Me.AxesCheck.Text = "Axes"
		Me.AxesCheck.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.AxesCheck.Visible = True
		' 
		'RtChart1
		' 
		Me.RtChart1.AutoSize = 0
		Me.RtChart1.AxesColor = 16776960
		Me.RtChart1.AxesTics = 0
		Me.RtChart1.AxesType = 3
		Me.RtChart1.BackColor = 8421376
		Me.RtChart1.BevelInner = 0
		Me.RtChart1.BevelOuter = 2
		Me.RtChart1.BevelWidth_Inner = 1
		Me.RtChart1.BevelWidth_Outer = 1
		Me.RtChart1.BorderColor = (-2147483633)
		Me.RtChart1.BorderWidth = 3
		Me.RtChart1.Chn_Select = 1
		Me.RtChart1.ChnData_Dimension = 1
		Me.RtChart1.ChnDspUserData = 0
		Me.RtChart1.ChnXform_Select = 1
		Me.RtChart1.ColorDepth = 0
		Me.RtChart1.ErrBase = 30200
		Me.RtChart1.Font = New System.Drawing.Font("Arial", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.RtChart1.FrameColor = 16776960
		Me.RtChart1.FrameOn = 0
		Me.RtChart1.FramesPerSec = 0
		Me.RtChart1.FrameTics = 0
		Me.RtChart1.GridColor = 8421504
		Me.RtChart1.GridOn = 0
		Me.RtChart1.GridSymmetry = 0
		Me.RtChart1.GridType = 0
		Me.RtChart1.HitTest = 0
		Me.RtChart1.ImageFile = String.Empty
		Me.RtChart1.LightColor = (-2147483634)
		Me.RtChart1.Location = New System.Drawing.Point(0, 24)
		Me.RtChart1.MajorDivHorz = 10
		Me.RtChart1.MajorDivVert = 8
		Me.RtChart1.MarginBottom = 20
		Me.RtChart1.MarginLeft = 20
		Me.RtChart1.MarginRight = 20
		Me.RtChart1.MarginTop = 20
		Me.RtChart1.MinorDiv = 5
		Me.RtChart1.MouseIcon = 0
		Me.RtChart1.Name = "RtChart1"
		Me.RtChart1.Outline = -1
		Me.RtChart1.OutlineColor = (-2147483642)
		Me.RtChart1.Pen_Select = 1
		Me.RtChart1.RoundedCorners = 0
		Me.RtChart1.Scale_Select = 1
		Me.RtChart1.ShadowColor = (-2147483632)
		Me.RtChart1.Size = New System.Drawing.Size(273, 233)
		Me.RtChart1.TabIndex = 93
		Me.RtChart1.Viewport_Select = 1
		Me.RtChart1.Viewports = 1
		Me.RtChart1.ViewportStorageColor = 12632256
		Me.RtChart1.ViewportStorageOn = 0
		Me.RtChart1.Wnd_Select = 1
		' 
		'ScalesForm
		' 
		Me.AllowDrop = True
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6, 13)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Window
		Me.ClientSize = New System.Drawing.Size(600, 456)
		Me.Controls.Add(Me.Frame4)
		Me.Controls.Add(Me.Frame3)
		Me.Controls.Add(Me.Frame7)
		Me.Controls.Add(Me.Frame1)
		Me.Controls.Add(Me.Frame11)
		Me.Controls.Add(Me.Frame13)
		Me.Controls.Add(Me.Frame12)
		Me.Controls.Add(Me.Frame10)
		Me.Controls.Add(Me.Frame9)
		Me.Controls.Add(Me.Frame2)
		Me.Controls.Add(Me.RtChart1)
		Me.Controls.Add(MainMenu1)
		Me.ForeColor = System.Drawing.SystemColors.WindowText
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Icon = CType(resources.GetObject("ScalesForm.Icon"), System.Drawing.Icon)
		Me.Location = New System.Drawing.Point(84, 160)
		Me.Location = New System.Drawing.Point(81, 126)
		Me.MaximizeBox = False
		Me.MinimizeBox = True
		Me.Name = "ScalesForm"
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Size = New System.Drawing.Size(606, 468)
		Me.Text = "LabOBJX Real Time Chart - Scale & Label Example"
		Me.Frame4.ResumeLayout(False)
		Me.Frame3.ResumeLayout(False)
		Me.Frame5.ResumeLayout(False)
		Me.Frame6.ResumeLayout(False)
		Me.Frame7.ResumeLayout(False)
		Me.Frame1.ResumeLayout(False)
		Me.Frame11.ResumeLayout(False)
		Me.Frame13.ResumeLayout(False)
		Me.Frame12.ResumeLayout(False)
		Me.Frame10.ResumeLayout(False)
		Me.Frame9.ResumeLayout(False)
		Me.Frame2.ResumeLayout(False)
		Me.ResumeLayout(False)
	End Sub
	Sub ReLoadForm(ByVal addEvents As Boolean)
		Form_Load()
		If addEvents Then
			AddHandler MyBase.Closed, AddressOf Me.Form_Closed
		End If
	End Sub
#End Region
End Class