<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ColorForm
#Region "Upgrade Support "
	Private Shared _Instance As ColorForm
	Private Shared _InitializingInstance As Boolean
	Public Shared Property DefInstance() As ColorForm
		Get
			If _Instance Is Nothing OrElse _Instance.IsDisposed Then
				_InitializingInstance = True
				_Instance = CreateInstance()
				_InitializingInstance = False
			End If
			Return _Instance
		End Get
		Set(ByVal value As ColorForm)
			_Instance = Value
		End Set
	End Property
#End Region
#Region "Windows Form Designer generated code "
	Public Shared Function CreateInstance() As ColorForm
		Dim theInstance As New ColorForm()
		Return theInstance
	End Function
	Private visualControls() As String = New String() {"components", "ToolTipMain", "CloseColorForm", "BackGroundColorComboBox", "FrameColorComboBox", "AxesColorComboBox", "GridColorComboBox", "Label11", "Label10", "Label9", "Label8", "Frame8", "BorderColorComboBox", "ShadowColorComboBox", "OutlineColorComboBox", "LightColorComboBox", "Label7", "Label6", "Label5", "Label4", "Frame7"}
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTipMain As System.Windows.Forms.ToolTip
	Public WithEvents CloseColorForm As System.Windows.Forms.Button
	Public WithEvents BackGroundColorComboBox As System.Windows.Forms.ComboBox
	Public WithEvents FrameColorComboBox As System.Windows.Forms.ComboBox
	Public WithEvents AxesColorComboBox As System.Windows.Forms.ComboBox
	Public WithEvents GridColorComboBox As System.Windows.Forms.ComboBox
	Public WithEvents Label11 As System.Windows.Forms.Label
	Public WithEvents Label10 As System.Windows.Forms.Label
	Public WithEvents Label9 As System.Windows.Forms.Label
	Public WithEvents Label8 As System.Windows.Forms.Label
	Public WithEvents Frame8 As System.Windows.Forms.GroupBox
	Public WithEvents BorderColorComboBox As System.Windows.Forms.ComboBox
	Public WithEvents ShadowColorComboBox As System.Windows.Forms.ComboBox
	Public WithEvents OutlineColorComboBox As System.Windows.Forms.ComboBox
	Public WithEvents LightColorComboBox As System.Windows.Forms.ComboBox
	Public WithEvents Label7 As System.Windows.Forms.Label
	Public WithEvents Label6 As System.Windows.Forms.Label
	Public WithEvents Label5 As System.Windows.Forms.Label
	Public WithEvents Label4 As System.Windows.Forms.Label
	Public WithEvents Frame7 As System.Windows.Forms.GroupBox
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> _
	 Private Sub InitializeComponent()
		Me.components = New System.ComponentModel.Container()
		Dim resources As New System.ComponentModel.ComponentResourceManager(GetType(ColorForm))
		Me.ToolTipMain = New System.Windows.Forms.ToolTip(Me.components)
		Me.CloseColorForm = New System.Windows.Forms.Button()
		Me.Frame8 = New System.Windows.Forms.GroupBox()
		Me.BackGroundColorComboBox = New System.Windows.Forms.ComboBox()
		Me.FrameColorComboBox = New System.Windows.Forms.ComboBox()
		Me.AxesColorComboBox = New System.Windows.Forms.ComboBox()
		Me.GridColorComboBox = New System.Windows.Forms.ComboBox()
		Me.Label11 = New System.Windows.Forms.Label()
		Me.Label10 = New System.Windows.Forms.Label()
		Me.Label9 = New System.Windows.Forms.Label()
		Me.Label8 = New System.Windows.Forms.Label()
		Me.Frame7 = New System.Windows.Forms.GroupBox()
		Me.BorderColorComboBox = New System.Windows.Forms.ComboBox()
		Me.ShadowColorComboBox = New System.Windows.Forms.ComboBox()
		Me.OutlineColorComboBox = New System.Windows.Forms.ComboBox()
		Me.LightColorComboBox = New System.Windows.Forms.ComboBox()
		Me.Label7 = New System.Windows.Forms.Label()
		Me.Label6 = New System.Windows.Forms.Label()
		Me.Label5 = New System.Windows.Forms.Label()
		Me.Label4 = New System.Windows.Forms.Label()
		Me.Frame8.SuspendLayout()
		Me.Frame7.SuspendLayout()
		Me.SuspendLayout()
		' 
		'CloseColorForm
		' 
		Me.CloseColorForm.AllowDrop = True
		Me.CloseColorForm.BackColor = System.Drawing.SystemColors.Control
		Me.CloseColorForm.ForeColor = System.Drawing.SystemColors.ControlText
		Me.CloseColorForm.Location = New System.Drawing.Point(100, 184)
		Me.CloseColorForm.Name = "CloseColorForm"
		Me.CloseColorForm.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.CloseColorForm.Size = New System.Drawing.Size(89, 29)
		Me.CloseColorForm.TabIndex = 18
		Me.CloseColorForm.Text = "Close"
		Me.CloseColorForm.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
		Me.CloseColorForm.UseVisualStyleBackColor = False
		' 
		'Frame8
		' 
		Me.Frame8.AllowDrop = True
		Me.Frame8.BackColor = System.Drawing.SystemColors.Window
		Me.Frame8.Controls.Add(Me.BackGroundColorComboBox)
		Me.Frame8.Controls.Add(Me.FrameColorComboBox)
		Me.Frame8.Controls.Add(Me.AxesColorComboBox)
		Me.Frame8.Controls.Add(Me.GridColorComboBox)
		Me.Frame8.Controls.Add(Me.Label11)
		Me.Frame8.Controls.Add(Me.Label10)
		Me.Frame8.Controls.Add(Me.Label9)
		Me.Frame8.Controls.Add(Me.Label8)
		Me.Frame8.Enabled = True
		Me.Frame8.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Frame8.Location = New System.Drawing.Point(148, 4)
		Me.Frame8.Name = "Frame8"
		Me.Frame8.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame8.Size = New System.Drawing.Size(137, 169)
		Me.Frame8.TabIndex = 9
		Me.Frame8.Text = "Graticule Colors"
		Me.Frame8.Visible = True
		' 
		'BackGroundColorComboBox
		' 
		Me.BackGroundColorComboBox.AllowDrop = True
		Me.BackGroundColorComboBox.BackColor = System.Drawing.SystemColors.Window
		Me.BackGroundColorComboBox.CausesValidation = True
		Me.BackGroundColorComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.BackGroundColorComboBox.Enabled = True
		Me.BackGroundColorComboBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.BackGroundColorComboBox.IntegralHeight = True
		Me.BackGroundColorComboBox.Location = New System.Drawing.Point(8, 32)
		Me.BackGroundColorComboBox.Name = "BackGroundColorComboBox"
		Me.BackGroundColorComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.BackGroundColorComboBox.Size = New System.Drawing.Size(121, 20)
		Me.BackGroundColorComboBox.Sorted = False
		Me.BackGroundColorComboBox.TabIndex = 13
		Me.BackGroundColorComboBox.TabStop = True
		Me.BackGroundColorComboBox.Visible = True
		' 
		'FrameColorComboBox
		' 
		Me.FrameColorComboBox.AllowDrop = True
		Me.FrameColorComboBox.BackColor = System.Drawing.SystemColors.Window
		Me.FrameColorComboBox.CausesValidation = True
		Me.FrameColorComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.FrameColorComboBox.Enabled = True
		Me.FrameColorComboBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.FrameColorComboBox.IntegralHeight = True
		Me.FrameColorComboBox.Location = New System.Drawing.Point(8, 69)
		Me.FrameColorComboBox.Name = "FrameColorComboBox"
		Me.FrameColorComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.FrameColorComboBox.Size = New System.Drawing.Size(121, 20)
		Me.FrameColorComboBox.Sorted = False
		Me.FrameColorComboBox.TabIndex = 12
		Me.FrameColorComboBox.TabStop = True
		Me.FrameColorComboBox.Visible = True
		' 
		'AxesColorComboBox
		' 
		Me.AxesColorComboBox.AllowDrop = True
		Me.AxesColorComboBox.BackColor = System.Drawing.SystemColors.Window
		Me.AxesColorComboBox.CausesValidation = True
		Me.AxesColorComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.AxesColorComboBox.Enabled = True
		Me.AxesColorComboBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.AxesColorComboBox.IntegralHeight = True
		Me.AxesColorComboBox.Location = New System.Drawing.Point(8, 106)
		Me.AxesColorComboBox.Name = "AxesColorComboBox"
		Me.AxesColorComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.AxesColorComboBox.Size = New System.Drawing.Size(121, 20)
		Me.AxesColorComboBox.Sorted = False
		Me.AxesColorComboBox.TabIndex = 11
		Me.AxesColorComboBox.TabStop = True
		Me.AxesColorComboBox.Visible = True
		' 
		'GridColorComboBox
		' 
		Me.GridColorComboBox.AllowDrop = True
		Me.GridColorComboBox.BackColor = System.Drawing.SystemColors.Window
		Me.GridColorComboBox.CausesValidation = True
		Me.GridColorComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.GridColorComboBox.Enabled = True
		Me.GridColorComboBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.GridColorComboBox.IntegralHeight = True
		Me.GridColorComboBox.Location = New System.Drawing.Point(8, 143)
		Me.GridColorComboBox.Name = "GridColorComboBox"
		Me.GridColorComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.GridColorComboBox.Size = New System.Drawing.Size(121, 20)
		Me.GridColorComboBox.Sorted = False
		Me.GridColorComboBox.TabIndex = 10
		Me.GridColorComboBox.TabStop = True
		Me.GridColorComboBox.Visible = True
		' 
		'Label11
		' 
		Me.Label11.AllowDrop = True
		Me.Label11.BackColor = System.Drawing.SystemColors.Window
		Me.Label11.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label11.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label11.Location = New System.Drawing.Point(8, 128)
		Me.Label11.Name = "Label11"
		Me.Label11.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label11.Size = New System.Drawing.Size(113, 13)
		Me.Label11.TabIndex = 17
		Me.Label11.Text = "Grid"
		' 
		'Label10
		' 
		Me.Label10.AllowDrop = True
		Me.Label10.BackColor = System.Drawing.SystemColors.Window
		Me.Label10.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label10.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label10.Location = New System.Drawing.Point(8, 92)
		Me.Label10.Name = "Label10"
		Me.Label10.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label10.Size = New System.Drawing.Size(113, 13)
		Me.Label10.TabIndex = 16
		Me.Label10.Text = "Axes"
		' 
		'Label9
		' 
		Me.Label9.AllowDrop = True
		Me.Label9.BackColor = System.Drawing.SystemColors.Window
		Me.Label9.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label9.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label9.Location = New System.Drawing.Point(8, 56)
		Me.Label9.Name = "Label9"
		Me.Label9.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label9.Size = New System.Drawing.Size(113, 13)
		Me.Label9.TabIndex = 15
		Me.Label9.Text = "Frame"
		' 
		'Label8
		' 
		Me.Label8.AllowDrop = True
		Me.Label8.BackColor = System.Drawing.SystemColors.Window
		Me.Label8.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label8.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label8.Location = New System.Drawing.Point(8, 20)
		Me.Label8.Name = "Label8"
		Me.Label8.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label8.Size = New System.Drawing.Size(113, 13)
		Me.Label8.TabIndex = 14
		Me.Label8.Text = "Back Ground"
		' 
		'Frame7
		' 
		Me.Frame7.AllowDrop = True
		Me.Frame7.BackColor = System.Drawing.SystemColors.Window
		Me.Frame7.Controls.Add(Me.BorderColorComboBox)
		Me.Frame7.Controls.Add(Me.ShadowColorComboBox)
		Me.Frame7.Controls.Add(Me.OutlineColorComboBox)
		Me.Frame7.Controls.Add(Me.LightColorComboBox)
		Me.Frame7.Controls.Add(Me.Label7)
		Me.Frame7.Controls.Add(Me.Label6)
		Me.Frame7.Controls.Add(Me.Label5)
		Me.Frame7.Controls.Add(Me.Label4)
		Me.Frame7.Enabled = True
		Me.Frame7.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Frame7.Location = New System.Drawing.Point(4, 4)
		Me.Frame7.Name = "Frame7"
		Me.Frame7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame7.Size = New System.Drawing.Size(137, 169)
		Me.Frame7.TabIndex = 0
		Me.Frame7.Text = "Border Colors"
		Me.Frame7.Visible = True
		' 
		'BorderColorComboBox
		' 
		Me.BorderColorComboBox.AllowDrop = True
		Me.BorderColorComboBox.BackColor = System.Drawing.SystemColors.Window
		Me.BorderColorComboBox.CausesValidation = True
		Me.BorderColorComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.BorderColorComboBox.Enabled = True
		Me.BorderColorComboBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.BorderColorComboBox.IntegralHeight = True
		Me.BorderColorComboBox.Location = New System.Drawing.Point(8, 32)
		Me.BorderColorComboBox.Name = "BorderColorComboBox"
		Me.BorderColorComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.BorderColorComboBox.Size = New System.Drawing.Size(121, 20)
		Me.BorderColorComboBox.Sorted = False
		Me.BorderColorComboBox.TabIndex = 4
		Me.BorderColorComboBox.TabStop = True
		Me.BorderColorComboBox.Visible = True
		' 
		'ShadowColorComboBox
		' 
		Me.ShadowColorComboBox.AllowDrop = True
		Me.ShadowColorComboBox.BackColor = System.Drawing.SystemColors.Window
		Me.ShadowColorComboBox.CausesValidation = True
		Me.ShadowColorComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.ShadowColorComboBox.Enabled = True
		Me.ShadowColorComboBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.ShadowColorComboBox.IntegralHeight = True
		Me.ShadowColorComboBox.Location = New System.Drawing.Point(8, 106)
		Me.ShadowColorComboBox.Name = "ShadowColorComboBox"
		Me.ShadowColorComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShadowColorComboBox.Size = New System.Drawing.Size(121, 20)
		Me.ShadowColorComboBox.Sorted = False
		Me.ShadowColorComboBox.TabIndex = 3
		Me.ShadowColorComboBox.TabStop = True
		Me.ShadowColorComboBox.Visible = True
		' 
		'OutlineColorComboBox
		' 
		Me.OutlineColorComboBox.AllowDrop = True
		Me.OutlineColorComboBox.BackColor = System.Drawing.SystemColors.Window
		Me.OutlineColorComboBox.CausesValidation = True
		Me.OutlineColorComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.OutlineColorComboBox.Enabled = True
		Me.OutlineColorComboBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.OutlineColorComboBox.IntegralHeight = True
		Me.OutlineColorComboBox.Location = New System.Drawing.Point(8, 144)
		Me.OutlineColorComboBox.Name = "OutlineColorComboBox"
		Me.OutlineColorComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.OutlineColorComboBox.Size = New System.Drawing.Size(121, 20)
		Me.OutlineColorComboBox.Sorted = False
		Me.OutlineColorComboBox.TabIndex = 2
		Me.OutlineColorComboBox.TabStop = True
		Me.OutlineColorComboBox.Visible = True
		' 
		'LightColorComboBox
		' 
		Me.LightColorComboBox.AllowDrop = True
		Me.LightColorComboBox.BackColor = System.Drawing.SystemColors.Window
		Me.LightColorComboBox.CausesValidation = True
		Me.LightColorComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.LightColorComboBox.Enabled = True
		Me.LightColorComboBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.LightColorComboBox.IntegralHeight = True
		Me.LightColorComboBox.Location = New System.Drawing.Point(8, 69)
		Me.LightColorComboBox.Name = "LightColorComboBox"
		Me.LightColorComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.LightColorComboBox.Size = New System.Drawing.Size(121, 20)
		Me.LightColorComboBox.Sorted = False
		Me.LightColorComboBox.TabIndex = 1
		Me.LightColorComboBox.TabStop = True
		Me.LightColorComboBox.Visible = True
		' 
		'Label7
		' 
		Me.Label7.AllowDrop = True
		Me.Label7.BackColor = System.Drawing.SystemColors.Window
		Me.Label7.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label7.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label7.Location = New System.Drawing.Point(8, 128)
		Me.Label7.Name = "Label7"
		Me.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label7.Size = New System.Drawing.Size(113, 13)
		Me.Label7.TabIndex = 8
		Me.Label7.Text = "Outline "
		' 
		'Label6
		' 
		Me.Label6.AllowDrop = True
		Me.Label6.BackColor = System.Drawing.SystemColors.Window
		Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label6.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label6.Location = New System.Drawing.Point(8, 92)
		Me.Label6.Name = "Label6"
		Me.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label6.Size = New System.Drawing.Size(113, 13)
		Me.Label6.TabIndex = 7
		Me.Label6.Text = "Shadow"
		' 
		'Label5
		' 
		Me.Label5.AllowDrop = True
		Me.Label5.BackColor = System.Drawing.SystemColors.Window
		Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label5.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label5.Location = New System.Drawing.Point(8, 56)
		Me.Label5.Name = "Label5"
		Me.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label5.Size = New System.Drawing.Size(113, 13)
		Me.Label5.TabIndex = 6
		Me.Label5.Text = "Light"
		' 
		'Label4
		' 
		Me.Label4.AllowDrop = True
		Me.Label4.BackColor = System.Drawing.SystemColors.Window
		Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label4.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label4.Location = New System.Drawing.Point(8, 20)
		Me.Label4.Name = "Label4"
		Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label4.Size = New System.Drawing.Size(113, 13)
		Me.Label4.TabIndex = 5
		Me.Label4.Text = "Border"
		' 
		'ColorForm
		' 
		Me.AllowDrop = True
		Me.AutoScaleDimensions = New System.Drawing.SizeF(7, 15)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Window
		Me.ClientSize = New System.Drawing.Size(289, 220)
		Me.ControlBox = False
		Me.Controls.Add(Me.CloseColorForm)
		Me.Controls.Add(Me.Frame8)
		Me.Controls.Add(Me.Frame7)
		Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.ForeColor = System.Drawing.SystemColors.WindowText
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
		Me.Location = New System.Drawing.Point(217, 199)
		Me.Location = New System.Drawing.Point(214, 177)
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.Name = "ColorForm"
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Size = New System.Drawing.Size(296, 245)
		Me.Text = "Select Colors"
		Me.Frame8.ResumeLayout(False)
		Me.Frame7.ResumeLayout(False)
		Me.ResumeLayout(False)
	End Sub
	Sub ReLoadForm(ByVal addEvents As Boolean)
		If addEvents Then
			AddHandler MyBase.Closed, AddressOf Me.Form_Closed
		End If
	End Sub
#End Region
End Class