Option Strict Off
Option Explicit On
Imports System
Module Program

    Public Const AssemblyTitle As String = "Real Time Strip Chart Star Burst Demo"
    Public Const AssemblyDescription As String = "Real Time Strip Chart Star Burst Demo"
    Public Const AssemblyProduct As String = "Visuals.Real.Time.Chart.StarBurst"


    Public VBDataArrayX(1000) As Single
    Public VBDataArrayY(1000) As Single

    Public seed As Integer
    Public step_Renamed As Integer
    Public numPoints As Integer
End Module