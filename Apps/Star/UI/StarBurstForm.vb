Option Strict Off
Option Explicit On
Imports System
Imports System.Windows.Forms
Imports isr.Visuals.RealTimeChart
Partial Friend Class StarburstForm
    Inherits System.Windows.Forms.Form

    Public Sub New()
        MyBase.New()
        If _Instance Is Nothing Then
            If _InitializingInstance Then
                _Instance = Me
            Else
                Try
                    'For the start-up form, the first instance created is the default instance.
                    If Not (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint Is Nothing) AndAlso System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType Is Me.GetType() Then
                        _Instance = Me
                    End If

                Catch
                End Try
            End If
        End If
        'This call is required by the Windows Form Designer.
        InitializeComponent()
        If Not Me.DesignMode Then ReLoadForm(False)
    End Sub


    Private Sub Form_Load()
        seed = 0
        step_Renamed = scrollSpeed.Value
        lbSpeed.Text = CStr(scrollSpeed.Value)
        lbInnerR.Text = CStr(scrollInnerR.Value)
        lbOuterR.Text = CStr(scrollOuterR.Value)
        lbEdges.Text = CStr(scrollEdge.Value)
    End Sub

    Sub DrawWheel()
        Dim innerx(360) As Double, innery(360) As Double, outerx(360) As Double, outery(360) As Double

        numPoints = 180

        seed += step_Renamed
        If seed >= 360 Then step_Renamed = -scrollSpeed.Value
        If seed <= 0 Then step_Renamed = scrollSpeed.Value
        Dim r1 As Double = seed / (360 / scrollInnerR.Value)
        Dim r2 As Double = seed / (360 / scrollOuterR.Value)
        Dim angle As Integer = seed
        Dim edge As Integer = scrollEdge.Value

        For i As Integer = 0 To edge - 1
            innerx(i) = r1 * Math.Cos((angle + i * 360 / edge) * 3.14 / 180)
            innery(i) = r1 * Math.Sin((angle + i * 360 / edge) * 3.14 / 180)
            outerx(i) = r2 * Math.Cos((angle + i * 360 / edge + 180 / edge) * 3.14 / 180)
            outery(i) = r2 * Math.Sin((angle + i * 360 / edge + 180 / edge) * 3.14 / 180)
        Next
        For j As Double = 0 To 360 / edge / 3
            For i As Integer = 0 To edge - 1
                VBDataArrayX(CInt(j * edge * 3 + 3 * i)) = innerx(i)
                VBDataArrayY(CInt(j * edge * 3 + 3 * i)) = innery(i)
                VBDataArrayX(CInt(j * edge * 3 + 3 * i + 1)) = outerx(i)
                VBDataArrayY(CInt(j * edge * 3 + 3 * i + 1)) = outery(i)
                VBDataArrayX(CInt(j * edge * 3 + 3 * i + 2)) = innerx((i + 1) Mod edge)
                VBDataArrayY(CInt(j * edge * 3 + 3 * i + 2)) = innery((i + 1) Mod edge)
            Next i
        Next j

    End Sub

    Private Sub scrollEdge_ValueChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles scrollEdge.ValueChanged
        lbEdges.Text = CStr(scrollEdge.Value)
    End Sub

    Private Sub scrollInnerR_ValueChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles scrollInnerR.ValueChanged
        lbInnerR.Text = CStr(scrollInnerR.Value)
    End Sub

    Private Sub scrollOuterR_ValueChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles scrollOuterR.ValueChanged
        lbOuterR.Text = CStr(scrollOuterR.Value)
    End Sub

    Private Sub scrollSpeed_ValueChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles scrollSpeed.ValueChanged
        step_Renamed = scrollSpeed.Value
        lbSpeed.Text = CStr(step_Renamed)
    End Sub

    Private Sub Timer1_Tick(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles Timer1.Tick

        Application.DoEvents()
        DrawWheel()
        Dim result As Integer = RtChart.ChartData(1, numPoints, VBDataArrayX, 0, VBDataArrayY, 0)
    End Sub
    Private Sub Form_Closed(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MyBase.Closed
    End Sub
End Class