<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class StarburstForm
#Region "Upgrade Support "
	Private Shared _Instance As StarburstForm
	Private Shared _InitializingInstance As Boolean
	Public Shared Property DefInstance() As StarburstForm
		Get
			If _Instance Is Nothing OrElse _Instance.IsDisposed Then
				_InitializingInstance = True
				_Instance = CreateInstance()
				_InitializingInstance = False
			End If
			Return _Instance
		End Get
		Set(ByVal value As StarburstForm)
			_Instance = Value
		End Set
	End Property
#End Region
#Region "Windows Form Designer generated code "
	Public Shared Function CreateInstance() As StarburstForm
		Dim theInstance As New StarburstForm()
		theInstance.Form_Load()
		Return theInstance
	End Function
	Private visualControls() As String = New String() {"components", "ToolTipMain", "scrollEdge", "scrollOuterR", "scrollInnerR", "scrollSpeed", "Timer1", "lbEdges", "Label5", "lbOuterR", "Label3", "lbInnerR", "Label1", "lbSpeed", "lbSpeedtxt", "RtChart"}
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTipMain As System.Windows.Forms.ToolTip
	Public WithEvents scrollEdge As System.Windows.Forms.HScrollBar
	Public WithEvents scrollOuterR As System.Windows.Forms.HScrollBar
	Public WithEvents scrollInnerR As System.Windows.Forms.HScrollBar
	Public WithEvents scrollSpeed As System.Windows.Forms.HScrollBar
	Public WithEvents Timer1 As System.Windows.Forms.Timer
	Public WithEvents lbEdges As System.Windows.Forms.Label
	Public WithEvents Label5 As System.Windows.Forms.Label
	Public WithEvents lbOuterR As System.Windows.Forms.Label
	Public WithEvents Label3 As System.Windows.Forms.Label
	Public WithEvents lbInnerR As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents lbSpeed As System.Windows.Forms.Label
	Public WithEvents lbSpeedtxt As System.Windows.Forms.Label
	Public WithEvents RtChart As isr.Visuals.RealTimeChart.RealTimeChartControl
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> _
	 Private Sub InitializeComponent()
		Me.components = New System.ComponentModel.Container()
		Dim resources As New System.ComponentModel.ComponentResourceManager(GetType(StarburstForm))
		Me.ToolTipMain = New System.Windows.Forms.ToolTip(Me.components)
		Me.scrollEdge = New System.Windows.Forms.HScrollBar()
		Me.scrollOuterR = New System.Windows.Forms.HScrollBar()
		Me.scrollInnerR = New System.Windows.Forms.HScrollBar()
		Me.scrollSpeed = New System.Windows.Forms.HScrollBar()
		Me.Timer1 = New System.Windows.Forms.Timer(components)
		Me.lbEdges = New System.Windows.Forms.Label()
		Me.Label5 = New System.Windows.Forms.Label()
		Me.lbOuterR = New System.Windows.Forms.Label()
		Me.Label3 = New System.Windows.Forms.Label()
		Me.lbInnerR = New System.Windows.Forms.Label()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.lbSpeed = New System.Windows.Forms.Label()
		Me.lbSpeedtxt = New System.Windows.Forms.Label()
		Me.RtChart = New isr.Visuals.RealTimeChart.RealTimeChartControl()
		Me.SuspendLayout()
		' 
		'scrollEdge
		' 
		Me.scrollEdge.AllowDrop = True
		Me.scrollEdge.CausesValidation = True
		Me.scrollEdge.Enabled = True
		Me.scrollEdge.LargeChange = 1
		Me.scrollEdge.Location = New System.Drawing.Point(272, 312)
		Me.scrollEdge.Maximum = 36
		Me.scrollEdge.Minimum = 1
		Me.scrollEdge.Name = "scrollEdge"
		Me.scrollEdge.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.scrollEdge.Size = New System.Drawing.Size(89, 25)
		Me.scrollEdge.SmallChange = 1
		Me.scrollEdge.TabIndex = 12
		Me.scrollEdge.TabStop = True
		Me.scrollEdge.Value = 20
		Me.scrollEdge.Visible = True
		' 
		'scrollOuterR
		' 
		Me.scrollOuterR.AllowDrop = True
		Me.scrollOuterR.CausesValidation = True
		Me.scrollOuterR.Enabled = True
		Me.scrollOuterR.LargeChange = 1
		Me.scrollOuterR.Location = New System.Drawing.Point(184, 312)
		Me.scrollOuterR.Maximum = 10
		Me.scrollOuterR.Minimum = 1
		Me.scrollOuterR.Name = "scrollOuterR"
		Me.scrollOuterR.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.scrollOuterR.Size = New System.Drawing.Size(89, 25)
		Me.scrollOuterR.SmallChange = 1
		Me.scrollOuterR.TabIndex = 3
		Me.scrollOuterR.TabStop = True
		Me.scrollOuterR.Value = 10
		Me.scrollOuterR.Visible = True
		' 
		'scrollInnerR
		' 
		Me.scrollInnerR.AllowDrop = True
		Me.scrollInnerR.CausesValidation = True
		Me.scrollInnerR.Enabled = True
		Me.scrollInnerR.LargeChange = 1
		Me.scrollInnerR.Location = New System.Drawing.Point(96, 312)
		Me.scrollInnerR.Maximum = 10
		Me.scrollInnerR.Minimum = 1
		Me.scrollInnerR.Name = "scrollInnerR"
		Me.scrollInnerR.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.scrollInnerR.Size = New System.Drawing.Size(89, 25)
		Me.scrollInnerR.SmallChange = 1
		Me.scrollInnerR.TabIndex = 2
		Me.scrollInnerR.TabStop = True
		Me.scrollInnerR.Value = 1
		Me.scrollInnerR.Visible = True
		' 
		'scrollSpeed
		' 
		Me.scrollSpeed.AllowDrop = True
		Me.scrollSpeed.CausesValidation = True
		Me.scrollSpeed.Enabled = True
		Me.scrollSpeed.LargeChange = 1
		Me.scrollSpeed.Location = New System.Drawing.Point(8, 312)
		Me.scrollSpeed.Maximum = 10
		Me.scrollSpeed.Minimum = 1
		Me.scrollSpeed.Name = "scrollSpeed"
		Me.scrollSpeed.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.scrollSpeed.Size = New System.Drawing.Size(89, 25)
		Me.scrollSpeed.SmallChange = 1
		Me.scrollSpeed.TabIndex = 1
		Me.scrollSpeed.TabStop = True
		Me.scrollSpeed.Value = 7
		Me.scrollSpeed.Visible = True
		' 
		'Timer1
		' 
		Me.Timer1.Enabled = True
		Me.Timer1.Interval = 1
		' 
		'lbEdges
		' 
		Me.lbEdges.AllowDrop = True
		Me.lbEdges.AutoSize = True
		Me.lbEdges.BackColor = System.Drawing.SystemColors.Control
		Me.lbEdges.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbEdges.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbEdges.Location = New System.Drawing.Point(312, 344)
		Me.lbEdges.Name = "lbEdges"
		Me.lbEdges.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbEdges.Size = New System.Drawing.Size(33, 13)
		Me.lbEdges.TabIndex = 11
		Me.lbEdges.Text = "Label2"
		' 
		'Label5
		' 
		Me.Label5.AllowDrop = True
		Me.Label5.AutoSize = True
		Me.Label5.BackColor = System.Drawing.SystemColors.Control
		Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label5.Location = New System.Drawing.Point(272, 344)
		Me.Label5.Name = "Label5"
		Me.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label5.Size = New System.Drawing.Size(32, 13)
		Me.Label5.TabIndex = 10
		Me.Label5.Text = "Edges"
		' 
		'lbOuterR
		' 
		Me.lbOuterR.AllowDrop = True
		Me.lbOuterR.AutoSize = True
		Me.lbOuterR.BackColor = System.Drawing.SystemColors.Control
		Me.lbOuterR.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbOuterR.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbOuterR.Location = New System.Drawing.Point(232, 344)
		Me.lbOuterR.Name = "lbOuterR"
		Me.lbOuterR.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbOuterR.Size = New System.Drawing.Size(33, 13)
		Me.lbOuterR.TabIndex = 9
		Me.lbOuterR.Text = "Label2"
		' 
		'Label3
		' 
		Me.Label3.AllowDrop = True
		Me.Label3.AutoSize = True
		Me.Label3.BackColor = System.Drawing.SystemColors.Control
		Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label3.Location = New System.Drawing.Point(184, 344)
		Me.Label3.Name = "Label3"
		Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label3.Size = New System.Drawing.Size(34, 13)
		Me.Label3.TabIndex = 8
		Me.Label3.Text = "OuterR"
		' 
		'lbInnerR
		' 
		Me.lbInnerR.AllowDrop = True
		Me.lbInnerR.AutoSize = True
		Me.lbInnerR.BackColor = System.Drawing.SystemColors.Control
		Me.lbInnerR.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbInnerR.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbInnerR.Location = New System.Drawing.Point(144, 344)
		Me.lbInnerR.Name = "lbInnerR"
		Me.lbInnerR.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbInnerR.Size = New System.Drawing.Size(33, 13)
		Me.lbInnerR.TabIndex = 7
		Me.lbInnerR.Text = "Label2"
		' 
		'Label1
		' 
		Me.Label1.AllowDrop = True
		Me.Label1.AutoSize = True
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Location = New System.Drawing.Point(96, 344)
		Me.Label1.Name = "Label1"
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.Size = New System.Drawing.Size(32, 13)
		Me.Label1.TabIndex = 6
		Me.Label1.Text = "InnerR"
		' 
		'lbSpeed
		' 
		Me.lbSpeed.AllowDrop = True
		Me.lbSpeed.BackColor = System.Drawing.SystemColors.Control
		Me.lbSpeed.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbSpeed.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbSpeed.Location = New System.Drawing.Point(56, 344)
		Me.lbSpeed.Name = "lbSpeed"
		Me.lbSpeed.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbSpeed.Size = New System.Drawing.Size(33, 13)
		Me.lbSpeed.TabIndex = 5
		Me.lbSpeed.Text = "Speed"
		' 
		'lbSpeedtxt
		' 
		Me.lbSpeedtxt.AllowDrop = True
		Me.lbSpeedtxt.AutoSize = True
		Me.lbSpeedtxt.BackColor = System.Drawing.SystemColors.Control
		Me.lbSpeedtxt.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbSpeedtxt.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbSpeedtxt.Location = New System.Drawing.Point(8, 344)
		Me.lbSpeedtxt.Name = "lbSpeedtxt"
		Me.lbSpeedtxt.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbSpeedtxt.Size = New System.Drawing.Size(33, 13)
		Me.lbSpeedtxt.TabIndex = 4
		Me.lbSpeedtxt.Text = "Speed"
		' 
		'RtChart
		' 
		Me.RtChart.AutoSize = 0
		Me.RtChart.AxesColor = 16776960
		Me.RtChart.AxesTics = 0
		Me.RtChart.AxesType = 3
		Me.RtChart.BackColor = 0
		Me.RtChart.BevelInner = 1
		Me.RtChart.BevelOuter = 2
		Me.RtChart.BevelWidth_Inner = 1
		Me.RtChart.BevelWidth_Outer = 1
		Me.RtChart.BorderColor = 16777215
		Me.RtChart.BorderWidth = 0
		Me.RtChart.Chn_Select = 1
		Me.RtChart.ChnData_Dimension = 1
		Me.RtChart.ChnDspUserData = 0
		Me.RtChart.ChnXform_Select = 1
		Me.RtChart.ColorDepth = 0
		Me.RtChart.ErrBase = 30200
		Me.RtChart.Font = New System.Drawing.Font("Arial", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.RtChart.FrameColor = 16776960
		Me.RtChart.FrameOn = 0
		Me.RtChart.FramesPerSec = 0
		Me.RtChart.FrameTics = 0
		Me.RtChart.GridColor = 65280
		Me.RtChart.GridOn = 0
		Me.RtChart.GridSymmetry = 0
		Me.RtChart.GridType = 0
		Me.RtChart.HitTest = 0
		Me.RtChart.ImageFile = String.Empty
		Me.RtChart.LightColor = 8454143
		Me.RtChart.Location = New System.Drawing.Point(8, 8)
		Me.RtChart.MajorDivHorz = 10
		Me.RtChart.MajorDivVert = 8
		Me.RtChart.MarginBottom = 0
		Me.RtChart.MarginLeft = 0
		Me.RtChart.MarginRight = 0
		Me.RtChart.MarginTop = 0
		Me.RtChart.MinorDiv = 5
		Me.RtChart.MouseIcon = 0
		Me.RtChart.Name = "RtChart"
		Me.RtChart.Outline = -1
		Me.RtChart.OutlineColor = (-2147483642)
		Me.RtChart.Pen_Select = 1
		Me.RtChart.RoundedCorners = 0
		Me.RtChart.Scale_Select = 1
		Me.RtChart.ShadowColor = (-2147483632)
		Me.RtChart.Size = New System.Drawing.Size(353, 305)
		Me.RtChart.TabIndex = 0
		Me.RtChart.Viewport_Select = 1
		Me.RtChart.Viewports = 1
		Me.RtChart.ViewportStorageColor = 12632256
		Me.RtChart.ViewportStorageOn = 0
		Me.RtChart.Wnd_Select = 1
		' 
		'formStarburst
		' 
		Me.AllowDrop = True
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6, 13)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.Color.Silver
		Me.ClientSize = New System.Drawing.Size(369, 365)
		Me.Controls.Add(Me.scrollEdge)
		Me.Controls.Add(Me.scrollOuterR)
		Me.Controls.Add(Me.scrollInnerR)
		Me.Controls.Add(Me.scrollSpeed)
		Me.Controls.Add(Me.lbEdges)
		Me.Controls.Add(Me.Label5)
		Me.Controls.Add(Me.lbOuterR)
		Me.Controls.Add(Me.Label3)
		Me.Controls.Add(Me.lbInnerR)
		Me.Controls.Add(Me.Label1)
		Me.Controls.Add(Me.lbSpeed)
		Me.Controls.Add(Me.lbSpeedtxt)
		Me.Controls.Add(Me.RtChart)
		Me.Icon = CType(resources.GetObject("formStarburst.Icon"), System.Drawing.Icon)
		Me.Location = New System.Drawing.Point(50, 100)
		Me.Location = New System.Drawing.Point(47, 81)
		Me.MaximizeBox = True
		Me.MinimizeBox = True
		Me.Name = "formStarburst"
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Size = New System.Drawing.Size(376, 387)
		Me.Text = "LabOBJX Real-Time Chart - Starburst"
		Me.ResumeLayout(False)
	End Sub
	Sub ReLoadForm(ByVal addEvents As Boolean)
		Form_Load()
		If addEvents Then
			AddHandler MyBase.Closed, AddressOf Me.Form_Closed
		End If
	End Sub
#End Region
End Class