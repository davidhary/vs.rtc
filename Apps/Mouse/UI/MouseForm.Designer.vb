<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
#Region "Upgrade Support "
	Private Shared _Instance As Form1
	Private Shared _InitializingInstance As Boolean
	Public Shared Property DefInstance() As Form1
		Get
			If _Instance Is Nothing OrElse _Instance.IsDisposed Then
				_InitializingInstance = True
				_Instance = CreateInstance()
				_InitializingInstance = False
			End If
			Return _Instance
		End Get
		Set(ByVal value As Form1)
			_Instance = Value
		End Set
	End Property
#End Region
#Region "Windows Form Designer generated code "
	Public Shared Function CreateInstance() As Form1
		Dim theInstance As New Form1()
		theInstance.Form_Load()
		Return theInstance
	End Function
	Private visualControls() As String = New String() {"components", "ToolTipMain", "Label2", "Frame2", "CurrentMousePointer", "ChannelOffsetY", "ChannelOffsetX", "ResetChannelOffset", "CursorPosX", "CursorPosY", "_Label1_4", "_Label1_3", "_Label1_2", "_Label1_1", "_Label1_0", "Frame1", "RtChart1", "Label1"}
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTipMain As System.Windows.Forms.ToolTip
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Frame2 As System.Windows.Forms.GroupBox
	Public WithEvents CurrentMousePointer As System.Windows.Forms.TextBox
	Public WithEvents ChannelOffsetY As System.Windows.Forms.TextBox
	Public WithEvents ChannelOffsetX As System.Windows.Forms.TextBox
	Public WithEvents ResetChannelOffset As System.Windows.Forms.Button
	Public WithEvents CursorPosX As System.Windows.Forms.TextBox
	Public WithEvents CursorPosY As System.Windows.Forms.TextBox
	Private WithEvents _Label1_4 As System.Windows.Forms.Label
	Private WithEvents _Label1_3 As System.Windows.Forms.Label
	Private WithEvents _Label1_2 As System.Windows.Forms.Label
	Private WithEvents _Label1_1 As System.Windows.Forms.Label
	Private WithEvents _Label1_0 As System.Windows.Forms.Label
	Public WithEvents Frame1 As System.Windows.Forms.GroupBox
	Public WithEvents RtChart1 As isr.Visuals.RealTimeChart.RealTimeChartControl
	Public Label1(4) As System.Windows.Forms.Label
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> _
	 Private Sub InitializeComponent()
		Me.components = New System.ComponentModel.Container()
		Dim resources As New System.ComponentModel.ComponentResourceManager(GetType(Form1))
		Me.ToolTipMain = New System.Windows.Forms.ToolTip(Me.components)
		Me.Frame2 = New System.Windows.Forms.GroupBox()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.Frame1 = New System.Windows.Forms.GroupBox()
		Me.CurrentMousePointer = New System.Windows.Forms.TextBox()
		Me.ChannelOffsetY = New System.Windows.Forms.TextBox()
		Me.ChannelOffsetX = New System.Windows.Forms.TextBox()
		Me.ResetChannelOffset = New System.Windows.Forms.Button()
		Me.CursorPosX = New System.Windows.Forms.TextBox()
		Me.CursorPosY = New System.Windows.Forms.TextBox()
		Me._Label1_4 = New System.Windows.Forms.Label()
		Me._Label1_3 = New System.Windows.Forms.Label()
		Me._Label1_2 = New System.Windows.Forms.Label()
		Me._Label1_1 = New System.Windows.Forms.Label()
		Me._Label1_0 = New System.Windows.Forms.Label()
		Me.RtChart1 = New isr.Visuals.RealTimeChart.RealTimeChartControl()
		Me.Frame2.SuspendLayout()
		Me.Frame1.SuspendLayout()
		Me.SuspendLayout()
		' 
		'Frame2
		' 
		Me.Frame2.AllowDrop = True
		Me.Frame2.BackColor = System.Drawing.SystemColors.Window
		Me.Frame2.Controls.Add(Me.Label2)
		Me.Frame2.Enabled = True
		Me.Frame2.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Frame2.Location = New System.Drawing.Point(288, 228)
		Me.Frame2.Name = "Frame2"
		Me.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame2.Size = New System.Drawing.Size(73, 157)
		Me.Frame2.TabIndex = 12
		Me.Frame2.Text = "Notes"
		Me.Frame2.Visible = True
		' 
		'Label2
		' 
		Me.Label2.AllowDrop = True
		Me.Label2.BackColor = System.Drawing.SystemColors.Window
		Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label2.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label2.Location = New System.Drawing.Point(4, 20)
		Me.Label2.Name = "Label2"
		Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label2.Size = New System.Drawing.Size(65, 121)
		Me.Label2.TabIndex = 13
		Me.Label2.Text = "Click and drag plot around srceen. "
		' 
		'Frame1
		' 
		Me.Frame1.AllowDrop = True
		Me.Frame1.BackColor = System.Drawing.SystemColors.Window
		Me.Frame1.Controls.Add(Me.CurrentMousePointer)
		Me.Frame1.Controls.Add(Me.ChannelOffsetY)
		Me.Frame1.Controls.Add(Me.ChannelOffsetX)
		Me.Frame1.Controls.Add(Me.ResetChannelOffset)
		Me.Frame1.Controls.Add(Me.CursorPosX)
		Me.Frame1.Controls.Add(Me.CursorPosY)
		Me.Frame1.Controls.Add(Me._Label1_4)
		Me.Frame1.Controls.Add(Me._Label1_3)
		Me.Frame1.Controls.Add(Me._Label1_2)
		Me.Frame1.Controls.Add(Me._Label1_1)
		Me.Frame1.Controls.Add(Me._Label1_0)
		Me.Frame1.Enabled = True
		Me.Frame1.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Frame1.Location = New System.Drawing.Point(12, 228)
		Me.Frame1.Name = "Frame1"
		Me.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame1.Size = New System.Drawing.Size(269, 157)
		Me.Frame1.TabIndex = 0
		Me.Frame1.Text = "Cursors"
		Me.Frame1.Visible = True
		' 
		'CurrentMousePointer
		' 
		Me.CurrentMousePointer.AcceptsReturn = True
		Me.CurrentMousePointer.AllowDrop = True
		Me.CurrentMousePointer.BackColor = System.Drawing.SystemColors.Window
		Me.CurrentMousePointer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.CurrentMousePointer.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.CurrentMousePointer.ForeColor = System.Drawing.SystemColors.WindowText
		Me.CurrentMousePointer.Location = New System.Drawing.Point(12, 127)
		Me.CurrentMousePointer.MaxLength = 0
		Me.CurrentMousePointer.Name = "CurrentMousePointer"
		Me.CurrentMousePointer.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.CurrentMousePointer.Size = New System.Drawing.Size(81, 21)
		Me.CurrentMousePointer.TabIndex = 10
		Me.CurrentMousePointer.Text = "Normal"
		' 
		'ChannelOffsetY
		' 
		Me.ChannelOffsetY.AcceptsReturn = True
		Me.ChannelOffsetY.AllowDrop = True
		Me.ChannelOffsetY.BackColor = System.Drawing.SystemColors.Window
		Me.ChannelOffsetY.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.ChannelOffsetY.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.ChannelOffsetY.ForeColor = System.Drawing.SystemColors.WindowText
		Me.ChannelOffsetY.Location = New System.Drawing.Point(132, 84)
		Me.ChannelOffsetY.MaxLength = 0
		Me.ChannelOffsetY.Name = "ChannelOffsetY"
		Me.ChannelOffsetY.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ChannelOffsetY.Size = New System.Drawing.Size(85, 21)
		Me.ChannelOffsetY.TabIndex = 8
		Me.ChannelOffsetY.Text = "0"
		' 
		'ChannelOffsetX
		' 
		Me.ChannelOffsetX.AcceptsReturn = True
		Me.ChannelOffsetX.AllowDrop = True
		Me.ChannelOffsetX.BackColor = System.Drawing.SystemColors.Window
		Me.ChannelOffsetX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.ChannelOffsetX.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.ChannelOffsetX.ForeColor = System.Drawing.SystemColors.WindowText
		Me.ChannelOffsetX.Location = New System.Drawing.Point(132, 40)
		Me.ChannelOffsetX.MaxLength = 0
		Me.ChannelOffsetX.Name = "ChannelOffsetX"
		Me.ChannelOffsetX.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ChannelOffsetX.Size = New System.Drawing.Size(85, 21)
		Me.ChannelOffsetX.TabIndex = 6
		Me.ChannelOffsetX.Text = "0"
		' 
		'ResetChannelOffset
		' 
		Me.ResetChannelOffset.AllowDrop = True
		Me.ResetChannelOffset.BackColor = System.Drawing.SystemColors.Control
		Me.ResetChannelOffset.ForeColor = System.Drawing.SystemColors.ControlText
		Me.ResetChannelOffset.Location = New System.Drawing.Point(124, 116)
		Me.ResetChannelOffset.Name = "ResetChannelOffset"
		Me.ResetChannelOffset.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ResetChannelOffset.Size = New System.Drawing.Size(97, 29)
		Me.ResetChannelOffset.TabIndex = 3
		Me.ResetChannelOffset.Text = "Reset Offet"
		Me.ResetChannelOffset.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
		Me.ResetChannelOffset.UseVisualStyleBackColor = False
		' 
		'CursorPosX
		' 
		Me.CursorPosX.AcceptsReturn = True
		Me.CursorPosX.AllowDrop = True
		Me.CursorPosX.BackColor = System.Drawing.SystemColors.Window
		Me.CursorPosX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.CursorPosX.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.CursorPosX.ForeColor = System.Drawing.SystemColors.WindowText
		Me.CursorPosX.Location = New System.Drawing.Point(12, 84)
		Me.CursorPosX.MaxLength = 0
		Me.CursorPosX.Name = "CursorPosX"
		Me.CursorPosX.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.CursorPosX.Size = New System.Drawing.Size(81, 21)
		Me.CursorPosX.TabIndex = 2
		Me.CursorPosX.Text = "0"
		' 
		'CursorPosY
		' 
		Me.CursorPosY.AcceptsReturn = True
		Me.CursorPosY.AllowDrop = True
		Me.CursorPosY.BackColor = System.Drawing.SystemColors.Window
		Me.CursorPosY.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.CursorPosY.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.CursorPosY.ForeColor = System.Drawing.SystemColors.WindowText
		Me.CursorPosY.Location = New System.Drawing.Point(12, 40)
		Me.CursorPosY.MaxLength = 0
		Me.CursorPosY.Name = "CursorPosY"
		Me.CursorPosY.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.CursorPosY.Size = New System.Drawing.Size(81, 21)
		Me.CursorPosY.TabIndex = 1
		Me.CursorPosY.Text = "0"
		' 
		'_Label1_4
		' 
		Me._Label1_4.AllowDrop = True
		Me._Label1_4.BackColor = System.Drawing.SystemColors.Window
		Me._Label1_4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label1_4.ForeColor = System.Drawing.SystemColors.WindowText
		Me._Label1_4.Location = New System.Drawing.Point(12, 112)
		Me._Label1_4.Name = "_Label1_4"
		Me._Label1_4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label1_4.Size = New System.Drawing.Size(89, 13)
		Me._Label1_4.TabIndex = 11
		Me._Label1_4.Text = "Mouse Pointer"
		' 
		'_Label1_3
		' 
		Me._Label1_3.AllowDrop = True
		Me._Label1_3.BackColor = System.Drawing.SystemColors.Window
		Me._Label1_3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label1_3.ForeColor = System.Drawing.SystemColors.WindowText
		Me._Label1_3.Location = New System.Drawing.Point(132, 68)
		Me._Label1_3.Name = "_Label1_3"
		Me._Label1_3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label1_3.Size = New System.Drawing.Size(89, 13)
		Me._Label1_3.TabIndex = 9
		Me._Label1_3.Text = "Y Offset"
		' 
		'_Label1_2
		' 
		Me._Label1_2.AllowDrop = True
		Me._Label1_2.BackColor = System.Drawing.SystemColors.Window
		Me._Label1_2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label1_2.ForeColor = System.Drawing.SystemColors.WindowText
		Me._Label1_2.Location = New System.Drawing.Point(132, 24)
		Me._Label1_2.Name = "_Label1_2"
		Me._Label1_2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label1_2.Size = New System.Drawing.Size(89, 13)
		Me._Label1_2.TabIndex = 7
		Me._Label1_2.Text = "X Offset"
		' 
		'_Label1_1
		' 
		Me._Label1_1.AllowDrop = True
		Me._Label1_1.BackColor = System.Drawing.SystemColors.Window
		Me._Label1_1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label1_1.ForeColor = System.Drawing.SystemColors.WindowText
		Me._Label1_1.Location = New System.Drawing.Point(12, 68)
		Me._Label1_1.Name = "_Label1_1"
		Me._Label1_1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label1_1.Size = New System.Drawing.Size(73, 13)
		Me._Label1_1.TabIndex = 5
		Me._Label1_1.Text = "Mouse X"
		' 
		'_Label1_0
		' 
		Me._Label1_0.AllowDrop = True
		Me._Label1_0.BackColor = System.Drawing.SystemColors.Window
		Me._Label1_0.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label1_0.ForeColor = System.Drawing.SystemColors.WindowText
		Me._Label1_0.Location = New System.Drawing.Point(12, 24)
		Me._Label1_0.Name = "_Label1_0"
		Me._Label1_0.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label1_0.Size = New System.Drawing.Size(73, 13)
		Me._Label1_0.TabIndex = 4
		Me._Label1_0.Text = "Mouse Y"
		' 
		'RtChart1
		' 
		Me.RtChart1.AutoSize = 0
		Me.RtChart1.AxesColor = 16776960
		Me.RtChart1.AxesTics = 0
		Me.RtChart1.AxesType = 0
		Me.RtChart1.BackColor = 8421376
		Me.RtChart1.BevelInner = 0
		Me.RtChart1.BevelOuter = 2
		Me.RtChart1.BevelWidth_Inner = 1
		Me.RtChart1.BevelWidth_Outer = 1
		Me.RtChart1.BorderColor = (-2147483633)
		Me.RtChart1.BorderWidth = 3
		Me.RtChart1.Chn_Select = 1
		Me.RtChart1.ChnData_Dimension = 1
		Me.RtChart1.ChnDspUserData = 0
		Me.RtChart1.ChnXform_Select = 1
		Me.RtChart1.ColorDepth = 0
		Me.RtChart1.ErrBase = 30200
		Me.RtChart1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.RtChart1.ForeColor = (-842150451)
		Me.RtChart1.FrameColor = 16776960
		Me.RtChart1.FrameOn = -1
		Me.RtChart1.FramesPerSec = 0
		Me.RtChart1.FrameTics = 0
		Me.RtChart1.GridColor = 8421504
		Me.RtChart1.GridOn = -1
		Me.RtChart1.GridSymmetry = 0
		Me.RtChart1.GridType = 0
		Me.RtChart1.HitTest = 0
		Me.RtChart1.ImageFile = String.Empty
		Me.RtChart1.LightColor = (-2147483634)
		Me.RtChart1.Location = New System.Drawing.Point(8, 0)
		Me.RtChart1.MajorDivHorz = 10
		Me.RtChart1.MajorDivVert = 8
		Me.RtChart1.MarginBottom = 10
		Me.RtChart1.MarginLeft = 10
		Me.RtChart1.MarginRight = 10
		Me.RtChart1.MarginTop = 10
		Me.RtChart1.MinorDiv = 5
		Me.RtChart1.MouseIcon = 0
		Me.RtChart1.Name = "RtChart1"
		Me.RtChart1.Outline = -1
		Me.RtChart1.OutlineColor = (-2147483642)
		Me.RtChart1.Pen_Select = 1
		Me.RtChart1.RoundedCorners = 0
		Me.RtChart1.Scale_Select = 1
		Me.RtChart1.ShadowColor = (-2147483632)
		Me.RtChart1.Size = New System.Drawing.Size(353, 217)
		Me.RtChart1.TabIndex = 14
		Me.RtChart1.Viewport_Select = 1
		Me.RtChart1.Viewports = 1
		Me.RtChart1.ViewportStorageColor = 12632256
		Me.RtChart1.ViewportStorageOn = 0
		Me.RtChart1.Wnd_Select = 1
		' 
		'Form1
		' 
		Me.AllowDrop = True
		Me.AutoScaleDimensions = New System.Drawing.SizeF(7, 12)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Window
		Me.ClientSize = New System.Drawing.Size(367, 392)
		Me.Controls.Add(Me.Frame2)
		Me.Controls.Add(Me.Frame1)
		Me.Controls.Add(Me.RtChart1)
		Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.ForeColor = System.Drawing.SystemColors.WindowText
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Location = New System.Drawing.Point(143, 175)
		Me.Location = New System.Drawing.Point(140, 156)
		Me.MaximizeBox = False
		Me.MinimizeBox = True
		Me.Name = "Form1"
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Size = New System.Drawing.Size(373, 413)
		Me.Text = "LabOBJX Real Time Chart - Mouse Example"
		Me.Frame2.ResumeLayout(False)
		Me.Frame1.ResumeLayout(False)
		Me.ResumeLayout(False)
	End Sub
	Sub ReLoadForm(ByVal addEvents As Boolean)
		InitializeLabel1()
		Form_Load()
		If addEvents Then
			AddHandler MyBase.Closed, AddressOf Me.Form_Closed
		End If
	End Sub
	Sub InitializeLabel1()
		ReDim Label1(4)
		Me.Label1(4) = _Label1_4
		Me.Label1(3) = _Label1_3
		Me.Label1(2) = _Label1_2
		Me.Label1(1) = _Label1_1
		Me.Label1(0) = _Label1_0
	End Sub
#End Region
End Class