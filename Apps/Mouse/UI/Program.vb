Option Strict Off
Option Explicit On
Imports System
Module Program
    ' LabOBJX Real-Time Chart - Example Code
    ' (C) Copyright 1995, Scientific Software Tools, Inc.
    ' All Rights Reserved.
    '
    Public Const AssemblyTitle As String = "Real Time Strip Chart Mouse Demo"
    Public Const AssemblyDescription As String = "Real Time Strip Chart Mouse Demo"
    Public Const AssemblyProduct As String = "Visuals.Real.Time.Chart.Mouse"

    Public result As Integer
    Public Running As Integer
    Public SelectedChannel As Integer ' currently selected data channel

    Public VBDataArrayY As Array ' VB array to store multi-channel voltage data values

    Public PenNormalColor(20) As Integer
    Public PenIntenseColor(20) As Integer

    ' signal waveform limits (volts peak)
    Public Const MaxVolts As Integer = 10
    Public Const MinVolts As Integer = -10

    Public Const NumSamples As Integer = 1000 ' number of samples
    Public Const NumChannels As Integer = 1 ' number of samples
    Public Const VoltsPerDiv As Integer = 5 ' vertical scaling is based on volts per division
    Public Const TimePerDiv As Integer = 100 ' horizontal scaling is based on seconds per division

    Public Const TWO_PIE As Double = 2 * 3.1415926535898

    Public Const MAX_VOLTSDIV As Integer = 50
    Public Const MAX_TIMEDIV As Integer = 1000
    Public Const MAX_SAMPLES As Integer = 30000

    Public Const NORMAL_COLOR As Integer = 1
    Public Const LIGHT_COLOR As Integer = 2
    Public Const DARK_COLOR As Integer = 3

    ' grid constants
    Public Const MAJOR_HORZ_DIV As Integer = 10
    Public Const MAJOR_VERT_DIV As Integer = 8
    Public Const MINOR_DIVISIONS As Integer = 5
End Module