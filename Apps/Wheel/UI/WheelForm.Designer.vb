<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class WheelForm
    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
	Public ToolTipMain As System.Windows.Forms.ToolTip
	Public WithEvents chkAuto As System.Windows.Forms.CheckBox
	Public WithEvents radius1Scroll As System.Windows.Forms.HScrollBar
	Public WithEvents barList As System.Windows.Forms.ComboBox
	Public WithEvents radiusScroll As System.Windows.Forms.HScrollBar
	Public WithEvents speedScroll As System.Windows.Forms.HScrollBar
	Public WithEvents Timer1 As System.Windows.Forms.Timer
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Label3 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents RtChart As isr.Visuals.RealTimeChart.RealTimeChartControl
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> _
	 Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ToolTipMain = New System.Windows.Forms.ToolTip(Me.components)
        Me.chkAuto = New System.Windows.Forms.CheckBox()
        Me.radius1Scroll = New System.Windows.Forms.HScrollBar()
        Me.barList = New System.Windows.Forms.ComboBox()
        Me.radiusScroll = New System.Windows.Forms.HScrollBar()
        Me.speedScroll = New System.Windows.Forms.HScrollBar()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.RtChart = New isr.Visuals.RealTimeChart.RealTimeChartControl()
        Me.SuspendLayout()
        '
        'chkAuto
        '
        Me.chkAuto.AllowDrop = True
        Me.chkAuto.AutoSize = True
        Me.chkAuto.BackColor = System.Drawing.SystemColors.Control
        Me.chkAuto.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkAuto.Location = New System.Drawing.Point(152, 320)
        Me.chkAuto.Name = "chkAuto"
        Me.chkAuto.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkAuto.Size = New System.Drawing.Size(73, 17)
        Me.chkAuto.TabIndex = 8
        Me.chkAuto.Text = "Automatic"
        Me.chkAuto.UseVisualStyleBackColor = False
        '
        'radius1Scroll
        '
        Me.radius1Scroll.AllowDrop = True
        Me.radius1Scroll.LargeChange = 1
        Me.radius1Scroll.Location = New System.Drawing.Point(80, 336)
        Me.radius1Scroll.Minimum = 1
        Me.radius1Scroll.Name = "radius1Scroll"
        Me.radius1Scroll.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.radius1Scroll.Size = New System.Drawing.Size(65, 17)
        Me.radius1Scroll.TabIndex = 7
        Me.radius1Scroll.TabStop = True
        Me.radius1Scroll.Value = 15
        '
        'barList
        '
        Me.barList.AllowDrop = True
        Me.barList.BackColor = System.Drawing.SystemColors.Window
        Me.barList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.barList.ForeColor = System.Drawing.SystemColors.WindowText
        Me.barList.Location = New System.Drawing.Point(248, 320)
        Me.barList.Name = "barList"
        Me.barList.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.barList.Size = New System.Drawing.Size(81, 21)
        Me.barList.TabIndex = 6
        '
        'radiusScroll
        '
        Me.radiusScroll.AllowDrop = True
        Me.radiusScroll.LargeChange = 1
        Me.radiusScroll.Location = New System.Drawing.Point(80, 320)
        Me.radiusScroll.Minimum = 1
        Me.radiusScroll.Name = "radiusScroll"
        Me.radiusScroll.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.radiusScroll.Size = New System.Drawing.Size(65, 17)
        Me.radiusScroll.TabIndex = 3
        Me.radiusScroll.TabStop = True
        Me.radiusScroll.Value = 40
        '
        'speedScroll
        '
        Me.speedScroll.AllowDrop = True
        Me.speedScroll.LargeChange = 1
        Me.speedScroll.Location = New System.Drawing.Point(0, 320)
        Me.speedScroll.Maximum = 15
        Me.speedScroll.Minimum = 1
        Me.speedScroll.Name = "speedScroll"
        Me.speedScroll.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.speedScroll.Size = New System.Drawing.Size(65, 17)
        Me.speedScroll.TabIndex = 1
        Me.speedScroll.TabStop = True
        Me.speedScroll.Value = 7
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 100
        '
        'Label2
        '
        Me.Label2.AllowDrop = True
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.SystemColors.Control
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label2.Location = New System.Drawing.Point(248, 304)
        Me.Label2.Name = "Label2"
        Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label2.Size = New System.Drawing.Size(55, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Parameter"
        '
        'Label3
        '
        Me.Label3.AllowDrop = True
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.SystemColors.Control
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label3.Location = New System.Drawing.Point(80, 304)
        Me.Label3.Name = "Label3"
        Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label3.Size = New System.Drawing.Size(40, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Radius"
        '
        'Label1
        '
        Me.Label1.AllowDrop = True
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.SystemColors.Control
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(0, 304)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label1.Size = New System.Drawing.Size(38, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Speed"
        '
        'RtChart
        '
        Me.RtChart.ChnXformGain = 1.0R
        Me.RtChart.ChnXformLSB = -1
        Me.RtChart.ChnXformMSB = -1
        Me.RtChart.ChnXformOffset = 0R
        Me.RtChart.Dock = System.Windows.Forms.DockStyle.Top
        Me.RtChart.Font = New System.Drawing.Font("Arial", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RtChart.Location = New System.Drawing.Point(0, 0)
        Me.RtChart.Name = "RtChart"
        Me.RtChart.Size = New System.Drawing.Size(356, 297)
        Me.RtChart.TabIndex = 9
        '
        'WheelForm
        '
        Me.AllowDrop = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(356, 362)
        Me.Controls.Add(Me.chkAuto)
        Me.Controls.Add(Me.radius1Scroll)
        Me.Controls.Add(Me.barList)
        Me.Controls.Add(Me.radiusScroll)
        Me.Controls.Add(Me.speedScroll)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.RtChart)
        Me.Location = New System.Drawing.Point(217, 117)
        Me.Name = "WheelForm"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

End Class