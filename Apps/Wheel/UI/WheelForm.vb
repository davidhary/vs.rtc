Option Strict Off
Option Explicit On
Imports System
Imports System.Windows.Forms

Imports isr.Visuals.RealTimeChart
Partial Friend Class WheelForm
    Inherits System.Windows.Forms.Form

    Private ReadOnly _DataArrayX(8000) As Single
    Private ReadOnly _DataArrayY(8000) As Single
    Private ReadOnly _AngleList(10) As Integer
    Private _Rotate As Integer
    Private _Step As Integer
    Private _Step1 As Integer
    Private _NumPoints As Integer

    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub New()
        MyBase.New()
        If _Instance Is Nothing Then
            If _InitializingInstance Then
                _Instance = Me
            Else
                Try
                    'For the start-up form, the first instance created is the default instance.
                    If Not (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint Is Nothing) AndAlso System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType Is Me.GetType() Then
                        _Instance = Me
                    End If

                Catch
                End Try
            End If
        End If
        'This call is required by the Windows Form Designer.
        InitializeComponent()
        If Not Me.DesignMode Then ReLoadForm(False)
    End Sub

#Region "Upgrade Support "
    Private Shared _Instance As WheelForm
    Private Shared _InitializingInstance As Boolean
    Public Shared Property DefInstance() As WheelForm
        Get
            If _Instance Is Nothing OrElse _Instance.IsDisposed Then
                _InitializingInstance = True
                _Instance = CreateInstance()
                _InitializingInstance = False
            End If
            Return _Instance
        End Get
        Set(ByVal value As WheelForm)
            _Instance = value
        End Set
    End Property
    Public Shared Function CreateInstance() As WheelForm
        Dim theInstance As New WheelForm()
        theInstance.Form_Load()
        Return theInstance
    End Function

    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    Public Sub ReLoadForm(ByVal addEvents As Boolean)
        Form_Load()
    End Sub

#End Region

#Region " UNUSED "
#If False Then
        Me.RtChart.AutoSize = 0
        Me.RtChart.AxesColor = 16776960
		Me.RtChart.AxesTics = 0
		Me.RtChart.AxesType = 3
		Me.RtChart.BackColor = 0
		Me.RtChart.BevelInner = 1
		Me.RtChart.BevelOuter = 2
		Me.RtChart.BevelWidth_Inner = 1
		Me.RtChart.BevelWidth_Outer = 1
		Me.RtChart.BorderColor = 16777215
		Me.RtChart.BorderWidth = 0
		Me.RtChart.Chn_Select = 1
		Me.RtChart.ChnData_Dimension = 1
		Me.RtChart.ChnDspUserData = 0
		Me.RtChart.ChnXform_Select = 1
		Me.RtChart.ColorDepth = 0
		Me.RtChart.ErrBase = 30200
        Me.RtChart.FrameColor = 16776960
        Me.RtChart.FrameOn = 0
		Me.RtChart.FramesPerSec = 0
		Me.RtChart.FrameTics = 0
		Me.RtChart.GridColor = 65280
		Me.RtChart.GridOn = 0
		Me.RtChart.GridSymmetry = 0
		Me.RtChart.GridType = 0
		Me.RtChart.HitTest = 0
		Me.RtChart.ImageFile = String.Empty
		Me.RtChart.LightColor = 8454143
        Me.RtChart.MajorDivHorz = 10
        Me.RtChart.MajorDivVert = 8
		Me.RtChart.MarginBottom = 0
		Me.RtChart.MarginLeft = 0
		Me.RtChart.MarginRight = 0
		Me.RtChart.MarginTop = 0
		Me.RtChart.MinorDiv = 5
		Me.RtChart.MouseIcon = 0
        Me.RtChart.Outline = -1
        Me.RtChart.OutlineColor = (-2147483642)
		Me.RtChart.Pen_Select = 1
		Me.RtChart.RoundedCorners = 0
		Me.RtChart.Scale_Select = 1
		Me.RtChart.ShadowColor = (-2147483632)
        Me.RtChart.TabIndex = 0
        Me.RtChart.Viewport_Select = 1
		Me.RtChart.Viewports = 1
		Me.RtChart.ViewportStorageColor = 12632256
		Me.RtChart.ViewportStorageOn = 0
		Me.RtChart.Wnd_Select = 1
#End If
#End Region
    Private Sub Form_Load()
        Me.RtChart.ResetKnownState()
        ' initialize RtChart Pens
        ChartPen.InitChannelsPens(Me.RtChart)

        RtChart.LogicalWindow = 1 ' select window
        'RtChart.WndXmin = -10 ' left minimum abscissa
        'RtChart.WndWidth = 20
        'RtChart.WndHeight = 40
        'RtChart.WndYmin = -20

        RtChart.LogicalChannel = 1 ' select channel
        RtChart.ChnDspWindow = 1 ' assign all channels to window 1
        RtChart.ChnDspViewport = 1 ' assign each channel to a viewport
        RtChart.ChnDspPen = 1 ' specify pen, use same "name" (value) as channel
        'RtChart.ChnDspBufLen = 1000 ' input buffer length for 2D array
        RtChart.ChnDspMode = ChannelDisplayMode.CRT ' per channel display mode, use RunMode constants
        RtChart.ChnDspStyle = ChannelDisplayStyle.Lines ' use line style for plotting arrays
        'RtChart.ChnDspOffsetX = 0 ' set offset to 0
        ' RtChart.ChnDspOffsetY = 0 ' set offset to 0



        'Dim scrollRadius As Double
        'Dim rotation As Integer = 0
        _Step = 1
        _Step1 = 1
        'Dim lbSpeed As Object = ReflectionHelper.GetPrimitiveValue(scrollSpeed)
        'Dim lbRadius As Double = scrollRadius / 10
        barList.Items.Add(CStr(90))
        barList.Items.Add(CStr(60))
        barList.Items.Add(CStr(45))
        barList.Items.Add(CStr(30))
        barList.Items.Add(CStr(22.5))
        barList.Items.Add(CStr(18))
        barList.Items.Add(CStr(15))
        barList.Items.Add(CStr(12))
        barList.Items.Add(CStr(10))
        barList.Items.Add(CStr(6))
        barList.SelectedIndex = 2
    End Sub


    Private Sub RadiusScroll_ValueChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles radiusScroll.ValueChanged
        Dim lbRadius As Integer = radiusScroll.Value
    End Sub

    Private Sub SpeedScroll_ValueChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles speedScroll.ValueChanged
        Dim lbSpeed As Integer = speedScroll.Value
    End Sub

    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub Timer1_Tick(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles Timer1.Tick
        Dim tmpx(1000) As Double
        Dim tmpy(1000) As Single
        Try
            If chkAuto.CheckState Then
                radiusScroll.Value += _Step
                If radiusScroll.Value >= (radiusScroll.Maximum - (radiusScroll.LargeChange + 1)) Then _Step = -_Step
                If radiusScroll.Value <= radiusScroll.Minimum Then _Step = -_Step

                radius1Scroll.Value += _Step1
                If radius1Scroll.Value >= (radius1Scroll.Maximum - (radius1Scroll.LargeChange + 1)) Then _Step1 = -_Step1
                If radius1Scroll.Value <= radius1Scroll.Minimum Then _Step1 = -_Step1
            End If
            Dim gap As Double = Math.Floor(CDbl(barList.Text))
            Dim n As Integer = 1
            _Rotate += speedScroll.Value
            If _Rotate >= 360 Then _Rotate = 0
            _NumPoints = 360 / gap
            For i As Double = 0 To _NumPoints
                tmpx(CInt(i)) = radiusScroll.Value / 10 * Math.Cos((gap * i + _Rotate) * 3.14 / 180.0#)
                tmpy(CInt(i)) = radius1Scroll.Value / 10 * Math.Sin((gap * i + _Rotate) * 3.14 / 180.0#)
            Next
            Dim repeat As Double = 90 / (_NumPoints / 2)
            For j As Double = 0 To repeat
                For i As Double = 0 To _NumPoints / 2 - 1
                    _DataArrayX(CInt(j * 2 * _NumPoints + 4 * i + 0)) = 0
                    _DataArrayY(CInt(j * 2 * _NumPoints + 4 * i + 0)) = 0
                    _DataArrayX(CInt(j * 2 * _NumPoints + 4 * i + 1)) = tmpx(CInt(2 * i * n))
                    _DataArrayY(CInt(j * 2 * _NumPoints + 4 * i + 1)) = tmpy(CInt(2 * i * n))
                    _DataArrayX(CInt(j * 2 * _NumPoints + 4 * i + 2)) = tmpx(CInt(CInt(2 * i * n + 1) Mod _NumPoints))
                    _DataArrayY(CInt(j * 2 * _NumPoints + 4 * i + 2)) = tmpy(CInt(CInt(2 * i * n + 1) Mod _NumPoints))
                    _DataArrayX(CInt(j * 2 * _NumPoints + 4 * i + 3)) = 0
                    _DataArrayY(CInt(j * 2 * _NumPoints + 4 * i + 3)) = 0
                Next
            Next
            Dim result As Object = RtChart.ChartData(1, repeat * 2 * _NumPoints, _DataArrayY, 0, _DataArrayX, 0)

        Catch exc As Exception
            System.Windows.Forms.MessageBox.Show(Me, exc.ToString, "Exception occurred", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class
