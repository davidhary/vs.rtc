' LabOBJX Real-Time Chart - Example Code
' (C) Copyright 1995, Scientific Software Tools, Inc.
' All Rights Reserved.


Option Strict Off
Option Explicit On
Imports System
Imports System.Windows.Forms
Imports isr.Visuals.RealTimeChart

''' <summary> A program. </summary>
''' <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Friend Module Program

    Public Const AssemblyTitle As String = "Real Time Strip Chart 2 Demo"
    Public Const AssemblyDescription As String = "Real Time Strip Chart 2 Demo"
    Public Const AssemblyProduct As String = "Visuals.Real.Time.Chart.StripChart2"

    Public result As Integer
    Public running As Integer

    ' grid constants
    Public Const HorizontalDivisionsCount As Integer = 10
    Public Const VerticalDivisionsCount As Integer = 8
    Public Const MinorDivisionsCount As Integer = 5

    Public Sub InitializeRtChart(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl)

        ' Global operation properties
        rtChart.ImageFile = String.Empty ' specifies file name for Write action
        rtChart.HitTest = False ' enables mouse pointer "hit" detection
        rtChart.FramesPerSec = 0 ' 0 for asynchronous, -1 for synchronous

        ' Border description properties
        rtChart.BorderColor = Color.LightGray
        rtChart.LightColor = Color.DarkGray
        rtChart.ShadowColor = Color.Black
        rtChart.BevelInner = BevelStyle.None
        rtChart.BevelOuter = BevelStyle.None
        rtChart.BevelWidthInner = 1
        rtChart.BevelWidthOuter = 1
        rtChart.BorderWidth = 3
        rtChart.Outline = True
        rtChart.OutlineColor = Color.Black
        rtChart.RoundedCorners = False

        ' Graticule description properties
        rtChart.BackColor = Color.DarkCyan
        rtChart.AutoSize = False ' false allows non-square border, grid is always square

        rtChart.FrameOn = True
        rtChart.FrameTics = FrameTicsStyle.MajorMinorFrameTics
        rtChart.FrameColor = Color.White

        rtChart.AxesType = AxesType.HorizontalVertical
        rtChart.AxesTics = AxesTicsStyle.MajorMinorAxesTics
        rtChart.AxesColor = Color.White

        rtChart.GridOn = True
        rtChart.GridType = GridType.HorizontalVertical
        rtChart.GridColor = Color.LightGray

        rtChart.MajorDivHorz = HorizontalDivisionsCount ' major horizontal divisions
        rtChart.MajorDivVert = VerticalDivisionsCount ' major vertical divisions
        rtChart.MinorDiv = MinorDivisionsCount ' minor divisions
        rtChart.MarginLeft = 1 ' left Graticule border
        rtChart.MarginTop = 1 ' top Graticule border
        rtChart.MarginRight = 1 ' right Graticule border
        rtChart.MarginBottom = 30 ' bottom Graticule border

        rtChart.ColorDepth = ColorDepth.Color16 '  256 palette type adapter only windows_g

    End Sub

    Public Sub InitializeScrollChannels(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl)

        ' Describe Channel
        rtChart.LogicalChannel = 1 ' select channel
        rtChart.ChnDspViewport = 1 ' assign view port to channel
        rtChart.ChnDspWindow = 1 ' assign window to channel
        rtChart.ChnDspPen = ChannelName.Cursor1 ' specify pen, same value as channel name
        rtChart.ChnDspBufLen = 100 ' total input buffer length
        rtChart.ChnDspMode = ChannelDisplayMode.ScrollRight ' per channel display mode
        rtChart.ChnDspStyle = ChannelDisplayStyle.Lines ' use point style for scroll plotting
        rtChart.ChnDspOffsetX = 0
        rtChart.ChnDspOffsetY = 0

        ' Describe vertical dimension (and, implicitly, horizontal)
        rtChart.ChannelDataDimension = LogicalDimension.Vertical
        rtChart.ChnDataShape = ChannelDataShape.Pair
        rtChart.ChnDataType = ChannelDataType.Single
        rtChart.ChnDataOffset = 0 ' offset into buffer (in points)
        rtChart.ChnDataIncr = 1 ' logical increment to next value for 2D array

        rtChart.ChnDspAction = ChannelDisplayAction.Initialize

    End Sub

    Public Sub InitializeViewPorts(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl)

        ' View port Description
        rtChart.Viewports = 1 ' number of view ports to be initialized
        rtChart.ViewportStorageColor = Color.Green ' storage mode plot color, same for all view ports

    End Sub

    Public Sub InitializeWindows(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl)

        rtChart.LogicalWindow = 1 ' select window
        rtChart.WndXmin = 0 ' left minimum abscissa
        rtChart.WndWidth = 10 * HorizontalDivisionsCount ' in Seconds (Time/div * 10 divisions)
        rtChart.WndHeight = 1 * VerticalDivisionsCount ' in Volts (Volts/div * 8 divisions)
        rtChart.WndYmin = rtChart.WndHeight / -2 ' bottom minimum ordinate

        ' initialize horizontal scale
        rtChart.LogicalScale = 1 ' select scale name (same for both dimensions)
        rtChart.ScaleViewport = 1 ' specify view port before selecting dimension
        rtChart.ScaleWindow = 1 ' specify window to be scaled before selecting dimension

        rtChart.ScaleDimension = LogicalDimension.Horizontal ' select horizontal or vertical scale/label
        rtChart.ScalePen = PenName.White ' specify pen color for scale

        rtChart.ScaleLocation = ScaleLocation.RightBottom ' Scale Location: axis or frame
        rtChart.ScalePosition = Position.BelowRight ' Scale Position: above or below ScaleLocation

        rtChart.ScaleOrientation = Orientation.Parallel ' Scale Orientation: Parallel or Perpendicular to ScaleDimension
        rtChart.ScalePath = Path.Right ' Scale Path: direction which characters follow: left. right, up, down
        rtChart.ScaleGap = 1 ' Scale Gap: space between ScaleLocation and scale/label in pixels

        rtChart.ScaleFactor = 1 ' multiplier used to convert window units to scale values
        rtChart.ScalePlaces = 0 ' number of significant digits to the right of decimal point

        ' update RtChart Label properties
        rtChart.LabelCaption = "Seconds" ' specify Label Caption text: "volts", "secs" , etc.
        rtChart.LabelPen = PenName.White ' specify pen color for Label

        rtChart.LabelLocation = LabelLocation.Center ' label location relative to scale: left, right, center
        rtChart.LabelPosition = Position.BelowRight ' label Position: above, below, or on LabelLocation
        rtChart.LabelOrientation = Orientation.Parallel ' label character orientation relative to ScaleDimension
        rtChart.LabelPath = Path.Right ' Scale Path: direction which characters follow

        rtChart.FontBold = False
        rtChart.FontItalic = False
        rtChart.FontName = "Arial"
        rtChart.FontSize = 6
        rtChart.FontStrikeout = False
        rtChart.FontUnderline = False

        rtChart.ScaleVisibility = ScaleVisibility.ScaleAndLabel ' scale visibility: none, scale, label, or both

        rtChart.ScaleAction = ScaleAction.Initialize ' execute selected Scale Action

    End Sub

    Public DataPoint As New PointF()

    Public SamplingInterval As Integer

    Public Sub PlotDataScroll(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl)

        Dim winWidth As Single = rtChart.WndWidth
        Do While running
            result = rtChart.ChartData(1, 1, DataPoint, 0, 0)
            If result >= 0 Then
                rtChart.LogicalWindow = 1 ' select window
                rtChart.WndXmin = DataPoint.X - winWidth ' left minimum abscissa
                rtChart.ScaleAction = ScaleAction.ChangeWindow ' update scale
            End If
            DataPoint.X += 1 ' update horizontal coordinate
            DataPoint.Y = CInt(DataPoint.Y + 1) Mod 4 ' update vertical coordinate
            Application.DoEvents() ' windows must have control to process paint, mouse and keyboard events
            Dim sw As Stopwatch = Stopwatch.StartNew
            Do While sw.Elapsed < TimeSpan.FromMilliseconds(Program.SamplingInterval)
                Application.DoEvents()
            Loop
            If Not running Then Exit Do
        Loop

    End Sub
End Module