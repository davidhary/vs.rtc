' LabOBJX Real-Time Chart - Example Code
' (C) Copyright 1995, Scientific Software Tools, Inc.
' All Rights Reserved.

Option Strict Off
Option Explicit On
Imports System
Imports System.Windows.Forms
Imports isr.Visuals.RealTimeChart

''' <summary> Form for viewing the strip chart. </summary>
''' <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 6/29/2019 </para></remarks>
Partial Friend Class StripChartForm
    Inherits System.Windows.Forms.Form

#Region " CONSTRUCTION and CLEANUP "

    Private ReadOnly _IsInitializingComponent As Boolean
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub New()
        MyBase.New()
        If _Instance Is Nothing Then
            If _InitializingInstance Then
                _Instance = Me
            Else
                Try
                    'For the start-up form, the first instance created is the default instance.
                    If Not (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint Is Nothing) AndAlso System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType Is Me.GetType() Then
                        _Instance = Me
                    End If

                Catch
                End Try
            End If
        End If
        'This call is required by the Windows Form Designer.
        _IsInitializingComponent = True
        InitializeComponent()
        _IsInitializingComponent = False
        If Not Me.DesignMode Then ReLoadForm(False)
    End Sub

    Private Shared _Instance As StripChartForm
    Private Shared _InitializingInstance As Boolean
    Public Shared Property DefInstance() As StripChartForm
        Get
            If _Instance Is Nothing OrElse _Instance.IsDisposed Then
                _InitializingInstance = True
                _Instance = CreateInstance()
                _InitializingInstance = False
            End If
            Return _Instance
        End Get
        Set(ByVal value As StripChartForm)
            _Instance = value
        End Set
    End Property

    Public Shared Function CreateInstance() As StripChartForm
        Dim theInstance As New StripChartForm()
        theInstance.Form_Load()
        Return theInstance
    End Function

#End Region

#Region " INITIALIZATION "

    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    Public Sub ReLoadForm(ByVal addEvents As Boolean)
        Form_Load()
    End Sub

    Private Sub Form_Load()
        Show()
        ' center form
        Me.SetBounds((Screen.PrimaryScreen.Bounds.Width - Me.Width) / 2, (Screen.PrimaryScreen.Bounds.Height - Me.Height) / 2, 0, 0, BoundsSpecified.X Or BoundsSpecified.Y)

        ' initialize program's global variables
        InitializeGlobalVariables()

        ' initialize RtChart Pens
        ChartPen.InitChannelsPens(_Chart)

        ' initialize RtChart global properties
        Program.InitializeRtChart(_Chart)

        ' initialize RtChart ViewPort
        Program.InitializeViewPorts(_Chart)

        ' initialize RtChart Windows
        Program.InitializeWindows(_Chart)

        Program.InitializeScrollChannels(_Chart)

    End Sub

#End Region

#Region " CONTROL EVENTS "


    Private ReadOnly _I As Integer ' index in for/next loops
    Private ReadOnly _Result As Integer ' return value from function call
    Private ReadOnly _Channel As Integer

    Private ReadOnly _TempValue As Integer ' temporary storage variable
    Private ReadOnly _SampleTime As Single ' calculated display time interval

    Private Sub AxesCheckBox_CheckStateChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _AxesCheckBox.CheckStateChanged
        If _IsInitializingComponent Then Return
        If _AxesCheckBox.CheckState Then
            _Chart.AxesType = AxesType.HorizontalVertical
        Else
            _Chart.AxesType = AxesType.NoAxes
        End If
    End Sub

    Private Sub FrameCheckBox_CheckStateChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _FrameCheckBox.CheckStateChanged
        If _IsInitializingComponent Then Return
        _Chart.FrameOn = (_FrameCheckBox.CheckState)
    End Sub

    Private Sub GridCheckBox_CheckStateChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _GridCheckBox.CheckStateChanged
        If _IsInitializingComponent Then Return
        _Chart.GridOn = (_GridCheckBox.CheckState)
    End Sub

    Private Sub ScaleCheckBox_CheckStateChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _ScaleCheckBox.CheckStateChanged
        If _IsInitializingComponent Then Return
        _Chart.LogicalScale = (1)
        If _ScaleCheckBox.CheckState Then
            _Chart.ScaleVisibility = ScaleVisibility.ScaleAndLabel
        Else
            _Chart.ScaleVisibility = ScaleVisibility.Hidden
        End If
        _Chart.ScaleAction = ((ScaleAction.Initialize)) ' execute selected Scale Action
    End Sub

    Private Shared Sub InitializeGlobalVariables()

        'initialize variables
        running = False
        DataPoint.X = 0
        DataPoint.Y = 0

    End Sub

    Private Sub StartButton_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _StartButton.Click

        If Not Program.Running Then
            Program.Running = True
            _StartButton.Text = "Stop"

            ' call scroll plotting routine
            PlotDataScroll(_Chart)

        Else
            Program.Running = False
            _StartButton.Text = "Scroll"

        End If

    End Sub

    Private Sub SamplingIntervalNumeric_ValueChanged(sender As Object, e As EventArgs) Handles _SamplingIntervalNumeric.ValueChanged
        Program.SamplingInterval = CInt(_SamplingIntervalNumeric.Value)
    End Sub

#End Region

End Class

