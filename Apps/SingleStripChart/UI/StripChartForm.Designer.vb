<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class StripChartForm

#Region "Windows Form Designer generated code "

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Private _ToolTip As System.Windows.Forms.ToolTip
    Private WithEvents _ScaleCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _AxesCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _GridCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _FrameCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _StartButton As System.Windows.Forms.Button
    Private WithEvents _Chart As isr.Visuals.RealTimeChart.RealTimeChartControl
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(StripChartForm))
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._ScaleCheckBox = New System.Windows.Forms.CheckBox()
        Me._AxesCheckBox = New System.Windows.Forms.CheckBox()
        Me._GridCheckBox = New System.Windows.Forms.CheckBox()
        Me._FrameCheckBox = New System.Windows.Forms.CheckBox()
        Me._StartButton = New System.Windows.Forms.Button()
        Me._Chart = New isr.Visuals.RealTimeChart.RealTimeChartControl()
        Me._SamplingIntervalNumeric = New System.Windows.Forms.NumericUpDown()
        Me._SamplingIntervalNumericLabel = New System.Windows.Forms.Label()
        Me._SamplingIntervalNumericUnitsLabel = New System.Windows.Forms.Label()
        CType(Me._SamplingIntervalNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_ScaleCheckBox
        '
        Me._ScaleCheckBox.AllowDrop = True
        Me._ScaleCheckBox.AutoSize = True
        Me._ScaleCheckBox.BackColor = System.Drawing.SystemColors.Control
        Me._ScaleCheckBox.Checked = True
        Me._ScaleCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me._ScaleCheckBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me._ScaleCheckBox.Location = New System.Drawing.Point(187, 320)
        Me._ScaleCheckBox.Name = "_ScaleCheckBox"
        Me._ScaleCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ScaleCheckBox.Size = New System.Drawing.Size(58, 17)
        Me._ScaleCheckBox.TabIndex = 4
        Me._ScaleCheckBox.Text = "Scale"
        Me._ScaleCheckBox.UseVisualStyleBackColor = False
        '
        '_AxesCheckBox
        '
        Me._AxesCheckBox.AllowDrop = True
        Me._AxesCheckBox.AutoSize = True
        Me._AxesCheckBox.BackColor = System.Drawing.SystemColors.Control
        Me._AxesCheckBox.Checked = True
        Me._AxesCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me._AxesCheckBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me._AxesCheckBox.Location = New System.Drawing.Point(128, 320)
        Me._AxesCheckBox.Name = "_AxesCheckBox"
        Me._AxesCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._AxesCheckBox.Size = New System.Drawing.Size(53, 17)
        Me._AxesCheckBox.TabIndex = 3
        Me._AxesCheckBox.Text = "Axes"
        Me._AxesCheckBox.UseVisualStyleBackColor = False
        '
        '_GridCheckBox
        '
        Me._GridCheckBox.AllowDrop = True
        Me._GridCheckBox.AutoSize = True
        Me._GridCheckBox.BackColor = System.Drawing.SystemColors.Control
        Me._GridCheckBox.Checked = True
        Me._GridCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me._GridCheckBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me._GridCheckBox.Location = New System.Drawing.Point(75, 320)
        Me._GridCheckBox.Name = "_GridCheckBox"
        Me._GridCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._GridCheckBox.Size = New System.Drawing.Size(49, 17)
        Me._GridCheckBox.TabIndex = 2
        Me._GridCheckBox.Text = "Grid"
        Me._GridCheckBox.UseVisualStyleBackColor = False
        '
        '_FrameCheckBox
        '
        Me._FrameCheckBox.AllowDrop = True
        Me._FrameCheckBox.AutoSize = True
        Me._FrameCheckBox.BackColor = System.Drawing.SystemColors.Control
        Me._FrameCheckBox.Checked = True
        Me._FrameCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me._FrameCheckBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me._FrameCheckBox.Location = New System.Drawing.Point(11, 320)
        Me._FrameCheckBox.Name = "_FrameCheckBox"
        Me._FrameCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._FrameCheckBox.Size = New System.Drawing.Size(60, 17)
        Me._FrameCheckBox.TabIndex = 1
        Me._FrameCheckBox.Text = "Frame"
        Me._FrameCheckBox.UseVisualStyleBackColor = False
        '
        '_StartButton
        '
        Me._StartButton.AllowDrop = True
        Me._StartButton.BackColor = System.Drawing.SystemColors.Control
        Me._StartButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._StartButton.Location = New System.Drawing.Point(407, 316)
        Me._StartButton.Name = "_StartButton"
        Me._StartButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._StartButton.Size = New System.Drawing.Size(89, 25)
        Me._StartButton.TabIndex = 0
        Me._StartButton.Text = "Run"
        Me._StartButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me._StartButton.UseVisualStyleBackColor = False
        '
        '_Chart
        '
        Me._Chart.Location = New System.Drawing.Point(8, 8)
        Me._Chart.Name = "_Chart"
        Me._Chart.Size = New System.Drawing.Size(489, 297)
        Me._Chart.TabIndex = 5
        '
        '_SamplingIntervalNumeric
        '
        Me._SamplingIntervalNumeric.Location = New System.Drawing.Point(310, 318)
        Me._SamplingIntervalNumeric.Name = "_SamplingIntervalNumeric"
        Me._SamplingIntervalNumeric.Size = New System.Drawing.Size(48, 20)
        Me._SamplingIntervalNumeric.TabIndex = 6
        Me._SamplingIntervalNumeric.Value = New Decimal(New Integer() {10, 0, 0, 0})
        '
        '_SamplingIntervalNumericLabel
        '
        Me._SamplingIntervalNumericLabel.AutoSize = True
        Me._SamplingIntervalNumericLabel.Location = New System.Drawing.Point(253, 321)
        Me._SamplingIntervalNumericLabel.Name = "_SamplingIntervalNumericLabel"
        Me._SamplingIntervalNumericLabel.Size = New System.Drawing.Size(54, 13)
        Me._SamplingIntervalNumericLabel.TabIndex = 7
        Me._SamplingIntervalNumericLabel.Text = "Interval:"
        Me._SamplingIntervalNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_SamplingIntervalNumericUnitsLabel
        '
        Me._SamplingIntervalNumericUnitsLabel.AutoSize = True
        Me._SamplingIntervalNumericUnitsLabel.Location = New System.Drawing.Point(361, 321)
        Me._SamplingIntervalNumericUnitsLabel.Name = "_SamplingIntervalNumericUnitsLabel"
        Me._SamplingIntervalNumericUnitsLabel.Size = New System.Drawing.Size(22, 13)
        Me._SamplingIntervalNumericUnitsLabel.TabIndex = 8
        Me._SamplingIntervalNumericUnitsLabel.Text = "ms"
        '
        'StripChartForm
        '
        Me.AllowDrop = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(508, 348)
        Me.Controls.Add(Me._SamplingIntervalNumericUnitsLabel)
        Me.Controls.Add(Me._SamplingIntervalNumericLabel)
        Me.Controls.Add(Me._SamplingIntervalNumeric)
        Me.Controls.Add(Me._ScaleCheckBox)
        Me.Controls.Add(Me._AxesCheckBox)
        Me.Controls.Add(Me._GridCheckBox)
        Me.Controls.Add(Me._FrameCheckBox)
        Me.Controls.Add(Me._StartButton)
        Me.Controls.Add(Me._Chart)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.SystemColors.WindowText
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(49, 106)
        Me.MaximizeBox = False
        Me.Name = "StripChartForm"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "LabOBJX Real-Time Chart - Strip Chart Example"
        CType(Me._SamplingIntervalNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Private WithEvents _SamplingIntervalNumeric As NumericUpDown
    Private WithEvents _SamplingIntervalNumericLabel As Label
    Private WithEvents _SamplingIntervalNumericUnitsLabel As Label
#End Region
End Class