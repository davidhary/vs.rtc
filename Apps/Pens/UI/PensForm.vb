Option Strict Off
Option Explicit On
Imports System
Imports System.Windows.Forms

Imports isr.Visuals.RealTimeChart
Partial Friend Class Form1
	Inherits System.Windows.Forms.Form

	' LabOBJX Real-Time Chart - Example Code
	' (C) Copyright 1995, Scientific Software Tools, Inc.
	' All Rights Reserved.
	'

	Dim VBDataArrayY(, ) As Single ' VB array to store multi-channel voltage data values

	' signal waveform volts peak
	Const VOLTS_PEAK As Integer = 10
	Const TWO_PIE As Double = 2# * 3.1415926535898
	Const NUM_SAMPLES As Integer = 100
	Public Sub New()
		MyBase.New()
		If _Instance Is Nothing Then
			If _InitializingInstance Then
				_Instance = Me
			Else
				Try
					'For the start-up form, the first instance created is the default instance.
					If Not (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint Is Nothing) AndAlso System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType Is Me.GetType() Then
						_Instance = Me
					End If

				Catch
				End Try
			End If
		End If
		'This call is required by the Windows Form Designer.
		InitializeComponent()
		ReLoadForm(False)
	End Sub






	Private Sub ChannelPenCombo_SelectedIndexChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _ChannelPenCombo_4.SelectedIndexChanged, _ChannelPenCombo_3.SelectedIndexChanged, _ChannelPenCombo_2.SelectedIndexChanged, _ChannelPenCombo_1.SelectedIndexChanged
		Dim Index As Integer = Array.IndexOf(Me.ChannelPenCombo, eventSender)
		Dim RtChart_ChnChangePen As Object

		' list control array indices are set at design-time to equal channels' number

		' select Channel's pen
		RtChart1.LogicalChannel = (Index)
		' get pen name value from ItemData property of combo box
		ReflectionHelper.LetMember(RtChart1, "ChnDspPen", ChannelPenCombo(Index).GetItemData(ChannelPenCombo(Index).SelectedIndex))

		' use change pen display action
		'UPGRADE_WARNING: (1068) RtChart_ChnChangePen of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart1.ChnDspAction = ((ChannelDisplayAction.ChangePen))

		' update array of channel pen names for this channel (combo box index)
		ChannelPenName(Index) = ReflectionHelper.GetMember(Of Integer)(RtChart1, "ChnDspPen")

	End Sub

	Private Sub Default_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles Default_Renamed.Click

		' restore original selections

		InitChannelPenArray()

		InitializePens(RtChart1)

		InitializeChannels(RtChart1, ChannelName.LastChannel, NUM_SAMPLES)

		InitComboBoxes()

		PlotData(RtChart1, ChannelName.LastChannel, NUM_SAMPLES, VBDataArrayY)

	End Sub

	Private Sub Form_Load()

		' initialize array of pen numbers indexed by channel number to maintain current channel pen
		' assignments for easy access and for pen assignment changes
		InitChannelPenArray()

		' build array of pen RGB color values, item list names, and combo list indexes
		' this provides a method to relate pen name strings to an RGB value and combo list ordering
		InitPenColorArray()

		' initialize custom pen array to default color values
		InitCustomPenArray()

		' define fixed pens
		InitializePens(RtChart1)

		' define custom pens
		InitializeCustomPens(RtChart1)

		' initialize RtChart for 4 viewports and no grid or axes
		InitializeRtChart(RtChart1)

		' initialize 4 channels, each in its own viewport for clarity
		InitializeChannels(RtChart1, ChannelName.LastChannel, NUM_SAMPLES)

		' add pen names and RGB color values to combo list boxes
		InitComboBoxes()

		' create 4 sine wave data arrays for plotting
		GenerateSineWaveArrays(ChannelName.LastChannel, NUM_SAMPLES, VOLTS_PEAK)

		' plot 4 channels' data
		PlotData(RtChart1, ChannelName.LastChannel, NUM_SAMPLES, VBDataArrayY)

	End Sub

	Private Sub GenerateSineWaveArrays(ByVal numChannels As Integer, ByVal numSamples As Integer, ByRef VoltsPeak As Single)

		Dim NumCycles, CycleTime As Single

		' NumSamples is 0-based, NumChannels is 1-based
		ReDim VBDataArrayY(NumSamples, NumChannels - 1) ' dimension array for 2D values

		Dim RadiansPerSample As Single = TWO_PIE / NumSamples

		' NumCycles determines # of cycles in buffer
		For channel As Integer = 1 To NumChannels
			NumCycles = ((Channel - 1) * RadiansPerSample)

			For sample As Integer = 0 To NumSamples - 1
				CycleTime = RadiansPerSample + NumCycles
				VBDataArrayY(sample, Channel) = VoltsPeak * Math.Sin(sample * CycleTime)

			Next sample
		Next Channel

	End Sub

	Private Sub InitChannelComboBox(ByRef RTCComboBox As ComboBox)


		' build list of pen color names and specify list index for ordering

		For Pen As Integer = FIRST_PEN To LAST_CUSTOM_PEN

			RTCComboBox.AddItem(PenColorArray(Pen).ItemName, PenColorArray(Pen).ListIndex)

			' the ItemData property is used to store pen name values
			RTCComboBox.SetItemData(PenColorArray(Pen).ListIndex, Pen)

		Next Pen

	End Sub

	Private Sub InitComboBoxes()


		' build list of fixed and custom pens
		For channel As Integer = ChannelName.Channel1 To ChannelName.Channel4

			InitChannelComboBox(ChannelPenCombo(Channel))

			' select current pen name for currently selected channel
			RtChart1.LogicalChannel = channel
			ChannelPenCombo(Channel).SelectedIndex = PenColorArray(ReflectionHelper.GetMember(Of Integer)(RtChart1, "ChnDspPen")).ListIndex

		Next Channel


		For Pen As Integer = FIRST_CUSTOM_PEN To LAST_CUSTOM_PEN

			' build list of pen colors and select current color for each custom pen
			InitPenComboBox(PenColorCombo(Pen))
			PenColorCombo(Pen).SelectedIndex = CustomPens.GetValue(Pen).PenListIndex

		Next Pen

	End Sub

	Private Sub InitPenComboBox(ByRef RTCComboBox As ComboBox)


		' build list of pen color names and specify list index for ordering

		For Pen As Integer = FIRST_PEN To LAST_PEN

			RTCComboBox.AddItem(PenColorArray(Pen).ItemName, PenColorArray(Pen).ListIndex)

			' the ItemData property is used to store RGB color values
			RTCComboBox.SetItemData(PenColorArray(Pen).ListIndex, PenColorArray(Pen).RGBValue)

		Next Pen

	End Sub

	Private Sub PenColorCombo_SelectedIndexChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _PenColorCombo_21.SelectedIndexChanged, _PenColorCombo_22.SelectedIndexChanged, _PenColorCombo_23.SelectedIndexChanged, _PenColorCombo_24.SelectedIndexChanged
		Dim Index As Integer = Array.IndexOf(Me.PenColorCombo, eventSender)
		Dim RtChart_SolidPen As Object

		' list control array indices are set at design-time to equal custom pens' number

		' describe new pen characteristics
		ReflectionHelper.LetMember(RtChart1, "Pen_Select", Index)
		' get RGB color value from ItemData property of combo box
		ReflectionHelper.LetMember(RtChart1, "PenColor", PenColorCombo(Index).GetItemData(PenColorCombo(Index).SelectedIndex))
		ReflectionHelper.LetMember(RtChart1, "PenWidth", 1) ' 1 pixel wide
		'UPGRADE_WARNING: (1068) RtChart_SolidPen of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		ReflectionHelper.LetMember(RtChart1, "PenStyle", ReflectionHelper.GetPrimitiveValue(RtChart_SolidPen)) ' specify solid line pen

		ChangeChannelsPen(RtChart1) ' use Change Pen Display Action for all channels

	End Sub

	'UPGRADE_NOTE: (7001) The following declaration (Plot_Click) seems to be dead code More Information: https://www.mobilize.net/vbtonet/ewis/ewi7001
	'Private Sub Plot_Click()
		'
		'PlotData(RtChart1, ChannelName.LastChannel, NUM_SAMPLES, VBDataArrayY)
		'
	'End Sub

	Private Sub SwapPens_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles SwapPens.Click


		' swap channel pen color with the other channels' pens

		' swap pens
		Dim TempPenName As Integer = ChannelPenName(ChannelName.Channel1)
		ChannelPenName(ChannelName.Channel1) = ChannelPenName(ChannelName.Channel2)
		ChannelPenName(ChannelName.Channel2) = ChannelPenName(ChannelName.Channel3)
		ChannelPenName(ChannelName.Channel3) = ChannelPenName(ChannelName.Channel4)
		ChannelPenName(ChannelName.Channel4) = TempPenName

		' select new pen for each channel and perform Change Pen Display Action
		ChangeChannelsPen(RtChart1)

		' update currently selected pen for each channel pen combo box
		For channel As Integer = ChannelName.Channel1 To ChannelName.Channel4
			ChannelPenCombo(Channel).SelectedIndex = PenColorArray(ChannelPenName(Channel)).ListIndex
		Next Channel

	End Sub
	Private Sub Form_Closed(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MyBase.Closed
	End Sub
End Class