<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
#Region "Upgrade Support "
	Private Shared _Instance As Form1
	Private Shared _InitializingInstance As Boolean
	Public Shared Property DefInstance() As Form1
		Get
			If _Instance Is Nothing OrElse _Instance.IsDisposed Then
				_InitializingInstance = True
				_Instance = CreateInstance()
				_InitializingInstance = False
			End If
			Return _Instance
		End Get
		Set(ByVal value As Form1)
			_Instance = Value
		End Set
	End Property
#End Region
#Region "Windows Form Designer generated code "
	Public Shared Function CreateInstance() As Form1
		Dim theInstance As New Form1()
		theInstance.Form_Load()
		Return theInstance
	End Function
	Private visualControls() As String = New String() {"components", "ToolTipMain", "Label9", "Frame3", "_PenColorCombo_21", "_PenColorCombo_22", "_PenColorCombo_23", "_PenColorCombo_24", "Label5", "Label6", "Label7", "Label8", "Frame2", "Default_Renamed", "_ChannelPenCombo_4", "_ChannelPenCombo_3", "_ChannelPenCombo_2", "_ChannelPenCombo_1", "Label4", "Label3", "Label2", "Label1", "Frame1", "SwapPens", "RtChart1", "ChannelPenCombo", "PenColorCombo"}
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTipMain As System.Windows.Forms.ToolTip
	Public WithEvents Label9 As System.Windows.Forms.Label
	Public WithEvents Frame3 As System.Windows.Forms.GroupBox
	Private WithEvents _PenColorCombo_21 As System.Windows.Forms.ComboBox
	Private WithEvents _PenColorCombo_22 As System.Windows.Forms.ComboBox
	Private WithEvents _PenColorCombo_23 As System.Windows.Forms.ComboBox
	Private WithEvents _PenColorCombo_24 As System.Windows.Forms.ComboBox
	Public WithEvents Label5 As System.Windows.Forms.Label
	Public WithEvents Label6 As System.Windows.Forms.Label
	Public WithEvents Label7 As System.Windows.Forms.Label
	Public WithEvents Label8 As System.Windows.Forms.Label
	Public WithEvents Frame2 As System.Windows.Forms.GroupBox
	Public WithEvents Default_Renamed As System.Windows.Forms.Button
	Private WithEvents _ChannelPenCombo_4 As System.Windows.Forms.ComboBox
	Private WithEvents _ChannelPenCombo_3 As System.Windows.Forms.ComboBox
	Private WithEvents _ChannelPenCombo_2 As System.Windows.Forms.ComboBox
	Private WithEvents _ChannelPenCombo_1 As System.Windows.Forms.ComboBox
	Public WithEvents Label4 As System.Windows.Forms.Label
	Public WithEvents Label3 As System.Windows.Forms.Label
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents Frame1 As System.Windows.Forms.GroupBox
	Public WithEvents SwapPens As System.Windows.Forms.Button
	Public WithEvents RtChart1 As isr.Visuals.RealTimeChart.RealTimeChartControl
	Public ChannelPenCombo(4) As System.Windows.Forms.ComboBox
	Public PenColorCombo(24) As System.Windows.Forms.ComboBox
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> _
	 Private Sub InitializeComponent()
		Me.components = New System.ComponentModel.Container()
		Dim resources As New System.ComponentModel.ComponentResourceManager(GetType(Form1))
		Me.ToolTipMain = New System.Windows.Forms.ToolTip(Me.components)
		Me.Frame3 = New System.Windows.Forms.GroupBox()
		Me.Label9 = New System.Windows.Forms.Label()
		Me.Frame2 = New System.Windows.Forms.GroupBox()
		Me._PenColorCombo_21 = New System.Windows.Forms.ComboBox()
		Me._PenColorCombo_22 = New System.Windows.Forms.ComboBox()
		Me._PenColorCombo_23 = New System.Windows.Forms.ComboBox()
		Me._PenColorCombo_24 = New System.Windows.Forms.ComboBox()
		Me.Label5 = New System.Windows.Forms.Label()
		Me.Label6 = New System.Windows.Forms.Label()
		Me.Label7 = New System.Windows.Forms.Label()
		Me.Label8 = New System.Windows.Forms.Label()
		Me.Default_Renamed = New System.Windows.Forms.Button()
		Me.Frame1 = New System.Windows.Forms.GroupBox()
		Me._ChannelPenCombo_4 = New System.Windows.Forms.ComboBox()
		Me._ChannelPenCombo_3 = New System.Windows.Forms.ComboBox()
		Me._ChannelPenCombo_2 = New System.Windows.Forms.ComboBox()
		Me._ChannelPenCombo_1 = New System.Windows.Forms.ComboBox()
		Me.Label4 = New System.Windows.Forms.Label()
		Me.Label3 = New System.Windows.Forms.Label()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.SwapPens = New System.Windows.Forms.Button()
		Me.RtChart1 = New isr.Visuals.RealTimeChart.RealTimeChartControl()
		Me.Frame3.SuspendLayout()
		Me.Frame2.SuspendLayout()
		Me.Frame1.SuspendLayout()
		Me.SuspendLayout()
		' 
		'Frame3
		' 
		Me.Frame3.AllowDrop = True
		Me.Frame3.BackColor = System.Drawing.SystemColors.Window
		Me.Frame3.Controls.Add(Me.Label9)
		Me.Frame3.Enabled = True
		Me.Frame3.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Frame3.Location = New System.Drawing.Point(360, 0)
		Me.Frame3.Name = "Frame3"
		Me.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame3.Size = New System.Drawing.Size(105, 209)
		Me.Frame3.TabIndex = 20
		Me.Frame3.Text = "Notes"
		Me.Frame3.Visible = True
		' 
		'Label9
		' 
		Me.Label9.AllowDrop = True
		Me.Label9.BackColor = System.Drawing.SystemColors.Window
		Me.Label9.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label9.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label9.Location = New System.Drawing.Point(4, 24)
		Me.Label9.Name = "Label9"
		Me.Label9.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label9.Size = New System.Drawing.Size(97, 181)
		Me.Label9.TabIndex = 21
		Me.Label9.Text = "This example demonstrates two basic methods for defining and changing pens. It uses 20 pre-defined (fixed) pens and four ""custom"" pens which can be set to any of the 20 available colors."
		' 
		'Frame2
		' 
		Me.Frame2.AllowDrop = True
		Me.Frame2.BackColor = System.Drawing.SystemColors.Window
		Me.Frame2.Controls.Add(Me._PenColorCombo_21)
		Me.Frame2.Controls.Add(Me._PenColorCombo_22)
		Me.Frame2.Controls.Add(Me._PenColorCombo_23)
		Me.Frame2.Controls.Add(Me._PenColorCombo_24)
		Me.Frame2.Controls.Add(Me.Label5)
		Me.Frame2.Controls.Add(Me.Label6)
		Me.Frame2.Controls.Add(Me.Label7)
		Me.Frame2.Controls.Add(Me.Label8)
		Me.Frame2.Enabled = True
		Me.Frame2.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Frame2.Location = New System.Drawing.Point(160, 224)
		Me.Frame2.Name = "Frame2"
		Me.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame2.Size = New System.Drawing.Size(145, 173)
		Me.Frame2.TabIndex = 15
		Me.Frame2.Text = "Custom Pen Colors"
		Me.Frame2.Visible = True
		' 
		'_PenColorCombo_21
		' 
		Me._PenColorCombo_21.AllowDrop = True
		Me._PenColorCombo_21.BackColor = System.Drawing.SystemColors.Window
		Me._PenColorCombo_21.CausesValidation = True
		Me._PenColorCombo_21.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me._PenColorCombo_21.Enabled = True
		Me._PenColorCombo_21.ForeColor = System.Drawing.SystemColors.WindowText
		Me._PenColorCombo_21.IntegralHeight = True
		Me._PenColorCombo_21.Location = New System.Drawing.Point(12, 36)
		Me._PenColorCombo_21.Name = "_PenColorCombo_21"
		Me._PenColorCombo_21.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._PenColorCombo_21.Size = New System.Drawing.Size(117, 20)
		Me._PenColorCombo_21.Sorted = False
		Me._PenColorCombo_21.TabIndex = 19
		Me._PenColorCombo_21.TabStop = True
		Me._PenColorCombo_21.Visible = True
		' 
		'_PenColorCombo_22
		' 
		Me._PenColorCombo_22.AllowDrop = True
		Me._PenColorCombo_22.BackColor = System.Drawing.SystemColors.Window
		Me._PenColorCombo_22.CausesValidation = True
		Me._PenColorCombo_22.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me._PenColorCombo_22.Enabled = True
		Me._PenColorCombo_22.ForeColor = System.Drawing.SystemColors.WindowText
		Me._PenColorCombo_22.IntegralHeight = True
		Me._PenColorCombo_22.Location = New System.Drawing.Point(12, 72)
		Me._PenColorCombo_22.Name = "_PenColorCombo_22"
		Me._PenColorCombo_22.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._PenColorCombo_22.Size = New System.Drawing.Size(117, 20)
		Me._PenColorCombo_22.Sorted = False
		Me._PenColorCombo_22.TabIndex = 18
		Me._PenColorCombo_22.TabStop = True
		Me._PenColorCombo_22.Visible = True
		' 
		'_PenColorCombo_23
		' 
		Me._PenColorCombo_23.AllowDrop = True
		Me._PenColorCombo_23.BackColor = System.Drawing.SystemColors.Window
		Me._PenColorCombo_23.CausesValidation = True
		Me._PenColorCombo_23.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me._PenColorCombo_23.Enabled = True
		Me._PenColorCombo_23.ForeColor = System.Drawing.SystemColors.WindowText
		Me._PenColorCombo_23.IntegralHeight = True
		Me._PenColorCombo_23.Location = New System.Drawing.Point(12, 109)
		Me._PenColorCombo_23.Name = "_PenColorCombo_23"
		Me._PenColorCombo_23.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._PenColorCombo_23.Size = New System.Drawing.Size(117, 20)
		Me._PenColorCombo_23.Sorted = False
		Me._PenColorCombo_23.TabIndex = 17
		Me._PenColorCombo_23.TabStop = True
		Me._PenColorCombo_23.Visible = True
		' 
		'_PenColorCombo_24
		' 
		Me._PenColorCombo_24.AllowDrop = True
		Me._PenColorCombo_24.BackColor = System.Drawing.SystemColors.Window
		Me._PenColorCombo_24.CausesValidation = True
		Me._PenColorCombo_24.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me._PenColorCombo_24.Enabled = True
		Me._PenColorCombo_24.ForeColor = System.Drawing.SystemColors.WindowText
		Me._PenColorCombo_24.IntegralHeight = True
		Me._PenColorCombo_24.Location = New System.Drawing.Point(12, 146)
		Me._PenColorCombo_24.Name = "_PenColorCombo_24"
		Me._PenColorCombo_24.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._PenColorCombo_24.Size = New System.Drawing.Size(117, 20)
		Me._PenColorCombo_24.Sorted = False
		Me._PenColorCombo_24.TabIndex = 16
		Me._PenColorCombo_24.TabStop = True
		Me._PenColorCombo_24.Visible = True
		' 
		'Label5
		' 
		Me.Label5.AllowDrop = True
		Me.Label5.BackColor = System.Drawing.SystemColors.Window
		Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label5.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label5.Location = New System.Drawing.Point(12, 20)
		Me.Label5.Name = "Label5"
		Me.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label5.Size = New System.Drawing.Size(93, 13)
		Me.Label5.TabIndex = 7
		Me.Label5.Text = "Pen 1"
		' 
		'Label6
		' 
		Me.Label6.AllowDrop = True
		Me.Label6.BackColor = System.Drawing.SystemColors.Window
		Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label6.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label6.Location = New System.Drawing.Point(12, 57)
		Me.Label6.Name = "Label6"
		Me.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label6.Size = New System.Drawing.Size(93, 13)
		Me.Label6.TabIndex = 8
		Me.Label6.Text = "Pen 2"
		' 
		'Label7
		' 
		Me.Label7.AllowDrop = True
		Me.Label7.BackColor = System.Drawing.SystemColors.Window
		Me.Label7.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label7.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label7.Location = New System.Drawing.Point(12, 94)
		Me.Label7.Name = "Label7"
		Me.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label7.Size = New System.Drawing.Size(93, 13)
		Me.Label7.TabIndex = 9
		Me.Label7.Text = "Pen 3"
		' 
		'Label8
		' 
		Me.Label8.AllowDrop = True
		Me.Label8.BackColor = System.Drawing.SystemColors.Window
		Me.Label8.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label8.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label8.Location = New System.Drawing.Point(12, 131)
		Me.Label8.Name = "Label8"
		Me.Label8.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label8.Size = New System.Drawing.Size(93, 13)
		Me.Label8.TabIndex = 10
		Me.Label8.Text = "Pen 4"
		' 
		'Default_Renamed
		' 
		Me.Default_Renamed.AllowDrop = True
		Me.Default_Renamed.BackColor = System.Drawing.SystemColors.Control
		Me.Default_Renamed.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Default_Renamed.Location = New System.Drawing.Point(320, 344)
		Me.Default_Renamed.Name = "Default_Renamed"
		Me.Default_Renamed.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Default_Renamed.Size = New System.Drawing.Size(93, 33)
		Me.Default_Renamed.TabIndex = 2
		Me.Default_Renamed.Text = "Default"
		Me.Default_Renamed.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
		Me.Default_Renamed.UseVisualStyleBackColor = False
		' 
		'Frame1
		' 
		Me.Frame1.AllowDrop = True
		Me.Frame1.BackColor = System.Drawing.SystemColors.Window
		Me.Frame1.Controls.Add(Me._ChannelPenCombo_4)
		Me.Frame1.Controls.Add(Me._ChannelPenCombo_3)
		Me.Frame1.Controls.Add(Me._ChannelPenCombo_2)
		Me.Frame1.Controls.Add(Me._ChannelPenCombo_1)
		Me.Frame1.Controls.Add(Me.Label4)
		Me.Frame1.Controls.Add(Me.Label3)
		Me.Frame1.Controls.Add(Me.Label2)
		Me.Frame1.Controls.Add(Me.Label1)
		Me.Frame1.Enabled = True
		Me.Frame1.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Frame1.Location = New System.Drawing.Point(8, 224)
		Me.Frame1.Name = "Frame1"
		Me.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame1.Size = New System.Drawing.Size(145, 173)
		Me.Frame1.TabIndex = 1
		Me.Frame1.Text = "Channel Pen Colors"
		Me.Frame1.Visible = True
		' 
		'_ChannelPenCombo_4
		' 
		Me._ChannelPenCombo_4.AllowDrop = True
		Me._ChannelPenCombo_4.BackColor = System.Drawing.SystemColors.Window
		Me._ChannelPenCombo_4.CausesValidation = True
		Me._ChannelPenCombo_4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me._ChannelPenCombo_4.Enabled = True
		Me._ChannelPenCombo_4.ForeColor = System.Drawing.SystemColors.WindowText
		Me._ChannelPenCombo_4.IntegralHeight = True
		Me._ChannelPenCombo_4.Location = New System.Drawing.Point(12, 146)
		Me._ChannelPenCombo_4.Name = "_ChannelPenCombo_4"
		Me._ChannelPenCombo_4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._ChannelPenCombo_4.Size = New System.Drawing.Size(117, 20)
		Me._ChannelPenCombo_4.Sorted = False
		Me._ChannelPenCombo_4.TabIndex = 14
		Me._ChannelPenCombo_4.TabStop = True
		Me._ChannelPenCombo_4.Visible = True
		' 
		'_ChannelPenCombo_3
		' 
		Me._ChannelPenCombo_3.AllowDrop = True
		Me._ChannelPenCombo_3.BackColor = System.Drawing.SystemColors.Window
		Me._ChannelPenCombo_3.CausesValidation = True
		Me._ChannelPenCombo_3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me._ChannelPenCombo_3.Enabled = True
		Me._ChannelPenCombo_3.ForeColor = System.Drawing.SystemColors.WindowText
		Me._ChannelPenCombo_3.IntegralHeight = True
		Me._ChannelPenCombo_3.Location = New System.Drawing.Point(12, 109)
		Me._ChannelPenCombo_3.Name = "_ChannelPenCombo_3"
		Me._ChannelPenCombo_3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._ChannelPenCombo_3.Size = New System.Drawing.Size(117, 20)
		Me._ChannelPenCombo_3.Sorted = False
		Me._ChannelPenCombo_3.TabIndex = 13
		Me._ChannelPenCombo_3.TabStop = True
		Me._ChannelPenCombo_3.Visible = True
		' 
		'_ChannelPenCombo_2
		' 
		Me._ChannelPenCombo_2.AllowDrop = True
		Me._ChannelPenCombo_2.BackColor = System.Drawing.SystemColors.Window
		Me._ChannelPenCombo_2.CausesValidation = True
		Me._ChannelPenCombo_2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me._ChannelPenCombo_2.Enabled = True
		Me._ChannelPenCombo_2.ForeColor = System.Drawing.SystemColors.WindowText
		Me._ChannelPenCombo_2.IntegralHeight = True
		Me._ChannelPenCombo_2.Location = New System.Drawing.Point(12, 72)
		Me._ChannelPenCombo_2.Name = "_ChannelPenCombo_2"
		Me._ChannelPenCombo_2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._ChannelPenCombo_2.Size = New System.Drawing.Size(117, 20)
		Me._ChannelPenCombo_2.Sorted = False
		Me._ChannelPenCombo_2.TabIndex = 12
		Me._ChannelPenCombo_2.TabStop = True
		Me._ChannelPenCombo_2.Visible = True
		' 
		'_ChannelPenCombo_1
		' 
		Me._ChannelPenCombo_1.AllowDrop = True
		Me._ChannelPenCombo_1.BackColor = System.Drawing.SystemColors.Window
		Me._ChannelPenCombo_1.CausesValidation = True
		Me._ChannelPenCombo_1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me._ChannelPenCombo_1.Enabled = True
		Me._ChannelPenCombo_1.ForeColor = System.Drawing.SystemColors.WindowText
		Me._ChannelPenCombo_1.IntegralHeight = True
		Me._ChannelPenCombo_1.Location = New System.Drawing.Point(12, 36)
		Me._ChannelPenCombo_1.Name = "_ChannelPenCombo_1"
		Me._ChannelPenCombo_1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._ChannelPenCombo_1.Size = New System.Drawing.Size(117, 20)
		Me._ChannelPenCombo_1.Sorted = False
		Me._ChannelPenCombo_1.TabIndex = 11
		Me._ChannelPenCombo_1.TabStop = True
		Me._ChannelPenCombo_1.Visible = True
		' 
		'Label4
		' 
		Me.Label4.AllowDrop = True
		Me.Label4.BackColor = System.Drawing.SystemColors.Window
		Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label4.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label4.Location = New System.Drawing.Point(12, 131)
		Me.Label4.Name = "Label4"
		Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label4.Size = New System.Drawing.Size(93, 13)
		Me.Label4.TabIndex = 6
		Me.Label4.Text = "Channel 4"
		' 
		'Label3
		' 
		Me.Label3.AllowDrop = True
		Me.Label3.BackColor = System.Drawing.SystemColors.Window
		Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label3.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label3.Location = New System.Drawing.Point(12, 94)
		Me.Label3.Name = "Label3"
		Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label3.Size = New System.Drawing.Size(93, 13)
		Me.Label3.TabIndex = 5
		Me.Label3.Text = "Channel 3"
		' 
		'Label2
		' 
		Me.Label2.AllowDrop = True
		Me.Label2.BackColor = System.Drawing.SystemColors.Window
		Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label2.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label2.Location = New System.Drawing.Point(12, 57)
		Me.Label2.Name = "Label2"
		Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label2.Size = New System.Drawing.Size(93, 13)
		Me.Label2.TabIndex = 4
		Me.Label2.Text = "Channel 2"
		' 
		'Label1
		' 
		Me.Label1.AllowDrop = True
		Me.Label1.BackColor = System.Drawing.SystemColors.Window
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label1.Location = New System.Drawing.Point(12, 20)
		Me.Label1.Name = "Label1"
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.Size = New System.Drawing.Size(93, 13)
		Me.Label1.TabIndex = 3
		Me.Label1.Text = "Channel 1"
		' 
		'SwapPens
		' 
		Me.SwapPens.AllowDrop = True
		Me.SwapPens.BackColor = System.Drawing.SystemColors.Control
		Me.SwapPens.ForeColor = System.Drawing.SystemColors.ControlText
		Me.SwapPens.Location = New System.Drawing.Point(320, 292)
		Me.SwapPens.Name = "SwapPens"
		Me.SwapPens.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.SwapPens.Size = New System.Drawing.Size(93, 33)
		Me.SwapPens.TabIndex = 0
		Me.SwapPens.Text = "Swap Pens"
		Me.SwapPens.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
		Me.SwapPens.UseVisualStyleBackColor = False
		' 
		'RtChart1
		' 
		Me.RtChart1.AutoSize = 0
		Me.RtChart1.AxesColor = 16776960
		Me.RtChart1.AxesTics = 0
		Me.RtChart1.AxesType = 0
		Me.RtChart1.BackColor = 8421376
		Me.RtChart1.BevelInner = 0
		Me.RtChart1.BevelOuter = 2
		Me.RtChart1.BevelWidth_Inner = 1
		Me.RtChart1.BevelWidth_Outer = 1
		Me.RtChart1.BorderColor = (-2147483633)
		Me.RtChart1.BorderWidth = 3
		Me.RtChart1.Chn_Select = 1
		Me.RtChart1.ChnData_Dimension = 1
		Me.RtChart1.ChnDspUserData = 0
		Me.RtChart1.ChnXform_Select = 1
		Me.RtChart1.ColorDepth = 0
		Me.RtChart1.ErrBase = 30200
		Me.RtChart1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.RtChart1.ForeColor = (-842150451)
		Me.RtChart1.FrameColor = 16776960
		Me.RtChart1.FrameOn = -1
		Me.RtChart1.FramesPerSec = 0
		Me.RtChart1.FrameTics = 0
		Me.RtChart1.GridColor = 8421504
		Me.RtChart1.GridOn = -1
		Me.RtChart1.GridSymmetry = 0
		Me.RtChart1.GridType = 0
		Me.RtChart1.HitTest = 0
		Me.RtChart1.ImageFile = String.Empty
		Me.RtChart1.LightColor = (-2147483634)
		Me.RtChart1.Location = New System.Drawing.Point(8, 0)
		Me.RtChart1.MajorDivHorz = 10
		Me.RtChart1.MajorDivVert = 8
		Me.RtChart1.MarginBottom = 10
		Me.RtChart1.MarginLeft = 10
		Me.RtChart1.MarginRight = 10
		Me.RtChart1.MarginTop = 10
		Me.RtChart1.MinorDiv = 5
		Me.RtChart1.MouseIcon = 0
		Me.RtChart1.Name = "RtChart1"
		Me.RtChart1.Outline = -1
		Me.RtChart1.OutlineColor = (-2147483642)
		Me.RtChart1.Pen_Select = 1
		Me.RtChart1.RoundedCorners = 0
		Me.RtChart1.Scale_Select = 1
		Me.RtChart1.ShadowColor = (-2147483632)
		Me.RtChart1.Size = New System.Drawing.Size(313, 225)
		Me.RtChart1.TabIndex = 22
		Me.RtChart1.Viewport_Select = 1
		Me.RtChart1.Viewports = 1
		Me.RtChart1.ViewportStorageColor = 12632256
		Me.RtChart1.ViewportStorageOn = 0
		Me.RtChart1.Wnd_Select = 1
		' 
		'Form1
		' 
		Me.AllowDrop = True
		Me.AutoScaleDimensions = New System.Drawing.SizeF(7, 12)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Window
		Me.ClientSize = New System.Drawing.Size(472, 405)
		Me.Controls.Add(Me.Frame3)
		Me.Controls.Add(Me.Frame2)
		Me.Controls.Add(Me.Default_Renamed)
		Me.Controls.Add(Me.Frame1)
		Me.Controls.Add(Me.SwapPens)
		Me.Controls.Add(Me.RtChart1)
		Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.ForeColor = System.Drawing.SystemColors.WindowText
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Location = New System.Drawing.Point(125, 133)
		Me.Location = New System.Drawing.Point(122, 115)
		Me.MaximizeBox = False
		Me.MinimizeBox = True
		Me.Name = "Form1"
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Size = New System.Drawing.Size(479, 427)
		Me.Text = "LabOBJX Real Time Chart - Pens Example"
		Me.Frame3.ResumeLayout(False)
		Me.Frame2.ResumeLayout(False)
		Me.Frame1.ResumeLayout(False)
		Me.ResumeLayout(False)
	End Sub
	Sub ReLoadForm(ByVal addEvents As Boolean)
		InitializePenColorCombo()
		InitializeChannelPenCombo()
		Form_Load()
		If addEvents Then
			AddHandler MyBase.Closed, AddressOf Me.Form_Closed
		End If
	End Sub
	Sub InitializePenColorCombo()
		ReDim PenColorCombo(24)
		Me.PenColorCombo(21) = _PenColorCombo_21
		Me.PenColorCombo(22) = _PenColorCombo_22
		Me.PenColorCombo(23) = _PenColorCombo_23
		Me.PenColorCombo(24) = _PenColorCombo_24
	End Sub
	Sub InitializeChannelPenCombo()
		ReDim ChannelPenCombo(4)
		Me.ChannelPenCombo(4) = _ChannelPenCombo_4
		Me.ChannelPenCombo(3) = _ChannelPenCombo_3
		Me.ChannelPenCombo(2) = _ChannelPenCombo_2
		Me.ChannelPenCombo(1) = _ChannelPenCombo_1
	End Sub
#End Region
End Class