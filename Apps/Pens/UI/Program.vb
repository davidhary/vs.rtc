Option Strict Off
Option Explicit On
Imports System
Imports isr.Visuals.RealTimeChart
Module Program
    ' LabOBJX Real-Time Chart - Example Code
    ' (C) Copyright 1995, Scientific Software Tools, Inc.
    ' All Rights Reserved.
    '
    Public Const AssemblyTitle As String = "Real Time Strip Chart Pens Demo"
    Public Const AssemblyDescription As String = "Real Time Strip Chart Pens Demo"
    Public Const AssemblyProduct As String = "Visuals.Real.Time.Chart.Pens"

    Public result As Integer

    ' array of Pen name (numbers) indexed by channel number
    ' maintains current channel pen assignments
    Public ChannelPenName(4) As Integer

    Public CustomPens As Array

    ' custom (user-defined) pen name constants
    Public Const FIRST_CUSTOM_PEN As Integer = LAST_PEN + 1
    Public Const PEN1 As Double = FIRST_CUSTOM_PEN
    Public Const PEN2 As Double = PEN1 + 1
    Public Const PEN3 As Double = PEN2 + 1
    Public Const PEN4 As Double = PEN3 + 1
    Public Const LAST_CUSTOM_PEN As Double = PEN4

    Sub ChangeChannelsPen(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl)
        Dim RtChart_ChnChangePen, RtChart_DisablePaint, RtChart_EnablePaint As Object


        ' disable painting until all channels have new pens
        rtChart.ChnDspAction = ((ChannelDisplayAction.DisablePaint))

        For channel As Integer = ChannelName.Channel1 To ChannelName.Channel4
            ' select Channel's pen
            rtChart.LogicalChannel = channel
            rtChart.ChnDspPen = (ChartPen.ChannelPenName(channel))

            ' use change pen display action
            'UPGRADE_WARNING: (1068) RtChart_ChnChangePen of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
            rtChart.ChnDspAction = ChannelDisplayAction.ChangePen

        Next channel

        ' paint all channels at once with new pens
        rtChart.ChnDspAction = ((ChannelDisplayAction.EnablePaint))

    End Sub

    Sub InitChannelPenArray()

        ' assign custom pens to each channel - holds current pen assignments
        ChannelPenName(ChannelName.Channel1) = PEN1
        ChannelPenName(ChannelName.Channel2) = PEN2
        ChannelPenName(ChannelName.Channel3) = PEN3
        ChannelPenName(ChannelName.Channel4) = PEN4

    End Sub

    Sub InitCustomPenArray()

        CustomPens = Array.CreateInstance(GetType(Module1.CustomPenArray), New Integer() {CInt(PEN4) - CInt(PEN1) + 1}, New Integer() {CInt(PEN1)})
        ReDim Preserve PenColorArray(CInt(LAST_CUSTOM_PEN))

        ' initialize custom pen specifications to default values
        ' this array provides one place to supply this information
        ' used to select current pen color name string from combo box
        ' used to specify pen color value for pen definition

        CustomPens.GetValue(CInt(PEN1)).PenName = PenColorArray(RED_PEN).ItemName
        CustomPens.GetValue(CInt(PEN1)).PenColorValue = PenColorArray(RED_PEN).RGBValue
        CustomPens.GetValue(CInt(PEN1)).PenListIndex = PenColorArray(RED_PEN).ListIndex

        CustomPens.GetValue(CInt(PEN2)).PenName = PenColorArray(WHITE_PEN).ItemName
        CustomPens.GetValue(CInt(PEN2)).PenColorValue = PenColorArray(WHITE_PEN).RGBValue
        CustomPens.GetValue(CInt(PEN2)).PenListIndex = PenColorArray(WHITE_PEN).ListIndex

        CustomPens.GetValue(CInt(PEN3)).PenName = PenColorArray(BLUE_PEN).ItemName
        CustomPens.GetValue(CInt(PEN3)).PenColorValue = PenColorArray(BLUE_PEN).RGBValue
        CustomPens.GetValue(CInt(PEN3)).PenListIndex = PenColorArray(BLUE_PEN).ListIndex

        CustomPens.GetValue(CInt(PEN4)).PenName = PenColorArray(BLACK_PEN).ItemName
        CustomPens.GetValue(CInt(PEN4)).PenColorValue = PenColorArray(BLACK_PEN).RGBValue
        CustomPens.GetValue(CInt(PEN4)).PenListIndex = PenColorArray(BLACK_PEN).ListIndex


        ' this array provides the means to relate the text pen name with the RGB value
        ' for each pen, it contains:
        '       ItemName - the text name of the pen to be listed in the combo box
        '       RGBValue - the RGB value used to specify the color of the pen
        '       ListIndex - controls the list ording in the combo box list

        PenColorArray(CInt(PEN1)).ItemName = "Pen1"
        PenColorArray(CInt(PEN1)).RGBValue = CustomPens.GetValue(CInt(PEN1)).PenColorValue
        PenColorArray(CInt(PEN1)).ListIndex = 20

        PenColorArray(CInt(PEN2)).ItemName = "Pen2"
        PenColorArray(CInt(PEN2)).RGBValue = CustomPens.GetValue(CInt(PEN2)).PenColorValue
        PenColorArray(CInt(PEN2)).ListIndex = 21

        PenColorArray(CInt(PEN3)).ItemName = "Pen3"
        PenColorArray(CInt(PEN3)).RGBValue = CustomPens.GetValue(CInt(PEN3)).PenColorValue
        PenColorArray(CInt(PEN3)).ListIndex = 22

        PenColorArray(CInt(PEN4)).ItemName = "Pen4"
        PenColorArray(CInt(PEN4)).RGBValue = CustomPens.GetValue(CInt(PEN4)).PenColorValue
        PenColorArray(CInt(PEN4)).ListIndex = 23

    End Sub

    Sub InitializeChannels(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal numChannels As Integer, ByVal samplesPerChannel As Integer)

        ' Initialize CRT Control

        ' Window Description
        rtChart.LogicalWindow = 1 ' select X-axis time window
        rtChart.WndXmin = 0 ' left minimum abscissa
        rtChart.WndWidth = samplesPerChannel ' in Seconds
        rtChart.WndHeight = (40) ' in Volts
        rtChart.WndYmin = (rtChart.WndHeight / -2) ' bottom minimum ordinate

        rtChart.Viewports = 4 ' specify number of viewports

        ' initialize all channels to be used
        For channel As Integer = ChannelName.Channel1 To ChannelName.Channel4

            ' Describe Channel
            rtChart.LogicalChannel = channel ' select channel
            rtChart.ChnDspWindow = 1 ' assign window to channel
            rtChart.ChnDspViewport = channel ' specify view port same as channel number
            rtChart.ChnDspPen = (ChartPen.ChannelPenName(channel)) ' specify pen "name" (number)
            rtChart.ChnDspBufLen = samplesPerChannel ' input buffer length for 2D array
            rtChart.ChnDspMode = ChannelDisplayMode.CRT ' per channel display mode, use RunMode constants
            rtChart.ChnDspStyle = ChannelDisplayStyle.Lines ' use line style for plotting arrays
            rtChart.ChnDspOffsetX = 0 ' set offset to 0
            rtChart.ChnDspOffsetY = 0 ' set offset to 0

            ' Describe horizontal dimension
            rtChart.ChannelDataDimension = LogicalDimension.Horizontal
            rtChart.ChnDataShape = ChannelDataShape.AutoIncr
            rtChart.ChnDataOffset = 0
            rtChart.ChnDataIncr = 1 ' logical increment to next value in array

            ' Describe vertical dimension
            rtChart.ChannelDataDimension = LogicalDimension.Vertical
            rtChart.ChnDataShape = ChannelDataShape.Scalar
            rtChart.ChnDataType = ChannelDataType.Single
            rtChart.ChnDataOffset = 0
            rtChart.ChnDataIncr = 1 ' logical increment to next value for 2D array

            rtChart.ChnDspAction = ChannelDisplayAction.Initialize

        Next channel

    End Sub

    Sub InitializeCustomPens(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl)
        Dim RtChart_SolidPen As Object


        ' describe pen characteristics for 20 pre-defined and custom pens
        ' get RGB values from PenColorArray for each pen

        For Pen As Integer = FIRST_CUSTOM_PEN To LAST_CUSTOM_PEN

            rtChart.LogicalPen = Pen)
			rtChart.PenColor = Drawing.Color.FromArgb(PenColorArray(Pen).RGBValue)
        rtChart.PenWidth = 1
            'UPGRADE_WARNING: (1068) RtChart_SolidPen of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
            rtChart.PenStyle = PenStyle.SolidPen

        Next Pen

    End Sub

    Sub InitializeRtChart(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl)
        Dim RtChart_HorzVertGrid, RtChart_Inset, RtChart_MajorMinorAxesTics, RtChart_MajorMinorFrameTics, RtChart_NoAxes, RtChart_Raised As Object

        ' Global operation properties
        rtChart.HitTest = False ' enables mouse pointer "hit" detection
        rtChart.FramesPerSec = 0 ' 0 for asynchronous, -1 for synchronous

        ' Border description properties
        rtChart.BorderColor = Color.LightGray
        rtChart.LightColor = Color.DarkGray
        rtChart.ShadowColor = Color.Black
        rtChart.BevelInner = BevelStyle.Inset
        rtChart.BevelOuter = BevelStyle.Raised
        rtChart.BevelWidthInner = 5
        rtChart.BevelWidthOuter = 5
        rtChart.BorderWidth = 3
        rtChart.Outline = True
        rtChart.OutlineColor = Color.Black
        rtChart.RoundedCorners = False

        ' Graticule description properties
        rtChart.BackColor = Color.DarkCyan
        rtChart.AutoSize = True ' true forces square border, frame is always square

        rtChart.FrameOn = True
        rtChart.FrameTics = FrameTicsStyle.MajorMinorFrameTics
        rtChart.FrameColor = Color.White

        rtChart.AxesType = AxesType.NoAxes ' no axes for this example
        rtChart.AxesTics = AxesTicsStyle.MajorMinorAxesTics
        rtChart.AxesColor = Color.White

        rtChart.GridOn = False ' no grid for this example
        rtChart.GridType = GridType.HorizontalVertical
        rtChart.GridColor = Color.LightGray

        ReflectionHelper.LetMember(rtChart, "MajorDivHorz", 10) ' major horizontal divisions
        rtChart.MajorDivVert = (8) ' major vertical divisions
        rtChart.MinorDiv = 5 ' minor divisions
        rtChart.MarginLeft = 2 ' left Graticule border
        rtChart.MarginTop = 2 ' top Graticule border
        rtChart.MarginRight = 2 ' right Graticule border
        rtChart.MarginBottom = 2 ' bottom Graticule border

    End Sub

    Sub PlotData(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal numChannels As Integer, ByVal numPoints As Integer, ByRef DataArray(,) As Single)

        ' RtChart exports ChartData function to plot data of selected channel


        For channel As Integer = ChannelName.Channel1 To numChannels

            result = rtChart.ChartData(channel, NumPoints, DataArray, NumPoints * (channel - 1), 0, 0)

        Next channel

    End Sub

    Sub RedefineChannelsPens(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl)
        Dim RtChart_ChnChangePen, RtChart_DisablePaint, RtChart_EnablePaint, RtChart_SolidPen As Object


        rtChart.ChnDspAction = ((ChannelDisplayAction.DisablePaint))

        For channel As Integer = ChannelName.Channel1 To ChannelName.Channel4

            ' describe new pen characteristics
            rtChart.LogicalPen = ChannelPenName(channel))
			rtChart.PenColor = Drawing.Color.FromArgb(PenColorArray(ChannelPenName(channel)).RGBValue)
            rtChart.PenWidth = 1
            'UPGRADE_WARNING: (1068) RtChart_SolidPen of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
            rtChart.PenStyle = PenStyle.SolidPen

            ' select Channel's pen
            rtChart.LogicalChannel = channel
            rtChart.ChnDspPen = (ChartPen.ChannelPenName(channel))

            ' use change pen display action
            'UPGRADE_WARNING: (1068) RtChart_ChnChangePen of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
            rtChart.ChnDspAction = ChannelDisplayAction.ChangePen

        Next channel

        rtChart.ChnDspAction = ((ChannelDisplayAction.EnablePaint))


    End Sub
End Module