Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic
Imports System
Imports System.Windows.Forms
Imports isr.Visuals.RealTimeChart
Partial Friend Class RTCPlotForm
	Inherits System.Windows.Forms.Form

	' LabOBJX Real-Time Chart - Example Code
	' (C) Copyright 1995, Scientific Software Tools, Inc.
	' All Rights Reserved.
	'

	Const TWO_PIE As Double = 2 * 3.1415926535898

	Dim FirstPoint As Integer
	Dim NumSamples As Integer ' number of samples
	Dim NumChannels As Integer ' number of channels

	' signal waveform limits (volts peak)
	Dim MaxVolts As Single
	Dim MinVolts As Single
	Public Sub New()
		MyBase.New()
		If _Instance Is Nothing Then
			If _InitializingInstance Then
				_Instance = Me
			Else
				Try
					'For the start-up form, the first instance created is the default instance.
					If Not (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint Is Nothing) AndAlso System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType Is Me.GetType() Then
						_Instance = Me
					End If

				Catch
				End Try
			End If
		End If
		'This call is required by the Windows Form Designer.
		InitializeComponent()
		ReLoadForm(False)
	End Sub



	Private Sub Append_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles Append.Click
		Dim RtChart_AppendToClipboard As Object

		' Append Real-Time Chart bitmap to the clipboard, do not clear
		' Note: Run Clipboard, Select View/Text or DIB Bitmap

		' Copy this text to clipboard
		'UPGRADE_WARNING: (2081) Clipboard.SetText has a new behavior. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2081
		Clipboard.SetText("Select View/Text or DIB Bitmap to display this text or RtChart bitmap.")

		' Add bitmap item to clipboard, can be added without
		' clearing or "over-writing" data in other formats (text, etc.)
		' text and bitamps can both be stored in clipboard at the same time.
		'UPGRADE_WARNING: (1068) RtChart_AppendToClipboard of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart1.ChartAction =  ((ChartAction.AppendToClipboard))

	End Sub

	Private Sub Copy_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles Copy.Click
		Dim RtChart_CopyToClipboard As Object

		' Copy Real-Time Chart bitmap to the clipboard, clearing all data
		' Note: Run Clipboard to display RtChart bitmap, all other data is cleared

		' Copy Real-Time Chart data to the clipboard over-writes all data
		'UPGRADE_WARNING: (1068) RtChart_CopyToClipboard of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart1.ChartAction =  ((ChartAction.CopyToClipboard))

	End Sub

	Private Sub Form_Load()

		' initialize program's global variables
		InitializeGlobalVariables()

        ' initialize RtChart Pens
        ChartPen.InitChannelsPens(RtChart1)

		' initialize RtChart global properties
		InitializeRtChart(RtChart1)

		' initialize RtChart Pens
		ChartPen.InitChannelsPens(RtChart1)

		' initialize RtChart for Volts vs Time plots
		InitializeChannels(RtChart1, NumChannels, NumSamples)

		' Clear all contents of clipboard
		Clipboard.Clear()

		' plot single buffer of 4 channels
		SinglePlot()

	End Sub

	Private Sub GenerateSineWaveArrays(ByVal numChannels As Integer, ByVal numSamples As Integer, ByRef VoltsPeak As Single)

		Dim NumCycles, CycleTime As Single

		' NumSamples is 0-based, NumChannels is 1-based
		ReDim VBDataArray(NumSamples, NumChannels - 1) ' dimension array for 2D values

		Dim RadiansPerSample As Single = TWO_PIE / NumSamples

		' NumCycles determines # of cycles per channel in buffer
		For channel As Integer = 1 To NumChannels
			NumCycles = (channel * RadiansPerSample)

			For sample As Integer = 0 To NumSamples - 1

				CycleTime = RadiansPerSample + NumCycles
				VBDataArray(sample, channel) = VoltsPeak * Math.Sin(sample * CycleTime)

			Next sample
		Next channel

	End Sub

	Private Sub InitializeGlobalVariables()

		'initialize variables
		Running = False

		NumChannels = 4
		NumSamples = 100

		MaxVolts = 15
		MinVolts = -15

	End Sub

	Private Sub SimulateContinuousData(ByVal numChannels As Integer, ByVal numSamples As Integer)

		Do While Running

			' Generate random starting point in buffer.
			FirstPoint = Math.Floor(CDbl((NumSamples - 1) * VBMath.Rnd() + 1))

			' PlotData call RtChart "ChartData" plotting function for each channel
			PlotData(RtChart1, NumChannels, NumSamples, FirstPoint, VBDataArray)

			Application.DoEvents() ' give control to Windows to process events (mouse, keyboard, etc.)

		Loop 

	End Sub

	Private Sub SinglePlot()

		' generate 2-dimensional array twice as large to plot "random" wave
		GenerateSineWaveArrays(NumChannels, NumSamples * 2, MaxVolts)

		' Generate random starting point in buffer.
		FirstPoint = Math.Floor(CDbl((NumSamples - 1) * VBMath.Rnd() + 1))

		' calls ChartData function once
		PlotData(RtChart1, NumChannels, NumSamples, FirstPoint, VBDataArray)

	End Sub

	Private Sub StartStop_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles StartStop.Click

		If Not Running Then
			Running = True
			StartStop.Text = "Stop"

			' generate 2-dimensional array twice as large to plot "random" wave
			GenerateSineWaveArrays(NumChannels, NumSamples * 2, MaxVolts)

			' Continuously call PlotData with random buffer start point
			SimulateContinuousData(NumChannels, NumSamples)

		Else
            Program.Running = False
			StartStop.Text = "Start"

		End If

	End Sub
	Private Sub Form_Closed(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MyBase.Closed
	End Sub
End Class