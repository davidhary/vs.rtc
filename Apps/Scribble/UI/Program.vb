Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic
Imports System
Imports System.Windows.Forms
Imports isr.Visuals.RealTimeChart
Module Program
    ' LabOBJX Real-Time Chart - Example Code
    ' (C) Copyright 1995, Scientific Software Tools, Inc.
    ' All Rights Reserved.
    '
    Public Const AssemblyTitle As String = "Real Time Strip Chart Scribble Demo"
    Public Const AssemblyDescription As String = "Real Time Strip Chart Scribble Demo"
    Public Const AssemblyProduct As String = "Visuals.Real.Time.Chart.Scribble"

    Public result As Integer
    Public Running As Integer
    Public SelectedChannel As Integer ' currently selected data channel

    Public VBDataArrayY As Array ' VB array to store multi-channel voltage data values

    Public PenNormalColor(20) As Integer
    Public PenIntenseColor(20) As Integer

    ' signal waveform limits (volts peak)
    Public Const MaxVolts As Integer = 10
    Public Const MinVolts As Integer = -10

    Public Const NumSamples As Integer = 1000 ' number of samples
    Public Const NumChannels As Integer = 1 ' number of samples
    Public Const VoltsPerDiv As Integer = 5 ' vertical scaling is based on volts per division
    Public Const TimePerDiv As Integer = 10 ' horizontal scaling is based on seconds per division

    Public Const TWO_PIE As Double = 2 * 3.1415926535898

    Public Const MAX_VOLTSDIV As Integer = 50
    Public Const MAX_TIMEDIV As Integer = 1000
    Public Const MAX_SAMPLES As Integer = 30000

    Public Const NORMAL_COLOR As Integer = 1
    Public Const LIGHT_COLOR As Integer = 2
    Public Const DARK_COLOR As Integer = 3

    ' grid constants
    Public Const MAJOR_HORZ_DIV As Integer = 10
    Public Const MAJOR_VERT_DIV As Integer = 8
    Public Const MINOR_DIVISIONS As Integer = 5

    Sub EraseChannels(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal firstChannel As Integer, ByVal lastChannel As Integer)



        ' prevent each channel from being erased individually
        rtChart.ChartAction = ChartAction.DisablePaint

        ' erase specified channels
        For channel As Integer = firstChannel To lastChannel
            rtChart.LogicalChannel = channel ' select channel to erase
            'UPGRADE_WARNING: (1068) RtChart_ChnErase of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
            rtChart.ChnDspAction = ChannelDisplayAction.Erase ' use channel erase display action
            'UPGRADE_WARNING: (1068) RtChart_ChnFlush of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
            rtChart.ChnDspAction = ((ChannelDisplayAction.Flush))
        Next channel

        ' paint entire window to erase all channels
        rtChart.ChartAction = ChartAction.EnablePaint

    End Sub

    Sub InitializePointChannels(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal numChannels As Integer, ByVal samplesPerChannel As Integer)
        Dim RtChart_AutoIncr, RtChart_ChnInitialize, RtChart_CRT, RtChart_Horz, RtChart_Lines, RtChart_Points, RtChart_Scalar, RtChart_Single, RtChart_Vert As Object


        ' Window Description
        rtChart.LogicalWindow = 1 ' select window
        rtChart.WndXmin = 0 ' left minimum abscissa
        rtChart.WndWidth = (TimePerDiv * MAJOR_HORZ_DIV) ' in Seconds (Time/div * 10 divisions)
        rtChart.WndHeight = (VoltsPerDiv * MAJOR_VERT_DIV) ' in Volts (Volts/div * 8 divisions)
        rtChart.WndYmin = (rtChart.WndHeight / -2) ' bottom minimum ordinate

        Dim channel As Integer = ChannelName.Channel1

        ' Describe Channel
        rtChart.LogicalChannel = channel ' select channel
        rtChart.ChnDspWindow = 1 ' assign X-axis (time) window to channel
        rtChart.ChnDspViewport = 1 ' all channels assigned to viewport 1
        rtChart.ChnDspPen = channel ' specify pen
        rtChart.ChnDspBufLen = samplesPerChannel ' total input buffer length
        rtChart.ChnDspMode = ChannelDisplayMode.CRT ' per channel display mode, use RunMode constants
        'UPGRADE_WARNING: (1068) RtChart_Points of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
        rtChart.ChnDspStyle = ChannelDisplayStyle.Points ' use point style for plotting single points
        rtChart.ChnDspOffsetX = 0
        rtChart.ChnDspOffsetY = 0

        ' Describe horizontal dimension
        rtChart.ChannelDataDimension = LogicalDimension.Horizontal
        rtChart.ChnDataShape = ChannelDataShape.Scalar
        rtChart.ChnDataType = ChannelDataType.Single
        rtChart.ChnDataOffset = 0 ' offset into buffer (in points)
        rtChart.ChnDataIncr = 1 ' logical increment to next value in array

        ' Describe vertical dimension
        rtChart.ChannelDataDimension = LogicalDimension.Vertical
        rtChart.ChnDataShape = ChannelDataShape.Scalar
        rtChart.ChnDataType = ChannelDataType.Single
        rtChart.ChnDataOffset = 0 ' offset into buffer (in points)
        rtChart.ChnDataIncr = 1 ' logical increment to next value for 2D array

        rtChart.ChnDspAction = ChannelDisplayAction.Initialize

        channel = ChannelName.Channel2

        ' Describe Channel
        rtChart.LogicalChannel = channel ' select channel
        rtChart.ChnDspWindow = 1 ' assign X-axis (time) window to channel
        rtChart.ChnDspViewport = 1 ' all channels assigned to viewport 1
        rtChart.ChnDspPen = channel ' specify pen
        rtChart.ChnDspBufLen = samplesPerChannel ' total input buffer length
        rtChart.ChnDspMode = ChannelDisplayMode.CRT ' per channel display mode, use RunMode constants
        rtChart.ChnDspStyle = ChannelDisplayStyle.Lines ' use point style for plotting single points
        rtChart.ChnDspOffsetX = 0
        rtChart.ChnDspOffsetY = 0

        ' Describe horizontal dimension
        rtChart.ChannelDataDimension = LogicalDimension.Horizontal
        rtChart.ChnDataShape = ChannelDataShape.AutoIncr
        rtChart.ChnDataOffset = 0 ' offset into buffer (in points)
        rtChart.ChnDataIncr = 1 ' logical increment to next value in array

        ' Describe vertical dimension
        rtChart.ChannelDataDimension = LogicalDimension.Vertical
        rtChart.ChnDataShape = ChannelDataShape.Scalar
        rtChart.ChnDataType = ChannelDataType.Single
        rtChart.ChnDataOffset = 0 ' offset into buffer (in points)
        rtChart.ChnDataIncr = 1 ' logical increment to next value for 2D array

        rtChart.ChnDspAction = ChannelDisplayAction.Initialize

    End Sub

    Sub InitializeRtChart(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl)

        ' Global operation properties
        rtChart.ImageFile = ("RTCImage.bmp") ' specifies file name for Write action
        rtChart.HitTest = True ' enables mouse pointer "hit" detection
        rtChart.FramesPerSec = 0 ' 0 for asynchronous, -1 for synchronous

        ' Border description properties
        rtChart.BorderColor = Color.LightGray
        rtChart.LightColor = Color.DarkGray
        rtChart.ShadowColor = Color.Black
        rtChart.BevelInner = BevelStyle.Inset
        rtChart.BevelOuter = BevelStyle.Raised
        rtChart.BevelWidthInner = 5
        rtChart.BevelWidthOuter = 5
        rtChart.BorderWidth = 3
        rtChart.Outline = True
        rtChart.OutlineColor = Color.Black
        rtChart.RoundedCorners = False

        ' Graticule description properties
        rtChart.BackColor = Color.DarkCyan
        rtChart.AutoSize = True ' true forces square border, frame is always square

        rtChart.FrameOn = True
        rtChart.FrameTics = FrameTicsStyle.MajorMinorFrameTics
        rtChart.FrameColor = Color.White

        rtChart.AxesType = AxesType.HorizontalVertical
        rtChart.AxesTics = AxesTicsStyle.MajorMinorAxesTics
        rtChart.AxesColor = Color.White

        rtChart.GridOn = False
        rtChart.GridType = GridType.HorizontalVertical
        rtChart.GridColor = Color.LightGray

        rtChart.MajorDivHorz = MAJOR_HORZ_DIV ' major horizontal divisions
        rtChart.MajorDivVert = MAJOR_VERT_DIV ' major vertical divisions
        rtChart.MinorDiv = MINOR_DIVISIONS ' minor divisions
        rtChart.MarginLeft = 2 ' left Graticule border
        rtChart.MarginTop = 2 ' top Graticule border
        rtChart.MarginRight = 2 ' right Graticule border
        rtChart.MarginBottom = 2 ' bottom Graticule border

        rtChart.ColorDepth = ColorDepth.Color16 '  256 palette type adapter only windows_g

    End Sub

    Sub PlotData(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal numChannels As Integer, ByVal numPoints As Integer)

        ' RtChart exports ChartData function to plot data of selected channel


        rtChart.FramesPerSec = -1 ' synchronous mode forces paint event to occur on each point

        result = rtChart.ChartData(ChannelName.Channel2, NumPoints, VBDataArrayY, (ChannelName.Channel1 - 1) * NumPoints, 0, 0)

        rtChart.FramesPerSec = 0 ' asynchronous mode

        ' display returned status code converted to string
        If result < 0 Then Interaction.MsgBox(RtChartError(result), MsgBoxStyle.Exclamation, "RtChart ChartData Error")

    End Sub

    Sub ScreenToWindow(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal Obj As Integer, ByRef X As Single, ByRef Y As Single)

        ' Convert screen coord in twips to world coordinates (engineering units) for object
        rtChart.LogicalChannel = (Obj) ' select channel object
        rtChart.LogicalWindow = rtChart.ChnDspWindow ' select channel's window
        rtChart.LogicalViewport = rtChart.ChnDspViewport ' select channel's viewport

        ' WndX & WndY properties take twips and return RtChart Window units

        rtChart.WndX = (X) ' convert twips to window coord
        X = rtChart.WndX ' return world coord

        rtChart.WndY = (Y) ' convert twips to window coord
        Y = rtChart.WndY ' return world coord

    End Sub
End Module