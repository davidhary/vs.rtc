Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic
Imports System
Imports System.Windows.Forms
Imports isr.Visuals.RealTimeChart
Partial Friend Class Form1
	Inherits System.Windows.Forms.Form

	' LabOBJX Real-Time Chart - Example Code
	' (C) Copyright 1995, Scientific Software Tools, Inc.
	' All Rights Reserved.
	'


	Dim i As Integer ' index in for/next loops
	Dim result As Integer ' return value from function call
	Dim TempValue As Integer ' temporary storage variable
	Dim CurrentPointer As Integer ' current mouse pointer

	' used in RtChart event procedures for re-positioning data channels and cursors
	Dim LastObject As Integer ' selected channel on MouseDown event
	Dim MouseX As Single ' previous mouse X-axis position
	Dim MouseY As Single ' previous mouse Y-axis position
	Public Sub New()
		MyBase.New()
		If _Instance Is Nothing Then
			If _InitializingInstance Then
				_Instance = Me
			Else
				Try
					'For the start-up form, the first instance created is the default instance.
					If Not (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint Is Nothing) AndAlso System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType Is Me.GetType() Then
						_Instance = Me
					End If

				Catch
				End Try
			End If
		End If
		'This call is required by the Windows Form Designer.
		InitializeComponent()
		ReLoadForm(False)
	End Sub



	Private Sub Command1_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles Command1.Click

		PlotData(RtChart1, NumChannels, NumSamples)

	End Sub

	Private Sub Command2_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles Command2.Click

		VBDataArrayY = Array.CreateInstance(GetType(Single), New Integer() {1, 1}, New Integer() {0, 0)
		VBDataArrayY = Array.CreateInstance(GetType(Single), New Integer() {NumSamples + 1, ChannelName.Channel4 + 1}, New Integer() {0, 0)

		InitializePointChannels(RtChart1, NumChannels, NumSamples)

	End Sub

	Private Sub Form_Load()

		VBDataArrayY = Array.CreateInstance(GetType(Single), New Integer() {NumSamples + 1, NumChannels}, New Integer() {0, 1}) ' dimension array for 2D values

		' load cursors from RtChart control
		LoadCustomCursors()

		InitializeRtChart(RtChart1)

		ChartPen.InitChannelsPens(RtChart1)

		InitializePointChannels(RtChart1, NumChannels, NumSamples)

		SelectedChannel = 1
		RtChart1.LogicalChannel = (SelectedChannel)

		' synchronous mode forces paint event to occur on each point and control returns after
		ReflectionHelper.LetMember(RtChart1, "FramesPerSec", -1)

	End Sub

	Private Sub Form_Closed(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MyBase.Closed

		UnloadCustomCursors()

	End Sub

	'UPGRADE_NOTE: (7001) The following declaration (GenerateSineWaveArrays) seems to be dead code More Information: https://www.mobilize.net/vbtonet/ewis/ewi7001
	'Private Sub GenerateSineWaveArrays(ByVal numChannels As Integer, ByVal numSamples As Integer, ByRef VoltsPeak As Single)
		'
		'Dim NumCycles, CycleTime As Single
		'
		' NumSamples is 0-based, NumChannels is 1-based
		'VBDataArrayY = Array.CreateInstance(GetType(Single), New Integer() {NumSamples + 1, NumChannels}, New Integer() {0, 1}) ' dimension array for 2D values
		'
		'Dim RadiansPerSample As Single = TWO_PIE / NumSamples
		'
		' NumCycles determines # of cycles in buffer
		'For 'channel As Integer = 1 To NumChannels
			'NumCycles = ((channel - 1) * RadiansPerSample)
			'
			'For 'sample As Integer = 0 To NumSamples - 1
				'CycleTime = RadiansPerSample + NumCycles
				'VBDataArrayY.SetValue(VoltsPeak * Math.Sin(sample * CycleTime), sample, channel)
				'
			'Next sample
		'Next channel
		'
	'End Sub

	'UPGRADE_NOTE: (7001) The following declaration (RtChart1_MouseDown) seems to be dead code More Information: https://www.mobilize.net/vbtonet/ewis/ewi7001
	'Private Sub RtChart1_MouseDown(ByRef Button As Integer, ByRef Shift As Integer, ByRef X As Single, ByRef Y As Single, ByRef Obj As Integer, ByRef position As Integer, ByRef ObjType As Integer)
		'
		' RtChart Mouse (button) Down event procedure, executes anytime mouse pointer is over Rtchart
		' and mouse button is pressed down. Used to select channels (and cursors) for dragging.
		' Changes plot color from normal to intense
		' Mouse pointer is changed to "size" cursor and restored to "hand" in Mouse Up event procedure.
		'
		'
		'RtChart1.LogicalChannel = (SelectedChannel)
		'
		'ReflectionHelper.LetMember(RtChart1, "MousePointer", CurrentPointer) ' must select default mousepointer to get custom cursors
		'
		' change mouse pointer back to "Hand" on mouse button up when over channel or cursor
		'CurrentPointer = HandCurHandle ' update current pointer value
		'CurrentMousePointer.Text = "Hand"
		'Application.DoEvents()
		'
	'End Sub

	'UPGRADE_NOTE: (7001) The following declaration (RtChart1_MouseMove) seems to be dead code More Information: https://www.mobilize.net/vbtonet/ewis/ewi7001
	'Private Sub RtChart1_MouseMove(ByRef Button As Integer, ByRef Shift As Integer, ByRef X As Single, ByRef Y As Single, ByRef Obj As Integer, ByRef position As Integer, ByRef ObjType As Integer)
		'
		' RtChart Mouse (pointer) Move event procedure, for every mouse pointer movement over Rtchart
		'
		' Convert screen coord in twips to world coordinates (engineering units) for object
		' WndX & WndY properties take twips and return RtChart Window units
		'ReflectionHelper.LetMember(RtChart1, "WndX", X) ' convert twips to window coord
		'X = ReflectionHelper.GetMember(Of Single)(RtChart1, "WndX") ' return world coord
		'
		'ReflectionHelper.LetMember(RtChart1, "WndY", Y) ' convert twips to window coord
		'Y = ReflectionHelper.GetMember(Of Single)(RtChart1, "WndY") ' return world coord
		'
		' update mouse pointer position text boxes with new location
		'MousePosY.Text = CStr(Y)
		'MousePosX.Text = CStr(X)
		'Dim xx(1) As Double
		'Dim yy(1) As Single
		'If Button = 1 Then
			'
			' plot point
			'xx(0) = X
			'yy(0) = Y
			'result = RtChart1.ChartData(ChannelName.Channel1, 1, yy, 0, xx, 0)
			'
			' store point in array, X = "time" (in samples)
			'VBDataArrayY.SetValue(Y, CInt(Math.Floor(CDbl(X))), ChannelName.Channel1)
		'End If
		'
	'End Sub

	'UPGRADE_NOTE: (7001) The following declaration (RtChart1_MouseUp) seems to be dead code More Information: https://www.mobilize.net/vbtonet/ewis/ewi7001
	'Private Sub RtChart1_MouseUp(ByRef Button As Integer, ByRef Shift As Integer, ByRef X As Single, ByRef Y As Single, ByRef Obj As Integer, ByRef position As Integer, ByRef ObjType As Integer)
		'
		' RtChart Mouse (button) Up event procedure, executes anytime mouse pointer is over Rtchart
		' and mouse button is let up. Changes plot color back to normal, Mouse pointer back to "hand" cursor
		' Scroll bars are updated for changes in channel offset
		'
		'
	'End Sub

	Private Sub WriteData_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles WriteData.Click


		FileSystem.FileOpen(1, "rtcdata.txt", OpenMode.Output)

		For sample As Integer = 0 To NumSamples - 1
			FileSystem.WriteLine(1, VBDataArrayY.GetValue(sample, ChannelName.Channel1).ToString())
		Next sample

		FileSystem.FileClose(1)

	End Sub

	Private Sub WriteImage_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles WriteImage.Click
		Dim RtChart_WriteToDisk As Object
		'UPGRADE_WARNING: (1068) RtChart_WriteToDisk of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart1.ChartAction =  ((ChartAction.WriteToDisk))
	End Sub

	'UPGRADE_NOTE: (7001) The following declaration (ZeroChannelOffsets) seems to be dead code More Information: https://www.mobilize.net/vbtonet/ewis/ewi7001
	'Private Sub ZeroChannelOffsets(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal firstChannel As Integer, ByVal lastChannel As Integer)
		'
		'
		'
		' prevent each channel from being re-painted individually on Translate Action
		''UPGRADE_WARNING: (1068) RtChart_DisablePaint of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		'RtChart.ChartAction = ChartAction.DisablePaint
		'
		'For 'channel As Integer = FirstChannel To LastChannel
			'
			' Describe Channel
			'RtChart.LogicalChannel = Channel ' select channel
			'RtChart.ChnDspOffsetY = 0 ' zero offset
			'RtChart.ChnDspOffsetX = 0 ' zero offset
			'
			''UPGRADE_WARNING: (1068) RtChart_ChnTranslate of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
			'RtChart.ChnDspAction = ChannelDisplayAction.Translate ' update channel position
			'
		'Next channel
		'
		' paint entire window with new channel positions for all channels
		''UPGRADE_WARNING: (1068) RtChart_EnablePaint of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		'RtChart.ChartAction = ChartAction.EnablePaint
		'
	'End Sub
End Class