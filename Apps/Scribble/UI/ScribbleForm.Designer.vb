<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
#Region "Upgrade Support "
	Private Shared _Instance As Form1
	Private Shared _InitializingInstance As Boolean
	Public Shared Property DefInstance() As Form1
		Get
			If _Instance Is Nothing OrElse _Instance.IsDisposed Then
				_InitializingInstance = True
				_Instance = CreateInstance()
				_InitializingInstance = False
			End If
			Return _Instance
		End Get
		Set(ByVal value As Form1)
			_Instance = Value
		End Set
	End Property
#End Region
#Region "Windows Form Designer generated code "
	Public Shared Function CreateInstance() As Form1
		Dim theInstance As New Form1()
		theInstance.Form_Load()
		Return theInstance
	End Function
	Private visualControls() As String = New String() {"components", "ToolTipMain", "Label2", "Frame2", "WriteData", "WriteImage", "Command1", "Command2", "CurrentMousePointer", "MousePosX", "MousePosY", "_Label1_4", "_Label1_1", "_Label1_0", "Frame1", "RtChart1", "Label1"}
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTipMain As System.Windows.Forms.ToolTip
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Frame2 As System.Windows.Forms.GroupBox
	Public WithEvents WriteData As System.Windows.Forms.Button
	Public WithEvents WriteImage As System.Windows.Forms.Button
	Public WithEvents Command1 As System.Windows.Forms.Button
	Public WithEvents Command2 As System.Windows.Forms.Button
	Public WithEvents CurrentMousePointer As System.Windows.Forms.TextBox
	Public WithEvents MousePosX As System.Windows.Forms.TextBox
	Public WithEvents MousePosY As System.Windows.Forms.TextBox
	Private WithEvents _Label1_4 As System.Windows.Forms.Label
	Private WithEvents _Label1_1 As System.Windows.Forms.Label
	Private WithEvents _Label1_0 As System.Windows.Forms.Label
	Public WithEvents Frame1 As System.Windows.Forms.GroupBox
	Public WithEvents RtChart1 As isr.Visuals.RealTimeChart.RealTimeChartControl
	Public Label1(4) As System.Windows.Forms.Label
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> _
	 Private Sub InitializeComponent()
		Me.components = New System.ComponentModel.Container()
		Dim resources As New System.ComponentModel.ComponentResourceManager(GetType(Form1))
		Me.ToolTipMain = New System.Windows.Forms.ToolTip(Me.components)
		Me.Frame2 = New System.Windows.Forms.GroupBox()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.Frame1 = New System.Windows.Forms.GroupBox()
		Me.WriteData = New System.Windows.Forms.Button()
		Me.WriteImage = New System.Windows.Forms.Button()
		Me.Command1 = New System.Windows.Forms.Button()
		Me.Command2 = New System.Windows.Forms.Button()
		Me.CurrentMousePointer = New System.Windows.Forms.TextBox()
		Me.MousePosX = New System.Windows.Forms.TextBox()
		Me.MousePosY = New System.Windows.Forms.TextBox()
		Me._Label1_4 = New System.Windows.Forms.Label()
		Me._Label1_1 = New System.Windows.Forms.Label()
		Me._Label1_0 = New System.Windows.Forms.Label()
		Me.RtChart1 = New isr.Visuals.RealTimeChart.RealTimeChartControl()
		Me.Frame2.SuspendLayout()
		Me.Frame1.SuspendLayout()
		Me.SuspendLayout()
		' 
		'Frame2
		' 
		Me.Frame2.AllowDrop = True
		Me.Frame2.BackColor = System.Drawing.SystemColors.Window
		Me.Frame2.Controls.Add(Me.Label2)
		Me.Frame2.Enabled = True
		Me.Frame2.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Frame2.Location = New System.Drawing.Point(280, 228)
		Me.Frame2.Name = "Frame2"
		Me.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame2.Size = New System.Drawing.Size(77, 157)
		Me.Frame2.TabIndex = 9
		Me.Frame2.Text = "Notes"
		Me.Frame2.Visible = True
		' 
		'Label2
		' 
		Me.Label2.AllowDrop = True
		Me.Label2.BackColor = System.Drawing.SystemColors.Window
		Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label2.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label2.Location = New System.Drawing.Point(4, 16)
		Me.Label2.Name = "Label2"
		Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label2.Size = New System.Drawing.Size(69, 137)
		Me.Label2.TabIndex = 10
		Me.Label2.Text = "Click and move mouse to ""draw"" waveform. Data is stored in array and can be plotted."
		' 
		'Frame1
		' 
		Me.Frame1.AllowDrop = True
		Me.Frame1.BackColor = System.Drawing.SystemColors.Window
		Me.Frame1.Controls.Add(Me.WriteData)
		Me.Frame1.Controls.Add(Me.WriteImage)
		Me.Frame1.Controls.Add(Me.Command1)
		Me.Frame1.Controls.Add(Me.Command2)
		Me.Frame1.Controls.Add(Me.CurrentMousePointer)
		Me.Frame1.Controls.Add(Me.MousePosX)
		Me.Frame1.Controls.Add(Me.MousePosY)
		Me.Frame1.Controls.Add(Me._Label1_4)
		Me.Frame1.Controls.Add(Me._Label1_1)
		Me.Frame1.Controls.Add(Me._Label1_0)
		Me.Frame1.Enabled = True
		Me.Frame1.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Frame1.Location = New System.Drawing.Point(8, 228)
		Me.Frame1.Name = "Frame1"
		Me.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame1.Size = New System.Drawing.Size(265, 157)
		Me.Frame1.TabIndex = 0
		Me.Frame1.Text = "Mouse"
		Me.Frame1.Visible = True
		' 
		'WriteData
		' 
		Me.WriteData.AllowDrop = True
		Me.WriteData.BackColor = System.Drawing.SystemColors.Control
		Me.WriteData.ForeColor = System.Drawing.SystemColors.ControlText
		Me.WriteData.Location = New System.Drawing.Point(140, 120)
		Me.WriteData.Name = "WriteData"
		Me.WriteData.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.WriteData.Size = New System.Drawing.Size(89, 29)
		Me.WriteData.TabIndex = 12
		Me.WriteData.Text = " WriteData"
		Me.WriteData.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
		Me.WriteData.UseVisualStyleBackColor = False
		' 
		'WriteImage
		' 
		Me.WriteImage.AllowDrop = True
		Me.WriteImage.BackColor = System.Drawing.SystemColors.Control
		Me.WriteImage.ForeColor = System.Drawing.SystemColors.ControlText
		Me.WriteImage.Location = New System.Drawing.Point(140, 84)
		Me.WriteImage.Name = "WriteImage"
		Me.WriteImage.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.WriteImage.Size = New System.Drawing.Size(89, 29)
		Me.WriteImage.TabIndex = 11
		Me.WriteImage.Text = "WriteImage"
		Me.WriteImage.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
		Me.WriteImage.UseVisualStyleBackColor = False
		' 
		'Command1
		' 
		Me.Command1.AllowDrop = True
		Me.Command1.BackColor = System.Drawing.SystemColors.Control
		Me.Command1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Command1.Location = New System.Drawing.Point(140, 12)
		Me.Command1.Name = "Command1"
		Me.Command1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Command1.Size = New System.Drawing.Size(89, 29)
		Me.Command1.TabIndex = 5
		Me.Command1.Text = "Plot"
		Me.Command1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
		Me.Command1.UseVisualStyleBackColor = False
		' 
		'Command2
		' 
		Me.Command2.AllowDrop = True
		Me.Command2.BackColor = System.Drawing.SystemColors.Control
		Me.Command2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Command2.Location = New System.Drawing.Point(140, 48)
		Me.Command2.Name = "Command2"
		Me.Command2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Command2.Size = New System.Drawing.Size(89, 29)
		Me.Command2.TabIndex = 6
		Me.Command2.Text = "Clear"
		Me.Command2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
		Me.Command2.UseVisualStyleBackColor = False
		' 
		'CurrentMousePointer
		' 
		Me.CurrentMousePointer.AcceptsReturn = True
		Me.CurrentMousePointer.AllowDrop = True
		Me.CurrentMousePointer.BackColor = System.Drawing.SystemColors.Window
		Me.CurrentMousePointer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.CurrentMousePointer.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.CurrentMousePointer.ForeColor = System.Drawing.SystemColors.WindowText
		Me.CurrentMousePointer.Location = New System.Drawing.Point(12, 127)
		Me.CurrentMousePointer.MaxLength = 0
		Me.CurrentMousePointer.Name = "CurrentMousePointer"
		Me.CurrentMousePointer.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.CurrentMousePointer.Size = New System.Drawing.Size(81, 21)
		Me.CurrentMousePointer.TabIndex = 7
		Me.CurrentMousePointer.Text = "Normal"
		' 
		'MousePosX
		' 
		Me.MousePosX.AcceptsReturn = True
		Me.MousePosX.AllowDrop = True
		Me.MousePosX.BackColor = System.Drawing.SystemColors.Window
		Me.MousePosX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.MousePosX.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.MousePosX.ForeColor = System.Drawing.SystemColors.WindowText
		Me.MousePosX.Location = New System.Drawing.Point(12, 84)
		Me.MousePosX.MaxLength = 0
		Me.MousePosX.Name = "MousePosX"
		Me.MousePosX.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.MousePosX.Size = New System.Drawing.Size(81, 21)
		Me.MousePosX.TabIndex = 2
		Me.MousePosX.Text = "0"
		' 
		'MousePosY
		' 
		Me.MousePosY.AcceptsReturn = True
		Me.MousePosY.AllowDrop = True
		Me.MousePosY.BackColor = System.Drawing.SystemColors.Window
		Me.MousePosY.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.MousePosY.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.MousePosY.ForeColor = System.Drawing.SystemColors.WindowText
		Me.MousePosY.Location = New System.Drawing.Point(12, 40)
		Me.MousePosY.MaxLength = 0
		Me.MousePosY.Name = "MousePosY"
		Me.MousePosY.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.MousePosY.Size = New System.Drawing.Size(81, 21)
		Me.MousePosY.TabIndex = 1
		Me.MousePosY.Text = "0"
		' 
		'_Label1_4
		' 
		Me._Label1_4.AllowDrop = True
		Me._Label1_4.BackColor = System.Drawing.SystemColors.Window
		Me._Label1_4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label1_4.ForeColor = System.Drawing.SystemColors.WindowText
		Me._Label1_4.Location = New System.Drawing.Point(12, 112)
		Me._Label1_4.Name = "_Label1_4"
		Me._Label1_4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label1_4.Size = New System.Drawing.Size(89, 13)
		Me._Label1_4.TabIndex = 8
		Me._Label1_4.Text = "Pointer"
		' 
		'_Label1_1
		' 
		Me._Label1_1.AllowDrop = True
		Me._Label1_1.BackColor = System.Drawing.SystemColors.Window
		Me._Label1_1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label1_1.ForeColor = System.Drawing.SystemColors.WindowText
		Me._Label1_1.Location = New System.Drawing.Point(12, 68)
		Me._Label1_1.Name = "_Label1_1"
		Me._Label1_1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label1_1.Size = New System.Drawing.Size(73, 13)
		Me._Label1_1.TabIndex = 4
		Me._Label1_1.Text = "Position X"
		' 
		'_Label1_0
		' 
		Me._Label1_0.AllowDrop = True
		Me._Label1_0.BackColor = System.Drawing.SystemColors.Window
		Me._Label1_0.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label1_0.ForeColor = System.Drawing.SystemColors.WindowText
		Me._Label1_0.Location = New System.Drawing.Point(12, 24)
		Me._Label1_0.Name = "_Label1_0"
		Me._Label1_0.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label1_0.Size = New System.Drawing.Size(73, 13)
		Me._Label1_0.TabIndex = 3
		Me._Label1_0.Text = "Position Y"
		' 
		'RtChart1
		' 
		Me.RtChart1.AutoSize = 0
		Me.RtChart1.AxesColor = 16776960
		Me.RtChart1.AxesTics = 0
		Me.RtChart1.AxesType = 0
		Me.RtChart1.BackColor = 8421376
		Me.RtChart1.BevelInner = 0
		Me.RtChart1.BevelOuter = 2
		Me.RtChart1.BevelWidth_Inner = 1
		Me.RtChart1.BevelWidth_Outer = 1
		Me.RtChart1.BorderColor = (-2147483633)
		Me.RtChart1.BorderWidth = 3
		Me.RtChart1.Chn_Select = 1
		Me.RtChart1.ChnData_Dimension = 1
		Me.RtChart1.ChnDspUserData = 0
		Me.RtChart1.ChnXform_Select = 1
		Me.RtChart1.ColorDepth = 0
		Me.RtChart1.ErrBase = 30200
		Me.RtChart1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.RtChart1.ForeColor = (-842150451)
		Me.RtChart1.FrameColor = 16776960
		Me.RtChart1.FrameOn = -1
		Me.RtChart1.FramesPerSec = 0
		Me.RtChart1.FrameTics = 0
		Me.RtChart1.GridColor = 8421504
		Me.RtChart1.GridOn = -1
		Me.RtChart1.GridSymmetry = 0
		Me.RtChart1.GridType = 0
		Me.RtChart1.HitTest = 0
		Me.RtChart1.ImageFile = String.Empty
		Me.RtChart1.LightColor = (-2147483634)
		Me.RtChart1.Location = New System.Drawing.Point(0, 0)
		Me.RtChart1.MajorDivHorz = 10
		Me.RtChart1.MajorDivVert = 8
		Me.RtChart1.MarginBottom = 10
		Me.RtChart1.MarginLeft = 10
		Me.RtChart1.MarginRight = 10
		Me.RtChart1.MarginTop = 10
		Me.RtChart1.MinorDiv = 5
		Me.RtChart1.MouseIcon = 0
		Me.RtChart1.Name = "RtChart1"
		Me.RtChart1.Outline = -1
		Me.RtChart1.OutlineColor = (-2147483642)
		Me.RtChart1.Pen_Select = 1
		Me.RtChart1.RoundedCorners = 0
		Me.RtChart1.Scale_Select = 1
		Me.RtChart1.ShadowColor = (-2147483632)
		Me.RtChart1.Size = New System.Drawing.Size(361, 225)
		Me.RtChart1.TabIndex = 13
		Me.RtChart1.Viewport_Select = 1
		Me.RtChart1.Viewports = 1
		Me.RtChart1.ViewportStorageColor = 12632256
		Me.RtChart1.ViewportStorageOn = 0
		Me.RtChart1.Wnd_Select = 1
		' 
		'Form1
		' 
		Me.AllowDrop = True
		Me.AutoScaleDimensions = New System.Drawing.SizeF(7, 12)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Window
		Me.ClientSize = New System.Drawing.Size(362, 392)
		Me.Controls.Add(Me.Frame2)
		Me.Controls.Add(Me.Frame1)
		Me.Controls.Add(Me.RtChart1)
		Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.ForeColor = System.Drawing.SystemColors.WindowText
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Location = New System.Drawing.Point(132, 139)
		Me.Location = New System.Drawing.Point(128, 120)
		Me.MaximizeBox = False
		Me.MinimizeBox = True
		Me.Name = "Form1"
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Size = New System.Drawing.Size(368, 413)
		Me.Text = "LabOBJX RT Chart - Scribble Example"
		Me.Frame2.ResumeLayout(False)
		Me.Frame1.ResumeLayout(False)
		Me.ResumeLayout(False)
	End Sub
	Sub ReLoadForm(ByVal addEvents As Boolean)
		InitializeLabel1()
		Form_Load()
		If addEvents Then
			AddHandler MyBase.Closed, AddressOf Me.Form_Closed
		End If
	End Sub
	Sub InitializeLabel1()
		ReDim Label1(4)
		Me.Label1(4) = _Label1_4
		Me.Label1(1) = _Label1_1
		Me.Label1(0) = _Label1_0
	End Sub
#End Region
End Class