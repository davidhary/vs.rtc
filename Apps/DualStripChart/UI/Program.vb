' file:		strpchartbas.vb
' LabOBJX Real-Time Chart - Example Code
' (C) Copyright 1995, Scientific Software Tools, Inc.
' All Rights Reserved.
' summary:	Program class

Option Strict Off
Option Explicit On
Imports System
Imports System.Windows.Forms
Imports isr.Visuals.RealTimeChart

Friend Module program
    Public Const AssemblyTitle As String = "Real Time Chart Strip Chart 1 Demo"
    Public Const AssemblyDescription As String = "Real Time Chart Strip Chart 1 Demo"
    Public Const AssemblyProduct As String = "Visuals.Real.Time.Chart.StripChart1"
    Public result As Integer
    Public Running As Integer
    Public NumSamples As Integer ' number of samples
    Public NumChannels As Integer ' number of channels
    Public TwoDimSampleChannelValue(,) As Single ' Stores multi-channel voltage data values for each sample point
    Public JaggedChannelValues()() As Single ' Stores multi-channel voltage data values for each sample point

    ' signal waveform limits (volts peak)
    Public MaxVolts As Single
    Public MinVolts As Single

    Public VoltsPerDiv As Single ' vertical scaling is based on volts per division
    Public TimePerDiv As Single ' horizontal scaling is based on seconds per division

    Public HScrollBarScaler As Integer

    '     Public Const TWO_PIE As Double = 2 * 3.1415926535898

    Public Const MaximumVoltsPerDivision As Integer = 50
    Public Const MaximumTimePerDivision As Integer = 1000

    ' grid constants
    Public Const HorizontalDivisionsCount As Integer = 20
    Public Const VerticalVisionsCount As Integer = 4
    Public Const MinorDivisionsCount As Integer = 5

    Public Structure ChannelParameter
        Public VoltsPerDivision As Single
        Public TimePerDivision As Single
        Public VoltsOffset As Single
        Public TimeOffset As Single
    End Structure

    Public ChannelParameters() As ChannelParameter = Nothing

    Public Sub EraseChannels(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal firstChannel As Integer, ByVal lastChannel As Integer)

        ' prevent each channel from being erased individually
        rtChart.ChartAction = ChartAction.DisablePaint

        ' erase specified channels
        For channel As Integer = firstChannel To lastChannel
            rtChart.LogicalChannel = Channel ' select channel to erase
            rtChart.ChnDspAction = ChannelDisplayAction.Erase ' use channel erase display action
            rtChart.ChnDspAction = ((ChannelDisplayAction.Flush))
        Next Channel

        ' paint entire window to erase all channels
        rtChart.ChartAction = ChartAction.EnablePaint

    End Sub

    Public Sub InitFonts(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl)

        ' The font properties are global for all RtChart Scales and Labels

        rtChart.FontBold = False
        rtChart.FontItalic = False
        rtChart.FontName = "Arial"
        rtChart.FontSize = 6
        rtChart.FontStrikeout = False
        rtChart.FontUnderline = False

    End Sub

    Public Sub InitHorizontalScale(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal scaleSelect As Integer, ByVal scaleViewport As Integer, ByVal scaleWindow As Integer)

        ' initialize horizontal scale
        rtChart.LogicalScale = scaleSelect ' select scale name (same for both dimensions)
        rtChart.ScaleViewport = scaleViewport ' specify view port before selecting dimension
        rtChart.ScaleWindow = scaleWindow ' specify window to be scaled before selecting dimension
        rtChart.ScaleDimension = LogicalDimension.Horizontal ' select horizontal or vertical scale/label
        rtChart.ScalePen = PenName.White ' specify pen color for scale
        rtChart.ScaleLocation = ScaleLocation.RightBottom ' Scale Location: axis or frame
        rtChart.ScalePosition = Position.BelowRight ' Scale Position: above or below ScaleLocation
        rtChart.ScaleOrientation = Orientation.Parallel ' Scale Orientation: Parallel or Perpendicular to ScaleDimension
        rtChart.ScalePath = Path.Right ' Scale Path: direction which characters follow: left. right, up, down
        rtChart.ScaleGap = 1 ' Scale Gap: space between ScaleLocation and scale/label in pixels
        rtChart.ScaleFactor = 1 ' multiplier used to convert window units to scale values
        rtChart.ScalePlaces = 0 ' number of significant digits to the right of decimal point

        ' update RtChart Label properties
        rtChart.LabelCaption = "Seconds" ' specify Label Caption text: "volts", "secs" , etc.
        rtChart.LabelPen = PenName.White ' specify pen color for Label
        rtChart.LabelLocation = LabelLocation.Center ' label location relative to scale: left, right, center
        rtChart.LabelPosition = Position.BelowRight ' label Position: above, below, or on LabelLocation
        rtChart.LabelOrientation = Orientation.Parallel ' label character orientation relative to ScaleDimension
        rtChart.LabelPath = Path.Right ' Scale Path: direction which characters follow
        rtChart.ScaleVisibility = ScaleVisibility.ScaleAndLabel ' scale visibility: none, scale, label, or both
        'RtChart.ScaleVisibility = RtChart_ScaleOnly
        rtChart.ScaleAction = ScaleAction.Initialize ' execute selected Scale Action

    End Sub

    Public Sub InitializeRtChart(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl)

        ' Global operation properties
        rtChart.ImageFile = String.Empty ' specifies file name for Write action
        rtChart.HitTest = False ' enables mouse pointer "hit" detection
        rtChart.FramesPerSec = 0 ' 0 for asynchronous, -1 for synchronous

        ' Border description properties
        rtChart.BorderColor = Color.LightGray
        rtChart.LightColor = Color.DarkGray
        rtChart.ShadowColor = Color.Black

        rtChart.BevelInner = BevelStyle.Inset
        rtChart.BevelOuter = BevelStyle.Raised
        rtChart.BevelWidthInner = 2
        rtChart.BevelWidthOuter = 2
        rtChart.BorderWidth = 2
        rtChart.Outline = False
        rtChart.OutlineColor = Color.Black
        rtChart.RoundedCorners = False

        ' Graticule description properties
        rtChart.BackColor = Color.DarkCyan
        rtChart.AutoSize = False ' false allows non-square border, grid is always square

        rtChart.FrameOn = True
        rtChart.FrameTics = FrameTicsStyle.MajorMinorFrameTics
        rtChart.FrameColor = Color.White

        rtChart.AxesType = AxesType.HorizontalVertical
        rtChart.AxesTics = AxesTicsStyle.MajorMinorAxesTics
        rtChart.AxesColor = Color.White

        rtChart.GridOn = False
        rtChart.GridType = GridType.Horizontal
        rtChart.GridColor = Color.LightGray

        rtChart.MajorDivHorz = HorizontalDivisionsCount ' major horizontal divisions
        rtChart.MajorDivVert = VerticalVisionsCount ' major vertical divisions
        rtChart.MinorDiv = MinorDivisionsCount ' minor divisions
        rtChart.MarginLeft = 25 ' left Graticule border
        rtChart.MarginTop = 0 ' top Graticule border
        rtChart.MarginRight = 0 ' right Graticule border
        rtChart.MarginBottom = 25 ' bottom Graticule border

        'RtChart.AutoSize = True    ' false allows non-square border, grid is always square

        rtChart.ColorDepth = ColorDepth.Color16 '  256 palette type adapter only windows_g

    End Sub

    Public Sub InitializeScrollChannels(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal channel As Integer, ByVal samplesPerChannel As Integer)

        ' Describe Channel
        rtChart.LogicalChannel = channel ' select channel
        rtChart.ChnDspViewport = 1 ' assign view port to channel
        rtChart.ChnDspWindow = channel ' assign window to channel
        rtChart.ChnDspPen = ChannelName.Cursor1 ' specify pen, same value as channel name
        rtChart.ChnDspBufLen = samplesPerChannel ' total input buffer length
        rtChart.ChnDspMode = ChannelDisplayMode.ScrollRight ' per channel display mode
        rtChart.ChnDspStyle = ChannelDisplayStyle.Lines ' use point style for scroll plotting
        rtChart.ChnDspOffsetX = 0
        rtChart.ChnDspOffsetY = 0
        rtChart.ChannelDataDimension = LogicalDimension.Horizontal
        rtChart.ChnDataShape = ChannelDataShape.AutoIncr
        rtChart.ChnDataOffset = 0 ' offset into buffer (in points)
        rtChart.ChnDataIncr = 1 ' logical increment to next value in array

        ' Describe vertical dimension
        rtChart.ChannelDataDimension = LogicalDimension.Vertical
        rtChart.ChnDataShape = ChannelDataShape.Scalar
        rtChart.ChnDataType = ChannelDataType.Single
        rtChart.ChnDataOffset = 0 ' offset into buffer (in points)
        rtChart.ChnDataIncr = 1 ' logical increment to next value for 2D array
        rtChart.ChnDspAction = ChannelDisplayAction.Initialize

    End Sub

    Public Sub InitializeViewPorts(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl)


        ' View port Description
        rtChart.Viewports = 4 ' number of view ports to be initialized
        rtChart.ViewportStorageColor = Color.Green ' storage mode plot color, same for all view ports

        ' initialize storage mode for each view port
        For channel As Integer = 1 To rtChart.Viewports
            rtChart.LogicalViewport = Channel ' select view port
            rtChart.ViewportStorageOn = False ' disable storage mode
        Next Channel

        rtChart.Viewports = 4 ' number of view ports displayed initially

    End Sub

    Public Sub InitializeWindows(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal window As Integer)

        ' Window Description
        rtChart.LogicalWindow = window ' select window
        rtChart.WndXmin = 0 ' left minimum abscissa
        rtChart.WndWidth = (TimePerDiv * HorizontalDivisionsCount) ' in Seconds (Time/div * 10 divisions)
        rtChart.WndHeight = (VoltsPerDiv * VerticalVisionsCount) ' in Volts (Volts/div * 8 divisions)
        rtChart.WndYmin = rtChart.WndHeight / -2 ' bottom minimum ordinate

    End Sub

    Public Sub InitVerticalScale(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal scaleSelect As Integer, ByVal scaleViewport As Integer, ByVal scaleWindow As Integer)

        ' initialize vertical scale
        rtChart.LogicalScale = scaleSelect ' select scale name (same for both dimensions)
        rtChart.ScaleViewport = scaleViewport ' specify view port before selecting dimension
        rtChart.ScaleWindow = scaleWindow ' specify window to be scaled before selecting dimension
        rtChart.ScaleDimension = LogicalDimension.Vertical ' select horizontal or vertical scale/label
        rtChart.ScalePen = PenName.White ' specify pen color for scale
        rtChart.ScaleLocation = ScaleLocation.LeftTop ' Scale Location: axis or frame
        rtChart.ScalePosition = Position.AboveLeft ' Scale Position: above or below ScaleLocation
        rtChart.ScaleOrientation = Orientation.Perpendicular ' Scale Orientation: Parallel or Perpendicular to ScaleDimension
        rtChart.ScalePath = Path.Right ' Scale Path: direction which characters follow: left. right, up, down
        rtChart.ScaleGap = 1 ' Scale Gap: space between ScaleLocation and scale/label in pixels
        rtChart.ScaleFactor = 1 ' multiplier used to convert window units to scale values
        rtChart.ScalePlaces = 0 ' number of significant digits to the right of decimal point

        ' update RtChart Label properties
        rtChart.LabelCaption = "Volts" ' specify Label Caption text: "volts", "secs" , etc.
        rtChart.LabelPen = PenName.White ' specify pen color for Label

        rtChart.LabelLocation = LabelLocation.Center ' label location relative to scale: left, right, center
        rtChart.LabelPosition = Position.AboveLeft ' label Position: above, below, or on LabelLocation
        rtChart.LabelOrientation = Orientation.Perpendicular ' label character orientation relative to ScaleDimension
        rtChart.LabelPath = Path.Down ' Scale Path: direction which characters follow
        rtChart.ScaleVisibility = ScaleVisibility.ScaleAndLabel ' scale visibility: none, scale, label, or both
        rtChart.ScaleAction = ScaleAction.Initialize ' execute selected Scale Action

    End Sub

    Public Sub PlotDataScroll(ByVal numChannels As Integer, ByVal numSamples As Integer, ByVal vbArray(,) As Single)

        Dim nPoints As Integer = 1

        Do While program.Running

            For i As Integer = 0 To numSamples - 1 Step nPoints

                For channel As Integer = ChannelName.Channel1 To numChannels
                    ' plot 1 point for each call
                    result = StripChartForm.DefInstance.Charts(Channel).ChartData(Channel, nPoints, vbArray, i + (Channel - 1) * numSamples, 0, 0)
                Next Channel

                Dim sw As Stopwatch = Stopwatch.StartNew
                Do While sw.ElapsedMilliseconds < (TimePerDiv / nPoints)
                    Application.DoEvents() ' windows must have control to process paint, mouse and keyboard events
                Loop
                If Not Running Then Exit Sub

            Next i

        Loop

    End Sub

    Public Sub PlotDataScroll(ByVal numChannels As Integer, ByVal numSamples As Integer, ByVal vbArray()() As Single)

        Dim nPoints As Integer = 1

        Do While program.Running

            For i As Integer = 0 To numSamples - 1 Step nPoints

                For channel As Integer = ChannelName.Channel1 To numChannels
                    ' plot 1 point for each call
                    result = StripChartForm.DefInstance.Charts(channel).ChartDataSingle(channel, nPoints, vbArray, i)
                Next channel

                Dim sw As Stopwatch = Stopwatch.StartNew
                Do While sw.ElapsedMilliseconds < (TimePerDiv / nPoints)
                    Application.DoEvents() ' windows must have control to process paint, mouse and keyboard events
                Loop
                If Not Running Then Exit Sub

            Next i

        Loop

    End Sub

    Public Sub UpdateWindowHeight(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal windowSelect As Integer, ByVal unitsPerDivision As Single)


        ' Window Description
        rtChart.LogicalWindow = windowSelect ' select window
        rtChart.WndHeight = unitsPerDivision * rtChart.MajorDivVert ' in Volts (assume V/div * 8 divisions)
        rtChart.WndYmin = rtChart.WndHeight / -2 ' bottom minimum ordinate

        rtChart.ChartAction = ChartAction.Rescale

    End Sub

    Public Sub UpdateWindowWidth(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal windowSelect As Integer, ByVal unitsPerDivision As Single)


        ' Window Description
        rtChart.LogicalWindow = windowSelect ' select window
        rtChart.WndWidth = unitsPerDivision * rtChart.MajorDivHorz ' window width in number of samples
        rtChart.WndXmin = 0 ' left minimum abscissa

        rtChart.ChartAction = ChartAction.Rescale

    End Sub

    Public Sub ZeroChannelOffsets(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal channel As Integer)

        ' prevent each channel from being re-painted individually on Translate Action
        rtChart.ChartAction = ChartAction.DisablePaint

        ' Describe Channel
        rtChart.LogicalChannel = channel ' select channel
        rtChart.ChnDspOffsetY = 0 ' zero offset
        rtChart.ChnDspOffsetX = 0 ' zero offset

        rtChart.ChnDspAction = ChannelDisplayAction.Translate ' update channel position

        ' paint entire window with new channel positions for all channels
        rtChart.ChartAction = ChartAction.EnablePaint

    End Sub
End Module