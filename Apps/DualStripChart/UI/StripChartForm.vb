' LabOBJX Real-Time Chart - Example Code
' (C) Copyright 1995, Scientific Software Tools, Inc.
' All Rights Reserved.
Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic
Imports System
Imports System.ComponentModel
Imports System.Windows.Forms
Imports isr.Visuals.RealTimeChart

''' <summary> Form for viewing the strip chart. </summary>
''' <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 6/28/2019 </para></remarks>
Partial Friend Class StripChartForm
    Inherits System.Windows.Forms.Form

#Region " CONSTRUCTION and CLEANUP "

    Private ReadOnly _IsInitializingComponent As Boolean

    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub New()
        MyBase.New()
        _IsInitializingComponent = True
        If _Instance Is Nothing Then
            If _InitializingInstance Then
                _Instance = Me
            Else
                Try
                    'For the start-up form, the first instance created is the default instance.
                    If Not (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint Is Nothing) AndAlso
                        System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType Is Me.GetType() Then
                        _Instance = Me
                    End If
                Catch
                End Try
            End If
        End If
        'This call is required by the Windows Form Designer.
        InitializeComponent()
        _IsInitializingComponent = False
        If Not Me.DesignMode Then ReLoadForm()
    End Sub

    Private Shared _Instance As StripChartForm
    Private Shared _InitializingInstance As Boolean
    Public Shared Property DefInstance() As StripChartForm
        Get
            If _Instance Is Nothing OrElse _Instance.IsDisposed Then
                _InitializingInstance = True
                _Instance = CreateInstance()
                _InitializingInstance = False
            End If
            Return _Instance
        End Get
        Set(ByVal value As StripChartForm)
            _Instance = value
        End Set
    End Property
    Public Shared Function CreateInstance() As StripChartForm
        Dim theInstance As New StripChartForm()
        theInstance.Form_Load()
        Return theInstance
    End Function

#End Region

#Region " INITIALIZATION "

    Public Sub ReLoadForm()
        InitializeVoltsDivTextBoxes()
        InitializeTimeDivTextBoxes()
        InitializeVoltsOffsetScrollBars()
        InitializeVoltsDivScrollBars()
        InitializeTimeDivScrollBars()
        InitializeCharts()
        Form_Load()
    End Sub

    Public Sub InitializeVoltsDivTextBoxes()
        ReDim VoltsDivTextBoxs(2)
        Me.VoltsDivTextBoxs(1) = _VoltsDivTextBox1
        Me.VoltsDivTextBoxs(2) = _VoltsDivTextBox2
    End Sub

    Public Sub InitializeTimeDivTextBoxes()
        ReDim TimeDivTextBoxes(2)
        Me.TimeDivTextBoxes(1) = _TimeDivTextBox1
        Me.TimeDivTextBoxes(2) = _TimeDivTextBox2
    End Sub

    Public Sub InitializeVoltsOffsetScrollBars()
        ReDim VoltsOffsetScrollBars(2)
        Me.VoltsOffsetScrollBars(2) = _VoltsOffsetScrollBar2
        Me.VoltsOffsetScrollBars(1) = _VoltsOffsetScrollBar1
    End Sub

    Public Sub InitializeVoltsDivScrollBars()
        ReDim VoltsDivScrollBars(2)
        Me.VoltsDivScrollBars(2) = _VoltsDivScrollBar2
        Me.VoltsDivScrollBars(1) = _VoltsDivScrollBar1
    End Sub

    Public Sub InitializeTimeDivScrollBars()
        ReDim TimeDivScrollBars(2)
        Me.TimeDivScrollBars(2) = _TimeDivScrollBar2
        Me.TimeDivScrollBars(1) = _TimeDivScrollBar1
    End Sub

    Public Sub InitializeCharts()
        ReDim Charts(2)
        Me.Charts(2) = _Chart2
        Me.Charts(1) = _Chart1

        Me._Chart2.Name = "_Chart2"
        Me._Chart2.ResetKnownState()
        Me._Chart2.Size = New System.Drawing.Size(433, 137)
        Me._Chart2.Location = New System.Drawing.Point(16, 136)
        Me._Chart2.TabIndex = 21

        Me._Chart1.Name = "_Chart1"
        Me._Chart1.ResetKnownState()
        Me._Chart1.Size = New System.Drawing.Size(433, 137)
        Me._Chart1.Location = New System.Drawing.Point(16, 0)
        Me._Chart1.TabIndex = 20
    End Sub

#End Region

#Region " FORM EVENTS "

    Private Sub Form_Load()

        ' initialize program's global variables
        InitializeGlobalVariables()

        ' initialize RtChart global properties
        For i As Integer = ChannelName.Channel1 To NumChannels
            InitializeRtChart(Charts(i))

            ' initialize RtChart Windows
            InitializeWindows(Charts(i), i)

            ' initialize RtChart Pens
            ChartPen.InitChannelsPens(Charts(i))

            InitFonts(Charts(i))

            InitializeScrollChannels(Charts(i), i, NumSamples)

        Next i

        ' initialize text boxes with default values
        ' NOTE: these cause change event procedures to execute
        For channel As Integer = ChannelName.Channel1 To NumChannels
            With VoltsDivScrollBars(channel)
                .Value = (.Maximum - (.LargeChange + 1)) - (VoltsPerDiv - 1)
                Me.VoltsDivScroll_ValueChanged(VoltsDivScrollBars(channel), New EventArgs())
            End With
            With TimeDivScrollBars(channel)
                .Value = (.Maximum - (.LargeChange + 1)) - (TimePerDiv - 1)
                Me.TimeDivScroll_ValueChanged(TimeDivScrollBars(channel), New EventArgs())
            End With
        Next channel

        ' generate 2-dimensional array to plot
        GenerateSineWaveArrays(NumChannels, NumSamples, MaxVolts)
        ' center window on screen
        Me.SetBounds((Screen.PrimaryScreen.Bounds.Width - Me.ClientRectangle.Width) / 2, (Screen.PrimaryScreen.Bounds.Height - Me.ClientRectangle.Height) / 2, 0, 0, BoundsSpecified.X Or BoundsSpecified.Y)

    End Sub

    Private Sub Form_Closed(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MyBase.Closed
        Environment.Exit(0)
    End Sub

    Private Shared Sub GenerateSineWaveArrays(ByVal numChannels As Integer, ByVal numSamples As Integer, ByRef voltsPeak As Single)

        Dim NumCycles, CycleTime As Single

        ' NumSamples is 0-based, NumChannels is 1-based
        JaggedChannelValues = New Single(numChannels - 1)() {}

        ReDim program.TwoDimSampleChannelValue(numSamples, numChannels) ' dimension array for 2D array

        Dim RadiansPerSample As Single = 2 * Math.PI / numSamples

        ' NumCycles determines # of cycles in buffer
        For channelNumber As Integer = 1 To numChannels
            NumCycles = channelNumber * RadiansPerSample
            Dim samples As Single() = New Single(numSamples - 1) {}
            For sample As Integer = 0 To numSamples - 1
                CycleTime = RadiansPerSample + NumCycles
                Dim value As Single = voltsPeak * Math.Sin(sample * CycleTime)
                program.TwoDimSampleChannelValue(sample, channelNumber - 1) = value
                samples(sample) = value
            Next sample
            JaggedChannelValues(channelNumber - 1) = samples
        Next channelNumber

    End Sub

    Private Shared Sub InitializeGlobalVariables()

        'initialize variables
        Running = False
        NumChannels = 2

        MaxVolts = 15
        MinVolts = -15
        NumSamples = 200

        VoltsPerDiv = 10
        TimePerDiv = NumSamples / program.HorizontalDivisionsCount ' in samples

        ReDim ChannelParameters(NumChannels)

        For i As Integer = ChannelName.Channel1 To NumChannels
            ChannelParameters(i).VoltsPerDivision = VoltsPerDiv
            ChannelParameters(i).TimePerDivision = TimePerDiv
            ChannelParameters(i).VoltsOffset = 0
            ChannelParameters(i).TimeOffset = 0
        Next i

    End Sub


#End Region

#Region " CONTROL EVENTS "

    Private ReadOnly _TempValue As Integer ' temporary storage variable
    Private ReadOnly _SampleTime As Single ' calculated display time interval

    Private Sub ExitButton_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _ExitButton.Click
        Form_Closed(Me, New CancelEventArgs())
    End Sub

    Private Sub EraseAllButton_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _EraseAllButton.Click
        For i As Integer = ChannelName.Channel1 To NumChannels
            ' prevent each channel from being re-painted individually
            Charts(i).ChartAction = ChartAction.DisablePaint

            ' setup RtChart control properties for scroll plotting
            InitializeScrollChannels(Charts(i), i, NumSamples)

            Charts(i).ChartAction = ChartAction.EnablePaint
        Next i
    End Sub

    ''' <summary> Scroll horizontally. </summary>
    ''' <param name="newScrollValue"> The new scroll value. </param>
    Private Sub ScrollHorizontally(ByVal newScrollValue As Integer)

        ' scroll bar Min, Max, & Value are multiplied by HScrollBarScaler to increase resolution
        ' as these properties are integers and RtChart offsets are single precision
        ' scroll bar Value must be divided by HScrollBarScaler to return it to a ChnDspOffset value
        For channel As Integer = ChannelName.Channel1 To NumChannels

            ' prevent each channel from being re-painted individually
            Charts(channel).ChartAction = ChartAction.DisablePaint

            ' change offset for all channels
            Charts(channel).LogicalChannel = channel
            Charts(channel).ChnDspOffsetX = newScrollValue / HScrollBarScaler
            Charts(channel).ChnDspAction = ChannelDisplayAction.Translate ' update channel position

            ' paint entire window with all channels at once
            Charts(channel).ChartAction = ChartAction.EnablePaint

        Next channel

    End Sub

    'Private Sub RtChart1_Refresh(ByVal Index As Integer, ByVal channel As Integer, ByVal samples As Integer)
    '
    ' RTChart.ChartAction = ReScale causes Refresh events for each displayed channel.
    ' Refresh occurs only if entire chart needs to be redrawn (new plot outside current plot area).
    ' Plots should be re-drawn after ReScale Action to maintain display resolution if changes in
    ' Window height and width are made.
    '
    ' re-draw data channels only
    ' RtChart1(Index).FramesPerSec = -1 ' 0 for asynchronous, -1 for synchronous
    ' result = RtChart1(Index).ChartData(channel, samples, VBDataArrayY3(0,channel), 0)
    ' RtChart1(Index).FramesPerSec = 0 ' 0 for asynchronous, -1 for synchronous
    '
    'End Sub

    Private Sub StartButton_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _StartButton.Click

        If Not program.Running Then

            program.Running = True

            _StartButton.Text = "Stop"

            ' update scroll bar range & reset position
            UpdateScrollBars(0)

            ' call scroll plotting routine
            ' program.PlotDataScroll(NumChannels, NumSamples, TwoDimSampleChannelValue)
            program.PlotDataScroll(NumChannels, NumSamples, JaggedChannelValues)

        Else
            program.Running = False
            _StartButton.Text = "Scroll"
        End If

    End Sub

    Private Sub TimeDivScroll_ValueChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _TimeDivScrollBar2.ValueChanged, _TimeDivScrollBar1.ValueChanged
        If _IsInitializingComponent Then Return
        Dim Index As Integer = Array.IndexOf(TimeDivScrollBars, eventSender)
        With TimeDivScrollBars(Index)
            TimeDivTextBoxes(Index).Text = ((.Maximum - (.LargeChange + 1)) - .Value + 1).ToString()
        End With
    End Sub

    Private Sub VoltsDivScroll_ValueChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _VoltsDivScrollBar2.ValueChanged, _VoltsDivScrollBar1.ValueChanged
        If _IsInitializingComponent Then Return
        Dim Index As Integer = Array.IndexOf(VoltsDivScrollBars, eventSender)
        With VoltsDivScrollBars(Index)
            VoltsDivTextBoxs(Index).Text = ((.Maximum - (.LargeChange + 1)) - .Value + 1).ToString()
        End With
    End Sub

    Private Sub VoltsOffsetScroll_ValueChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _VoltsOffsetScrollBar2.ValueChanged, _VoltsOffsetScrollBar1.ValueChanged
        If _IsInitializingComponent Then Return
        Dim Index As Integer = Array.IndexOf(VoltsOffsetScrollBars, eventSender)
        Charts(Index).LogicalChannel = Index
        ChannelParameters(Index).VoltsOffset = VoltsOffsetScrollBars(Index).Value
        Charts(Index).ChnDspOffsetY = ChannelParameters(Index).VoltsOffset
        Charts(Index).ChnDspAction = ChannelDisplayAction.Translate ' update channel position
    End Sub

    Private Sub TimeDivTextBox_TextChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _TimeDivTextBox1.TextChanged, _TimeDivTextBox2.TextChanged

        If _IsInitializingComponent Then Return
        Dim Index As Integer = Array.IndexOf(Me.TimeDivTextBoxes, eventSender)
        ' prevents NULL or disallowed values from being used
        If String.IsNullOrEmpty(TimeDivTextBoxes(Index).Text) Or Conversion.Val(TimeDivTextBoxes(Index).Text) <= 0 Then Exit Sub

        ' get value if entered by keyboard
        ChannelParameters(Index).TimePerDivision = CInt(TimeDivTextBoxes(Index).Text)

        ' prevent intermediate changes from being displayed
        Charts(Index).ChartAction = ChartAction.DisablePaint

        ' update width properties for time window
        UpdateWindowWidth(Charts(Index), Index, ChannelParameters(Index).TimePerDivision)

        ' zero channel offsets after change in window height
        ZeroChannelOffsets(Charts(Index), Index)

        ' update scroll bar range & reset position
        UpdateScrollBars(0)

        InitHorizontalScale(Charts(Index), Index, 1, Index)

        ' paint new trace after all changes are made
        Charts(Index).ChartAction = ChartAction.EnablePaint

    End Sub

    Private Sub UpdateScrollBars(ByVal horizontalOffset As Single)
        Dim newLargeChange As Integer

        ' called by RtChart MouseUp procedure to synchronize scroll bar Value with new channel offset
        ' called by volts/div & time/div to rescale scroll bars according to new window size
        ' called to change horizontal scroll bar according to number of samples in plot

        HScrollBarScaler = 1

        ' update horizontal scroll bar Min & Max for change in number of samples
        ' for time scales (0 - t-max), use Rtchart.ChnDspBufLen to allow full range of offset
        _TimeOffsetScroll.Minimum = (-NumSamples) * HScrollBarScaler
        _TimeOffsetScroll.Maximum = (NumSamples * HScrollBarScaler + _TimeOffsetScroll.LargeChange - 1)

        _TimeOffsetScroll.SmallChange = NumSamples * 0.01 * HScrollBarScaler
        newLargeChange = NumSamples * 0.1 * HScrollBarScaler
        _TimeOffsetScroll.Maximum = _TimeOffsetScroll.Maximum + newLargeChange - _TimeOffsetScroll.LargeChange
        _TimeOffsetScroll.LargeChange = newLargeChange

        ' prevent Value from exceeding Min or Max
        horizontalOffset *= HScrollBarScaler
        _TimeOffsetScroll.Value = horizontalOffset

    End Sub

    Private Sub VoltsDivTextBox_TextChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _VoltsDivTextBox1.TextChanged, _VoltsDivTextBox2.TextChanged

        If _IsInitializingComponent Then Return
        Dim Index As Integer = Array.IndexOf(Me.VoltsDivTextBoxs, eventSender)

        ' prevents NULL or disallowed values from being used
        If String.IsNullOrEmpty(VoltsDivTextBoxs(Index).Text) Or Conversion.Val(VoltsDivTextBoxs(Index).Text) <= 0 Then Exit Sub

        ' get value if entered by keyboard
        ChannelParameters(Index).VoltsPerDivision = CSng(VoltsDivTextBoxs(Index).Text)

        ' prevent intermediate changes from being displayed
        Charts(Index).ChartAction = ChartAction.DisablePaint

        ' update height properties for time window
        UpdateWindowHeight(Charts(Index), Index, ChannelParameters(Index).VoltsPerDivision)

        ' zero channel offsets after change in window height
        ZeroChannelOffsets(Charts(Index), Index)

        InitVerticalScale(Charts(Index), Index, 1, Index)

        ' paint new trace after all changes are made
        Charts(Index).ChartAction = ChartAction.EnablePaint

    End Sub

    Private Sub TimeOffsetScroll_Scroll(ByVal eventSender As Object, ByVal eventArgs As ScrollEventArgs) Handles _TimeOffsetScroll.Scroll
        If _IsInitializingComponent Then Return
        Select Case eventArgs.Type
            Case ScrollEventType.ThumbTrack
                Me.ScrollHorizontally(eventArgs.NewValue)
        End Select
    End Sub

#End Region

End Class
