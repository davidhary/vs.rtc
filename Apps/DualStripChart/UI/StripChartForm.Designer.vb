<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class StripChartForm

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Private WithEvents _VoltsOffsetScrollBar2 As System.Windows.Forms.VScrollBar
    Private WithEvents _VoltsOffsetScrollBar1 As System.Windows.Forms.VScrollBar
    Private WithEvents _TimeDivScrollBar2 As System.Windows.Forms.VScrollBar
    Private WithEvents _TimeDivScrollBar1 As System.Windows.Forms.VScrollBar
    Private WithEvents _VoltsDivScrollBar2 As System.Windows.Forms.VScrollBar
    Private WithEvents _VoltsDivScrollBar1 As System.Windows.Forms.VScrollBar
    Private WithEvents _TimeDivTextBox1 As System.Windows.Forms.TextBox
    Private WithEvents _VoltsDivTextBox1 As System.Windows.Forms.TextBox
    Private WithEvents _VoltsDivTextBox2 As System.Windows.Forms.TextBox
    Private WithEvents _TimeDivTextBox2 As System.Windows.Forms.TextBox
    Public WithEvents _TimeOffsetScroll As System.Windows.Forms.HScrollBar
    Public WithEvents _StartButton As System.Windows.Forms.Button
    Public WithEvents _EraseAllButton As System.Windows.Forms.Button
    Public WithEvents _ExitButton As System.Windows.Forms.Button
    Private WithEvents _Chart2 As isr.Visuals.RealTimeChart.RealTimeChartControl
    Private WithEvents _Chart1 As isr.Visuals.RealTimeChart.RealTimeChartControl
    Public WithEvents _TimeDivTextBox1Label As System.Windows.Forms.Label
    Public WithEvents _VoltsDivTextBox1Label As System.Windows.Forms.Label
    Public WithEvents _RtChart1Label As System.Windows.Forms.Label
    Public WithEvents _RtChart2Label As System.Windows.Forms.Label
    Public WithEvents _VoltsDivTextBox2Label As System.Windows.Forms.Label
    Public WithEvents _TimeDivTextBox2Label As System.Windows.Forms.Label
    Public Charts(2) As isr.Visuals.RealTimeChart.RealTimeChartControl
    Public TimeDivScrollBars(2) As System.Windows.Forms.VScrollBar
    Public VoltsDivScrollBars(2) As System.Windows.Forms.VScrollBar
    Public VoltsOffsetScrollBars(2) As System.Windows.Forms.VScrollBar
    Public TimeDivTextBoxes(2) As System.Windows.Forms.TextBox
    Public VoltsDivTextBoxs(2) As System.Windows.Forms.TextBox
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(StripChartForm))
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._VoltsOffsetScrollBar2 = New System.Windows.Forms.VScrollBar()
        Me._VoltsOffsetScrollBar1 = New System.Windows.Forms.VScrollBar()
        Me._TimeDivScrollBar2 = New System.Windows.Forms.VScrollBar()
        Me._TimeDivScrollBar1 = New System.Windows.Forms.VScrollBar()
        Me._VoltsDivScrollBar2 = New System.Windows.Forms.VScrollBar()
        Me._VoltsDivScrollBar1 = New System.Windows.Forms.VScrollBar()
        Me._TimeDivTextBox1 = New System.Windows.Forms.TextBox()
        Me._VoltsDivTextBox1 = New System.Windows.Forms.TextBox()
        Me._VoltsDivTextBox2 = New System.Windows.Forms.TextBox()
        Me._TimeDivTextBox2 = New System.Windows.Forms.TextBox()
        Me._TimeOffsetScroll = New System.Windows.Forms.HScrollBar()
        Me._StartButton = New System.Windows.Forms.Button()
        Me._EraseAllButton = New System.Windows.Forms.Button()
        Me._ExitButton = New System.Windows.Forms.Button()
        Me._Chart2 = New isr.Visuals.RealTimeChart.RealTimeChartControl()
        Me._Chart1 = New isr.Visuals.RealTimeChart.RealTimeChartControl()
        Me._TimeDivTextBox1Label = New System.Windows.Forms.Label()
        Me._VoltsDivTextBox1Label = New System.Windows.Forms.Label()
        Me._RtChart1Label = New System.Windows.Forms.Label()
        Me._RtChart2Label = New System.Windows.Forms.Label()
        Me._VoltsDivTextBox2Label = New System.Windows.Forms.Label()
        Me._TimeDivTextBox2Label = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        '_VoltsOffsetScroll2
        '
        Me._VoltsOffsetScrollBar2.AllowDrop = True
        Me._VoltsOffsetScrollBar2.LargeChange = 1
        Me._VoltsOffsetScrollBar2.Location = New System.Drawing.Point(433, 136)
        Me._VoltsOffsetScrollBar2.Maximum = 50
        Me._VoltsOffsetScrollBar2.Minimum = -50
        Me._VoltsOffsetScrollBar2.Name = "_VoltsOffsetScroll2"
        Me._VoltsOffsetScrollBar2.Size = New System.Drawing.Size(17, 136)
        Me._VoltsOffsetScrollBar2.TabIndex = 19
        Me._VoltsOffsetScrollBar2.TabStop = True
        '
        '_VoltsOffsetScroll1
        '
        Me._VoltsOffsetScrollBar1.AllowDrop = True
        Me._VoltsOffsetScrollBar1.LargeChange = 1
        Me._VoltsOffsetScrollBar1.Location = New System.Drawing.Point(433, 0)
        Me._VoltsOffsetScrollBar1.Maximum = 50
        Me._VoltsOffsetScrollBar1.Minimum = -50
        Me._VoltsOffsetScrollBar1.Name = "_VoltsOffsetScroll1"
        Me._VoltsOffsetScrollBar1.Size = New System.Drawing.Size(17, 136)
        Me._VoltsOffsetScrollBar1.TabIndex = 18
        Me._VoltsOffsetScrollBar1.TabStop = True
        '
        '_TimeDivScroll2
        '
        Me._TimeDivScrollBar2.AllowDrop = True
        Me._TimeDivScrollBar2.LargeChange = 1
        Me._TimeDivScrollBar2.Location = New System.Drawing.Point(505, 232)
        Me._TimeDivScrollBar2.Maximum = 1000
        Me._TimeDivScrollBar2.Minimum = 1
        Me._TimeDivScrollBar2.Name = "_TimeDivScroll2"
        Me._TimeDivScrollBar2.Size = New System.Drawing.Size(17, 25)
        Me._TimeDivScrollBar2.TabIndex = 17
        Me._TimeDivScrollBar2.TabStop = True
        Me._TimeDivScrollBar2.Value = 1
        '
        '_TimeDivScroll1
        '
        Me._TimeDivScrollBar1.AllowDrop = True
        Me._TimeDivScrollBar1.LargeChange = 1
        Me._TimeDivScrollBar1.Location = New System.Drawing.Point(505, 94)
        Me._TimeDivScrollBar1.Maximum = 1000
        Me._TimeDivScrollBar1.Minimum = 1
        Me._TimeDivScrollBar1.Name = "_TimeDivScroll1"
        Me._TimeDivScrollBar1.Size = New System.Drawing.Size(17, 25)
        Me._TimeDivScrollBar1.TabIndex = 16
        Me._TimeDivScrollBar1.TabStop = True
        Me._TimeDivScrollBar1.Value = 1
        '
        '_VoltsDivScroll2
        '
        Me._VoltsDivScrollBar2.AllowDrop = True
        Me._VoltsDivScrollBar2.LargeChange = 1
        Me._VoltsDivScrollBar2.Location = New System.Drawing.Point(505, 171)
        Me._VoltsDivScrollBar2.Maximum = 50
        Me._VoltsDivScrollBar2.Minimum = 1
        Me._VoltsDivScrollBar2.Name = "_VoltsDivScroll2"
        Me._VoltsDivScrollBar2.Size = New System.Drawing.Size(17, 25)
        Me._VoltsDivScrollBar2.TabIndex = 15
        Me._VoltsDivScrollBar2.TabStop = True
        Me._VoltsDivScrollBar2.Value = 1
        '
        '_VoltsDivScroll1
        '
        Me._VoltsDivScrollBar1.AllowDrop = True
        Me._VoltsDivScrollBar1.LargeChange = 1
        Me._VoltsDivScrollBar1.Location = New System.Drawing.Point(505, 38)
        Me._VoltsDivScrollBar1.Maximum = 50
        Me._VoltsDivScrollBar1.Minimum = 1
        Me._VoltsDivScrollBar1.Name = "_VoltsDivScroll1"
        Me._VoltsDivScrollBar1.Size = New System.Drawing.Size(17, 25)
        Me._VoltsDivScrollBar1.TabIndex = 14
        Me._VoltsDivScrollBar1.TabStop = True
        Me._VoltsDivScrollBar1.Value = 1
        '
        '_TimeDivTextBox1
        '
        Me._TimeDivTextBox1.AcceptsReturn = True
        Me._TimeDivTextBox1.AllowDrop = True
        Me._TimeDivTextBox1.BackColor = System.Drawing.Color.Silver
        Me._TimeDivTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._TimeDivTextBox1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._TimeDivTextBox1.ForeColor = System.Drawing.SystemColors.WindowText
        Me._TimeDivTextBox1.Location = New System.Drawing.Point(455, 96)
        Me._TimeDivTextBox1.MaxLength = 0
        Me._TimeDivTextBox1.Name = "_TimeDivTextBox1"
        Me._TimeDivTextBox1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._TimeDivTextBox1.Size = New System.Drawing.Size(49, 20)
        Me._TimeDivTextBox1.TabIndex = 7
        Me._TimeDivTextBox1.Text = "Text1"
        '
        '_VoltsDivTextBox1
        '
        Me._VoltsDivTextBox1.AcceptsReturn = True
        Me._VoltsDivTextBox1.AllowDrop = True
        Me._VoltsDivTextBox1.BackColor = System.Drawing.Color.Silver
        Me._VoltsDivTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._VoltsDivTextBox1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._VoltsDivTextBox1.ForeColor = System.Drawing.SystemColors.WindowText
        Me._VoltsDivTextBox1.Location = New System.Drawing.Point(455, 40)
        Me._VoltsDivTextBox1.MaxLength = 0
        Me._VoltsDivTextBox1.Name = "_VoltsDivTextBox1"
        Me._VoltsDivTextBox1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._VoltsDivTextBox1.Size = New System.Drawing.Size(49, 20)
        Me._VoltsDivTextBox1.TabIndex = 6
        Me._VoltsDivTextBox1.Text = "Text1"
        '
        '_VoltsDivTextBox2
        '
        Me._VoltsDivTextBox2.AcceptsReturn = True
        Me._VoltsDivTextBox2.AllowDrop = True
        Me._VoltsDivTextBox2.BackColor = System.Drawing.Color.Silver
        Me._VoltsDivTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._VoltsDivTextBox2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._VoltsDivTextBox2.ForeColor = System.Drawing.SystemColors.WindowText
        Me._VoltsDivTextBox2.Location = New System.Drawing.Point(455, 173)
        Me._VoltsDivTextBox2.MaxLength = 0
        Me._VoltsDivTextBox2.Name = "_VoltsDivTextBox2"
        Me._VoltsDivTextBox2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._VoltsDivTextBox2.Size = New System.Drawing.Size(49, 20)
        Me._VoltsDivTextBox2.TabIndex = 5
        Me._VoltsDivTextBox2.Text = "Text1"
        '
        '_TimeDivTextBox2
        '
        Me._TimeDivTextBox2.AcceptsReturn = True
        Me._TimeDivTextBox2.AllowDrop = True
        Me._TimeDivTextBox2.BackColor = System.Drawing.Color.Silver
        Me._TimeDivTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._TimeDivTextBox2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._TimeDivTextBox2.ForeColor = System.Drawing.SystemColors.WindowText
        Me._TimeDivTextBox2.Location = New System.Drawing.Point(455, 234)
        Me._TimeDivTextBox2.MaxLength = 0
        Me._TimeDivTextBox2.Name = "_TimeDivTextBox2"
        Me._TimeDivTextBox2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._TimeDivTextBox2.Size = New System.Drawing.Size(49, 20)
        Me._TimeDivTextBox2.TabIndex = 4
        Me._TimeDivTextBox2.Text = "Text1"
        '
        '_TimeOffsetScroll
        '
        Me._TimeOffsetScroll.AllowDrop = True
        Me._TimeOffsetScroll.LargeChange = 1
        Me._TimeOffsetScroll.Location = New System.Drawing.Point(1, 272)
        Me._TimeOffsetScroll.Maximum = 32767
        Me._TimeOffsetScroll.Name = "_TimeOffsetScroll"
        Me._TimeOffsetScroll.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._TimeOffsetScroll.Size = New System.Drawing.Size(431, 21)
        Me._TimeOffsetScroll.TabIndex = 3
        Me._TimeOffsetScroll.TabStop = True
        Me._TimeOffsetScroll.Value = 16384
        '
        '_StartButton
        '
        Me._StartButton.AllowDrop = True
        Me._StartButton.BackColor = System.Drawing.SystemColors.Control
        Me._StartButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._StartButton.Location = New System.Drawing.Point(353, 296)
        Me._StartButton.Name = "_StartButton"
        Me._StartButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._StartButton.Size = New System.Drawing.Size(77, 25)
        Me._StartButton.TabIndex = 2
        Me._StartButton.Text = "Run"
        Me._StartButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me._StartButton.UseVisualStyleBackColor = False
        '
        '_EraseAllButton
        '
        Me._EraseAllButton.AllowDrop = True
        Me._EraseAllButton.BackColor = System.Drawing.SystemColors.Control
        Me._EraseAllButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._EraseAllButton.Location = New System.Drawing.Point(178, 296)
        Me._EraseAllButton.Name = "_EraseAllButton"
        Me._EraseAllButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._EraseAllButton.Size = New System.Drawing.Size(77, 25)
        Me._EraseAllButton.TabIndex = 1
        Me._EraseAllButton.Text = "Erase"
        Me._EraseAllButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me._EraseAllButton.UseVisualStyleBackColor = False
        '
        '_ExitButton
        '
        Me._ExitButton.AllowDrop = True
        Me._ExitButton.BackColor = System.Drawing.SystemColors.Control
        Me._ExitButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._ExitButton.Location = New System.Drawing.Point(1, 296)
        Me._ExitButton.Name = "_ExitButton"
        Me._ExitButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ExitButton.Size = New System.Drawing.Size(77, 25)
        Me._ExitButton.TabIndex = 0
        Me._ExitButton.Text = "Exit"
        Me._ExitButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me._ExitButton.UseVisualStyleBackColor = False
        '
        '_RtChart2
        '
        Me._Chart2.Location = New System.Drawing.Point(1, 136)
        Me._Chart2.Name = "_RtChart2"
        Me._Chart2.Size = New System.Drawing.Size(431, 136)
        Me._Chart2.TabIndex = 20
        '
        '_RtChart1
        '
        Me._Chart1.Location = New System.Drawing.Point(1, 0)
        Me._Chart1.Name = "_RtChart1"
        Me._Chart1.Size = New System.Drawing.Size(431, 136)
        Me._Chart1.TabIndex = 21
        '
        '_TimeDivTextBox1Label
        '
        Me._TimeDivTextBox1Label.AllowDrop = True
        Me._TimeDivTextBox1Label.AutoSize = True
        Me._TimeDivTextBox1Label.BackColor = System.Drawing.Color.Silver
        Me._TimeDivTextBox1Label.ForeColor = System.Drawing.Color.Black
        Me._TimeDivTextBox1Label.Location = New System.Drawing.Point(455, 78)
        Me._TimeDivTextBox1Label.Name = "_TimeDivTextBox1Label"
        Me._TimeDivTextBox1Label.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._TimeDivTextBox1Label.Size = New System.Drawing.Size(59, 13)
        Me._TimeDivTextBox1Label.TabIndex = 13
        Me._TimeDivTextBox1Label.Text = "Time/Div"
        '
        '_VoltsDivTextBox1Label
        '
        Me._VoltsDivTextBox1Label.AllowDrop = True
        Me._VoltsDivTextBox1Label.AutoSize = True
        Me._VoltsDivTextBox1Label.BackColor = System.Drawing.Color.Silver
        Me._VoltsDivTextBox1Label.ForeColor = System.Drawing.Color.Black
        Me._VoltsDivTextBox1Label.Location = New System.Drawing.Point(455, 22)
        Me._VoltsDivTextBox1Label.Name = "_VoltsDivTextBox1Label"
        Me._VoltsDivTextBox1Label.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._VoltsDivTextBox1Label.Size = New System.Drawing.Size(60, 13)
        Me._VoltsDivTextBox1Label.TabIndex = 12
        Me._VoltsDivTextBox1Label.Text = "Volts/Div"
        '
        '_RtChart1Label
        '
        Me._RtChart1Label.AllowDrop = True
        Me._RtChart1Label.AutoSize = True
        Me._RtChart1Label.BackColor = System.Drawing.Color.Silver
        Me._RtChart1Label.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._RtChart1Label.ForeColor = System.Drawing.Color.Black
        Me._RtChart1Label.Location = New System.Drawing.Point(453, 1)
        Me._RtChart1Label.Name = "_RtChart1Label"
        Me._RtChart1Label.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._RtChart1Label.Size = New System.Drawing.Size(16, 15)
        Me._RtChart1Label.TabIndex = 11
        Me._RtChart1Label.Text = "1"
        Me._RtChart1Label.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        '_RtChart2Label
        '
        Me._RtChart2Label.AllowDrop = True
        Me._RtChart2Label.AutoSize = True
        Me._RtChart2Label.BackColor = System.Drawing.Color.Silver
        Me._RtChart2Label.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._RtChart2Label.ForeColor = System.Drawing.Color.Black
        Me._RtChart2Label.Location = New System.Drawing.Point(453, 136)
        Me._RtChart2Label.Name = "_RtChart2Label"
        Me._RtChart2Label.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._RtChart2Label.Size = New System.Drawing.Size(16, 15)
        Me._RtChart2Label.TabIndex = 10
        Me._RtChart2Label.Text = "2"
        Me._RtChart2Label.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        '_VoltsDivTextBox2Label
        '
        Me._VoltsDivTextBox2Label.AllowDrop = True
        Me._VoltsDivTextBox2Label.AutoSize = True
        Me._VoltsDivTextBox2Label.BackColor = System.Drawing.Color.Silver
        Me._VoltsDivTextBox2Label.ForeColor = System.Drawing.Color.Black
        Me._VoltsDivTextBox2Label.Location = New System.Drawing.Point(455, 155)
        Me._VoltsDivTextBox2Label.Name = "_VoltsDivTextBox2Label"
        Me._VoltsDivTextBox2Label.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._VoltsDivTextBox2Label.Size = New System.Drawing.Size(60, 13)
        Me._VoltsDivTextBox2Label.TabIndex = 9
        Me._VoltsDivTextBox2Label.Text = "Volts/Div"
        '
        '_TimeDivTextBox2Label
        '
        Me._TimeDivTextBox2Label.AllowDrop = True
        Me._TimeDivTextBox2Label.AutoSize = True
        Me._TimeDivTextBox2Label.BackColor = System.Drawing.Color.Silver
        Me._TimeDivTextBox2Label.ForeColor = System.Drawing.Color.Black
        Me._TimeDivTextBox2Label.Location = New System.Drawing.Point(455, 216)
        Me._TimeDivTextBox2Label.Name = "_TimeDivTextBox2Label"
        Me._TimeDivTextBox2Label.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._TimeDivTextBox2Label.Size = New System.Drawing.Size(59, 13)
        Me._TimeDivTextBox2Label.TabIndex = 8
        Me._TimeDivTextBox2Label.Text = "Time/Div"
        '
        'StripChartForm
        '
        Me.AllowDrop = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Silver
        Me.ClientSize = New System.Drawing.Size(528, 326)
        Me.Controls.Add(Me._VoltsOffsetScrollBar2)
        Me.Controls.Add(Me._VoltsOffsetScrollBar1)
        Me.Controls.Add(Me._TimeDivScrollBar2)
        Me.Controls.Add(Me._TimeDivScrollBar1)
        Me.Controls.Add(Me._VoltsDivScrollBar2)
        Me.Controls.Add(Me._VoltsDivScrollBar1)
        Me.Controls.Add(Me._TimeDivTextBox1)
        Me.Controls.Add(Me._VoltsDivTextBox1)
        Me.Controls.Add(Me._VoltsDivTextBox2)
        Me.Controls.Add(Me._TimeDivTextBox2)
        Me.Controls.Add(Me._TimeOffsetScroll)
        Me.Controls.Add(Me._StartButton)
        Me.Controls.Add(Me._EraseAllButton)
        Me.Controls.Add(Me._ExitButton)
        Me.Controls.Add(Me._Chart2)
        Me.Controls.Add(Me._Chart1)
        Me.Controls.Add(Me._TimeDivTextBox1Label)
        Me.Controls.Add(Me._VoltsDivTextBox1Label)
        Me.Controls.Add(Me._RtChart1Label)
        Me.Controls.Add(Me._RtChart2Label)
        Me.Controls.Add(Me._VoltsDivTextBox2Label)
        Me.Controls.Add(Me._TimeDivTextBox2Label)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.SystemColors.WindowText
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(68, 115)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "StripChartForm"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "LabOBJX Real-Time Chart - Strip Chart Example"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Private WithEvents _ToolTip As ToolTip
End Class