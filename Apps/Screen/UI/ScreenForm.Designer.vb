<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ScreenForm
#Region "Upgrade Support "
	Private Shared _Instance As ScreenForm
	Private Shared _InitializingInstance As Boolean
	Public Shared Property DefInstance() As ScreenForm
		Get
			If _Instance Is Nothing OrElse _Instance.IsDisposed Then
				_InitializingInstance = True
				_Instance = CreateInstance()
				_InitializingInstance = False
			End If
			Return _Instance
		End Get
		Set(ByVal value As ScreenForm)
			_Instance = Value
		End Set
	End Property
#End Region
#Region "Windows Form Designer generated code "
	Public Shared Function CreateInstance() As ScreenForm
		Dim theInstance As New ScreenForm()
		theInstance.Form_Load()
		Return theInstance
	End Function
	Private visualControls() As String = New String() {"components", "ToolTipMain", "DefaultButton", "RtChartWidth", "RtChartHeight", "Label22", "Label21", "Frame11", "MajorDivVertTextBox", "MajorDivHorzTextBox", "MinorDivTextBox", "Label20", "Label19", "Label18", "Frame10", "MarginLeftTextBox", "MarginTopTextBox", "MarginBottomTextBox", "MarginRightTextBox", "Label17", "Label16", "Label15", "Label14", "Frame9", "AutoSizeCheck", "RoundCornersCheck", "OutlineCheck", "BorderWidthTextBox", "Label13", "Frame4", "LightColorComboBox", "OutlineColorComboBox", "ShadowColorComboBox", "BorderColorComboBox", "Label4", "Label5", "Label6", "Label7", "Frame7", "GridColorComboBox", "AxesColorComboBox", "FrameColorComboBox", "BackGroundColorComboBox", "Label8", "Label9", "Label10", "Label11", "Frame8", "BevelStyleOuterComboBox", "BevelStyleInnerComboBox", "Label1", "Label2", "Frame6", "BevelWidthInnerTextBox", "BevelWidthOuterTextBox", "Label12", "Label3", "Frame5", "Frame3", "Viewports4", "Viewports2", "Viewports1", "Frame1", "FrameCheck", "GridCheck", "AxesCheck", "Frame2", "RtChart1"}
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTipMain As System.Windows.Forms.ToolTip
	Public WithEvents DefaultButton As System.Windows.Forms.Button
	Public WithEvents RtChartWidth As System.Windows.Forms.TextBox
	Public WithEvents RtChartHeight As System.Windows.Forms.TextBox
	Public WithEvents Label22 As System.Windows.Forms.Label
	Public WithEvents Label21 As System.Windows.Forms.Label
	Public WithEvents Frame11 As System.Windows.Forms.GroupBox
	Public WithEvents MajorDivVertTextBox As System.Windows.Forms.TextBox
	Public WithEvents MajorDivHorzTextBox As System.Windows.Forms.TextBox
	Public WithEvents MinorDivTextBox As System.Windows.Forms.TextBox
	Public WithEvents Label20 As System.Windows.Forms.Label
	Public WithEvents Label19 As System.Windows.Forms.Label
	Public WithEvents Label18 As System.Windows.Forms.Label
	Public WithEvents Frame10 As System.Windows.Forms.GroupBox
	Public WithEvents MarginLeftTextBox As System.Windows.Forms.TextBox
	Public WithEvents MarginTopTextBox As System.Windows.Forms.TextBox
	Public WithEvents MarginBottomTextBox As System.Windows.Forms.TextBox
	Public WithEvents MarginRightTextBox As System.Windows.Forms.TextBox
	Public WithEvents Label17 As System.Windows.Forms.Label
	Public WithEvents Label16 As System.Windows.Forms.Label
	Public WithEvents Label15 As System.Windows.Forms.Label
	Public WithEvents Label14 As System.Windows.Forms.Label
	Public WithEvents Frame9 As System.Windows.Forms.GroupBox
	Public WithEvents AutoSizeCheck As System.Windows.Forms.CheckBox
	Public WithEvents RoundCornersCheck As System.Windows.Forms.CheckBox
	Public WithEvents OutlineCheck As System.Windows.Forms.CheckBox
	Public WithEvents BorderWidthTextBox As System.Windows.Forms.TextBox
	Public WithEvents Label13 As System.Windows.Forms.Label
	Public WithEvents Frame4 As System.Windows.Forms.GroupBox
	Public WithEvents LightColorComboBox As System.Windows.Forms.ComboBox
	Public WithEvents OutlineColorComboBox As System.Windows.Forms.ComboBox
	Public WithEvents ShadowColorComboBox As System.Windows.Forms.ComboBox
	Public WithEvents BorderColorComboBox As System.Windows.Forms.ComboBox
	Public WithEvents Label4 As System.Windows.Forms.Label
	Public WithEvents Label5 As System.Windows.Forms.Label
	Public WithEvents Label6 As System.Windows.Forms.Label
	Public WithEvents Label7 As System.Windows.Forms.Label
	Public WithEvents Frame7 As System.Windows.Forms.GroupBox
	Public WithEvents GridColorComboBox As System.Windows.Forms.ComboBox
	Public WithEvents AxesColorComboBox As System.Windows.Forms.ComboBox
	Public WithEvents FrameColorComboBox As System.Windows.Forms.ComboBox
	Public WithEvents BackGroundColorComboBox As System.Windows.Forms.ComboBox
	Public WithEvents Label8 As System.Windows.Forms.Label
	Public WithEvents Label9 As System.Windows.Forms.Label
	Public WithEvents Label10 As System.Windows.Forms.Label
	Public WithEvents Label11 As System.Windows.Forms.Label
	Public WithEvents Frame8 As System.Windows.Forms.GroupBox
	Public WithEvents BevelStyleOuterComboBox As System.Windows.Forms.ComboBox
	Public WithEvents BevelStyleInnerComboBox As System.Windows.Forms.ComboBox
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Frame6 As System.Windows.Forms.GroupBox
	Public WithEvents BevelWidthInnerTextBox As System.Windows.Forms.TextBox
	Public WithEvents BevelWidthOuterTextBox As System.Windows.Forms.TextBox
	Public WithEvents Label12 As System.Windows.Forms.Label
	Public WithEvents Label3 As System.Windows.Forms.Label
	Public WithEvents Frame5 As System.Windows.Forms.GroupBox
	Public WithEvents Frame3 As System.Windows.Forms.GroupBox
	Public WithEvents Viewports4 As System.Windows.Forms.RadioButton
	Public WithEvents Viewports2 As System.Windows.Forms.RadioButton
	Public WithEvents Viewports1 As System.Windows.Forms.RadioButton
	Public WithEvents Frame1 As System.Windows.Forms.GroupBox
	Public WithEvents FrameCheck As System.Windows.Forms.CheckBox
	Public WithEvents GridCheck As System.Windows.Forms.CheckBox
	Public WithEvents AxesCheck As System.Windows.Forms.CheckBox
	Public WithEvents Frame2 As System.Windows.Forms.GroupBox
	Public WithEvents RtChart1 As isr.Visuals.RealTimeChart.RealTimeChartControl
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> _
	 Private Sub InitializeComponent()
		Me.components = New System.ComponentModel.Container()
		Dim resources As New System.ComponentModel.ComponentResourceManager(GetType(ScreenForm))
		Me.ToolTipMain = New System.Windows.Forms.ToolTip(Me.components)
		Me.Frame11 = New System.Windows.Forms.GroupBox()
		Me.DefaultButton = New System.Windows.Forms.Button()
		Me.RtChartWidth = New System.Windows.Forms.TextBox()
		Me.RtChartHeight = New System.Windows.Forms.TextBox()
		Me.Label22 = New System.Windows.Forms.Label()
		Me.Label21 = New System.Windows.Forms.Label()
		Me.Frame10 = New System.Windows.Forms.GroupBox()
		Me.MajorDivVertTextBox = New System.Windows.Forms.TextBox()
		Me.MajorDivHorzTextBox = New System.Windows.Forms.TextBox()
		Me.MinorDivTextBox = New System.Windows.Forms.TextBox()
		Me.Label20 = New System.Windows.Forms.Label()
		Me.Label19 = New System.Windows.Forms.Label()
		Me.Label18 = New System.Windows.Forms.Label()
		Me.Frame9 = New System.Windows.Forms.GroupBox()
		Me.MarginLeftTextBox = New System.Windows.Forms.TextBox()
		Me.MarginTopTextBox = New System.Windows.Forms.TextBox()
		Me.MarginBottomTextBox = New System.Windows.Forms.TextBox()
		Me.MarginRightTextBox = New System.Windows.Forms.TextBox()
		Me.Label17 = New System.Windows.Forms.Label()
		Me.Label16 = New System.Windows.Forms.Label()
		Me.Label15 = New System.Windows.Forms.Label()
		Me.Label14 = New System.Windows.Forms.Label()
		Me.Frame4 = New System.Windows.Forms.GroupBox()
		Me.AutoSizeCheck = New System.Windows.Forms.CheckBox()
		Me.RoundCornersCheck = New System.Windows.Forms.CheckBox()
		Me.OutlineCheck = New System.Windows.Forms.CheckBox()
		Me.BorderWidthTextBox = New System.Windows.Forms.TextBox()
		Me.Label13 = New System.Windows.Forms.Label()
		Me.Frame7 = New System.Windows.Forms.GroupBox()
		Me.LightColorComboBox = New System.Windows.Forms.ComboBox()
		Me.OutlineColorComboBox = New System.Windows.Forms.ComboBox()
		Me.ShadowColorComboBox = New System.Windows.Forms.ComboBox()
		Me.BorderColorComboBox = New System.Windows.Forms.ComboBox()
		Me.Label4 = New System.Windows.Forms.Label()
		Me.Label5 = New System.Windows.Forms.Label()
		Me.Label6 = New System.Windows.Forms.Label()
		Me.Label7 = New System.Windows.Forms.Label()
		Me.Frame8 = New System.Windows.Forms.GroupBox()
		Me.GridColorComboBox = New System.Windows.Forms.ComboBox()
		Me.AxesColorComboBox = New System.Windows.Forms.ComboBox()
		Me.FrameColorComboBox = New System.Windows.Forms.ComboBox()
		Me.BackGroundColorComboBox = New System.Windows.Forms.ComboBox()
		Me.Label8 = New System.Windows.Forms.Label()
		Me.Label9 = New System.Windows.Forms.Label()
		Me.Label10 = New System.Windows.Forms.Label()
		Me.Label11 = New System.Windows.Forms.Label()
		Me.Frame3 = New System.Windows.Forms.GroupBox()
		Me.Frame6 = New System.Windows.Forms.GroupBox()
		Me.BevelStyleOuterComboBox = New System.Windows.Forms.ComboBox()
		Me.BevelStyleInnerComboBox = New System.Windows.Forms.ComboBox()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.Frame5 = New System.Windows.Forms.GroupBox()
		Me.BevelWidthInnerTextBox = New System.Windows.Forms.TextBox()
		Me.BevelWidthOuterTextBox = New System.Windows.Forms.TextBox()
		Me.Label12 = New System.Windows.Forms.Label()
		Me.Label3 = New System.Windows.Forms.Label()
		Me.Frame1 = New System.Windows.Forms.GroupBox()
		Me.Viewports4 = New System.Windows.Forms.RadioButton()
		Me.Viewports2 = New System.Windows.Forms.RadioButton()
		Me.Viewports1 = New System.Windows.Forms.RadioButton()
		Me.Frame2 = New System.Windows.Forms.GroupBox()
		Me.FrameCheck = New System.Windows.Forms.CheckBox()
		Me.GridCheck = New System.Windows.Forms.CheckBox()
		Me.AxesCheck = New System.Windows.Forms.CheckBox()
		Me.RtChart1 = New isr.Visuals.RealTimeChart.RealTimeChartControl()
		Me.Frame11.SuspendLayout()
		Me.Frame10.SuspendLayout()
		Me.Frame9.SuspendLayout()
		Me.Frame4.SuspendLayout()
		Me.Frame7.SuspendLayout()
		Me.Frame8.SuspendLayout()
		Me.Frame3.SuspendLayout()
		Me.Frame6.SuspendLayout()
		Me.Frame5.SuspendLayout()
		Me.Frame1.SuspendLayout()
		Me.Frame2.SuspendLayout()
		Me.SuspendLayout()
		' 
		'Frame11
		' 
		Me.Frame11.AllowDrop = True
		Me.Frame11.BackColor = System.Drawing.SystemColors.Window
		Me.Frame11.Controls.Add(Me.DefaultButton)
		Me.Frame11.Controls.Add(Me.RtChartWidth)
		Me.Frame11.Controls.Add(Me.RtChartHeight)
		Me.Frame11.Controls.Add(Me.Label22)
		Me.Frame11.Controls.Add(Me.Label21)
		Me.Frame11.Enabled = True
		Me.Frame11.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Frame11.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Frame11.Location = New System.Drawing.Point(368, 332)
		Me.Frame11.Name = "Frame11"
		Me.Frame11.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame11.Size = New System.Drawing.Size(117, 81)
		Me.Frame11.TabIndex = 61
		Me.Frame11.Text = "RtChart Size"
		Me.Frame11.Visible = True
		' 
		'DefaultButton
		' 
		Me.DefaultButton.AllowDrop = True
		Me.DefaultButton.BackColor = System.Drawing.SystemColors.Control
		Me.DefaultButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.DefaultButton.ForeColor = System.Drawing.SystemColors.ControlText
		Me.DefaultButton.Location = New System.Drawing.Point(16, 52)
		Me.DefaultButton.Name = "DefaultButton"
		Me.DefaultButton.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.DefaultButton.Size = New System.Drawing.Size(81, 25)
		Me.DefaultButton.TabIndex = 64
		Me.DefaultButton.Text = "Reset All"
		Me.DefaultButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
		Me.DefaultButton.UseVisualStyleBackColor = False
		' 
		'RtChartWidth
		' 
		Me.RtChartWidth.AcceptsReturn = True
		Me.RtChartWidth.AllowDrop = True
		Me.RtChartWidth.BackColor = System.Drawing.SystemColors.Window
		Me.RtChartWidth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.RtChartWidth.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.RtChartWidth.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.RtChartWidth.ForeColor = System.Drawing.SystemColors.WindowText
		Me.RtChartWidth.Location = New System.Drawing.Point(64, 28)
		Me.RtChartWidth.MaxLength = 5
		Me.RtChartWidth.Name = "RtChartWidth"
		Me.RtChartWidth.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.RtChartWidth.Size = New System.Drawing.Size(41, 20)
		Me.RtChartWidth.TabIndex = 18
		' 
		'RtChartHeight
		' 
		Me.RtChartHeight.AcceptsReturn = True
		Me.RtChartHeight.AllowDrop = True
		Me.RtChartHeight.BackColor = System.Drawing.SystemColors.Window
		Me.RtChartHeight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.RtChartHeight.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.RtChartHeight.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.RtChartHeight.ForeColor = System.Drawing.SystemColors.WindowText
		Me.RtChartHeight.Location = New System.Drawing.Point(8, 28)
		Me.RtChartHeight.MaxLength = 5
		Me.RtChartHeight.Name = "RtChartHeight"
		Me.RtChartHeight.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.RtChartHeight.Size = New System.Drawing.Size(41, 20)
		Me.RtChartHeight.TabIndex = 17
		' 
		'Label22
		' 
		Me.Label22.AllowDrop = True
		Me.Label22.BackColor = System.Drawing.SystemColors.Window
		Me.Label22.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label22.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label22.Location = New System.Drawing.Point(8, 16)
		Me.Label22.Name = "Label22"
		Me.Label22.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label22.Size = New System.Drawing.Size(45, 13)
		Me.Label22.TabIndex = 63
		Me.Label22.Text = "Height"
		' 
		'Label21
		' 
		Me.Label21.AllowDrop = True
		Me.Label21.BackColor = System.Drawing.SystemColors.Window
		Me.Label21.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label21.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label21.Location = New System.Drawing.Point(64, 16)
		Me.Label21.Name = "Label21"
		Me.Label21.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label21.Size = New System.Drawing.Size(41, 13)
		Me.Label21.TabIndex = 62
		Me.Label21.Text = "Width"
		' 
		'Frame10
		' 
		Me.Frame10.AllowDrop = True
		Me.Frame10.BackColor = System.Drawing.SystemColors.Window
		Me.Frame10.Controls.Add(Me.MajorDivVertTextBox)
		Me.Frame10.Controls.Add(Me.MajorDivHorzTextBox)
		Me.Frame10.Controls.Add(Me.MinorDivTextBox)
		Me.Frame10.Controls.Add(Me.Label20)
		Me.Frame10.Controls.Add(Me.Label19)
		Me.Frame10.Controls.Add(Me.Label18)
		Me.Frame10.Enabled = True
		Me.Frame10.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Frame10.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Frame10.Location = New System.Drawing.Point(368, 244)
		Me.Frame10.Name = "Frame10"
		Me.Frame10.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame10.Size = New System.Drawing.Size(117, 85)
		Me.Frame10.TabIndex = 57
		Me.Frame10.Text = "Divisions"
		Me.Frame10.Visible = True
		' 
		'MajorDivVertTextBox
		' 
		Me.MajorDivVertTextBox.AcceptsReturn = True
		Me.MajorDivVertTextBox.AllowDrop = True
		Me.MajorDivVertTextBox.BackColor = System.Drawing.SystemColors.Window
		Me.MajorDivVertTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.MajorDivVertTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.MajorDivVertTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.MajorDivVertTextBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.MajorDivVertTextBox.Location = New System.Drawing.Point(68, 28)
		Me.MajorDivVertTextBox.MaxLength = 0
		Me.MajorDivVertTextBox.Name = "MajorDivVertTextBox"
		Me.MajorDivVertTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.MajorDivVertTextBox.Size = New System.Drawing.Size(37, 20)
		Me.MajorDivVertTextBox.TabIndex = 15
		' 
		'MajorDivHorzTextBox
		' 
		Me.MajorDivHorzTextBox.AcceptsReturn = True
		Me.MajorDivHorzTextBox.AllowDrop = True
		Me.MajorDivHorzTextBox.BackColor = System.Drawing.SystemColors.Window
		Me.MajorDivHorzTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.MajorDivHorzTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.MajorDivHorzTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.MajorDivHorzTextBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.MajorDivHorzTextBox.Location = New System.Drawing.Point(12, 28)
		Me.MajorDivHorzTextBox.MaxLength = 0
		Me.MajorDivHorzTextBox.Name = "MajorDivHorzTextBox"
		Me.MajorDivHorzTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.MajorDivHorzTextBox.Size = New System.Drawing.Size(37, 20)
		Me.MajorDivHorzTextBox.TabIndex = 14
		' 
		'MinorDivTextBox
		' 
		Me.MinorDivTextBox.AcceptsReturn = True
		Me.MinorDivTextBox.AllowDrop = True
		Me.MinorDivTextBox.BackColor = System.Drawing.SystemColors.Window
		Me.MinorDivTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.MinorDivTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.MinorDivTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.MinorDivTextBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.MinorDivTextBox.Location = New System.Drawing.Point(40, 60)
		Me.MinorDivTextBox.MaxLength = 0
		Me.MinorDivTextBox.Name = "MinorDivTextBox"
		Me.MinorDivTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.MinorDivTextBox.Size = New System.Drawing.Size(37, 20)
		Me.MinorDivTextBox.TabIndex = 16
		' 
		'Label20
		' 
		Me.Label20.AllowDrop = True
		Me.Label20.BackColor = System.Drawing.SystemColors.Window
		Me.Label20.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label20.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label20.Location = New System.Drawing.Point(68, 16)
		Me.Label20.Name = "Label20"
		Me.Label20.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label20.Size = New System.Drawing.Size(37, 13)
		Me.Label20.TabIndex = 60
		Me.Label20.Text = "Vert"
		' 
		'Label19
		' 
		Me.Label19.AllowDrop = True
		Me.Label19.BackColor = System.Drawing.SystemColors.Window
		Me.Label19.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label19.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label19.Location = New System.Drawing.Point(12, 16)
		Me.Label19.Name = "Label19"
		Me.Label19.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label19.Size = New System.Drawing.Size(37, 13)
		Me.Label19.TabIndex = 59
		Me.Label19.Text = "Horiz"
		' 
		'Label18
		' 
		Me.Label18.AllowDrop = True
		Me.Label18.BackColor = System.Drawing.SystemColors.Window
		Me.Label18.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label18.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label18.Location = New System.Drawing.Point(40, 48)
		Me.Label18.Name = "Label18"
		Me.Label18.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label18.Size = New System.Drawing.Size(41, 13)
		Me.Label18.TabIndex = 58
		Me.Label18.Text = "Minor"
		' 
		'Frame9
		' 
		Me.Frame9.AllowDrop = True
		Me.Frame9.BackColor = System.Drawing.SystemColors.Window
		Me.Frame9.Controls.Add(Me.MarginLeftTextBox)
		Me.Frame9.Controls.Add(Me.MarginTopTextBox)
		Me.Frame9.Controls.Add(Me.MarginBottomTextBox)
		Me.Frame9.Controls.Add(Me.MarginRightTextBox)
		Me.Frame9.Controls.Add(Me.Label17)
		Me.Frame9.Controls.Add(Me.Label16)
		Me.Frame9.Controls.Add(Me.Label15)
		Me.Frame9.Controls.Add(Me.Label14)
		Me.Frame9.Enabled = True
		Me.Frame9.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Frame9.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Frame9.Location = New System.Drawing.Point(316, 124)
		Me.Frame9.Name = "Frame9"
		Me.Frame9.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame9.Size = New System.Drawing.Size(81, 109)
		Me.Frame9.TabIndex = 37
		Me.Frame9.Text = "Margin"
		Me.Frame9.Visible = True
		' 
		'MarginLeftTextBox
		' 
		Me.MarginLeftTextBox.AcceptsReturn = True
		Me.MarginLeftTextBox.AllowDrop = True
		Me.MarginLeftTextBox.BackColor = System.Drawing.SystemColors.Window
		Me.MarginLeftTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.MarginLeftTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.MarginLeftTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.MarginLeftTextBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.MarginLeftTextBox.Location = New System.Drawing.Point(8, 80)
		Me.MarginLeftTextBox.MaxLength = 0
		Me.MarginLeftTextBox.Name = "MarginLeftTextBox"
		Me.MarginLeftTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.MarginLeftTextBox.Size = New System.Drawing.Size(29, 20)
		Me.MarginLeftTextBox.TabIndex = 25
		' 
		'MarginTopTextBox
		' 
		Me.MarginTopTextBox.AcceptsReturn = True
		Me.MarginTopTextBox.AllowDrop = True
		Me.MarginTopTextBox.BackColor = System.Drawing.SystemColors.Window
		Me.MarginTopTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.MarginTopTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.MarginTopTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.MarginTopTextBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.MarginTopTextBox.Location = New System.Drawing.Point(8, 36)
		Me.MarginTopTextBox.MaxLength = 0
		Me.MarginTopTextBox.Name = "MarginTopTextBox"
		Me.MarginTopTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.MarginTopTextBox.Size = New System.Drawing.Size(29, 20)
		Me.MarginTopTextBox.TabIndex = 23
		' 
		'MarginBottomTextBox
		' 
		Me.MarginBottomTextBox.AcceptsReturn = True
		Me.MarginBottomTextBox.AllowDrop = True
		Me.MarginBottomTextBox.BackColor = System.Drawing.SystemColors.Window
		Me.MarginBottomTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.MarginBottomTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.MarginBottomTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.MarginBottomTextBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.MarginBottomTextBox.Location = New System.Drawing.Point(44, 36)
		Me.MarginBottomTextBox.MaxLength = 0
		Me.MarginBottomTextBox.Name = "MarginBottomTextBox"
		Me.MarginBottomTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.MarginBottomTextBox.Size = New System.Drawing.Size(29, 20)
		Me.MarginBottomTextBox.TabIndex = 24
		' 
		'MarginRightTextBox
		' 
		Me.MarginRightTextBox.AcceptsReturn = True
		Me.MarginRightTextBox.AllowDrop = True
		Me.MarginRightTextBox.BackColor = System.Drawing.SystemColors.Window
		Me.MarginRightTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.MarginRightTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.MarginRightTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.MarginRightTextBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.MarginRightTextBox.Location = New System.Drawing.Point(44, 80)
		Me.MarginRightTextBox.MaxLength = 0
		Me.MarginRightTextBox.Name = "MarginRightTextBox"
		Me.MarginRightTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.MarginRightTextBox.Size = New System.Drawing.Size(29, 20)
		Me.MarginRightTextBox.TabIndex = 26
		' 
		'Label17
		' 
		Me.Label17.AllowDrop = True
		Me.Label17.BackColor = System.Drawing.SystemColors.Window
		Me.Label17.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label17.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label17.Location = New System.Drawing.Point(8, 64)
		Me.Label17.Name = "Label17"
		Me.Label17.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label17.Size = New System.Drawing.Size(33, 13)
		Me.Label17.TabIndex = 56
		Me.Label17.Text = "Left"
		' 
		'Label16
		' 
		Me.Label16.AllowDrop = True
		Me.Label16.BackColor = System.Drawing.SystemColors.Window
		Me.Label16.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label16.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label16.Location = New System.Drawing.Point(44, 64)
		Me.Label16.Name = "Label16"
		Me.Label16.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label16.Size = New System.Drawing.Size(33, 13)
		Me.Label16.TabIndex = 55
		Me.Label16.Text = "Right"
		' 
		'Label15
		' 
		Me.Label15.AllowDrop = True
		Me.Label15.BackColor = System.Drawing.SystemColors.Window
		Me.Label15.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label15.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label15.Location = New System.Drawing.Point(8, 20)
		Me.Label15.Name = "Label15"
		Me.Label15.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label15.Size = New System.Drawing.Size(37, 13)
		Me.Label15.TabIndex = 54
		Me.Label15.Text = "Top"
		' 
		'Label14
		' 
		Me.Label14.AllowDrop = True
		Me.Label14.BackColor = System.Drawing.SystemColors.Window
		Me.Label14.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label14.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label14.Location = New System.Drawing.Point(44, 20)
		Me.Label14.Name = "Label14"
		Me.Label14.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label14.Size = New System.Drawing.Size(33, 13)
		Me.Label14.TabIndex = 53
		Me.Label14.Text = "Botm"
		' 
		'Frame4
		' 
		Me.Frame4.AllowDrop = True
		Me.Frame4.BackColor = System.Drawing.SystemColors.Window
		Me.Frame4.Controls.Add(Me.AutoSizeCheck)
		Me.Frame4.Controls.Add(Me.RoundCornersCheck)
		Me.Frame4.Controls.Add(Me.OutlineCheck)
		Me.Frame4.Controls.Add(Me.BorderWidthTextBox)
		Me.Frame4.Controls.Add(Me.Label13)
		Me.Frame4.Enabled = True
		Me.Frame4.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Frame4.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Frame4.Location = New System.Drawing.Point(404, 124)
		Me.Frame4.Name = "Frame4"
		Me.Frame4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame4.Size = New System.Drawing.Size(81, 109)
		Me.Frame4.TabIndex = 38
		Me.Frame4.Text = "Border"
		Me.Frame4.Visible = True
		' 
		'AutoSizeCheck
		' 
		Me.AutoSizeCheck.AllowDrop = True
		Me.AutoSizeCheck.Appearance = System.Windows.Forms.Appearance.Normal
		Me.AutoSizeCheck.BackColor = System.Drawing.SystemColors.Window
		Me.AutoSizeCheck.CausesValidation = True
		Me.AutoSizeCheck.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.AutoSizeCheck.CheckState = System.Windows.Forms.CheckState.Checked
		Me.AutoSizeCheck.Enabled = True
		Me.AutoSizeCheck.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.AutoSizeCheck.ForeColor = System.Drawing.SystemColors.WindowText
		Me.AutoSizeCheck.Location = New System.Drawing.Point(8, 56)
		Me.AutoSizeCheck.Name = "AutoSizeCheck"
		Me.AutoSizeCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.AutoSizeCheck.Size = New System.Drawing.Size(69, 13)
		Me.AutoSizeCheck.TabIndex = 28
		Me.AutoSizeCheck.TabStop = True
		Me.AutoSizeCheck.Text = "AutoSize"
		Me.AutoSizeCheck.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.AutoSizeCheck.Visible = True
		' 
		'RoundCornersCheck
		' 
		Me.RoundCornersCheck.AllowDrop = True
		Me.RoundCornersCheck.Appearance = System.Windows.Forms.Appearance.Normal
		Me.RoundCornersCheck.BackColor = System.Drawing.SystemColors.Window
		Me.RoundCornersCheck.CausesValidation = True
		Me.RoundCornersCheck.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.RoundCornersCheck.CheckState = System.Windows.Forms.CheckState.Unchecked
		Me.RoundCornersCheck.Enabled = True
		Me.RoundCornersCheck.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.RoundCornersCheck.ForeColor = System.Drawing.SystemColors.WindowText
		Me.RoundCornersCheck.Location = New System.Drawing.Point(8, 88)
		Me.RoundCornersCheck.Name = "RoundCornersCheck"
		Me.RoundCornersCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.RoundCornersCheck.Size = New System.Drawing.Size(61, 13)
		Me.RoundCornersCheck.TabIndex = 30
		Me.RoundCornersCheck.TabStop = True
		Me.RoundCornersCheck.Text = "Round"
		Me.RoundCornersCheck.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.RoundCornersCheck.Visible = True
		' 
		'OutlineCheck
		' 
		Me.OutlineCheck.AllowDrop = True
		Me.OutlineCheck.Appearance = System.Windows.Forms.Appearance.Normal
		Me.OutlineCheck.BackColor = System.Drawing.SystemColors.Window
		Me.OutlineCheck.CausesValidation = True
		Me.OutlineCheck.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.OutlineCheck.CheckState = System.Windows.Forms.CheckState.Checked
		Me.OutlineCheck.Enabled = True
		Me.OutlineCheck.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.OutlineCheck.ForeColor = System.Drawing.SystemColors.WindowText
		Me.OutlineCheck.Location = New System.Drawing.Point(8, 72)
		Me.OutlineCheck.Name = "OutlineCheck"
		Me.OutlineCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.OutlineCheck.Size = New System.Drawing.Size(61, 13)
		Me.OutlineCheck.TabIndex = 29
		Me.OutlineCheck.TabStop = True
		Me.OutlineCheck.Text = "Outline"
		Me.OutlineCheck.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.OutlineCheck.Visible = True
		' 
		'BorderWidthTextBox
		' 
		Me.BorderWidthTextBox.AcceptsReturn = True
		Me.BorderWidthTextBox.AllowDrop = True
		Me.BorderWidthTextBox.BackColor = System.Drawing.SystemColors.Window
		Me.BorderWidthTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.BorderWidthTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.BorderWidthTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.BorderWidthTextBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.BorderWidthTextBox.Location = New System.Drawing.Point(8, 32)
		Me.BorderWidthTextBox.MaxLength = 5
		Me.BorderWidthTextBox.Name = "BorderWidthTextBox"
		Me.BorderWidthTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.BorderWidthTextBox.Size = New System.Drawing.Size(41, 20)
		Me.BorderWidthTextBox.TabIndex = 27
		' 
		'Label13
		' 
		Me.Label13.AllowDrop = True
		Me.Label13.BackColor = System.Drawing.SystemColors.Window
		Me.Label13.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label13.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label13.Location = New System.Drawing.Point(8, 20)
		Me.Label13.Name = "Label13"
		Me.Label13.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label13.Size = New System.Drawing.Size(45, 13)
		Me.Label13.TabIndex = 39
		Me.Label13.Text = "Width"
		' 
		'Frame7
		' 
		Me.Frame7.AllowDrop = True
		Me.Frame7.BackColor = System.Drawing.SystemColors.Window
		Me.Frame7.Controls.Add(Me.LightColorComboBox)
		Me.Frame7.Controls.Add(Me.OutlineColorComboBox)
		Me.Frame7.Controls.Add(Me.ShadowColorComboBox)
		Me.Frame7.Controls.Add(Me.BorderColorComboBox)
		Me.Frame7.Controls.Add(Me.Label4)
		Me.Frame7.Controls.Add(Me.Label5)
		Me.Frame7.Controls.Add(Me.Label6)
		Me.Frame7.Controls.Add(Me.Label7)
		Me.Frame7.Enabled = True
		Me.Frame7.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Frame7.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Frame7.Location = New System.Drawing.Point(4, 244)
		Me.Frame7.Name = "Frame7"
		Me.Frame7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame7.Size = New System.Drawing.Size(137, 169)
		Me.Frame7.TabIndex = 31
		Me.Frame7.Text = "Border Colors"
		Me.Frame7.Visible = True
		' 
		'LightColorComboBox
		' 
		Me.LightColorComboBox.AllowDrop = True
		Me.LightColorComboBox.BackColor = System.Drawing.SystemColors.Window
		Me.LightColorComboBox.CausesValidation = True
		Me.LightColorComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.LightColorComboBox.Enabled = True
		Me.LightColorComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.LightColorComboBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.LightColorComboBox.IntegralHeight = True
		Me.LightColorComboBox.Location = New System.Drawing.Point(8, 69)
		Me.LightColorComboBox.Name = "LightColorComboBox"
		Me.LightColorComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.LightColorComboBox.Size = New System.Drawing.Size(121, 20)
		Me.LightColorComboBox.Sorted = False
		Me.LightColorComboBox.TabIndex = 1
		Me.LightColorComboBox.TabStop = True
		Me.LightColorComboBox.Visible = True
		' 
		'OutlineColorComboBox
		' 
		Me.OutlineColorComboBox.AllowDrop = True
		Me.OutlineColorComboBox.BackColor = System.Drawing.SystemColors.Window
		Me.OutlineColorComboBox.CausesValidation = True
		Me.OutlineColorComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.OutlineColorComboBox.Enabled = True
		Me.OutlineColorComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.OutlineColorComboBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.OutlineColorComboBox.IntegralHeight = True
		Me.OutlineColorComboBox.Location = New System.Drawing.Point(8, 144)
		Me.OutlineColorComboBox.Name = "OutlineColorComboBox"
		Me.OutlineColorComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.OutlineColorComboBox.Size = New System.Drawing.Size(121, 20)
		Me.OutlineColorComboBox.Sorted = False
		Me.OutlineColorComboBox.TabIndex = 3
		Me.OutlineColorComboBox.TabStop = True
		Me.OutlineColorComboBox.Visible = True
		' 
		'ShadowColorComboBox
		' 
		Me.ShadowColorComboBox.AllowDrop = True
		Me.ShadowColorComboBox.BackColor = System.Drawing.SystemColors.Window
		Me.ShadowColorComboBox.CausesValidation = True
		Me.ShadowColorComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.ShadowColorComboBox.Enabled = True
		Me.ShadowColorComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.ShadowColorComboBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.ShadowColorComboBox.IntegralHeight = True
		Me.ShadowColorComboBox.Location = New System.Drawing.Point(8, 106)
		Me.ShadowColorComboBox.Name = "ShadowColorComboBox"
		Me.ShadowColorComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShadowColorComboBox.Size = New System.Drawing.Size(121, 20)
		Me.ShadowColorComboBox.Sorted = False
		Me.ShadowColorComboBox.TabIndex = 2
		Me.ShadowColorComboBox.TabStop = True
		Me.ShadowColorComboBox.Visible = True
		' 
		'BorderColorComboBox
		' 
		Me.BorderColorComboBox.AllowDrop = True
		Me.BorderColorComboBox.BackColor = System.Drawing.SystemColors.Window
		Me.BorderColorComboBox.CausesValidation = True
		Me.BorderColorComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.BorderColorComboBox.Enabled = True
		Me.BorderColorComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.BorderColorComboBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.BorderColorComboBox.IntegralHeight = True
		Me.BorderColorComboBox.Location = New System.Drawing.Point(8, 32)
		Me.BorderColorComboBox.Name = "BorderColorComboBox"
		Me.BorderColorComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.BorderColorComboBox.Size = New System.Drawing.Size(121, 20)
		Me.BorderColorComboBox.Sorted = False
		Me.BorderColorComboBox.TabIndex = 0
		Me.BorderColorComboBox.TabStop = True
		Me.BorderColorComboBox.Visible = True
		' 
		'Label4
		' 
		Me.Label4.AllowDrop = True
		Me.Label4.BackColor = System.Drawing.SystemColors.Window
		Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label4.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label4.Location = New System.Drawing.Point(8, 20)
		Me.Label4.Name = "Label4"
		Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label4.Size = New System.Drawing.Size(113, 13)
		Me.Label4.TabIndex = 52
		Me.Label4.Text = "Border"
		' 
		'Label5
		' 
		Me.Label5.AllowDrop = True
		Me.Label5.BackColor = System.Drawing.SystemColors.Window
		Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label5.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label5.Location = New System.Drawing.Point(8, 56)
		Me.Label5.Name = "Label5"
		Me.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label5.Size = New System.Drawing.Size(113, 13)
		Me.Label5.TabIndex = 51
		Me.Label5.Text = "Light"
		' 
		'Label6
		' 
		Me.Label6.AllowDrop = True
		Me.Label6.BackColor = System.Drawing.SystemColors.Window
		Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label6.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label6.Location = New System.Drawing.Point(8, 92)
		Me.Label6.Name = "Label6"
		Me.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label6.Size = New System.Drawing.Size(113, 13)
		Me.Label6.TabIndex = 50
		Me.Label6.Text = "Shadow"
		' 
		'Label7
		' 
		Me.Label7.AllowDrop = True
		Me.Label7.BackColor = System.Drawing.SystemColors.Window
		Me.Label7.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label7.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label7.Location = New System.Drawing.Point(8, 128)
		Me.Label7.Name = "Label7"
		Me.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label7.Size = New System.Drawing.Size(113, 13)
		Me.Label7.TabIndex = 49
		Me.Label7.Text = "Outline "
		' 
		'Frame8
		' 
		Me.Frame8.AllowDrop = True
		Me.Frame8.BackColor = System.Drawing.SystemColors.Window
		Me.Frame8.Controls.Add(Me.GridColorComboBox)
		Me.Frame8.Controls.Add(Me.AxesColorComboBox)
		Me.Frame8.Controls.Add(Me.FrameColorComboBox)
		Me.Frame8.Controls.Add(Me.BackGroundColorComboBox)
		Me.Frame8.Controls.Add(Me.Label8)
		Me.Frame8.Controls.Add(Me.Label9)
		Me.Frame8.Controls.Add(Me.Label10)
		Me.Frame8.Controls.Add(Me.Label11)
		Me.Frame8.Enabled = True
		Me.Frame8.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Frame8.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Frame8.Location = New System.Drawing.Point(148, 244)
		Me.Frame8.Name = "Frame8"
		Me.Frame8.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame8.Size = New System.Drawing.Size(137, 169)
		Me.Frame8.TabIndex = 44
		Me.Frame8.Text = "Graticule Colors"
		Me.Frame8.Visible = True
		' 
		'GridColorComboBox
		' 
		Me.GridColorComboBox.AllowDrop = True
		Me.GridColorComboBox.BackColor = System.Drawing.SystemColors.Window
		Me.GridColorComboBox.CausesValidation = True
		Me.GridColorComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.GridColorComboBox.Enabled = True
		Me.GridColorComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.GridColorComboBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.GridColorComboBox.IntegralHeight = True
		Me.GridColorComboBox.Location = New System.Drawing.Point(8, 143)
		Me.GridColorComboBox.Name = "GridColorComboBox"
		Me.GridColorComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.GridColorComboBox.Size = New System.Drawing.Size(121, 20)
		Me.GridColorComboBox.Sorted = False
		Me.GridColorComboBox.TabIndex = 7
		Me.GridColorComboBox.TabStop = True
		Me.GridColorComboBox.Visible = True
		' 
		'AxesColorComboBox
		' 
		Me.AxesColorComboBox.AllowDrop = True
		Me.AxesColorComboBox.BackColor = System.Drawing.SystemColors.Window
		Me.AxesColorComboBox.CausesValidation = True
		Me.AxesColorComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.AxesColorComboBox.Enabled = True
		Me.AxesColorComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.AxesColorComboBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.AxesColorComboBox.IntegralHeight = True
		Me.AxesColorComboBox.Location = New System.Drawing.Point(8, 106)
		Me.AxesColorComboBox.Name = "AxesColorComboBox"
		Me.AxesColorComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.AxesColorComboBox.Size = New System.Drawing.Size(121, 20)
		Me.AxesColorComboBox.Sorted = False
		Me.AxesColorComboBox.TabIndex = 6
		Me.AxesColorComboBox.TabStop = True
		Me.AxesColorComboBox.Visible = True
		' 
		'FrameColorComboBox
		' 
		Me.FrameColorComboBox.AllowDrop = True
		Me.FrameColorComboBox.BackColor = System.Drawing.SystemColors.Window
		Me.FrameColorComboBox.CausesValidation = True
		Me.FrameColorComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.FrameColorComboBox.Enabled = True
		Me.FrameColorComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.FrameColorComboBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.FrameColorComboBox.IntegralHeight = True
		Me.FrameColorComboBox.Location = New System.Drawing.Point(8, 69)
		Me.FrameColorComboBox.Name = "FrameColorComboBox"
		Me.FrameColorComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.FrameColorComboBox.Size = New System.Drawing.Size(121, 20)
		Me.FrameColorComboBox.Sorted = False
		Me.FrameColorComboBox.TabIndex = 5
		Me.FrameColorComboBox.TabStop = True
		Me.FrameColorComboBox.Visible = True
		' 
		'BackGroundColorComboBox
		' 
		Me.BackGroundColorComboBox.AllowDrop = True
		Me.BackGroundColorComboBox.BackColor = System.Drawing.SystemColors.Window
		Me.BackGroundColorComboBox.CausesValidation = True
		Me.BackGroundColorComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.BackGroundColorComboBox.Enabled = True
		Me.BackGroundColorComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.BackGroundColorComboBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.BackGroundColorComboBox.IntegralHeight = True
		Me.BackGroundColorComboBox.Location = New System.Drawing.Point(8, 32)
		Me.BackGroundColorComboBox.Name = "BackGroundColorComboBox"
		Me.BackGroundColorComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.BackGroundColorComboBox.Size = New System.Drawing.Size(121, 20)
		Me.BackGroundColorComboBox.Sorted = False
		Me.BackGroundColorComboBox.TabIndex = 4
		Me.BackGroundColorComboBox.TabStop = True
		Me.BackGroundColorComboBox.Visible = True
		' 
		'Label8
		' 
		Me.Label8.AllowDrop = True
		Me.Label8.BackColor = System.Drawing.SystemColors.Window
		Me.Label8.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label8.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label8.Location = New System.Drawing.Point(8, 20)
		Me.Label8.Name = "Label8"
		Me.Label8.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label8.Size = New System.Drawing.Size(113, 13)
		Me.Label8.TabIndex = 48
		Me.Label8.Text = "Back Ground"
		' 
		'Label9
		' 
		Me.Label9.AllowDrop = True
		Me.Label9.BackColor = System.Drawing.SystemColors.Window
		Me.Label9.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label9.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label9.Location = New System.Drawing.Point(8, 56)
		Me.Label9.Name = "Label9"
		Me.Label9.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label9.Size = New System.Drawing.Size(113, 13)
		Me.Label9.TabIndex = 47
		Me.Label9.Text = "Frame"
		' 
		'Label10
		' 
		Me.Label10.AllowDrop = True
		Me.Label10.BackColor = System.Drawing.SystemColors.Window
		Me.Label10.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label10.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label10.Location = New System.Drawing.Point(8, 92)
		Me.Label10.Name = "Label10"
		Me.Label10.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label10.Size = New System.Drawing.Size(113, 13)
		Me.Label10.TabIndex = 46
		Me.Label10.Text = "Axes"
		' 
		'Label11
		' 
		Me.Label11.AllowDrop = True
		Me.Label11.BackColor = System.Drawing.SystemColors.Window
		Me.Label11.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label11.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label11.Location = New System.Drawing.Point(8, 128)
		Me.Label11.Name = "Label11"
		Me.Label11.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label11.Size = New System.Drawing.Size(113, 13)
		Me.Label11.TabIndex = 45
		Me.Label11.Text = "Grid"
		' 
		'Frame3
		' 
		Me.Frame3.AllowDrop = True
		Me.Frame3.BackColor = System.Drawing.SystemColors.Window
		Me.Frame3.Controls.Add(Me.Frame6)
		Me.Frame3.Controls.Add(Me.Frame5)
		Me.Frame3.Enabled = True
		Me.Frame3.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Frame3.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Frame3.Location = New System.Drawing.Point(316, 4)
		Me.Frame3.Name = "Frame3"
		Me.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame3.Size = New System.Drawing.Size(169, 117)
		Me.Frame3.TabIndex = 34
		Me.Frame3.Text = "Bevel"
		Me.Frame3.Visible = True
		' 
		'Frame6
		' 
		Me.Frame6.AllowDrop = True
		Me.Frame6.BackColor = System.Drawing.SystemColors.Window
		Me.Frame6.Controls.Add(Me.BevelStyleOuterComboBox)
		Me.Frame6.Controls.Add(Me.BevelStyleInnerComboBox)
		Me.Frame6.Controls.Add(Me.Label1)
		Me.Frame6.Controls.Add(Me.Label2)
		Me.Frame6.Enabled = True
		Me.Frame6.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Frame6.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Frame6.Location = New System.Drawing.Point(8, 16)
		Me.Frame6.Name = "Frame6"
		Me.Frame6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame6.Size = New System.Drawing.Size(89, 93)
		Me.Frame6.TabIndex = 35
		Me.Frame6.Text = "&Style"
		Me.Frame6.Visible = True
		' 
		'BevelStyleOuterComboBox
		' 
		Me.BevelStyleOuterComboBox.AllowDrop = True
		Me.BevelStyleOuterComboBox.BackColor = System.Drawing.SystemColors.Window
		Me.BevelStyleOuterComboBox.CausesValidation = True
		Me.BevelStyleOuterComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
		Me.BevelStyleOuterComboBox.Enabled = True
		Me.BevelStyleOuterComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.BevelStyleOuterComboBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.BevelStyleOuterComboBox.IntegralHeight = True
		Me.BevelStyleOuterComboBox.Location = New System.Drawing.Point(8, 68)
		Me.BevelStyleOuterComboBox.Name = "BevelStyleOuterComboBox"
		Me.BevelStyleOuterComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.BevelStyleOuterComboBox.Size = New System.Drawing.Size(73, 20)
		Me.BevelStyleOuterComboBox.Sorted = False
		Me.BevelStyleOuterComboBox.TabIndex = 20
		Me.BevelStyleOuterComboBox.TabStop = True
		Me.BevelStyleOuterComboBox.Visible = True
		' 
		'BevelStyleInnerComboBox
		' 
		Me.BevelStyleInnerComboBox.AllowDrop = True
		Me.BevelStyleInnerComboBox.BackColor = System.Drawing.SystemColors.Window
		Me.BevelStyleInnerComboBox.CausesValidation = True
		Me.BevelStyleInnerComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
		Me.BevelStyleInnerComboBox.Enabled = True
		Me.BevelStyleInnerComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.BevelStyleInnerComboBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.BevelStyleInnerComboBox.IntegralHeight = True
		Me.BevelStyleInnerComboBox.Location = New System.Drawing.Point(8, 32)
		Me.BevelStyleInnerComboBox.Name = "BevelStyleInnerComboBox"
		Me.BevelStyleInnerComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.BevelStyleInnerComboBox.Size = New System.Drawing.Size(73, 20)
		Me.BevelStyleInnerComboBox.Sorted = False
		Me.BevelStyleInnerComboBox.TabIndex = 19
		Me.BevelStyleInnerComboBox.TabStop = True
		Me.BevelStyleInnerComboBox.Visible = True
		' 
		'Label1
		' 
		Me.Label1.AllowDrop = True
		Me.Label1.BackColor = System.Drawing.SystemColors.Window
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label1.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label1.Location = New System.Drawing.Point(8, 20)
		Me.Label1.Name = "Label1"
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.Size = New System.Drawing.Size(45, 13)
		Me.Label1.TabIndex = 43
		Me.Label1.Text = "Inner"
		' 
		'Label2
		' 
		Me.Label2.AllowDrop = True
		Me.Label2.BackColor = System.Drawing.SystemColors.Window
		Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label2.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label2.Location = New System.Drawing.Point(8, 56)
		Me.Label2.Name = "Label2"
		Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label2.Size = New System.Drawing.Size(45, 13)
		Me.Label2.TabIndex = 42
		Me.Label2.Text = "Outer"
		' 
		'Frame5
		' 
		Me.Frame5.AllowDrop = True
		Me.Frame5.BackColor = System.Drawing.SystemColors.Window
		Me.Frame5.Controls.Add(Me.BevelWidthInnerTextBox)
		Me.Frame5.Controls.Add(Me.BevelWidthOuterTextBox)
		Me.Frame5.Controls.Add(Me.Label12)
		Me.Frame5.Controls.Add(Me.Label3)
		Me.Frame5.Enabled = True
		Me.Frame5.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Frame5.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Frame5.Location = New System.Drawing.Point(104, 16)
		Me.Frame5.Name = "Frame5"
		Me.Frame5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame5.Size = New System.Drawing.Size(57, 93)
		Me.Frame5.TabIndex = 36
		Me.Frame5.Text = "&Width"
		Me.Frame5.Visible = True
		' 
		'BevelWidthInnerTextBox
		' 
		Me.BevelWidthInnerTextBox.AcceptsReturn = True
		Me.BevelWidthInnerTextBox.AllowDrop = True
		Me.BevelWidthInnerTextBox.BackColor = System.Drawing.SystemColors.Window
		Me.BevelWidthInnerTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.BevelWidthInnerTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.BevelWidthInnerTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.BevelWidthInnerTextBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.BevelWidthInnerTextBox.Location = New System.Drawing.Point(8, 32)
		Me.BevelWidthInnerTextBox.MaxLength = 5
		Me.BevelWidthInnerTextBox.Name = "BevelWidthInnerTextBox"
		Me.BevelWidthInnerTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.BevelWidthInnerTextBox.Size = New System.Drawing.Size(41, 20)
		Me.BevelWidthInnerTextBox.TabIndex = 21
		' 
		'BevelWidthOuterTextBox
		' 
		Me.BevelWidthOuterTextBox.AcceptsReturn = True
		Me.BevelWidthOuterTextBox.AllowDrop = True
		Me.BevelWidthOuterTextBox.BackColor = System.Drawing.SystemColors.Window
		Me.BevelWidthOuterTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.BevelWidthOuterTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.BevelWidthOuterTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.BevelWidthOuterTextBox.ForeColor = System.Drawing.SystemColors.WindowText
		Me.BevelWidthOuterTextBox.Location = New System.Drawing.Point(8, 68)
		Me.BevelWidthOuterTextBox.MaxLength = 5
		Me.BevelWidthOuterTextBox.Name = "BevelWidthOuterTextBox"
		Me.BevelWidthOuterTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.BevelWidthOuterTextBox.Size = New System.Drawing.Size(41, 20)
		Me.BevelWidthOuterTextBox.TabIndex = 22
		' 
		'Label12
		' 
		Me.Label12.AllowDrop = True
		Me.Label12.BackColor = System.Drawing.SystemColors.Window
		Me.Label12.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label12.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label12.Location = New System.Drawing.Point(8, 56)
		Me.Label12.Name = "Label12"
		Me.Label12.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label12.Size = New System.Drawing.Size(37, 13)
		Me.Label12.TabIndex = 41
		Me.Label12.Text = "Outer"
		' 
		'Label3
		' 
		Me.Label3.AllowDrop = True
		Me.Label3.BackColor = System.Drawing.SystemColors.Window
		Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Label3.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label3.Location = New System.Drawing.Point(8, 20)
		Me.Label3.Name = "Label3"
		Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label3.Size = New System.Drawing.Size(33, 13)
		Me.Label3.TabIndex = 40
		Me.Label3.Text = "Inner"
		' 
		'Frame1
		' 
		Me.Frame1.AllowDrop = True
		Me.Frame1.BackColor = System.Drawing.SystemColors.Window
		Me.Frame1.Controls.Add(Me.Viewports4)
		Me.Frame1.Controls.Add(Me.Viewports2)
		Me.Frame1.Controls.Add(Me.Viewports1)
		Me.Frame1.Enabled = True
		Me.Frame1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Frame1.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Frame1.Location = New System.Drawing.Point(292, 332)
		Me.Frame1.Name = "Frame1"
		Me.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame1.Size = New System.Drawing.Size(69, 81)
		Me.Frame1.TabIndex = 33
		Me.Frame1.Text = "Viewports"
		Me.Frame1.Visible = True
		' 
		'Viewports4
		' 
		Me.Viewports4.AllowDrop = True
		Me.Viewports4.Appearance = System.Windows.Forms.Appearance.Normal
		Me.Viewports4.BackColor = System.Drawing.SystemColors.Window
		Me.Viewports4.CausesValidation = True
		Me.Viewports4.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.Viewports4.Checked = False
		Me.Viewports4.Enabled = True
		Me.Viewports4.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Viewports4.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Viewports4.Location = New System.Drawing.Point(8, 60)
		Me.Viewports4.Name = "Viewports4"
		Me.Viewports4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Viewports4.Size = New System.Drawing.Size(49, 13)
		Me.Viewports4.TabIndex = 13
		Me.Viewports4.TabStop = True
		Me.Viewports4.Text = " 4"
		Me.Viewports4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.Viewports4.Visible = True
		' 
		'Viewports2
		' 
		Me.Viewports2.AllowDrop = True
		Me.Viewports2.Appearance = System.Windows.Forms.Appearance.Normal
		Me.Viewports2.BackColor = System.Drawing.SystemColors.Window
		Me.Viewports2.CausesValidation = True
		Me.Viewports2.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.Viewports2.Checked = False
		Me.Viewports2.Enabled = True
		Me.Viewports2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Viewports2.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Viewports2.Location = New System.Drawing.Point(8, 40)
		Me.Viewports2.Name = "Viewports2"
		Me.Viewports2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Viewports2.Size = New System.Drawing.Size(49, 13)
		Me.Viewports2.TabIndex = 12
		Me.Viewports2.TabStop = True
		Me.Viewports2.Text = " 2"
		Me.Viewports2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.Viewports2.Visible = True
		' 
		'Viewports1
		' 
		Me.Viewports1.AllowDrop = True
		Me.Viewports1.Appearance = System.Windows.Forms.Appearance.Normal
		Me.Viewports1.BackColor = System.Drawing.SystemColors.Window
		Me.Viewports1.CausesValidation = True
		Me.Viewports1.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.Viewports1.Checked = True
		Me.Viewports1.Enabled = True
		Me.Viewports1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Viewports1.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Viewports1.Location = New System.Drawing.Point(8, 20)
		Me.Viewports1.Name = "Viewports1"
		Me.Viewports1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Viewports1.Size = New System.Drawing.Size(49, 13)
		Me.Viewports1.TabIndex = 11
		Me.Viewports1.TabStop = True
		Me.Viewports1.Text = " 1"
		Me.Viewports1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.Viewports1.Visible = True
		' 
		'Frame2
		' 
		Me.Frame2.AllowDrop = True
		Me.Frame2.BackColor = System.Drawing.SystemColors.Window
		Me.Frame2.Controls.Add(Me.FrameCheck)
		Me.Frame2.Controls.Add(Me.GridCheck)
		Me.Frame2.Controls.Add(Me.AxesCheck)
		Me.Frame2.Enabled = True
		Me.Frame2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.Frame2.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Frame2.Location = New System.Drawing.Point(292, 244)
		Me.Frame2.Name = "Frame2"
		Me.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame2.Size = New System.Drawing.Size(69, 85)
		Me.Frame2.TabIndex = 32
		Me.Frame2.Text = "Graticule"
		Me.Frame2.Visible = True
		' 
		'FrameCheck
		' 
		Me.FrameCheck.AllowDrop = True
		Me.FrameCheck.Appearance = System.Windows.Forms.Appearance.Normal
		Me.FrameCheck.BackColor = System.Drawing.SystemColors.Window
		Me.FrameCheck.CausesValidation = True
		Me.FrameCheck.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.FrameCheck.CheckState = System.Windows.Forms.CheckState.Checked
		Me.FrameCheck.Enabled = True
		Me.FrameCheck.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.FrameCheck.ForeColor = System.Drawing.SystemColors.WindowText
		Me.FrameCheck.Location = New System.Drawing.Point(8, 20)
		Me.FrameCheck.Name = "FrameCheck"
		Me.FrameCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.FrameCheck.Size = New System.Drawing.Size(53, 13)
		Me.FrameCheck.TabIndex = 8
		Me.FrameCheck.TabStop = True
		Me.FrameCheck.Text = "Frame"
		Me.FrameCheck.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.FrameCheck.Visible = True
		' 
		'GridCheck
		' 
		Me.GridCheck.AllowDrop = True
		Me.GridCheck.Appearance = System.Windows.Forms.Appearance.Normal
		Me.GridCheck.BackColor = System.Drawing.SystemColors.Window
		Me.GridCheck.CausesValidation = True
		Me.GridCheck.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.GridCheck.CheckState = System.Windows.Forms.CheckState.Checked
		Me.GridCheck.Enabled = True
		Me.GridCheck.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.GridCheck.ForeColor = System.Drawing.SystemColors.WindowText
		Me.GridCheck.Location = New System.Drawing.Point(8, 64)
		Me.GridCheck.Name = "GridCheck"
		Me.GridCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.GridCheck.Size = New System.Drawing.Size(45, 13)
		Me.GridCheck.TabIndex = 10
		Me.GridCheck.TabStop = True
		Me.GridCheck.Text = "Grid"
		Me.GridCheck.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.GridCheck.Visible = True
		' 
		'AxesCheck
		' 
		Me.AxesCheck.AllowDrop = True
		Me.AxesCheck.Appearance = System.Windows.Forms.Appearance.Normal
		Me.AxesCheck.BackColor = System.Drawing.SystemColors.Window
		Me.AxesCheck.CausesValidation = True
		Me.AxesCheck.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.AxesCheck.CheckState = System.Windows.Forms.CheckState.Checked
		Me.AxesCheck.Enabled = True
		Me.AxesCheck.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.AxesCheck.ForeColor = System.Drawing.SystemColors.WindowText
		Me.AxesCheck.Location = New System.Drawing.Point(8, 41)
		Me.AxesCheck.Name = "AxesCheck"
		Me.AxesCheck.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.AxesCheck.Size = New System.Drawing.Size(49, 13)
		Me.AxesCheck.TabIndex = 9
		Me.AxesCheck.TabStop = True
		Me.AxesCheck.Text = "Axes"
		Me.AxesCheck.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.AxesCheck.Visible = True
		' 
		'RtChart1
		' 
		Me.RtChart1.AutoSize = 0
		Me.RtChart1.AxesColor = 16776960
		Me.RtChart1.AxesTics = 0
		Me.RtChart1.AxesType = 0
		Me.RtChart1.BackColor = 8421376
		Me.RtChart1.BevelInner = 0
		Me.RtChart1.BevelOuter = 2
		Me.RtChart1.BevelWidth_Inner = 1
		Me.RtChart1.BevelWidth_Outer = 1
		Me.RtChart1.BorderColor = (-2147483633)
		Me.RtChart1.BorderWidth = 3
		Me.RtChart1.Chn_Select = 0
		Me.RtChart1.ChnData_Dimension = 0
		Me.RtChart1.ChnDspUserData = 0
		Me.RtChart1.ChnXform_Select = 0
		Me.RtChart1.ColorDepth = 0
		Me.RtChart1.ErrBase = 30200
		Me.RtChart1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.RtChart1.ForeColor = (-842150451)
		Me.RtChart1.FrameColor = 16776960
		Me.RtChart1.FrameOn = -1
		Me.RtChart1.FramesPerSec = 0
		Me.RtChart1.FrameTics = 0
		Me.RtChart1.GridColor = 8421504
		Me.RtChart1.GridOn = -1
		Me.RtChart1.GridSymmetry = 0
		Me.RtChart1.GridType = 0
		Me.RtChart1.HitTest = 0
		Me.RtChart1.ImageFile = String.Empty
		Me.RtChart1.LightColor = (-2147483634)
		Me.RtChart1.Location = New System.Drawing.Point(0, 0)
		Me.RtChart1.MajorDivHorz = 10
		Me.RtChart1.MajorDivVert = 8
		Me.RtChart1.MarginBottom = 1
		Me.RtChart1.MarginLeft = 1
		Me.RtChart1.MarginRight = 1
		Me.RtChart1.MarginTop = 1
		Me.RtChart1.MinorDiv = 5
		Me.RtChart1.MouseIcon = 0
		Me.RtChart1.Name = "RtChart1"
		Me.RtChart1.Outline = -1
		Me.RtChart1.OutlineColor = (-2147483642)
		Me.RtChart1.Pen_Select = 0
		Me.RtChart1.RoundedCorners = 0
		Me.RtChart1.Scale_Select = 0
		Me.RtChart1.ShadowColor = (-2147483632)
		Me.RtChart1.Size = New System.Drawing.Size(305, 233)
		Me.RtChart1.TabIndex = 65
		Me.RtChart1.Viewport_Select = 0
		Me.RtChart1.Viewports = 1
		Me.RtChart1.ViewportStorageColor = 0
		Me.RtChart1.ViewportStorageOn = -1
		Me.RtChart1.Wnd_Select = 0
		' 
		'Form1
		' 
		Me.AllowDrop = True
		Me.AutoScaleDimensions = New System.Drawing.SizeF(15, 29)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Window
		Me.ClientSize = New System.Drawing.Size(489, 417)
		Me.Controls.Add(Me.Frame11)
		Me.Controls.Add(Me.Frame10)
		Me.Controls.Add(Me.Frame9)
		Me.Controls.Add(Me.Frame4)
		Me.Controls.Add(Me.Frame7)
		Me.Controls.Add(Me.Frame8)
		Me.Controls.Add(Me.Frame3)
		Me.Controls.Add(Me.Frame1)
		Me.Controls.Add(Me.Frame2)
		Me.Controls.Add(Me.RtChart1)
		Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.ForeColor = System.Drawing.SystemColors.WindowText
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Location = New System.Drawing.Point(88, 117)
		Me.Location = New System.Drawing.Point(85, 99)
		Me.MaximizeBox = False
		Me.MinimizeBox = True
		Me.Name = "Form1"
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Size = New System.Drawing.Size(496, 439)
		Me.Text = "LabOBJX Real Time Chart - Display Example"
		Me.Frame11.ResumeLayout(False)
		Me.Frame10.ResumeLayout(False)
		Me.Frame9.ResumeLayout(False)
		Me.Frame4.ResumeLayout(False)
		Me.Frame7.ResumeLayout(False)
		Me.Frame8.ResumeLayout(False)
		Me.Frame3.ResumeLayout(False)
		Me.Frame6.ResumeLayout(False)
		Me.Frame5.ResumeLayout(False)
		Me.Frame1.ResumeLayout(False)
		Me.Frame2.ResumeLayout(False)
		Me.ResumeLayout(False)
	End Sub
	Sub ReLoadForm(ByVal addEvents As Boolean)
		Form_Load()
		If addEvents Then
			AddHandler MyBase.Closed, AddressOf Me.Form_Closed
		End If
	End Sub
#End Region
End Class