Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic
Imports System
Imports System.Windows.Forms

Imports isr.Visuals.RealTimeChart
Partial Friend Class ScreenForm
    Inherits System.Windows.Forms.Form

    ' LabOBJX Real-Time Chart - Example Code
    ' (C) Copyright 1995, Scientific Software Tools, Inc.
    ' All Rights Reserved.
    '

    Dim i As Integer

    Dim PenColorArray() As Module2.ItemNameValue = Nothing
    Dim BevelStyleArray() As String

    Dim TempVal As Integer

    ' size value after border, margin, and outlines are added
    Const DefaultRtChartHeight As Integer = 2784
    Const DefaultRtChartWidth As Integer = 3384

    Private isInitializingComponent As Boolean
    Public Sub New()
        MyBase.New()
        If _Instance Is Nothing Then
            If _InitializingInstance Then
                _Instance = Me
            Else
                Try
                    'For the start-up form, the first instance created is the default instance.
                    If Not (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint Is Nothing) AndAlso System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType Is Me.GetType() Then
                        _Instance = Me
                    End If

                Catch
                End Try
            End If
        End If
        'This call is required by the Windows Form Designer.
        isInitializingComponent = True
        InitializeComponent()
        isInitializingComponent = False
        If Not Me.DesignMode Then ReLoadForm(False)
    End Sub


    Private Sub AutoSizeCheck_CheckStateChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles AutoSizeCheck.CheckStateChanged
        If isInitializingComponent Then Return

        ' true forces square border, frame is always square
        ReflectionHelper.LetMember(RtChart1, "AutoSize", AutoSizeCheck.CheckState)

        ' update RtChart height and width
        RtChartHeight.Text = ReflectionHelper.GetMember(RtChart1, "Height").ToString()
        RtChartWidth.Text = ReflectionHelper.GetMember(RtChart1, "Width").ToString()

    End Sub

    Private Sub AxesCheck_CheckStateChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles AxesCheck.CheckStateChanged
        If isInitializingComponent Then Return

        If AxesCheck.CheckState = CheckState.Checked Then
                RtChart1.AxesType = AxesType.HorizontalVertical
        Else : AxesCheck.CheckState = CheckState.Unchecked
                RtChart1.AxesType = AxesType.NoAxes
        End If

    End Sub

    Private Sub AxesColorComboBox_SelectedIndexChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles AxesColorComboBox.SelectedIndexChanged

        ' the ItemData property stores the RGB color value which is indexed by the seleted list item
        ReflectionHelper.LetMember(RtChart1, "AxesColor", AxesColorComboBox.GetItemData(AxesColorComboBox.SelectedIndex))

    End Sub

    Private Sub BackGroundColorComboBox_SelectedIndexChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles BackGroundColorComboBox.SelectedIndexChanged

        ' the ItemData property stores the RGB color value which is indexed by the seleted list item
        ReflectionHelper.LetMember(RtChart1, "BackColor", BackGroundColorComboBox.GetItemData(BackGroundColorComboBox.SelectedIndex))

    End Sub

    Private Sub BevelStyleInnerComboBox_SelectionChangeCommitted(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles BevelStyleInnerComboBox.SelectionChangeCommitted

        ReflectionHelper.LetMember(RtChart1, "BevelInner", BevelStyleInnerComboBox.SelectedIndex)

        ' update RtChart height and width
        RtChartHeight.Text = ReflectionHelper.GetMember(RtChart1, "Height").ToString()
        RtChartWidth.Text = ReflectionHelper.GetMember(RtChart1, "Width").ToString()

    End Sub

    Private Sub BevelStyleOuterComboBox_SelectionChangeCommitted(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles BevelStyleOuterComboBox.SelectionChangeCommitted

        ReflectionHelper.LetMember(RtChart1, "BevelOuter", BevelStyleOuterComboBox.SelectedIndex)

        ' update RtChart height and width
        RtChartHeight.Text = ReflectionHelper.GetMember(RtChart1, "Height").ToString()
        RtChartWidth.Text = ReflectionHelper.GetMember(RtChart1, "Width").ToString()

    End Sub

    Private Sub BevelWidthInnerTextBox_Enter(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles BevelWidthInnerTextBox.Enter

        BevelWidthInnerTextBox.SelectionStart = 0
        BevelWidthInnerTextBox.SelectionLength = Strings.Len(BevelWidthInnerTextBox.Text)

    End Sub

    Private Sub BevelWidthInnerTextBox_KeyDown(ByVal eventSender As Object, ByVal eventArgs As KeyEventArgs) Handles BevelWidthInnerTextBox.KeyDown
        Dim KeyCode As Keys = eventArgs.KeyCode
        Dim Shift As Integer = eventArgs.KeyData / 65536
        Try

            If KeyCode = Keys.Return Then
                TempVal = CInt(Conversion.Val(BevelWidthInnerTextBox.Text))
                If TempVal < 0 Or TempVal > 32767 Then Exit Sub

                ReflectionHelper.LetMember(RtChart1, "BevelWidth_Inner", TempVal)

                ' update RtChart height and width
                RtChartHeight.Text = ReflectionHelper.GetMember(RtChart1, "Height").ToString()
                RtChartWidth.Text = ReflectionHelper.GetMember(RtChart1, "Width").ToString()
            End If
        Finally
            eventArgs.Handled = KeyCode = 0
        End Try

    End Sub

    Private Sub BevelWidthInnerTextBox_Leave(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles BevelWidthInnerTextBox.Leave

        BevelWidthInnerTextBox_KeyDown(BevelWidthInnerTextBox, New KeyEventArgs(Keys.Return))

    End Sub

    Private Sub BevelWidthOuterTextBox_Enter(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles BevelWidthOuterTextBox.Enter

        BevelWidthOuterTextBox.SelectionStart = 0
        BevelWidthOuterTextBox.SelectionLength = Strings.Len(BevelWidthOuterTextBox.Text)

    End Sub

    Private Sub BevelWidthOuterTextBox_KeyDown(ByVal eventSender As Object, ByVal eventArgs As KeyEventArgs) Handles BevelWidthOuterTextBox.KeyDown
        Dim KeyCode As Keys = eventArgs.KeyCode
        Dim Shift As Integer = eventArgs.KeyData / 65536
        Try

            If KeyCode = Keys.Return Then
                TempVal = CInt(Conversion.Val(BevelWidthOuterTextBox.Text))
                If TempVal < 0 Or TempVal > 32767 Then Exit Sub

                ReflectionHelper.LetMember(RtChart1, "BevelWidth_Outer", TempVal)

                ' update RtChart height and width
                RtChartHeight.Text = ReflectionHelper.GetMember(RtChart1, "Height").ToString()
                RtChartWidth.Text = ReflectionHelper.GetMember(RtChart1, "Width").ToString()
            End If
        Finally
            eventArgs.Handled = KeyCode = 0
        End Try

    End Sub

    Private Sub BevelWidthOuterTextBox_Leave(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles BevelWidthOuterTextBox.Leave

        BevelWidthOuterTextBox_KeyDown(BevelWidthOuterTextBox, New KeyEventArgs(Keys.Return))

    End Sub

    Private Sub BorderColorComboBox_SelectedIndexChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles BorderColorComboBox.SelectedIndexChanged

        ' the ItemData property stores the RGB color value which is indexed by the seleted list item
        ReflectionHelper.LetMember(RtChart1, "BorderColor", BorderColorComboBox.GetItemData(BorderColorComboBox.SelectedIndex))

    End Sub

    Private Sub BorderWidthTextBox_Enter(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles BorderWidthTextBox.Enter

        BorderWidthTextBox.SelectionStart = 0
        BorderWidthTextBox.SelectionLength = Strings.Len(BorderWidthTextBox.Text)

    End Sub

    Private Sub BorderWidthTextBox_KeyDown(ByVal eventSender As Object, ByVal eventArgs As KeyEventArgs) Handles BorderWidthTextBox.KeyDown
        Dim KeyCode As Keys = eventArgs.KeyCode
        Dim Shift As Integer = eventArgs.KeyData / 65536
        Try

            If KeyCode = Keys.Return Then
                TempVal = CInt(Conversion.Val(BorderWidthTextBox.Text))
                If TempVal < 0 Or TempVal > 32767 Then Exit Sub

                ReflectionHelper.LetMember(RtChart1, "BorderWidth", TempVal)

                ' update RtChart height and width
                RtChartHeight.Text = ReflectionHelper.GetMember(RtChart1, "Height").ToString()
                RtChartWidth.Text = ReflectionHelper.GetMember(RtChart1, "Width").ToString()
            End If
        Finally
            eventArgs.Handled = KeyCode = 0
        End Try

    End Sub

    Private Sub BorderWidthTextBox_Leave(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles BorderWidthTextBox.Leave

        BorderWidthTextBox_KeyDown(BorderWidthTextBox, New KeyEventArgs(Keys.Return))

    End Sub

    Private Sub DefaultButton_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles DefaultButton.Click

        InitializeRtChart(RtChart1)

        InitFrontPanel(RtChart1)

    End Sub

    Private Sub Form_Load()

        InitColorArray()
        InitComboBoxes()

        InitBevelStyleArray()

        InitializeRtChart(RtChart1)

        InitFrontPanel(RtChart1)

    End Sub

    Private Sub FrameCheck_CheckStateChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles FrameCheck.CheckStateChanged
        If isInitializingComponent Then Return

        ' set RtChart property
        RtChart1.FrameOn = (FrameCheck.CheckState)

    End Sub

    Private Sub FrameColorComboBox_SelectedIndexChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles FrameColorComboBox.SelectedIndexChanged

        ' the ItemData property stores the RGB color value which is indexed by the seleted list item
        ReflectionHelper.LetMember(RtChart1, "FrameColor", FrameColorComboBox.GetItemData(FrameColorComboBox.SelectedIndex))

    End Sub

    Private Sub GridCheck_CheckStateChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles GridCheck.CheckStateChanged
        If isInitializingComponent Then Return

        ' set RtChart property
        RtChart1.GridOn = (GridCheck.CheckState)

    End Sub

    Private Sub GridColorComboBox_SelectedIndexChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles GridColorComboBox.SelectedIndexChanged

        ' the ItemData property stores the RGB color value which is indexed by the seleted list item
        ReflectionHelper.LetMember(RtChart1, "GridColor", GridColorComboBox.GetItemData(GridColorComboBox.SelectedIndex))

    End Sub

    Private Sub InitBevelStyleArray()

        ReDim BevelStyleArray(3)

        BevelStyleArray(0) = "None"
        BevelStyleArray(1) = "Inset"
        BevelStyleArray(2) = "Raised"

        For i = 0 To 2
            BevelStyleInnerComboBox.AddItem(BevelStyleArray(i), i)
            BevelStyleOuterComboBox.AddItem(BevelStyleArray(i), i)
        Next i

        ' select current style from list (default)
        BevelStyleInnerComboBox.Text = "Inset"
        BevelStyleOuterComboBox.Text = "Raised"

    End Sub

    Private Sub InitColorArray()

        ReDim PenColorArray(19)

        ' this array provides the means to relate the text pen name with the RGB value
        ' for each pen, it contains:
        '       ItemName - the text name of the pen to be listed in the combo box
        '       RGBValue - the RGB value used to specify the color of the pen
        '       ListIndex - controls the list ording in the combo box list

        ' use 20 pure Windows colors
        PenColorArray(BLACK_PEN).RGBValue = BLACK
    PenColorArray(BLACK_PEN).ItemName = "Black" ' item name in ComboBox
        PenColorArray(BLACK_PEN).ListIndex = 0 ' sets list ordering in ComboBox

        PenColorArray(WHITE_PEN).RGBValue = WHITE
        PenColorArray(WHITE_PEN).ItemName = "White"
        PenColorArray(WHITE_PEN).ListIndex = 1

        PenColorArray(DARKGRAY_PEN).RGBValue = DARKGRAY
        PenColorArray(DARKGRAY_PEN).ItemName = "DarkGray"
        PenColorArray(DARKGRAY_PEN).ListIndex = 2

        PenColorArray(LIGHTGRAY_PEN).RGBValue = LIGHTGRAY
        PenColorArray(LIGHTGRAY_PEN).ItemName = "LightGray"
        PenColorArray(LIGHTGRAY_PEN).ListIndex = 3

        PenColorArray(RED_PEN).RGBValue = RED
        PenColorArray(RED_PEN).ItemName = "Red"
        PenColorArray(RED_PEN).ListIndex = 4

        PenColorArray(DARKRED_PEN).RGBValue = DARKRED
        PenColorArray(DARKRED_PEN).ItemName = "DarkRed"
        PenColorArray(DARKRED_PEN).ListIndex = 5

        PenColorArray(LIGHTRED_PEN).RGBValue = LIGHTRED
        PenColorArray(LIGHTRED_PEN).ItemName = "LightRed"
        PenColorArray(LIGHTRED_PEN).ListIndex = 6

        PenColorArray(GREEN_PEN).RGBValue = GREEN
        PenColorArray(GREEN_PEN).ItemName = "Green"
        PenColorArray(GREEN_PEN).ListIndex = 7

        PenColorArray(DARKGREEN_PEN).RGBValue = DARKGREEN
        PenColorArray(DARKGREEN_PEN).ItemName = "DarkGreen"
        PenColorArray(DARKGREEN_PEN).ListIndex = 8

        PenColorArray(LIGHTGREEN_PEN).RGBValue = LIGHTGREEN
        PenColorArray(LIGHTGREEN_PEN).ItemName = "LightGreen"
        PenColorArray(LIGHTGREEN_PEN).ListIndex = 9

        PenColorArray(BLUE_PEN).RGBValue = BLUE
        PenColorArray(BLUE_PEN).ItemName = "Blue"
        PenColorArray(BLUE_PEN).ListIndex = 10

        PenColorArray(DARKBLUE_PEN).RGBValue = DARKBLUE
        PenColorArray(DARKBLUE_PEN).ItemName = "DarkBlue"
        PenColorArray(DARKBLUE_PEN).ListIndex = 11

        PenColorArray(LIGHTBLUE_PEN).RGBValue = LIGHTBLUE
        PenColorArray(LIGHTBLUE_PEN).ItemName = "LightBlue"
        PenColorArray(LIGHTBLUE_PEN).ListIndex = 12

        PenColorArray(YELLOW_PEN).RGBValue = YELLOW
        PenColorArray(YELLOW_PEN).ItemName = "Yellow"
        PenColorArray(YELLOW_PEN).ListIndex = 13

        PenColorArray(DARKYELLOW_PEN).RGBValue = DARKYELLOW
        PenColorArray(DARKYELLOW_PEN).ItemName = "DarkYellow"
        PenColorArray(DARKYELLOW_PEN).ListIndex = 14

        PenColorArray(LIGHTYELLOW_PEN).RGBValue = LIGHTYELLOW
        PenColorArray(LIGHTYELLOW_PEN).ItemName = "LightYellow"
        PenColorArray(LIGHTYELLOW_PEN).ListIndex = 15

        PenColorArray(CYAN_PEN).RGBValue = CYAN
        PenColorArray(CYAN_PEN).ItemName = "Cyan"
        PenColorArray(CYAN_PEN).ListIndex = 16

        PenColorArray(DARKCYAN_PEN).RGBValue = DARKCYAN
        PenColorArray(DARKCYAN_PEN).ItemName = "DarkCyan"
        PenColorArray(DARKCYAN_PEN).ListIndex = 17

        PenColorArray(MAGENTA_PEN).RGBValue = MAGENTA
        PenColorArray(MAGENTA_PEN).ItemName = "Magenta"
        PenColorArray(MAGENTA_PEN).ListIndex = 18

        PenColorArray(DARKMAGENTA_PEN).RGBValue = DARKMAGENTA
        PenColorArray(DARKMAGENTA_PEN).ItemName = "DarkMagenta"
        PenColorArray(DARKMAGENTA_PEN).ListIndex = 19

    End Sub

    Private Sub InitComboBox(ByRef RTCComboBox As ComboBox)

        ' build list of pen color names and specify list index for ordering

        For i As Integer = 1 To 20
            RTCComboBox.AddItem(PenColorArray(i).ItemName, PenColorArray(i).ListIndex)
            ' the ItemData property is used to store RGB color values
            RTCComboBox.SetItemData(PenColorArray(i).ListIndex, PenColorArray(i).RGBValue)
        Next i

    End Sub

    Private Sub InitComboBoxes()

        ' initialize each color combobox and set default color
        InitComboBox(AxesColorComboBox)
        InitComboBox(BackGroundColorComboBox)
        InitComboBox(BorderColorComboBox)
        InitComboBox(FrameColorComboBox)
        InitComboBox(GridColorComboBox)
        InitComboBox(LightColorComboBox)
        InitComboBox(OutlineColorComboBox)
        InitComboBox(ShadowColorComboBox)

    End Sub

    Private Sub InitFrontPanel(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl)

        ' color group - select default color
        AxesColorComboBox.SelectedIndex = PenColorArray(WHITE_PEN).ListIndex
        BackGroundColorComboBox.SelectedIndex = PenColorArray(DARKCYAN_PEN).ListIndex
        BorderColorComboBox.SelectedIndex = PenColorArray(LIGHTGRAY_PEN).ListIndex
        FrameColorComboBox.SelectedIndex = PenColorArray(WHITE_PEN).ListIndex
        GridColorComboBox.SelectedIndex = PenColorArray(CYAN_PEN).ListIndex
        LightColorComboBox.SelectedIndex = PenColorArray(DARKGRAY_PEN).ListIndex
        OutlineColorComboBox.SelectedIndex = PenColorArray(BLACK_PEN).ListIndex
        ShadowColorComboBox.SelectedIndex = PenColorArray(BLACK_PEN).ListIndex

        ' bevel group
        BevelStyleInnerComboBox.Text = BevelStyleArray(ReflectionHelper.GetMember(Of Integer)(rtChart, "BevelInner"))
        BevelStyleOuterComboBox.Text = BevelStyleArray(ReflectionHelper.GetMember(Of Integer)(rtChart, "BevelOuter"))
        BevelWidthOuterTextBox.Text = ReflectionHelper.GetMember(rtChart, "BevelWidth_Outer").ToString()
        BevelWidthInnerTextBox.Text = ReflectionHelper.GetMember(rtChart, "BevelWidth_Inner").ToString()

        ' border group
        BorderWidthTextBox.Text = ReflectionHelper.GetMember(rtChart, "BorderWidth").ToString()
        AutoSizeCheck.CheckState = Math.Abs(ReflectionHelper.GetMember(Of Double)(RtChart1, "AutoSize"))
        OutlineCheck.CheckState = Math.Abs(ReflectionHelper.GetMember(Of Double)(rtChart, "Outline"))
        RoundCornersCheck.CheckState = ReflectionHelper.GetMember(Of CheckState)(rtChart, "RoundedCorners")

        ' margin group
        MarginRightTextBox.Text = ReflectionHelper.GetMember(RtChart1, "MarginRight").ToString()
        MarginLeftTextBox.Text = ReflectionHelper.GetMember(RtChart1, "MarginLeft").ToString()
        MarginBottomTextBox.Text = ReflectionHelper.GetMember(RtChart1, "MarginBottom").ToString()
        MarginTopTextBox.Text = ReflectionHelper.GetMember(RtChart1, "MarginTop").ToString()

        ' divisions group
        MajorDivHorzTextBox.Text = ReflectionHelper.GetMember(RtChart1, "MajorDivHorz").ToString()
        MajorDivVertTextBox.Text = ReflectionHelper.GetMember(RtChart1, "MajorDivVert").ToString()
        MinorDivTextBox.Text = ReflectionHelper.GetMember(RtChart1, "MinorDiv").ToString()

        ' RtChart size group
        RtChartHeight.Text = ReflectionHelper.GetMember(RtChart1, "Height").ToString()
        RtChartWidth.Text = ReflectionHelper.GetMember(RtChart1, "Width").ToString()

        ' Graticule group
        FrameCheck.CheckState = CheckState.Checked
        AxesCheck.CheckState = CheckState.Checked
        GridCheck.CheckState = CheckState.Checked

        ' viewport group
        Viewports1.Checked = True

    End Sub

    Private Sub InitializeRtChart(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl)

        ' Global operation properties
        rtChart.HitTest = False ' disable mouse pointer "hit" detection
        rtChart.FramesPerSec = 0 ' 0 for asynchronous, -1 for synchronous

        ' set auto size to false to resize chart
        rtChart.AutoSize = (False)
        'RtChart.Height = DefaultRtChartHeight       ' VB standard property
        ' RtChart.Width = DefaultRtChartWidth       ' VB standard property

        ' View port Description
        rtChart.Viewports = 1 ' number of view ports displayed initially
        rtChart.ViewportStorageColor = Color.Green ' storage mode plot color, same for all view ports
        rtChart.LogicalViewport = 1 ' select view port
        rtChart.ViewportStorageOn = False ' disable storage mode

        ' Border description properties
        rtChart.BorderColor = Color.LightGray
        rtChart.LightColor = Color.DarkGray
        rtChart.ShadowColor = Color.Black
        rtChart.OutlineColor = Color.Black
        rtChart.Outline = True
        rtChart.RoundedCorners = False ' when true, removes corner pixel

        ' Bevel description properties
        rtChart.BevelInner = BevelStyle.Inset
        rtChart.BevelOuter = BevelStyle.Raised
        rtChart.BevelWidthInner = (4)
        rtChart.BevelWidthOuter = (4)
        rtChart.BorderWidth = 3

        ' Graticule description properties
        rtChart.BackColor = Color.DarkCyan ' background color

        rtChart.FrameOn = True
        rtChart.FrameTics = FrameTicsStyle.MajorMinorFrameTics
        rtChart.FrameColor = Color.White

        rtChart.AxesType = AxesType.HorizontalVertical
        rtChart.AxesTics = AxesTicsStyle.MajorMinorAxesTics
        rtChart.AxesColor = Color.White

        rtChart.GridOn = True
        rtChart.GridType = GridType.HorizontalVertical
        rtChart.GridColor = Drawing.Color.FromArgb(ChartPen.CYAN)

        ReflectionHelper.LetMember(rtChart, "MajorDivHorz", 10) ' major horizontal divisions
        rtChart.MajorDivVert = (8) ' major vertical divisions
        rtChart.MinorDiv = 5 ' minor divisions

        rtChart.MarginTop = 2 ' top Graticule border
        rtChart.MarginBottom = 2 ' bottom Graticule border
        rtChart.MarginLeft = 2 ' left Graticule border
        rtChart.MarginRight = 2 ' right Graticule border

        rtChart.ColorDepth = ColorDepth.Color16 ' 16 color mode

        ' true forces the same border width on all sides
        'RtChart.AutoSize = True                   ' frame and grid are always square

    End Sub

    Private Sub LightColorComboBox_SelectedIndexChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles LightColorComboBox.SelectedIndexChanged

        ' the ItemData property stores the RGB color value which is indexed by the seleted list item
        ReflectionHelper.LetMember(RtChart1, "LightColor", LightColorComboBox.GetItemData(LightColorComboBox.SelectedIndex))

    End Sub

    Private Sub MajorDivHorzTextBox_Enter(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MajorDivHorzTextBox.Enter

        MajorDivHorzTextBox.SelectionStart = 0
        MajorDivHorzTextBox.SelectionLength = Strings.Len(MajorDivHorzTextBox.Text)

    End Sub

    Private Sub MajorDivHorzTextBox_KeyDown(ByVal eventSender As Object, ByVal eventArgs As KeyEventArgs) Handles MajorDivHorzTextBox.KeyDown
        Dim KeyCode As Keys = eventArgs.KeyCode
        Dim Shift As Integer = eventArgs.KeyData / 65536
        Try

            If KeyCode = Keys.Return Then
                TempVal = CInt(Conversion.Val(MajorDivHorzTextBox.Text)) ' major horizontal divisions

                If TempVal < 0 Then Exit Sub

                ' number of horizontal grid lines
                ReflectionHelper.LetMember(RtChart1, "MajorDivHorz", TempVal)

                ' update RtChart height and width
                RtChartHeight.Text = ReflectionHelper.GetMember(RtChart1, "Height").ToString()
                RtChartWidth.Text = ReflectionHelper.GetMember(RtChart1, "Width").ToString()
            End If
        Finally
            eventArgs.Handled = KeyCode = 0
        End Try

    End Sub

    Private Sub MajorDivHorzTextBox_Leave(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MajorDivHorzTextBox.Leave

        MajorDivHorzTextBox_KeyDown(MajorDivHorzTextBox, New KeyEventArgs(Keys.Return))

    End Sub

    Private Sub MajorDivVertTextBox_Enter(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MajorDivVertTextBox.Enter

        MajorDivVertTextBox.SelectionStart = 0
        MajorDivVertTextBox.SelectionLength = Strings.Len(MajorDivVertTextBox.Text)

    End Sub

    Private Sub MajorDivVertTextBox_KeyDown(ByVal eventSender As Object, ByVal eventArgs As KeyEventArgs) Handles MajorDivVertTextBox.KeyDown
        Dim KeyCode As Keys = eventArgs.KeyCode
        Dim Shift As Integer = eventArgs.KeyData / 65536
        Try

            If KeyCode = Keys.Return Then
                TempVal = CInt(Conversion.Val(MajorDivVertTextBox.Text)) ' major vertical divisions

                If TempVal < 0 Then Exit Sub

                ' number of vertical grid lines
                ReflectionHelper.LetMember(RtChart1, "MajorDivVert", TempVal)

                ' update RtChart height and width
                RtChartHeight.Text = ReflectionHelper.GetMember(RtChart1, "Height").ToString()
                RtChartWidth.Text = ReflectionHelper.GetMember(RtChart1, "Width").ToString()
            End If
        Finally
            eventArgs.Handled = KeyCode = 0
        End Try

    End Sub

    Private Sub MajorDivVertTextBox_Leave(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MajorDivVertTextBox.Leave

        MajorDivVertTextBox_KeyDown(MajorDivVertTextBox, New KeyEventArgs(Keys.Return))

    End Sub

    Private Sub MarginBottomTextBox_Enter(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MarginBottomTextBox.Enter

        MarginBottomTextBox.SelectionStart = 0
        MarginBottomTextBox.SelectionLength = Strings.Len(MarginBottomTextBox.Text)

    End Sub

    Private Sub MarginBottomTextBox_KeyDown(ByVal eventSender As Object, ByVal eventArgs As KeyEventArgs) Handles MarginBottomTextBox.KeyDown
        Dim KeyCode As Keys = eventArgs.KeyCode
        Dim Shift As Integer = eventArgs.KeyData / 65536
        Try

            If KeyCode = Keys.Return Then
                TempVal = CInt(Conversion.Val(MarginBottomTextBox.Text))

                If TempVal < 0 Then Exit Sub

                ' bottom Graticule border
                ReflectionHelper.LetMember(RtChart1, "MarginBottom", TempVal)

                ' update RtChart height and width
                RtChartHeight.Text = ReflectionHelper.GetMember(RtChart1, "Height").ToString()
                RtChartWidth.Text = ReflectionHelper.GetMember(RtChart1, "Width").ToString()
            End If
        Finally
            eventArgs.Handled = KeyCode = 0
        End Try

    End Sub

    Private Sub MarginBottomTextBox_Leave(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MarginBottomTextBox.Leave

        MarginBottomTextBox_KeyDown(MarginBottomTextBox, New KeyEventArgs(Keys.Return))

    End Sub

    Private Sub MarginLeftTextBox_Enter(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MarginLeftTextBox.Enter

        MarginLeftTextBox.SelectionStart = 0
        MarginLeftTextBox.SelectionLength = Strings.Len(MarginLeftTextBox.Text)

    End Sub

    Private Sub MarginLeftTextBox_KeyDown(ByVal eventSender As Object, ByVal eventArgs As KeyEventArgs) Handles MarginLeftTextBox.KeyDown
        Dim KeyCode As Keys = eventArgs.KeyCode
        Dim Shift As Integer = eventArgs.KeyData / 65536
        Try

            If KeyCode = Keys.Return Then
                TempVal = CInt(Conversion.Val(MarginLeftTextBox.Text))

                If TempVal < 0 Then Exit Sub

                ' left Graticule border
                ReflectionHelper.LetMember(RtChart1, "MarginLeft", TempVal)

                ' update RtChart height and width
                RtChartHeight.Text = ReflectionHelper.GetMember(RtChart1, "Height").ToString()
                RtChartWidth.Text = ReflectionHelper.GetMember(RtChart1, "Width").ToString()
            End If
        Finally
            eventArgs.Handled = KeyCode = 0
        End Try

    End Sub

    Private Sub MarginLeftTextBox_Leave(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MarginLeftTextBox.Leave

        MarginLeftTextBox_KeyDown(MarginLeftTextBox, New KeyEventArgs(Keys.Return))

    End Sub

    Private Sub MarginRightTextBox_Enter(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MarginRightTextBox.Enter

        MarginRightTextBox.SelectionStart = 0
        MarginRightTextBox.SelectionLength = Strings.Len(MarginRightTextBox.Text)

    End Sub

    Private Sub MarginRightTextBox_KeyDown(ByVal eventSender As Object, ByVal eventArgs As KeyEventArgs) Handles MarginRightTextBox.KeyDown
        Dim KeyCode As Keys = eventArgs.KeyCode
        Dim Shift As Integer = eventArgs.KeyData / 65536
        Try

            If KeyCode = Keys.Return Then
                TempVal = CInt(Conversion.Val(MarginRightTextBox.Text))

                If TempVal < 0 Then Exit Sub

                ' right Graticule border
                ReflectionHelper.LetMember(RtChart1, "MarginRight", TempVal)

                ' update RtChart height and width
                RtChartHeight.Text = ReflectionHelper.GetMember(RtChart1, "Height").ToString()
                RtChartWidth.Text = ReflectionHelper.GetMember(RtChart1, "Width").ToString()
            End If
        Finally
            eventArgs.Handled = KeyCode = 0
        End Try

    End Sub

    Private Sub MarginRightTextBox_Leave(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MarginRightTextBox.Leave

        MarginRightTextBox_KeyDown(MarginRightTextBox, New KeyEventArgs(Keys.Return))

    End Sub

    Private Sub MarginTopTextBox_Enter(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MarginTopTextBox.Enter

        MarginTopTextBox.SelectionStart = 0
        MarginTopTextBox.SelectionLength = Strings.Len(MarginTopTextBox.Text)

    End Sub

    Private Sub MarginTopTextBox_KeyDown(ByVal eventSender As Object, ByVal eventArgs As KeyEventArgs) Handles MarginTopTextBox.KeyDown
        Dim KeyCode As Keys = eventArgs.KeyCode
        Dim Shift As Integer = eventArgs.KeyData / 65536
        Try

            If KeyCode = Keys.Return Then
                TempVal = CInt(Conversion.Val(MarginTopTextBox.Text))

                If TempVal < 0 Then Exit Sub

                ' top Graticule border
                ReflectionHelper.LetMember(RtChart1, "MarginTop", TempVal)

                ' update RtChart height and width
                RtChartHeight.Text = ReflectionHelper.GetMember(RtChart1, "Height").ToString()
                RtChartWidth.Text = ReflectionHelper.GetMember(RtChart1, "Width").ToString()
            End If
        Finally
            eventArgs.Handled = KeyCode = 0
        End Try

    End Sub

    Private Sub MarginTopTextBox_Leave(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MarginTopTextBox.Leave

        MarginTopTextBox_KeyDown(MarginTopTextBox, New KeyEventArgs(Keys.Return))

    End Sub

    'UPGRADE_NOTE: (7001) The following declaration (MenuItem_Exit_Click) seems to be dead code More Information: https://www.mobilize.net/vbtonet/ewis/ewi7001
    'Private Sub MenuItem_Exit_Click()
    '
    'Me.Close()
    '
    'End Sub

    Private Sub MinorDivTextBox_TextChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MinorDivTextBox.TextChanged
        If isInitializingComponent Then Return

        TempVal = CInt(Conversion.Val(MinorDivTextBox.Text)) ' minor divisions

        If TempVal < 0 Then Exit Sub

        ReflectionHelper.LetMember(RtChart1, "MinorDiv", TempVal)

    End Sub

    Private Sub MinorDivTextBox_Enter(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MinorDivTextBox.Enter

        MinorDivTextBox.SelectionStart = 0
        MinorDivTextBox.SelectionLength = Strings.Len(MinorDivTextBox.Text)

    End Sub

    Private Sub MinorDivTextBox_KeyDown(ByVal eventSender As Object, ByVal eventArgs As KeyEventArgs) Handles MinorDivTextBox.KeyDown
        Dim KeyCode As Keys = eventArgs.KeyCode
        Dim Shift As Integer = eventArgs.KeyData / 65536
        Try

            If KeyCode = Keys.Return Then
                TempVal = CInt(Conversion.Val(MinorDivTextBox.Text)) ' minor divisions

                If TempVal < 0 Then Exit Sub

                ReflectionHelper.LetMember(RtChart1, "MinorDiv", TempVal)
            End If
        Finally
            eventArgs.Handled = KeyCode = 0
        End Try

    End Sub

    Private Sub MinorDivTextBox_Leave(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MinorDivTextBox.Leave

        MinorDivTextBox_KeyDown(MinorDivTextBox, New KeyEventArgs(Keys.Return))

    End Sub

    Private Sub OutlineCheck_CheckStateChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles OutlineCheck.CheckStateChanged
        If isInitializingComponent Then Return

        ReflectionHelper.LetMember(RtChart1, "Outline", OutlineCheck.CheckState)

    End Sub

    Private Sub OutlineColorComboBox_SelectedIndexChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles OutlineColorComboBox.SelectedIndexChanged

        ' the ItemData property stores the RGB color value which is indexed by the seleted list item
        ReflectionHelper.LetMember(RtChart1, "OutlineColor", OutlineColorComboBox.GetItemData(OutlineColorComboBox.SelectedIndex))

    End Sub

    Private Sub RoundCornersCheck_CheckStateChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles RoundCornersCheck.CheckStateChanged
        If isInitializingComponent Then Return

        ReflectionHelper.LetMember(RtChart1, "RoundedCorners", RoundCornersCheck.CheckState)

        ' update RtChart height and width
        RtChartHeight.Text = ReflectionHelper.GetMember(RtChart1, "Height").ToString()
        RtChartWidth.Text = ReflectionHelper.GetMember(RtChart1, "Width").ToString()

    End Sub

    Private Sub RtChartHeight_Enter(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles RtChartHeight.Enter

        RtChartHeight.SelectionStart = 0
        RtChartHeight.SelectionLength = Strings.Len(RtChartHeight.Text)

    End Sub

    Private Sub RtChartHeight_KeyDown(ByVal eventSender As Object, ByVal eventArgs As KeyEventArgs) Handles RtChartHeight.KeyDown
        Dim KeyCode As Keys = eventArgs.KeyCode
        Dim Shift As Integer = eventArgs.KeyData / 65536
        Try

            If KeyCode = Keys.Return Then
                TempVal = CInt(Conversion.Val(RtChartHeight.Text))
                If TempVal < 0 Or TempVal > 32767 Then Exit Sub

                ReflectionHelper.LetMember(RtChart1, "Height", TempVal)
            End If
        Finally
            eventArgs.Handled = KeyCode = 0
        End Try

    End Sub

    Private Sub RtChartHeight_Leave(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles RtChartHeight.Leave

        RtChartHeight_KeyDown(RtChartHeight, New KeyEventArgs(Keys.Return))

    End Sub

    Private Sub RtChartWidth_Enter(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles RtChartWidth.Enter

        RtChartWidth.SelectionStart = 0
        RtChartWidth.SelectionLength = Strings.Len(RtChartWidth.Text)

    End Sub

    Private Sub RtChartWidth_KeyDown(ByVal eventSender As Object, ByVal eventArgs As KeyEventArgs) Handles RtChartWidth.KeyDown
        Dim KeyCode As Keys = eventArgs.KeyCode
        Dim Shift As Integer = eventArgs.KeyData / 65536
        Try

            If KeyCode = Keys.Return Then
                TempVal = CInt(Conversion.Val(RtChartWidth.Text))
                If TempVal < 0 Or TempVal > 32767 Then Exit Sub

                ReflectionHelper.LetMember(RtChart1, "Width", TempVal)
            End If
        Finally
            eventArgs.Handled = KeyCode = 0
        End Try

    End Sub

    Private Sub RtChartWidth_Leave(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles RtChartWidth.Leave

        RtChartWidth_KeyDown(RtChartWidth, New KeyEventArgs(Keys.Return))

    End Sub

    Private Sub ShadowColorComboBox_SelectedIndexChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles ShadowColorComboBox.SelectedIndexChanged

        ' the ItemData property stores the RGB color value which is indexed by the seleted list item
        ReflectionHelper.LetMember(RtChart1, "ShadowColor", ShadowColorComboBox.GetItemData(ShadowColorComboBox.SelectedIndex))

    End Sub

    Private Sub Viewports1_CheckedChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles Viewports1.CheckedChanged
        If eventSender.Checked Then
            If isInitializingComponent Then
                Exit Sub
            End If

            ' number of viewports
            ReflectionHelper.LetMember(RtChart1, "Viewports", 1)

        End If
    End Sub

    Private Sub Viewports2_CheckedChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles Viewports2.CheckedChanged
        If eventSender.Checked Then
            If isInitializingComponent Then
                Exit Sub
            End If

            ' number of viewports
            ReflectionHelper.LetMember(RtChart1, "Viewports", 2)

        End If
    End Sub

    Private Sub Viewports4_CheckedChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles Viewports4.CheckedChanged
        If eventSender.Checked Then
            If isInitializingComponent Then
                Exit Sub
            End If

            ' number of viewports
            ReflectionHelper.LetMember(RtChart1, "Viewports", 4)

        End If
    End Sub
    Private Sub Form_Closed(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MyBase.Closed
    End Sub
End Class