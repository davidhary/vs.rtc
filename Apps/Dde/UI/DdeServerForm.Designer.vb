<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DdeServerForm
#Region "Upgrade Support "
	Private Shared _Instance As DdeServerForm
	Private Shared _InitializingInstance As Boolean
	Public Shared Property DefInstance() As DdeServerForm
		Get
			If _Instance Is Nothing OrElse _Instance.IsDisposed Then
				_InitializingInstance = True
				_Instance = CreateInstance()
				_InitializingInstance = False
			End If
			Return _Instance
		End Get
		Set(ByVal value As DdeServerForm)
			_Instance = Value
		End Set
	End Property
#End Region
#Region "Windows Form Designer generated code "
	Public Shared Function CreateInstance() As DdeServerForm
		Dim theInstance As New DdeServerForm()
		theInstance.Form_Load()
		Return theInstance
	End Function
	Private visualControls() As String = New String() {"components", "ToolTipMain", "_mnuFileItem_0", "_mnuFileItem_1", "_mnuFileItem_2", "_mnuFileItem_3", "_mnuFileItem_4", "_mnuFileItem_5", "_mnuFileItem_6", "mnuFile", "_mnuEditItem_0", "mnuEdit", "_mnuHelpItem_0", "mnuHelp", "MainMenu1", "_ColorDepth_1", "_ColorDepth_0", "Frame5", "chkGrid", "chkAxes", "chkFrame", "Frame4", "comboScreens", "cmdErase", "chkStorage", "Frame3", "chkIntense", "chkLines", "Frame2", "comboScale", "Frame1", "StartStop", "Copy", "Save", "Print", "Zero", "VScroll1", "HScroll1", "Timer1", "RtChart1", "CMDialog1Print", "CMDialog1", "Label1", "SysItems", "Formats", "Topics", "ColorDepth", "mnuEditItem", "mnuFileItem", "mnuHelpItem", "listBoxComboBoxHelper1"}
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTipMain As System.Windows.Forms.ToolTip
	Private WithEvents _mnuFileItem_0 As System.Windows.Forms.ToolStripMenuItem
	Private WithEvents _mnuFileItem_1 As System.Windows.Forms.ToolStripMenuItem
	Private WithEvents _mnuFileItem_2 As System.Windows.Forms.ToolStripSeparator
	Private WithEvents _mnuFileItem_3 As System.Windows.Forms.ToolStripMenuItem
	Private WithEvents _mnuFileItem_4 As System.Windows.Forms.ToolStripMenuItem
	Private WithEvents _mnuFileItem_5 As System.Windows.Forms.ToolStripSeparator
	Private WithEvents _mnuFileItem_6 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFile As System.Windows.Forms.ToolStripMenuItem
	Private WithEvents _mnuEditItem_0 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuEdit As System.Windows.Forms.ToolStripMenuItem
	Private WithEvents _mnuHelpItem_0 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuHelp As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents MainMenu1 As System.Windows.Forms.MenuStrip
	Private WithEvents _ColorDepth_1 As System.Windows.Forms.RadioButton
	Private WithEvents _ColorDepth_0 As System.Windows.Forms.RadioButton
	Public WithEvents Frame5 As System.Windows.Forms.GroupBox
	Public WithEvents chkGrid As System.Windows.Forms.CheckBox
	Public WithEvents chkAxes As System.Windows.Forms.CheckBox
	Public WithEvents chkFrame As System.Windows.Forms.CheckBox
	Public WithEvents Frame4 As System.Windows.Forms.GroupBox
	Public WithEvents comboScreens As System.Windows.Forms.ComboBox
	Public WithEvents cmdErase As System.Windows.Forms.Button
	Public WithEvents chkStorage As System.Windows.Forms.CheckBox
	Public WithEvents Frame3 As System.Windows.Forms.GroupBox
	Public WithEvents chkIntense As System.Windows.Forms.CheckBox
	Public WithEvents chkLines As System.Windows.Forms.CheckBox
	Public WithEvents Frame2 As System.Windows.Forms.GroupBox
	Public WithEvents comboScale As System.Windows.Forms.ComboBox
	Public WithEvents Frame1 As System.Windows.Forms.GroupBox
	Public WithEvents StartStop As System.Windows.Forms.Button
	Public WithEvents Copy As System.Windows.Forms.Button
	Public WithEvents Save As System.Windows.Forms.Button
	Public WithEvents Print As System.Windows.Forms.Button
	Public WithEvents Zero As System.Windows.Forms.Button
	Public WithEvents VScroll1 As System.Windows.Forms.VScrollBar
	Public WithEvents HScroll1 As System.Windows.Forms.HScrollBar
	Public WithEvents Timer1 As System.Windows.Forms.Timer
	Public WithEvents RtChart1 As isr.Visuals.RealTimeChart.RealTimeChartControl
	Public CMDialog1Print As System.Windows.Forms.PrintDialog
	Public CMDialog1 As UpgradeSolution1Support.UpgradeStubs.AxMSComDlg_AxCommonDialog
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents SysItems As System.Windows.Forms.Label
	Public WithEvents Formats As System.Windows.Forms.Label
	Public WithEvents Topics As System.Windows.Forms.Label
	Public ColorDepth(1) As System.Windows.Forms.RadioButton
	Public mnuEditItem(0) As System.Windows.Forms.ToolStripItem
	Public mnuFileItem(6) As System.Windows.Forms.ToolStripItem
	Public mnuHelpItem(0) As System.Windows.Forms.ToolStripItem
	Private listBoxComboBoxHelper1 As UpgradeHelpers.Gui.ListControlHelper
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> _
	 Private Sub InitializeComponent()
		Me.components = New System.ComponentModel.Container()
		Dim resources As New System.ComponentModel.ComponentResourceManager(GetType(DdeServerForm))
		Me.ToolTipMain = New System.Windows.Forms.ToolTip(Me.components)
		Me.MainMenu1 = New System.Windows.Forms.MenuStrip()
		Me.mnuFile = New System.Windows.Forms.ToolStripMenuItem()
		Me._mnuFileItem_0 = New System.Windows.Forms.ToolStripMenuItem()
		Me._mnuFileItem_1 = New System.Windows.Forms.ToolStripMenuItem()
		Me._mnuFileItem_2 = New System.Windows.Forms.ToolStripSeparator()
		Me._mnuFileItem_3 = New System.Windows.Forms.ToolStripMenuItem()
		Me._mnuFileItem_4 = New System.Windows.Forms.ToolStripMenuItem()
		Me._mnuFileItem_5 = New System.Windows.Forms.ToolStripSeparator()
		Me._mnuFileItem_6 = New System.Windows.Forms.ToolStripMenuItem()
		Me.mnuEdit = New System.Windows.Forms.ToolStripMenuItem()
		Me._mnuEditItem_0 = New System.Windows.Forms.ToolStripMenuItem()
		Me.mnuHelp = New System.Windows.Forms.ToolStripMenuItem()
		Me._mnuHelpItem_0 = New System.Windows.Forms.ToolStripMenuItem()
		Me.Frame5 = New System.Windows.Forms.GroupBox()
		Me._ColorDepth_1 = New System.Windows.Forms.RadioButton()
		Me._ColorDepth_0 = New System.Windows.Forms.RadioButton()
		Me.Frame4 = New System.Windows.Forms.GroupBox()
		Me.chkGrid = New System.Windows.Forms.CheckBox()
		Me.chkAxes = New System.Windows.Forms.CheckBox()
		Me.chkFrame = New System.Windows.Forms.CheckBox()
		Me.Frame3 = New System.Windows.Forms.GroupBox()
		Me.comboScreens = New System.Windows.Forms.ComboBox()
		Me.cmdErase = New System.Windows.Forms.Button()
		Me.chkStorage = New System.Windows.Forms.CheckBox()
		Me.Frame2 = New System.Windows.Forms.GroupBox()
		Me.chkIntense = New System.Windows.Forms.CheckBox()
		Me.chkLines = New System.Windows.Forms.CheckBox()
		Me.Frame1 = New System.Windows.Forms.GroupBox()
		Me.comboScale = New System.Windows.Forms.ComboBox()
		Me.StartStop = New System.Windows.Forms.Button()
		Me.Copy = New System.Windows.Forms.Button()
		Me.Save = New System.Windows.Forms.Button()
		Me.Print = New System.Windows.Forms.Button()
		Me.Zero = New System.Windows.Forms.Button()
		Me.VScroll1 = New System.Windows.Forms.VScrollBar()
		Me.HScroll1 = New System.Windows.Forms.HScrollBar()
		Me.Timer1 = New System.Windows.Forms.Timer(components)
		Me.RtChart1 = New isr.Visuals.RealTimeChart.RealTimeChartControl()
		Me.CMDialog1Print = New System.Windows.Forms.PrintDialog()
		Me.CMDialog1Print.PrinterSettings = New System.Drawing.Printing.PrinterSettings()
		Me.CMDialog1 = New UpgradeSolution1Support.UpgradeStubs.AxMSComDlg_AxCommonDialog()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.SysItems = New System.Windows.Forms.Label()
		Me.Formats = New System.Windows.Forms.Label()
		Me.Topics = New System.Windows.Forms.Label()
		Me.Frame5.SuspendLayout()
		Me.Frame4.SuspendLayout()
		Me.Frame3.SuspendLayout()
		Me.Frame2.SuspendLayout()
		Me.Frame1.SuspendLayout()
		Me.SuspendLayout()
		Me.listBoxComboBoxHelper1 = New UpgradeHelpers.Gui.ListControlHelper(Me.components)
		Me.listBoxComboBoxHelper1.BeginInit()
		' 
		'MainMenu1
		' 
		Me.MainMenu1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFile, Me.mnuEdit, Me.mnuHelp})
		' 
		'mnuFile
		' 
		Me.mnuFile.Available = True
		Me.mnuFile.Checked = False
		Me.mnuFile.Enabled = True
		Me.mnuFile.Name = "mnuFile"
		Me.mnuFile.Text = "&File"
		Me.mnuFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._mnuFileItem_0, Me._mnuFileItem_1, Me._mnuFileItem_2, Me._mnuFileItem_3, Me._mnuFileItem_4, Me._mnuFileItem_5, Me._mnuFileItem_6})
		' 
		'_mnuFileItem_0
		' 
		Me._mnuFileItem_0.Available = True
		Me._mnuFileItem_0.Checked = False
		Me._mnuFileItem_0.Enabled = True
		Me._mnuFileItem_0.Name = "_mnuFileItem_0"
		Me._mnuFileItem_0.ShortcutKeys = CType(System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S, System.Windows.Forms.Keys)
		Me._mnuFileItem_0.Text = "&Save File"
		' 
		'_mnuFileItem_1
		' 
		Me._mnuFileItem_1.Available = True
		Me._mnuFileItem_1.Checked = False
		Me._mnuFileItem_1.Enabled = True
		Me._mnuFileItem_1.Name = "_mnuFileItem_1"
		Me._mnuFileItem_1.ShortcutKeys = CType(System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.A, System.Windows.Forms.Keys)
		Me._mnuFileItem_1.Text = "Save File &As..."
		' 
		'_mnuFileItem_2
		' 
		Me._mnuFileItem_2.AllowDrop = True
		Me._mnuFileItem_2.Available = True
		Me._mnuFileItem_2.Enabled = True
		Me._mnuFileItem_2.Name = "_mnuFileItem_2"
		' 
		'_mnuFileItem_3
		' 
		Me._mnuFileItem_3.Available = True
		Me._mnuFileItem_3.Checked = False
		Me._mnuFileItem_3.Enabled = True
		Me._mnuFileItem_3.Name = "_mnuFileItem_3"
		Me._mnuFileItem_3.ShortcutKeys = CType(System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.P, System.Windows.Forms.Keys)
		Me._mnuFileItem_3.Text = "&Print..."
		' 
		'_mnuFileItem_4
		' 
		Me._mnuFileItem_4.Available = True
		Me._mnuFileItem_4.Checked = False
		Me._mnuFileItem_4.Enabled = True
		Me._mnuFileItem_4.Name = "_mnuFileItem_4"
		Me._mnuFileItem_4.Text = "Print &Setup..."
		' 
		'_mnuFileItem_5
		' 
		Me._mnuFileItem_5.AllowDrop = True
		Me._mnuFileItem_5.Available = True
		Me._mnuFileItem_5.Enabled = True
		Me._mnuFileItem_5.Name = "_mnuFileItem_5"
		' 
		'_mnuFileItem_6
		' 
		Me._mnuFileItem_6.Available = True
		Me._mnuFileItem_6.Checked = False
		Me._mnuFileItem_6.Enabled = True
		Me._mnuFileItem_6.Name = "_mnuFileItem_6"
		Me._mnuFileItem_6.Text = "E&xit"
		' 
		'mnuEdit
		' 
		Me.mnuEdit.Available = True
		Me.mnuEdit.Checked = False
		Me.mnuEdit.Enabled = True
		Me.mnuEdit.Name = "mnuEdit"
		Me.mnuEdit.Text = "&Edit"
		Me.mnuEdit.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._mnuEditItem_0})
		' 
		'_mnuEditItem_0
		' 
		Me._mnuEditItem_0.Available = True
		Me._mnuEditItem_0.Checked = False
		Me._mnuEditItem_0.Enabled = True
		Me._mnuEditItem_0.Name = "_mnuEditItem_0"
		Me._mnuEditItem_0.ShortcutKeys = CType(System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.C, System.Windows.Forms.Keys)
		Me._mnuEditItem_0.Text = "&Copy"
		' 
		'mnuHelp
		' 
		Me.mnuHelp.Available = True
		Me.mnuHelp.Checked = False
		Me.mnuHelp.Enabled = True
		Me.mnuHelp.Name = "mnuHelp"
		Me.mnuHelp.Text = "&Help"
		Me.mnuHelp.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._mnuHelpItem_0})
		' 
		'_mnuHelpItem_0
		' 
		Me._mnuHelpItem_0.Available = True
		Me._mnuHelpItem_0.Checked = False
		Me._mnuHelpItem_0.Enabled = True
		Me._mnuHelpItem_0.Name = "_mnuHelpItem_0"
		Me._mnuHelpItem_0.Text = "&About Real-Time Chart..."
		' 
		'Frame5
		' 
		Me.Frame5.AllowDrop = True
		Me.Frame5.BackColor = System.Drawing.SystemColors.Control
		Me.Frame5.Controls.Add(Me._ColorDepth_1)
		Me.Frame5.Controls.Add(Me._ColorDepth_0)
		Me.Frame5.Enabled = True
		Me.Frame5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Frame5.Location = New System.Drawing.Point(344, 368)
		Me.Frame5.Name = "Frame5"
		Me.Frame5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame5.Size = New System.Drawing.Size(113, 65)
		Me.Frame5.TabIndex = 22
		Me.Frame5.Text = "Color Depth"
		Me.Frame5.Visible = True
		' 
		'_ColorDepth_1
		' 
		Me._ColorDepth_1.AllowDrop = True
		Me._ColorDepth_1.Appearance = System.Windows.Forms.Appearance.Normal
		Me._ColorDepth_1.BackColor = System.Drawing.SystemColors.Control
		Me._ColorDepth_1.CausesValidation = True
		Me._ColorDepth_1.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me._ColorDepth_1.Checked = False
		Me._ColorDepth_1.Enabled = False
		Me._ColorDepth_1.ForeColor = System.Drawing.SystemColors.ControlText
		Me._ColorDepth_1.Location = New System.Drawing.Point(16, 32)
		Me._ColorDepth_1.Name = "_ColorDepth_1"
		Me._ColorDepth_1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._ColorDepth_1.Size = New System.Drawing.Size(81, 17)
		Me._ColorDepth_1.TabIndex = 24
		Me._ColorDepth_1.TabStop = True
		Me._ColorDepth_1.Text = "256 color"
		Me._ColorDepth_1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me._ColorDepth_1.Visible = True
		' 
		'_ColorDepth_0
		' 
		Me._ColorDepth_0.AllowDrop = True
		Me._ColorDepth_0.Appearance = System.Windows.Forms.Appearance.Normal
		Me._ColorDepth_0.BackColor = System.Drawing.SystemColors.Control
		Me._ColorDepth_0.CausesValidation = True
		Me._ColorDepth_0.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me._ColorDepth_0.Checked = True
		Me._ColorDepth_0.Enabled = True
		Me._ColorDepth_0.ForeColor = System.Drawing.SystemColors.ControlText
		Me._ColorDepth_0.Location = New System.Drawing.Point(16, 16)
		Me._ColorDepth_0.Name = "_ColorDepth_0"
		Me._ColorDepth_0.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._ColorDepth_0.Size = New System.Drawing.Size(81, 17)
		Me._ColorDepth_0.TabIndex = 23
		Me._ColorDepth_0.TabStop = True
		Me._ColorDepth_0.Text = "16 color"
		Me._ColorDepth_0.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me._ColorDepth_0.Visible = True
		' 
		'Frame4
		' 
		Me.Frame4.AllowDrop = True
		Me.Frame4.BackColor = System.Drawing.SystemColors.Control
		Me.Frame4.Controls.Add(Me.chkGrid)
		Me.Frame4.Controls.Add(Me.chkAxes)
		Me.Frame4.Controls.Add(Me.chkFrame)
		Me.Frame4.Enabled = True
		Me.Frame4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Frame4.Location = New System.Drawing.Point(256, 336)
		Me.Frame4.Name = "Frame4"
		Me.Frame4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame4.Size = New System.Drawing.Size(81, 97)
		Me.Frame4.TabIndex = 18
		Me.Frame4.Text = "Graticule"
		Me.Frame4.Visible = True
		' 
		'chkGrid
		' 
		Me.chkGrid.AllowDrop = True
		Me.chkGrid.Appearance = System.Windows.Forms.Appearance.Normal
		Me.chkGrid.BackColor = System.Drawing.SystemColors.Control
		Me.chkGrid.CausesValidation = True
		Me.chkGrid.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.chkGrid.CheckState = System.Windows.Forms.CheckState.Checked
		Me.chkGrid.Enabled = True
		Me.chkGrid.ForeColor = System.Drawing.SystemColors.ControlText
		Me.chkGrid.Location = New System.Drawing.Point(8, 64)
		Me.chkGrid.Name = "chkGrid"
		Me.chkGrid.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.chkGrid.Size = New System.Drawing.Size(57, 25)
		Me.chkGrid.TabIndex = 21
		Me.chkGrid.TabStop = True
		Me.chkGrid.Text = "Grid"
		Me.chkGrid.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.chkGrid.Visible = True
		' 
		'chkAxes
		' 
		Me.chkAxes.AllowDrop = True
		Me.chkAxes.Appearance = System.Windows.Forms.Appearance.Normal
		Me.chkAxes.BackColor = System.Drawing.SystemColors.Control
		Me.chkAxes.CausesValidation = True
		Me.chkAxes.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.chkAxes.CheckState = System.Windows.Forms.CheckState.Checked
		Me.chkAxes.Enabled = True
		Me.chkAxes.ForeColor = System.Drawing.SystemColors.ControlText
		Me.chkAxes.Location = New System.Drawing.Point(8, 40)
		Me.chkAxes.Name = "chkAxes"
		Me.chkAxes.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.chkAxes.Size = New System.Drawing.Size(49, 17)
		Me.chkAxes.TabIndex = 20
		Me.chkAxes.TabStop = True
		Me.chkAxes.Text = "Axes"
		Me.chkAxes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.chkAxes.Visible = True
		' 
		'chkFrame
		' 
		Me.chkFrame.AllowDrop = True
		Me.chkFrame.Appearance = System.Windows.Forms.Appearance.Normal
		Me.chkFrame.BackColor = System.Drawing.SystemColors.Control
		Me.chkFrame.CausesValidation = True
		Me.chkFrame.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.chkFrame.CheckState = System.Windows.Forms.CheckState.Checked
		Me.chkFrame.Enabled = True
		Me.chkFrame.ForeColor = System.Drawing.SystemColors.ControlText
		Me.chkFrame.Location = New System.Drawing.Point(8, 16)
		Me.chkFrame.Name = "chkFrame"
		Me.chkFrame.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.chkFrame.Size = New System.Drawing.Size(57, 17)
		Me.chkFrame.TabIndex = 19
		Me.chkFrame.TabStop = True
		Me.chkFrame.Text = "Frame"
		Me.chkFrame.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.chkFrame.Visible = True
		' 
		'Frame3
		' 
		Me.Frame3.AllowDrop = True
		Me.Frame3.BackColor = System.Drawing.SystemColors.Control
		Me.Frame3.Controls.Add(Me.comboScreens)
		Me.Frame3.Controls.Add(Me.cmdErase)
		Me.Frame3.Controls.Add(Me.chkStorage)
		Me.Frame3.Enabled = True
		Me.Frame3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Frame3.Location = New System.Drawing.Point(160, 336)
		Me.Frame3.Name = "Frame3"
		Me.Frame3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame3.Size = New System.Drawing.Size(89, 97)
		Me.Frame3.TabIndex = 15
		Me.Frame3.Text = "Screens"
		Me.Frame3.Visible = True
		' 
		'comboScreens
		' 
		Me.comboScreens.AllowDrop = True
		Me.comboScreens.BackColor = System.Drawing.SystemColors.Window
		Me.comboScreens.CausesValidation = True
		Me.comboScreens.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.comboScreens.Enabled = True
		Me.comboScreens.ForeColor = System.Drawing.SystemColors.WindowText
		Me.comboScreens.IntegralHeight = True
		Me.comboScreens.Location = New System.Drawing.Point(16, 16)
		Me.comboScreens.Name = "comboScreens"
		Me.comboScreens.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.comboScreens.Size = New System.Drawing.Size(49, 21)
		Me.comboScreens.Sorted = False
		Me.comboScreens.TabIndex = 26
		Me.comboScreens.TabStop = True
		Me.comboScreens.Visible = True
		Me.comboScreens.Items.AddRange(New Object() {"1", "2", "4"})
		' 
		'cmdErase
		' 
		Me.cmdErase.AllowDrop = True
		Me.cmdErase.BackColor = System.Drawing.SystemColors.Control
		Me.cmdErase.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdErase.Location = New System.Drawing.Point(8, 64)
		Me.cmdErase.Name = "cmdErase"
		Me.cmdErase.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdErase.Size = New System.Drawing.Size(73, 25)
		Me.cmdErase.TabIndex = 17
		Me.cmdErase.Text = "Erase"
		Me.cmdErase.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
		Me.cmdErase.UseVisualStyleBackColor = False
		' 
		'chkStorage
		' 
		Me.chkStorage.AllowDrop = True
		Me.chkStorage.Appearance = System.Windows.Forms.Appearance.Normal
		Me.chkStorage.BackColor = System.Drawing.SystemColors.Control
		Me.chkStorage.CausesValidation = True
		Me.chkStorage.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.chkStorage.CheckState = System.Windows.Forms.CheckState.Unchecked
		Me.chkStorage.Enabled = True
		Me.chkStorage.ForeColor = System.Drawing.SystemColors.ControlText
		Me.chkStorage.Location = New System.Drawing.Point(8, 40)
		Me.chkStorage.Name = "chkStorage"
		Me.chkStorage.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.chkStorage.Size = New System.Drawing.Size(73, 17)
		Me.chkStorage.TabIndex = 16
		Me.chkStorage.TabStop = True
		Me.chkStorage.Text = "Storage"
		Me.chkStorage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.chkStorage.Visible = True
		' 
		'Frame2
		' 
		Me.Frame2.AllowDrop = True
		Me.Frame2.BackColor = System.Drawing.SystemColors.Control
		Me.Frame2.Controls.Add(Me.chkIntense)
		Me.Frame2.Controls.Add(Me.chkLines)
		Me.Frame2.Enabled = True
		Me.Frame2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Frame2.Location = New System.Drawing.Point(0, 392)
		Me.Frame2.Name = "Frame2"
		Me.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame2.Size = New System.Drawing.Size(153, 41)
		Me.Frame2.TabIndex = 12
		Me.Frame2.Text = "Channels"
		Me.Frame2.Visible = True
		' 
		'chkIntense
		' 
		Me.chkIntense.AllowDrop = True
		Me.chkIntense.Appearance = System.Windows.Forms.Appearance.Normal
		Me.chkIntense.BackColor = System.Drawing.SystemColors.Control
		Me.chkIntense.CausesValidation = True
		Me.chkIntense.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.chkIntense.CheckState = System.Windows.Forms.CheckState.Unchecked
		Me.chkIntense.Enabled = True
		Me.chkIntense.ForeColor = System.Drawing.SystemColors.ControlText
		Me.chkIntense.Location = New System.Drawing.Point(72, 16)
		Me.chkIntense.Name = "chkIntense"
		Me.chkIntense.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.chkIntense.Size = New System.Drawing.Size(65, 17)
		Me.chkIntense.TabIndex = 14
		Me.chkIntense.TabStop = True
		Me.chkIntense.Text = "Intense"
		Me.chkIntense.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.chkIntense.Visible = True
		' 
		'chkLines
		' 
		Me.chkLines.AllowDrop = True
		Me.chkLines.Appearance = System.Windows.Forms.Appearance.Normal
		Me.chkLines.BackColor = System.Drawing.SystemColors.Control
		Me.chkLines.CausesValidation = True
		Me.chkLines.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.chkLines.CheckState = System.Windows.Forms.CheckState.Checked
		Me.chkLines.Enabled = True
		Me.chkLines.ForeColor = System.Drawing.SystemColors.ControlText
		Me.chkLines.Location = New System.Drawing.Point(8, 16)
		Me.chkLines.Name = "chkLines"
		Me.chkLines.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.chkLines.Size = New System.Drawing.Size(57, 17)
		Me.chkLines.TabIndex = 13
		Me.chkLines.TabStop = True
		Me.chkLines.Text = "Lines"
		Me.chkLines.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.chkLines.Visible = True
		' 
		'Frame1
		' 
		Me.Frame1.AllowDrop = True
		Me.Frame1.BackColor = System.Drawing.SystemColors.Control
		Me.Frame1.Controls.Add(Me.comboScale)
		Me.Frame1.Enabled = True
		Me.Frame1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Frame1.Location = New System.Drawing.Point(0, 336)
		Me.Frame1.Name = "Frame1"
		Me.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame1.Size = New System.Drawing.Size(153, 41)
		Me.Frame1.TabIndex = 11
		Me.Frame1.Text = "Scale Window"
		Me.Frame1.Visible = True
		' 
		'comboScale
		' 
		Me.comboScale.AllowDrop = True
		Me.comboScale.BackColor = System.Drawing.SystemColors.Window
		Me.comboScale.CausesValidation = True
		Me.comboScale.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.comboScale.Enabled = True
		Me.comboScale.ForeColor = System.Drawing.SystemColors.WindowText
		Me.comboScale.IntegralHeight = True
		Me.comboScale.Location = New System.Drawing.Point(32, 16)
		Me.comboScale.Name = "comboScale"
		Me.comboScale.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.comboScale.Size = New System.Drawing.Size(73, 21)
		Me.comboScale.Sorted = False
		Me.comboScale.TabIndex = 27
		Me.comboScale.TabStop = True
		Me.comboScale.Visible = True
		Me.comboScale.Items.AddRange(New Object() {"x 1", "x 2", "x 5", "x 10"})
		' 
		'StartStop
		' 
		Me.StartStop.AllowDrop = True
		Me.StartStop.BackColor = System.Drawing.SystemColors.Control
		Me.StartStop.ForeColor = System.Drawing.SystemColors.ControlText
		Me.StartStop.Location = New System.Drawing.Point(365, 80)
		Me.StartStop.Name = "StartStop"
		Me.StartStop.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.StartStop.Size = New System.Drawing.Size(66, 28)
		Me.StartStop.TabIndex = 10
		Me.StartStop.Text = "Start"
		Me.StartStop.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
		Me.StartStop.UseVisualStyleBackColor = False
		' 
		'Copy
		' 
		Me.Copy.AllowDrop = True
		Me.Copy.BackColor = System.Drawing.SystemColors.Control
		Me.Copy.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Copy.Location = New System.Drawing.Point(365, 120)
		Me.Copy.Name = "Copy"
		Me.Copy.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Copy.Size = New System.Drawing.Size(66, 28)
		Me.Copy.TabIndex = 9
		Me.Copy.Text = "Copy"
		Me.Copy.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
		Me.Copy.UseVisualStyleBackColor = False
		' 
		'Save
		' 
		Me.Save.AllowDrop = True
		Me.Save.BackColor = System.Drawing.SystemColors.Control
		Me.Save.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Save.Location = New System.Drawing.Point(365, 158)
		Me.Save.Name = "Save"
		Me.Save.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Save.Size = New System.Drawing.Size(66, 28)
		Me.Save.TabIndex = 8
		Me.Save.Text = "Save"
		Me.Save.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
		Me.Save.UseVisualStyleBackColor = False
		' 
		'Print
		' 
		Me.Print.AllowDrop = True
		Me.Print.BackColor = System.Drawing.SystemColors.Control
		Me.Print.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Print.Location = New System.Drawing.Point(365, 197)
		Me.Print.Name = "Print"
		Me.Print.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Print.Size = New System.Drawing.Size(66, 28)
		Me.Print.TabIndex = 7
		Me.Print.Text = "Print"
		Me.Print.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
		Me.Print.UseVisualStyleBackColor = False
		' 
		'Zero
		' 
		Me.Zero.AllowDrop = True
		Me.Zero.BackColor = System.Drawing.SystemColors.Control
		Me.Zero.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Zero.Location = New System.Drawing.Point(365, 235)
		Me.Zero.Name = "Zero"
		Me.Zero.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Zero.Size = New System.Drawing.Size(66, 28)
		Me.Zero.TabIndex = 6
		Me.Zero.Text = "Zero"
		Me.Zero.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
		Me.Zero.UseVisualStyleBackColor = False
		' 
		'VScroll1
		' 
		Me.VScroll1.AllowDrop = True
		Me.VScroll1.CausesValidation = True
		Me.VScroll1.Enabled = True
		Me.VScroll1.LargeChange = 16
		Me.VScroll1.Location = New System.Drawing.Point(336, 32)
		Me.VScroll1.Maximum = 142
		Me.VScroll1.Minimum = -128
		Me.VScroll1.Name = "VScroll1"
		Me.VScroll1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.VScroll1.Size = New System.Drawing.Size(18, 273)
		Me.VScroll1.SmallChange = 1
		Me.VScroll1.TabIndex = 1
		Me.VScroll1.TabStop = True
		Me.VScroll1.Value = 0
		Me.VScroll1.Visible = True
		' 
		'HScroll1
		' 
		Me.HScroll1.AllowDrop = True
		Me.HScroll1.CausesValidation = True
		Me.HScroll1.Enabled = True
		Me.HScroll1.LargeChange = 128
		Me.HScroll1.Location = New System.Drawing.Point(16, 304)
		Me.HScroll1.Maximum = 2174
		Me.HScroll1.Minimum = 0
		Me.HScroll1.Name = "HScroll1"
		Me.HScroll1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.HScroll1.Size = New System.Drawing.Size(323, 20)
		Me.HScroll1.SmallChange = 1
		Me.HScroll1.TabIndex = 0
		Me.HScroll1.TabStop = True
		Me.HScroll1.Value = 0
		Me.HScroll1.Visible = True
		' 
		'Timer1
		' 
		Me.Timer1.Enabled = False
		Me.Timer1.Interval = 1
		' 
		'RtChart1
		' 
		Me.RtChart1.AutoSize = 0
		Me.RtChart1.AxesColor = 16776960
		Me.RtChart1.AxesTics = 0
		Me.RtChart1.AxesType = 0
		Me.RtChart1.BackColor = 8421376
		Me.RtChart1.BevelInner = 0
		Me.RtChart1.BevelOuter = 2
		Me.RtChart1.BevelWidth_Inner = 1
		Me.RtChart1.BevelWidth_Outer = 1
		Me.RtChart1.BorderColor = (-2147483633)
		Me.RtChart1.BorderWidth = 3
		Me.RtChart1.Chn_Select = 1
		Me.RtChart1.ChnData_Dimension = 1
		Me.RtChart1.ChnDspUserData = 0
		Me.RtChart1.ChnXform_Select = 1
		Me.RtChart1.ColorDepth = 0
		Me.RtChart1.ErrBase = 30200
		Me.RtChart1.Font = New System.Drawing.Font("Arial", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.RtChart1.FrameColor = 16776960
		Me.RtChart1.FrameOn = -1
		Me.RtChart1.FramesPerSec = 0
		Me.RtChart1.FrameTics = 0
		Me.RtChart1.GridColor = 8421504
		Me.RtChart1.GridOn = -1
		Me.RtChart1.GridSymmetry = 0
		Me.RtChart1.GridType = 0
		Me.RtChart1.HitTest = 0
		Me.RtChart1.ImageFile = String.Empty
		Me.RtChart1.LightColor = (-2147483634)
		Me.RtChart1.Location = New System.Drawing.Point(16, 32)
		Me.RtChart1.MajorDivHorz = 10
		Me.RtChart1.MajorDivVert = 8
		Me.RtChart1.MarginBottom = 20
		Me.RtChart1.MarginLeft = 20
		Me.RtChart1.MarginRight = 20
		Me.RtChart1.MarginTop = 20
		Me.RtChart1.MinorDiv = 5
		Me.RtChart1.MouseIcon = 0
		Me.RtChart1.Name = "RtChart1"
		Me.RtChart1.Outline = -1
		Me.RtChart1.OutlineColor = (-2147483642)
		Me.RtChart1.Pen_Select = 1
		Me.RtChart1.RoundedCorners = 0
		Me.RtChart1.Scale_Select = 1
		Me.RtChart1.ShadowColor = (-2147483632)
		Me.RtChart1.Size = New System.Drawing.Size(321, 273)
		Me.RtChart1.TabIndex = 25
		Me.RtChart1.Viewport_Select = 1
		Me.RtChart1.Viewports = 1
		Me.RtChart1.ViewportStorageColor = 12632256
		Me.RtChart1.ViewportStorageOn = 0
		Me.RtChart1.Wnd_Select = 1
		' 
		'Label1
		' 
		Me.Label1.AllowDrop = True
		Me.Label1.BackColor = System.Drawing.Color.Silver
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label1.Location = New System.Drawing.Point(344, 336)
		Me.Label1.Name = "Label1"
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.Size = New System.Drawing.Size(112, 28)
		Me.Label1.TabIndex = 5
		Me.Label1.Text = "Drag screen traces with mouse."
		' 
		'SysItems
		' 
		Me.SysItems.AllowDrop = True
		Me.SysItems.AutoSize = True
		Me.SysItems.BackColor = System.Drawing.SystemColors.Window
		Me.SysItems.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.SysItems.ForeColor = System.Drawing.SystemColors.WindowText
		Me.SysItems.Location = New System.Drawing.Point(380, 293)
		Me.SysItems.Name = "SysItems"
		Me.SysItems.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.SysItems.Size = New System.Drawing.Size(39, 13)
		Me.SysItems.TabIndex = 4
		Me.SysItems.Text = "Label2"
		Me.SysItems.Visible = False
		' 
		'Formats
		' 
		Me.Formats.AllowDrop = True
		Me.Formats.AutoSize = True
		Me.Formats.BackColor = System.Drawing.SystemColors.Window
		Me.Formats.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Formats.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Formats.Location = New System.Drawing.Point(392, 278)
		Me.Formats.Name = "Formats"
		Me.Formats.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Formats.Size = New System.Drawing.Size(21, 13)
		Me.Formats.TabIndex = 3
		Me.Formats.Text = "DIB"
		Me.Formats.Visible = False
		' 
		'Topics
		' 
		Me.Topics.AllowDrop = True
		Me.Topics.AutoSize = True
		Me.Topics.BackColor = System.Drawing.SystemColors.Window
		Me.Topics.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Topics.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Topics.Location = New System.Drawing.Point(375, 264)
		Me.Topics.Name = "Topics"
		Me.Topics.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Topics.Size = New System.Drawing.Size(42, 13)
		Me.Topics.TabIndex = 2
		Me.Topics.Text = "System"
		Me.Topics.Visible = False
		' 
		'formDDEServer
		' 
		Me.AllowDrop = True
		Me.AutoScaleDimensions = New System.Drawing.SizeF(7, 13)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.Color.Silver
		Me.ClientSize = New System.Drawing.Size(464, 439)
		Me.Controls.Add(Me.Frame5)
		Me.Controls.Add(Me.Frame4)
		Me.Controls.Add(Me.Frame3)
		Me.Controls.Add(Me.Frame2)
		Me.Controls.Add(Me.Frame1)
		Me.Controls.Add(Me.StartStop)
		Me.Controls.Add(Me.Copy)
		Me.Controls.Add(Me.Save)
		Me.Controls.Add(Me.Print)
		Me.Controls.Add(Me.Zero)
		Me.Controls.Add(Me.VScroll1)
		Me.Controls.Add(Me.HScroll1)
		Me.Controls.Add(Me.RtChart1)
		Me.Controls.Add(Me.Label1)
		Me.Controls.Add(Me.SysItems)
		Me.Controls.Add(Me.Formats)
		Me.Controls.Add(Me.Topics)
		Me.Controls.Add(MainMenu1)
		Me.Font = New System.Drawing.Font("Arial", 8.4!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.ForeColor = System.Drawing.SystemColors.WindowText
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
		Me.Icon = CType(resources.GetObject("formDDEServer.Icon"), System.Drawing.Icon)
		Me.Location = New System.Drawing.Point(104, 257)
		Me.Location = New System.Drawing.Point(100, 224)
		Me.MaximizeBox = False
		Me.MinimizeBox = True
		Me.Name = "formDDEServer"
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Size = New System.Drawing.Size(470, 452)
		Me.Text = "LabOBJX Real-Time Chart - DDE Server Example"
		Me.listBoxComboBoxHelper1.SetItemData(Me.comboScreens, New Integer() {1, 2, 4})
		Me.listBoxComboBoxHelper1.SetItemData(Me.comboScale, New Integer() {1, 2, 5, 10})
		Me.listBoxComboBoxHelper1.EndInit()
		Me.Frame5.ResumeLayout(False)
		Me.Frame4.ResumeLayout(False)
		Me.Frame3.ResumeLayout(False)
		Me.Frame2.ResumeLayout(False)
		Me.Frame1.ResumeLayout(False)
		Me.ResumeLayout(False)
	End Sub
	Sub ReLoadForm(ByVal addEvents As Boolean)
		InitializemnuHelpItem()
		InitializemnuFileItem()
		InitializemnuEditItem()
		InitializeColorDepth()
		Form_Load()
		If addEvents Then
			AddHandler MyBase.Closed, AddressOf Me.Form_Closed
		End If
	End Sub
	Sub InitializemnuHelpItem()
		ReDim mnuHelpItem(0)
		Me.mnuHelpItem(0) = _mnuHelpItem_0
	End Sub
	Sub InitializemnuFileItem()
		ReDim mnuFileItem(6)
		Me.mnuFileItem(0) = _mnuFileItem_0
		Me.mnuFileItem(1) = _mnuFileItem_1
		Me.mnuFileItem(2) = _mnuFileItem_2
		Me.mnuFileItem(3) = _mnuFileItem_3
		Me.mnuFileItem(4) = _mnuFileItem_4
		Me.mnuFileItem(5) = _mnuFileItem_5
		Me.mnuFileItem(6) = _mnuFileItem_6
	End Sub
	Sub InitializemnuEditItem()
		ReDim mnuEditItem(0)
		Me.mnuEditItem(0) = _mnuEditItem_0
	End Sub
	Sub InitializeColorDepth()
		ReDim ColorDepth(1)
		Me.ColorDepth(1) = _ColorDepth_1
		Me.ColorDepth(0) = _ColorDepth_0
	End Sub
#End Region
End Class