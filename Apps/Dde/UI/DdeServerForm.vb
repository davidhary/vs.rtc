Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic
Imports System
Imports System.Drawing
Imports System.Windows.Forms
Imports isr.Visuals.RealTimeChart
Partial Friend Class DdeServerForm
    Inherits System.Windows.Forms.Form

    ' Real-Time Chart Control Demo
    ' (C) Copyright 1995, Scientific Software Tools, Inc.
    ' All Rights Reserved.
    '


    Const BufLen As Integer = 2048
    Const SampleSet As Integer = BufLen / 2
    Const JitterRange As Integer = 100
    Const DataLen As Integer = BufLen + JitterRange
    Const NCursors As Integer = 4 ' number of cursor channels
    Const Cursor1 As Integer = 5 ' first cursor channel

    Dim values(DataLen) As Single
    Dim jitter(1024) As Integer
    Dim ofs As Integer ' next "jitter" index
    Dim lastJitter As Integer ' last "jitter" value
    Dim LineCoord(8) As Single ' (x, y)-coordinate pairs
    Dim LastChn As Integer ' currently selected channel
    Dim MouseX As Single ' x-mouse coord of last channel selected
    Dim MouseY As Single ' y-mouse coord of last channel selected

    Dim CurrentCursor As Integer

    Private isInitializingComponent As Boolean
    Public Sub New()
        MyBase.New()
        If _Instance Is Nothing Then
            If _InitializingInstance Then
                _Instance = Me
            Else
                Try
                    'For the start-up form, the first instance created is the default instance.
                    If Not (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint Is Nothing) AndAlso System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType Is Me.GetType() Then
                        _Instance = Me
                    End If

                Catch
                End Try
            End If
        End If
        'This call is required by the Windows Form Designer.
        isInitializingComponent = True
        InitializeComponent()
        isInitializingComponent = False
        If Not Me.DesignMode Then ReLoadForm(False)
    End Sub


    Private Sub chkAxes_CheckStateChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles chkAxes.CheckStateChanged
        ' Enable/disable display axes
        If chkAxes.CheckState Then
                RtChart1.AxesType = AxesType.HorizontalVertical
        Else
                RtChart1.AxesType = AxesType.NoAxes
        End If
    End Sub

    Private Sub chkFrame_CheckStateChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles chkFrame.CheckStateChanged
        If isInitializingComponent Then Return
        ' Enable/disable display frame

        RtChart1.FrameOn = (chkFrame.CheckState)
    End Sub

    Private Sub chkGrid_CheckStateChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles chkGrid.CheckStateChanged
        ' Enable/disable display grid
        RtChart1.GridOn = (chkGrid.CheckState)
    End Sub

    Private Sub chkIntense_CheckStateChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles chkIntense.CheckStateChanged
        If isInitializingComponent Then Return
        Dim RtChart_ChnChangePen, RtChart_ChnIntenseColor, RtChart_ChnNormalColor, RtChart_DashDotPen As Object
        ' Switch between normal and intense color

        RtChart1.LogicalChannel = (1)

        ' Specify a drawing pen
        ReflectionHelper.LetMember(RtChart1, "Pen_Select", 2)
        ReflectionHelper.LetMember(RtChart1, "PenColor", ColorTranslator.ToOle(Color.FromArgb(255, 255, 255)))
        ReflectionHelper.LetMember(RtChart1, "PenIntenseColor", ColorTranslator.ToOle(Color.FromArgb(255, 255, 0)))
        'UPGRADE_WARNING: (1068) RtChart_DashDotPen of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
        ReflectionHelper.LetMember(RtChart1, "PenStyle", ReflectionHelper.GetPrimitiveValue(RtChart_DashDotPen))
        ReflectionHelper.LetMember(RtChart1, "PenWidth", 0) ' force to 1 pixel
        If chkIntense.CheckState Then
            ReflectionHelper.LetMember(RtChart1, "ChnDspPen", 2)
        Else
            ReflectionHelper.LetMember(RtChart1, "ChnDspPen", 1)
        End If
        'UPGRADE_WARNING: (1068) RtChart_ChnChangePen of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
        RtChart1.ChnDspAction = ((ChannelDisplayAction.ChangePen))
        Exit Sub

        If chkIntense.CheckState Then
            'UPGRADE_WARNING: (1068) RtChart_ChnIntenseColor of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
            RtChart1.ChnDspAction = ((ChannelDisplayAction.IntenseColor))
        Else
            'UPGRADE_WARNING: (1068) RtChart_ChnNormalColor of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
            RtChart1.ChnDspAction = ((ChannelDisplayAction.NormalColor))
        End If
    End Sub

    Private Sub chkLines_CheckStateChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles chkLines.CheckStateChanged
        If isInitializingComponent Then Return
        Dim RtChart_Lines, RtChart_Points As Object
        ' Switch between drawing points and lines


        For i As Integer = 1 To RtChart1.Viewports
            RtChart1.LogicalChannel = (i)
            If chkLines.CheckState Then
                    RtChart1.ChnDspStyle = ChannelDisplayStyle.Lines
            Else
                'UPGRADE_WARNING: (1068) RtChart_Points of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
                RtChart1.ChnDspStyle = ChannelDisplayStyle.Points
            End If
        Next i
    End Sub

    Private Sub chkStorage_CheckStateChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles chkStorage.CheckStateChanged
        If isInitializingComponent Then Return
        ' Enable/disable analog storage mode

        For i As Integer = 1 To RtChart1.Viewports
            RtChart1.LogicalViewport = (i)
            RtChart1.ViewportStorageOn = (chkStorage.CheckState)
        Next i
    End Sub

    Private Sub cmdErase_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles cmdErase.Click
        Dim RtChart_ChnErase As Object
        ' Erase channel data


        For i As Integer = 1 To RtChart1.Viewports
            RtChart1.LogicalChannel = (i)
            'UPGRADE_WARNING: (1068) RtChart_ChnErase of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
            RtChart1.ChnDspAction = ((ChannelDisplayAction.Erase))
        Next i
    End Sub

    Private Sub ColorDepth_CheckedChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _ColorDepth_1.CheckedChanged, _ColorDepth_0.CheckedChanged
        Dim Index As Integer = Array.IndexOf(Me.ColorDepth, eventSender)
        If eventSender.Checked Then
            If isInitializingComponent Then
                Exit Sub
            End If
            Dim RtChart_Color256, RtChart_ColorDefault As Object
            Select Case Index
                Case 0
                    If ColorDepth(Index).Checked Then
                        'UPGRADE_WARNING: (1068) RtChart_ColorDefault of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
                        ReflectionHelper.LetMember(RtChart1, "ColorDepth", ReflectionHelper.GetPrimitiveValue(RtChart_ColorDefault))
                    End If
                Case 1
                    If ColorDepth(Index).Checked Then
                        'UPGRADE_WARNING: (1068) RtChart_Color256 of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
                        ReflectionHelper.LetMember(RtChart1, "ColorDepth", ReflectionHelper.GetPrimitiveValue(RtChart_Color256))
                    End If
                Case Else
                    'UPGRADE_WARNING: (1068) RtChart_ColorDefault of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
                    ReflectionHelper.LetMember(RtChart1, "ColorDepth", ReflectionHelper.GetPrimitiveValue(RtChart_ColorDefault))
            End Select
        End If
    End Sub

    Private Sub comboScale_SelectedIndexChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles comboScale.SelectedIndexChanged
        Dim RtChart_ChnChangeWindow As Object
        Dim factor As Single
        Dim Index As Integer

        With comboScale
            Index = .SelectedIndex
            factor = .GetItemData(Index)
        End With

        Static initialized As Integer
        If Not initialized Then
            initialized = True
            For i As Integer = 1 To 4
                ReflectionHelper.LetMember(RtChart1, "Wnd_Select", i)
                Select Case i
                    Case 1
                        factor = 1
                    Case 2
                        factor = 2
                    Case 3
                        factor = 5
                    Case Else
                        factor = 10
                End Select
                ReflectionHelper.LetMember(RtChart1, "WndXmin", 0)
                ReflectionHelper.LetMember(RtChart1, "WndWidth", BufLen * 0.1 * factor)
                ReflectionHelper.LetMember(RtChart1, "WndYmin", -factor)
                ReflectionHelper.LetMember(RtChart1, "WndHeight", 2 * factor)
            Next i
        End If

        RtChart1.LogicalChannel = (1)
        ReflectionHelper.LetMember(RtChart1, "ChnDspWindow", Index + 1)
        'UPGRADE_WARNING: (1068) RtChart_ChnChangeWindow of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
        RtChart1.ChnDspAction = ((ChannelDisplayAction.ChangeWindow))

    End Sub

    Private Sub comboScreens_SelectedIndexChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles comboScreens.SelectedIndexChanged
        Dim RtChart_ChnInitialize As Object
        Dim Index, result As Integer

        With comboScreens
            Index = .GetItemData(.SelectedIndex)
        End With

        With RtChart1
            ' Initialize channel specification
            ReflectionHelper.LetMember(RtChart1, "Viewports", Index)
            For i As Integer = 1 To Index
                RtChart1.LogicalChannel = (i)
                    RtChart1.ChnDspAction = ((ChannelDisplayAction.Initialize))

                lastJitter = jitter(ofs)
                If ofs >= 1023 Then ofs = 0 Else ofs += 1
                result = RtChart1.ChartData(i, SampleSet, values, lastJitter, 0, 0)
                result = RtChart1.ChartData(i, SampleSet, values, SampleSet + lastJitter, 0, 0)
            Next i
        End With
    End Sub

    Private Sub Copy_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles Copy.Click
        Dim RtChart_AppendToClipboard As Object
        ' Copy Real-Time Chart data to the clipboard

        Clipboard.Clear()

        ' Add paste link item to clipboard for DDE data transfer
        Const CF_LINK As Integer = &HBF00S
        'UPGRADE_ISSUE: (2064) Form property formDDEServer.LinkTopic was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2064
        'UPGRADE_WARNING: (2065) App property App.EXEName has a new behavior. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2065
        Dim AppTopicName As String = My.Application.Info.AssemblyName & "|" & Me.getLinkTopic() & "!" & "RtChart1"
        'UPGRADE_ISSUE: (2064) Clipboard method Global.Clipboard was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2064
        'UPGRADE_ISSUE: (2064) Clipboard method Clipboard.SetText was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2064
        UpgradeSolution1Support.UpgradeStubs.VB_Clipboard.SetText(AppTopicName, CF_LINK)

        ' Add bitmap item to clipboard for ordinary paste operation
        'UPGRADE_WARNING: (1068) RtChart_AppendToClipboard of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
        RtChart1.ChartAction = ((ChartAction.AppendToClipboard))

        ' Alternatively, you can copy just the bitmap data to the clipboard using
        ' RtChart1.Action = RtChart_CopyToClipboard
    End Sub

    Private Sub DrawCursor(ByVal Chn As Integer, ByVal X1 As Single, ByVal Y1 As Single, ByVal X2 As Single, ByVal Y2 As Single)
        ' Draw a cursor

        LineCoord(0) = X1
        LineCoord(1) = Y1
        LineCoord(2) = X2
        LineCoord(3) = Y2

        Dim result As Integer = RtChart1.ChartData(Chn, 2, LineCoord, 0, 0, 0)

    End Sub

    'UPGRADE_NOTE: (7001) The following declaration (EraseChn_Click) seems to be dead code More Information: https://www.mobilize.net/vbtonet/ewis/ewi7001
    'Private Sub EraseChn_Click()
    'Dim RtChart_ChnErase As Object
    'RtChart1.LogicalChannel = (1)
    ''UPGRADE_WARNING: (1068) RtChart_ChnErase of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
    'RtChart1.ChnDspAction = ((ChannelDisplayAction.Erase))
    'End Sub

    'UPGRADE_WARNING: (2050) Form Event Form.LinkExecute was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2050
    Private Sub Form_LinkExecute(ByRef CmdStr As String, ByRef Cancel As Integer)
        If CmdStr = "[Quit]" Then
            Cancel = False
            Me.Close()
        Else
            Cancel = True
        End If
    End Sub

    Private Sub Form_Load()
        Dim RtChart_ChnInitialize As Object

        Dim ret As Integer
        InitRtChart()

        comboScreens.SelectedIndex = 0
        comboScale.SelectedIndex = 0

        ' Make some dummy data
        For i As Integer = 0 To DataLen
            values(i) = Math.Sin(8 * 3.14159 * i / SampleSet)
        Next i

        ' Precompute jitter values
        For i As Integer = 0 To 1024
            jitter(i) = Math.Floor(CDbl(JitterRange * VBMath.Rnd()))
        Next i
        ofs = 0

        ' Enable hit-testing
        ReflectionHelper.LetMember(RtChart1, "HitTest", True)

        ' Setup test app controls
        LastChn = -1
        ReflectionHelper.LetMember(RtChart1, "ImageFile", "RtChart.bmp")

        ' Draw cursor 1
        RtChart1.LogicalChannel = (Cursor1)
        RtChart1.ChnDspAction = ((ChannelDisplayAction.Initialize))
        ReflectionHelper.LetMember(RtChart1, "Wnd_Select", 1)
        DrawCursor(Cursor1, RtChart1.WndXmin, 0.6, ReflectionHelper.GetMember(RtChart1, "WndXmin") + ReflectionHelper.GetMember(RtChart1, "WndWidth"), 0.6)

        ' Load DDE server
        SetupSystemTopic()

        '  LoadCustomCursors

        ' Position window in upper left corner
        Me.SetBounds(0, 0, 0, 0, BoundsSpecified.X Or BoundsSpecified.Y)

    End Sub

    Private Sub Form_Closed(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MyBase.Closed
        '  UnloadCustomCursors
    End Sub

    Private Sub HScroll1_ValueChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles HScroll1.ValueChanged

        ' Scale display horizontally

        ReflectionHelper.LetMember(RtChart1, "WndXmin", ReflectionHelper.GetMember(Of Double)(RtChart1, "WndWidth") * HScroll1.Value / ((HScroll1.Maximum - (HScroll1.LargeChange + 1)) - HScroll1.Minimum))
        RtChart1.ChartAction = ((ChartAction.Rescale))
    End Sub

    Private Sub HScroll1_Scroll_Renamed(ByVal newScrollValue As Integer)
        HScroll1_ValueChanged(HScroll1, New EventArgs())
    End Sub

    Private Sub InitRtChart()
        Dim RtChart_AutoIncr, RtChart_Horz, RtChart_Lines, RtChart_Pair, RtChart_Scalar, RtChart_Single, RtChart_SolidPen, RtChart_Vert As Object


        ' Setup user windows
        For i As Integer = 1 To 4
            ReflectionHelper.LetMember(RtChart1, "Wnd_Select", i)
            ReflectionHelper.LetMember(RtChart1, "WndXmin", 0)
            ReflectionHelper.LetMember(RtChart1, "WndWidth", BufLen * 0.1)
            ReflectionHelper.LetMember(RtChart1, "WndYmin", -1)
            ReflectionHelper.LetMember(RtChart1, "WndHeight", 2)
        Next i

        ' Specify a drawing pen
        ReflectionHelper.LetMember(RtChart1, "Pen_Select", 1)
        ReflectionHelper.LetMember(RtChart1, "PenColor", ColorTranslator.ToOle(Color.FromArgb(0, 255, 255)))
        ReflectionHelper.LetMember(RtChart1, "PenIntenseColor", ColorTranslator.ToOle(Color.FromArgb(255, 255, 0)))
        'UPGRADE_WARNING: (1068) RtChart_SolidPen of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
        ReflectionHelper.LetMember(RtChart1, "PenStyle", ReflectionHelper.GetPrimitiveValue(RtChart_SolidPen))
        ReflectionHelper.LetMember(RtChart1, "PenWidth", 1)

        ' Setup display channel parameters
        For i As Integer = 1 To 4
            RtChart1.LogicalChannel = (i)
            ReflectionHelper.LetMember(RtChart1, "ChnDspWindow", i) ' logical coord range
            ReflectionHelper.LetMember(RtChart1, "ChnDspViewport", i) ' default display location
            ReflectionHelper.LetMember(RtChart1, "ChnDspPen", 1) ' drawing pen
            ReflectionHelper.LetMember(RtChart1, "ChnDspBufLen", BufLen) ' input buffer length
            RtChart1.ChnDspStyle = ChannelDisplayStyle.Lines

            ' Describe horizontal dimension
            ReflectionHelper.LetMember(RtChart1, "ChnData_Dimension", ReflectionHelper.GetPrimitiveValue(RtChart_Horz))
            ReflectionHelper.LetMember(RtChart1, "ChnDataShape", ReflectionHelper.GetPrimitiveValue(RtChart_AutoIncr))
            ReflectionHelper.LetMember(RtChart1, "ChnDataOffset", 0) ' starting point in buffer

            ' Describe vertical dimension
            ReflectionHelper.LetMember(RtChart1, "ChnData_Dimension", ReflectionHelper.GetPrimitiveValue(RtChart_Vert))
            ReflectionHelper.LetMember(RtChart1, "ChnDataShape", ReflectionHelper.GetPrimitiveValue(RtChart_Scalar))
            ReflectionHelper.LetMember(RtChart1, "ChnDataType", ReflectionHelper.GetPrimitiveValue(RtChart_Single))
            ReflectionHelper.LetMember(RtChart1, "ChnDataOffset", 0) ' starting point in buffer
            ReflectionHelper.LetMember(RtChart1, "ChnDataIncr", 1) ' logical increment to next value

        Next i

        ' Setup cursor channel parameters
        For i As Integer = Cursor1 To Cursor1 + NCursors - 1
            RtChart1.LogicalChannel = (i)
            ReflectionHelper.LetMember(RtChart1, "ChnDspWindow", i - 4) ' logical coord range
            ReflectionHelper.LetMember(RtChart1, "ChnDspViewport", i - 4) ' default display location
            ReflectionHelper.LetMember(RtChart1, "ChnDspPen", 1) ' drawing pen
            ReflectionHelper.LetMember(RtChart1, "ChnDspBufLen", 2) ' input buffer length
            RtChart1.ChnDspStyle = ChannelDisplayStyle.Lines

            ' Describe horizontal/vertical dimension
            ReflectionHelper.LetMember(RtChart1, "ChnData_Dimension", ReflectionHelper.GetPrimitiveValue(RtChart_Vert))
            'UPGRADE_WARNING: (1068) RtChart_Pair of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
            ReflectionHelper.LetMember(RtChart1, "ChnDataShape", ReflectionHelper.GetPrimitiveValue(RtChart_Pair))
            ReflectionHelper.LetMember(RtChart1, "ChnDataOffset", 0) ' starting point in buffer
            ReflectionHelper.LetMember(RtChart1, "ChnDataIncr", 1) ' logical increment to next value
            ReflectionHelper.LetMember(RtChart1, "ChnDataType", ReflectionHelper.GetPrimitiveValue(RtChart_Single))
        Next i

    End Sub

    Public Sub mnuEditItem_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _mnuEditItem_0.Click
        Dim Index As Integer = Array.IndexOf(Me.mnuEditItem, eventSender)
        ' Copy data to clipboard

        Const CopyItem As Integer = 0

        Select Case Index
            Case CopyItem
                Copy_Click(Copy, New EventArgs())
        End Select
    End Sub

    Public Sub mnuFileItem_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _mnuFileItem_0.Click, _mnuFileItem_1.Click, _mnuFileItem_2.Click, _mnuFileItem_3.Click, _mnuFileItem_4.Click, _mnuFileItem_5.Click, _mnuFileItem_6.Click
        Dim PrintError As Boolean = False
        Dim Index As Integer = Array.IndexOf(Me.mnuFileItem, eventSender)
        Dim RtChart_WriteToDisk As Object
        ' Process saving and print Real-Time Chart

        Const SaveFile As Integer = 0
        Const SaveFileAs As Integer = 1
        Const PrintDialog As Integer = 3
        Const PrintSetup As Integer = 4
        Const ExitApp As Integer = 6
        Const PD_DISABLEPRINTTOFILE As Integer = &H80000
        Const PD_HIDEPRINTTOFILE As Integer = &H100000
        Const PD_NOPAGENUMS As Integer = &H8
        Const PD_SHOWHELP As Integer = &H800
        Const PD_USEDEVMODECOPIES As Integer = &H40000
        Const PD_PRINTSETUP As Integer = &H40
        Const PD_NOSELECTION As Integer = &H4

        Try
            Select Case Index
                Case SaveFile
                    'UPGRADE_WARNING: (1068) RtChart_WriteToDisk of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
                    RtChart1.ChartAction = ((ChartAction.WriteToDisk))

                Case PrintDialog
                    PrintError = True
                    'UPGRADE_ISSUE: (2064) MSComDlg.CommonDialog property CmDialog1.CancelError was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2064
                    CMDialog1.setCancelError(True)
                    'UPGRADE_ISSUE: (2064) MSComDlg.CommonDialog property CmDialog1.Flags was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2064
                    CMDialog1.setFlags(PD_HIDEPRINTTOFILE Or PD_NOPAGENUMS Or PD_NOSELECTION Or PD_USEDEVMODECOPIES Or PD_SHOWHELP)
                    CMDialog1Print.PrinterSettings.Copies = 1
                    'UPGRADE_ISSUE: (2064) MSComDlg.CommonDialog property CMDialog1.Action was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2064
                    CMDialog1.setAction(5)
                    Print_Click(Print, New EventArgs())

                Case PrintSetup
                    PrintError = True
                    'UPGRADE_ISSUE: (2064) MSComDlg.CommonDialog property CmDialog1.CancelError was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2064
                    CMDialog1.setCancelError(True)
                    'UPGRADE_ISSUE: (2064) MSComDlg.CommonDialog property CmDialog1.Flags was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2064
                    CMDialog1.setFlags(PD_PRINTSETUP Or PD_SHOWHELP)
                    'UPGRADE_ISSUE: (2064) MSComDlg.CommonDialog property CMDialog1.Action was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2064
                    CMDialog1.setAction(5)

                Case ExitApp
                    Me.Close()
            End Select

        Catch excep As Exception
            If Not PrintError Then
                Throw excep
            End If

            If PrintError Then

                Exit Sub
            End If
        End Try
    End Sub

    'UPGRADE_NOTE: (7001) The following declaration (Option1_Click) seems to be dead code More Information: https://www.mobilize.net/vbtonet/ewis/ewi7001
    'Private Sub Option1_Click()
    'End Sub

    'UPGRADE_NOTE: (7001) The following declaration (optScaling_Click) seems to be dead code More Information: https://www.mobilize.net/vbtonet/ewis/ewi7001
    'Private Sub optScaling_Click(ByRef Index As Integer)
    ' Change display scaling factor
    '
    '
    'End Sub

    'UPGRADE_NOTE: (7001) The following declaration (optScreens_Click) seems to be dead code More Information: https://www.mobilize.net/vbtonet/ewis/ewi7001
    'Private Sub optScreens_Click(ByRef Index As Integer)
    ' Change number of display screens
    '
    'Select Case Index
    'Case 0
    'ReflectionHelper.LetMember(RtChart1, "Viewports", 1)
    'Case 1
    'ReflectionHelper.LetMember(RtChart1, "Viewports", 2)
    'Case 2
    'ReflectionHelper.LetMember(RtChart1, "Viewports", 4)
    'End Select
    'End Sub

    Public Sub mnuHelpItem_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _mnuHelpItem_0.Click
        RtChart1.AboutBox()
    End Sub

    Private Sub Print_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles Print.Click
        Dim PRINT_BESTFIT, PRINT_WINDOW As Object
        ' Print the demo form


        Dim result As Integer = RtChart1.VBPrintWindow(Handle.ToInt32(), PRINT_WINDOW, PRINT_BESTFIT, 0, 0, "Real-Time Chart Demo")
        If result <> 0 Then
            System.Windows.Forms.MessageBox.Show("Error printing", Application.ProductName)
        End If

        ' Alternatively, you can print just the RtChart control using
        ' RtChart1.Action = RtChart_Print
    End Sub

    'UPGRADE_NOTE: (7001) The following declaration (RtChart1_MouseDown) seems to be dead code More Information: https://www.mobilize.net/vbtonet/ewis/ewi7001
    'Private Sub RtChart1_MouseDown(ByRef Button As Integer, ByRef Shift As Integer, ByRef X As Single, ByRef Y As Single, ByRef Obj As Integer, ByRef Position As Integer, ByRef ObjType As Integer)
    'Dim RtChart_ChnIntenseColor As Object
    ' Select object if hit
    '
    ' RtChart1.SetFocus()
    '
    'If Obj > 0 And Obj <> LastChn Then
    'RtChart1.LogicalChannel = (Obj) ' select channel that was hit
    ''UPGRADE_WARNING: (1068) RtChart_ChnIntenseColor of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
    'RtChart1.ChnDspAction = ((ChannelDisplayAction.IntenseColor)) ' and change it's color
    'LastChn = Obj ' save selected object
    'ScreenToWindow(Obj, X, Y) ' convert mouse position
    'MouseX = X ' and save location
    'MouseY = Y
    'End If
    'End Sub

    'UPGRADE_NOTE: (7001) The following declaration (RtChart1_MouseMove) seems to be dead code More Information: https://www.mobilize.net/vbtonet/ewis/ewi7001
    'Private Sub RtChart1_MouseMove(ByRef Button As Integer, ByRef Shift As Integer, ByRef X As Single, ByRef Y As Single, ByRef Obj As Integer, ByRef Position As Integer, ByRef ObjType As Integer)
    '
    '
    'If LastChn <= 0 Then 'Exit Sub ' object selected?
    '
    'ScreenToWindow(LastChn, X, Y)
    'Select Case LastChn
    'Case Cursor1
    'RtChart1.ChnDspOffsetY = (Y - 0.6) ' displacement relative to original position
    'Case 1 To 4 ' drag channels 1-4
    'RtChart1.ChnDspOffsetX = (RtChart1.ChnDspOffsetX + X - MouseX)
    'RtChart1.ChnDspOffsetY = (RtChart1.ChnDspOffsetY + Y - MouseY)
    'MouseX = X
    'MouseY = Y
    'Case Else
    'Exit Sub
    'End Select
    'ReflectionHelper.LetMember(RtChart1, "FramesPerSec", -1) ' force synchronous drawing
    ''UPGRADE_WARNING: (1068) RtChart_ChnTranslate of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
    'RtChart1.ChnDspAction = ChannelDisplayAction.Translate ' update channel position
    'ReflectionHelper.LetMember(RtChart1, "FramesPerSec", 0) ' draw when idle
    'End Sub

    'UPGRADE_NOTE: (7001) The following declaration (RtChart1_MouseUp) seems to be dead code More Information: https://www.mobilize.net/vbtonet/ewis/ewi7001
    'Private Sub RtChart1_MouseUp(ByRef Button As Integer, ByRef Shift As Integer, ByRef X As Single, ByRef Y As Single, ByRef Obj As Integer, ByRef Position As Integer, ByRef ObjType As Integer)
    'Dim RtChart_ChnNormalColor As Object
    'If LastChn > 0 Then ' unselect channel
    'RtChart1.LogicalChannel = (LastChn)
    ''UPGRADE_WARNING: (1068) RtChart_ChnNormalColor of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
    'RtChart1.ChnDspAction = ((ChannelDisplayAction.NormalColor))
    'LastChn = -1
    'End If
    'End Sub

    'UPGRADE_NOTE: (7001) The following declaration (RtChart1_Refresh) seems to be dead code More Information: https://www.mobilize.net/vbtonet/ewis/ewi7001
    'Private Sub RtChart1_Refresh(ByRef Chn As Integer, ByRef nSamples As Integer)
    'If Chn >= Cursor1 Then 'Exit Sub
    '
    'Dim result As Integer = RtChart1.ChartData(Chn, SampleSet, values, lastJitter, 0, 0)
    'result = RtChart1.ChartData(Chn, SampleSet, values, SampleSet + lastJitter, 0, 0)
    'End Sub

    Private Sub Save_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles Save.Click
        Dim RtChart_WriteToDisk As Object
        ' Save Real-Time Chart data to a bitmap file (.BMP)

        ' The save file name is RtChart1.ImageFile
        'UPGRADE_WARNING: (1068) RtChart_WriteToDisk of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
        RtChart1.ChartAction = ((ChartAction.WriteToDisk))
    End Sub

    'UPGRADE_NOTE: (7001) The following declaration (Screens_Click) seems to be dead code More Information: https://www.mobilize.net/vbtonet/ewis/ewi7001
    'Private Sub Screens_Click(ByRef Index As Integer)
    'Select Case Index
    'Case 0
    'ReflectionHelper.LetMember(RtChart1, "Viewports", 1)
    'Case 1
    'ReflectionHelper.LetMember(RtChart1, "Viewports", 2)
    'Case 2
    'ReflectionHelper.LetMember(RtChart1, "Viewports", 4)
    'End Select
    'End Sub

    Private Sub ScreenToWindow(ByVal Obj As Integer, ByRef X As Single, ByRef Y As Single)
        ' Convert screen coord in twips to world coordinates for object
        If Obj <= 0 Then Exit Sub

        RtChart1.LogicalChannel = (Obj) ' select channel object
        ReflectionHelper.LetMember(RtChart1, "Wnd_Select", ReflectionHelper.GetMember(RtChart1, "ChnDspWindow")) ' select channel's window
        RtChart1.LogicalViewport = (ReflectionHelper.GetMember(RtChart1, "ChnDspViewport")) ' select channel's viewport
        ReflectionHelper.LetMember(RtChart1, "WndX", X) ' convert twips to window coord
        X = ReflectionHelper.GetMember(Of Single)(RtChart1, "WndX") ' return world coord
        ReflectionHelper.LetMember(RtChart1, "WndY", Y) ' convert twips to window coord
        Y = ReflectionHelper.GetMember(Of Single)(RtChart1, "WndY") ' return world coord
    End Sub

    Private Sub SetupSystemTopic()
        SysItems.Text = "SysItems" & Strings.Chr(9).ToString() & "Topics" & Strings.Chr(9).ToString() & "Formats" & Strings.Chr(9).ToString() & "RtChart1"
    End Sub

    Private Sub StartStop_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles StartStop.Click
        ' Start/stop real-time data simulation

        ' Stop if running
        If Timer1.Enabled Then
            Timer1.Enabled = False
            StartStop.Text = "Start"
            Exit Sub
        End If

        ' Start timer
        Timer1.Interval = 1
        Timer1.Enabled = True
        StartStop.Text = "Stop"
    End Sub

    Private Sub Timer1_Tick(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles Timer1.Tick
        ' Simulate new data on every timer tic

        Dim result As Integer

        For i As Integer = 1 To RtChart1.Viewports
            lastJitter = jitter(ofs)
            If ofs >= 1023 Then ofs = 0 Else ofs += 1
            result = RtChart1.ChartData(i, SampleSet, values, lastJitter, 0, 0)
            result = RtChart1.ChartData(i, SampleSet, values, SampleSet + lastJitter, 0, 0)
        Next i
    End Sub

    Private Sub VScroll1_ValueChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles VScroll1.ValueChanged

        ' Scroll display vertically

        ReflectionHelper.LetMember(RtChart1, "WndYmin", -1 + 2 * ReflectionHelper.GetMember(Of Double)(RtChart1, "WndHeight") * VScroll1.Value / ((VScroll1.Maximum - (VScroll1.LargeChange + 1)) - VScroll1.Minimum))
        RtChart1.ChartAction = ((ChartAction.Rescale))
    End Sub

    'UPGRADE_NOTE: (2010) VScroll1.Scroll was changed from an event to a procedure. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2010
    Private Sub VScroll1_Scroll_Renamed(ByVal newScrollValue As Integer)
        VScroll1_ValueChanged(VScroll1, New EventArgs())
    End Sub

    Private Sub Zero_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles Zero.Click

        ' Reset zero-point on windows controlled by scroll bars
        HScroll1.Value = 0
        VScroll1.Value = 0

        ' Reset offsets for each channel
        For i As Integer = 1 To RtChart1.Viewports
            RtChart1.LogicalChannel = (i)
            RtChart1.ChnDspOffsetX = (0)
            RtChart1.ChnDspOffsetY = (0)
            RtChart1.ChnDspAction = ChannelDisplayAction.Translate
        Next i

        ' Reset cursor offset
        RtChart1.LogicalChannel = (Cursor1)
        RtChart1.ChnDspOffsetY = (0)
        RtChart1.ChnDspAction = ChannelDisplayAction.Translate
    End Sub
    Private Sub HScroll1_Scroll(ByVal eventSender As Object, ByVal eventArgs As ScrollEventArgs) Handles HScroll1.Scroll
        Select Case eventArgs.Type
            Case ScrollEventType.ThumbTrack
                HScroll1_Scroll_Renamed(eventArgs.NewValue)
        End Select
    End Sub
    Private Sub VScroll1_Scroll(ByVal eventSender As Object, ByVal eventArgs As ScrollEventArgs) Handles VScroll1.Scroll
        Select Case eventArgs.Type
            Case ScrollEventType.ThumbTrack
                VScroll1_Scroll_Renamed(eventArgs.NewValue)
        End Select
    End Sub
End Class