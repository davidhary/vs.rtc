<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DdeClientForm
#Region "Upgrade Support "
	Private Shared _Instance As DdeClientForm
	Private Shared _InitializingInstance As Boolean
	Public Shared Property DefInstance() As DdeClientForm
		Get
			If _Instance Is Nothing OrElse _Instance.IsDisposed Then
				_InitializingInstance = True
				_Instance = CreateInstance()
				_InitializingInstance = False
			End If
			Return _Instance
		End Get
		Set(ByVal value As DdeClientForm)
			_Instance = Value
		End Set
	End Property
#End Region
#Region "Windows Form Designer generated code "
	Public Shared Function CreateInstance() As DdeClientForm
		Dim theInstance As New DdeClientForm()
		theInstance.Form_Load()
		Return theInstance
	End Function
	Private visualControls() As String = New String() {"components", "ToolTipMain", "mnuExit", "mnuFile", "mnuPaste", "mnuPasteLink", "mnuEdit", "MainMenu1", "Request", "AutomaticLink", "ManualLink", "Picture1"}
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTipMain As System.Windows.Forms.ToolTip
	Public WithEvents mnuExit As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFile As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuPaste As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuPasteLink As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuEdit As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents MainMenu1 As System.Windows.Forms.MenuStrip
	Public WithEvents Request As System.Windows.Forms.Button
	Public WithEvents AutomaticLink As System.Windows.Forms.RadioButton
	Public WithEvents ManualLink As System.Windows.Forms.RadioButton
	Public WithEvents Picture1 As System.Windows.Forms.PictureBox
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> _
	 Private Sub InitializeComponent()
		Me.components = New System.ComponentModel.Container()
		Dim resources As New System.ComponentModel.ComponentResourceManager(GetType(DdeClientForm))
		Me.ToolTipMain = New System.Windows.Forms.ToolTip(Me.components)
		Me.MainMenu1 = New System.Windows.Forms.MenuStrip()
		Me.mnuFile = New System.Windows.Forms.ToolStripMenuItem()
		Me.mnuExit = New System.Windows.Forms.ToolStripMenuItem()
		Me.mnuEdit = New System.Windows.Forms.ToolStripMenuItem()
		Me.mnuPaste = New System.Windows.Forms.ToolStripMenuItem()
		Me.mnuPasteLink = New System.Windows.Forms.ToolStripMenuItem()
		Me.Request = New System.Windows.Forms.Button()
		Me.AutomaticLink = New System.Windows.Forms.RadioButton()
		Me.ManualLink = New System.Windows.Forms.RadioButton()
		Me.Picture1 = New System.Windows.Forms.PictureBox()
		Me.SuspendLayout()
		' 
		'MainMenu1
		' 
		Me.MainMenu1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFile, Me.mnuEdit})
		' 
		'mnuFile
		' 
		Me.mnuFile.Available = True
		Me.mnuFile.Checked = False
		Me.mnuFile.Enabled = True
		Me.mnuFile.Name = "mnuFile"
		Me.mnuFile.Text = "&File"
		Me.mnuFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuExit})
		' 
		'mnuExit
		' 
		Me.mnuExit.Available = True
		Me.mnuExit.Checked = False
		Me.mnuExit.Enabled = True
		Me.mnuExit.Name = "mnuExit"
		Me.mnuExit.Text = "E&xit"
		' 
		'mnuEdit
		' 
		Me.mnuEdit.Available = True
		Me.mnuEdit.Checked = False
		Me.mnuEdit.Enabled = True
		Me.mnuEdit.Name = "mnuEdit"
		Me.mnuEdit.Text = "&Edit"
		Me.mnuEdit.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuPaste, Me.mnuPasteLink})
		' 
		'mnuPaste
		' 
		Me.mnuPaste.Available = True
		Me.mnuPaste.Checked = False
		Me.mnuPaste.Enabled = True
		Me.mnuPaste.Name = "mnuPaste"
		Me.mnuPaste.Text = "&Paste"
		' 
		'mnuPasteLink
		' 
		Me.mnuPasteLink.Available = True
		Me.mnuPasteLink.Checked = False
		Me.mnuPasteLink.Enabled = True
		Me.mnuPasteLink.Name = "mnuPasteLink"
		Me.mnuPasteLink.Text = "Paste &Link"
		' 
		'Request
		' 
		Me.Request.AllowDrop = True
		Me.Request.BackColor = System.Drawing.SystemColors.Control
		Me.Request.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Request.Location = New System.Drawing.Point(204, 376)
		Me.Request.Name = "Request"
		Me.Request.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Request.Size = New System.Drawing.Size(64, 28)
		Me.Request.TabIndex = 3
		Me.Request.Text = "Request"
		Me.Request.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
		Me.Request.UseVisualStyleBackColor = False
		' 
		'AutomaticLink
		' 
		Me.AutomaticLink.AllowDrop = True
		Me.AutomaticLink.Appearance = System.Windows.Forms.Appearance.Normal
		Me.AutomaticLink.BackColor = System.Drawing.SystemColors.Window
		Me.AutomaticLink.CausesValidation = True
		Me.AutomaticLink.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.AutomaticLink.Checked = False
		Me.AutomaticLink.Enabled = True
		Me.AutomaticLink.ForeColor = System.Drawing.SystemColors.WindowText
		Me.AutomaticLink.Location = New System.Drawing.Point(96, 374)
		Me.AutomaticLink.Name = "AutomaticLink"
		Me.AutomaticLink.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.AutomaticLink.Size = New System.Drawing.Size(102, 28)
		Me.AutomaticLink.TabIndex = 2
		Me.AutomaticLink.TabStop = True
		Me.AutomaticLink.Text = "Automatic Link"
		Me.AutomaticLink.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.AutomaticLink.Visible = True
		' 
		'ManualLink
		' 
		Me.ManualLink.AllowDrop = True
		Me.ManualLink.Appearance = System.Windows.Forms.Appearance.Normal
		Me.ManualLink.BackColor = System.Drawing.SystemColors.Window
		Me.ManualLink.CausesValidation = True
		Me.ManualLink.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.ManualLink.Checked = False
		Me.ManualLink.Enabled = True
		Me.ManualLink.ForeColor = System.Drawing.SystemColors.WindowText
		Me.ManualLink.Location = New System.Drawing.Point(5, 374)
		Me.ManualLink.Name = "ManualLink"
		Me.ManualLink.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ManualLink.Size = New System.Drawing.Size(88, 28)
		Me.ManualLink.TabIndex = 1
		Me.ManualLink.TabStop = True
		Me.ManualLink.Text = "Manual Link"
		Me.ManualLink.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.ManualLink.Visible = True
		' 
		'Picture1
		' 
		Me.Picture1.AllowDrop = True
		Me.Picture1.BackColor = System.Drawing.SystemColors.Window
		Me.Picture1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Picture1.CausesValidation = True
		Me.Picture1.Dock = System.Windows.Forms.DockStyle.None
		Me.Picture1.Enabled = True
		Me.Picture1.Location = New System.Drawing.Point(5, 29)
		Me.Picture1.Name = "Picture1"
		Me.Picture1.Size = New System.Drawing.Size(20, 20)
		Me.Picture1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
		Me.Picture1.TabIndex = 0
		Me.Picture1.TabStop = True
		Me.Picture1.Visible = True
		' 
		'Form1
		' 
		Me.AllowDrop = True
		Me.AutoScaleDimensions = New System.Drawing.SizeF(7, 13)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Window
		Me.ClientSize = New System.Drawing.Size(428, 410)
		Me.Controls.Add(Me.Request)
		Me.Controls.Add(Me.AutomaticLink)
		Me.Controls.Add(Me.ManualLink)
		Me.Controls.Add(Me.Picture1)
		Me.Controls.Add(MainMenu1)
		Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.ForeColor = System.Drawing.SystemColors.WindowText
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
		Me.Icon = CType(resources.GetObject("Form1.Icon"), System.Drawing.Icon)
		Me.Location = New System.Drawing.Point(111, 150)
		Me.Location = New System.Drawing.Point(107, 108)
		Me.MaximizeBox = True
		Me.MinimizeBox = True
		Me.Name = "Form1"
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Size = New System.Drawing.Size(436, 432)
		Me.Text = "LabOBJX Real-Time Chart - DDE Client Example"
		Me.ResumeLayout(False)
	End Sub
	Sub ReLoadForm(ByVal addEvents As Boolean)
		Form_Load()
		If addEvents Then
			AddHandler MyBase.Closed, AddressOf Me.Form_Closed
		End If
	End Sub
#End Region
End Class