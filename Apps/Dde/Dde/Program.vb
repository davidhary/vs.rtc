' LabOBJX Real-Time Chart - Example Code
' (C) Copyright 1995, Scientific Software Tools, Inc.
' All Rights Reserved.


Option Strict Off
Option Explicit On
Imports System
Imports System.Windows.Forms
Imports isr.Visuals.RealTimeChart

''' <summary> A program. </summary>
''' <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Module Program

    Public Const AssemblyTitle As String = "Real Time Strip Chart DDE Client"
    Public Const AssemblyDescription As String = "Real Time Strip DDE Client"
    Public Const AssemblyProduct As String = "Visuals.Real.Time.Chart.Dde.Client"


End Module