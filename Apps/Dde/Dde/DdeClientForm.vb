Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic
Imports System
Imports System.Diagnostics
Imports System.Drawing
Imports System.IO
Imports System.Windows.Forms
Imports isr.Visuals.RealTimeChart
Partial Friend Class DdeClientForm
    Inherits System.Windows.Forms.Form
    ' LabOBJX Real-Time Chart - Example Code
    ' (C) Copyright 1995, Scientific Software Tools, Inc.
    ' All Rights Reserved.
    '

    'UPGRADE_ISSUE: (2068) LinkModeConstants object was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2068
    'UPGRADE_ISSUE: (2070) Constant vbLinkNone was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2070
    Shared ReadOnly LinkNone As UpgradeSolution1Support.UpgradeStubs.VBRUN_LinkModeConstantsEnum = UpgradeSolution1Support.UpgradeStubs.VBRUN_LinkModeConstants.getvbLinkNone()
    'UPGRADE_ISSUE: (2068) LinkModeConstants object was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2068
    'UPGRADE_ISSUE: (2070) Constant vbLinkSource was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2070
    Shared ReadOnly LinkAutomatic As UpgradeSolution1Support.UpgradeStubs.VBRUN_LinkModeConstantsEnum = UpgradeSolution1Support.UpgradeStubs.VBRUN_LinkModeConstants.getvbLinkSource()
    'UPGRADE_ISSUE: (2068) LinkModeConstants object was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2068
    'UPGRADE_ISSUE: (2070) Constant vbLinkManual was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2070
    Shared ReadOnly LinkManual As UpgradeSolution1Support.UpgradeStubs.VBRUN_LinkModeConstantsEnum = UpgradeSolution1Support.UpgradeStubs.VBRUN_LinkModeConstants.getvbLinkManual()

    Const CF_DIB As Integer = 8
    Const CF_BITMAP As Integer = 2
    Const CF_LINK As Integer = &HBF00S

    Private isInitializingComponent As Boolean
    Public Sub New()
        MyBase.New()
        If _Instance Is Nothing Then
            If _InitializingInstance Then
                _Instance = Me
            Else
                Try
                    'For the start-up form, the first instance created is the default instance.
                    If Not (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint Is Nothing) AndAlso System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType Is Me.GetType() Then
                        _Instance = Me
                    End If

                Catch
                End Try
            End If
        End If
        'This call is required by the Windows Form Designer.
        isInitializingComponent = True
        InitializeComponent()
        isInitializingComponent = False
        If Not Me.DesignMode Then ReLoadForm(False)
    End Sub


    Private Sub AutomaticLink_CheckedChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles AutomaticLink.CheckedChanged
        If eventSender.Checked Then
            If isInitializingComponent Then
                Exit Sub
            End If
            Request.Visible = False
            If Not SetServerLinkMode(LinkAutomatic) Then
                System.Windows.Forms.MessageBox.Show("Cannot set DDE server link to automatic mode.", Application.ProductName)
            End If
        End If
    End Sub

    Private Sub Form_Load()
        Directory.SetCurrentDirectory(My.Application.Info.DirectoryPath)

        Request.Visible = False

        If SetServerLinkMode(LinkManual) Then
            ManualLink.Checked = True ' initialize option button
        Else
            System.Windows.Forms.MessageBox.Show("Cannot link to DDE server. Try running Chart.exe before running this example", Application.ProductName)
        End If


        ' Position window in lower, right corner
        Me.SetBounds(Screen.PrimaryScreen.Bounds.Width - Me.Width, Screen.PrimaryScreen.Bounds.Height - Me.Height - Request.Height, 0, 0, BoundsSpecified.X Or BoundsSpecified.Y)

    End Sub

    Private Sub Form_Closed(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MyBase.Closed
        'UPGRADE_ISSUE: (2064) PictureBox property Picture1.LinkMode was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2064
        Picture1.setLinkMode(LinkNone) ' clear DDE link
    End Sub

    Private Function SetServerLinkMode(ByRef mode As Integer) As Boolean
        Dim result As Boolean = False
        Const DDE_NO_APP As ErrObject = 282

        Dim strMsg As String = String.Empty, strTitle As String = String.Empty

        Try

            result = True

            'UPGRADE_ISSUE: (2064) PictureBox property Picture1.LinkTopic was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2064
            Picture1.setLinkTopic("Chart|System") ' link to server
            'UPGRADE_ISSUE: (2064) PictureBox property Picture1.LinkItem was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2064
            Picture1.setLinkItem("RtChart1") ' link to real-time chart

            'UPGRADE_ISSUE: (2064) PictureBox property Picture1.LinkMode was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2064
            Picture1.setLinkMode(LinkNone) ' clear DDE link
            'UPGRADE_ISSUE: (2068) LinkModeConstants object was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2068
            'UPGRADE_ISSUE: (2064) PictureBox property Picture1.LinkMode was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2064
            Picture1.setLinkMode(mode) ' Set DDE link mode

        Catch

            If Information.Err().Number = DDE_NO_APP.Number Then
                LaunchServer()
            Else
                With Information.Err()
                    strMsg = "Error #" & Conversion.Str(.Number) & " from " & .Source & Environment.NewLine & Environment.NewLine & .Description
                End With

                strTitle = "Set Server Link Mode Error"
                System.Windows.Forms.MessageBox.Show(strMsg, strTitle, MessageBoxButtons.OK)

                result = False
            End If

            'UPGRADE_TODO: (1065) Error handling statement (Resume Next) could not be converted. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1065
            UpgradeHelpers.Helpers.NotUpgradedHelper.NotifyNotUpgradedElement("Resume Next Statement")
        End Try

        Return result
    End Function

    Private Function LaunchServer() As Boolean

        Dim result As Boolean = False
        Try

            'UPGRADE_TODO: (7005) parameters (if any) must be set using the Arguments property of ProcessStartInfo More Information: https://www.mobilize.net/vbtonet/ewis/ewi7005
            Dim startInfo As ProcessStartInfo = New ProcessStartInfo("Chart.exe")
            startInfo.WindowStyle = ProcessWindowStyle.Normal
            Dim handle_Renamed As Integer = Process.Start(startInfo).Id ' start source application

            result = handle_Renamed <> 0

        Catch exc As Exception
            NotUpgradedHelper.NotifyNotUpgradedElement("Resume in On-Error-Resume-Next Block")
        End Try

        Return result
    End Function

    Private Sub ManualLink_CheckedChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles ManualLink.CheckedChanged
        If eventSender.Checked Then
            If isInitializingComponent Then
                Exit Sub
            End If
            If SetServerLinkMode(LinkManual) Then
                Request.Visible = True
            Else
                System.Windows.Forms.MessageBox.Show("Cannot set DDE server link to manual mode.", Application.ProductName)
                Request.Visible = False
            End If

        End If
    End Sub

    Public Sub mnuEdit_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles mnuEdit.Click
        mnuPaste.Enabled = False
        mnuPasteLink.Enabled = False
        If Clipboard.ContainsData(DataFormats.Dib) Or Clipboard.ContainsData(DataFormats.Bitmap) Then mnuPaste.Enabled = True
        'UPGRADE_ISSUE: (2068) ClipBoardConstants object was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2068
        'UPGRADE_ISSUE: (2064) Clipboard method Global.Clipboard was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2064
        'UPGRADE_ISSUE: (2064) Clipboard method Clipboard.GetFormat was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2064
        If mnuPaste.Enabled And UpgradeSolution1Support.UpgradeStubs.VB_Clipboard.GetFormat(CF_LINK) Then mnuPasteLink.Enabled = True
    End Sub

    Public Sub mnuExit_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles mnuExit.Click
        Me.Close()
    End Sub

    Public Sub mnuPaste_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles mnuPaste.Click
        If Clipboard.ContainsData(DataFormats.Dib) Or Clipboard.ContainsData(DataFormats.Bitmap) Then
            Picture1.Image = Clipboard.GetData(Nothing)
        End If
    End Sub

    Public Sub mnuPasteLink_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles mnuPasteLink.Click

        'UPGRADE_ISSUE: (2064) Clipboard method Global.Clipboard was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2064
        'UPGRADE_ISSUE: (2064) Clipboard method Clipboard.GetText was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2064
        Dim link As String = UpgradeSolution1Support.UpgradeStubs.VB_Clipboard.GetText(CF_LINK)
        Dim item As Integer = (link.IndexOf("!"c) + 1)
        If item <> 0 Then
            Request.Visible = False
            AutomaticLink.Checked = True
            'UPGRADE_ISSUE: (2064) PictureBox property Picture1.LinkMode was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2064
            Picture1.setLinkMode(LinkNone)
            'UPGRADE_ISSUE: (2064) PictureBox property Picture1.LinkTopic was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2064
            Picture1.setLinkTopic(link.Substring(0, Math.Min(item - 1, link.Length)))
            'UPGRADE_ISSUE: (2064) PictureBox property Picture1.LinkItem was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2064
            Picture1.setLinkItem(link.Substring(item))
            'UPGRADE_ISSUE: (2064) PictureBox property Picture1.LinkMode was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2064
            Picture1.setLinkMode(LinkAutomatic)
        ElseIf (link.IndexOf("|"c) + 1) Then
            Request.Visible = False
            AutomaticLink.Checked = True
            'UPGRADE_ISSUE: (2064) PictureBox property Picture1.LinkMode was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2064
            Picture1.setLinkMode(LinkNone)
            'UPGRADE_ISSUE: (2064) PictureBox property Picture1.LinkTopic was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2064
            Picture1.setLinkTopic(link)
            'UPGRADE_ISSUE: (2064) PictureBox property Picture1.LinkItem was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2064
            Picture1.setLinkItem("")
            'UPGRADE_ISSUE: (2064) PictureBox property Picture1.LinkMode was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2064
            Picture1.setLinkMode(LinkAutomatic)
        End If
    End Sub

    'UPGRADE_WARNING: (2050) PictureBox Event Picture1.LinkClose was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2050
    Private Sub Picture1_LinkClose()
        Request.Visible = False
    End Sub

    'UPGRADE_WARNING: (2050) PictureBox Event Picture1.LinkOpen was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2050
    Private Sub Picture1_LinkOpen(ByRef Cancel As Integer)
        'UPGRADE_ISSUE: (2064) PictureBox property Picture1.LinkMode was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2064
        Request.Visible = Picture1.getLinkMode() = LinkManual
    End Sub

    Private Sub Request_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles Request.Click
        Dim strMsg As String = String.Empty, strTitle As String = String.Empty

        Try

            'UPGRADE_ISSUE: (2064) PictureBox method Picture1.LinkRequest was not upgraded. More Information: https://www.mobilize.net/vbtonet/ewis/ewi2064
            Picture1.LinkRequest()

        Catch

            With Information.Err()
                strMsg = "Error #" & Conversion.Str(.Number) & " from " & .Source & Environment.NewLine & Environment.NewLine & .Description
            End With

            strTitle = "Request DDE Data Error"
            System.Windows.Forms.MessageBox.Show(strMsg, strTitle, MessageBoxButtons.OK)
        End Try

    End Sub
End Class