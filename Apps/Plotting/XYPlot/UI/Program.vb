' file:		UI\Program.vb
' LabOBJX Real-Time Chart - Example Code
' (C) Copyright 1995, Scientific Software Tools, Inc.
' All Rights Reserved.
' summary:	Program class
Option Strict Off
Option Explicit On
Imports System
Imports System.Windows.Forms

Friend Module Program
    Public Const AssemblyTitle As String = "Real Time Chart XY Plot Demo"
    Public Const AssemblyDescription As String = "Real Time Chart XY Plot Demo"
    Public Const AssemblyProduct As String = "Visuals.Real.Time.Chart.XYPlot"
    Public Property IsRunning As Integer
    Public Property DataUpperBound As Integer = 99
    <CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>")>
    Public Ordinate(DataUpperBound) As Double
    <CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>")>
    Public Abscissa(DataUpperBound) As Double
    Public Property PhaseRate As Single
    Public Property Cycles As Single
    Public Property Phase As Single
    Public Property RtChart As isr.Visuals.RealTimeChart.RealTimeChartControl
    Public Property ScottPlot As ScottPlot.ScottPlotUC
    <STAThread>
    Public Sub Main()
        Application.EnableVisualStyles()
        Application.SetCompatibleTextRenderingDefault(False)
        Dim tempLoadForm As XYScottPlotForm = XYScottPlotForm.Instance
        ' Application.Run(DisplayForm.DefInstance)
        XYScottPlotForm.Instance.Show()

        ' Calculate the static, x-axis wave
        Dim twoPi As Double = 2 * Math.PI
        For i As Integer = 0 To Program.DataUpperBound
            Program.Abscissa(i) = Math.Cos(twoPi * i / Program.DataUpperBound)
        Next i

        RtChart.ChnDataType = ChannelDataType.Double

        'Set the initial phase to zero
        Program.Phase = 0

        Dim result As Integer
        Do While Application.OpenForms.Count
            Application.DoEvents()
            If IsRunning Then

                ' Calculate new wave with added phase shift
                For i As Integer = 0 To Program.DataUpperBound
                    Program.Ordinate(i) = Math.Sin(Program.Cycles * twoPi * i / Program.DataUpperBound + Program.Phase)
                Next i

                'Increase the phase shift for next time
                Program.Phase += Program.PhaseRate

                ' Chart the new wave on the real time chart.
                result = Program.RtChart?.ChartData(1, Program.DataUpperBound + 1, Ordinate, 0, Abscissa, 0)

                ' Chart the new wave on the Scott plot
                ScottPlot?.Render(True)

            End If
        Loop
    End Sub
End Module
