<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class XYPlotForm

#Region "Windows Form Designer generated code "
    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Private ToolTipMain As System.Windows.Forms.ToolTip
    Private WithEvents _AboutButton As System.Windows.Forms.Button
    Private WithEvents _StopButton As System.Windows.Forms.Button
    Private WithEvents _CyclesTextBox As System.Windows.Forms.TextBox
    Private WithEvents _RatioTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _CyclesGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _HighSpeedRadioButton As System.Windows.Forms.RadioButton
    Private WithEvents _MediumSpeedRadioButton As System.Windows.Forms.RadioButton
    Private WithEvents _LowSpeedRadioButton As System.Windows.Forms.RadioButton
    Private WithEvents _SpeedGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _StartButton As System.Windows.Forms.Button
    Private WithEvents _RealTimeChart As isr.Visuals.RealTimeChart.RealTimeChartControl
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XYPlotForm))
        Me.ToolTipMain = New System.Windows.Forms.ToolTip(Me.components)
        Me._AboutButton = New System.Windows.Forms.Button()
        Me._StopButton = New System.Windows.Forms.Button()
        Me._CyclesGroupBox = New System.Windows.Forms.GroupBox()
        Me._CyclesTextBox = New System.Windows.Forms.TextBox()
        Me._RatioTextBoxLabel = New System.Windows.Forms.Label()
        Me._SpeedGroupBox = New System.Windows.Forms.GroupBox()
        Me._HighSpeedRadioButton = New System.Windows.Forms.RadioButton()
        Me._MediumSpeedRadioButton = New System.Windows.Forms.RadioButton()
        Me._LowSpeedRadioButton = New System.Windows.Forms.RadioButton()
        Me._StartButton = New System.Windows.Forms.Button()
        Me._RealTimeChart = New isr.Visuals.RealTimeChart.RealTimeChartControl()
        Me._CyclesGroupBox.SuspendLayout()
        Me._SpeedGroupBox.SuspendLayout()
        Me.SuspendLayout()
        '
        '_AboutButton
        '
        Me._AboutButton.AllowDrop = True
        Me._AboutButton.BackColor = System.Drawing.SystemColors.Control
        Me._AboutButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._AboutButton.Location = New System.Drawing.Point(245, 190)
        Me._AboutButton.Name = "_AboutButton"
        Me._AboutButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._AboutButton.Size = New System.Drawing.Size(107, 27)
        Me._AboutButton.TabIndex = 9
        Me._AboutButton.Text = "&About"
        Me._AboutButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me._AboutButton.UseVisualStyleBackColor = False
        '
        '_StopButton
        '
        Me._StopButton.AllowDrop = True
        Me._StopButton.BackColor = System.Drawing.SystemColors.Control
        Me._StopButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._StopButton.Location = New System.Drawing.Point(245, 159)
        Me._StopButton.Name = "_StopButton"
        Me._StopButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._StopButton.Size = New System.Drawing.Size(107, 27)
        Me._StopButton.TabIndex = 8
        Me._StopButton.Text = "S&top"
        Me._StopButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me._StopButton.UseVisualStyleBackColor = False
        '
        '_CyclesGroupBox
        '
        Me._CyclesGroupBox.AllowDrop = True
        Me._CyclesGroupBox.BackColor = System.Drawing.SystemColors.Window
        Me._CyclesGroupBox.Controls.Add(Me._CyclesTextBox)
        Me._CyclesGroupBox.Controls.Add(Me._RatioTextBoxLabel)
        Me._CyclesGroupBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._CyclesGroupBox.Location = New System.Drawing.Point(245, 85)
        Me._CyclesGroupBox.Name = "_CyclesGroupBox"
        Me._CyclesGroupBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._CyclesGroupBox.Size = New System.Drawing.Size(107, 37)
        Me._CyclesGroupBox.TabIndex = 5
        Me._CyclesGroupBox.TabStop = False
        '
        '_CyclesTextBox
        '
        Me._CyclesTextBox.AcceptsReturn = True
        Me._CyclesTextBox.AllowDrop = True
        Me._CyclesTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._CyclesTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._CyclesTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._CyclesTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._CyclesTextBox.Location = New System.Drawing.Point(65, 12)
        Me._CyclesTextBox.MaxLength = 0
        Me._CyclesTextBox.Name = "_CyclesTextBox"
        Me._CyclesTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._CyclesTextBox.Size = New System.Drawing.Size(31, 20)
        Me._CyclesTextBox.TabIndex = 6
        Me._CyclesTextBox.Text = "2"
        '
        '_RatioTextBoxLabel
        '
        Me._RatioTextBoxLabel.AllowDrop = True
        Me._RatioTextBoxLabel.AutoSize = True
        Me._RatioTextBoxLabel.BackColor = System.Drawing.SystemColors.Window
        Me._RatioTextBoxLabel.ForeColor = System.Drawing.SystemColors.WindowText
        Me._RatioTextBoxLabel.Location = New System.Drawing.Point(19, 16)
        Me._RatioTextBoxLabel.Name = "_RatioTextBoxLabel"
        Me._RatioTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._RatioTextBoxLabel.Size = New System.Drawing.Size(41, 13)
        Me._RatioTextBoxLabel.TabIndex = 7
        Me._RatioTextBoxLabel.Text = "Ratio:"
        '
        '_SpeedGroupBox
        '
        Me._SpeedGroupBox.AllowDrop = True
        Me._SpeedGroupBox.BackColor = System.Drawing.SystemColors.Window
        Me._SpeedGroupBox.Controls.Add(Me._HighSpeedRadioButton)
        Me._SpeedGroupBox.Controls.Add(Me._MediumSpeedRadioButton)
        Me._SpeedGroupBox.Controls.Add(Me._LowSpeedRadioButton)
        Me._SpeedGroupBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._SpeedGroupBox.Location = New System.Drawing.Point(245, 1)
        Me._SpeedGroupBox.Name = "_SpeedGroupBox"
        Me._SpeedGroupBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._SpeedGroupBox.Size = New System.Drawing.Size(107, 79)
        Me._SpeedGroupBox.TabIndex = 1
        Me._SpeedGroupBox.TabStop = False
        Me._SpeedGroupBox.Text = "Speed"
        '
        '_HighSpeedRadioButton
        '
        Me._HighSpeedRadioButton.AllowDrop = True
        Me._HighSpeedRadioButton.AutoSize = True
        Me._HighSpeedRadioButton.BackColor = System.Drawing.SystemColors.Window
        Me._HighSpeedRadioButton.ForeColor = System.Drawing.SystemColors.WindowText
        Me._HighSpeedRadioButton.Location = New System.Drawing.Point(17, 57)
        Me._HighSpeedRadioButton.Name = "_HighSpeedRadioButton"
        Me._HighSpeedRadioButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._HighSpeedRadioButton.Size = New System.Drawing.Size(51, 17)
        Me._HighSpeedRadioButton.TabIndex = 4
        Me._HighSpeedRadioButton.TabStop = True
        Me._HighSpeedRadioButton.Text = "High"
        Me._HighSpeedRadioButton.UseVisualStyleBackColor = False
        '
        '_MediumSpeedRadioButton
        '
        Me._MediumSpeedRadioButton.AllowDrop = True
        Me._MediumSpeedRadioButton.AutoSize = True
        Me._MediumSpeedRadioButton.BackColor = System.Drawing.SystemColors.Window
        Me._MediumSpeedRadioButton.ForeColor = System.Drawing.SystemColors.WindowText
        Me._MediumSpeedRadioButton.Location = New System.Drawing.Point(17, 37)
        Me._MediumSpeedRadioButton.Name = "_MediumSpeedRadioButton"
        Me._MediumSpeedRadioButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._MediumSpeedRadioButton.Size = New System.Drawing.Size(68, 17)
        Me._MediumSpeedRadioButton.TabIndex = 3
        Me._MediumSpeedRadioButton.TabStop = True
        Me._MediumSpeedRadioButton.Text = "Medium"
        Me._MediumSpeedRadioButton.UseVisualStyleBackColor = False
        '
        '_LowSpeedRadioButton
        '
        Me._LowSpeedRadioButton.AllowDrop = True
        Me._LowSpeedRadioButton.AutoSize = True
        Me._LowSpeedRadioButton.BackColor = System.Drawing.SystemColors.Window
        Me._LowSpeedRadioButton.ForeColor = System.Drawing.SystemColors.WindowText
        Me._LowSpeedRadioButton.Location = New System.Drawing.Point(17, 17)
        Me._LowSpeedRadioButton.Name = "_LowSpeedRadioButton"
        Me._LowSpeedRadioButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._LowSpeedRadioButton.Size = New System.Drawing.Size(48, 17)
        Me._LowSpeedRadioButton.TabIndex = 2
        Me._LowSpeedRadioButton.TabStop = True
        Me._LowSpeedRadioButton.Text = "Low"
        Me._LowSpeedRadioButton.UseVisualStyleBackColor = False
        '
        '_StartButton
        '
        Me._StartButton.AllowDrop = True
        Me._StartButton.BackColor = System.Drawing.SystemColors.Control
        Me._StartButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._StartButton.Location = New System.Drawing.Point(245, 128)
        Me._StartButton.Name = "_StartButton"
        Me._StartButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._StartButton.Size = New System.Drawing.Size(107, 27)
        Me._StartButton.TabIndex = 0
        Me._StartButton.Text = "&Start"
        Me._StartButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me._StartButton.UseVisualStyleBackColor = False
        '
        '_RealTimeChart
        '
        Me._RealTimeChart.Location = New System.Drawing.Point(0, 0)
        Me._RealTimeChart.Name = "_RealTimeChart"
        Me._RealTimeChart.Size = New System.Drawing.Size(238, 217)
        Me._RealTimeChart.TabIndex = 10
        '
        'DisplayForm
        '
        Me.AllowDrop = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Window
        Me.ClientSize = New System.Drawing.Size(357, 219)
        Me.Controls.Add(Me._AboutButton)
        Me.Controls.Add(Me._StopButton)
        Me.Controls.Add(Me._CyclesGroupBox)
        Me.Controls.Add(Me._SpeedGroupBox)
        Me.Controls.Add(Me._StartButton)
        Me.Controls.Add(Me._RealTimeChart)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.SystemColors.WindowText
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(231, 169)
        Me.MaximizeBox = False
        Me.Name = "DisplayForm"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "LabOBJX Real-Time Chart - XY Example"
        Me._CyclesGroupBox.ResumeLayout(False)
        Me._CyclesGroupBox.PerformLayout()
        Me._SpeedGroupBox.ResumeLayout(False)
        Me._SpeedGroupBox.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
#End Region
End Class