Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic
Imports System
Imports System.Drawing
Imports System.Media
Imports System.Windows.Forms
Imports isr.Visuals.RealTimeChart

''' <summary> Form for reporting Real Time Chart and Scot Plots. </summary>
Partial Friend Class XYScottPlotForm
    Inherits System.Windows.Forms.Form

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Gets or sets the is initializing component. </summary>
    ''' <value> The is initializing component. </value>
    Private Property IsInitializingComponent As Boolean

    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub New()
        MyBase.New()
        If _Instance Is Nothing Then
            If _InitializingInstance Then
                XYScottPlotForm._Instance = Me
            Else
                Try
                    ' For the start-up form, the first instance created is the default instance.
                    If Not (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint Is Nothing) AndAlso
                        System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType Is Me.GetType() Then
                        XYScottPlotForm._Instance = Me
                    End If
                Catch
                End Try
            End If
        End If
        'This call is required by the Windows Form Designer.
        Me.IsInitializingComponent = True
        Me.InitializeComponent()
        Me.IsInitializingComponent = False
    End Sub

#End Region

#Region " SINGLETON INSTANCE "

    Private Shared _InitializingInstance As Boolean

    Private Shared _Instance As XYScottPlotForm

    ''' <summary> Gets or sets the instance. </summary>
    ''' <value> The instance. </value>
    Public Shared Property Instance() As XYScottPlotForm
        Get
            If XYScottPlotForm._Instance Is Nothing OrElse _Instance.IsDisposed Then
                XYScottPlotForm._InitializingInstance = True
                XYScottPlotForm._Instance = XYScottPlotForm.CreateInstance()
                XYScottPlotForm._InitializingInstance = False
            End If
            Return XYScottPlotForm._Instance
        End Get
        Set(ByVal value As XYScottPlotForm)
            XYScottPlotForm._Instance = Value
        End Set
    End Property

    ''' <summary> Creates the instance. </summary>
    ''' <returns> The new instance. </returns>
    Public Shared Function CreateInstance() As XYScottPlotForm
        Dim theInstance As New XYScottPlotForm()
        theInstance.InitializeControlsState()
        Return theInstance
    End Function

#End Region

#Region " FORM EVENTS "

    ''' <summary> Initializes the controls state. </summary>
    Private Sub InitializeControlsState()

        ' Set the initial speed to medium
        Me.SpeedOptionButton_CheckedChanged(Me._MediumSpeedRadioButton, New EventArgs())
        Me._MediumSpeedRadioButton.Checked = True

        ' Initialize the Cycles variable
        Me.CyclesTextBox_TextChanged(Me._CyclesTextBox, New EventArgs())

        ' Setup RtChart control
        Me.InitRealTimeChart()
        Me.InitializeScoptPlot()

    End Sub

    Private Sub InitializeScoptPlot()
        ' Me._ScottPlot.plt.Style(Global.ScottPlot.Style.Gray1)
        Me._ScottPlot.plt.Style(Color.FromArgb(0, 128, 128), Color.FromArgb(0, 128, 128), Color.FromArgb(128, 128, 128),
                                Color.FromArgb(0, 255, 255), Color.FromArgb(0, 255, 255), Color.FromArgb(0, 255, 255))
        'Me._ScottPlot.BorderStyle = BorderStyle.Fixed3D
        'Me._ScottPlot.BackColor = Color.FromArgb(255, 255, 0)
        'Me._ScottPlot.plt.Ticks(color:=Color.FromArgb(128, 128, 128))
        'Me._ScottPlot.plt.Frame(frameColor:=Color.FromArgb(0, 255, 255))
        Me._ScottPlot.plt.PlotScatter(Program.Abscissa, Program.Ordinate, markerSize:=0, color:=Color.FromArgb(255, 255, 0))
        Me._ScottPlot.plt.AxisAuto(0.05, 0.05)
        Program.ScottPlot = Me._ScottPlot
        Me._ScottPlot.Render()
    End Sub


    ''' <summary> Initializes the real time chart. </summary>
    Private Sub InitRealTimeChart()

        ' define chart
        Me._RealTimeChart.ColorDepth = 0
        Me._RealTimeChart.ErrorBase = 30200
        Me._RealTimeChart.ForeColor = Color.FromArgb(-842150451) ' B2 32 32 33
        Me._RealTimeChart.HitTest = False
        Me._RealTimeChart.ImageFile = String.Empty
        Me._RealTimeChart.LightColor = Color.FromArgb(&HFF, &HFF, &HFF, &HF2) ' (-2147483634)
        Me._RealTimeChart.Outline = True
        Me._RealTimeChart.OutlineColor = Color.FromArgb(&HFF, &HFF, &HFF, &HFA) '(-2147483642)
        Me._RealTimeChart.RoundedCorners = False
        Me._RealTimeChart.ShadowColor = Color.FromArgb(&HFF, &HFF, &HFF, &HF0) '(-2147483632)

        ' Font
        Me._RealTimeChart.FontBold = False

        ' define chart border
        Me._RealTimeChart.BevelInner = isr.Visuals.RealTimeChart.BevelStyle.Inset
        Me._RealTimeChart.BevelOuter = isr.Visuals.RealTimeChart.BevelStyle.Raised
        Me._RealTimeChart.BevelWidthInner = 1
        Me._RealTimeChart.BevelWidthOuter = 1
        Me._RealTimeChart.BorderColor = Color.FromArgb(&HFF, &HFF, &HFF, &HF1)
        Me._RealTimeChart.BorderStyle = BorderStyle.Fixed3D
        Me._RealTimeChart.BorderWidth = 3

        ' Define logical drawing viewport
        Me._RealTimeChart.LogicalViewport = 1
        Me._RealTimeChart.Viewports = 1
        Me._RealTimeChart.ViewportStorageColor = Color.FromArgb(0, &HC0, &HC0) ' 12632256 ' C0 C0 C0
        Me._RealTimeChart.ViewportStorageOn = False

        ' Define logical drawing window
        Me._RealTimeChart.LogicalWindow = 1 ' Window "name"
        Me._RealTimeChart.WndXmin = -1.1 ' -1 <= x <= 1
        Me._RealTimeChart.WndWidth = 2.2
        Me._RealTimeChart.WndYmin = -1.1 ' -1 <= y <= 1
        Me._RealTimeChart.WndHeight = 2.2

        ' define drawing area
        Me._RealTimeChart.AutoSize = False
        Me._RealTimeChart.BackColor = Color.FromArgb(0, 128, 128)
        Me._RealTimeChart.MarginBottom = 10
        Me._RealTimeChart.MarginLeft = 10
        Me._RealTimeChart.MarginRight = 10
        Me._RealTimeChart.MarginTop = 10

        ' define drawing frame
        Me._RealTimeChart.FrameColor = Color.FromArgb(0, 255, 255)
        Me._RealTimeChart.FrameOn = True
        Me._RealTimeChart.FramesPerSec = 0 ' synchronous drawing
        Me._RealTimeChart.FrameTics = isr.Visuals.RealTimeChart.FrameTicsStyle.MajorMinorFrameTics

        ' define the grid
        Me._RealTimeChart.GridColor = Color.FromArgb(128, 128, 128)
        Me._RealTimeChart.GridOn = True
        Me._RealTimeChart.GridSymmetry = isr.Visuals.RealTimeChart.GridSymmetry.IsotropicGrid
        Me._RealTimeChart.GridType = isr.Visuals.RealTimeChart.GridType.HorizontalVertical
        Me._RealTimeChart.MajorDivHorz = 10
        Me._RealTimeChart.MajorDivVert = 8

        ' define Axes
        Me._RealTimeChart.AxesColor = Color.FromArgb(0, 255, 255)
        Me._RealTimeChart.AxesTics = isr.Visuals.RealTimeChart.AxesTicsStyle.MajorMinorAxesTics
        Me._RealTimeChart.AxesType = isr.Visuals.RealTimeChart.AxesType.HorizontalVertical

        ' Specify a drawing pen
        Me._RealTimeChart.LogicalPen = 1 ' Pen "name"
        Me._RealTimeChart.PenColor = Color.FromArgb(255, 255, 0)
        Me._RealTimeChart.PenIntenseColor = Color.FromArgb(255, 255, 255) ' ColorTranslator.ToOle(Color.FromArgb(255, 255, 255))
        Me._RealTimeChart.PenStyle = isr.Visuals.RealTimeChart.PenStyle.SolidPen
        Me._RealTimeChart.PenWidth = 1

        ' Setup display channel parameters
        Me._RealTimeChart.LogicalChannel = 1 ' Channel "name"
        Me._RealTimeChart.ChannelDataDimension = isr.Visuals.RealTimeChart.LogicalDimension.Vertical
        Me._RealTimeChart.ChannelDisplayUserData = 0
        Me._RealTimeChart.ChannelTransformation = 1
        Me._RealTimeChart.ChnDspWindow = 1 ' logical window to use
        Me._RealTimeChart.ChnDspViewport = 1 ' viewport to use
        Me._RealTimeChart.ChnDspPen = 1 ' pen to use
        Me._RealTimeChart.ChnDspBufLen = Program.DataUpperBound + 1 ' input buffer length
        Me._RealTimeChart.ChnDspStyle = isr.Visuals.RealTimeChart.ChannelDisplayStyle.Lines
        Me._RealTimeChart.LogicalScale = 1

        ' Describe horizontal dimension
        Me._RealTimeChart.ChannelDataDimension = LogicalDimension.Horizontal
        Me._RealTimeChart.ChnDataShape = ChannelDataShape.Scalar
        Me._RealTimeChart.ChnDataType = ChannelDataType.Double
        Me._RealTimeChart.ChnDataOffset = 0
        Me._RealTimeChart.ChnDataIncr = 1 ' logical increment to next value

        ' Describe vertical dimension
        Me._RealTimeChart.ChannelDataDimension = LogicalDimension.Vertical
        Me._RealTimeChart.ChnDataShape = ChannelDataShape.Scalar
        Me._RealTimeChart.ChnDataType = ChannelDataType.Double
        Me._RealTimeChart.ChnDataOffset = 0
        Me._RealTimeChart.ChnDataIncr = 1 ' logical increment to next value

        ' Initialize the channel
        Me._RealTimeChart.ChnDspAction = ChannelDisplayAction.Initialize

        ' set reference to the initialized chart
        Program.RtChart = _RealTimeChart

    End Sub

#End Region

#Region " CONTROL EVENTS  "

    Private Sub AboutButton_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _AboutButton.Click

        Dim LF As String = Strings.Chr(10).ToString() & Strings.Chr(13).ToString()

        Dim msg As String = "This example demonstrates how to plot one data set against another.  Two transfer controls are used to supply data "
        msg &= "to the graph.  To make this example more interesting, after each plot completes, the graph's ProcessDone "
        msg &= "procedure changes the Y data set and sends both data sets back into the graph.  The effects you see are the "
        msg &= "result of charting two, out of phase, sinusoid signals against one another.  Note, Visual Basic is used to "
        msg &= "calculate the sinusoids, transfer the arrays, and re-start the controls. "

        System.Windows.Forms.MessageBox.Show(msg, "About This Example", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

    End Sub

    Private Sub StartButton_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _StartButton.Click

        ' Determine the length of the data array
        Program.DataUpperBound = Program.Ordinate.GetUpperBound(0)

        ' Calculate the Y-axis wave
        Dim twoPi As Double = 2 + Math.PI
        For i As Integer = 0 To Program.DataUpperBound
            Program.Ordinate(i) = Math.Sin(Program.Cycles * twoPi * i / Program.DataUpperBound)
        Next i
        Program.Phase += Program.PhaseRate

        IsRunning = True

    End Sub

    Private Sub StopButton_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _StopButton.Click
        Program.IsRunning = False
    End Sub

    Private Sub SpeedOptionButton_CheckedChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _HighSpeedRadioButton.CheckedChanged,
                                                                                                           _MediumSpeedRadioButton.CheckedChanged,
                                                                                                           _LowSpeedRadioButton.CheckedChanged
        If IsInitializingComponent Then Return
        Select Case True
            Case _HighSpeedRadioButton.Checked
                Program.PhaseRate = 1
            Case _MediumSpeedRadioButton.Checked
                Program.PhaseRate = 0.1
            Case _LowSpeedRadioButton.Checked
                Program.PhaseRate = 0.01
        End Select
    End Sub

    Private Sub CyclesTextBox_TextChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _CyclesTextBox.TextChanged
        If IsInitializingComponent Then Return
        ' Update the number of cycles in the Y-wave
        Program.Cycles = Conversion.Val(Me._CyclesTextBox.Text)
    End Sub

    ''' <summary> Defines the maximum number of characters that we will accept in text boxes (used by the "ValidNumKey" function) </summary>
    Private Const _NumericCycleLength As Integer = 3

    Private Sub CyclesTextBox_KeyPress(ByVal eventSender As Object, ByVal eventArgs As KeyPressEventArgs) Handles _CyclesTextBox.KeyPress
        Dim KeyAscii As Integer = Strings.Asc(eventArgs.KeyChar)
        Try
            ' Reject any non-numeric key presses
            If Not ValidNumKey(KeyAscii, _CyclesTextBox, _NumericCycleLength) Then
                SystemSounds.Beep.Play()
            End If
        Finally
            If KeyAscii = 0 Then
                eventArgs.Handled = True
            End If
            eventArgs.KeyChar = Convert.ToChar(KeyAscii)
        End Try
    End Sub

    Private Function ValidNumKey(ByRef keyAscii As Integer, ByRef inputTextBox As TextBox, ByRef length As Integer) As Integer

        Dim result As Integer
        Dim characterValue As String

        ' Validates numeric entry. Allows numeric chars, decimal point and -ve sign
        ' (if valid usage) and limits the numeric entry to specified length
        Dim enteredTextBox As TextBox
        If TypeOf inputTextBox Is TextBox Then
            enteredTextBox = inputTextBox
            If Strings.Len(enteredTextBox.Text.TrimStart()) >= length And (enteredTextBox.SelectionLength = 0) Then
                ' only allow controlling keys...
                Return keyAscii = &H8S
            End If
        End If

        result = False

        Select Case keyAscii
            Case 48 To 57
                'Numbers
                result = True
            Case 8
                'control key
                result = True
            Case Strings.Asc("-"c), Strings.Asc("+"c)
                'Negative character.  Valid if first char, precedes "e" or "d" or
                'this typing will replace all (all text is selected)
                If (Strings.Len(inputTextBox.Text) = 0) Or inputTextBox.SelectionLength = Strings.Len(inputTextBox.Text) Then result = True
                If inputTextBox.SelectionLength = 0 Then
                    characterValue = inputTextBox.Text.Substring(Math.Max(inputTextBox.Text.Length - 1, 0))
                Else
                    If inputTextBox.SelectionStart = 0 Then characterValue = inputTextBox.Text.Substring(inputTextBox.SelectionStart, Math.Min(1, inputTextBox.Text.Length - inputTextBox.SelectionStart)) Else characterValue = inputTextBox.Text.Substring(inputTextBox.SelectionStart - 1, Math.Min(1, inputTextBox.Text.Length - (inputTextBox.SelectionStart - 1)))
                End If
                If characterValue = "e" Or characterValue = "E" Or characterValue = "d" Or characterValue = "D" Then result = True

            Case Strings.Asc("."c)
                'Decimal point.  Valid if does not already exist or this typing replaces existing one
                If (inputTextBox.Text.IndexOf("."c) + 1) = 0 Or inputTextBox.SelectedText.IndexOf("."c) >= 0 Then result = True

            Case Strings.Asc("e"c), Strings.Asc("E"c), Strings.Asc("d"c), Strings.Asc("D"c)
                'Exponential and precision descriptors
                If (inputTextBox.Text.IndexOf("e"c) + 1) = 0 Or inputTextBox.SelectedText.IndexOf("e"c) >= 0 Then
                    If (inputTextBox.Text.IndexOf("d"c) + 1) = 0 Or inputTextBox.SelectedText.IndexOf("d"c) >= 0 Then
                        If (inputTextBox.Text.IndexOf("D"c) + 1) = 0 Or inputTextBox.SelectedText.IndexOf("D"c) >= 0 Then
                            If (inputTextBox.Text.IndexOf("E"c) + 1) = 0 Or inputTextBox.SelectedText.IndexOf("E"c) >= 0 Then result = True
                        End If
                    End If
                End If
                If (Strings.Len(inputTextBox.Text) = 0) Or inputTextBox.SelectionLength = Strings.Len(inputTextBox.Text) Then result = False

            Case 13
                result = True

        End Select

        Return result
    End Function

    Protected Overrides Sub OnClosed(e As EventArgs)
        MyBase.OnClosed(e)
    End Sub

#End Region

End Class
