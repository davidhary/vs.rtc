<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RealtimeForm

    Private visualControls() As String = New String() {"components", "ToolTipMain", "StartStop", "ChartDataResults", "RtChart1"}
    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Public _ToolTip As System.Windows.Forms.ToolTip
    Public WithEvents _StartStopButton As System.Windows.Forms.Button
    Public WithEvents _ChartDataResultsTextBox As System.Windows.Forms.TextBox
    Public WithEvents _Chart As isr.Visuals.RealTimeChart.RealTimeChartControl
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._StartStopButton = New System.Windows.Forms.Button()
        Me._ChartDataResultsTextBox = New System.Windows.Forms.TextBox()
        Me._Chart = New isr.Visuals.RealTimeChart.RealTimeChartControl()
        Me._MillisecondsDelayNumeric = New System.Windows.Forms.NumericUpDown()
        Me._MillisecondsDelayNumericLabel = New System.Windows.Forms.Label()
        CType(Me._MillisecondsDelayNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_StartStopButton
        '
        Me._StartStopButton.AllowDrop = True
        Me._StartStopButton.BackColor = System.Drawing.SystemColors.Control
        Me._StartStopButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._StartStopButton.Location = New System.Drawing.Point(279, 341)
        Me._StartStopButton.Name = "_StartStopButton"
        Me._StartStopButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._StartStopButton.Size = New System.Drawing.Size(89, 29)
        Me._StartStopButton.TabIndex = 1
        Me._StartStopButton.Text = "Start"
        Me._StartStopButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me._StartStopButton.UseVisualStyleBackColor = False
        '
        '_ChartDataResultsTextBox
        '
        Me._ChartDataResultsTextBox.AcceptsReturn = True
        Me._ChartDataResultsTextBox.AllowDrop = True
        Me._ChartDataResultsTextBox.BackColor = System.Drawing.SystemColors.ControlLight
        Me._ChartDataResultsTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._ChartDataResultsTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._ChartDataResultsTextBox.Dock = System.Windows.Forms.DockStyle.Top
        Me._ChartDataResultsTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._ChartDataResultsTextBox.Location = New System.Drawing.Point(0, 313)
        Me._ChartDataResultsTextBox.MaxLength = 0
        Me._ChartDataResultsTextBox.Name = "_ChartDataResultsTextBox"
        Me._ChartDataResultsTextBox.ReadOnly = True
        Me._ChartDataResultsTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ChartDataResultsTextBox.Size = New System.Drawing.Size(370, 20)
        Me._ChartDataResultsTextBox.TabIndex = 0
        '
        '_Chart
        '
        Me._Chart.Dock = System.Windows.Forms.DockStyle.Top
        Me._Chart.Location = New System.Drawing.Point(0, 0)
        Me._Chart.Name = "_Chart"
        Me._Chart.Size = New System.Drawing.Size(370, 313)
        Me._Chart.TabIndex = 2
        '
        '_MillisecondsDelayNumeric
        '
        Me._MillisecondsDelayNumeric.Location = New System.Drawing.Point(118, 345)
        Me._MillisecondsDelayNumeric.Name = "_MillisecondsDelayNumeric"
        Me._MillisecondsDelayNumeric.Size = New System.Drawing.Size(70, 20)
        Me._MillisecondsDelayNumeric.TabIndex = 3
        Me._MillisecondsDelayNumeric.Value = New Decimal(New Integer() {20, 0, 0, 0})
        '
        '_MillisecondsDelayNumericLabel
        '
        Me._MillisecondsDelayNumericLabel.AutoSize = True
        Me._MillisecondsDelayNumericLabel.Location = New System.Drawing.Point(2, 349)
        Me._MillisecondsDelayNumericLabel.Name = "_MillisecondsDelayNumericLabel"
        Me._MillisecondsDelayNumericLabel.Size = New System.Drawing.Size(114, 13)
        Me._MillisecondsDelayNumericLabel.TabIndex = 4
        Me._MillisecondsDelayNumericLabel.Text = "Update period, ms:"
        '
        'RealtimeForm
        '
        Me.AllowDrop = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(370, 372)
        Me.Controls.Add(Me._MillisecondsDelayNumericLabel)
        Me.Controls.Add(Me._MillisecondsDelayNumeric)
        Me.Controls.Add(Me._StartStopButton)
        Me.Controls.Add(Me._ChartDataResultsTextBox)
        Me.Controls.Add(Me._Chart)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.SystemColors.WindowText
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Location = New System.Drawing.Point(115, 115)
        Me.MaximizeBox = False
        Me.Name = "RealtimeForm"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "LabOBJX Real-Time Chart - Real Time Data"
        CType(Me._MillisecondsDelayNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Private WithEvents _MillisecondsDelayNumeric As NumericUpDown
    Private WithEvents _MillisecondsDelayNumericLabel As Label
End Class