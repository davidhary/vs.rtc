Option Strict Off
Option Explicit On
Imports System
Imports isr.Visuals.RealTimeChart

Friend Module Program
    ' LabOBJX Real-Time Chart - Example Code
    ' (C) Copyright 1995, Scientific Software Tools, Inc.
    ' All Rights Reserved.
    '

    Public Const AssemblyTitle As String = "Real Time Strip Chart 2 Demo"
    Public Const AssemblyDescription As String = "Real Time Strip Chart 2 Demo"
    Public Const AssemblyProduct As String = "Visuals.Real.Time.Chart.StripChart2"


    Public Property Result As Integer ' return value from function call
    Public Property Running As Integer

    Public Property VBDataArrayY1() As Single ' VB array to store multi-channel voltage data values
    Public Property VBDataArrayY2() As Single ' VB array to store multi-channel voltage data values

    <CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>")>
    Public VBDataArray(,) As Single ' VB array to store multi-channel voltage data values

    <CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>")>
    Public JaggedArray()() As Single ' VB array to store multi-channel voltage data values

    <CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>")>
    Public Abscissa() As Single

    Public Const VOLTS_PER_DIV As Integer = 5 ' vertical scaling is based on volts per division
    Public Const TIME_PER_DIV As Integer = 10 ' horizontal scaling is based on seconds per division

    ' grid constants
    Public Const MAJOR_HORZ_DIV As Integer = 10
    Public Const MAJOR_VERT_DIV As Integer = 8
    Public Const MINOR_DIVISIONS As Integer = 5

    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    Public Sub InitializeChannels(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal numChannels As Integer, ByVal samplesPerChannel As Integer)

        ' Initialize RtChart Window, Viewports, & Channels in CRT mode, Line style.

        ' View port Description
        rtChart.Viewports = 4 ' number of view ports displayed initially
        rtChart.ViewportStorageColor = Color.Green ' storage mode plot color, same for all view ports

        ' initialize storage mode for each view port
        For viewport As Integer = 1 To rtChart.Viewports
            rtChart.LogicalViewport = (viewport) ' select view port
            rtChart.ViewportStorageOn = False ' disable storage mode
        Next viewport

        ' Window Description
        rtChart.LogicalWindow = 1 ' select window
        rtChart.WndXmin = 0 ' left minimum abscissa
        rtChart.WndWidth = (TIME_PER_DIV * MAJOR_HORZ_DIV) ' in Seconds (Time/div * 10 divisions) 10 * 10 = 100
        rtChart.WndHeight = (VOLTS_PER_DIV * MAJOR_VERT_DIV) ' in Volts (Volts/div * 8 divisions) 5 * 8 = 40
        rtChart.WndYmin = (rtChart.WndHeight / -2) ' bottom minimum ordinate (negative full scale) 40 / -2 = -20

        ' initialize all channels to be used
        For channel As Integer = ChannelName.Channel1 To ChannelName.Channel4

            ' Channel Description
            rtChart.LogicalChannel = channel ' select channel
            rtChart.ChnDspWindow = 1 ' assign all channels to window 1
            rtChart.ChnDspViewport = channel ' assign each channel to a viewport
            rtChart.ChnDspPen = channel ' specify pen, use same "name" (value) as channel
            rtChart.ChnDspBufLen = samplesPerChannel ' input buffer length for 2D array
            rtChart.ChnDspMode = ChannelDisplayMode.CRT ' per channel display mode, use RunMode constants
            rtChart.ChnDspStyle = ChannelDisplayStyle.Lines ' use line style for plotting arrays
            rtChart.ChnDspOffsetX = 0 ' set offset to 0
            rtChart.ChnDspOffsetY = 0 ' set offset to 0

            ' Describe horizontal dimension
            rtChart.ChannelDataDimension = LogicalDimension.Horizontal
            rtChart.ChnDataShape = ChannelDataShape.AutoIncr
            rtChart.ChnDataOffset = 0 ' offset into buffer (in points)
            rtChart.ChnDataIncr = 1 ' logical increment to next value in array

            ' Describe vertical dimension
            rtChart.ChannelDataDimension = LogicalDimension.Vertical
            rtChart.ChnDataShape = ChannelDataShape.Scalar
            rtChart.ChnDataType = ChannelDataType.Single
            rtChart.ChnDataOffset = 0 ' offset into buffer (in points)
            rtChart.ChnDataIncr = 1 ' logical increment to next value for 2D array

            rtChart.ChnDspAction = ChannelDisplayAction.Initialize

        Next channel

    End Sub

    Public Sub InitializeRtChart(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl)

        ' Global operation properties
        rtChart.ImageFile = String.Empty ' specifies file name for Write action
        rtChart.HitTest = False ' enables mouse pointer "hit" detection
        rtChart.FramesPerSec = -1 ' 0 for asynchronous, -1 for synchronous

        ' Border description properties
        rtChart.BorderColor = Color.LightGray
        rtChart.LightColor = Color.DarkGray
        rtChart.ShadowColor = Color.Black
        rtChart.BevelInner = BevelStyle.Inset
        rtChart.BevelOuter = BevelStyle.Raised
        rtChart.BevelWidthInner = 5
        rtChart.BevelWidthOuter = 5
        rtChart.BorderWidth = 3
        rtChart.Outline = True
        rtChart.OutlineColor = Color.Black
        rtChart.RoundedCorners = False

        ' Graticule description properties
        rtChart.BackColor = Color.DarkCyan
        rtChart.AutoSize = True ' true forces square border, frame is always square

        rtChart.FrameOn = True
        rtChart.FrameTics = FrameTicsStyle.MajorMinorFrameTics
        rtChart.FrameColor = Color.White

        rtChart.AxesType = AxesType.NoAxes ' RtChart_HorzVertAxes
        rtChart.AxesTics = AxesTicsStyle.MajorMinorAxesTics
        rtChart.AxesColor = Color.White

        rtChart.GridOn = False
        rtChart.GridType = GridType.HorizontalVertical
        rtChart.GridColor = Color.LightGray

        rtChart.MajorDivHorz = MAJOR_HORZ_DIV ' major horizontal divisions
        rtChart.MajorDivVert = MAJOR_VERT_DIV ' major vertical divisions
        rtChart.MinorDiv = MINOR_DIVISIONS ' minor divisions
        rtChart.MarginLeft = 2 ' left Graticule border
        rtChart.MarginTop = 2 ' top Graticule border
        rtChart.MarginRight = 2 ' right Graticule border
        rtChart.MarginBottom = 2 ' bottom Graticule border

        rtChart.ColorDepth = ColorDepth.Color16 '  16 colors

    End Sub

    Public Sub PlotData(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal numChannels As Integer,
                        ByVal numPoints As Integer, ByRef firstPoint As Double, ByRef vbDataArray(,) As Single)

        ' RtChart exports ChartData function to plot data of selected channel
        For channel As Integer = ChannelName.Channel1 To numChannels
            Result = rtChart.ChartData(channel, numPoints, vbDataArray, firstPoint + numPoints * channel, 0, 0)

            ' display result of chart data call
            If Result > 0 Then
                ' returned value is points charted
                RealtimeForm.DefInstance._ChartDataResultsTextBox.Text = "Number of points charted = " & Result.ToString()
            Else
                ' returned value is error code, convert to error string
                RealtimeForm.DefInstance._ChartDataResultsTextBox.Text = ChartPen.ChartErrorMessage(Result)
            End If
        Next channel

    End Sub

    Public Sub PlotData(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal numChannels As Integer,
                        ByVal numPoints As Integer, ByRef firstPoint As Double, ByVal vbDataArray()() As Single)


        ' RtChart exports ChartData function to plot data of selected channel
        For channel As Integer = ChannelName.Channel1 To numChannels
            Result = rtChart.ChartData(channel, numPoints, vbDataArray(channel - 1), firstPoint, Abscissa, firstPoint)

            ' display result of chart data call
            If Result > 0 Then
                ' returned value is points charted
                RealtimeForm.DefInstance._ChartDataResultsTextBox.Text = "Number of points charted = " & Result.ToString()
            Else
                ' returned value is error code, convert to error string
                RealtimeForm.DefInstance._ChartDataResultsTextBox.Text = ChartPen.ChartErrorMessage(Result)
            End If
        Next channel

    End Sub

End Module
