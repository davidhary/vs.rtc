Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic
Imports System
Imports System.Windows.Forms
Imports isr.Visuals.RealTimeChart
Partial Friend Class RealtimeForm
    Inherits System.Windows.Forms.Form

    ' LabOBJX Real-Time Chart - Example Code
    ' (C) Copyright 1995, Scientific Software Tools, Inc.
    ' All Rights Reserved.
    '

#Region " CONSTRUCTION and CLEANUP "

    Private ReadOnly _IsInitializingComponent As Boolean

    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub New()
        MyBase.New()
        _IsInitializingComponent = True
        If _Instance Is Nothing Then
            If _InitializingInstance Then
                _Instance = Me
            Else
                Try
                    'For the start-up form, the first instance created is the default instance.
                    If Not (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint Is Nothing) AndAlso System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType Is Me.GetType() Then
                        _Instance = Me
                    End If

                Catch
                End Try
            End If
        End If
        'This call is required by the Windows Form Designer.
        InitializeComponent()
        _IsInitializingComponent = False
        If Not Me.DesignMode Then ReLoadForm(False)
    End Sub

#Region " SINGLTON "
    Private Shared _Instance As RealtimeForm
    Private Shared _InitializingInstance As Boolean
    Public Shared Property DefInstance() As RealtimeForm
        Get
            If _Instance Is Nothing OrElse _Instance.IsDisposed Then
                _InitializingInstance = True
                _Instance = CreateInstance()
                _InitializingInstance = False
            End If
            Return _Instance
        End Get
        Set(ByVal value As RealtimeForm)
            _Instance = Value
        End Set
    End Property

    Public Shared Function CreateInstance() As RealtimeForm
        Dim theInstance As New RealtimeForm()
        theInstance.ResetKnownState()
        Return theInstance
    End Function

    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    Public Sub ReLoadForm(ByVal addEvents As Boolean)
        ResetKnownState()
    End Sub


#End Region

#End Region

    Private Const _TWO_PIE As Double = 2 * 3.1415926535898

    Private _FirstPoint As Integer
    Private _NumSamples As Integer ' number of samples
    Private _NumChannels As Integer ' number of channels

    ' signal waveform limits (volts peak)
    Private _MaxVolts As Single
    Private _MinVolts As Single


    Private Sub ResetKnownState()

        ' initialize program's global variables
        InitializeGlobalVariables()

        ' initialize RtChart Pens
        ChartPen.InitChannelsPens(_Chart)

        ' initialize RtChart global properties
        InitializeRtChart(_Chart)

        ' initialize RtChart Pens
        ChartPen.InitChannelsPens(_Chart)

        ' initialize RtChart for Volts vs Time plots
        InitializeChannels(_Chart, _NumChannels, _NumSamples)

    End Sub

    Private Sub GenerateSineWaveArrays(ByVal numChannels As Integer, ByVal numSamples As Integer, ByRef voltsPeak As Single)

        Dim cycleCount, cycleTime As Single

        JaggedArray = New Single(numChannels - 1)() {}
        Abscissa = New Single(numSamples - 1) {}

        ' NumSamples is 0-based, NumChannels is 1-based
        ReDim VBDataArray(numSamples, numChannels) ' dimension array for 2D values

        Dim RadiansPerSample As Single = _TWO_PIE / numSamples

        ' NumCycles determines # of cycles per channel in buffer
        For channel As Integer = 1 To numChannels
            cycleCount = (channel * RadiansPerSample)
            Dim samples As Single() = New Single(numSamples - 1) {}
            For sample As Integer = 0 To numSamples - 1
                Abscissa(sample) = sample
                cycleTime = RadiansPerSample + cycleCount
                Dim value As Single = voltsPeak * Math.Sin(sample * cycleTime)
                VBDataArray(sample, channel) = value
                samples(sample) = value
            Next sample
            JaggedArray(channel - 1) = samples
        Next channel

    End Sub

    Private Sub InitializeGlobalVariables()

        'initialize variables
        Program.Running = False

        _NumChannels = 4
        _NumSamples = 100

        _MaxVolts = 15
        _MinVolts = -15

    End Sub

    Private Sub SimulateContinuousData(ByVal numChannels As Integer, ByVal numSamples As Integer)


        Do While Running

            ' prevent each channel from being re-painted individually
            _Chart.ChartAction = ChartAction.DisablePaint

            ' Generate random starting point in buffer.
            _FirstPoint = Math.Floor(CDbl((numSamples - 1) * VBMath.Rnd() + 1))

            ' PlotData call RtChart "ChartData" plotting function for each channel
            ' PlotData(_Chart, numChannels, numSamples, FirstPoint, VBDataArray)
            PlotData(_Chart, numChannels, numSamples, _FirstPoint, JaggedArray)

            ' paint entire window with all channels at once
            _Chart.ChartAction = ChartAction.EnablePaint

            Application.DoEvents() ' give control to Windows to process events (mouse, keyboard, etc.)

            Threading.Thread.Sleep(Me._MillisecondsDelayNumeric.Value)

        Loop

    End Sub

    Private Sub StartStop_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _StartStopButton.Click

        If Not Program.Running Then
            Program.Running = True
            _StartStopButton.Text = "Stop"

            ' generate 2-dimensional array twice as large to plot "random" wave
            GenerateSineWaveArrays(_NumChannels, _NumSamples * 2, _MaxVolts)

            ' Continuously call PlotData with random buffer start point
            SimulateContinuousData(_NumChannels, _NumSamples)

        Else
            Program.Running = False
            _StartStopButton.Text = "Start"

        End If

    End Sub

End Class

