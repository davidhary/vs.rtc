Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic
Imports System
Imports System.Windows.Forms
Partial Friend Class IntegersForm
    Inherits System.Windows.Forms.Form

    ' LabOBJX Real-Time Chart - Example Code
    ' (C) Copyright 1995, Scientific Software Tools, Inc.
    ' All Rights Reserved.
    '

    Const TWO_PIE As Double = 2 * 3.1415926535898

    Dim NumSamples As Integer ' number of samples
    Dim FirstPoint As Integer

    ' signal waveform limits (volts peak)
    Dim MaxValue As Integer
    Dim MinValue As Integer

    Public Sub New()
        MyBase.New()
        If _Instance Is Nothing Then
            If _InitializingInstance Then
                _Instance = Me
            Else
                Try
                    'For the start-up form, the first instance created is the default instance.
                    If Not (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint Is Nothing) AndAlso System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType Is Me.GetType() Then
                        _Instance = Me
                    End If

                Catch
                End Try
            End If
        End If
        'This call is required by the Windows Form Designer.
        InitializeComponent()
        If Not Me.DesignMode Then ReLoadForm(False)
    End Sub


    Private Sub Form_Load()

        ' initialize program's global variables
        InitializeGlobalVariables()

        ' initialize RtChart Pens
        ChartPen.InitChannelsPens(RtChart1)

        ' initialize RtChart global properties
        InitializeRtChart(RtChart1)

        ' initialize RtChart Pens
        ChartPen.InitChannelsPens(RtChart1)

        ' initialize RtChart for Volts vs Time plots
        ' Window height is 65536 to plot full range of integers, must be converted to long
        InitializeChannels(RtChart1, MinValue, MaxValue - MinValue, NumSamples)


    End Sub

    Private Sub GenerateSineArrayIntegers(ByVal numSamples As Integer, ByRef PeakValue As Integer)


        'redim array for NumSamples Single precision values
        ReDim VBDataArray(numSamples)

        Dim RadiansPerSample As Single = TWO_PIE / numSamples

        Dim NumCycles As Integer = 4 ' 4 cycles in buffer, 1/2 of buffer is plotted

        For sample As Integer = 0 To numSamples - 1

            VBDataArray(sample) = PeakValue * Math.Sin(sample * RadiansPerSample * NumCycles)

        Next sample

    End Sub

    Private Sub InitializeGlobalVariables()

        'initialize variables

        NumSamples = 2000

        ' integers have 5 significant digits
        MaxValue = 32767
        MinValue = -32768

    End Sub

    Private Sub Plot_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles Plot.Click

        ' generate array of integer values twice as large to plot "random" wave
        GenerateSineArrayIntegers(NumSamples * 2, MaxValue)

        ' Generate random starting point in buffer.
        FirstPoint = Math.Floor(CDbl((NumSamples - 1) * VBMath.Rnd() + 1))

        ' calls ChartData function
        PlotData(RtChart1, NumSamples, FirstPoint, VBDataArray)

    End Sub
    Private Sub Form_Closed(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MyBase.Closed
    End Sub
End Class