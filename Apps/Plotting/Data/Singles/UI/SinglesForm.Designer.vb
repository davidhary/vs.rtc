<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SinglesForm
#Region "Upgrade Support "
	Private Shared _Instance As SinglesForm
	Private Shared _InitializingInstance As Boolean
	Public Shared Property DefInstance() As SinglesForm
		Get
			If _Instance Is Nothing OrElse _Instance.IsDisposed Then
				_InitializingInstance = True
				_Instance = CreateInstance()
				_InitializingInstance = False
			End If
			Return _Instance
		End Get
		Set(ByVal value As SinglesForm)
			_Instance = Value
		End Set
	End Property
#End Region
#Region "Windows Form Designer generated code "
	Public Shared Function CreateInstance() As SinglesForm
		Dim theInstance As New SinglesForm()
		theInstance.Form_Load()
		Return theInstance
	End Function
	Private visualControls() As String = New String() {"components", "ToolTipMain", "Plot", "ChartDataResults", "RtChart1"}
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTipMain As System.Windows.Forms.ToolTip
	Public WithEvents Plot As System.Windows.Forms.Button
	Public WithEvents ChartDataResults As System.Windows.Forms.TextBox
	Public WithEvents RtChart1 As isr.Visuals.RealTimeChart.RealTimeChartControl
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> _
	 Private Sub InitializeComponent()
		Me.components = New System.ComponentModel.Container()
		Dim resources As New System.ComponentModel.ComponentResourceManager(GetType(SinglesForm))
		Me.ToolTipMain = New System.Windows.Forms.ToolTip(Me.components)
		Me.Plot = New System.Windows.Forms.Button()
		Me.ChartDataResults = New System.Windows.Forms.TextBox()
		Me.RtChart1 = New isr.Visuals.RealTimeChart.RealTimeChartControl()
		Me.SuspendLayout()
		' 
		'Plot
		' 
		Me.Plot.AllowDrop = True
		Me.Plot.BackColor = System.Drawing.SystemColors.Control
		Me.Plot.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Plot.Location = New System.Drawing.Point(140, 352)
		Me.Plot.Name = "Plot"
		Me.Plot.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Plot.Size = New System.Drawing.Size(89, 29)
		Me.Plot.TabIndex = 1
		Me.Plot.Text = "Plot"
		Me.Plot.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
		Me.Plot.UseVisualStyleBackColor = False
		' 
		'ChartDataResults
		' 
		Me.ChartDataResults.AcceptsReturn = True
		Me.ChartDataResults.AllowDrop = True
		Me.ChartDataResults.BackColor = System.Drawing.SystemColors.Window
		Me.ChartDataResults.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.ChartDataResults.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.ChartDataResults.ForeColor = System.Drawing.SystemColors.WindowText
		Me.ChartDataResults.Location = New System.Drawing.Point(68, 320)
		Me.ChartDataResults.MaxLength = 0
		Me.ChartDataResults.Name = "ChartDataResults"
		Me.ChartDataResults.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ChartDataResults.Size = New System.Drawing.Size(245, 21)
		Me.ChartDataResults.TabIndex = 0
		' 
		'RtChart1
		' 
		Me.RtChart1.AutoSize = 0
		Me.RtChart1.AxesColor = 16776960
		Me.RtChart1.AxesTics = 0
		Me.RtChart1.AxesType = 0
		Me.RtChart1.BackColor = 8421376
		Me.RtChart1.BevelInner = 0
		Me.RtChart1.BevelOuter = 2
		Me.RtChart1.BevelWidth_Inner = 1
		Me.RtChart1.BevelWidth_Outer = 1
		Me.RtChart1.BorderColor = (-2147483633)
		Me.RtChart1.BorderWidth = 3
		Me.RtChart1.Chn_Select = 1
		Me.RtChart1.ChnData_Dimension = 1
		Me.RtChart1.ChnDspUserData = 0
		Me.RtChart1.ChnXform_Select = 1
		Me.RtChart1.ColorDepth = 0
		Me.RtChart1.ErrBase = 30200
		Me.RtChart1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.RtChart1.ForeColor = (-842150451)
		Me.RtChart1.FrameColor = 16776960
		Me.RtChart1.FrameOn = -1
		Me.RtChart1.FramesPerSec = 0
		Me.RtChart1.FrameTics = 0
		Me.RtChart1.GridColor = 8421504
		Me.RtChart1.GridOn = -1
		Me.RtChart1.GridSymmetry = 0
		Me.RtChart1.GridType = 0
		Me.RtChart1.HitTest = 0
		Me.RtChart1.ImageFile = String.Empty
		Me.RtChart1.LightColor = (-2147483634)
		Me.RtChart1.Location = New System.Drawing.Point(0, 0)
		Me.RtChart1.MajorDivHorz = 10
		Me.RtChart1.MajorDivVert = 8
		Me.RtChart1.MarginBottom = 10
		Me.RtChart1.MarginLeft = 10
		Me.RtChart1.MarginRight = 10
		Me.RtChart1.MarginTop = 10
		Me.RtChart1.MinorDiv = 5
		Me.RtChart1.MouseIcon = 0
		Me.RtChart1.Name = "RtChart1"
		Me.RtChart1.Outline = -1
		Me.RtChart1.OutlineColor = (-2147483642)
		Me.RtChart1.Pen_Select = 1
		Me.RtChart1.RoundedCorners = 0
		Me.RtChart1.Scale_Select = 1
		Me.RtChart1.ShadowColor = (-2147483632)
		Me.RtChart1.Size = New System.Drawing.Size(353, 305)
		Me.RtChart1.TabIndex = 2
		Me.RtChart1.Viewport_Select = 1
		Me.RtChart1.Viewports = 1
		Me.RtChart1.ViewportStorageColor = 12632256
		Me.RtChart1.ViewportStorageOn = 0
		Me.RtChart1.Wnd_Select = 1
		' 
		'RTCPlotForm
		' 
		Me.AllowDrop = True
		Me.AutoScaleDimensions = New System.Drawing.SizeF(7, 12)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Window
		Me.ClientSize = New System.Drawing.Size(355, 392)
		Me.Controls.Add(Me.Plot)
		Me.Controls.Add(Me.ChartDataResults)
		Me.Controls.Add(Me.RtChart1)
		Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.ForeColor = System.Drawing.SystemColors.WindowText
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Location = New System.Drawing.Point(119, 135)
		Me.Location = New System.Drawing.Point(116, 116)
		Me.MaximizeBox = False
		Me.MinimizeBox = True
		Me.Name = "RTCPlotForm"
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Size = New System.Drawing.Size(361, 414)
		Me.Text = "LabOBJX RT Chart - Single Precision"
		Me.ResumeLayout(False)
	End Sub
	Sub ReLoadForm(ByVal addEvents As Boolean)
		Form_Load()
		If addEvents Then
			AddHandler MyBase.Closed, AddressOf Me.Form_Closed
		End If
	End Sub
#End Region
End Class