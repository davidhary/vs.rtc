Option Strict Off
Option Explicit On
Imports System
Imports isr.Visuals.RealTimeChart
Module SINGLES1
    ' LabOBJX Real-Time Chart - Example Code
    ' (C) Copyright 1995, Scientific Software Tools, Inc.
    ' All Rights Reserved.
    '

    Public Const AssemblyTitle As String = "Real Time Strip Chart Singles Demo"
    Public Const AssemblyDescription As String = "Real Time Strip Chart Singles Demo"
    Public Const AssemblyProduct As String = "Visuals.Real.Time.Chart.Singles"

    Public result As Integer ' return value from function call

	Public VBDataArray() As Single ' VB array to store Single precision voltage data values

	Public Const VOLTS_PER_DIV As Integer = 5 ' vertical scaling is based on volts per division
	Public Const TIME_PER_DIV As Integer = 10 ' horizontal scaling is based on seconds per division

	' grid constants
	Public Const MAJOR_HORZ_DIV As Integer = 10
	Public Const MAJOR_VERT_DIV As Integer = 8
	Public Const MINOR_DIVISIONS As Integer = 5

	Sub InitializeChannels(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByRef WindowMin As Single, ByRef WindowHeight As Single, ByVal numSamples As Integer)
		Dim RtChart_AutoIncr, RtChart_ChnInitialize, RtChart_CRT, RtChart_Horz, RtChart_Lines, RtChart_Scalar, RtChart_Single, RtChart_Vert As Object

		' Initialize RtChart Window, Viewports, & Channels in CRT mode, Line style.
		' specify Single precision data type

		' View port Description
		RtChart.Viewports = 1 ' number of view ports displayed initially
		RtChart.ViewportStorageColor  = Color.Green ' storage mode plot color, same for all view ports

		' Window Description
		RtChart.LogicalWindow = 1 ' select window
		RtChart.WndWidth = (NumSamples) ' in number of samples
		RtChart.WndXmin = 0 ' left minimum abscissa
		RtChart.WndHeight = (WindowHeight) ' range of values (span)
		ReflectionHelper.LetMember(RtChart, "WndYmin", WindowMin) ' bottom minimum ordinate (negative full scale)

		' initialize channel

		' Channel Description
		rtChart.LogicalChannel = ( ChannelName.Channel1) ' select channel
		RtChart.ChnDspWindow = 1 ' assign all channels to window 1
		RtChart.ChnDspViewport = 1 ' assign channel to a viewport
		RtChart.ChnDspPen = ChannelName.Channel1 ' specify pen, same value as channel number
		RtChart.ChnDspBufLen = (NumSamples) ' input buffer length for each channel
		'UPGRADE_WARNING: (1068) RtChart_CRT of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.ChnDspMode = ChannelDisplayMode.CRT ' per channel display mode, use RunMode constants
		'UPGRADE_WARNING: (1068) RtChart_Lines of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.ChnDspStyle = ChannelDisplayStyle.Lines ' use line style for plotting arrays
		RtChart.ChnDspOffsetX = 0 ' set offset to 0
		RtChart.ChnDspOffsetY = 0 ' set offset to 0

		' Describe horizontal dimension
		'UPGRADE_WARNING: (1068) RtChart_Horz of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.ChannelDataDimension = LogicalDimension.Horizontal
		'UPGRADE_WARNING: (1068) RtChart_AutoIncr of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.ChnDataShape = ChannelDataShape.AutoIncr
		RtChart.ChnDataOffset = 0 ' offset into buffer (in points)
		RtChart.ChnDataIncr = 1 ' logical increment to next value in array

		' Describe vertical dimension
		'UPGRADE_WARNING: (1068) RtChart_Vert of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.ChannelDataDimension = LogicalDimension.Vertical
		'UPGRADE_WARNING: (1068) RtChart_Scalar of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.ChnDataShape = ChannelDataShape.Scalar
		RtChart.ChnDataOffset = 0 ' location of first point in buffer
		RtChart.ChnDataIncr = 1 ' logical increment to next value for 1D array
		'UPGRADE_WARNING: (1068) RtChart_Single of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.ChnDataType = ChannelDataType.Single ' specify single precision data type

		'UPGRADE_WARNING: (1068) RtChart_ChnInitialize of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.ChnDspAction = ChannelDisplayAction.Initialize

	End Sub

	Sub InitializeRtChart(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl)
		Dim RtChart_Color16, RtChart_HorzVertGrid, RtChart_Inset, RtChart_MajorMinorAxesTics, RtChart_MajorMinorFrameTics, RtChart_NoAxes, RtChart_Raised As Object

		' Global operation properties
		RtChart.ImageFile = String.Empty ' specifies file name for Write action
		RtChart.HitTest = False ' enables mouse pointer "hit" detection
		RtChart.FramesPerSec = 0 ' 0 for asynchronous, -1 for synchronous

		' Border description properties
		RtChart.BorderColor = Color.LightGray
		RtChart.LightColor = Color.DarkGray
		RtChart.ShadowColor = Color.Black
		'UPGRADE_WARNING: (1068) RtChart_Inset of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.BevelInner = BevelStyle.Inset
		'UPGRADE_WARNING: (1068) RtChart_Raised of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.BevelOuter = BevelStyle.Raised
		RtChart.BevelWidthInner = ( 5)
		RtChart.BevelWidthOuter = ( 5)
		RtChart.BorderWidth = 3
		RtChart.Outline = True
		RtChart.OutlineColor = Color.Black
		RtChart.RoundedCorners = False

		' Graticule description properties
		RtChart.BackColor = Color.DarkCyan
		RtChart.AutoSize = ( True) ' true forces square border, frame is always square

		RtChart.FrameOn = True
		'UPGRADE_WARNING: (1068) RtChart_MajorMinorFrameTics of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.FrameTics = FrameTicsStyle.MajorMinorFrameTics
		RtChart.FrameColor = Color.White

		'UPGRADE_WARNING: (1068) RtChart_NoAxes of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.AxesType  = AxesType.NoAxes ' RtChart_HorzVertAxes
		'UPGRADE_WARNING: (1068) RtChart_MajorMinorAxesTics of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.AxesTics = AxesTicsStyle.MajorMinorAxesTics
		RtChart.AxesColor = Color.White

		RtChart.GridOn = False
		'UPGRADE_WARNING: (1068) RtChart_HorzVertGrid of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.GridType = GridType.HorizontalVertical
		RtChart.GridColor = Color.LightGray

		RtChart.MajorDivHorz = MAJOR_HORZ_DIV ' major horizontal divisions
		RtChart.MajorDivVert = MAJOR_VERT_DIV ' major vertical divisions
		RtChart.MinorDiv = MINOR_DIVISIONS ' minor divisions
		RtChart.MarginLeft = 2 ' left Graticule border
		RtChart.MarginTop = 2 ' top Graticule border
		RtChart.MarginRight = 2 ' right Graticule border
		RtChart.MarginBottom = 2 ' bottom Graticule border

		'UPGRADE_WARNING: (1068) RtChart_Color16 of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.ColorDepth = ColorDepth.Color16 '  16 colors

	End Sub

	Sub PlotData(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal numPoints As Integer, ByVal VBDataArray As Single())

		' RtChart exports ChartData function to plot data of selected channel
		result = RtChart.ChartData(1, NumPoints, VBDataArray, 0, 0, 0)
		' display result of chart data call
		If result > 0 Then
			' returned value is points charted
			SinglesForm.DefInstance.ChartDataResults.Text = "Number of points charted = " & result.ToString()
		Else
			' returned value is error code, convert to error string
			SinglesForm.DefInstance.ChartDataResults.Text = ChartPen.ChartErrorMessage(result)
		End If

	End Sub
End Module