Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic
Imports System
Imports System.Windows.Forms
Partial Friend Class SinglesForm
    Inherits System.Windows.Forms.Form

    ' LabOBJX Real-Time Chart - Example Code
    ' (C) Copyright 1995, Scientific Software Tools, Inc.
    ' All Rights Reserved.
    '

    Const TWO_PIE As Double = 2 * 3.1415926535898

    Dim NumSamples As Integer ' number of samples

    ' signal waveform limits (volts peak)
    Dim MaxVolts As Single
    Dim MinVolts As Single

    Public Sub New()
        MyBase.New()
        If _Instance Is Nothing Then
            If _InitializingInstance Then
                _Instance = Me
            Else
                Try
                    'For the start-up form, the first instance created is the default instance.
                    If Not (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint Is Nothing) AndAlso System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType Is Me.GetType() Then
                        _Instance = Me
                    End If

                Catch
                End Try
            End If
        End If
        'This call is required by the Windows Form Designer.
        InitializeComponent()
        If Not Me.DesignMode Then ReLoadForm(False)
    End Sub


    Private Sub Form_Load()

        ' initialize program's global variables
        InitializeGlobalVariables()

        ' initialize RtChart Pens
        ChartPen.InitChannelsPens(RtChart1)

        ' initialize RtChart global properties
        InitializeRtChart(RtChart1)

        ' initialize RtChart Pens
        ChartPen.InitChannelsPens(RtChart1)

        ' initialize RtChart for Volts vs Time plots,
        InitializeChannels(RtChart1, MinVolts, MaxVolts - MinVolts, NumSamples)

    End Sub

    Private Sub GenerateSineArraySingles(ByVal numSamples As Integer, ByRef VoltsPeak As Single)


        'redim array for NumSamples Single precision values
        ReDim VBDataArray(numSamples)

        Dim RadiansPerSample As Single = TWO_PIE / numSamples

        ' Generate random cycle times, ensure at least 1 radian per sample
        RadiansPerSample = (RadiansPerSample * VBMath.Rnd()) + RadiansPerSample

        For sample As Integer = 0 To numSamples - 1

            VBDataArray(sample) = VoltsPeak * Math.Sin(sample * RadiansPerSample)

        Next sample

    End Sub

    Private Sub InitializeGlobalVariables()

        'initialize variables

        NumSamples = 1000

        ' singles have 8 significant digits of precision
        MaxVolts = 1.2345678E+38
        MinVolts = -1.2345678E+38

    End Sub

    Private Sub Plot_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles Plot.Click

        ' generate array of Single precision values
        GenerateSineArraySingles(NumSamples, MaxVolts)

        ' calls ChartData function
        PlotData(RtChart1, NumSamples, VBDataArray)

    End Sub
    Private Sub Form_Closed(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MyBase.Closed
    End Sub
End Class