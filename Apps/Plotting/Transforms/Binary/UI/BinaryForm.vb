Option Strict Off
Option Explicit On
Imports System
Imports System.Windows.Forms
Partial Friend Class RTCPlotForm
	Inherits System.Windows.Forms.Form

	' LabOBJX Real-Time Chart - Example Code
	' (C) Copyright 1995, Scientific Software Tools, Inc.
	' All Rights Reserved.
	'

	Const TWO_PIE As Double = 2 * 3.1415926535898

	Dim NumSamples As Integer ' number of samples
	Dim NumChannels As Integer ' number of channels

	Public Sub New()
		MyBase.New()
		If _Instance Is Nothing Then
			If _InitializingInstance Then
				_Instance = Me
			Else
				Try
					'For the start-up form, the first instance created is the default instance.
					If Not (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint Is Nothing) AndAlso System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType Is Me.GetType() Then
						_Instance = Me
					End If

				Catch
				End Try
			End If
		End If
		'This call is required by the Windows Form Designer.
		InitializeComponent()
		ReLoadForm(False)
	End Sub


	Private Sub Form_Load()

		' initialize program's global variables
		InitializeGlobalVariables()

		' initialize RtChart Pens
		ChartPen.InitChannelsPens(RtChart1)

		' initialize RtChart global properties
		InitializeRtChart(RtChart1)

		' initialize RtChart Pens
		ChartPen.InitChannelsPens(RtChart1)

		' initialize RtChart for Volts vs Time plots
		InitializeChannels(RtChart1, NumChannels, NumSamples)

	End Sub

	Private Sub GenerateADCode()

		' generate A/D code array of values from 0-4095
		' shifted left 4 bits by multiplying values by 16
		' A/D codes >2047 when shifted are >32K, must use Two's Complement
		' negative representation and RtChart reads back as unsigned integers

		Dim ADCode As Integer
		ReDim VBDataArray(NumSamples)

		' values 0 to 2047 are shifted left 4 bits
		For i As Integer = 0 To 2047
			ADCode = i
			VBDataArray(i) = ADCode * 16
		Next i

		ADCode = -2048

		' values -2048 to -1 are shifted left 4 bits
		' these values are represented in two's complement
		For i As Integer = 2048 To 4095
			VBDataArray(i) = ADCode * 16
			ADCode += 1
		Next i

	End Sub

	Private Sub InitializeGlobalVariables()

		'initialize variables

		NumChannels = 1
		NumSamples = 4096

	End Sub

	Private Sub Single_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles Single_Renamed.Click

		' generate A/D code array of values
		' from 0-4095 shifted left 4 bits
		GenerateADCode()

		' calls ChartData function once
		PlotData(RtChart1, NumSamples, VBDataArray)

	End Sub
	Private Sub Form_Closed(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MyBase.Closed
	End Sub
End Class