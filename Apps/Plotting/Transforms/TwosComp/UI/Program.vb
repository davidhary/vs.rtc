Option Strict Off
Option Explicit On
Imports System
Imports isr.Visuals.RealTimeChart
Module Program
    ' LabOBJX Real-Time Chart - Example Code
    ' (C) Copyright 1995, Scientific Software Tools, Inc.
    ' All Rights Reserved.
    '
    Public Const AssemblyTitle As String = "Real Time Strip Chart Twos Complement Demo"
    Public Const AssemblyDescription As String = "Real Time Strip Chart Twos Complement Demo"
    Public Const AssemblyProduct As String = "Visuals.Real.Time.Chart.TwosComplement"


    Public result As Integer ' return value from function call

    Public VBDataArray() As Integer ' VB array to store data values

    Public Const VOLTS_PER_DIV As Integer = 5 ' vertical scaling is based on volts per division
    Public Const TIME_PER_DIV As Double = 409.6 ' horizontal scaling is based on samples per division

    ' grid constants
    Public Const MAJOR_HORZ_DIV As Integer = 10
    Public Const MAJOR_VERT_DIV As Integer = 8
    Public Const MINOR_DIVISIONS As Integer = 5

    Sub InitializeChannels(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal numChannels As Integer, ByVal samplesPerChannel As Integer)
        Dim RtChart_AutoIncr, RtChart_ChnInitialize, RtChart_CRT, RtChart_Horz, RtChart_Integer, RtChart_Lines, RtChart_Scalar, RtChart_Vert As Object

        ' Initialize RtChart Window, Viewports, & Channels in CRT mode, Line style.
        ' This function uses a window scaled to +/- 20v and uses a linear transform
        ' to map the A/D integer range of -2048 to 2047 into a voltage range of -10 to +10v


        ' View port Description
        rtChart.Viewports = 1 ' number of view ports displayed initially

        ' Window Description
        rtChart.LogicalWindow = 1 ' select window
        rtChart.WndWidth = (TIME_PER_DIV * MAJOR_HORZ_DIV) ' in Seconds (Time/div * 10 divisions) 10 * 10 = 100
        rtChart.WndXmin = 0 ' left minimum abscissa
        rtChart.WndHeight = (VOLTS_PER_DIV * MAJOR_VERT_DIV) ' voltage range
        rtChart.WndYmin = (rtChart.WndHeight / -2) ' bottom minimum ordinate, -1/2 of range

        ' Channel Description
        rtChart.LogicalChannel = (ChannelName.Channel1) ' select channel
        rtChart.ChnDspWindow = 1 ' assign channel to window 1
        rtChart.ChnDspViewport = 1 ' assign channel to viewport 1
        rtChart.ChnDspPen = ChannelName.Channel1 ' specify pen, same value as channel number
        rtChart.ChnDspBufLen = samplesPerChannel ' input buffer length
        rtChart.ChnDspMode = ChannelDisplayMode.CRT ' channel display mode
        rtChart.ChnDspStyle = ChannelDisplayStyle.Lines ' use line style for plotting arrays
        rtChart.ChnDspOffsetX = 0 ' set offset to 0
        rtChart.ChnDspOffsetY = 0 ' set offset to 0

        ' Describe horizontal dimension
        rtChart.ChannelDataDimension = LogicalDimension.Horizontal
        rtChart.ChnDataShape = ChannelDataShape.AutoIncr
        rtChart.ChnDataOffset = 0 ' offset into buffer (in points)
        rtChart.ChnDataIncr = 1 ' logical increment to next value in array

        ' Describe vertical dimension
        rtChart.ChannelDataDimension = LogicalDimension.Vertical
        rtChart.ChnDataShape = ChannelDataShape.Scalar
        'UPGRADE_WARNING: (1068) RtChart_Integer of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
        rtChart.ChnDataType = (ChannelDataType.Integer)) '
		rtChart.ChnDataOffset = 0 ' offset into buffer (in points)
        rtChart.ChnDataIncr = 1 ' logical increment to next value
        rtChart.ChnDataXform = (1) ' specify transform object #1

        ' Describe transform #1                   ' transform -2048 to 2047 into -10 to +10 volt range
        rtChart.ChannelTransformation = (1) ' select transform #1
        rtChart.ChnXformGain = (20 / 4096) ' transform 4096 count range to 20 volt range
        rtChart.ChnXformOffset = (0) ' output range is -10 to +10, no offset
        rtChart.ChnXformMSB = (15) ' must specify bit mask, all 16 bits used
        rtChart.ChnXformLSD = (0) ' must specify bit mask, all 16 bits used

        rtChart.ChnDspAction = ChannelDisplayAction.Initialize

    End Sub

    Sub InitializeRtChart(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl)

        ' Global operation properties
        rtChart.HitTest = False ' enables mouse pointer "hit" detection
        rtChart.FramesPerSec = 0 ' 0 for asynchronous, -1 for synchronous

        ' Border description properties
        rtChart.BorderColor = Color.LightGray
        rtChart.LightColor = Color.DarkGray
        rtChart.ShadowColor = Color.Black
        rtChart.BevelInner = BevelStyle.Inset
        rtChart.BevelOuter = BevelStyle.Raised
        rtChart.BevelWidthInner = 5
        rtChart.BevelWidthOuter = 5
        rtChart.BorderWidth = 3
        rtChart.Outline = True
        rtChart.OutlineColor = Color.Black
        rtChart.RoundedCorners = False

        ' Graticule description properties
        rtChart.BackColor = Color.DarkCyan
        rtChart.AutoSize = True ' true forces square border, frame is always square

        rtChart.FrameOn = True
        rtChart.FrameTics = FrameTicsStyle.MajorMinorFrameTics
        rtChart.FrameColor = Color.White

        rtChart.AxesType = AxesType.HorizontalVertical
        rtChart.AxesTics = AxesTicsStyle.MajorMinorAxesTics
        rtChart.AxesColor = Color.White

        rtChart.GridOn = False
        rtChart.GridType = GridType.HorizontalVertical
        rtChart.GridColor = Color.LightGray

        rtChart.MajorDivHorz = MAJOR_HORZ_DIV ' major horizontal divisions
        rtChart.MajorDivVert = MAJOR_VERT_DIV ' major vertical divisions
        rtChart.MinorDiv = MINOR_DIVISIONS ' minor divisions
        rtChart.MarginLeft = 2 ' left Graticule border
        rtChart.MarginTop = 2 ' top Graticule border
        rtChart.MarginRight = 2 ' right Graticule border
        rtChart.MarginBottom = 2 ' bottom Graticule border

        rtChart.ColorDepth = ColorDepth.Color16 '  16 colors

    End Sub

    Sub PlotData(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal numPoints As Integer, ByVal VBDataArray() As Integer)

        ' RtChart exports ChartData function to plot data of selected channel
        result = rtChart.ChartData(ChannelName.Channel1, NumPoints, VBDataArray, 0, 0, 0)

        ' display result of chart data call
        If result > 0 Then
            ' returned value is points charted
            TwosComplementForm.DefInstance.ChartDataResults.Text = "Number of points charted = " & result.ToString()
        Else
            ' returned value is error code, convert to error string
            TwosComplementForm.DefInstance.ChartDataResults.Text = ChartPen.ChartErrorMessage(result)
        End If

    End Sub
End Module