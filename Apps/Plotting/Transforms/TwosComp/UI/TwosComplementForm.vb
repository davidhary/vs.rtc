Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic
Imports System
Imports System.Windows.Forms
Partial Friend Class TwosComplementForm
    Inherits System.Windows.Forms.Form

    ' LabOBJX Real-Time Chart - Example Code
    ' (C) Copyright 1995, Scientific Software Tools, Inc.
    ' All Rights Reserved.
    '

    Const TWO_PIE As Double = 2 * 3.1415926535898

    Dim NumSamples As Integer ' number of samples
    Dim NumChannels As Integer ' number of channels

    ' signal waveform limits (volts peak)
    Dim MaxInteger As Integer
    Dim MinInteger As Integer

    Public Sub New()
        MyBase.New()
        If _Instance Is Nothing Then
            If _InitializingInstance Then
                _Instance = Me
            Else
                Try
                    'For the start-up form, the first instance created is the default instance.
                    If Not (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint Is Nothing) AndAlso System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType Is Me.GetType() Then
                        _Instance = Me
                    End If

                Catch
                End Try
            End If
        End If
        'This call is required by the Windows Form Designer.
        InitializeComponent()
        If Not Me.DesignMode Then ReLoadForm(False)
    End Sub


    Private Sub Form_Load()

        ' initialize program's global variables
        InitializeGlobalVariables()

        ' initialize RtChart Pens
        ChartPen.InitChannelsPens(RtChart1)

        ' initialize RtChart global properties
        InitializeRtChart(RtChart1)

        ' initialize RtChart Pens
        ChartPen.InitChannelsPens(RtChart1)

        ' initialize RtChart for Volts vs Time plots
        InitializeChannels(RtChart1, NumChannels, NumSamples)

    End Sub

    Private Sub GenerateIntegers(ByRef MaxInteger As Integer, ByRef MinInteger As Integer)

        ' generate integer array of values from -2048 to 2047


        ReDim VBDataArray(NumSamples)

        For i As Integer = MinInteger To MaxInteger
            VBDataArray(i + 2048) = i ' this array is 0-based
        Next i

    End Sub

    'UPGRADE_NOTE: (7001) The following declaration (GenerateSineWaveArray) seems to be dead code More Information: https://www.mobilize.net/vbtonet/ewis/ewi7001
    'Private Sub GenerateSineWaveArray(ByVal numSamples As Integer, ByRef MaxInteger As Integer)
    '
    '
    ''ReDim VBDataArray(NumSamples)
    '
    'Dim RadiansPerSample As Single = TWO_PIE / NumSamples
    '
    ' Generate random cycle times, ensure at least 1 radian per sample
    'Dim CycleTime As Single = (RadiansPerSample * VBMath.Rnd()) + RadiansPerSample
    '
    'For 'sample As Integer = 0 To NumSamples - 1
    'VBDataArray(sample) = MaxInteger * Math.Sin(sample * CycleTime)
    'Next sample
    '
    'End Sub

    Private Sub InitializeGlobalVariables()

        'initialize variables

        NumChannels = 1
        NumSamples = 4096

        MaxInteger = 2047
        MinInteger = -2048

    End Sub

    Private Sub Single_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles Single_Renamed.Click

        ' generate array of values from -2048 to 2047
        GenerateIntegers(MaxInteger, MinInteger)

        ' generate sine wave array of values from -2047 to 2047
        'GenerateSineWaveArray NumSamples, MaxInteger

        ' calls ChartData function once
        PlotData(RtChart1, NumSamples, VBDataArray)

    End Sub
    Private Sub Form_Closed(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MyBase.Closed
    End Sub
End Class