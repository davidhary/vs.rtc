Option Strict Off
Option Explicit On
Imports System
Imports isr.Visuals.RealTimeChart

Friend Module Program
    ' LabOBJX Real-Time Chart - Example Code
    ' (C) Copyright 1995, Scientific Software Tools, Inc.
    ' All Rights Reserved.
    '
    Public Const AssemblyTitle As String = "Real Time Strip Chart Transform Demo"
    Public Const AssemblyDescription As String = "Real Time Strip Chart Transform Demo"
    Public Const AssemblyProduct As String = "Visuals.Real.Time.Chart.Transform"

    <CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>")>
    Public Result As Integer ' return value from function call

    <CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification:="<Pending>")>
    Public VBDataArray() As Single ' VB array to store voltage data values

    Public Const DEGREES_PER_DIV As Integer = 100 ' vertical scaling is based on degrees per division
    Public Const TIME_PER_DIV As Integer = 10 ' horizontal scaling is based on samples per division

    ' grid constants
    Public Const MAJOR_HORZ_DIV As Integer = 10
    Public Const MAJOR_VERT_DIV As Integer = 10
    Public Const MINOR_DIVISIONS As Integer = 5

    Public Sub DisplayAbout()
        Dim message As String = $"Uses a window scaled to +/- {(DEGREES_PER_DIV * MAJOR_VERT_DIV) / 2} degrees and uses a linear transform to map the voltage range of 0 to 5v into a temperature range of -100 to +400 degrees"
        System.Windows.Forms.MessageBox.Show(message, "About this demo", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    Public Sub InitializeChannels(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal numChannels As Integer, ByVal samplesPerChannel As Integer)

        ' Initialize RtChart Window, Viewports, & Channels in CRT mode, Line style.
        ' This function uses a window scaled to +/- 500 degrees and uses a linear transform
        ' to map the voltage range of 0 to 5v into a temperature range of -100 to +400 degrees

        ' View port Description
        rtChart.Viewports = 1 ' number of view ports displayed initially

        ' Window Description
        rtChart.LogicalWindow = 1 ' select window
        rtChart.WndWidth = TIME_PER_DIV * MAJOR_HORZ_DIV ' in Seconds (Time/div * 10 divisions) 10 * 10 = 100
        rtChart.WndXmin = 0 ' left minimum abscissa
        rtChart.WndHeight = DEGREES_PER_DIV * MAJOR_VERT_DIV ' temperature range in degrees
        rtChart.WndYmin = rtChart.WndHeight / -2 ' bottom minimum ordinate, -1/2 of range

        ' Channel Description
        rtChart.LogicalChannel = (ChannelName.Channel1) ' select channel
        rtChart.ChnDspWindow = 1 ' assign channel to window 1
        rtChart.ChnDspViewport = 1 ' assign channel to viewport 1
        rtChart.ChnDspPen = ChannelName.Channel1 ' specify pen, same value as channel number
        rtChart.ChnDspBufLen = samplesPerChannel ' input buffer length
        rtChart.ChnDspMode = ChannelDisplayMode.CRT ' channel display mode
        rtChart.ChnDspStyle = ChannelDisplayStyle.Lines ' use line style for plotting arrays
        rtChart.ChnDspOffsetX = 0 ' set offset to 0
        rtChart.ChnDspOffsetY = 0 ' set offset to 0

        ' Describe horizontal dimension
        rtChart.ChannelDataDimension = LogicalDimension.Horizontal
        rtChart.ChnDataShape = ChannelDataShape.AutoIncr
        rtChart.ChnDataOffset = 0 ' offset into buffer (in points)
        rtChart.ChnDataIncr = 1 ' logical increment to next value in array

        ' Describe vertical dimension
        rtChart.ChannelDataDimension = LogicalDimension.Vertical
        rtChart.ChnDataShape = ChannelDataShape.Scalar
        rtChart.ChnDataType = ChannelDataType.Single
        rtChart.ChnDataOffset = 0 ' offset into buffer (in points)
        rtChart.ChnDataIncr = 1 ' logical increment to next value for 2D array
        rtChart.ChnDataXform = (1) ' specify transform object #1

        ' Describe Transform #1                   ' transform is 0 to +5v to -100 to +400 degrees
        rtChart.ChannelTransformation = (1) ' select transform #1
        rtChart.ChnXformGain = (100) ' transform +5v range to 500 degree range
        rtChart.ChnXformOffset = (-100) ' -100 degree offset in output (plotted data)
        rtChart.ChnXformOffset = (-100) ' -100 degree offset in output (plotted data)

        rtChart.ChnDspAction = ChannelDisplayAction.Initialize

    End Sub

    Public Sub InitializeRtChart(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl)

        ' Global operation properties
        rtChart.ImageFile = String.Empty ' specifies file name for Write action
        rtChart.HitTest = False ' enables mouse pointer "hit" detection
        rtChart.FramesPerSec = 0 ' 0 for asynchronous, -1 for synchronous

        ' Border description properties
        rtChart.BorderColor = Color.LightGray
        rtChart.LightColor = Color.DarkGray
        rtChart.ShadowColor = Color.Black
        rtChart.BevelInner = BevelStyle.Inset
        rtChart.BevelOuter = BevelStyle.Raised
        rtChart.BevelWidthInner = 5
        rtChart.BevelWidthOuter = 5
        rtChart.BorderWidth = 3
        rtChart.Outline = True
        rtChart.OutlineColor = Color.Black
        rtChart.RoundedCorners = False

        ' Graticule description properties
        rtChart.BackColor = Color.DarkCyan
        rtChart.AutoSize = True ' true forces square border, frame is always square

        rtChart.FrameOn = True
        rtChart.FrameTics = FrameTicsStyle.MajorMinorFrameTics
        rtChart.FrameColor = Color.White

        rtChart.AxesType = AxesType.HorizontalVertical
        rtChart.AxesTics = AxesTicsStyle.MajorMinorAxesTics
        rtChart.AxesColor = Color.White

        rtChart.GridOn = False
        rtChart.GridType = GridType.HorizontalVertical
        rtChart.GridColor = Color.LightGray

        rtChart.MajorDivHorz = MAJOR_HORZ_DIV ' major horizontal divisions
        rtChart.MajorDivVert = MAJOR_VERT_DIV ' major vertical divisions
        rtChart.MinorDiv = MINOR_DIVISIONS ' minor divisions
        rtChart.MarginLeft = 2 ' left Graticule border
        rtChart.MarginTop = 2 ' top Graticule border
        rtChart.MarginRight = 2 ' right Graticule border
        rtChart.MarginBottom = 2 ' bottom Graticule border

        rtChart.ColorDepth = ColorDepth.Color16 '  16 colors

    End Sub

End Module
