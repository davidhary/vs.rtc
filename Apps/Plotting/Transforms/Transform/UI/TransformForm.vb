Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic
Imports System
Imports System.Windows.Forms
Partial Friend Class TransformForm
    Inherits System.Windows.Forms.Form

    ' LabOBJX Real-Time Chart - Example Code
    ' (C) Copyright 1995, Scientific Software Tools, Inc.
    ' All Rights Reserved.
    '

#Region " CONSTRUCTION and CLEANUP "

    Private ReadOnly _IsInitializingComponent As Boolean
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub New()
        MyBase.New()
        If _Instance Is Nothing Then
            If _InitializingInstance Then
                _Instance = Me
            Else
                Try
                    'For the start-up form, the first instance created is the default instance.
                    If Not (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint Is Nothing) AndAlso System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType Is Me.GetType() Then
                        _Instance = Me
                    End If

                Catch
                End Try
            End If
        End If
        _IsInitializingComponent = True
        'This call is required by the Windows Form Designer.
        InitializeComponent()
        _IsInitializingComponent = False
        If Not Me.DesignMode Then ReLoadForm(False)
    End Sub

    Private Shared _Instance As TransformForm
    Private Shared _InitializingInstance As Boolean
    Public Shared Property DefInstance() As TransformForm
        Get
            If _Instance Is Nothing OrElse _Instance.IsDisposed Then
                _InitializingInstance = True
                _Instance = CreateInstance()
                _InitializingInstance = False
            End If
            Return _Instance
        End Get
        Set(ByVal value As TransformForm)
            _Instance = Value
        End Set
    End Property
    Public Shared Function CreateInstance() As TransformForm
        Dim theInstance As New TransformForm()
        theInstance.ResetKnownState()
        Return theInstance
    End Function

    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    Public Sub ReLoadForm(ByVal addEvents As Boolean)
        ResetKnownState()
    End Sub

#End Region

#Region " FORM AND CONTROL METHODS "

    ''' <summary> Gets or sets the chart results. </summary>
    ''' <value> The chart results. </value>
    Private Property ChartResults As String
        Get
            Return Me._ChartDataResultsTextBox.Text
        End Get
        Set(value As String)
            Me._ChartDataResultsTextBox.Text = value
        End Set
    End Property

    Private Const _TWO_PIE As Double = 2 * 3.1415926535898

    Private _NumSamples As Integer ' number of samples
    Private _NumChannels As Integer ' number of channels

    ' signal waveform limits (volts peak)
    Private _VoltsPeak As Single
    Private _VoltsOffset As Single


    Private Sub ResetKnownState()

        ' initialize program's global variables
        InitializeGlobalVariables()

        ' initialize RtChart Pens
        ChartPen.InitChannelsPens(RtChart1)

        ' initialize RtChart global properties
        InitializeRtChart(RtChart1)

        ' initialize RtChart Pens
        ChartPen.InitChannelsPens(RtChart1)

        ' initialize RtChart for Volts vs Time plots
        InitializeChannels(RtChart1, _NumChannels, _NumSamples)


    End Sub

    Private Sub GenerateSineWaveArray(ByVal numSamples As Integer, ByRef voltsPeak As Single, ByRef voltsOffset As Single)


        ReDim VBDataArray(numSamples)

        Dim RadiansPerSample As Single = _TWO_PIE / numSamples

        ' Generate random cycle times, ensure at least 1 radian per sample
        Dim CycleTime As Single = (RadiansPerSample * VBMath.Rnd()) + RadiansPerSample

        For sample As Integer = 0 To numSamples - 1
            VBDataArray(sample) = voltsOffset + voltsPeak * Math.Sin(sample * CycleTime)
        Next sample

    End Sub

    Private Sub InitializeGlobalVariables()

        'initialize variables

        _NumChannels = 1
        _NumSamples = 100

        _VoltsPeak = 2.5
        _VoltsOffset = 2.5

    End Sub

    Private Sub SingleButton_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles _SingleButton.Click

        ' generate array of voltages 0 to +5v
        GenerateSineWaveArray(_NumSamples, _VoltsPeak, _VoltsOffset)

        ' calls ChartData function once
        Me.PlotData(RtChart1, _NumSamples, VBDataArray)

    End Sub

    Public Function PlotData(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal numPoints As Integer, ByVal vbDataArray As Single()) As Integer

        ' RtChart exports ChartData function to plot data of selected channel
        Result = rtChart.ChartData(ChannelName.Channel1, numPoints, vbDataArray)

        ' display result of chart data call
        If Result > 0 Then
            ' returned value is points charted
            Me.ChartResults = $"Number of points charted = {Result}"
        Else
            ' returned value is error code, convert to error string
            Me.ChartResults = ChartPen.ChartErrorMessage(Result)
        End If

        Return Result

    End Function

#End Region

End Class
