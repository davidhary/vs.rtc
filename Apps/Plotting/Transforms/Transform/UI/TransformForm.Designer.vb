<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TransformForm

    Private visualControls() As String = New String() {"components", "ToolTipMain", "Single_Renamed", "ChartDataResults", "RtChart1"}
    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Public _ToolTip As System.Windows.Forms.ToolTip
    Public WithEvents _SingleButton As System.Windows.Forms.Button
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._SingleButton = New System.Windows.Forms.Button()
        Me._ChartDataResultsTextBox = New System.Windows.Forms.TextBox()
        Me.RtChart1 = New isr.Visuals.RealTimeChart.RealTimeChartControl()
        Me.SuspendLayout()
        '
        '_SingleButton
        '
        Me._SingleButton.AllowDrop = True
        Me._SingleButton.BackColor = System.Drawing.SystemColors.Control
        Me._SingleButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._SingleButton.Location = New System.Drawing.Point(117, 257)
        Me._SingleButton.Name = "_SingleButton"
        Me._SingleButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._SingleButton.Size = New System.Drawing.Size(89, 29)
        Me._SingleButton.TabIndex = 1
        Me._SingleButton.Text = "Single"
        Me._SingleButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me._SingleButton.UseVisualStyleBackColor = False
        '
        '_ChartDataResultsTextBox
        '
        Me._ChartDataResultsTextBox.AcceptsReturn = True
        Me._ChartDataResultsTextBox.AllowDrop = True
        Me._ChartDataResultsTextBox.BackColor = System.Drawing.SystemColors.ControlLight
        Me._ChartDataResultsTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._ChartDataResultsTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._ChartDataResultsTextBox.Dock = System.Windows.Forms.DockStyle.Top
        Me._ChartDataResultsTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._ChartDataResultsTextBox.Location = New System.Drawing.Point(0, 225)
        Me._ChartDataResultsTextBox.MaxLength = 0
        Me._ChartDataResultsTextBox.Name = "_ChartDataResultsTextBox"
        Me._ChartDataResultsTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ChartDataResultsTextBox.Size = New System.Drawing.Size(323, 19)
        Me._ChartDataResultsTextBox.TabIndex = 0
        '
        'RtChart1
        '
        Me.RtChart1.ChnXformGain = 1.0R
        Me.RtChart1.ChnXformLSB = -1
        Me.RtChart1.ChnXformMSB = -1
        Me.RtChart1.ChnXformOffset = 0R
        Me.RtChart1.Dock = System.Windows.Forms.DockStyle.Top
        Me.RtChart1.Location = New System.Drawing.Point(0, 0)
        Me.RtChart1.Name = "RtChart1"
        Me.RtChart1.Size = New System.Drawing.Size(323, 225)
        Me.RtChart1.TabIndex = 2
        '
        'TransformForm
        '
        Me.AllowDrop = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(323, 294)
        Me.Controls.Add(Me._SingleButton)
        Me.Controls.Add(Me._ChartDataResultsTextBox)
        Me.Controls.Add(Me.RtChart1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.SystemColors.WindowText
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Location = New System.Drawing.Point(200, 159)
        Me.MaximizeBox = False
        Me.Name = "TransformForm"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "RT Chart - Volts to Temp Transform "
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Private WithEvents _ChartDataResultsTextBox As TextBox
    Private WithEvents RtChart1 As RealTimeChartControl
End Class