Option Strict Off
Option Explicit On
Imports System
Imports isr.Visuals.RealTimeChart
Module ONEDIM1
    ' LabOBJX Real-Time Chart - Example Code
    ' (C) Copyright 1995, Scientific Software Tools, Inc.
    ' All Rights Reserved.
    '

    Public Const AssemblyTitle As String = "Real Time Strip Chart 2 Demo"
    Public Const AssemblyDescription As String = "Real Time Strip Chart 2 Demo"
    Public Const AssemblyProduct As String = "Visuals.Real.Time.Chart.StripChart2"


    Public result As Integer ' return value from function call
	Public VBDataArray() As Single ' VB array to store multi-channel voltage data values

	Public Const VOLTS_PER_DIV As Integer = 5 ' vertical scaling is based on volts per division
	Public Const TIME_PER_DIV As Integer = 10 ' horizontal scaling is based on seconds per division

	' grid constants
	Public Const MAJOR_HORZ_DIV As Integer = 10
	Public Const MAJOR_VERT_DIV As Integer = 8
	Public Const MINOR_DIVISIONS As Integer = 5

	Sub InitializeChannels(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal numChannels As Integer, ByVal samplesPerChannel As Integer)
		Dim RtChart_AutoIncr, RtChart_ChnInitialize, RtChart_CRT, RtChart_Horz, RtChart_Lines, RtChart_Scalar, RtChart_Single, RtChart_Vert As Object

		' Initialize RtChart Window, Viewports, & Channels in CRT mode, Line style.
		' Setup channel properties for interleaved data array using
		' ChnDataOffset = (0-based channel)
		' ChnDataIncr = number of channels


		' View port Description
		RtChart.Viewports = 4 ' number of view ports displayed initially
		RtChart.ViewportStorageColor  = Color.Green ' storage mode plot color, same for all view ports

		' initialize storage mode for each view port
		For viewport As Integer = 1 To rtChart.Viewports
			RtChart.LogicalViewport = (viewport) ' select view port
			RtChart.ViewportStorageOn = False ' disable storage mode
		Next viewport

		' Window Description
		RtChart.LogicalWindow = 1 ' select window
		RtChart.WndXmin = 0 ' left minimum abscissa
		RtChart.WndWidth = (TIME_PER_DIV * MAJOR_HORZ_DIV) ' in Seconds (Time/div * 10 divisions) 10 * 10 = 100
		RtChart.WndHeight = (VOLTS_PER_DIV * MAJOR_VERT_DIV) ' in Volts (Volts/div * 8 divisions) 5 * 8 = 40
		RtChart.WndYmin = (RtChart.WndHeight/ -2) ' bottom minimum ordinate (negative full scale) 40 / -2 = -20

		' initialize all channels to be used
		For channel As Integer = ChannelName.Channel1 To ChannelName.Channel4

			' Channel Description
			RtChart.LogicalChannel = Channel ' select channel
			RtChart.ChnDspWindow = 1 ' assign all channels to window 1
			RtChart.ChnDspViewport = channel ' assign each channel to a viewport
			RtChart.ChnDspPen = channel ' specify pen, use same "name" (value) as channel
			RtChart.ChnDspBufLen = samplesPerChannel ' input buffer length for each channel
			'UPGRADE_WARNING: (1068) RtChart_CRT of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
			RtChart.ChnDspMode = ChannelDisplayMode.CRT ' per channel display mode, use RunMode constants
			'UPGRADE_WARNING: (1068) RtChart_Lines of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
			RtChart.ChnDspStyle = ChannelDisplayStyle.Lines ' use line style for plotting arrays
			RtChart.ChnDspOffsetX = 0 ' set offset to 0
			RtChart.ChnDspOffsetY = 0 ' set offset to 0

			' Describe horizontal dimension
			'UPGRADE_WARNING: (1068) RtChart_Horz of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
			RtChart.ChannelDataDimension = LogicalDimension.Horizontal
			'UPGRADE_WARNING: (1068) RtChart_AutoIncr of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
			RtChart.ChnDataShape = ChannelDataShape.AutoIncr
			RtChart.ChnDataOffset = 0 ' offset into buffer (in points)
			RtChart.ChnDataIncr = 1 ' logical increment to next value in array

			' Describe vertical dimension
			'UPGRADE_WARNING: (1068) RtChart_Vert of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
			RtChart.ChannelDataDimension = LogicalDimension.Vertical
			'UPGRADE_WARNING: (1068) RtChart_Scalar of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
			RtChart.ChnDataShape = ChannelDataShape.Scalar
			'UPGRADE_WARNING: (1068) RtChart_Single of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
			RtChart.ChnDataType = ChannelDataType.Single
			RtChart.ChnDataOffset = (channel - 1) ' offset into buffer for 1st value of each channel
			RtChart.ChnDataIncr = (NumChannels) ' logical increment to next value for 1D array

			'UPGRADE_WARNING: (1068) RtChart_ChnInitialize of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
			RtChart.ChnDspAction = ChannelDisplayAction.Initialize

		Next channel

	End Sub

	Sub InitializeRtChart(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl)
		Dim RtChart_Color16, RtChart_HorzVertGrid, RtChart_Inset, RtChart_MajorMinorAxesTics, RtChart_MajorMinorFrameTics, RtChart_NoAxes, RtChart_Raised As Object

		' Global operation properties
		'RtChart.ImageFile = String.Empty            ' specifies file name for Write action
		RtChart.HitTest = False ' enables mouse pointer "hit" detection
		RtChart.FramesPerSec = 0 ' 0 for asynchronous, -1 for synchronous

		' Border description properties
		RtChart.BorderColor = Color.LightGray
		RtChart.LightColor = Color.DarkGray
		RtChart.ShadowColor = Color.Black
		'UPGRADE_WARNING: (1068) RtChart_Inset of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.BevelInner = BevelStyle.Inset
		'UPGRADE_WARNING: (1068) RtChart_Raised of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.BevelOuter = BevelStyle.Raised
		RtChart.BevelWidthInner = ( 5)
		RtChart.BevelWidthOuter = ( 5)
		RtChart.BorderWidth = 3
		RtChart.Outline = True
		RtChart.OutlineColor = Color.Black
		RtChart.RoundedCorners = False

		' Graticule description properties
		RtChart.BackColor = Color.DarkCyan
		RtChart.AutoSize = ( True) ' true forces square border, frame is always square

		RtChart.FrameOn = True
		'UPGRADE_WARNING: (1068) RtChart_MajorMinorFrameTics of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.FrameTics = FrameTicsStyle.MajorMinorFrameTics
		RtChart.FrameColor = Color.White

		'UPGRADE_WARNING: (1068) RtChart_NoAxes of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.AxesType  = AxesType.NoAxes ' RtChart_HorzVertAxes
		'UPGRADE_WARNING: (1068) RtChart_MajorMinorAxesTics of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.AxesTics = AxesTicsStyle.MajorMinorAxesTics
		RtChart.AxesColor = Color.White

		RtChart.GridOn = False
		'UPGRADE_WARNING: (1068) RtChart_HorzVertGrid of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.GridType = GridType.HorizontalVertical
		RtChart.GridColor = Color.LightGray

		RtChart.MajorDivHorz = MAJOR_HORZ_DIV ' major horizontal divisions
		RtChart.MajorDivVert = MAJOR_VERT_DIV ' major vertical divisions
		RtChart.MinorDiv = MINOR_DIVISIONS ' minor divisions
		RtChart.MarginLeft = 2 ' left Graticule border
		RtChart.MarginTop = 2 ' top Graticule border
		RtChart.MarginRight = 2 ' right Graticule border
		RtChart.MarginBottom = 2 ' bottom Graticule border

		'UPGRADE_WARNING: (1068) RtChart_Color16 of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.ColorDepth = ColorDepth.Color16 '  16 colors

	End Sub

	Sub PlotData(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal numChannels As Integer, ByVal numPoints As Integer, ByRef FirstPoint As Double, ByVal VBDataArray As Single())

		' RtChart exports ChartData function to plot data of selected channel
		' ChnDataOffset and ChnDataIncr supply 1st point and "step" value for 1D (interleaved) array


		RtChart.FramesPerSec = -1 ' synchronous mode - control returns after paint event occurs

		For channel As Integer = ChannelName.Channel1 To NumChannels
			result = RtChart.ChartData(channel, NumPoints, VBDataArray, 0, 0, 0)
		Next channel

		RtChart.FramesPerSec = 0 ' asynchronous mode - control returns immediately after ChartData calls

		' display result of chart data call
		If result >= 0 Then
			' returned value is points charted
			OneDimForm.DefInstance.ChartDataResults.Text = "Number of points charted = " & result.ToString()
		Else
			' returned value is error code, convert to error string
			OneDimForm.DefInstance.ChartDataResults.Text = ChartPen.ChartErrorMessage(result)
		End If

	End Sub
End Module