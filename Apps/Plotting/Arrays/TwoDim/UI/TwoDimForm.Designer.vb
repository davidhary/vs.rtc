<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TwoDimForm
#Region "Upgrade Support "
	Private Shared _Instance As TwoDimForm
	Private Shared _InitializingInstance As Boolean
	Public Shared Property DefInstance() As TwoDimForm
		Get
			If _Instance Is Nothing OrElse _Instance.IsDisposed Then
				_InitializingInstance = True
				_Instance = CreateInstance()
				_InitializingInstance = False
			End If
			Return _Instance
		End Get
		Set(ByVal value As TwoDimForm)
			_Instance = Value
		End Set
	End Property
#End Region
#Region "Windows Form Designer generated code "
	Public Shared Function CreateInstance() As TwoDimForm
		Dim theInstance As New TwoDimForm()
		theInstance.Form_Load()
		Return theInstance
	End Function
	Private visualControls() As String = New String() {"components", "ToolTipMain", "Single_Renamed", "ChartDataResults", "RtChart1"}
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTipMain As System.Windows.Forms.ToolTip
	Public WithEvents Single_Renamed As System.Windows.Forms.Button
	Public WithEvents ChartDataResults As System.Windows.Forms.TextBox
	Public WithEvents RtChart1 As isr.Visuals.RealTimeChart.RealTimeChartControl
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> _
	 Private Sub InitializeComponent()
		Me.components = New System.ComponentModel.Container()
		Dim resources As New System.ComponentModel.ComponentResourceManager(GetType(TwoDimForm))
		Me.ToolTipMain = New System.Windows.Forms.ToolTip(Me.components)
		Me.Single_Renamed = New System.Windows.Forms.Button()
		Me.ChartDataResults = New System.Windows.Forms.TextBox()
		Me.RtChart1 = New isr.Visuals.RealTimeChart.RealTimeChartControl()
		Me.SuspendLayout()
		' 
		'Single_Renamed
		' 
		Me.Single_Renamed.AllowDrop = True
		Me.Single_Renamed.BackColor = System.Drawing.SystemColors.Control
		Me.Single_Renamed.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Single_Renamed.Location = New System.Drawing.Point(140, 352)
		Me.Single_Renamed.Name = "Single_Renamed"
		Me.Single_Renamed.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Single_Renamed.Size = New System.Drawing.Size(89, 29)
		Me.Single_Renamed.TabIndex = 1
		Me.Single_Renamed.Text = "Single"
		Me.Single_Renamed.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
		Me.Single_Renamed.UseVisualStyleBackColor = False
		' 
		'ChartDataResults
		' 
		Me.ChartDataResults.AcceptsReturn = True
		Me.ChartDataResults.AllowDrop = True
		Me.ChartDataResults.BackColor = System.Drawing.SystemColors.Window
		Me.ChartDataResults.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.ChartDataResults.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.ChartDataResults.ForeColor = System.Drawing.SystemColors.WindowText
		Me.ChartDataResults.Location = New System.Drawing.Point(68, 320)
		Me.ChartDataResults.MaxLength = 0
		Me.ChartDataResults.Name = "ChartDataResults"
		Me.ChartDataResults.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ChartDataResults.Size = New System.Drawing.Size(245, 21)
		Me.ChartDataResults.TabIndex = 0
		' 
		'RtChart1
		' 
		Me.RtChart1.AutoSize = 0
		Me.RtChart1.AxesColor = 16776960
		Me.RtChart1.AxesTics = 0
		Me.RtChart1.AxesType = 0
		Me.RtChart1.BackColor = 8421376
		Me.RtChart1.BevelInner = 0
		Me.RtChart1.BevelOuter = 2
		Me.RtChart1.BevelWidth_Inner = 1
		Me.RtChart1.BevelWidth_Outer = 1
		Me.RtChart1.BorderColor = (-2147483633)
		Me.RtChart1.BorderWidth = 3
		Me.RtChart1.Chn_Select = 1
		Me.RtChart1.ChnData_Dimension = 1
		Me.RtChart1.ChnDspUserData = 0
		Me.RtChart1.ChnXform_Select = 1
		Me.RtChart1.ColorDepth = 0
		Me.RtChart1.ErrBase = 30200
		Me.RtChart1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.RtChart1.ForeColor = (-842150451)
		Me.RtChart1.FrameColor = 16776960
		Me.RtChart1.FrameOn = -1
		Me.RtChart1.FramesPerSec = 0
		Me.RtChart1.FrameTics = 0
		Me.RtChart1.GridColor = 8421504
		Me.RtChart1.GridOn = -1
		Me.RtChart1.GridSymmetry = 0
		Me.RtChart1.GridType = 0
		Me.RtChart1.HitTest = 0
		Me.RtChart1.ImageFile = String.Empty
		Me.RtChart1.LightColor = (-2147483634)
		Me.RtChart1.Location = New System.Drawing.Point(8, 8)
		Me.RtChart1.MajorDivHorz = 10
		Me.RtChart1.MajorDivVert = 8
		Me.RtChart1.MarginBottom = 10
		Me.RtChart1.MarginLeft = 10
		Me.RtChart1.MarginRight = 10
		Me.RtChart1.MarginTop = 10
		Me.RtChart1.MinorDiv = 5
		Me.RtChart1.MouseIcon = 0
		Me.RtChart1.Name = "RtChart1"
		Me.RtChart1.Outline = -1
		Me.RtChart1.OutlineColor = (-2147483642)
		Me.RtChart1.Pen_Select = 1
		Me.RtChart1.RoundedCorners = 0
		Me.RtChart1.Scale_Select = 1
		Me.RtChart1.ShadowColor = (-2147483632)
		Me.RtChart1.Size = New System.Drawing.Size(353, 289)
		Me.RtChart1.TabIndex = 2
		Me.RtChart1.Viewport_Select = 1
		Me.RtChart1.Viewports = 1
		Me.RtChart1.ViewportStorageColor = 12632256
		Me.RtChart1.ViewportStorageOn = 0
		Me.RtChart1.Wnd_Select = 1
		' 
		'RTCPlotForm
		' 
		Me.AllowDrop = True
		Me.AutoScaleDimensions = New System.Drawing.SizeF(7, 13)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Window
		Me.ClientSize = New System.Drawing.Size(372, 392)
		Me.Controls.Add(Me.Single_Renamed)
		Me.Controls.Add(Me.ChartDataResults)
		Me.Controls.Add(Me.RtChart1)
		Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.ForeColor = System.Drawing.SystemColors.WindowText
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Location = New System.Drawing.Point(119, 134)
		Me.Location = New System.Drawing.Point(115, 111)
		Me.MaximizeBox = False
		Me.MinimizeBox = True
		Me.Name = "RTCPlotForm"
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Size = New System.Drawing.Size(380, 419)
		Me.Text = "LabOBJX Real-Time Chart - 2-D Arrays"
		Me.ResumeLayout(False)
	End Sub
	Sub ReLoadForm(ByVal addEvents As Boolean)
		Form_Load()
		If addEvents Then
			AddHandler MyBase.Closed, AddressOf Me.Form_Closed
		End If
	End Sub
#End Region
End Class