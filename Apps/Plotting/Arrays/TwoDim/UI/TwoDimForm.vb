Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic
Imports System
Imports System.Windows.Forms
Partial Friend Class TwoDimForm
    Inherits System.Windows.Forms.Form

    ' LabOBJX Real-Time Chart - Example Code
    ' (C) Copyright 1995, Scientific Software Tools, Inc.
    ' All Rights Reserved.
    '

    Const TWO_PIE As Double = 2 * 3.1415926535898

    Dim FirstPoint As Integer
    Dim NumSamples As Integer ' number of samples
    Dim NumChannels As Integer ' number of channels

    ' signal waveform limits (volts peak)
    Dim MaxVolts As Single
    Dim MinVolts As Single

    Public Sub New()
        MyBase.New()
        If _Instance Is Nothing Then
            If _InitializingInstance Then
                _Instance = Me
            Else
                Try
                    'For the start-up form, the first instance created is the default instance.
                    If Not (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint Is Nothing) AndAlso System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType Is Me.GetType() Then
                        _Instance = Me
                    End If

                Catch
                End Try
            End If
        End If
        'This call is required by the Windows Form Designer.
        InitializeComponent()
        If Not Me.DesignMode Then ReLoadForm(False)
    End Sub


    Private Sub Form_Load()

        ' initialize program's global variables
        InitializeGlobalVariables()

        ' initialize RtChart Pens
        ChartPen.InitChannelsPens(RtChart1)

        ' initialize RtChart global properties
        InitializeRtChart(RtChart1)

        ' initialize RtChart Pens
        ChartPen.InitChannelsPens(RtChart1)

        ' initialize RtChart for Volts vs Time plots
        InitializeChannels(RtChart1, NumChannels, NumSamples)

    End Sub

    Private Sub Generate2D_SineWaveArray(ByVal numChannels As Integer, ByVal numSamples As Integer, ByRef VoltsPeak As Single)

        Dim NumCycles, CycleTime As Single

        ' NumSamples is 0-based, NumChannels is 1-based
        ReDim VBDataArray(numSamples, numChannels - 1) ' dimension array for 2D values

        Dim RadiansPerSample As Single = TWO_PIE / numSamples

        ' Generate random cycle times, ensure at least 1 radian per sample
        RadiansPerSample = (RadiansPerSample * VBMath.Rnd()) + RadiansPerSample

        ' NumCycles determines # of cycles per channel in buffer
        For channel As Integer = 1 To numChannels

            NumCycles = (channel * RadiansPerSample)
            CycleTime = RadiansPerSample + NumCycles

            For sample As Integer = 0 To numSamples - 1
                VBDataArray(sample, channel) = VoltsPeak * Math.Sin(sample * CycleTime)
            Next sample

        Next channel

    End Sub

    Private Sub InitializeGlobalVariables()

        'initialize variables

        NumChannels = 4
        NumSamples = 100

        MaxVolts = 15
        MinVolts = -15

    End Sub

    Private Sub Single_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles Single_Renamed.Click

        ' generate 2-dimensional array twice as large to plot "random" wave
        Generate2D_SineWaveArray(NumChannels, NumSamples, MaxVolts)

        ' calls ChartData function once
        PlotData(RtChart1, NumChannels, NumSamples, VBDataArray)

    End Sub
    Private Sub Form_Closed(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MyBase.Closed
    End Sub
End Class