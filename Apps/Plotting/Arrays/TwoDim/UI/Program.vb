Option Strict Off
Option Explicit On
Imports System
Imports isr.Visuals.RealTimeChart
Module Program
    ' LabOBJX Real-Time Chart - Example Code
    ' (C) Copyright 1995, Scientific Software Tools, Inc.
    ' All Rights Reserved.
    '

    Public Const AssemblyTitle As String = "Real Time Strip Chart2 Dim Demo"
    Public Const AssemblyDescription As String = "Real Time Strip Chart 2 Dim Demo"
    Public Const AssemblyProduct As String = "Visuals.Real.Time.Chart.2Dim"

    Public result As Integer ' return value from function call

    Public VBDataArray(,) As Single ' VB array to store multi-channel voltage data values

    Public Const VOLTS_PER_DIV As Integer = 5 ' vertical scaling is based on volts per division
    Public Const TIME_PER_DIV As Integer = 10 ' horizontal scaling is based on seconds per division

    ' grid constants
    Public Const MAJOR_HORZ_DIV As Integer = 10
    Public Const MAJOR_VERT_DIV As Integer = 8
    Public Const MINOR_DIVISIONS As Integer = 5

    Sub InitChannelsFixedPens(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl)
        Dim RtChart_SolidPen As Object

        ' describe pen characteristics
        ' use channel values for pen names
        ' each pen name has the same value as its asigned channel

        ' Pen for ChannelName.Channel1
        rtChart.LogicalPen = ChannelName.Channel1)
		rtChart.PenColor = Drawing.Color.FromArgb(RED)
        rtChart.PenIntenseColor = Drawing.Color.FromArgb(YELLOW)
        rtChart.PenWidth = 1
        'UPGRADE_WARNING: (1068) RtChart_SolidPen of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
        rtChart.PenStyle = PenStyle.SolidPen

        ' Pen for ChannelName.Channel2
        rtChart.LogicalPen = ChannelName.Channel2)
		rtChart.PenColor = Drawing.Color.FromArgb(WHITE)
        rtChart.PenIntenseColor = Drawing.Color.FromArgb(YELLOW)
        rtChart.PenWidth = 1
        'UPGRADE_WARNING: (1068) RtChart_SolidPen of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
        rtChart.PenStyle = PenStyle.SolidPen

        ' Pen for ChannelName.Channel3
        rtChart.LogicalPen = ChannelName.Channel3)
		rtChart.PenColor = Drawing.Color.FromArgb(BLUE)
        rtChart.PenIntenseColor = Drawing.Color.FromArgb(YELLOW)
        rtChart.PenWidth = 1
        'UPGRADE_WARNING: (1068) RtChart_SolidPen of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
        rtChart.PenStyle = PenStyle.SolidPen

        ' Pen for ChannelName.Channel4
        rtChart.LogicalPen = ChannelName.Channel4)
		rtChart.PenColor = Drawing.Color.FromArgb(BLACK)
        rtChart.PenIntenseColor = Drawing.Color.FromArgb(YELLOW)
        rtChart.PenWidth = 1
        'UPGRADE_WARNING: (1068) RtChart_SolidPen of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
        rtChart.PenStyle = PenStyle.SolidPen

    End Sub

    Sub InitializeChannels(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal numChannels As Integer, ByVal samplesPerChannel As Integer)

        ' Initialize RtChart Window, Viewports, & Channels in CRT mode, Line style.


        ' View port Description
        rtChart.Viewports = 4 ' number of view ports displayed initially
        rtChart.ViewportStorageColor = Color.Green ' storage mode plot color, same for all view ports

        ' initialize storage mode for each view port
        For viewport As Integer = 1 To rtChart.Viewports
            rtChart.LogicalViewport = (viewport) ' select view port
            rtChart.ViewportStorageOn = False ' disable storage mode
        Next viewport

        ' Window Description
        rtChart.LogicalWindow = 1 ' select window
        rtChart.WndXmin = 0 ' left minimum abscissa
        rtChart.WndWidth = (TIME_PER_DIV * MAJOR_HORZ_DIV) ' in Seconds (Time/div * 10 divisions) 10 * 10 = 100
        rtChart.WndHeight = (VOLTS_PER_DIV * MAJOR_VERT_DIV) ' in Volts (Volts/div * 8 divisions) 5 * 8 = 40
        rtChart.WndYmin = (rtChart.WndHeight / -2) ' bottom minimum ordinate (negative full scale) 40 / -2 = -20

        ' initialize all channels to be used
        For channel As Integer = ChannelName.Channel1 To ChannelName.Channel4

            ' Channel Description
            rtChart.LogicalChannel = channel ' select channel
            rtChart.ChnDspWindow = 1 ' assign all channels to window 1
            rtChart.ChnDspViewport = channel ' assign each channel to a viewport
            rtChart.ChnDspPen = channel ' specify pen, use same "name" (value) as channel
            rtChart.ChnDspBufLen = samplesPerChannel ' input buffer length for 2D array
            rtChart.ChnDspMode = ChannelDisplayMode.CRT ' per channel display mode, use RunMode constants
            rtChart.ChnDspStyle = ChannelDisplayStyle.Lines ' use line style for plotting arrays
            rtChart.ChnDspOffsetX = 0 ' set offset to 0
            rtChart.ChnDspOffsetY = 0 ' set offset to 0

            ' Describe horizontal dimension
            rtChart.ChannelDataDimension = LogicalDimension.Horizontal
            rtChart.ChnDataShape = ChannelDataShape.AutoIncr
            rtChart.ChnDataOffset = 0 ' offset into buffer (in points)
            rtChart.ChnDataIncr = 1 ' logical increment to next value in array

            ' Describe vertical dimension
            rtChart.ChannelDataDimension = LogicalDimension.Vertical
            rtChart.ChnDataShape = ChannelDataShape.Scalar
            rtChart.ChnDataType = ChannelDataType.Single
            rtChart.ChnDataOffset = 0 ' offset into buffer (in points)
            rtChart.ChnDataIncr = 1 ' logical increment to next value for 2D array

            rtChart.ChnDspAction = ChannelDisplayAction.Initialize

        Next channel

    End Sub

    Sub InitializeRtChart(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl)

        ' Global operation properties
        rtChart.ImageFile = String.Empty ' specifies file name for Write action
        rtChart.HitTest = False ' enables mouse pointer "hit" detection
        rtChart.FramesPerSec = 0 ' 0 for asynchronous, -1 for synchronous

        ' Border description properties
        rtChart.BorderColor = Color.LightGray
        rtChart.LightColor = Color.DarkGray
        rtChart.ShadowColor = Color.Black
        rtChart.BevelInner = BevelStyle.Inset
        rtChart.BevelOuter = BevelStyle.Raised
        rtChart.BevelWidthInner = 5
        rtChart.BevelWidthOuter = 5
        rtChart.BorderWidth = 3
        rtChart.Outline = True
        rtChart.OutlineColor = Color.Black
        rtChart.RoundedCorners = False

        ' Graticule description properties
        rtChart.BackColor = Color.DarkCyan
        rtChart.AutoSize = True ' true forces square border, frame is always square

        rtChart.FrameOn = True
        rtChart.FrameTics = FrameTicsStyle.MajorMinorFrameTics
        rtChart.FrameColor = Color.White

        rtChart.AxesType = AxesType.NoAxes ' RtChart_HorzVertAxes
        rtChart.AxesTics = AxesTicsStyle.MajorMinorAxesTics
        rtChart.AxesColor = Color.White

        rtChart.GridOn = False
        rtChart.GridType = GridType.HorizontalVertical
        rtChart.GridColor = Color.LightGray

        rtChart.MajorDivHorz = MAJOR_HORZ_DIV ' major horizontal divisions
        rtChart.MajorDivVert = MAJOR_VERT_DIV ' major vertical divisions
        rtChart.MinorDiv = MINOR_DIVISIONS ' minor divisions
        rtChart.MarginLeft = 2 ' left Graticule border
        rtChart.MarginTop = 2 ' top Graticule border
        rtChart.MarginRight = 2 ' right Graticule border
        rtChart.MarginBottom = 2 ' bottom Graticule border

        rtChart.ColorDepth = ColorDepth.Color16 '  16 colors

    End Sub

    Sub PlotData(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal numChannels As Integer, ByVal numPoints As Integer, ByRef VBDataArray(,) As Single)

        ' RtChart exports ChartData function to plot data of selected channel


        rtChart.FramesPerSec = -1 ' synchronous mode - control returns after paint event occurs

        For channel As Integer = ChannelName.Channel1 To numChannels
            '       result = ChartData(RtChart, channel, NumPoints, VBDataArray(0, channel), ByVal 0&)
            result = rtChart.ChartData(channel, NumPoints, VBDataArray, (channel - 1) * NumPoints, 0, 0)
        Next channel

        rtChart.FramesPerSec = 0 ' asynchronous mode - control returns immediately after ChartData calls

        ' display result of chart data call
        If result >= 0 Then
            ' returned value is points charted
            TwoDimForm.DefInstance.ChartDataResults.Text = "Number of points charted = " & result.ToString()
        Else
            ' returned value is error code, convert to error string
            TwoDimForm.DefInstance.ChartDataResults.Text = ChartPen.ChartErrorMessage(result)
        End If

    End Sub
End Module