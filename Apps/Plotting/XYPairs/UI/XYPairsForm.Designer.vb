<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XYPairsForm
#Region "Upgrade Support "
	Private Shared _Instance As XYPairsForm
	Private Shared _InitializingInstance As Boolean
	Public Shared Property DefInstance() As XYPairsForm
		Get
			If _Instance Is Nothing OrElse _Instance.IsDisposed Then
				_InitializingInstance = True
				_Instance = CreateInstance()
				_InitializingInstance = False
			End If
			Return _Instance
		End Get
		Set(ByVal value As XYPairsForm)
			_Instance = Value
		End Set
	End Property
#End Region
#Region "Windows Form Designer generated code "
	Public Shared Function CreateInstance() As XYPairsForm
		Dim theInstance As New XYPairsForm()
		theInstance.Form_Load()
		Return theInstance
	End Function
	Private visualControls() As String = New String() {"components", "ToolTipMain", "Label2", "Frame2", "AutoCursor", "CurX4AbsPos", "ResetCursors", "DeltaTime", "CurX3AbsPos", "DeltaVolts", "CurY2AbsPos", "CurY1AbsPos", "_Label1_5", "_Label1_4", "_Label1_3", "_Label1_2", "_Label1_1", "_Label1_0", "Frame1", "RtChart1", "Label1"}
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTipMain As System.Windows.Forms.ToolTip
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Frame2 As System.Windows.Forms.GroupBox
	Public WithEvents AutoCursor As System.Windows.Forms.CheckBox
	Public WithEvents CurX4AbsPos As System.Windows.Forms.TextBox
	Public WithEvents ResetCursors As System.Windows.Forms.Button
	Public WithEvents DeltaTime As System.Windows.Forms.TextBox
	Public WithEvents CurX3AbsPos As System.Windows.Forms.TextBox
	Public WithEvents DeltaVolts As System.Windows.Forms.TextBox
	Public WithEvents CurY2AbsPos As System.Windows.Forms.TextBox
	Public WithEvents CurY1AbsPos As System.Windows.Forms.TextBox
	Private WithEvents _Label1_5 As System.Windows.Forms.Label
	Private WithEvents _Label1_4 As System.Windows.Forms.Label
	Private WithEvents _Label1_3 As System.Windows.Forms.Label
	Private WithEvents _Label1_2 As System.Windows.Forms.Label
	Private WithEvents _Label1_1 As System.Windows.Forms.Label
	Private WithEvents _Label1_0 As System.Windows.Forms.Label
	Public WithEvents Frame1 As System.Windows.Forms.GroupBox
	Public WithEvents RtChart1 As isr.Visuals.RealTimeChart.RealTimeChartControl
	Public Label1(5) As System.Windows.Forms.Label
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> _
	 Private Sub InitializeComponent()
		Me.components = New System.ComponentModel.Container()
		Dim resources As New System.ComponentModel.ComponentResourceManager(GetType(XYPairsForm))
		Me.ToolTipMain = New System.Windows.Forms.ToolTip(Me.components)
		Me.Frame2 = New System.Windows.Forms.GroupBox()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.Frame1 = New System.Windows.Forms.GroupBox()
		Me.AutoCursor = New System.Windows.Forms.CheckBox()
		Me.CurX4AbsPos = New System.Windows.Forms.TextBox()
		Me.ResetCursors = New System.Windows.Forms.Button()
		Me.DeltaTime = New System.Windows.Forms.TextBox()
		Me.CurX3AbsPos = New System.Windows.Forms.TextBox()
		Me.DeltaVolts = New System.Windows.Forms.TextBox()
		Me.CurY2AbsPos = New System.Windows.Forms.TextBox()
		Me.CurY1AbsPos = New System.Windows.Forms.TextBox()
		Me._Label1_5 = New System.Windows.Forms.Label()
		Me._Label1_4 = New System.Windows.Forms.Label()
		Me._Label1_3 = New System.Windows.Forms.Label()
		Me._Label1_2 = New System.Windows.Forms.Label()
		Me._Label1_1 = New System.Windows.Forms.Label()
		Me._Label1_0 = New System.Windows.Forms.Label()
		Me.RtChart1 = New isr.Visuals.RealTimeChart.RealTimeChartControl()
		Me.Frame2.SuspendLayout()
		Me.Frame1.SuspendLayout()
		Me.SuspendLayout()
		' 
		'Frame2
		' 
		Me.Frame2.AllowDrop = True
		Me.Frame2.BackColor = System.Drawing.SystemColors.Window
		Me.Frame2.Controls.Add(Me.Label2)
		Me.Frame2.Enabled = True
		Me.Frame2.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Frame2.Location = New System.Drawing.Point(292, 228)
		Me.Frame2.Name = "Frame2"
		Me.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame2.Size = New System.Drawing.Size(69, 153)
		Me.Frame2.TabIndex = 14
		Me.Frame2.Text = "Notes"
		Me.Frame2.Visible = True
		' 
		'Label2
		' 
		Me.Label2.AllowDrop = True
		Me.Label2.BackColor = System.Drawing.SystemColors.Window
		Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label2.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Label2.Location = New System.Drawing.Point(4, 16)
		Me.Label2.Name = "Label2"
		Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label2.Size = New System.Drawing.Size(57, 133)
		Me.Label2.TabIndex = 15
		Me.Label2.Text = "Drag cursors. Enable Autotrack mode, Y cursor intersects X cursor at data point."
		' 
		'Frame1
		' 
		Me.Frame1.AllowDrop = True
		Me.Frame1.BackColor = System.Drawing.SystemColors.Window
		Me.Frame1.Controls.Add(Me.AutoCursor)
		Me.Frame1.Controls.Add(Me.CurX4AbsPos)
		Me.Frame1.Controls.Add(Me.ResetCursors)
		Me.Frame1.Controls.Add(Me.DeltaTime)
		Me.Frame1.Controls.Add(Me.CurX3AbsPos)
		Me.Frame1.Controls.Add(Me.DeltaVolts)
		Me.Frame1.Controls.Add(Me.CurY2AbsPos)
		Me.Frame1.Controls.Add(Me.CurY1AbsPos)
		Me.Frame1.Controls.Add(Me._Label1_5)
		Me.Frame1.Controls.Add(Me._Label1_4)
		Me.Frame1.Controls.Add(Me._Label1_3)
		Me.Frame1.Controls.Add(Me._Label1_2)
		Me.Frame1.Controls.Add(Me._Label1_1)
		Me.Frame1.Controls.Add(Me._Label1_0)
		Me.Frame1.Enabled = True
		Me.Frame1.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Frame1.Location = New System.Drawing.Point(12, 228)
		Me.Frame1.Name = "Frame1"
		Me.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame1.Size = New System.Drawing.Size(273, 153)
		Me.Frame1.TabIndex = 0
		Me.Frame1.Text = "Cursors"
		Me.Frame1.Visible = True
		' 
		'AutoCursor
		' 
		Me.AutoCursor.AllowDrop = True
		Me.AutoCursor.Appearance = System.Windows.Forms.Appearance.Normal
		Me.AutoCursor.BackColor = System.Drawing.SystemColors.Window
		Me.AutoCursor.CausesValidation = True
		Me.AutoCursor.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.AutoCursor.CheckState = System.Windows.Forms.CheckState.Unchecked
		Me.AutoCursor.Enabled = True
		Me.AutoCursor.ForeColor = System.Drawing.SystemColors.WindowText
		Me.AutoCursor.Location = New System.Drawing.Point(184, 112)
		Me.AutoCursor.Name = "AutoCursor"
		Me.AutoCursor.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.AutoCursor.Size = New System.Drawing.Size(81, 21)
		Me.AutoCursor.TabIndex = 16
		Me.AutoCursor.TabStop = True
		Me.AutoCursor.Text = "Autotrack"
		Me.AutoCursor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.AutoCursor.Visible = True
		' 
		'CurX4AbsPos
		' 
		Me.CurX4AbsPos.AcceptsReturn = True
		Me.CurX4AbsPos.AllowDrop = True
		Me.CurX4AbsPos.BackColor = System.Drawing.SystemColors.Window
		Me.CurX4AbsPos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.CurX4AbsPos.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.CurX4AbsPos.ForeColor = System.Drawing.SystemColors.WindowText
		Me.CurX4AbsPos.Location = New System.Drawing.Point(96, 82)
		Me.CurX4AbsPos.MaxLength = 0
		Me.CurX4AbsPos.Name = "CurX4AbsPos"
		Me.CurX4AbsPos.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.CurX4AbsPos.Size = New System.Drawing.Size(73, 21)
		Me.CurX4AbsPos.TabIndex = 8
		' 
		'ResetCursors
		' 
		Me.ResetCursors.AllowDrop = True
		Me.ResetCursors.BackColor = System.Drawing.SystemColors.Control
		Me.ResetCursors.ForeColor = System.Drawing.SystemColors.ControlText
		Me.ResetCursors.Location = New System.Drawing.Point(180, 44)
		Me.ResetCursors.Name = "ResetCursors"
		Me.ResetCursors.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ResetCursors.Size = New System.Drawing.Size(65, 57)
		Me.ResetCursors.TabIndex = 6
		Me.ResetCursors.Text = "Reset"
		Me.ResetCursors.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
		Me.ResetCursors.UseVisualStyleBackColor = False
		' 
		'DeltaTime
		' 
		Me.DeltaTime.AcceptsReturn = True
		Me.DeltaTime.AllowDrop = True
		Me.DeltaTime.BackColor = System.Drawing.SystemColors.Window
		Me.DeltaTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.DeltaTime.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.DeltaTime.ForeColor = System.Drawing.SystemColors.WindowText
		Me.DeltaTime.Location = New System.Drawing.Point(96, 124)
		Me.DeltaTime.MaxLength = 0
		Me.DeltaTime.Name = "DeltaTime"
		Me.DeltaTime.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.DeltaTime.Size = New System.Drawing.Size(73, 21)
		Me.DeltaTime.TabIndex = 5
		' 
		'CurX3AbsPos
		' 
		Me.CurX3AbsPos.AcceptsReturn = True
		Me.CurX3AbsPos.AllowDrop = True
		Me.CurX3AbsPos.BackColor = System.Drawing.SystemColors.Window
		Me.CurX3AbsPos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.CurX3AbsPos.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.CurX3AbsPos.ForeColor = System.Drawing.SystemColors.WindowText
		Me.CurX3AbsPos.Location = New System.Drawing.Point(96, 40)
		Me.CurX3AbsPos.MaxLength = 0
		Me.CurX3AbsPos.Name = "CurX3AbsPos"
		Me.CurX3AbsPos.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.CurX3AbsPos.Size = New System.Drawing.Size(73, 21)
		Me.CurX3AbsPos.TabIndex = 4
		' 
		'DeltaVolts
		' 
		Me.DeltaVolts.AcceptsReturn = True
		Me.DeltaVolts.AllowDrop = True
		Me.DeltaVolts.BackColor = System.Drawing.SystemColors.Window
		Me.DeltaVolts.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.DeltaVolts.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.DeltaVolts.ForeColor = System.Drawing.SystemColors.WindowText
		Me.DeltaVolts.Location = New System.Drawing.Point(12, 124)
		Me.DeltaVolts.MaxLength = 0
		Me.DeltaVolts.Name = "DeltaVolts"
		Me.DeltaVolts.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.DeltaVolts.Size = New System.Drawing.Size(73, 21)
		Me.DeltaVolts.TabIndex = 3
		' 
		'CurY2AbsPos
		' 
		Me.CurY2AbsPos.AcceptsReturn = True
		Me.CurY2AbsPos.AllowDrop = True
		Me.CurY2AbsPos.BackColor = System.Drawing.SystemColors.Window
		Me.CurY2AbsPos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.CurY2AbsPos.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.CurY2AbsPos.ForeColor = System.Drawing.SystemColors.WindowText
		Me.CurY2AbsPos.Location = New System.Drawing.Point(12, 82)
		Me.CurY2AbsPos.MaxLength = 0
		Me.CurY2AbsPos.Name = "CurY2AbsPos"
		Me.CurY2AbsPos.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.CurY2AbsPos.Size = New System.Drawing.Size(73, 21)
		Me.CurY2AbsPos.TabIndex = 2
		' 
		'CurY1AbsPos
		' 
		Me.CurY1AbsPos.AcceptsReturn = True
		Me.CurY1AbsPos.AllowDrop = True
		Me.CurY1AbsPos.BackColor = System.Drawing.SystemColors.Window
		Me.CurY1AbsPos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.CurY1AbsPos.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.CurY1AbsPos.ForeColor = System.Drawing.SystemColors.WindowText
		Me.CurY1AbsPos.Location = New System.Drawing.Point(12, 40)
		Me.CurY1AbsPos.MaxLength = 0
		Me.CurY1AbsPos.Name = "CurY1AbsPos"
		Me.CurY1AbsPos.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.CurY1AbsPos.Size = New System.Drawing.Size(73, 21)
		Me.CurY1AbsPos.TabIndex = 1
		' 
		'_Label1_5
		' 
		Me._Label1_5.AllowDrop = True
		Me._Label1_5.BackColor = System.Drawing.SystemColors.Window
		Me._Label1_5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label1_5.ForeColor = System.Drawing.SystemColors.WindowText
		Me._Label1_5.Location = New System.Drawing.Point(96, 108)
		Me._Label1_5.Name = "_Label1_5"
		Me._Label1_5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label1_5.Size = New System.Drawing.Size(73, 13)
		Me._Label1_5.TabIndex = 13
		Me._Label1_5.Text = "Delta Time"
		' 
		'_Label1_4
		' 
		Me._Label1_4.AllowDrop = True
		Me._Label1_4.BackColor = System.Drawing.SystemColors.Window
		Me._Label1_4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label1_4.ForeColor = System.Drawing.SystemColors.WindowText
		Me._Label1_4.Location = New System.Drawing.Point(96, 68)
		Me._Label1_4.Name = "_Label1_4"
		Me._Label1_4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label1_4.Size = New System.Drawing.Size(73, 13)
		Me._Label1_4.TabIndex = 12
		Me._Label1_4.Text = "X4 Time"
		' 
		'_Label1_3
		' 
		Me._Label1_3.AllowDrop = True
		Me._Label1_3.BackColor = System.Drawing.SystemColors.Window
		Me._Label1_3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label1_3.ForeColor = System.Drawing.SystemColors.WindowText
		Me._Label1_3.Location = New System.Drawing.Point(96, 24)
		Me._Label1_3.Name = "_Label1_3"
		Me._Label1_3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label1_3.Size = New System.Drawing.Size(73, 13)
		Me._Label1_3.TabIndex = 11
		Me._Label1_3.Text = "X3 Time"
		' 
		'_Label1_2
		' 
		Me._Label1_2.AllowDrop = True
		Me._Label1_2.BackColor = System.Drawing.SystemColors.Window
		Me._Label1_2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label1_2.ForeColor = System.Drawing.SystemColors.WindowText
		Me._Label1_2.Location = New System.Drawing.Point(12, 108)
		Me._Label1_2.Name = "_Label1_2"
		Me._Label1_2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label1_2.Size = New System.Drawing.Size(73, 13)
		Me._Label1_2.TabIndex = 10
		Me._Label1_2.Text = "Delta Volts"
		' 
		'_Label1_1
		' 
		Me._Label1_1.AllowDrop = True
		Me._Label1_1.BackColor = System.Drawing.SystemColors.Window
		Me._Label1_1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label1_1.ForeColor = System.Drawing.SystemColors.WindowText
		Me._Label1_1.Location = New System.Drawing.Point(12, 68)
		Me._Label1_1.Name = "_Label1_1"
		Me._Label1_1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label1_1.Size = New System.Drawing.Size(73, 13)
		Me._Label1_1.TabIndex = 9
		Me._Label1_1.Text = "Y2 Volts"
		' 
		'_Label1_0
		' 
		Me._Label1_0.AllowDrop = True
		Me._Label1_0.BackColor = System.Drawing.SystemColors.Window
		Me._Label1_0.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label1_0.ForeColor = System.Drawing.SystemColors.WindowText
		Me._Label1_0.Location = New System.Drawing.Point(12, 24)
		Me._Label1_0.Name = "_Label1_0"
		Me._Label1_0.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label1_0.Size = New System.Drawing.Size(73, 13)
		Me._Label1_0.TabIndex = 7
		Me._Label1_0.Text = "Y1 Volts"
		' 
		'RtChart1
		' 
		Me.RtChart1.AutoSize = 0
		Me.RtChart1.AxesColor = 16776960
		Me.RtChart1.AxesTics = 0
		Me.RtChart1.AxesType = 0
		Me.RtChart1.BackColor = 8421376
		Me.RtChart1.BevelInner = 0
		Me.RtChart1.BevelOuter = 2
		Me.RtChart1.BevelWidth_Inner = 1
		Me.RtChart1.BevelWidth_Outer = 1
		Me.RtChart1.BorderColor = (-2147483633)
		Me.RtChart1.BorderWidth = 3
		Me.RtChart1.Chn_Select = 1
		Me.RtChart1.ChnData_Dimension = 1
		Me.RtChart1.ChnDspUserData = 0
		Me.RtChart1.ChnXform_Select = 1
		Me.RtChart1.ColorDepth = 0
		Me.RtChart1.ErrBase = 30200
		Me.RtChart1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.RtChart1.ForeColor = (-842150451)
		Me.RtChart1.FrameColor = 16776960
		Me.RtChart1.FrameOn = -1
		Me.RtChart1.FramesPerSec = 0
		Me.RtChart1.FrameTics = 0
		Me.RtChart1.GridColor = 8421504
		Me.RtChart1.GridOn = -1
		Me.RtChart1.GridSymmetry = 0
		Me.RtChart1.GridType = 0
		Me.RtChart1.HitTest = 0
		Me.RtChart1.ImageFile = String.Empty
		Me.RtChart1.LightColor = (-2147483634)
		Me.RtChart1.Location = New System.Drawing.Point(8, 8)
		Me.RtChart1.MajorDivHorz = 10
		Me.RtChart1.MajorDivVert = 8
		Me.RtChart1.MarginBottom = 10
		Me.RtChart1.MarginLeft = 10
		Me.RtChart1.MarginRight = 10
		Me.RtChart1.MarginTop = 10
		Me.RtChart1.MinorDiv = 5
		Me.RtChart1.MouseIcon = 0
		Me.RtChart1.Name = "RtChart1"
		Me.RtChart1.Outline = -1
		Me.RtChart1.OutlineColor = (-2147483642)
		Me.RtChart1.Pen_Select = 1
		Me.RtChart1.RoundedCorners = 0
		Me.RtChart1.Scale_Select = 1
		Me.RtChart1.ShadowColor = (-2147483632)
		Me.RtChart1.Size = New System.Drawing.Size(353, 209)
		Me.RtChart1.TabIndex = 17
		Me.RtChart1.Viewport_Select = 1
		Me.RtChart1.Viewports = 1
		Me.RtChart1.ViewportStorageColor = 12632256
		Me.RtChart1.ViewportStorageOn = 0
		Me.RtChart1.Wnd_Select = 1
		' 
		'Form1
		' 
		Me.AllowDrop = True
		Me.AutoScaleDimensions = New System.Drawing.SizeF(7, 13)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Window
		Me.ClientSize = New System.Drawing.Size(366, 391)
		Me.Controls.Add(Me.Frame2)
		Me.Controls.Add(Me.Frame1)
		Me.Controls.Add(Me.RtChart1)
		Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0)
		Me.ForeColor = System.Drawing.SystemColors.WindowText
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Location = New System.Drawing.Point(195, 138)
		Me.Location = New System.Drawing.Point(191, 115)
		Me.MaximizeBox = False
		Me.MinimizeBox = True
		Me.Name = "Form1"
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Size = New System.Drawing.Size(374, 418)
		Me.Text = "LabOBJX Real-Time Chart - Plotting XY Pairs"
		Me.Frame2.ResumeLayout(False)
		Me.Frame1.ResumeLayout(False)
		Me.ResumeLayout(False)
	End Sub
	Sub ReLoadForm(ByVal addEvents As Boolean)
		InitializeLabel1()
		Form_Load()
		If addEvents Then
			AddHandler MyBase.Closed, AddressOf Me.Form_Closed
		End If
	End Sub
	Sub InitializeLabel1()
		ReDim Label1(5)
		Me.Label1(5) = _Label1_5
		Me.Label1(4) = _Label1_4
		Me.Label1(3) = _Label1_3
		Me.Label1(2) = _Label1_2
		Me.Label1(1) = _Label1_1
		Me.Label1(0) = _Label1_0
	End Sub
#End Region
End Class