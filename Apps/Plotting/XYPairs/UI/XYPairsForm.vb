Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic
Imports System
Imports System.Windows.Forms
Imports isr.Visuals.RealTimeChart
Partial Friend Class XYPairsForm
    Inherits System.Windows.Forms.Form

    ' LabOBJX Real-Time Chart - Example Code
    ' (C) Copyright 1995, Scientific Software Tools, Inc.
    ' All Rights Reserved.
    '


    Dim i As Integer ' index in for/next loops
    Dim result As Integer ' return value from function call
    Dim TempValue As Integer ' temporary storage variable
    Dim AutoCursorFlag As Integer ' boolean to control auto cursor mode

    ' used in RtChart event procedures for re-positioning data channels and cursors
    Dim LastObject As Integer ' selected channel on MouseDown event
    Dim MouseX As Single ' previous mouse X-axis position
    Dim MouseY As Single ' previous mouse Y-axis position
    Dim CursorVolts As Single
    Dim Cursor1Volts As Single ' voltage position value of cursor1
    Dim Cursor2Volts As Single ' voltage position value of cursor2
    Dim Cursor3Time As Single ' time position value of cursor3
    Dim Cursor4Time As Single ' time position value of cursor4

    Private isInitializingComponent As Boolean
    Public Sub New()
        MyBase.New()
        If _Instance Is Nothing Then
            If _InitializingInstance Then
                _Instance = Me
            Else
                Try
                    'For the start-up form, the first instance created is the default instance.
                    If Not (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint Is Nothing) AndAlso System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType Is Me.GetType() Then
                        _Instance = Me
                    End If

                Catch
                End Try
            End If
        End If
        'This call is required by the Windows Form Designer.
        isInitializingComponent = True
        InitializeComponent()
        isInitializingComponent = False
        If Not Me.DesignMode Then ReLoadForm(False)
    End Sub


    Private Sub AutoCursor_CheckStateChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles AutoCursor.CheckStateChanged
        AutoCursorFlag = AutoCursor.CheckState
    End Sub

    Private Sub AutoMoveCursor(ByRef YCursorName As Integer, ByRef XCursorTime As Single)

        ' takes name of Y cursor to auto position and time position of X cursor
        ' globals - SelectedChannel, NumSamples, CursorVolts, Cursor1Volts, Cursor2Volts, LastObject

        RtChart1.LogicalChannel = (SelectedChannel) ' select current data channel to get its offset

        ' index = "time" (in samples) - data channel X-axis offset
        ' index i must be within data buffer index range
        i = XCursorTime - RtChart1.ChnDspOffsetX
        If i < 0 Then i = 0
        If i >= NumSamples Then i = NumSamples - 1

        ' get voltage point which intersects with time YCursorName, add data channel Y-axis offset
        ' compensate for any offset in data plot
        CursorVolts = RtChart1.ChnDspOffsetY + VBDataArrayY(YData, i)

        ' use absolute cursor voltage value for readout and delta values
        Select Case YCursorName ' select which cursor voltage to update for text box
            Case ChannelName.Cursor1
                Cursor1Volts = CursorVolts
                CurY1AbsPos.Text = Cursor1Volts.ToString() ' update Cursor1 position text box
            Case ChannelName.Cursor2
                Cursor2Volts = CursorVolts
                CurY2AbsPos.Text = Cursor2Volts.ToString() ' update Cursor2 position text box
        End Select

        ' select volts cursor and get offset to compensate for existing offset in cursor
        ' to correctly place cursor over data plot
        RtChart1.LogicalChannel = (YCursorName)
        CursorVolts -= RtChart1.ChnDspOffsetY

        ' move volts cursor to new position
        DrawCursor(RtChart1, YCursorName, RtChart1.WndXmin, CursorVolts, RtChart1.WndWidth, CursorVolts)

        ' re-select current object (YCursorName) for mouse up event
        RtChart1.LogicalChannel = (LastObject)

        ' calculate and update voltage cursors' delta
        DeltaVolts.Text = (Cursor1Volts - Cursor2Volts).ToString()

    End Sub

    Private Sub DrawCursor(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal Chn As Integer, ByVal X1 As Single, ByVal Y1 As Single, ByVal X2 As Single, ByVal Y2 As Single)
        ' draws a cursor (2-point line) at the coordinates given

        ' Draw a single cursor
        Dim LineCoord(4) As Single

        ' assign coordinates of cursor line to array
        LineCoord(0) = X1
        LineCoord(1) = Y1
        LineCoord(2) = X2
        LineCoord(3) = Y2

        ' pass 2 points (4 coordinates) for single line in LineCoord array
        result = rtChart.ChartData(Chn, 2, LineCoord, 0, 0, 0)

    End Sub

    Private Sub DrawCursors(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl)

        ' determines the coordinates to draw four cursors at fixed positions between two grid lines
        ' based on the current channel's window size and calls DrawCursor to plot

        ' cursor position variables

        ' prevent each cursor from being re-painted individually on Initialize Action
        rtChart.ChartAction = ChartAction.DisablePaint

        ' zero cursor channel offsets
        ZeroChannelOffsets(rtChart, ChannelName.Cursor1, ChannelName.Cursor4)

        ' select channel to get currently selected channel's window & view port
        rtChart.LogicalChannel = (SelectedChannel)

        ' select appropriate window to reference specified Window properties
        rtChart.LogicalWindow = rtChart.ChnDspWindow ' select "time" window

        ' calculate absolute full-scale (maximum) window position
        Dim PosFS_WndY As Single = rtChart.WndYmin + rtChart.WndHeight
        Dim PosFS_WndX As Single = rtChart.WndXmin + rtChart.WndWidth

        ' assign values to cursor position variables, locate cursors 1.5 divs inside frame
        Dim PosY1 As Single = PosFS_WndY - 1.5 * rtChart.WndHeight / MAJOR_VERT_DIV ' -1.5 * (WndHeight / 8 divs)
        Dim PosY2 As Single = rtChart.WndYmin + 1.5 * rtChart.WndHeight / MAJOR_VERT_DIV ' +1.5 * (WndHeight / 8 divs)
        Dim PosX3 As Single = rtChart.WndXmin + 1.5 * rtChart.WndWidth / MAJOR_HORZ_DIV ' +1.5 * (WndWidth / 10 divs)
        Dim PosX4 As Single = PosFS_WndX - 1.5 * rtChart.WndWidth / MAJOR_HORZ_DIV ' -1.5 * (WndWidth / 10 divs)

        ' plot four cursor lines
        DrawCursor(rtChart, ChannelName.Cursor1, rtChart.WndXmin, PosY1, PosFS_WndX, PosY1)
        DrawCursor(rtChart, ChannelName.Cursor2, rtChart.WndXmin, PosY2, PosFS_WndX, PosY2)
        DrawCursor(rtChart, ChannelName.Cursor3, PosX3, rtChart.WndYmin, PosX3, PosFS_WndY)
        DrawCursor(rtChart, ChannelName.Cursor4, PosX4, rtChart.WndYmin, PosX4, PosFS_WndY)

        ' paint entire window with new positions for all cursors
        rtChart.ChartAction = ChartAction.EnablePaint

        ' update text readout of absolute cursor position values
        CurY1AbsPos.Text = PosY1.ToString()
        CurY2AbsPos.Text = PosY2.ToString()
        CurX3AbsPos.Text = PosX3.ToString()
        CurX4AbsPos.Text = PosX4.ToString()

        ' update text readout of relative (delta) cursor position values
        DeltaVolts.Text = (PosY1 - PosY2).ToString()
        DeltaTime.Text = (PosX4 - PosX3).ToString()

    End Sub

    Private Sub Form_Load()

        GenerateXYPairArrays(NumChannels, NumSamples, MaxVolts)

        InitRtChartErrorArray()

        InitializeRtChart(RtChart1)

        ChartPen.InitChannelsPens(RtChart1)

        InitXYPairChannels(RtChart1, NumChannels, NumSamples)

        InitializeCursors(RtChart1, 1)

        PlotData(RtChart1, NumChannels, NumSamples)

        DrawCursors(RtChart1)

        SelectedChannel = 1

    End Sub

    Private Sub GenerateXYPairArrays(ByVal numChannels As Integer, ByVal numSamples As Integer, ByRef VoltsPeak As Single)

        Dim NumCycles, CycleTime As Single

        ' NumChannels is 1-based
        ReDim VBDataArrayY(YData, numSamples - 1) ' 2D array for XY pairs

        Dim RadiansPerSample As Single = TWO_PIE / numSamples

        ' build XY pair array (X1, Y1, X2, Y2, X3, Y3, ...)

        ' build X values equal to sample number
        For sample As Integer = 0 To numSamples - 1
            VBDataArrayY(XData, sample) = sample
        Next sample

        ' build Y equal to volts
        For sample As Integer = 0 To numSamples - 1
            CycleTime = RadiansPerSample + NumCycles
            VBDataArrayY(YData, sample) = VoltsPeak * Math.Sin(sample * CycleTime)
        Next sample

    End Sub

    Private Sub InitializeCursors(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal window As Integer)
        Dim RtChart_ChnInitialize, RtChart_CRT, RtChart_Lines, RtChart_Pair, RtChart_Single, RtChart_Vert As Object

        ' Setup cursor channel parameters

        For channel As Integer = ChannelName.Cursor1 To ChannelName.Cursor4

            rtChart.LogicalChannel = channel
            rtChart.ChnDspWindow = window ' logical coord range
            rtChart.ChnDspViewport = 1 ' default display location
            rtChart.ChnDspPen = ChannelName.Cursor1 ' drawing pen, same value as cursor1
            rtChart.ChnDspBufLen = 2 ' input buffer length is 2 point for line
            rtChart.ChnDspMode = ChannelDisplayMode.CRT ' channel display mode
            rtChart.ChnDspStyle = ChannelDisplayStyle.Lines
            rtChart.ChnDspOffsetX = 0
            rtChart.ChnDspOffsetY = 0

            ' Describe input channel as 2 point line for cursor
            rtChart.ChannelDataDimension = LogicalDimension.Vertical
            'UPGRADE_WARNING: (1068) RtChart_Pair of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
            rtChart.ChnDataShape = ChannelDataShape.Pair
            rtChart.ChnDataType = ChannelDataType.Single
            rtChart.ChnDataOffset = 0 ' offset into buffer (in points)
            rtChart.ChnDataIncr = 1 ' logical increment to next value

            rtChart.ChnDspAction = ChannelDisplayAction.Initialize

        Next channel

    End Sub

    Private Sub InitializeRtChart(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl)

        ' Global operation properties
        rtChart.ImageFile = String.Empty ' specifies file name for Write action
        rtChart.HitTest = True ' enables mouse pointer "hit" detection
        rtChart.FramesPerSec = 0 ' 0 for asynchronous, -1 for synchronous
        'RtChart.Image = 0                'hDC to offscreen bmp??? RO

        ' Border description properties
        rtChart.BorderColor = Color.LightGray
        rtChart.LightColor = Color.DarkGray
        rtChart.ShadowColor = Color.Black
        rtChart.BevelInner = BevelStyle.Inset
        rtChart.BevelOuter = BevelStyle.Raised
        rtChart.BevelWidthInner = 5
        rtChart.BevelWidthOuter = 5
        rtChart.BorderWidth = 3
        rtChart.Outline = True
        rtChart.OutlineColor = Color.Black
        rtChart.RoundedCorners = False

        ' Graticule description properties
        rtChart.BackColor = Color.DarkCyan
        rtChart.AutoSize = True ' true forces square border, frame is always square

        rtChart.FrameOn = True
        rtChart.FrameTics = FrameTicsStyle.MajorMinorFrameTics
        rtChart.FrameColor = Color.White

        rtChart.AxesType = AxesType.HorizontalVertical
        rtChart.AxesTics = AxesTicsStyle.MajorMinorAxesTics
        rtChart.AxesColor = Color.White

        rtChart.GridOn = False
        rtChart.GridType = GridType.HorizontalVertical
        rtChart.GridColor = Color.LightGray

        rtChart.MajorDivHorz = MAJOR_HORZ_DIV ' major horizontal divisions
        rtChart.MajorDivVert = MAJOR_VERT_DIV ' major vertical divisions
        rtChart.MinorDiv = MINOR_DIVISIONS ' minor divisions
        rtChart.MarginLeft = 2 ' left Graticule border
        rtChart.MarginTop = 2 ' top Graticule border
        rtChart.MarginRight = 2 ' right Graticule border
        rtChart.MarginBottom = 2 ' bottom Graticule border

        rtChart.ColorDepth = ColorDepth.Color16 '  256 palette type adapter only windows_g

    End Sub

    Private Sub InitXYPairChannels(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal numChannels As Integer, ByVal samplesPerChannel As Integer)
        Dim RtChart_ChnInitialize, RtChart_CRT, RtChart_Lines, RtChart_Pair, RtChart_Single, RtChart_Vert As Object

        ' Initialize CRT Control

        ' Window Description
        rtChart.LogicalWindow = 1 ' select X-axis time window
        rtChart.WndXmin = 0 ' left minimum abscissa
        rtChart.WndWidth = (TimePerDiv * MAJOR_HORZ_DIV) ' in Seconds (Time/div * 10 divisions) 100 * 10 = 1000
        rtChart.WndHeight = (VoltsPerDiv * MAJOR_VERT_DIV) ' in Volts (Volts/div * 8 divisions) 5 * 8 = 40
        rtChart.WndYmin = (rtChart.WndHeight / -2) ' bottom minimum ordinate (negative full scale) 40 / -2 = -20

        ' initialize channel

        ' Describe Channel
        rtChart.LogicalChannel = (ChannelName.Channel1) ' select channel
        rtChart.ChnDspWindow = 1 ' assign window #1 to channel
        rtChart.ChnDspViewport = 1 ' specify viewport
        rtChart.ChnDspPen = ChannelName.Channel1 ' specify pen, same value as channel
        rtChart.ChnDspBufLen = samplesPerChannel ' input buffer length for XY pair array (number of points)
        rtChart.ChnDspMode = ChannelDisplayMode.CRT ' per channel display mode, use RunMode constants
        rtChart.ChnDspStyle = ChannelDisplayStyle.Lines ' use line style for plotting arrays
        rtChart.ChnDspOffsetX = 0 ' set offset to 0
        rtChart.ChnDspOffsetY = 0 ' set offset to 0

        ' Describe vertical dimension
        rtChart.ChannelDataDimension = LogicalDimension.Vertical
        'UPGRADE_WARNING: (1068) RtChart_Pair of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
        rtChart.ChnDataShape = ChannelDataShape.Pair ' specify XY pair
        rtChart.ChnDataType = ChannelDataType.Single
        rtChart.ChnDataOffset = 0 ' offset into buffer (in points)
        rtChart.ChnDataIncr = 1 ' logical increment to next value for 2D array

        rtChart.ChnDspAction = ChannelDisplayAction.Initialize

    End Sub

    Private Sub PlotData(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal numChannels As Integer, ByVal numPoints As Integer)

        ' RtChart exports ChartData function to plot data of selected channel


        ' pass first point of XY pair array, RtChart reads array as XY pairs
        result = rtChart.ChartData(ChannelName.Channel1, NumPoints, VBDataArrayY, 0, 0, 0)

        ' display returned status code converted to string
        If result < 0 Then Interaction.MsgBox(RtChartError(result), MsgBoxStyle.Exclamation, "RtChart ChartData Error")

    End Sub

    Private Sub ResetCursors_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles ResetCursors.Click


        ' draw (re-draw) cursors in pre-defined position

        ' prevent each cursor from being re-painted individually on Initialize Action
        RtChart1.ChartAction = ChartAction.DisablePaint

        ' zero cursor channel offsets
        ZeroChannelOffsets(RtChart1, ChannelName.Cursor1, ChannelName.Cursor4)

        ' DrawCursors zeros cursor offsets, if any
        DrawCursors(RtChart1)

        ' paint entire window with new positions for all cursors
        RtChart1.ChartAction = ChartAction.EnablePaint

    End Sub

    'UPGRADE_NOTE: (7001) The following declaration (RtChart1_MouseDown) seems to be dead code More Information: https://www.mobilize.net/vbtonet/ewis/ewi7001
    'Private Sub RtChart1_MouseDown(ByRef Button As Integer, ByRef Shift As Integer, ByRef X As Single, ByRef Y As Single, ByRef Obj As Integer, ByRef position As Integer, ByRef ObjType As Integer)
    'Dim RtChart_ChnIntenseColor As Object
    '
    ' RtChart Mouse (button) Down event procedure, executes anytime mouse pointer is over Rtchart
    ' and mouse button is pressed down. Used to select channels (and cursors) for dragging.
    ' Changes plot color from normal to intense
    ' Mouse pointer is changed to "size" cursor and restored to "hand" in Mouse Up event procedure.
    '
    'If Obj > 0 And Obj <> LastObject Then ' if object selected, but not previously selected
    '
    'RtChart1.LogicalChannel = (Obj) ' select object that was hit
    ''UPGRADE_WARNING: (1068) RtChart_ChnIntenseColor of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
    'RtChart1.ChnDspAction = ((ChannelDisplayAction.IntenseColor)) ' use intense pen color to highlight
    '
    'ScreenToWindow(RtChart1, Obj, X, Y) ' convert screen positions to window coordinates
    '
    ' used to calculate offset from current position
    'MouseX = X ' store current mouse location X
    'MouseY = Y ' store current mouse location Y
    '
    'LastObject = Obj ' save selected object to be moved
    '
    'End If
    '
    'End Sub

    'UPGRADE_NOTE: (7001) The following declaration (RtChart1_MouseMove) seems to be dead code More Information: https://www.mobilize.net/vbtonet/ewis/ewi7001
    'Private Sub RtChart1_MouseMove(ByRef Button As Integer, ByRef Shift As Integer, ByRef X As Single, ByRef Y As Single, ByRef Obj As Integer, ByRef position As Integer, ByRef ObjType As Integer)
    '
    '
    ' convert mouse pointer position to world coordinates (engineering units)
    'ScreenToWindow(RtChart1, SelectedChannel, X, Y) ' use currently selected channel's window
    '
    'If LastObject <= 0 Then 'Exit Sub ' no objects were selected by mouse down event, exit sub
    '
    'RtChart1.LogicalChannel = (LastObject) ' select object to change offset (move)
    '
    ' drag cursors (move) by changing cursors channel offsets
    ' use displacement relative to original position
    'Select Case LastObject
    'Case ChannelName.Cursor1 ' top voltage cursor (horizontal)
    ' must convert absolute mouse position values to a relative offset value
    ' new offset = old offset + new mouse position - previous mouse position
    'RtChart1.ChnDspOffsetY = (RtChart1.ChnDspOffsetY + Y - MouseY)
    ' store current Y-axis mouse position in RtChart Window coordinates
    'MouseY = Y
    ' store current Y-axis Cursor1 position for delta measurements
    'Cursor1Volts = MouseY
    ' update Cursor1 position text box
    'CurY1AbsPos.Text = Cursor1Volts.ToString()
    ' calculate and update voltage cursors' delta
    'DeltaVolts.Text = (Cursor1Volts - Cursor2Volts).ToString()
    '
    'Case ChannelName.Cursor2 ' bottom voltage cursor (horizontal)
    'RtChart1.ChnDspOffsetY = (RtChart1.ChnDspOffsetY + Y - MouseY)
    'MouseY = Y
    'Cursor2Volts = MouseY
    'CurY2AbsPos.Text = Cursor2Volts.ToString()
    'DeltaVolts.Text = (Cursor1Volts - Cursor2Volts).ToString()
    '
    'Case ChannelName.Cursor3 ' left time cursor (vertical)
    'RtChart1.ChnDspOffsetX = (RtChart1.ChnDspOffsetX + X - MouseX)
    'MouseX = X
    'Cursor3Time = MouseX
    'CurX3AbsPos.Text = Cursor3Time.ToString()
    'DeltaTime.Text = (Cursor4Time - Cursor3Time).ToString()
    ' move voltage cursor to data point intersecting with time cursor
    'If AutoCursorFlag Then 'AutoMoveCursor(ChannelName.Cursor1, Cursor3Time)
    '
    'Case ChannelName.Cursor4 ' right time cursor (vertical)
    'RtChart1.ChnDspOffsetX = (RtChart1.ChnDspOffsetX + X - MouseX)
    'MouseX = X
    'Cursor4Time = MouseX
    'CurX4AbsPos.Text = Cursor4Time.ToString()
    'DeltaTime.Text = (Cursor4Time - Cursor3Time).ToString()
    ' move voltage cursor to data point intersecting with time cursor
    'If AutoCursorFlag Then 'AutoMoveCursor(ChannelName.Cursor2, Cursor4Time)
    '
    'Case Else
    '
    'End Select
    '
    ' synchronous mode forces paint event to occur on each point and control returns after
    'ReflectionHelper.LetMember(RtChart1, "FramesPerSec", -1)
    '
    ''UPGRADE_WARNING: (1068) RtChart_ChnTranslate of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
    'RtChart1.ChnDspAction = ChannelDisplayAction.Translate ' update channel position
    '
    ' asynchronous mode allows control to return immediately and paint when Windows is idle
    'ReflectionHelper.LetMember(RtChart1, "FramesPerSec", 0)
    '
    'End Sub

    'UPGRADE_NOTE: (7001) The following declaration (RtChart1_MouseUp) seems to be dead code More Information: https://www.mobilize.net/vbtonet/ewis/ewi7001
    'Private Sub RtChart1_MouseUp(ByRef Button As Integer, ByRef Shift As Integer, ByRef X As Single, ByRef Y As Single, ByRef Obj As Integer, ByRef position As Integer, ByRef ObjType As Integer)
    'Dim RtChart_ChnNormalColor As Object
    '
    ' RtChart Mouse (button) Up event procedure, executes anytime mouse pointer is over Rtchart
    ' and mouse button is let up. Changes plot color back to normal, Mouse pointer back to "hand" cursor
    ' Scroll bars are updated for changes in channel offset
    '
    'If LastObject > 0 Then ' if object was selected by mouse down event
    '
    ' select channel (object)
    'RtChart1.LogicalChannel = (LastObject)
    ' "unselect" channel by changing pen color back to normal
    ''UPGRADE_WARNING: (1068) RtChart_ChnNormalColor of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
    'RtChart1.ChnDspAction = ((ChannelDisplayAction.NormalColor))
    '
    'LastObject = -1 ' object move is complete, reset value for mouse move event
    '
    'End If
    '
    'End Sub

    'UPGRADE_NOTE: (7001) The following declaration (RtChart1_Refresh) seems to be dead code More Information: https://www.mobilize.net/vbtonet/ewis/ewi7001
    'Private Sub RtChart1_Refresh(ByRef Chn As Integer, ByRef Samples As Integer)
    '
    ' RTChart.Action = RtChart_ReScale causes Refresh events for each displayed channel.
    ' Refresh occurs only if entire chart needs to be redrawn (new plot outside current plot area).
    ' Plots should be re-drawn after ReScale Action to maintain display resolution if changes in
    ' Window height and width are made.
    '
    ' re-draw data channels only
    'result = RtChart1.ChartData(Chn, NumSamples, VBDataArrayY, 0, 0, 0)
    '
    'End Sub

    Private Sub ScreenToWindow(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal Obj As Integer, ByRef X As Single, ByRef Y As Single)

        ' Convert screen coord in twips to world coordinates (engineering units) for object
        rtChart.LogicalChannel = (Obj) ' select channel object
        rtChart.LogicalWindow = rtChart.ChnDspWindow ' select channel's window
        rtChart.LogicalViewport = rtChart.ChnDspViewport ' select channel's viewport

        ' WndX & WndY properties take twips and return RtChart Window units

        rtChart.WndX = (X) ' convert twips to window coord
        X = rtChart.WndX ' return world coord

        rtChart.WndY = (Y) ' convert twips to window coord
        Y = rtChart.WndY ' return world coord

    End Sub

    Private Sub ZeroChannelOffsets(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal firstChannel As Integer, ByVal lastChannel As Integer)



        ' prevent each channel from being re-painted individually on Translate Action
        rtChart.ChartAction = ChartAction.DisablePaint

        For channel As Integer = firstChannel To lastChannel

            ' Describe Channel
            rtChart.LogicalChannel = channel ' select channel
            rtChart.ChnDspOffsetY = 0 ' zero offset
            rtChart.ChnDspOffsetX = 0 ' zero offset

            rtChart.ChnDspAction = ChannelDisplayAction.Translate ' update channel position

        Next channel

        ' paint entire window with new channel positions for all channels
        rtChart.ChartAction = ChartAction.EnablePaint

    End Sub
    Private Sub Form_Closed(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MyBase.Closed
    End Sub
End Class