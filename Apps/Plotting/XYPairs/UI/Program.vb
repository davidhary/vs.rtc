Option Strict Off
Option Explicit On
Imports System
Module XYPairs
    ' LabOBJX Real-Time Chart - Example Code
    ' (C) Copyright 1995, Scientific Software Tools, Inc.
    ' All Rights Reserved.
    '
    Public Const AssemblyTitle As String = "Real Time Strip Chart XY Pairs Demo"
    Public Const AssemblyDescription As String = "Real Time Strip XY Pairs Demo"
    Public Const AssemblyProduct As String = "Visuals.Real.Time.Chart.XYPairs"


    Public result As Integer
    Public SelectedChannel As Integer ' currently selected data channel
    Public VBDataArrayY(,) As Single ' VB array to store XY pair time and voltage data values

    Public Const XData As Integer = 0 ' index used to reference X data in XY pair array
    Public Const YData As Integer = 1 ' index used to reference Y data in XY pair array

    ' signal waveform limits (volts peak)
    Public Const MaxVolts As Integer = 10
    Public Const MinVolts As Integer = -10

    Public Const NumSamples As Integer = 1000 ' number of samples
    Public Const NumChannels As Integer = 1 ' number of samples
    Public Const VoltsPerDiv As Integer = 5 ' vertical scaling is based on volts per division
    Public Const TimePerDiv As Integer = 100 ' horizontal scaling is based on seconds per division

    Public Const TWO_PIE As Double = 2 * 3.1415926535898

    ' grid constants
    Public Const MAJOR_HORZ_DIV As Integer = 10
    Public Const MAJOR_VERT_DIV As Integer = 8
    Public Const MINOR_DIVISIONS As Integer = 5
End Module