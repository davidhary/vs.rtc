Option Strict Off
Option Explicit On
Imports System
Imports System.Windows.Forms
Imports isr.Visuals.RealTimeChart
Partial Friend Class Form1
	Inherits System.Windows.Forms.Form

	' LabOBJX Real-Time Chart - Example Code
	' (C) Copyright 1995, Scientific Software Tools, Inc.
	' All Rights Reserved.
	'


	Dim i As Integer ' index in for/next loops
	Dim result As Integer ' return value from function call
	Dim TempValue As Integer ' temporary storage variable
	Dim AutoCursorFlag As Integer ' boolean to control auto cursor mode

	' used in RtChart event procedures for re-positioning data channels and cursors
	Dim LastObject As Integer ' selected channel on MouseDown event
	Dim MouseX As Single ' previous mouse X-axis position
	Dim MouseY As Single ' previous mouse Y-axis position
	Dim CursorVolts As Single
	Dim Cursor1Volts As Single ' voltage position value of cursor1
	Dim Cursor2Volts As Single ' voltage position value of cursor2
	Dim Cursor3Time As Single ' time position value of cursor3
	Dim Cursor4Time As Single ' time position value of cursor4

	Private isInitializingComponent As Boolean
	Public Sub New()
		MyBase.New()
		If _Instance Is Nothing Then
			If _InitializingInstance Then
				_Instance = Me
			Else
				Try
					'For the start-up form, the first instance created is the default instance.
					If Not (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint Is Nothing) AndAlso System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType Is Me.GetType() Then
						_Instance = Me
					End If

				Catch
				End Try
			End If
		End If
		'This call is required by the Windows Form Designer.
		isInitializingComponent = True
		InitializeComponent()
		isInitializingComponent = False
		ReLoadForm(False)
	End Sub


	Private Sub AutoCursor_CheckStateChanged(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles AutoCursor.CheckStateChanged
		AutoCursorFlag = AutoCursor.CheckState
	End Sub

	Private Sub AutoMoveCursor(ByRef YCursorName As Integer, ByRef XCursorTime As Single)

		' takes name of Y cursor to auto position and time position of X cursor
		' globals - SelectedChannel, NumSamples, CursorVolts, Cursor1Volts, Cursor2Volts, LastObject

		RtChart1.LogicalChannel = (SelectedChannel) ' select current data channel to get its offset

		' index = "time" (in samples) - data channel X-axis offset
		' index i must be within data buffer index range
		i = XCursorTime - RtChart1.ChnDspOffsetX
		If i < 0 Then i = 0
		If i >= NumSamples Then i = NumSamples - 1

		' get voltage point which intersects with time YCursorName, add data channel Y-axis offset
		' compensate for any offset in data plot
		CursorVolts = RtChart1.ChnDspOffsetY + VBDataArrayY.GetValue(i, SelectedChannel)

		' use absolute cursor voltage value for readout and delta values
		Select Case YCursorName ' select which cursor voltage to update for text box
			Case ChannelName.Cursor1
				Cursor1Volts = CursorVolts
				CurY1AbsPos.Text = Cursor1Volts.ToString() ' update Cursor1 position text box
			Case ChannelName.Cursor2
				Cursor2Volts = CursorVolts
				CurY2AbsPos.Text = Cursor2Volts.ToString() ' update Cursor2 position text box
		End Select

		' select volts cursor and get offset to compensate for existing offset in cursor
		' to correctly place cursor over data plot
		RtChart1.LogicalChannel = (YCursorName)
		CursorVolts -= RtChart1.ChnDspOffsetY

		' move volts cursor to new position
		DrawCursor(RtChart1, YCursorName, RtChart1.WndXmin, CursorVolts, RtChart1.WndWidth, CursorVolts)

		' re-select current object (YCursorName) for mouse up event
		RtChart1.LogicalChannel = (LastObject)

		' calculate and update voltage cursors' delta
		DeltaVolts.Text = (Cursor1Volts - Cursor2Volts).ToString()

	End Sub

	Private Sub DrawCursor(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal Chn As Integer, ByVal X1 As Single, ByVal Y1 As Single, ByVal X2 As Single, ByVal Y2 As Single)
		' draws a cursor (2-point line) at the coordinates given

		' Draw a single cursor
		Dim LineCoord(4) As Single

		' assign coordinates of cursor line to array
		LineCoord(0) = X1
		LineCoord(1) = Y1
		LineCoord(2) = X2
		LineCoord(3) = Y2

		' pass 2 points (4 coordinates) for single line in LineCoord array
		result = RtChart.ChartData(Chn, 2, LineCoord, 0, 0, 0)

	End Sub

	Private Sub DrawCursors(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl)
		
		' determines the coordinates to draw four cursors at fixed positions between two grid lines
		' based on the current channel's window size and calls DrawCursor to plot

		' cursor position variables

		' prevent each cursor from being re-painted individually on Initialize Action
		RtChart.ChartAction = ChartAction.DisablePaint

		' zero cursor channel offsets
		ZeroChannelOffsets(RtChart, ChannelName.Cursor1, ChannelName.Cursor4)

		' select channel to get currently selected channel's window & view port
		rtChart.LogicalChannel = ( SelectedChannel)

		' select appropriate window to reference specified Window properties
		RtChart.LogicalWindow = rtChart.ChnDspWindow ' select "time" window

		' calculate absolute full-scale (maximum) window position
		Dim PosFS_WndY As Single = rtChart.WndYmin + rtChart.WndHeight
		Dim PosFS_WndX As Single = rtChart.WndXmin + rtChart.WndWidth

		' assign values to cursor position variables, locate cursors 1.5 divs inside frame
		Dim PosY1 As Single = PosFS_WndY - 1.5 * rtChart.WndHeight / MAJOR_VERT_DIV ' -1.5 * (WndHeight / 8 divs)
		Dim PosY2 As Single = rtChart.WndYmin + 1.5 * rtChart.WndHeight / MAJOR_VERT_DIV ' +1.5 * (WndHeight / 8 divs)
		Dim PosX3 As Single = rtChart.WndXmin + 1.5 * rtChart.WndWidth / MAJOR_HORZ_DIV ' +1.5 * (WndWidth / 10 divs)
		Dim PosX4 As Single = PosFS_WndX - 1.5 * rtChart.WndWidth / MAJOR_HORZ_DIV ' -1.5 * (WndWidth / 10 divs)

		' plot four cursor lines
		DrawCursor(RtChart, ChannelName.Cursor1, rtChart.WndXmin, PosY1, PosFS_WndX, PosY1)
		DrawCursor(RtChart, ChannelName.Cursor2, rtChart.WndXmin, PosY2, PosFS_WndX, PosY2)
		DrawCursor(RtChart, ChannelName.Cursor3, PosX3, rtChart.WndYmin, PosX3, PosFS_WndY)
		DrawCursor(RtChart, ChannelName.Cursor4, PosX4, rtChart.WndYmin, PosX4, PosFS_WndY)

		' paint entire window with new positions for all cursors
		RtChart.ChartAction = ChartAction.EnablePaint

		' update text readout of absolute cursor position values
		CurY1AbsPos.Text = PosY1.ToString()
		CurY2AbsPos.Text = PosY2.ToString()
		CurX3AbsPos.Text = PosX3.ToString()
		CurX4AbsPos.Text = PosX4.ToString()

		' update text readout of relative (delta) cursor position values
		DeltaVolts.Text = (PosY1 - PosY2).ToString()
		DeltaTime.Text = (PosX4 - PosX3).ToString()

	End Sub

	Private Sub Form_Load()

		GenerateSineWaveArrays(NumChannels, NumSamples, MaxVolts)

		InitializeRtChart(RtChart1)

		ChartPen.InitChannelsPens(RtChart1)

		InitializeChannels(RtChart1, NumChannels, NumSamples)

		InitializeCursors(RtChart1, 1)

		PlotData(RtChart1, NumChannels, NumSamples)

		DrawCursors(RtChart1)

		SelectedChannel = 1

	End Sub

	Private Sub GenerateSineWaveArrays(ByVal numChannels As Integer, ByVal numSamples As Integer, ByRef VoltsPeak As Single)

		Dim NumCycles, CycleTime As Single

		' NumSamples is 0-based, NumChannels is 1-based
		VBDataArrayY = Array.CreateInstance(GetType(Single), New Integer() {NumSamples + 1, NumChannels}, New Integer() {0, 1}) ' dimension array for 2D values

		Dim RadiansPerSample As Single = TWO_PIE / NumSamples

		' NumCycles determines # of cycles in buffer
		For channel As Integer = 1 To NumChannels
			NumCycles = ((channel - 1) * RadiansPerSample)

			For sample As Integer = 0 To NumSamples - 1
				CycleTime = RadiansPerSample + NumCycles
				VBDataArrayY.SetValue(VoltsPeak * Math.Sin(sample * CycleTime), sample, channel)

			Next sample
		Next channel

	End Sub

	Private Sub InitializeChannels(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal numChannels As Integer, ByVal samplesPerChannel As Integer)
		Dim RtChart_AutoIncr, RtChart_ChnInitialize, RtChart_CRT, RtChart_Horz, RtChart_Lines, RtChart_Scalar, RtChart_Single, RtChart_Vert As Object

		' Initialize CRT Control

		' Window Description
		RtChart.LogicalWindow = 1 ' select X-axis time window
		RtChart.WndXmin = 0 ' left minimum abscissa
		RtChart.WndWidth = (TimePerDiv * MAJOR_HORZ_DIV) ' in Seconds (Time/div * 10 divisions) 100 * 10 = 1000
		RtChart.WndHeight = (VoltsPerDiv * MAJOR_VERT_DIV) ' in Volts (Volts/div * 8 divisions) 5 * 8 = 40
		RtChart.WndYmin = (RtChart.WndHeight/ -2) ' bottom minimum ordinate (negative full scale) 40 / -2 = -20

		' initialize channel

		' Describe Channel
		rtChart.LogicalChannel = ( ChannelName.Channel1) ' select channel
		RtChart.ChnDspWindow = 1 ' assign X-axis (time) window to channel
		RtChart.ChnDspViewport = 1 ' specify viewport
		RtChart.ChnDspPen = ChannelName.Channel1 ' specify pen, same value as channel
		RtChart.ChnDspBufLen = samplesPerChannel ' input buffer length for 2D array
		'UPGRADE_WARNING: (1068) RtChart_CRT of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.ChnDspMode = ChannelDisplayMode.CRT ' per channel display mode, use RunMode constants
		'UPGRADE_WARNING: (1068) RtChart_Lines of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.ChnDspStyle = ChannelDisplayStyle.Lines ' use line style for plotting arrays
		RtChart.ChnDspOffsetX = 0 ' set offset to 0
		RtChart.ChnDspOffsetY = 0 ' set offset to 0

		' Describe horizontal dimension
		'UPGRADE_WARNING: (1068) RtChart_Horz of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.ChannelDataDimension = LogicalDimension.Horizontal
		'UPGRADE_WARNING: (1068) RtChart_AutoIncr of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.ChnDataShape = ChannelDataShape.AutoIncr
		RtChart.ChnDataOffset = 0 ' offset into buffer (in points)
		RtChart.ChnDataIncr = 1 ' logical increment to next value in array

		' Describe vertical dimension
		'UPGRADE_WARNING: (1068) RtChart_Vert of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.ChannelDataDimension = LogicalDimension.Vertical
		'UPGRADE_WARNING: (1068) RtChart_Scalar of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.ChnDataShape = ChannelDataShape.Scalar
		'UPGRADE_WARNING: (1068) RtChart_Single of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.ChnDataType = ChannelDataType.Single
		RtChart.ChnDataOffset = 0 ' offset into buffer (in points)
		RtChart.ChnDataIncr = 1 ' logical increment to next value for 2D array

		'UPGRADE_WARNING: (1068) RtChart_ChnInitialize of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.ChnDspAction = ChannelDisplayAction.Initialize

	End Sub

	Private Sub InitializeCursors(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal window As Integer)
		Dim RtChart_ChnInitialize, RtChart_CRT, RtChart_Lines, RtChart_Pair, RtChart_Single, RtChart_Vert As Object

		' Setup cursor channel parameters

		For channel As Integer = ChannelName.Cursor1 To ChannelName.Cursor4

			RtChart.LogicalChannel = Channel
			RtChart.ChnDspWindow = window ' logical coord range
			RtChart.ChnDspViewport = 1 ' default display location
			RtChart.ChnDspPen = ChannelName.Cursor1 ' drawing pen, same value as cursor1
			RtChart.ChnDspBufLen = 2 ' input buffer length is 2 point for line
			'UPGRADE_WARNING: (1068) RtChart_CRT of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
			RtChart.ChnDspMode = ChannelDisplayMode.CRT ' channel display mode
			'UPGRADE_WARNING: (1068) RtChart_Lines of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
			RtChart.ChnDspStyle = ChannelDisplayStyle.Lines
			RtChart.ChnDspOffsetX = 0
			RtChart.ChnDspOffsetY = 0

			' Describe input channel as 2 point line for cursor
			'UPGRADE_WARNING: (1068) RtChart_Vert of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
			RtChart.ChannelDataDimension = LogicalDimension.Vertical
			'UPGRADE_WARNING: (1068) RtChart_Pair of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
			RtChart.ChnDataShape = ChannelDataShape.Pair
			'UPGRADE_WARNING: (1068) RtChart_Single of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
			RtChart.ChnDataType = ChannelDataType.Single
			RtChart.ChnDataOffset = 0 ' offset into buffer (in points)
			RtChart.ChnDataIncr = 1 ' logical increment to next value

			'UPGRADE_WARNING: (1068) RtChart_ChnInitialize of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
			RtChart.ChnDspAction = ChannelDisplayAction.Initialize

		Next channel

	End Sub

	Private Sub InitializeRtChart(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl)
		Dim RtChart_Color16, RtChart_HorzVertAxes, RtChart_HorzVertGrid, RtChart_Inset, RtChart_MajorMinorAxesTics, RtChart_MajorMinorFrameTics, RtChart_Raised As Object

		' Global operation properties
		RtChart.ImageFile = String.Empty ' specifies file name for Write action
		RtChart.HitTest = True ' enables mouse pointer "hit" detection
		RtChart.FramesPerSec = 0 ' 0 for asynchronous, -1 for synchronous
		'RtChart.Image = 0                'hDC to offscreen bmp??? RO

		' Border description properties
		RtChart.BorderColor = Color.LightGray
		RtChart.LightColor = Color.DarkGray
		RtChart.ShadowColor = Color.Black
		'UPGRADE_WARNING: (1068) RtChart_Inset of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.BevelInner = BevelStyle.Inset
		'UPGRADE_WARNING: (1068) RtChart_Raised of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.BevelOuter = BevelStyle.Raised
		RtChart.BevelWidthInner = ( 5)
		RtChart.BevelWidthOuter = ( 5)
		RtChart.BorderWidth = 3
		RtChart.Outline = True
		RtChart.OutlineColor = Color.Black
		RtChart.RoundedCorners = False

		' Graticule description properties
		RtChart.BackColor = Color.DarkCyan
		RtChart.AutoSize = ( True) ' true forces square border, frame is always square

		RtChart.FrameOn = True
		'UPGRADE_WARNING: (1068) RtChart_MajorMinorFrameTics of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.FrameTics = FrameTicsStyle.MajorMinorFrameTics
		RtChart.FrameColor = Color.White

		'UPGRADE_WARNING: (1068) RtChart_HorzVertAxes of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.AxesType  = AxesType.HorizontalVertical
		'UPGRADE_WARNING: (1068) RtChart_MajorMinorAxesTics of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.AxesTics = AxesTicsStyle.MajorMinorAxesTics
		RtChart.AxesColor = Color.White

		RtChart.GridOn = False
		'UPGRADE_WARNING: (1068) RtChart_HorzVertGrid of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.GridType = GridType.HorizontalVertical
		RtChart.GridColor = Color.LightGray

		RtChart.MajorDivHorz = MAJOR_HORZ_DIV ' major horizontal divisions
		RtChart.MajorDivVert = MAJOR_VERT_DIV ' major vertical divisions
		RtChart.MinorDiv = MINOR_DIVISIONS ' minor divisions
		RtChart.MarginLeft = 2 ' left Graticule border
		RtChart.MarginTop = 2 ' top Graticule border
		RtChart.MarginRight = 2 ' right Graticule border
		RtChart.MarginBottom = 2 ' bottom Graticule border

		'UPGRADE_WARNING: (1068) RtChart_Color16 of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		RtChart.ColorDepth = ColorDepth.Color16 '  256 palette type adapter only windows_g

	End Sub

	Private Sub PlotData(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal numChannels As Integer, ByVal numPoints As Integer)

		' RtChart exports ChartData function to plot data of selected channel


		RtChart.FramesPerSec = -1 ' synchronous mode forces paint event to occur on each point

		For channel As Integer = ChannelName.Channel1 To NumChannels
			result = RtChart.ChartData(channel, NumPoints, VBDataArrayY, (channel - 1) * NumPoints, 0, 0)
		Next channel

		RtChart.FramesPerSec = 0 ' asynchronous mode

	End Sub

	Private Sub ResetCursors_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles ResetCursors.Click
		

		' draw (re-draw) cursors in pre-defined position

		' prevent each cursor from being re-painted individually on Initialize Action
		RtChart1.ChartAction =  ChartAction.DisablePaint

		' zero cursor channel offsets
		ZeroChannelOffsets(RtChart1, ChannelName.Cursor1, ChannelName.Cursor4)

		' DrawCursors zeros cursor offsets, if any
		DrawCursors(RtChart1)

		' paint entire window with new positions for all cursors
		RtChart1.ChartAction =  ChartAction.EnablePaint

	End Sub

	'UPGRADE_NOTE: (7001) The following declaration (RtChart1_MouseDown) seems to be dead code More Information: https://www.mobilize.net/vbtonet/ewis/ewi7001
	'Private Sub RtChart1_MouseDown(ByRef Button As Integer, ByRef Shift As Integer, ByRef X As Single, ByRef Y As Single, ByRef Obj As Integer, ByRef position As Integer, ByRef ObjType As Integer)
		'Dim RtChart_ChnIntenseColor As Object
		'
		' RtChart Mouse (button) Down event procedure, executes anytime mouse pointer is over Rtchart
		' and mouse button is pressed down. Used to select channels (and cursors) for dragging.
		' Changes plot color from normal to intense
		' Mouse pointer is changed to "size" cursor and restored to "hand" in Mouse Up event procedure.
		'
		'RtChart1.SetFocus         ' move focus to RtChart control for copy and other functions???
		'
		'If Obj > 0 And Obj <> LastObject Then ' if object selected, but not previously selected
			'
			'RtChart1.LogicalChannel = (Obj) ' select object that was hit
			''UPGRADE_WARNING: (1068) RtChart_ChnIntenseColor of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
			'RtChart1.ChnDspAction = ((ChannelDisplayAction.IntenseColor)) ' use intense pen color to highlight
			'
			'ScreenToWindow(RtChart1, Obj, X, Y) ' convert screen positions to window coordinates
			'
			' used to calculate offset from current position
			'MouseX = X ' store current mouse location X
			'MouseY = Y ' store current mouse location Y
			'
			'LastObject = Obj ' save selected object to be moved
			'
		'End If
		'
	'End Sub

	'UPGRADE_NOTE: (7001) The following declaration (RtChart1_MouseMove) seems to be dead code More Information: https://www.mobilize.net/vbtonet/ewis/ewi7001
	'Private Sub RtChart1_MouseMove(ByRef Button As Integer, ByRef Shift As Integer, ByRef X As Single, ByRef Y As Single, ByRef Obj As Integer, ByRef position As Integer, ByRef ObjType As Integer)
		'
		'
		' convert mouse pointer position to world coordinates (engineering units)
		'ScreenToWindow(RtChart1, SelectedChannel, X, Y) ' use currently selected channel's window
		'
		'If LastObject <= 0 Then 'Exit Sub ' no objects were selected by mouse down event, exit sub
		'
		'RtChart1.LogicalChannel = (LastObject) ' select object to change offset (move)
		'
		' drag channels (move) by changing channel offsets
		' use displacement relative to original position
		'Select Case LastObject
			'Case ChannelName.Cursor1 ' top voltage cursor (horizontal)
				' must convert absolute mouse position values to a relative offset value
				' new offset = old offset + new mouse position - previous mouse position
				'RtChart1.ChnDspOffsetY = (RtChart1.ChnDspOffsetY + Y - MouseY)
				' store current Y-axis mouse position in RtChart Window coordinates
				'MouseY = Y
				' store current Y-axis Cursor1 position for delta measurements
				'Cursor1Volts = MouseY
				' update Cursor1 position text box
				'CurY1AbsPos.Text = Cursor1Volts.ToString()
				' calculate and update voltage cursors' delta
				'DeltaVolts.Text = (Cursor1Volts - Cursor2Volts).ToString()
				'
			'Case ChannelName.Cursor2 ' bottom voltage cursor (horizontal)
				'RtChart1.ChnDspOffsetY = (RtChart1.ChnDspOffsetY + Y - MouseY)
				'MouseY = Y
				'Cursor2Volts = MouseY
				'CurY2AbsPos.Text = Cursor2Volts.ToString()
				'DeltaVolts.Text = (Cursor1Volts - Cursor2Volts).ToString()
				'
			'Case ChannelName.Cursor3 ' left time cursor (vertical)
				'RtChart1.ChnDspOffsetX = (RtChart1.ChnDspOffsetX + X - MouseX)
				'MouseX = X
				'Cursor3Time = MouseX
				'CurX3AbsPos.Text = Cursor3Time.ToString()
				'DeltaTime.Text = (Cursor4Time - Cursor3Time).ToString()
				' move voltage cursor to data point intersecting with time cursor
				'If AutoCursorFlag Then 'AutoMoveCursor(ChannelName.Cursor1, Cursor3Time)
				'
			'Case ChannelName.Cursor4 ' right time cursor (vertical)
				'RtChart1.ChnDspOffsetX = (RtChart1.ChnDspOffsetX + X - MouseX)
				'MouseX = X
				'Cursor4Time = MouseX
				'CurX4AbsPos.Text = Cursor4Time.ToString()
				'DeltaTime.Text = (Cursor4Time - Cursor3Time).ToString()
				' move voltage cursor to data point intersecting with time cursor
				'If AutoCursorFlag Then 'AutoMoveCursor(ChannelName.Cursor2, Cursor4Time)
				'
			'Case Else
				'
		'End Select
		'
		' synchronous mode forces paint event to occur on each point and control returns after
		'ReflectionHelper.LetMember(RtChart1, "FramesPerSec", -1)
		'
		''UPGRADE_WARNING: (1068) RtChart_ChnTranslate of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
		'RtChart1.ChnDspAction = ChannelDisplayAction.Translate ' update channel position
		'
		' asynchronous mode allows control to return immediately and paint when Windows is idle
		'ReflectionHelper.LetMember(RtChart1, "FramesPerSec", 0)
		'
	'End Sub

	'UPGRADE_NOTE: (7001) The following declaration (RtChart1_MouseUp) seems to be dead code More Information: https://www.mobilize.net/vbtonet/ewis/ewi7001
	'Private Sub RtChart1_MouseUp(ByRef Button As Integer, ByRef Shift As Integer, ByRef X As Single, ByRef Y As Single, ByRef Obj As Integer, ByRef position As Integer, ByRef ObjType As Integer)
		'Dim RtChart_ChnNormalColor As Object
		'
		' RtChart Mouse (button) Up event procedure, executes anytime mouse pointer is over Rtchart
		' and mouse button is let up. Changes plot color back to normal, Mouse pointer back to "hand" cursor
		' Scroll bars are updated for changes in channel offset
		'
		'If LastObject > 0 Then ' if object was selected by mouse down event
			'
			' select channel (object)
			'RtChart1.LogicalChannel = (LastObject)
			' "unselect" channel by changing pen color back to normal
			''UPGRADE_WARNING: (1068) RtChart_ChnNormalColor of type Variant is being forced to Scalar. More Information: https://www.mobilize.net/vbtonet/ewis/ewi1068
			'RtChart1.ChnDspAction = ((ChannelDisplayAction.NormalColor))
			'
			'LastObject = -1 ' object move is complete, reset value for mouse move event
			'
		'End If
		'
	'End Sub

	'UPGRADE_NOTE: (7001) The following declaration (RtChart1_Refresh) seems to be dead code More Information: https://www.mobilize.net/vbtonet/ewis/ewi7001
	'Private Sub RtChart1_Refresh(ByRef Chn As Integer, ByRef Samples As Integer)
		' RTChart.Action = RtChart_ReScale causes Refresh events for each displayed channel.
		' Refresh occurs only if entire chart needs to be redrawn (new plot outside current plot area).
		' Plots should be re-drawn after ReScale Action to maintain display resolution if changes in
		' Window height and width are made.
		'
		' re-draw data channels only
		'result = RtChart1.ChartData(Chn, NumSamples, VBDataArrayY, (Chn - 1) * Samples, 0, 0)
		'
	'End Sub

	Private Sub ScreenToWindow(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal Obj As Integer, ByRef X As Single, ByRef Y As Single)

		' Convert screen coord in twips to world coordinates (engineering units) for object
		rtChart.LogicalChannel = ( Obj) ' select channel object
		RtChart.LogicalWindow = rtChart.ChnDspWindow ' select channel's window
		RtChart.LogicalViewport = rtChart.ChnDspViewport ' select channel's viewport

		' WndX & WndY properties take twips and return RtChart Window units

		rtChart.WndX = ( X) ' convert twips to window coord
		X = rtChart.WndX ' return world coord

		rtChart.WndY = ( Y) ' convert twips to window coord
		Y = rtChart.WndY ' return world coord

	End Sub

	Private Sub ZeroChannelOffsets(ByVal rtChart As isr.Visuals.RealTimeChart.RealTimeChartControl, ByVal firstChannel As Integer, ByVal lastChannel As Integer)
		


		' prevent each channel from being re-painted individually on Translate Action
		RtChart.ChartAction = ChartAction.DisablePaint

		For channel As Integer = FirstChannel To LastChannel

			' Describe Channel
			RtChart.LogicalChannel = Channel ' select channel
			RtChart.ChnDspOffsetY = 0 ' zero offset
			RtChart.ChnDspOffsetX = 0 ' zero offset

			RtChart.ChnDspAction = ChannelDisplayAction.Translate ' update channel position

		Next channel

		' paint entire window with new channel positions for all channels
		RtChart.ChartAction = ChartAction.EnablePaint

	End Sub
	Private Sub Form_Closed(ByVal eventSender As Object, ByVal eventArgs As EventArgs) Handles MyBase.Closed
	End Sub
End Class